using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using YooAsset;
//using UnityEngine.ResourceManagement.AsyncOperations;
//using YooAsset;

public class StartHomeManager : MonoBehaviour
{
    [SerializeField]
    private HomeMapUiManager uiManager;
    [SerializeField]
    private MapManager mapManager;
    private int playGameLevelCount = 0;

    public MapManager MapManager { get => mapManager; }
    public HomeMapUiManager UiManager { get => uiManager; }

    private SystmConfigurationManager systmConfigurationManager;
    private bool isLoodingCompelent = false;

    public bool IsLoodingCompelent { get => isLoodingCompelent;  }

    public int PlayGameLevelCount { get => playGameLevelCount; set => playGameLevelCount = value; }
    private void Awake()
    {
        Application.targetFrameRate = 60;
        Application.runInBackground = true;
        isLoodingCompelent = true;
    }


    private void Start()
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        CloudFunction.Init();
#endif
      
        systmConfigurationManager = SystmConfigurationManager.instance;
        StartCoroutine(LoadConfigur());

    }


    private IEnumerator LoadConfigur()
    {
      

        if (YooResoursceManagur.Instace == null)
        {
            yield return  YooResoursceManagur.InitYoo();
       
        }
        var isloadNull = LoadConfigursManager.CreateLoadData();
        if (isloadNull)
        {
           yield return LoadConfigursManager.instans.Configuer(systmConfigurationManager);
        }
        if (SystemDataFactoy.Create())
        {
            yield return SystemDataFactoy.instans.Configur(systmConfigurationManager);
        }
        if (PlayerData.instance == null)
        {
            PlayerData.Create();

#if UNITY_WEBGL && !UNITY_EDITOR
                yield return PlayerData.instance.LoaderCloundData();
#elif UNITY_EDITOR   
#endif
        }

        // //等待协程次啊在完毕
        // float whileTime = 0f; 
        //while(true)
        // {
        //     whileTime += Time.deltaTime;
        //     if (
        //         LoadConfigursManager.instans.IsLoadCompelet
        //         && SystemDataFactoy.instans.YooAssetsLoadCompelet
        //         && PlayerData.instance.IsLoaddCompelet)
        //     {
        //         isLoodingCompelent = true;
        //         Debug.Log("----------------------whhile--------------------  ");
        //         Debug.Log("LoadConfigursManager  " + LoadConfigursManager.instans.IsLoadCompelet);
        //         Debug.Log("SystemDataFactoy  " + SystemDataFactoy.instans.YooAssetsLoadCompelet);
        //         Debug.Log("PlayerData  " + PlayerData.instance.IsLoaddCompelet);
        //         Debug.Log("-----------------------------------------------  ");
        //         break;
        //     }

        //     Debug.Log("LoadConfigursManager  " + LoadConfigursManager.instans.IsLoadCompelet);
        //     Debug.Log("SystemDataFactoy  " + SystemDataFactoy.instans.YooAssetsLoadCompelet);
        //     Debug.Log("PlayerData  " + PlayerData.instance.IsLoaddCompelet);
        //     Debug.Log("-----------------------------------------------  ");

        //     isLoodingCompelent = true;
        //     if (whileTime >= 30f)
        //     {
        //         Debug.Log("加载超时退出！");
        //         break;
        //     }
        // }

        isLoodingCompelent = true;
        PlayerData.instance.LocadDataOperate();
        mapManager.Configur(this);
        uiManager.Configur(this);


    }
}
