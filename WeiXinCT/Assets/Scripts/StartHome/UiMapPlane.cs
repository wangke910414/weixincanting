using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using LitJson;


public class UiMapPlane : UiWoPropAbility
{
    private AsyncOperation asyncOper;

    [SerializeField]
    private List<RestaurentCompnent> restaurentList = new List<RestaurentCompnent>();

    private RestaurentCompnent selectRestaurent;
    private  bool isMapLock;
    private bool isHaveRestaurentLock = false;
    private string unLockShopNmae = string.Empty;


    public bool IsMapLock { get => isMapLock; set => isMapLock = value; }

    public bool IsHaveRestaurentLock { get => isHaveRestaurentLock; }

    private HomeMapUiManager uiManager;

    public void Init(HomeMapUiManager uiManager)
    {
        this.uiManager = uiManager;
        isHaveRestaurentLock = false;
        selectRestaurent = null;
        unLockShopNmae = string.Empty;
       InitRestaurent();
    }



    private void InitRestaurent()
    {
        var shopDit = LoadConfigursManager.instans.ShopDict;

        for (int i=0; i<restaurentList.Count; i++)
        {
            var restaurentName = restaurentList[i].RestaurentName;
            restaurentList[i].IsLock = CheckRestaurentIsLock(restaurentName);
            if (shopDit.ContainsKey(restaurentName))
            {
                var data = shopDit[restaurentName];

                var b = PlayerData.playerDataParame.keyNumber >= data.unLockKeyCount ? true : false;
                var c =  PlayerData.restaurentUnLock.restaurentUnlockDit.ContainsKey(restaurentName);
               
               if (b && c)
                {
                    restaurentList[i].IsLock = true;
                    PlayerData.instance.curPlayeShop = restaurentList[i].RestaurentName;
                }
               else
                {
                    if (data.isDefaultLock == 1)
                        restaurentList[i].IsLock = true;
                    else
                        restaurentList[i].IsLock = false;
                }
                restaurentList[i].NeedKeyCount = data.unLockKeyCount;
                restaurentList[i].UnLockKeyCount = data.unLockKeyCount;
                restaurentList[i].RestaurentID = data.shopID;
            }
          
            restaurentList[i].UiMapPanel = this;
            restaurentList[i].SetRestaurent();
        }
         UnLockRestaurent();
    }


    public IEnumerator PlayAniamtion()
    {
        RestaurentCompnent lastRestautrent = null;
        UnLockRestaurent();

        for (int i=0; i<restaurentList.Count; i++)
        {
            if (PlayerData.instance.curPlayeShop == restaurentList[i].RestaurentName)
            {
                //金币动画
                if (PlayerData.instance.isCionFlight && PlayerData.instance.CionFlightCount > 0)
                {
                    if (PlayerData.instance.currSingleLevelData.cion > 0)
                    {
                        restaurentList[i].PlayCionAnitamin();
                        PlayerData.instance.currSingleLevelData.cion = 0;
                    }

                } 
            }
           // else
            {
                //钥匙动画
               if (isHaveRestaurentLock && PlayerData.instance.keyIsFlightAnimation)
                {
                    if (lastRestautrent != null)
                    {
                        if (PlayerData.instance.keyIsFlightAnimation)
                        {
                            lastRestautrent.PlayerKeyAnimation(restaurentList[i].transform);
                           PlayerData.instance.keyIsFlightAnimation = false;
                        }

                    }
                }
            }
          
            lastRestautrent = restaurentList[i];
        }

        yield return null;
    }

    public void ClickLevel(int Lenvel)
    {
        if (uiManager == null)
        {
            return;
        }
        if (!uiManager.isLoadingConpelet)
        {
            return;
        }
        var strName = "Shop_" + Lenvel;
        if (!LoadConfigursManager.instans.ShopDict.ContainsKey(strName))
        {
            return;
        }
        var shop = LoadConfigursManager.instans.ShopDict[strName];
        PlayerData.instance.curPlayeShop = strName;

        selectRestaurent = restaurentList[Lenvel - 1];

        if  (!PlayerData.restaurentUnLock.restaurentUnlockDit.ContainsKey(strName) &&
           strName != "Shop_1")
        {
            TipDlgUnLockRestaurentMsg msg = new TipDlgUnLockRestaurentMsg();
            msg.tipDlgType = TipDlgType.UnLockRestaurentTip;
            msg.keyCount = PlayerData.playerDataParame.keyNumber;
            msg.needKeyCount = shop.unLockKeyCount;
            uiManager.TipDlgManager.OpenTipDlg(TipDlgType.UnLockRestaurentTip, this,null,msg);
            return;
        }
        uiManager.OpenLevelInfoPlane();
    }

   public void ClickLock()
    {
   
        uiManager.OpenLoackPlane();
     
    }

    public void GuideClick()
    {
        uiManager.OpenLevelInfoPlane();
    }

    public void CallCloseLevelInfoDlg()
    {

    }
    public void TeachintMsgOperation(EventTeachingMsg msg)
    {
        ClickLevel(1);
    }

    private bool CheckRestaurentIsLock(string restaurentName)
    {
        var restaurentDit = LoadConfigursManager.instans.ShopDict;
        var keyCount = PlayerData.playerDataParame.keyNumber;
        if (restaurentDit.ContainsKey(restaurentName))
        {
            return true;
        }
        
        return false;
    }


    //解锁餐厅
    private void UnLockRestaurent()
    {
        if (PlayerData.playerDataParame.keyNumber == 0)
            return;

        foreach (var item in restaurentList)
        {

            if (item.RestaurentName == "Shop_1")
                continue;

            if (item.UnLockKeyCount <= PlayerData.playerDataParame.keyNumber)
            {
                if (!PlayerData.restaurentUnLock.restaurentUnlockDit.ContainsKey(item.RestaurentName))
                {
                    item.UnLick();
                    isHaveRestaurentLock = true;
                    unLockShopNmae = item.RestaurentName;
                    Debug.Log("当前解锁餐厅名称  ！" + unLockShopNmae);
                    return;
                }
            }
        }
    }
    public void CallUnLockRestaurent()
    {
        TipDlgUnLockRestaurentMsg msg = new TipDlgUnLockRestaurentMsg();
        msg.tipDlgType = TipDlgType.UnLockRestaurentTip;
        msg.keyCount = PlayerData.playerDataParame.keyNumber;
        uiManager.TipDlgManager.OpenTipDlg(TipDlgType.UnLockRestaurentTip, this,null,msg);
    }


    /// <summary>
    /// 解锁弹窗返回消息
    /// </summary>
    /// <param name="type"></param> 窗口类型
    /// <param name="success"></param> 是否成功
    public override void TipDlgExitCallBanck(TipDlg tipDlg,TipDlgType type, bool success)
    {
        Debug.Log("说到保存消息 ");

        if (type == TipDlgType.UnLockRestaurentTip)
        {
            if (!success)
            {
                Debug.Log("钥匙解锁失败！");
                ObtanKeyLevel();
                uiManager.OpenLevelInfoPlane();
                return;
            }
     
           // Debug.Log("竟然解锁餐厅保存1   ");
            if (isHaveRestaurentLock)
            {
               // Debug.Log("竟然解锁餐厅保存2  ");
                if (unLockShopNmae != string.Empty)
                {
                    //Debug.Log("竟然解锁餐厅保存3 ");
                    if (!PlayerData.restaurentUnLock.restaurentUnlockDit.ContainsKey(unLockShopNmae))
                        PlayerData.restaurentUnLock.restaurentUnlockDit.Add(unLockShopNmae, 1);

                    var data = JsonMapper.ToJson(PlayerData.restaurentUnLock);

                  //  Debug.Log("解锁餐厅完成保存数据   " + data);
#if UNITY_WEBGL && UNITY_EDITOR
                    CoroutineHandler.StartStaticCoroutine( PlayerData.instance.SaveData("LoadRestaurentData", data));
#elif UNITY_WEBGL && !UNITY_EDITOR
                CoroutineHandler.StartStaticCoroutine( PlayerData.instance.SaveData("UpdataRestaurentLockData",data));
#endif  
                }

                foreach (var item in restaurentList)
                {
                    if (item.fingerTransform == null)
                        continue;
                    if (item.fingerTransform.gameObject.activeSelf)
                    {
                        item.UnLockCompelet();
                        var level = int.Parse(item.RestaurentID);
                        StartCoroutine(UnLockWaiOpenLevelInfoPanel(level));
                    }
                }
            }
        }
    }



    private IEnumerator UnLockWaiOpenLevelInfoPanel(int level)
    {
        yield return new WaitForSeconds(0.5f);
        ClickLevel(level);
    }

    private RestaurentCompnent GetRestautrent(string stopName)
    {
        foreach (var item in restaurentList)
        {
            if (item.RestaurentName == stopName)
            {
                return item;
            }
        }
        return null;
    }
    private void ObtanKeyLevel()
    {
       
        for (int i=0; i<restaurentList.Count; i++)
        {

            if (i == restaurentList.Count - 1)
            {
                return;
            }

            Debug.Log("回到餐厅名称    " + restaurentList[i].RestaurentName);
            if (PlayerData.playerDataParame.keyNumber >= restaurentList[i].NeedKeyCount && 
                PlayerData.playerDataParame.keyNumber < restaurentList[i +1].NeedKeyCount)
            {
                Debug.Log("回到餐厅名称 1   " +restaurentList[i].RestaurentName);
              PlayerData.instance.curPlayeShop = restaurentList[i].RestaurentName;
                return;
            }
        }
    } 

    //获取当前解锁到关卡

    //public int GetUnLockLevel()
    //{
    //    var shopDit = LoadConfigursManager.instans.ShopDict;
    //    var lvelDit = LoadConfigursManager.instans.LevelShopDataDuct;

    //    List<string> keyList = new List<string>(shopDit.Keys);
    //    //int index = 0;
    //    for (int i=0; i<keyList.Count;i ++)
    //    {
    //        var currResaurent = shopDit[keyList[i]];
    //        var levelName = currResaurent.leveData + "_1";
    //        var levelData = lvelDit[levelName];

    //        if (levelData == null)
    //            return 0;
    //        for (int j=0; j< levelData.Count; j++)
    //        {
    //            if (PlayerData.playerDataParame.keyNumber <= levelData[j].unLoackKeyCunnt)
    //            {
    //                return j+1;
    //            }
    //        }
    //    }
    //    return 0;
    //}
    private void Update()
    {
        if (uiManager!= null &&  uiManager.StartHomeManager.PlayGameLevelCount == 0)
        {
            RefrashFRestaurentLock();
        }
     }

    public void RefrashFRestaurentLock()
    {

        if (PlayerData.restaurentUnLock == null)
            return;
        var storeLock = PlayerData.restaurentUnLock.restaurentUnlockDit;

        for (int i=0; i<restaurentList.Count; i++)
        {
            if (storeLock.ContainsKey(restaurentList[i].RestaurentName) && 
               PlayerData.playerDataParame.keyNumber >= restaurentList[i].NeedKeyCount
                )
            {
                restaurentList[i].IsLock = true;
              
            }
            else if (restaurentList[i].RestaurentName == "Shop_1")
            {
                restaurentList[i].IsLock = true;
            }

            restaurentList[i].SetRestaurent();
        }
    }
}
