using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HomeMapUiManager : MonoBehaviour
{
    private StartHomeManager startHomeManager;

    [SerializeField]
    private TipDlgManager tipDlgManager;
    [SerializeField]
    private UiMapPlane mapHome;
    

    [SerializeField]
    private LevelInfoPlane levelInfoPlane;

    [SerializeField]
    private lockPlanel lockPlanel;

    [SerializeField]
    private DamondPlane damonPlane;

    [SerializeField]
    private HomePlanel homePlanel;

    [SerializeField]
    private HeadPlanel headPlanel;

    [SerializeField]
    private ShoppPlanel shopPlanel;

    [SerializeField]
    private AchievePanel achievePanel;
    [SerializeField]
    private DailyPanel dailyPanel;

    //[SerializeField]
    //private DataPlanel dataPlanel;

    [SerializeField]
    private AboutPlanel aboutPlanel;
    [SerializeField]
    private SetingPlanel setingPlanel;
    [SerializeField]
    private SignPlanel signPlanel;
    [SerializeField]
    private TeachingPanle teachingPanle;
    [SerializeField]
    private AssocriationPanel assocriationPanel;
    [SerializeField]
    private MiniMaoPanel miniMapPanel;
    [SerializeField]
    private StrengthSupplementPanel strenghtSupplementPanel;


    public Transform uiParent;


    public bool isLoadingConpelet = false;
    
    [SerializeField]
    private ComparePlayerDataPlanel comparePlayerDataPlanel;
    [SerializeField]
    private CanvasGroup shadowEfficeGrop;

    public HomePlanel HomePlanel { get => homePlanel; }
    public DamondPlane DamonPlane { get => damonPlane; }
    public HeadPlanel HeadPlanel { get => headPlanel; }
    public ShoppPlanel ShoppPlanel { get => shopPlanel; }
    public AchievePanel AchievePanel { get => achievePanel; }
    //public DataPlanel DataPlanel { get => dataPlanel; }
    public SignPlanel SignPlanel { get => signPlanel; }
    public AboutPlanel AboutPlanel { get => aboutPlanel; }
    public SetingPlanel SetingPlanel { get => setingPlanel; }
    public DailyPanel DailyPanel { get => dailyPanel; }
    public TipDlgManager TipDlgManager { get => tipDlgManager; }

    public MiniMaoPanel MiniMapPanel { get => miniMapPanel; }
    public UiMapPlane MapHome { get => mapHome; }
    public ComparePlayerDataPlanel ComparePlayerDataPlanel { get => comparePlayerDataPlanel; }
    public TeachingPanle TeachingPanle { get => teachingPanle; }

    public AssocriationPanel AssocriationPanel { get => assocriationPanel; }
    public StartHomeManager StartHomeManager { get => startHomeManager; }
    public StrengthSupplementPanel StrenghtSupplementPanel { get => strenghtSupplementPanel; }

    public void Configur(StartHomeManager startHomeManager)
    {
        this.startHomeManager = startHomeManager;
        InitUiPanel();

    }

    private void InitUiPanel()
    {
        miniMapPanel.Init(this);
        mapHome = miniMapPanel.GetCurrMap();
        if (mapHome != null)
        {
            mapHome.Init(this);
        }
        levelInfoPlane.Init(this);
        shopPlanel.Init(this);
        homePlanel.Confgur(this);
        damonPlane.Confgur(this);
        achievePanel.Init(this);
       // headPlanel.Confgur(this);
        aboutPlanel.Init(this);
        signPlanel.Init(this);
        dailyPanel.Init(this);
        strenghtSupplementPanel.Init(this);
        isLoadingConpelet = true;
        TipDlgManager.Confgur(this);
        assocriationPanel.Init(this);
        StartCompelet();
        SystemEventManager.StartListening("TeachingMsg", ReceiveTeachingMsg);
    }
    public void OpenLevelInfoPlane()
    {
        if (!startHomeManager)
            return;
        levelInfoPlane.OpenUiWo();
    }
    public void CloseLevelInfoPlane()
    {
        if (!startHomeManager)
            return;
        mapHome.CallCloseLevelInfoDlg();
    }

    public void OpenLoackPlane()
    {
        if (!startHomeManager)
            return;
        lockPlanel.OpenUiWo();
    }
    private GameObject FastOpenPlanle = null;
    public void FastOpenShopDlg(GameObject planel)
    {
        FastOpenPlanle = planel;
        shopPlanel.isFastOpen = true;
        shopPlanel.OpenUiWo();
    }

    public void FastOpenDiamondShopDlg(GameObject planel)
    {
        FastOpenPlanle = planel;
        shopPlanel.isFastOpen = true;
        shopPlanel.ShowIndex = 1;
        shopPlanel.OpenUiWo();
        shopPlanel.OnClickShowBut(1);
    }

    public void DailyFastOpen()
    {

        if (FastOpenPlanle != null)
        {
            FastOpenPlanle.GetComponent<UiWoPropAbility>().OpenUiWo();
            FastOpenPlanle = null;
        }
    }

    public void FastOpenCloseDlg()
    {
        if (FastOpenPlanle != null)
        {
            FastOpenPlanle.GetComponent<UiWoPropAbility>().OpenUiWo();
            levelInfoPlane.UpdataConsumeProp();
            FastOpenPlanle = null;
        }
    }
    public void OpenAboutPlanl()
    {
        aboutPlanel.OpenUiWo();
    }
    public void OpenSetingPlanel()
    {
        setingPlanel.OpenUiWo();
    }

    public void StartCompelet()
    {
        Debug.Log("调用Home UI 显示");
        StartCoroutine(WaitLoodCompelet());
    }
    private IEnumerator WaitLoodCompelet()
    {
        yield return homePlanel.WaitShow();
        homePlanel.gameObject.SetActive(true);
        if (PlayerData.instance.teachingValue.levelOnClick == 0)
        {
            TeachingOperation(TeachingType.LevelOnClick);
        }
        yield return mapHome.PlayAniamtion();
        homePlanel.RefeshPanel();
        yield return new WaitForSeconds(1.2f);
        if (PlayerData.instance.continueGame && !mapHome.IsHaveRestaurentLock)
        {
          levelInfoPlane.OpenUiWo();
        }
    }

    //关闭面板刷新界面
    public void RefreshPanel()
    {
        StartCoroutine(homePlanel.WaitShow());
        if (MapHome != null)
        {
            CoroutineHandler.StartStaticCoroutine(MapHome.PlayAniamtion());
        }

    }
    private void ReceiveTeachingMsg(string eventName, EventParamProperties msg)
    {
        var receiveMsg = msg as EventTeachingMsg;
        if (receiveMsg.teachingType == TeachingType.LevelOnClick)
        {
            mapHome.TeachintMsgOperation(receiveMsg);
            PlayerData.instance.teachingValue.levelOnClick++;
        }
        else if (receiveMsg.teachingType == TeachingType.SelectLevelClick)
        {
            levelInfoPlane.TeachintMsgOperation(receiveMsg);
            PlayerData.instance.teachingValue.selectLevelClick++;
        }
        else if (receiveMsg.teachingType == TeachingType.StartGameClick)
        {
            levelInfoPlane.TeachintMsgOperation(receiveMsg);
            PlayerData.instance.teachingValue.startGameClick++;
        }
        else if (receiveMsg.teachingType == TeachingType.DviceUpGradeOne)
        {
            levelInfoPlane.OnClilkUpGread(2);
            CoroutineHandler.StartStaticCoroutine (TeachingDeveiceUpGradeTwo());
        }
        else if (receiveMsg.teachingType == TeachingType.DviceUpGradeTwo)
        {
            CoroutineHandler.StartStaticCoroutine(TeachingDeveiceUpGradeYgree());
        }
        else if (receiveMsg.teachingType == TeachingType.DviceUpGradeThree)
        {
            levelInfoPlane.OnClickDeviceUp();
            PlayerData.instance.teachingValue.deveiceUpGradeClick++;
        }
        else if (receiveMsg.teachingType == TeachingType.FoodUpGradeOne)
        {
            levelInfoPlane.OnClilkUpGread(1);
            CoroutineHandler.StartStaticCoroutine(TeachingFoodUpGradeOne());
        }
        else if (receiveMsg.teachingType == TeachingType.FoodUpGradeTwo)
        {
            CoroutineHandler.StartStaticCoroutine(TeachingFoodUpGradeTwo());
        }
        else if (receiveMsg.teachingType == TeachingType.FoodUpGradeThree)
        {
            levelInfoPlane.OnClickFoodUp();
            PlayerData.instance.teachingValue.foodUpGradeClick++;
        }
        PlayerData.instance.SaveTeachingData();
    }


    public void TeachingOperation(TeachingType type)
    {
        return;
            teachingPanle.TeachingOperation(type);
    }

    private IEnumerator TeachingDeveiceUpGradeTwo()
    {
        yield return new WaitForSeconds(0.5f);
        TeachingOperation(TeachingType.DviceUpGradeTwo);
    }
    private IEnumerator TeachingDeveiceUpGradeYgree()
    {
        yield return new WaitForSeconds(0.5f);
        TeachingOperation(TeachingType.DviceUpGradeThree);
    }
    private IEnumerator TeachingFoodUpGradeOne()
    {
        yield return new WaitForSeconds(0.5f);
        TeachingOperation(TeachingType.FoodUpGradeTwo);
    }
    private IEnumerator TeachingFoodUpGradeTwo()
    {
        yield return new WaitForSeconds(0.5f);
        TeachingOperation(TeachingType.FoodUpGradeThree);
    }

    public IEnumerator UnLockRestaution()
    {
        yield return new WaitForSeconds(0.7f);
        //MapHome.UnLockRestaurent();
    }
    
}
