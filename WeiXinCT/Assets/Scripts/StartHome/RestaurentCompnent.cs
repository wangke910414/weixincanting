using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;


//����
public class RestaurentCompnent : MonoBehaviour
{
    [Header("UI")]
    [SerializeField]
    private string restaurentName;

    [SerializeField]
    private Sprite[] skin;

    [SerializeField]
    private Transform unLockFinger;
    [SerializeField]
    private Button unLockBut;
    [SerializeField]
    private GhostFlightAnimation keyAnimation;      //Կ�׷��ж���
    

    private bool isLock = false;
    private int unLockKeyCount;
    private UiMapPlane uiMapPanel;
    private string restaurentID = string.Empty;
    private int needKeyCount;


    public bool IsLock { get => isLock; set => isLock = value; }
    public string RestaurentName { get => restaurentName; }

    public int UnLockKeyCount { get => unLockKeyCount; set => unLockKeyCount = value; }
    public UiMapPlane UiMapPanel { set => uiMapPanel = value; }
    public Transform fingerTransform { get => unLockFinger; }
    public string RestaurentID { get => restaurentID; set => restaurentID = value; }
    public int NeedKeyCount { get => needKeyCount; set => needKeyCount = value; }


    public void SetRestaurent()
    {
      
        if (isLock)
        {
            this.GetComponent<Image>().sprite = skin[1];
        }
        else
        {
            this.GetComponent<Image>().sprite = skin[0];
        }
        
    }
    public void UnLick()
    {
        isLock = true;
        Debug.Log("�����ؿ� " + restaurentName);

        CoroutineHandler.StartStaticCoroutine(WaitShowGinger());
    }

    private IEnumerator WaitShowGinger()
    {
        yield return new WaitForSeconds(2);

        if (unLockFinger != null)
        {
            unLockFinger.gameObject.SetActive(true);
            unLockFinger.transform.DOShakeScale(0.6f, 0.5f);
        }

    }
   

    public void OnClickFingerUnLock()
    {
        if (!unLockFinger.gameObject.activeSelf)
            return;

        if (uiMapPanel != null)
            uiMapPanel.CallUnLockRestaurent();
    }
    public void UnLockCompelet()
    {
        unLockFinger.gameObject.SetActive(false);
        SetRestaurent();


    }
    public void PlayCionAnitamin()
    {
      var ghostAnimation = this.transform.GetComponent<GhostFlightAnimation>();
        if (ghostAnimation != null)
        {
            Debug.Log("���Ŷ����� ");
          ghostAnimation.PlayFlightCionAnimation(6);
        }
    }

    public void PlayerKeyAnimation(Transform target)
    {
        Debug.Log("����Կ�׶���2");
        if (keyAnimation == null)
            return;
        Debug.Log("����Կ�׷��ж���3");
       keyAnimation.PlayFlightKeyAnimation(target,6);
    }

}
