using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopItem : MonoBehaviour
{
    private string shopNmae;
    private string unLockorice;
    private int mapIndex;

    
    public string ShopNmae { get => shopNmae; set => shopNmae = value; }
    public string UnLockorice { get => unLockorice; set => unLockorice = value; }
    public int MapIndex { get => mapIndex; set => mapIndex = value; }

}
