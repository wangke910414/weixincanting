using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TipDlgManager : MonoBehaviour, CTinterface
{
    [SerializeField]
    private List<TipDlg> tipDlgList = new List<TipDlg>();

    private UiWoPropAbility parentDlg;
    private TipDlg currTipDlg = null;

    HomeMapUiManager uiManager;

    public void Confgur(HomeMapUiManager uiManager)
    {
        this.uiManager = uiManager;
        Init();
    }

    private void Init()
    {
        for (int i = 0; i < tipDlgList.Count; i++)
        {
            tipDlgList[i].IipDligManager = this;
      
        }
    }

    public void OpenTipDlg(TipDlgType type, UiWoPropAbility dlg, string discrible = null, TipDlgMessage msg = null)
    {
        if (this.gameObject.activeSelf == true)
        {
            CoroutineHandler.StartStaticCoroutine(WaitOpen(type, dlg, discrible, msg));
            return;
        }
        this.gameObject.SetActive(true);
        parentDlg = null;
        currTipDlg = null;
        parentDlg = dlg != null ? dlg : null;
        var tipDlg = QueryDlg(type);
        currTipDlg = tipDlg;
        if (tipDlg != null)
        {
            tipDlg.OpenDlg();
            tipDlg.tInsterface = this;
            if (discrible != null)
                tipDlg.IlusterateText.text = discrible;
        }

        if (msg != null)
        {
            if (tipDlg != null)
                tipDlg.TipDlgMsg(msg);
        }
    }

    private IEnumerator WaitOpen(TipDlgType type, UiWoPropAbility dlg, string discrible = null, TipDlgMessage msg = null)
    {
        yield return new WaitForSeconds(0.5f);
        OpenTipDlg(type, dlg, discrible, msg);
    }

    public void CallExit()
    {
        this.gameObject.SetActive(false);
    }

    public void TipDlgOk()
    {
        if (parentDlg!= null)
        {
            Debug.Log("保存回调消息！");
            var isSuceess = currTipDlg.IsSuccess();
            parentDlg.TipDlgExitCallBanck(currTipDlg,currTipDlg.tipDlgType, isSuceess);

        }
        parentDlg = null;
        currTipDlg = null;
    }
    
    private TipDlg QueryDlg(TipDlgType type)
    {
        Debug.Log("查找窗口！");
        for (int i=0; i<tipDlgList.Count; i++)
        {
            if (tipDlgList[i].tipDlgType == type)
                return tipDlgList[i];
        }
        return null;     
    }
   
}
