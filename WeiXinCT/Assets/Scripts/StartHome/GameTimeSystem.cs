using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTimeSystem : MonoBehaviour
{
    public static GameTimeSystem instance;
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
    }
   // private  void

}
