using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class MapData
{
    public string mapId;        //地图ID
    public string mapName;
    public string resourceName;          //地图名称
    public string restaurentCount;      //包含餐厅数量
    public int startRestaurentID;       //餐厅开始ID(包含)
    public int endRestaurentID;             //餐厅结束ID（包含）

}

public class shopData

{
    public string shopID;               //id
    public string shopName;
    public string hallResources;        //大厅资源
    public int unLockKeyCount;           //解锁长度
    public string leveData;             //关卡数据
    public int startCount;              //开始长度
    public int endCount;                //结束长度          
    public string mapName;              //所在地图名称
    public string foodMenuName;         //菜单名称
    public int levelListCount;          //关卡配置表个数
    public string shopDescribleName;    //标题显示名称
    public int isDefaultLock = 0;       //是否默认解锁
}

public class LevelData
{
    public string levelID;

    public string shopName;

    public string levelGrade;

    public string[] mainFood = new string[3];    //主食1

    public string[] otherFood = new string[7];  //配菜资源

    public string[] device = new string[4];    //设备名称
    public string[] foodMakeDeviceName = new string[2];
    public string[] foodSubMitDeviceName = new string[2];
    public string drinkeDeviceName = string.Empty;

    public string drinke;       //饮料
    public string drinkDevice;     //饮料设备名称

    public string levelResources;     //大厅资源

    public string levelGuestTablle;  //顾客数据表
    public int chairCount;            //椅子长度

    public int unLoackKeyCunnt;            //解锁钥匙数目

    public int prizeCionNumber;           //奖品金币数
    public int prizeKeyNumber;              //奖品钥匙数

    public int cionTarget;          //金币目标
    public int compeletFoodNumber;  //完成出参数目目标
    public int goodTarget;          //好评目标
    public int continuTarget;       //连击目标
    
    public int gustCount;                   //顾客人数
    public int levelCompeletTime = 0;       //完成时间

    public int[] specialLimitType = new int [3];    //特殊限制
    //推荐烈性
    public int recommendUpType = 0;               //0 无  1 食物；2 设备
    public string recomenUpdName = string.Empty;         //推荐名名称
    public int toUpGrade = 0;                          //土建升级到等级
    public ConsumePropType recomendPropType;            //推荐道具升级
                                                        //
}


//public enum LevelCompeletTarget : int
//{
//    Null,
//    TargetCion
//}


/// <summary>
/// 单个玩家数据
/// </summary>
[Serializable]
public class LevelGuestData
{
    public string guestID;

    public string guestOrder;

    public float fromTime;

    public string[]  needFood = new string[3];

}


[Serializable]
public class GuestTable
{
   
    public string levelName;
    public List<LevelGuestData> guestDataList = new List<LevelGuestData>();
}



