using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public enum CupStateType : int
{
    Null,
    Emply,
    Fill
}

public class DeviceCup : MonoBehaviour
{
    public Image emplyImg;
    public Image fillImg;
    public CupStateType cupState;
    private Tween steakDTween;

    public void SetCupEmply()
    {
        emplyImg.gameObject.SetActive(true);
        fillImg.gameObject.SetActive(false);
        cupState = CupStateType.Emply;
    }
    public void SetCupFill()
    {
        emplyImg.gameObject.SetActive(false);
        fillImg.gameObject.SetActive(true);
        cupState = CupStateType.Fill;
    }
    public void OnClickEffcet()
    {
        if (!PlayerData.instance.currSingleLevelData.automaticSubmitProp)
        {

            if (steakDTween == null)
            {
                steakDTween = this.transform.DOScale(1.2f, 0.1f).SetLoops(2, LoopType.Yoyo);
            }
            else if (steakDTween != null && !steakDTween.active)
            {
                steakDTween.Kill();
                steakDTween = this.transform.DOScale(1.2f, 0.1f).SetLoops(2, LoopType.Yoyo);
            }
        }

    }
}
