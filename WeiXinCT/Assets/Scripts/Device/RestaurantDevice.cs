using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RestaurantDevice : MonoBehaviour
{
    [SerializeField]
    private Sprite[] deviceGradeImg;

    public Sprite[] DeviceGradeSkin { get => deviceGradeImg; }
}
