using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum DeveiceType : int
{
    DeveiceConvention,      //����
    DeveiceOne,
}
public enum DeveiceState : int  //����״̬
{
    Null,
    Wait,
    Makeing,
}

public class DeveiceComponent : MonoBehaviour
{
    [SerializeField]
    private string deveiceName;
    public string DeveiceName { get => deveiceName; set => deveiceName = value; }
    [SerializeField]
    private Image NullImg;

    private int deviceGradeUp;

    [SerializeField]
    protected DeveiceType deveiceType = DeveiceType.DeveiceConvention;

    private DeveiceState deviceState = DeveiceState.Null;

    public int DeveiceGradeUp { get => deviceGradeUp; set => deviceGradeUp = value; }

    public virtual void SetDeveiceInit()
    {
        if (NullImg == null)
            return;
        var nulllSkin = NullImg.GetComponent<CompomentSkin>().compnentSkin;
        NullImg.sprite = nulllSkin[deviceGradeUp - 1];
    }
    public void SetDeveiceState(DeveiceState state)
    {
        deviceState = state;
        if (deviceState == DeveiceState.Wait)
        {
            NullImg.gameObject.SetActive(true);
        }
        else if (deviceState == DeveiceState.Makeing)
        {
            NullImg.gameObject.SetActive(false);
        }
    }
}
