using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeveiceComponentTypeOne : DeveiceComponent
{
    [SerializeField]
    private Image[] guraddeUpImg;
    public override void SetDeveiceInit()
    {
       if (DeveiceGradeUp == 2)
        {
            guraddeUpImg[0].gameObject.SetActive(true);
            guraddeUpImg[1].gameObject.SetActive(false);
        }
       else if (DeveiceGradeUp == 3)
        {
            guraddeUpImg[0].gameObject.SetActive(true);
            guraddeUpImg[1].gameObject.SetActive(true);

        }
       
    }
}
