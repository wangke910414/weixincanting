using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedFoodComponent : MonoBehaviour
{
    [SerializeField]
    private string needFoodName;
    [SerializeField]
    private GameObject[] otherFood;
    [SerializeField]
    private ShapeChange shapeChange;

    private void Start()
    {
       // shapeChange = GetComponent<ShapeChange>();
    }
    public string NeedFoodName { get => needFoodName; }
    
    public void SetFoodShow(string foodName)
    {
        
        for (int i = 0; i < otherFood.Length; i++)
        {
            if (foodName == otherFood[i].name)
            {
                otherFood[i].gameObject.SetActive(true);
                if (shapeChange != null)
                    shapeChange.ChangeOperate();
            }
        }
    }
}
