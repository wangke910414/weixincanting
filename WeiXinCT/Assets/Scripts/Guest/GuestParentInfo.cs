using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuestParentInfo : MonoBehaviour
{
    public List<Transform> guestWartFoodPos = new List<Transform>();

    public Transform guestStartPos1;

    public Transform guestStartPos2;

    public List<bool> isHaveGuestLIst = new List<bool>();

    private int maxChairCount;

    public int MaxChairCount { get => maxChairCount; set => maxChairCount = value; }
    private void Start()
    {
      
        for (int i=0; i<guestWartFoodPos.Count; i++)
        {
            isHaveGuestLIst.Add(false);
        }
    }

    /// <summary>
    /// 判断全部移植有没有客人
    /// </summary>
    /// <returns></returns>
    public bool AllChairIsEmply()
    {
        for (int i=0; i< isHaveGuestLIst.Count; i++)
        {
              Debug.Log(i + "     " + isHaveGuestLIst[i]);
            if (isHaveGuestLIst[i])
            {
      
                return true;
            }
        
        }
        return false;
    }

    public void SetWartFoodPos(bool value)
    {
        for (int i = 0; i < guestWartFoodPos.Count; i++)
        {
            isHaveGuestLIst[i] = value;
        }
    }

    //获取等待位置是否有顾客
    public bool GetHaveGuest(int i)
    {
        if (i < isHaveGuestLIst.Count)
            return isHaveGuestLIst[i];
        return false;
    }
    //设置等待位置为空
    public void SetHaveGuest(int index,bool vlaue)
    {
        if (index < isHaveGuestLIst.Count && index >=0)
            isHaveGuestLIst[index] = vlaue;
        
    }
    public void ResetGuestParentInfo()
    {
        for (int i = 0; i < guestWartFoodPos.Count; i++)
        {
            isHaveGuestLIst[i] = false;
        }
    }
}
