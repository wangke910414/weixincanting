using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Audio;

public class PlateItem : MonoBehaviour
{
    [SerializeField]
    private AudioSource  submitAudio;
    public string ItemName;

    private string mainfood;

    private string[] otherFood = new string[4];

    private string foodID;

    public int plateIndex;

    private Tween clickTween;

    private FoodProperty foodItem;

    private float allPrice = 0f;

    private float mainFoodPrice = 0f;

    private int plateGrade = 0;

    private int clickCount = 0;

    private int otherFoodCount;
    public int PlateGrade { set => plateGrade = value; }
    public string FoodNmae { set => mainfood = value; get => mainfood; }

   /// private float continuFoodTime = 0f;

    public string[] OtherFoodName { get => otherFood; }

    public FoodProperty FoodItem { get => foodItem; set => foodItem = value; }

    //public float ContinuFoodTime { get => continuFoodTime; set => continuFoodTime = value; }

    public float MainFoodPrice { set => mainFoodPrice = value; }

    private void Start()
    {
        mainfood = "null";
        for (int i = 0; i < otherFood.Length; i++)
        {
            otherFood[i] = "null";
        }
    }

    
    public void SetFoodPrice(float price)
    {
        this.allPrice += price;
    }
    public string GetPlateFoodID()
    {
        foodID = mainfood + otherFood[0] + otherFood[1] + otherFood[2]+otherFood[3];
        return foodID;
    }

    public float GetPlateFoddIDNumber()
    {
        float idNum = 0;
        var mainFoodDct = LoadConfigursManager.instans.MainFoodItemDictionary;
        var otherFoodDct = LoadConfigursManager.instans.OetherFoodDictionary;

        if (mainfood != "null" && mainFoodDct.ContainsKey(mainfood))
        {
            idNum = float.Parse( mainFoodDct[mainfood].mianFoodID);
            var foodUpGrade = 1;
            if (PlayerData.foodDataPramerer.foodDataDict.ContainsKey(mainfood))
            {
                foodUpGrade = int.Parse(PlayerData.foodDataPramerer.foodDataDict[mainfood]);
            }
            allPrice += mainFoodDct[mainfood].mainFoodPrice + (foodUpGrade - 1);
        }

        foreach (string key in otherFood)
        {
            if (key != "null")
            {
                idNum += float.Parse(otherFoodDct[key].OtherFoodID);
                var foodUpGrade = 1;
                if (PlayerData.foodDataPramerer.foodDataDict.ContainsKey(key))
                {
                    foodUpGrade = int.Parse(PlayerData.foodDataPramerer.foodDataDict[key]);
                }
                allPrice += otherFoodDct[key].otherFoodPrice + (foodUpGrade - 1);
            }
        }
        return idNum;
    }

    public void ClearPlate()
    {
        mainfood = "null";
        for (int i = 0; i < otherFood.Length; i++)
        {
            otherFood[i] = "null";
        }
        foodItem.ClearFood();
        allPrice = 0f;
        otherFoodCount = 0;
    }

    public  bool  AddOtherFood(string food)
    { 
  
        var b = FoodItem.AddOtherFood(food);
       if (b)
        {
         otherFood[otherFoodCount] = food;
           otherFoodCount++;
           return true;
       }

        return false;
    }
    //交付
    public void DeliverFood()
    {
     ///   Debug.Log ("点击提交食物  " + mainfood);
        if (mainfood =="null")
        {
            return;
        }

        clickCount++;
        if (clickCount == 1)
        {
            StartCoroutine(ClickDisCard());
        }
        if (clickCount >= 2)
        {
            clickCount = 0;
            ClearPlate();
            foodItem.DisCardFood();
        }
        if (!submitAudio.isPlaying)
        {
            submitAudio.Play();
        }
       

        EventDeliverCookedFood   eventMsg = new EventDeliverCookedFood();
       eventMsg.foodID = GetPlateFoodID();
        eventMsg.foodIDNum = GetPlateFoddIDNumber();
        eventMsg.totalPrice = allPrice;
        eventMsg.plateIndex = plateIndex;
        SystemEventManager.TriggerEvent("GuestReceiveFood", eventMsg);
 
            if (clickTween == null)
            {
                clickTween = this.transform.DOScale(1.1f, 0.1f).SetLoops(2, LoopType.Yoyo);
            }
            else if (!clickTween.active)
            {

                clickTween = this.transform.DOScale(1.1f, 0.1f).SetLoops(2, LoopType.Yoyo);
            }
    }

    private void AutomaticOpertion()
    {

            if (mainfood == "null")
            {
                return;
            }

            EventDeliverCookedFood eventMsg = new EventDeliverCookedFood();
            eventMsg.foodID = GetPlateFoodID();
            eventMsg.foodIDNum = GetPlateFoddIDNumber();
            eventMsg.totalPrice = allPrice;
            eventMsg.plateIndex = plateIndex;
            SystemEventManager.TriggerEvent("GuestReceiveFood", eventMsg);
    }
    //自动提交食物
    public void AutomaticSubmitFood()
    {
        AutomaticOpertion();
    }

    private IEnumerator ClickDisCard()
    {
        yield return new WaitForSeconds(1f);
                clickCount = 0;             
    }

    public void SubmitSuccess(Transform target)
    {
        SuccessEffice(target);
        ClearPlate();
    }
    private void SuccessEffice(Transform target)
    {
        var effice = Instantiate(this.gameObject);
        effice.transform.transform.parent = this.transform.parent;
        effice.transform.position = this.transform.position;
        effice.transform.localScale = Vector3.one * 0.5f;
        effice.transform.DOMoveY(target.transform.position.y, 0.3f).SetEase(Ease.InOutSine);
        effice.transform.DOMoveX(target.transform.position.x, 0.5f).SetEase(Ease.InSine).onComplete = () =>
        {
            effice.gameObject.SetActive(false);
            Destroy(effice);
        };
    }

}
