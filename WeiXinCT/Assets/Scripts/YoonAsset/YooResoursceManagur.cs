using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using YooAsset;
//using UnityEditor.AddressableAssets.Build;

public class YooResoursceManagur
{

    static private YooResoursceManagur instace;
    private ResourcePackage package;
    private SystmConfigurationManager configur;

    private bool isLoadCompelet = false;
    private bool loadIngComoelet = false;
    static public YooResoursceManagur Instace { get => instace; }
    public bool LoadIngComoelet { get => loadIngComoelet; }

    public bool IsLoadCompelet { get => isLoadCompelet; }

    static public IEnumerator InitYoo()
    {
        if (instace == null)
        {
            instace = new YooResoursceManagur();
            YooAssets.Initialize();
            var initParameters = new WebPlayModeParameters();

            instace.configur = SystmConfigurationManager.instance;
            yield return instace.loadAsset();
        }
    }
    private IEnumerator loadAsset()
    {
        yield return InintCuble();
        yield return UpdatePackageVersion();
        yield return UpdatePackageManifest();
        yield return Download();
        loadIngComoelet = true;
    }
    public ResourcePackage GetPackage()
    {
        return package;
    }
    private IEnumerator InintCuble()
    {
       // package = YooAssets.CreatePackage("DefaultPackage");

        ///YooAssets.SetDefaultPackage(package);

        if (configur.PlayMode == EPlayMode.EditorSimulateMode)
        {
            package = YooAssets.CreatePackage("DefaultPackage");
            var initParameters = new EditorSimulateModeParameters();
            initParameters.SimulateManifestFilePath = EditorSimulateModeHelper.SimulateBuild("DefaultPackage");
            yield return package.InitializeAsync(initParameters);
            YooAssets.SetDefaultPackage(package);
            /// AssetOperationHandle handle = package.LoadAssetAsync<GameObject>("Assets/Prefabs/Prant/PlateParent.prefab");
            // yield return handle;
            // GameObject go = handle.InstantiateSync();
            // Debug.Log($"Prefab name is {go.name}");
        }
        if (configur.PlayMode == EPlayMode.WebPlayMode)
        {
            package = YooAssets.CreatePackage("DefaultPackage");
            /// string defaultHostServer = "http://127.0.0.1/CDN/WebGL/v1.0";
            // string fallbackHostServer = "http://127.0.0.1/CDN/WebGL/v1.0";

           //string defaultHostServer = "http://127.0.0.1:57630/Res/CDN/WebGL/v1.0";
           // string fallbackHostServer = "http://127.0.0.1:57630/Res/CDN/WebGL/v1.0";

             string defaultHostServer = "https://cloud1-4gbcohble01bbed3-1320641560.tcloudbaseapp.com/webgl/v1.0";
            string fallbackHostServer = "https://cloud1-4gbcohble01bbed3-1320641560.tcloudbaseapp.com/webgl/v1.0";


            var initParameters = new WebPlayModeParameters();
            initParameters.BuildinQueryServices = new GameQueryServices(); //太空战机DEMO的脚本类，详细见StreamingAssetsHelper
            initParameters.RemoteServices = new RemoteServices(defaultHostServer, fallbackHostServer);
            var initOperation = package.InitializeAsync(initParameters);
            yield return initOperation;

            if (initOperation.Status == EOperationStatus.Succeed)
            {
                Debug.Log("资源包初始化成功！");
            }
            else
            {
                Debug.LogError($"资源包初始化失败：{initOperation.Error}");
            }
        }
        if (configur.PlayMode == EPlayMode.OfflinePlayMode)
        {
            var initParameters = new OfflinePlayModeParameters();
            yield return package.InitializeAsync(initParameters);
        }
    }
    //获取资源版本
    private IEnumerator UpdatePackageVersion()
    {
        var package = YooAssets.GetPackage("DefaultPackage");
        var operation = package.UpdatePackageVersionAsync();
        yield return operation;

        if (operation.Status == EOperationStatus.Succeed)
        {
            //更新成功
            string packageVersion = operation.PackageVersion;
            Debug.Log($"Updated package Version : {packageVersion}");
        }
        else
        {
            //更新失败
            Debug.LogError(operation.Error);
        }
    }

    //更新资源清单
    private IEnumerator UpdatePackageManifest()
    {
        // 更新成功后自动保存版本号，作为下次初始化的版本。
        // 也可以通过operation.SavePackageVersion()方法保存。
        bool savePackageVersion = true;
        var package = YooAssets.GetPackage("DefaultPackage");
        var operation = package.UpdatePackageManifestAsync("v1.0", savePackageVersion);
        yield return operation;
        Debug.Log(Application.persistentDataPath);
        if (operation.Status == EOperationStatus.Succeed)
        {
            //更新成功
            Debug.Log("更新成功");
        }
        else
        {
            //更新失败
            Debug.LogError(operation.Error);
        }
    }
    //源包下载
    IEnumerator Download()
    {
        int downloadingMaxNum = 10;
        int failedTryAgain = 3;
        var package = YooAssets.GetPackage("DefaultPackage");
        var downloader = package.CreateResourceDownloader(downloadingMaxNum, failedTryAgain);



        //AssetOperationHandle handle = package.LoadAssetAsync<GameObject>("Assets/Prefabs/textImg.prefab");
        //yield return handle;
        //GameObject go = handle.InstantiateSync();
        //Debug.Log($"Prefab name is {go.name}");

        //没有需要下载的资源
        if (downloader.TotalDownloadCount == 0)
        {
            isLoadCompelet = true;
            Debug.Log("无资源下载！");
            yield break;
        }

        //需要下载的文件总数和总大小
        int totalDownloadCount = downloader.TotalDownloadCount;
        long totalDownloadBytes = downloader.TotalDownloadBytes;

        //注册回调方法
        downloader.OnDownloadErrorCallback = OnDownloadErrorFunction;
        downloader.OnDownloadProgressCallback = OnDownloadProgressUpdateFunction;
        downloader.OnDownloadOverCallback = OnDownloadOverFunction;
        downloader.OnStartDownloadFileCallback = OnStartDownloadFileFunction;

        //开启下载
        downloader.BeginDownload();
        yield return downloader;

        //检测下载结果
        if (downloader.Status == EOperationStatus.Succeed)
        {
            Debug.Log("下载成功");
            //下载成功
        }
        else
        {
            Debug.Log("下载失败");
            //下载失败
        }
    }
    private void OnDownloadErrorFunction(string fileName, string error)
    {
        Debug.Log("下载错误");
    }
    private void OnDownloadProgressUpdateFunction(int totalDownloadCount, int currentDownloadCount, long totalDownloadBytes, long currentDownloadBytes)
    {
        Debug.Log("进度" + currentDownloadCount);
    }
    private void OnDownloadOverFunction(bool isSucceed)
    {
        isLoadCompelet = true;
        Debug.Log("下载结束");
    }
    private void OnStartDownloadFileFunction(string fileName, long sizeBytes)
    {
        Debug.Log("下载文件"   + fileName);
    }

}

/// <summary>
/// 远端资源地址查询服务类
/// </summary>
public class RemoteServices : IRemoteServices
{
    private readonly string _defaultHostServer;
    private readonly string _fallbackHostServer;

    public RemoteServices(string defaultHostServer, string fallbackHostServer)
    {
        _defaultHostServer = defaultHostServer;
        _fallbackHostServer = fallbackHostServer;
    }
    string IRemoteServices.GetRemoteMainURL(string fileName)
    {
        return $"{_defaultHostServer}/{fileName}";
    }
    string IRemoteServices.GetRemoteFallbackURL(string fileName)
    {
        return $"{_fallbackHostServer}/{fileName}";
    }
}
