using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum RestaurentType : int
{
    FrenshFries,
    Humburg,
}
public class RestaurantInfo : MonoBehaviour
{
    public RestaurentType restaurentType;
    public RestaurentAudio restaurentAudio;
    public Transform PlateParent;

    public Transform FoodMakeParent;

    public Transform RawFoodParent;

    public LevelData levelData;

    public Transform guestPrant;

    public Transform otherFoodParent;

    public Transform drinkCup;        //饮料注入

    public Transform[] drinkPlate;  //饮料盘子节点

    // public Transform[] DeveiceFood;//   设备上的食物
    public Transform[] otherFood;       //配菜食物

    [SerializeField]
    private List<Transform> rawMainFoodList = new List<Transform>();
    public List<Transform> RawMainFoodList { get => rawMainFoodList; }

    SystemGameManager systemGameManager;
    [SerializeField]
    private DrinkOpreat drinkOpreat;

    public DrinkOpreat DrinkOpreat { get => drinkOpreat; }
    public void Configuer(SystemGameManager systemGameManager)
    {
        this.systemGameManager = systemGameManager;
        drinkOpreat.Configuer(systemGameManager);
    }

        //public string levelGrade;      //等级

        //public string levelID;

        //public string mainFood1;    //主食1

        //public string mainFood2;    //主食1

        //public string mainFood3;    //主食1

        //public string drinke;       //饮料

        //public string targetCoin;  //关卡目标

        //public string levelResources; //大厅资源

        //public string levelGuestTablle;  //玩家数据表
    }
