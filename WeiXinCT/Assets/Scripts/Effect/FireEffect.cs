using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using TMPro;

public class FireEffect : MonoBehaviour
{
    [SerializeField]
    private Transform fireEffectParent;

    [Header("Prefab")]
    [SerializeField]
    private GameObject fireEffectPrefab;

   [SerializeField]
   private int maxFire = 4;

    [Header("play fire effect time")]
    [SerializeField]
    private float maxPlayTime = 2.5f;

    [SerializeField]
    private GameObject ContuinueBtn;

    private float playedTime;

    private List<GameObject> effcetList = new List<GameObject>();

    [SerializeField]
    private GameObject successsImage;

    [SerializeField]
    private GameObject manImage;

    [SerializeField]
    private GameObject CoinImage;


    [SerializeField]
    private TextMeshProUGUI CoinText;


    private void Start()
    {
        playedTime = maxPlayTime;
        CoinText.text = PlayerData.instance.getCion.ToString();
            StartCoroutine(CreateFireEffect());
    }

    private IEnumerator CreateFireEffect()
    {
     
        while (playedTime>0)
        {

            playedTime -= Time.deltaTime * 100;
            Debug.Log("�̻�  " + playedTime);
            var rande = UnityEngine.Random.Range(0.5f, 1f);
            yield return new WaitForSeconds(rande);

           

            var fire = Instantiate(fireEffectPrefab);
            fire.transform.SetParent(fireEffectParent);
            var x = UnityEngine.Random.Range(-4f, 4.5f);
            var y = UnityEngine.Random.Range(-2f, 2f);
            var vet = new Vector3(x, y, 10f);
            fire.transform.localPosition = vet;
            fire.transform.localScale = new Vector3(3, 3, 3);
          fire.GetComponent<ParticleSystem>().Play();
          effcetList.Add(fire);
        }
        StartCoroutine( BehindEffcet());
    }

    
    private IEnumerator BehindEffcet()
    {
        yield return new WaitForSeconds(0.4f);
        successsImage.SetActive(true);
        ContuinueBtn.SetActive(true);
        CoinImage.SetActive(true);


        ContuinueBtn.transform.DOScale(1.5f, 0.5f).SetLoops(1, LoopType.Yoyo);
        successsImage.transform.DOScale(1.1f, 0.5f).SetLoops(1, LoopType.Yoyo);
        manImage.transform.DOLocalMoveX(-600f, 0.5f).SetEase(Ease.OutSine);

        successsImage.transform.DOScale(1f, 0.5f).SetEase(Ease.InSine).onComplete = ()=> {
            successsImage.transform.DOLocalMoveY(380f, 0.5f).SetEase(Ease.InOutSine);
        
        };
    }



}
