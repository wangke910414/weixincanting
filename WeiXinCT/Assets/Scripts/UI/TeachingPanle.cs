using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeachingPanle : MonoBehaviour
{

    [SerializeField]
    private List<TeachingCompnent> teachingList = new List<TeachingCompnent>();

    static private TeachingPanle instace;
    static public TeachingPanle Instace { get => instace; }

    public void Awake()
    {
        if (instace == null)
        {
            instace = this;
        }
    }
    public void Start()
    {
        Init();
    }
    private void Init()
    {
        for (int i=0; i< teachingList.Count; i++)
        {
            teachingList[i].TeachingPanle = this;
        }
    }

    public void OnClikcTeaching()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    ///���ý�е
    /// </summary>
    /// <param name="type"></param>
    public void TeachingOperation(TeachingType type)
    {
        this.gameObject.SetActive(true);
        for (int i=0; i< teachingList.Count; i++)
        {
            if (type == teachingList[i].TeachingType)
            {
                teachingList[i].gameObject.SetActive(true);
                return;
            }
        }
    }
}
