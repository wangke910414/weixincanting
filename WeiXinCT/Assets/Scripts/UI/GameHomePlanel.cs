using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class GameHomePlanel : UiWoPropAbility
{
    [SerializeField]
    private GameObject consumPropPrefab;
    [SerializeField]
    private TextMeshProUGUI cionTargetText;
    [SerializeField]
    private Slider cionSlider;
    [SerializeField]
    private Image ciconTargetImg;
    [SerializeField]
    private Image compeletFoodImg;
    [SerializeField]
    private Image goodCommendImg;
    [SerializeField]
    private Image limiTimeImge;
    [SerializeField]
    private Image limitGuestCountImage;
    [SerializeField]
    private TextMeshProUGUI limitationText;

    [SerializeField]
    private TextMeshProUGUI manText;

    [SerializeField]
    private Transform propParent;

    [SerializeField]
    private Image continuTimeImg;
    [SerializeField]
    private Transform specialLimitTransform;        //��������λ��
    [SerializeField]
    private Transform targetTransform;
    [SerializeField]
    private Transform limitTransform;

    [SerializeField]
    private Transform targetDisPlayConent;
    [SerializeField]
    private Transform limintDisPlayConent;

    [SerializeField]
    private float maxContinuTime = 5f;

    [SerializeField]
    private SpecialLimit[] specialLimtArray;

    private float CountContinuTime = 0;
    private float showTime = 0f;
    private int specialLimitCount = 0;

    public Transform SpecialLimitTransform { get => specialLimitTransform; }
    public Transform TargetTransform { get => targetTransform; }
    public Transform LimitTransform { get => limitTransform; }

    [SerializeField]
    private Image[] continuNumberImg;
    private List<LevelInfoPlanleProp> propList = new List<LevelInfoPlanleProp>();
    public override void InitWo()
    {
        CountContinuTime = maxContinuTime;
        ConsumePropConfigur oropConfigur = SystemDataFactoy.instans.GetResourceConfigur("ConsumePropConfigur") as ConsumePropConfigur;
        int index = 0;
        foreach (var iten in oropConfigur.ConsumeProp)
        {
            for (int i = 0; i < PlayerData.instance.currSelectPropList.Count; i++)
            {
                if (iten.comsumePropType == PlayerData.instance.currSelectPropList[i])
                {
                    var prop = Instantiate(consumPropPrefab);
                    prop.transform.SetParent(propParent);
                    prop.transform.localPosition = new Vector3(-index * 100, 0, 0);
                    var campnent = prop.GetComponent<LevelInfoPlanleProp>();
                    campnent.IconImg.sprite = iten.Icon;
                    campnent.ConsumePropType = PlayerData.instance.currSelectPropList[i];
                    campnent.SetGameHomeState();
                    propList.Add(campnent);
                    index++;
                }
            }
        }
        Limitation();
        SetTraget();
        StartCoroutine(WaitPlayPropAniamtion());
        base.InitWo();
    }

    private IEnumerator WaitPlayPropAniamtion()
    {
        yield return new WaitForSeconds(0.5f);
        foreach (var item in propList)
        {
            item.transform.DOScale(new Vector3(1.3f,1.3f, 0),0.3f).SetEase(Ease.Flash).SetLoops(2,LoopType.Yoyo);
        }
    }

    public override void OpenUiWo()
    {
       this.gameObject.SetActive(true);
        limintDisPlayConent.gameObject.SetActive(false);
        targetDisPlayConent.gameObject.SetActive(false);
       

        manText.text = SystemGameManager.mGuestManager.GetCurrLevelGuestCount().ToString(); 
        // base.OpenUiWo();
    }
    public override void CloseUiWo()
    {
        this.gameObject.SetActive(false);
     
        base.CloseUiWo();
    }

    public void SetTraget()
    {
        if (PlayerData.instance.targetType == TargetType.CIonTraget)
        {
            compeletFoodImg.gameObject.SetActive(false);
            ciconTargetImg.gameObject.SetActive(true);
            goodCommendImg.gameObject.SetActive(false);
            var temp1 = PlayerData.instance.currSingleLevelData.cion;
            Debug.Log("tem1 " + temp1);
            var temp2 = PlayerData.instance.targerCount;
            cionTargetText.text = temp1.ToString() + "/" + temp2.ToString();
            cionSlider.value = (float)temp1 / (float)temp2;
        }
        else if (PlayerData.instance.targetType == TargetType.CompeletFood)
        {
            compeletFoodImg.gameObject.SetActive(true);
            ciconTargetImg.gameObject.SetActive(false);
            goodCommendImg.gameObject.SetActive(false);

            var temp1 = PlayerData.instance.currSingleLevelData.compeleFoodNumber;
            Debug.Log("tem1 " + temp1);
            var temp2 = PlayerData.instance.targerCount;
            cionTargetText.text = temp1.ToString() + "/" + temp2.ToString();
            cionSlider.value = (float)temp1 / (float)temp2;
        }
        else if (PlayerData.instance.targetType == TargetType.GoodCommend)
        {
            compeletFoodImg.gameObject.SetActive(false);
            ciconTargetImg.gameObject.SetActive(false);
            goodCommendImg.gameObject.SetActive(true);
            var temp1 = PlayerData.instance.currSingleLevelData.goodCommendNumber;
            Debug.Log("tem1 " + temp1);
            var temp2 = PlayerData.instance.targerCount;
            cionTargetText.text = temp1.ToString() + "/" + temp2.ToString();
            cionSlider.value = (float)temp1 / (float)temp2;
        }
    }
    public void Limitation()
    {
        if (PlayerData.instance.limitType == LimitType.GustCountLimit)
        {
            limitGuestCountImage.gameObject.SetActive(true);
            limiTimeImge.gameObject.SetActive(false);
        }
        else if (PlayerData.instance.limitType == LimitType.TimeLimit)
        {
            showTime = PlayerData.instance.limitCount;
            limitGuestCountImage.gameObject.SetActive(false);
            limiTimeImge.gameObject.SetActive(true);
        }
    }
    private void UpdateUI()
    {
        if (PlayerData.instance.limitType == LimitType.GustCountLimit)
        {
            var temp = PlayerData.instance.limitCount - PlayerData.instance.currSingleLevelData.comeGuestCount;
            temp = temp > 0 ? temp : 0; 
            limitationText.text = temp.ToString();
        }
        else if (PlayerData.instance.limitType == LimitType.TimeLimit)
        {
            if (showTime <= 0)
                return;

            showTime -= Time.deltaTime;
            var min = (int)showTime / 60;
            var s = (int)showTime % 60;
            var strm = string.Empty;
            var strs = string.Empty;
            if (min < 10)
                strm = "0" + min.ToString();
            else
                strm = min.ToString();
            if (s<10)
                strs = "0" + s.ToString();
            else
                strs = s.ToString();

            limitationText.text = strm + ":" + strs;
        }
 
    }
    
    private void Update()
    {
        UpdateUI();
    }
    public void OnClickStop()
    {
        SystemGameManager.UiManager.OpenStopDlg();
        
        
    }
    public void SetCoutinuValue(float vvalue)
    {
        continuTimeImg.fillAmount = vvalue;
    }
    public void setContinuNumerImg(float number)
    {
        if (number < 2)
        {
            for (int i = 0; i < continuNumberImg.Length; i++)
            {

                continuNumberImg[i].gameObject.SetActive(false);
            }
            return;
        }

       for (int i=0; i<continuNumberImg.Length; i++)
        {
            if (i == number-2)
                continuNumberImg[i].gameObject.SetActive(true);
            else
                continuNumberImg[i].gameObject.SetActive(false);

        }
    }

    public void UpdatePropShow()
    {
        if (PlayerData.instance.currSelectPropList.Count == propList.Count)
        {
            ShowUpdate();
            return;
        }


        int index = propList.Count;
        ConsumePropConfigur oropConfigur = SystemDataFactoy.instans.GetResourceConfigur("ConsumePropConfigur") as ConsumePropConfigur;

        for (int i=0; i<PlayerData.instance.currSelectPropList.Count; i++)
        {
            if (PlayerData.instance.currSelectPropList[i] == ConsumePropType.AutomaticSubmitFood)
                PlayerData.instance.currSingleLevelData.automaticSubmitProp = true;

            var isPropFind = false;
            for (int j=0; j< propList.Count; j++)
            {
                if (PlayerData.instance.currSelectPropList[i] == propList[j].ConsumePropType)
                {
                    isPropFind = true;
                    propList[j].SetGameHomeState();
                    break;
                } 
            }
            if (isPropFind)
            {
                continue;
            }

            var prop = Instantiate(consumPropPrefab);
            prop.transform.SetParent(propParent);
            prop.transform.localPosition = new Vector3(-index * 100, 0, 0);
            var propConfigue = oropConfigur.GetConsumeProp(PlayerData.instance.currSelectPropList[i]);
            var campnent = prop.GetComponent<LevelInfoPlanleProp>();
            campnent.IconImg.sprite = propConfigue.Icon;
            campnent.PropUseType = propConfigue.useType;
            campnent.ConsumePropType = PlayerData.instance.currSelectPropList[i];
            campnent.SetGameHomeState();
            campnent.transform.DOScale(new Vector3(1.5f, 1.5f, 0), 0.5f).SetEase(Ease.Flash).SetLoops(2, LoopType.Yoyo);
            propList.Add(campnent);
            index++;

        }
    }
    private void ShowUpdate()
    {
        for (int j = 0; j < propList.Count; j++)
        {
            if ( propList[j].PropUseType == UseType.moment)
            {
                propList[j].SetGameHomeState();
                break;
            }
        }
    }

    public void SpecialLimitShow()
    {
        var currLevel = SystemGameManager.LevelManager.GetCurrLevelData();
        var specialLimitConfigur = SystemDataFactoy.instans.GetResourceConfigur("SpecialLimitCongigur") as SpecialLimitCongigur;
        if (currLevel == null || specialLimitConfigur == null)
            return;

        for (int i = 0; i < currLevel.specialLimitType.Length; i++)
        {
            specialLimtArray[i].gameObject.SetActive(false);
            if (currLevel.specialLimitType[i] != 0)
            {
                var temp = (SpecialLimitType)currLevel.specialLimitType[i];
                var item = specialLimitConfigur.GetSpecialLimit(temp);
                if (item != null)
                {
                    specialLimtArray[specialLimitCount].gameObject.SetActive(true);
                    specialLimtArray[specialLimitCount].GetComponent<Image>().sprite = item.GetComponent<Image>().sprite;
                    specialLimtArray[specialLimitCount].GetComponent<Image>().SetNativeSize();
                    specialLimitCount++;
                }

            }
        }
        if (specialLimitCount == 1)
        {
            specialLimtArray[0].transform.localPosition = Vector3.zero;
        }

    }
    public void TargetShow()
    {
        limintDisPlayConent.gameObject.SetActive(true);
    }
    public void LimitShow()
    {
        targetDisPlayConent.gameObject.SetActive(true);
    }
    
}
