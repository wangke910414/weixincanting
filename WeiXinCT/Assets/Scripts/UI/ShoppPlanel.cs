using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using LitJson;

public class ShoppPlanel : UiWoPropAbility
{
    private HomeMapUiManager uiManager;

    [SerializeField]
    private GameObject shopItemPrefab;
    [SerializeField]
    private Button[] buttonArray;
    [SerializeField]
    private Image slideImg;
    [SerializeField]
    private GameObject[] conentParent;

    [SerializeField]
    private GameObject[] scrollView;

    private ConsumePropConfigur shopItemConfigur;
    [SerializeField]
    private List<ShopItemCell> itemPropList = new List<ShopItemCell>();
    private int butonIndex = 0;

    [SerializeField]
    private TextMeshProUGUI gemNumberText;

    private List<ConsumePropType> casuminessPuyPropList = new List<ConsumePropType>();
    [SerializeField]
    private List<Button> casuminessButPuyList = new List<Button>();

    [SerializeField]
    private Material greyMaterial;

    [SerializeField]
    private List<ShopBuyDiamond> shopBytList = new List<ShopBuyDiamond>();
    [SerializeField]
    private Transform flightTerget;

    [SerializeField]
    private Transform obtainEfficeContent;

    public bool isFastOpen = false;    //是否快速打开
    public bool dailyfastOpen = false;   //每日快速打开
    private Color textColor;
    private Color textSelectColor;      //选中

    private int itemIdenx = -1;
    public int ShowIndex { set => butonIndex = value; }

    public void Init(HomeMapUiManager uiManager)
    {
        this.uiManager = uiManager;

    }

    public override void InitWo()
    {
        InteShop();
        CreateShopPropItem();
        gemNumberText.text = PlayerData.playerDataParame.diamond.ToString();
       textColor =  casuminessButPuyList[0].GetComponentInChildren<TextMeshProUGUI>().color;
        textSelectColor = gemNumberText.color;

        base.InitWo();

    }


    public void OnClickShowBut(int index)
    {
        for (int i = 0; i < buttonArray.Length; i++)
        {
            if (i != index)
            {
                buttonArray[i].interactable = true;
                scrollView[i].SetActive(false);
            }
            else
            {
                buttonArray[i].interactable = false;
                scrollView[i].SetActive(true);

            }
        }
        if (index == 0)
        {
            RestItemProp();
            RestGemItem();
            StartCoroutine(ShowItemProp());
            slideImg.transform.SetParent(buttonArray[0].transform);
            slideImg.transform.DOLocalMoveX(0, 0.15f).SetEase(Ease.InSine);
            slideImg.transform.SetSiblingIndex(0);
           
        }
        else if (index == 1)
        {

            RestGemItem();
            RestItemProp();
            StartCoroutine(ShowItemGem());
            slideImg.transform.SetParent(buttonArray[1].transform);
            slideImg.transform.DOLocalMoveX(0, 0.15f).SetEase(Ease.InSine);
            slideImg.transform.SetSiblingIndex(0);
            var lastPin = 0;
            for (int i = 0; i < 2; i++)
            {
                var pin = Random.Range(1, 9);
               while (lastPin == pin)
                {
                  pin = Random.Range(1, 9);
                }
                CasuminessPuyuProp(pin);
                lastPin = pin;
            }

        }

    }


    private void InteShop()
    {
        for (int i = 0; i < buttonArray.Length; i++)
        {
            if (i != butonIndex)
            {
                buttonArray[i].interactable = true;
                scrollView[i].SetActive(false);
            }
            else
            {
                buttonArray[i].interactable = false;
                scrollView[i].SetActive(true);

            }
        }
        shopItemConfigur = SystemDataFactoy.instans.GetResourceConfigur("ConsumePropConfigur") as ConsumePropConfigur;
        //RestGemItem();

        for (int i = 0; i < casuminessButPuyList.Count; i++)
        {
            casuminessButPuyList[i].GetComponent<Image>().material = greyMaterial;
            casuminessButPuyList[i].GetComponent<SelectBtnComponent>().IsSelect = false;
        }
    }

    public override void OpenUiWo()
    {
        RestGemItem();
        RestItemProp();
        InteShop();
        base.OpenUiWo();

    }


    public void OnClickClose()
    {
        RestItemProp();
        butonIndex = 0;
        CloseUiWo();
        if (isFastOpen == true)
        {
            isFastOpen = false;
            uiManager.FastOpenCloseDlg();
        }
        if (dailyfastOpen == true)
        {
            uiManager.DailyFastOpen();
            dailyfastOpen = false;
        }
    }
    public override void OpenEnd()
    {
        if (this.gameObject.activeSelf)
        {
            StartCoroutine(ShowItemProp());

        }
        for (int i = 0; i < itemPropList.Count; i++)
        {
            bool isLock = PlayerData.instance.GetUnLockLevel() >= itemPropList[i].LockCount ? true : false;
            Debug.Log("isLock    " + i + "   " + isLock);
            itemPropList[i].SetUnLockStart(isLock);
        }
    }

    public void CreateShopPropItem()
    {

        var shopItemProp = shopItemConfigur.ConsumeProp;
        var shopData = SystemDataFactoy.instans.GetResourceConfigur("ShoppItemConfigur") as ShoppItemConfigur;
        for (int i = 0; i < shopItemProp.Length; i++)
        {
            itemPropList[i].gameObject.SetActive(false);
            var compent = itemPropList[i].GetComponent<ShopItemCell>();
            compent.propImage.sprite = shopItemProp[i].Icon;
            compent.propType = shopItemProp[i].comsumePropType;
            compent.buyNumber = shopData.shopItemPropList[i].buyNumber;
            //Debug.Log("道具商城    " + i + "   " + shopItemProp[i].unLosckLevel   + " level      "ckLevel());
            bool isLock =PlayerData.instance.GetUnLockLevel() >= shopItemProp[i].unLosckLevel ? true : false;
            Debug.Log("isLock    " + i + "   " +  isLock);
            compent.SetUnLockStart(isLock);
            compent.LockCount = shopItemProp[i].unLosckLevel;
            compent.ShoppPlanel = this;
            compent.cellIndex = i;
            compent.DescribeText.text = shopData.shopItemPropList[i].describe;
            compent.priceText.text = shopData.shopItemPropList[i].price.ToString();
        }

        for (int i = 0; i < shopBytList.Count; i++)
        {
            var component = shopBytList[i];
            component.ShopPanel = this;
            component.ResetItme();
        }
    }

    private void RestItemProp()
    {
        for (int i = 0; i < itemPropList.Count; i++)
        {
            itemPropList[i].transform.localScale = Vector3.one;
            itemPropList[i].gameObject.SetActive(false);
        }
    }

    private IEnumerator ShowItemProp()
    {
        yield return new WaitForSeconds(0.35f);
        for (int i = 0; i < itemPropList.Count; i++)
        {
            itemPropList[i].gameObject.SetActive(true);
            itemPropList[i].transform.DOShakeScale(0.2f, 0.2f, 1).SetEase(Ease.Flash); ;
            yield return new WaitForSeconds(0.06f);
        }
    }

    private void RestGemItem()
    {

        for (int i = 0; i < shopBytList.Count; i++)
        {

            shopBytList[i].gameObject.SetActive(false);
            shopBytList[i].GetComponent<Transform>().localScale = Vector3.one * 0.9f;
            shopBytList[i].ResetItme();
        }
    }
    private IEnumerator ShowItemGem()
    {
        yield return new WaitForSeconds(0.35f);
        for (int i = 0; i < shopBytList.Count; i++)
        {
            shopBytList[i].gameObject.SetActive(true);
            shopBytList[i].transform.DOShakeScale(0.2f,0.2f, 1).SetEase(Ease.Flash);
           // shopBytList[i].transform.DOScale(1f, 0.1f).SetEase(Ease.InFlash);
            yield return new WaitForSeconds(0.06f);
        }
    }

    public void CallBuyPropFail()
    {
        string str = "当前钻石数量不足，请购买！";
        uiManager.TipDlgManager.OpenTipDlg(TipDlgType.SamllTipDlg, this, str);
    }

    public void CallPropBut(int index)
    {
        string msg = "确定使用钻石！";
        TipDiamondComsume buff = new TipDiamondComsume();
        buff.tipDlgType = TipDlgType.DimaondCofirmTip;
        buff.isSuccesss = true;
        itemIdenx = index;
        uiManager.TipDlgManager.OpenTipDlg(TipDlgType.DimaondCofirmTip, this, msg,buff);
    }

    public override void TipDlgExitCallBanck(TipDlg tipDlg,TipDlgType type, bool success)
    {
        if (!gameObject.activeSelf)
            return;
        if (tipDlg == null)
            return;
        if (type == TipDlgType.DimaondCofirmTip)
        {
            if (itemIdenx != -1 && success)
            {
                Debug.Log("确信购买");
                buyPropOPerate(itemIdenx);
                itemIdenx = -1;
            }
        }
        else if (type == TipDlgType.ObttainTip)
        {
            Debug.Log("获得弹窗返回");
            ObtainTip dlg = tipDlg as ObtainTip;
            if (dlg == null)
            {
                Debug.LogError("Obtaion Dlg Null!");
            }
            var count = int.Parse(dlg.PrizeCountText.text);
            count = count < 5 ? 3 : 6;
            PlayObtainAnimation(dlg.PrizeImg.transform, count);
        }

        base.TipDlgExitCallBanck(tipDlg,type, success);
    }


    /// <summary>
    /// 购买此操作
    /// </summary>
    private void buyPropOPerate(int index )
    {

       StartCoroutine ( itemPropList[index].PlayAnimation());
        var propName = itemPropList[index].propType.ToString();
         var price =  int .Parse(itemPropList[index].priceText.text);
        PlayerData.instance.PlayerDataValueDiamondOprate(-price);

        if (PlayerData.consumePropDataParameter.consumePropDataDict.ContainsKey(propName))
        {
            var tempCount = int.Parse(PlayerData.consumePropDataParameter.consumePropDataDict[propName]) + itemPropList[index].buyNumber;
            PlayerData.consumePropDataParameter.consumePropDataDict[propName] = tempCount.ToString();
        }
        else
        {
            PlayerData.consumePropDataParameter.consumePropDataDict.Add(propName, itemPropList[index].buyNumber.ToString());
        }

#if UNITY_EDITOR
        var daata = JsonMapper.ToJson(PlayerData.consumePropDataParameter);
        Debug.Log("保存道具数据    " + daata);
        CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("consumeProp", daata));
#elif !UNITY_EDITOR && UNITY_WEBGL
        var daata = JsonMapper.ToJson(PlayerData.consumePropDataParameter);
        Debug.Log("保存道具数据    " + daata);
        CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("UpdateConsumeProp", daata));
#endif
        gemNumberText.text = PlayerData.playerDataParame.diamond.ToString();
    }

    public void CasuminessPuyDiamond()
    {

    }

    public void CasuminessPuyuProp(int index)
    {
        var b = casuminessButPuyList[index - 1].GetComponent<SelectBtnComponent>().IsSelect;
        if (b)
        {
            casuminessButPuyList[index - 1].GetComponent<Image>().material = greyMaterial;
            casuminessButPuyList[index - 1].transform.GetComponent<Transform>().localScale = Vector3.one;
            casuminessButPuyList[index - 1].GetComponentInChildren<TextMeshProUGUI>().color = textColor;
            casuminessButPuyList[index - 1].GetComponent<SelectBtnComponent>().IsSelect = false;
            var consumeProp = (ConsumePropType)index;
            ClearSelectProp(consumeProp);
        }
        else
        {
            var selectCont = 0;
            foreach (var item in casuminessButPuyList)
            {
                var select = item.GetComponent<SelectBtnComponent>().IsSelect;
                if (select)
                    selectCont++;
            }

            if (selectCont >= 2)
                return;

            casuminessButPuyList[index - 1].GetComponent<Image>().material = null;
            casuminessButPuyList[index - 1].transform.GetComponent<Transform>().localScale = Vector3.one * 1.1f;
            casuminessButPuyList[index - 1].GetComponentInChildren<TextMeshProUGUI>().color = textSelectColor;
            casuminessButPuyList[index - 1].GetComponent<SelectBtnComponent>().IsSelect = true;
            var consumeProp = (ConsumePropType)index;
            casuminessPuyPropList.Add(consumeProp);
        }
    }
    private void ClearSelectProp(ConsumePropType type)
    {
        for (int i = 0; i < casuminessPuyPropList.Count; i++)
        {
            if (casuminessPuyPropList[i] == type)
                casuminessPuyPropList.Remove(casuminessPuyPropList[i]);
        }
    }

    //任选确定
    public void CasumiPuyDiamoneClick()
    {
        // Debug.Log("点击任选按钮");
        DiamondStoreTipEvent ent = new DiamondStoreTipEvent();
        ent.style = ObatainTipShowType.DiamondStoreOne;
        ent.seletPropList = casuminessPuyPropList;
        ent.giftCount = 200;
        uiManager.TipDlgManager.OpenTipDlg(TipDlgType.ObttainTip, this,null, ent);

        //---------后期接口数据操作--------------
        bool isSuccess = true;
        if (isSuccess)
        {
            PlayerData.instance.PlayerDataValueDiamondOprate(ent.giftCount);
            PropSaveOpertae(ObatainTipShowType.DiamondStoreOne);
            gemNumberText.text = PlayerData.playerDataParame.diamond.ToString();
        }

    }
    //大礼包
    public void GiftPacksClick()
    {
        DiamondStoreTipEvent ent = new DiamondStoreTipEvent();
        ent.style = ObatainTipShowType.DiamondSroreTwo;
        ent.giftCount = 300;
        uiManager.TipDlgManager.OpenTipDlg(TipDlgType.ObttainTip, this, null, ent);


        //---------后期接口数据操作--------------
        bool isSuccess = true;
        if (isSuccess)
        {
            PlayerData.instance.PlayerDataValueDiamondOprate(ent.giftCount);
            PropSaveOpertae(ObatainTipShowType.DiamondSroreTwo);
            gemNumberText.text = PlayerData.playerDataParame.diamond.ToString();
        }
    }

    public void PropIntroduce(bool state,int grade)
    {
        if(state)
        {
            uiManager.TipDlgManager.OpenTipDlg(TipDlgType.PropIllustrate, this);
        }
        else
        {
            string str = "该道具将在过关后1-"+ grade + "解锁";
            uiManager.TipDlgManager.OpenTipDlg(TipDlgType.SamllTipDlg, this, str);
        }
    }
   

    //广告.增加获得 控件点击消息
    public void SmallBtnOnClick(int diamondCount = 0, int moneyCount = 0)
    {
    
        DiamondStoreTipEvent ent = new DiamondStoreTipEvent();
        ent.style = ObatainTipShowType.DiamondStoreThree;
        ent.seletPropList = casuminessPuyPropList;
        ent.giftCount = diamondCount;
        uiManager.TipDlgManager.OpenTipDlg(TipDlgType.ObttainTip, this, null, ent);


        //---------后期接口数据操作--------------
        bool isSuccess = true;
        if (isSuccess)
        {
            PlayerData.instance.PlayerDataValueDiamondOprate(diamondCount);
            gemNumberText.text = PlayerData.playerDataParame.diamond.ToString();
        }
    
    }

    //public override void TipDlgExitCallBanck(TipDlgType type, bool success)
    //{

    //}


    //public void PlayerDataValueDiamondOprate(int number)
    //{
    //    if (number < 0)
    //    {
    //        var strName = PlayerData.instance.dailyTaskDataValue.consumeDiamond;
    //        PlayerData.instance.dailyTaksValueDit[strName] += Mathf.Abs( number);
    //        PlayerData.instance.achieveParmeter.comsumeDiamondCount += Mathf.Abs(number);
    //    }

    //    else if (number > 0)
    //    {
    //        var strName = PlayerData.instance.dailyTaskDataValue.obtainDiamond;
    //        PlayerData.instance.dailyTaksValueDit[strName] += number;
    //        PlayerData.instance.achieveParmeter.obtanDiamondCount += number;
    //    }

    //    PlayerData.playerDataParame.diamond += number;
    //    PlayerData.instance.PlayerDataParameSave();
    //    PlayerData.instance.SaveDayliTaskValueData();
    //    PlayerData.instance.SaveAchieveTaskValueData();

    //}

    //活到道具操作
    private void PropSaveOpertae(ObatainTipShowType type)
    {
        if (type == ObatainTipShowType.DiamondStoreOne)
        {
            for (int i = 0; i < casuminessPuyPropList.Count; i++)
            {
                var propName = casuminessPuyPropList[i].ToString();
                if (PlayerData.consumePropDataParameter.consumePropDataDict.ContainsKey(propName))
                {
                    var tempCount = int.Parse(PlayerData.consumePropDataParameter.consumePropDataDict[propName]) + 6;
                    PlayerData.consumePropDataParameter.consumePropDataDict[propName] = tempCount.ToString();
                }
                else
                {
                    PlayerData.consumePropDataParameter.consumePropDataDict.Add(propName, 6.ToString());
                }
            }

        }
        else if (type == ObatainTipShowType.DiamondSroreTwo)
        {
            ConsumePropConfigur consumePropConfigur = SystemDataFactoy.instans.GetResourceConfigur("ConsumePropConfigur") as ConsumePropConfigur;
            for (int i=0; i< shopItemConfigur.ConsumeProp.Length; i++)
            {
                var propName = shopItemConfigur.ConsumeProp[i].comsumePropType.ToString();
                if (PlayerData.consumePropDataParameter.consumePropDataDict.ContainsKey(propName))
                {
                    var tempCount = int.Parse(PlayerData.consumePropDataParameter.consumePropDataDict[propName]) + 6;
                    PlayerData.consumePropDataParameter.consumePropDataDict[propName] = tempCount.ToString();
                }
                else
                {
                    PlayerData.consumePropDataParameter.consumePropDataDict.Add(propName, 6.ToString());
                }
            }

        }


#if UNITY_EDITOR
        var daata = JsonMapper.ToJson(PlayerData.consumePropDataParameter);
        Debug.Log("保存道具数据    " + daata);
        CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("consumeProp", daata));
#elif !UNITY_EDITOR && UNITY_WEBGL
        var daata = JsonMapper.ToJson(PlayerData.consumePropDataParameter);
        Debug.Log("保存道具数据    " + daata);
        CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("UpdateConsumeProp", daata));
#endif 
    }
    private void PlayObtainAnimation(Transform prize, int count)
    {
        if (prize == null)
            return;
        if (obtainEfficeContent == null)
            return;
       /// Debug.Log("播放动画！");
        obtainEfficeContent.transform.localScale = Vector3.one;
        obtainEfficeContent.transform.position = prize.transform.position;

        var compeonent = obtainEfficeContent.transform.GetComponent<GhostFlightAnimation>();
        if (compeonent != null)
        {
            compeonent.PlayFlightAnimation(prize.gameObject,flightTerget, count);
        }
    }
}
