using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//面板父类
public class UiWoPropAbility : MonoBehaviour
{
   private  SystemGameManager systemGameManager;

    [SerializeField]
    private CanvasGroup targetGroup = null;

    [SerializeField]
    private CanvasGroup canvasHomeGroup = null;

    [SerializeField]
    private float changeTime = 1.5f;

    public CanvasGroup TargetGroup { get => targetGroup; set => targetGroup = value; }

    public float ChangeTime { get => changeTime; set => changeTime = value; }

    //public CanvasGroup CanvasHomeGroup { get => canvasHomeGroup; set => canvasHomeGroup = value;  }

    public SystemGameManager SystemGameManager { get => systemGameManager; set => systemGameManager = value; }

    public void Configur(SystemGameManager systemGameManager)
    {
        this.SystemGameManager = systemGameManager;      
    }

    private void Start()
    {
        InitWo();
    }
      public  virtual void OpenUiWo()
    {
        SetUiEffect(false);
        
    }

    public virtual void CloseUiWo()
    {
        SetUiEffect(true); 
    }

    //UI 窗口打开动画 
    private void SetUiEffect(bool value)
    {
        if (!value)
        {
           canvasHomeGroup.DOKill();

            targetGroup.gameObject.SetActive(true);
            canvasHomeGroup.DOFade(0f, changeTime).SetEase(Ease.OutSine);
                 canvasHomeGroup.transform.DOScale(1.15f, changeTime).SetEase(Ease.InBack);

            canvasHomeGroup.transform.DOScale(1.15f, changeTime).SetEase(Ease.InBack).OnComplete(() =>
             {
                 canvasHomeGroup.gameObject.SetActive(false);
                 if (targetGroup != null)
                 {
                     targetGroup.gameObject.SetActive(true);
                     targetGroup.DOKill();
                     targetGroup.DOFade(1f, changeTime).SetEase(Ease.OutCubic);
                     targetGroup.transform.DOScale(1f, changeTime).SetEase(Ease.OutCubic);
                     OpenEnd();

                 }
             });

        }
        else
        {

          targetGroup.DOKill();
            targetGroup.DOFade(0f, changeTime).SetEase(Ease.OutCubic);
            targetGroup.transform.DOScale(0.95f, changeTime).SetEase(Ease.OutCubic).OnComplete(() =>
             {
                 targetGroup.gameObject.SetActive(false);

                 canvasHomeGroup.gameObject.SetActive(true);
                 canvasHomeGroup.DOKill();
                 canvasHomeGroup.DOFade(1f, changeTime).SetEase(Ease.OutCubic);
                 canvasHomeGroup.transform.DOScale(1f, changeTime).SetEase(Ease.InBack);
                 CloseEnd();
             });
        }    
    }
    public virtual void InitWo()
    {

    }
    //打开完成
   public virtual void OpenEnd()
    {

    }
    //关闭完成
    public virtual void CloseEnd()
    {

    }

    //子弹窗退出回调
    public virtual void TipDlgExitCallBanck(TipDlg tipDlg, TipDlgType type, bool success)
    {

    }
}
