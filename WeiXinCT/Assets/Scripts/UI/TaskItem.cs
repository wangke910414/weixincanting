using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TaskItem : MonoBehaviour
{
    public TextMeshProUGUI taskName;
    public TextMeshProUGUI taskDescribe;
    public TextMeshProUGUI taskPrizeCountText;
    public TextMeshProUGUI progessText;
  
    [SerializeField]
    private Image bgImg;
    [SerializeField]
    private Sprite[] bgShowSkin;
    [SerializeField]
    private Image progessValueImg;


    [HideInInspector]
    public int tagetCount;
    [HideInInspector]
    public int currCount;
    private AchieveTaskBase achieveTask;
    private AchievePanel ahievePanle;
    public AchieveTaskBase AchieveTask { get => achieveTask; set => achieveTask = value; }
    public AchievePanel AhievePanle { set => ahievePanle = value; }
    public void UpdateShowUI()
    {
       var isCompelet =  CeckIsCompelet();
        if (isCompelet)
        {
            bgImg.sprite = bgShowSkin[1];
        }
        else
        {
            bgImg.sprite = bgShowSkin[0];
        }

        currCount = achieveTask.currCount > achieveTask.tagerCount ? achieveTask.tagerCount : achieveTask.currCount;
        

        progessValueImg.fillAmount = (float)currCount / (float)tagetCount;
        progessText.text = currCount.ToString() + "/" +  tagetCount.ToString();

    }

    public float GetValue()
    {
        return (float)currCount / (float)tagetCount;
    }

    private bool CeckIsCompelet()
    {
        if (achieveTask == null)
            return false;
        return achieveTask.OperateHandle();
    }
    public void ClickAchieve()
    {
        if (CeckIsCompelet())
        {
            currCount = 0;
            bgImg.sprite = bgShowSkin[0];
             int temp = int.Parse(taskPrizeCountText.text);
            PlayerData.playerDataParame.diamond += temp;
            PlayerData.instance.achieveParmeter.obtanDiamondCount += temp;

            achieveTask.currCount = 0;
            achieveTask.SetCurrValue();
            PlayerData.instance.achieveParmeter.tatolCount++;
            //Debug.Log("�����   " + PlayerData.instance.achieveParmeter.tatolCount);
            ahievePanle.ResponseAchieveItem();
            UpdateShowUI();
            transform.SetSiblingIndex ( ahievePanle.GetTaskCount() - 1);
        }
    }
}
