using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using YooAsset;

public class MiniMaoPanel : UiWoPropAbility
{
    private HomeMapUiManager uiManager;
    private UiMapPlane currMap;
    [SerializeField]
    private List<MiniMapItem> miniMapList = new List<MiniMapItem>();
    [SerializeField]
    private Transform miniMapParent;
    [SerializeField]
    private GameObject miniMapPrefab;

    private bool openOne = true;

    public void Init(HomeMapUiManager uiManager)
    {
        this.uiManager = uiManager;
        if (openOne)
        {
            SpawnMiniMap();
            openOne = false;
        }
    }

    public override void OpenEnd()
    {
    }

    //Assets/Prefabs/UI/UiCompent/YooLoad/MiniMapitem.prefab
    //Assets/Prefabs/Food/needFood/NeedShuPian.prefab
    private void SpawnMiniMap()
    {
        AssetOperationHandle gustPrefab = null;
       //var  ustPrefab = Instantiate(miniMapPrefab);
        float waitTime = 0f;
        while (gustPrefab == null)
        {
           gustPrefab = SystemDataFactoy.instans.GetYooAssetHandle("Assets/Prefabs/UI/UiCompent/YooLoad/MiniMapitem.prefab");
            waitTime += Time.deltaTime;
            if (waitTime >= 10f)
                break;
        }
        if (gustPrefab == null)
        {
            return;
        }

        var map = uiManager.StartHomeManager.MapManager;
        for (int i=0; i<map.MapList.Count; i++)
        {
             var miniMap = gustPrefab.InstantiateSync();
            miniMap.transform.SetParent(miniMapParent);
            miniMap.transform.localScale = Vector3.one;
            var compenent = miniMap.GetComponent<MiniMapItem>();

            if (i == 0)
            {
                compenent.IsLock = true;
            }
            compenent.UpdataMiniMapShow();
            miniMapList.Add(compenent);
 
        }

        currMap = map.MapList[0];
        currMap.gameObject.SetActive(true);
    }

    public UiMapPlane GetCurrMap()
    {
        return currMap;
    }
}
