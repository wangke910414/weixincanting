using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class OverPlanel : UiWoPropAbility
{

    private UiManager uiManager;

    [SerializeField]
    private Transform cionBg;

    public void UiConfigur(UiManager uiManager)
    {
        this.uiManager = uiManager;
    }

    public override void OpenUiWo()
    {
        this.gameObject.SetActive(true);
        cionBg.gameObject.SetActive(false);
        //TargetGroup.transform.localPosition = new Vector3(0,-1000f, 0);
        //TargetGroup.transform.DOLocalMoveY(0,1f).SetEase(Ease.OutBounce);
        StartCoroutine(CionScore());
    }

    private IEnumerator CionScore()
    {
        yield return new WaitForSeconds(3);
        cionBg.gameObject.SetActive(true);
        cionBg.localPosition = new Vector3(0, -1000f, 0);
        cionBg.DOLocalMoveY(0, 1f).SetEase(Ease.OutBounce);
    }

    public override void CloseUiWo()
    {
        cionBg.DOLocalMoveY(-1000, 1f).SetEase(Ease.OutBack);
    }
    
    public void OnClickContinue()
    {
       //  uiManager.LoadingPlanle();
         this.gameObject.SetActive(false);
        UnityEngine.SceneManagement.SceneManager.LoadScene("Start", UnityEngine.SceneManagement.LoadSceneMode.Additive);
        //CloseUiWo();
    }

}
