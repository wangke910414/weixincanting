using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using LitJson;
public class AchievePanel : UiWoPropAbility
{
    private HomeMapUiManager uiManager;
    //[SerializeField]
    //private Button wholeBtn;
    //[SerializeField]
    //private Button receiveBtn;

    [SerializeField]
    private GameObject[] contentParent;

    [SerializeField]
    private GameObject[] scrollView;

    [SerializeField]
    private GameObject taskItemPrafab;
    [SerializeField]
    private TextMeshProUGUI totalCompeletTaskText;
    [SerializeField]
    private Button totallBut;
    [SerializeField]
    private Button OnceReciveBtn;
    [SerializeField]
    private Transform ObtainAnimationContent;
    [SerializeField]
    private Transform obtainAnimationTarget;
    [SerializeField]
    private TextMeshProUGUI diamondText;

   // []

    private AchieveTaskConfigur achieveConfigur;
    private List<TaskItem> achieveList = new List<TaskItem>();

    public void Init(HomeMapUiManager uiManager)
    {
        this.uiManager = uiManager;
    }

    //private int index = 0;

    public override void InitWo()
    {
       

        ResetTask();
        CreaAchieveTaks();
        base.InitWo();
    }

    public void OnClickClose()
    {
        CloseUiWo();
    }
    public override void OpenUiWo()
    {
        Debug.Log("调打开");
        UpdateUI();
        base.OpenUiWo();
        obtainAnimationTarget.parent.gameObject.SetActive(false);
    }

    private void CreaAchieveTaks()
    {
        achieveConfigur = SystemDataFactoy.instans.GetResourceConfigur("AchieveTaskConfigur") as AchieveTaskConfigur;
        // yield return LoadConfigursManager.instans;
        for (int i = 0; i <achieveConfigur.levelAchievelList.Count; i++)
        {
           var item = achieveConfigur.levelAchievelList[i];
            var task = Instantiate(taskItemPrafab);
            task.transform.SetParent(contentParent[1].transform);
            task.transform.localScale = Vector3.one;
            var taskComponent = task.GetComponent<TaskItem>();
            taskComponent.taskName.text = item.acheveDescrible;
            taskComponent.taskDescribe.text = item.acheveDescrible;
            taskComponent.taskPrizeCountText.text = item.prize.PrizeCount.ToString();
            taskComponent.currCount = item.currCount;
            taskComponent.tagetCount = item.tagerCount;
            taskComponent.AchieveTask = item;
            taskComponent.AhievePanle = this;
            taskComponent.UpdateShowUI();
            achieveList.Add(taskComponent);
        }

        for (int i = 0; i < achieveConfigur.continuSubmitAchievelList.Count; i++)
        {
            var item = achieveConfigur.continuSubmitAchievelList[i];
            var task = Instantiate(taskItemPrafab);
            task.transform.SetParent(contentParent[1].transform);
            task.transform.localScale = Vector3.one;
            var taskComponent = task.GetComponent<TaskItem>();
            taskComponent.taskName.text = item.acheveDescrible;
            taskComponent.taskDescribe.text = item.acheveDescrible;
            taskComponent.taskPrizeCountText.text = item.prize.PrizeCount.ToString();
            taskComponent.currCount = item.currCount;
            taskComponent.tagetCount = item.tagerCount;
            taskComponent.AchieveTask = item;
            taskComponent.AhievePanle = this;
            taskComponent.UpdateShowUI();
            achieveList.Add(taskComponent);
        }
        for (int i = 0; i < achieveConfigur.upGradationAchievelList.Count; i++)
        {
            var item = achieveConfigur.upGradationAchievelList[i];
            var task = Instantiate(taskItemPrafab);
            task.transform.SetParent(contentParent[1].transform);
            task.transform.localScale = Vector3.one;
            var taskComponent = task.GetComponent<TaskItem>();
            taskComponent.taskName.text = item.acheveDescrible;
            taskComponent.taskDescribe.text = item.acheveDescrible;
            taskComponent.taskPrizeCountText.text = item.prize.PrizeCount.ToString();
            taskComponent.currCount = item.currCount;
            taskComponent.tagetCount = item.tagerCount;
            taskComponent.AchieveTask = item;
            taskComponent.UpdateShowUI();
            taskComponent.AhievePanle = this;
            achieveList.Add(taskComponent);
        }
        for (int i = 0; i < achieveConfigur.PropAchievelList.Count; i++)
        {
            var item = achieveConfigur.PropAchievelList[i];
            var task = Instantiate(taskItemPrafab);
            task.transform.SetParent(contentParent[1].transform);
            task.transform.localScale = Vector3.one;
            var taskComponent = task.GetComponent<TaskItem>();
            taskComponent.taskName.text = item.acheveDescrible;
            taskComponent.taskDescribe.text = item.acheveDescrible;
            taskComponent.taskPrizeCountText.text = item.prize.PrizeCount.ToString();
            taskComponent.currCount = item.currCount;
            taskComponent.tagetCount = item.tagerCount;
            taskComponent.AhievePanle = this;
            taskComponent.AchieveTask = item;
            taskComponent.UpdateShowUI();
            achieveList.Add(taskComponent);
        }
        for (int i = 0; i < achieveConfigur.currentcyAchievelList.Count; i++)
        {
            var item = achieveConfigur.currentcyAchievelList[i];
            var task = Instantiate(taskItemPrafab);
            task.transform.SetParent(contentParent[1].transform);
            task.transform.localScale = Vector3.one;
            var taskComponent = task.GetComponent<TaskItem>();
            taskComponent.taskName.text = item.acheveDescrible;
            taskComponent.taskDescribe.text = item.acheveDescrible;
            taskComponent.taskPrizeCountText.text = item.prize.PrizeCount.ToString();
            taskComponent.currCount = item.currCount;
            taskComponent.tagetCount = item.tagerCount;
            taskComponent.AchieveTask = item;
            taskComponent.AhievePanle = this;
            taskComponent.UpdateShowUI();
            achieveList.Add(taskComponent);
        }
        for (int i = 0; i < achieveConfigur.restaurentAchievelList.Count; i++)
        {
            var item = achieveConfigur.restaurentAchievelList[i];
            var task = Instantiate(taskItemPrafab);
            task.transform.SetParent(contentParent[1].transform);
            task.transform.localScale = Vector3.one;
            var taskComponent = task.GetComponent<TaskItem>();
            taskComponent.taskName.text = item.acheveDescrible;
            taskComponent.taskDescribe.text = item.acheveDescrible;
            taskComponent.taskPrizeCountText.text = item.prize.PrizeCount.ToString();
            taskComponent.currCount = item.currCount;
            taskComponent.tagetCount = item.tagerCount;
            taskComponent.AchieveTask = item;
            taskComponent.UpdateShowUI();
            taskComponent.AhievePanle = this;
            achieveList.Add(taskComponent);
        }

        UpdateUI();
    }
    private void UpdateUI()
    {
        if (achieveList.Count == 0)
            return;

        for (int i=0; i< achieveList.Count; i++ )
        {
            
             achieveList[i].UpdateShowUI();
            AcheveTaskSrort();

        }
        var count = PlayerData.instance.achieveParmeter.tatolCount > 20 ? 20 : PlayerData.instance.achieveParmeter.tatolCount;
        totalCompeletTaskText.text = count.ToString() + "/" + 20.ToString();
        totallBut.interactable = false;

       bool b = CheckTaskCompelet();
       OnceReciveBtn.interactable = b;
    }


    private bool CheckTaskCompelet()
    {
        for (int i = 0; i < achieveList.Count; i++)
        {
            if (achieveList[i].GetValue() >= 1f)
            {
                return true;
            }
        }
        return false;
    }

    //一键领取
    public void OnWholeRecriveClickBtn()
    {
        var diamondCount = 0;
        for (int i = 0; i < achieveList.Count; i++)
        {
            achieveList[i].ClickAchieve();
            if(achieveList[i].GetValue() >= 1f)
            {
                diamondCount += int.Parse(achieveList[i].taskPrizeCountText.text.ToString());
            }
        }
       // if (diamondCount <= 0)
            //return;

        DiamondStoreTipEvent ent = new DiamondStoreTipEvent();
        ent.style = ObatainTipShowType.DiamondStoreThree;
        ent.giftCount = diamondCount;
        if (uiManager != null)
            uiManager.TipDlgManager.OpenTipDlg(TipDlgType.ObttainTip, this, null, ent);
        
        PlayerData.instance.PlayerDataParameSave();
        OnceReciveBtn.interactable = false;
    }


    //领取总奖励
    public void OnRecriveClickBtn()
    {
        DiamondStoreTipEvent ent = new DiamondStoreTipEvent();
        ent.style = ObatainTipShowType.DiamondStoreThree;
        ent.giftCount = 100;
        if (uiManager != null)
            uiManager.TipDlgManager.OpenTipDlg(TipDlgType.ObttainTip, this, null, ent);
        PlayerData.instance.achieveParmeter.tatolCount = 0;
        totalCompeletTaskText.text = PlayerData.instance.achieveParmeter.tatolCount.ToString() + "/" + 20.ToString();
        totallBut.interactable = false;
        PropReward();
        var achieveData = JsonMapper.ToJson(PlayerData.instance.achieveParmeter);
        PlayerData.instance.SavePlayerPrefsValue("AchieveTaskData", achieveData);
    }
    private void PropReward()
    {
        var rangProp = (ConsumePropType)UnityEngine.Random.Range(1, 7);
        Debug.Log("随机道具   " + rangProp);
        var propName = rangProp.ToString();

        if (PlayerData.consumePropDataParameter.consumePropDataDict.ContainsKey(propName))
        {
            var tempCount = int.Parse(PlayerData.consumePropDataParameter.consumePropDataDict[propName]) + 1;
            PlayerData.consumePropDataParameter.consumePropDataDict[propName] = tempCount.ToString();
        }
        else
        {
            PlayerData.consumePropDataParameter.consumePropDataDict.Add(propName, 1.ToString());
        }

#if UNITY_EDITOR
        var daata = JsonMapper.ToJson(PlayerData.consumePropDataParameter);
        Debug.Log("保存道具数据    " + daata);
        CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("consumeProp", daata));
#elif !UNITY_EDITOR && UNITY_WEBGL
        var daata = JsonMapper.ToJson(PlayerData.consumePropDataParameter);
        Debug.Log("保存道具数据    " + daata);
        CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("UpdateConsumeProp", daata));
#endif
    }

    private void ResetTask()
    {
    }
    
    /// <summary>
    /// 领取建立
    /// </summary>
    public void ResponseAchieveItem()
    {
        CompeletTotallTaks();
        UpdateUI();
    }

    private void CompeletTotallTaks()
    {
        totallBut.interactable = false; 
        if ( PlayerData.instance.achieveParmeter.tatolCount >= 20 )
        {
            PlayerData.instance.achieveParmeter.tatolCount = 20;
            totallBut.interactable = true;
        }

        totalCompeletTaskText.text = PlayerData.instance.achieveParmeter.tatolCount.ToString() + "/" + 20.ToString();
        var achieveData = JsonMapper.ToJson(PlayerData.instance.achieveParmeter);
        PlayerData.instance.SavePlayerPrefsValue("AchieveTaskData", achieveData);
    }

    private void AcheveTaskSrort()
    {
       /// Debug.Log("排序前任务长度   " + achieveList.Count);
        for (int i=0; i<achieveList.Count -1; i++)
        {
            for (int j=0; j<achieveList.Count - i -1; j++)
            {
               

                if (achieveList[j].GetValue()*100 <achieveList[j+1].GetValue() * 100)
                {
                    var temp = achieveList[j];
                    achieveList[j] = achieveList[j + 1];
                    achieveList[j + 1] = temp;
                }
            }
        }
       
        for (int i=0; i< achieveList.Count; i++)
        {
            achieveList[i].transform.SetSiblingIndex(i);
        }

    }
    public int GetTaskCount()
    {
        return achieveList.Count;
    }

    private void ObtainSave()
    {
        var achieveData = JsonMapper.ToJson(PlayerData.instance.achieveParmeter);
        PlayerData.instance.SavePlayerPrefsValue("AchieveTaskData", achieveData);
    }

    public override void TipDlgExitCallBanck(TipDlg tipDlg, TipDlgType type, bool success)
    {
        if (!this.gameObject.activeSelf)
            return;
        if (tipDlg == null)
            return;

        if (type == TipDlgType.ObttainTip)
        {
            ObtainTip dlg = tipDlg as ObtainTip;
            var count = int.Parse(dlg.PrizeCountText.text);
            count = count < 5 ? 3 : 6; 
            PlayObtainAnimation(dlg.PrizeImg.transform, count);
            obtainAnimationTarget.parent.gameObject.SetActive(true);
        }
        base.TipDlgExitCallBanck(tipDlg, type, success); 
    }
    private void PlayObtainAnimation(Transform prize, int count)
    {

        var comonent = ObtainAnimationContent.transform.GetComponent<GhostFlightAnimation>();
        if (comonent == null)
            return;
        ObtainAnimationContent.position = prize.position;

        comonent.PlayFlightAnimation(prize.gameObject, obtainAnimationTarget, count,() => {

            obtainAnimationTarget.parent.gameObject.SetActive(false);
            diamondText.text = PlayerData.playerDataParame.diamond.ToString();
        });

    }
}
