using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

//体力补充
public class StrengthSupplementPanel : UiWoPropAbility
{
    [SerializeField]
    private TextMeshProUGUI diamondText;

    private HomeMapUiManager uiManager;
    private bool buyCloase = false;

    [SerializeField]
    private Image[] strenghtIconImg;

    [SerializeField]
    private Button adbButton;
    [SerializeField]
    private Button buyButton;
    [SerializeField]
    private Transform adbStrenght;
    [SerializeField]
    private Transform diamondStrength;
    [SerializeField]
    private GhostFlightAnimation flightEffice;

    private int strenghtLmiit = 5;            //根据会员体力上限
    [SerializeField]
    private Material materialNull;

    [SerializeField]
    private Transform adbButenOff;          //广告
    [SerializeField]
    private Transform buyButtenoff;         //购买
                                            //
    [SerializeField]
    private Image[] adbImg;
    [SerializeField]
    private Image[] buyImg;

    public void Init(HomeMapUiManager uiManager)
    {
        this.uiManager = uiManager;
    }
    private void OnEnable()
    {
        buyCloase = false;
    }
    public override void OpenEnd()
    {
        DisPlayRefesh();

        base.OpenEnd();
    }

    private void DisPlayRefesh()
    {
        diamondText.text = PlayerData.playerDataParame.diamond.ToString();
        Debug.Log("当前体力    " + PlayerData.playerDataParame.energy);
        if (PlayerData.playerDataParame.energy >= 5)
        {
            SetAdbButton(false);
            SetBuyButton(false);
        }
        else if (PlayerData.playerDataParame.energy > 2)
        {
            SetAdbButton(true);
            SetBuyButton(false);

        }
        else if (PlayerData.playerDataParame.energy <= 2)
        {
            SetAdbButton(true);
           SetBuyButton(true);
   
        }
        int showCount = GetStrenghtShow();
  
        for (int i = 0; i < showCount; i++)
        {
            strenghtIconImg[i].gameObject.SetActive(true);
        }
    }
    //
    public void OnClickStrenghtSupplement()
    {
        buyCloase = true;
        this.CloseUiWo();

    }
    public override void CloseEnd()
    {
        if (buyCloase)
            uiManager.FastOpenDiamondShopDlg(gameObject);
    }

    //广告获得
    public void OnClickAdbObtain()
    {
         PlayAniamtion();
        if (PlayerData.playerDataParame.energy < 5)
            PlayerData.playerDataParame.energy++;
        if (PlayerData.playerDataParame.energy >= 5)
        {
            PlayerData.instance.SavePlayLocadData();
        }
    }


    private void PlayAniamtion()
    {
            var showCount = GetStrenghtShow();
        var trarget = strenghtIconImg[showCount].transform ;
            flightEffice.PlayFlightAnimation(trarget, EfficePlayEnd);
        adbButton.interactable = false;
        buyButton.interactable = false;
    }

    //钻石获得
    public void OnClickDiamondObtain()
    {

        string msg = "确定使用钻石！";
        uiManager.TipDlgManager.OpenTipDlg(TipDlgType.DimaondCofirmTip, this, msg);
    }
    private void DiamondBuy()
    {
        if (PlayerData.playerDataParame.energy > 5)
            return;
        if (PlayerData.playerDataParame.diamond - 30 > 0)
        {


            if (PlayerData.playerDataParame.energy < 5)
            {
               // Debug.Log("当像体力值2  ");
                var tempCount = 5 - PlayerData.playerDataParame.energy;
                //Debug.Log("当像体力值   " + tempCount);
                //Debug.Log("当像体力值   " + PlayerData.playerDataParame.energy);

                adbButton.interactable = false;
                buyButton.interactable = false;
                for (int i= PlayerData.playerDataParame.energy; i< 5; i++)
                {
                    Debug.Log("当像体力值   " + i);
                    var count = GetStrenghtShow();
                    var target = strenghtIconImg[i].transform;
                    var fligth = Instantiate(flightEffice.gameObject);
                    fligth.transform.SetParent(strenghtIconImg[i].transform.parent);
                    fligth.transform.localScale = Vector3.one;
                    var component = fligth.GetComponent<GhostFlightAnimation>();
                    if (component != null)
                        component.PlayFlightAnimation(target, EfficePlayEnd);
                }
            }

            PlayerData.playerDataParame.energy += 3;
            //if (PlayerData.playerDataParame.energy >= 5)
            //{
            //   // PlayerData.instance.playerLoacadData.strengthConsumeTime = 0f;
            //    PlayerData.instance.SavePlayLocadData();
            //}
           PlayerData.instance.PlayerDataParameSave();
           
        }
    }

    private void MutipleAnimation()
    {

    }

    private void EfficePlayEnd()
    {
        DisPlayRefesh();

        buyButton.interactable = true;
        adbButton.interactable = true;
    }
    public override void TipDlgExitCallBanck(TipDlg tipDlg, TipDlgType type, bool success)
    {
        if (!gameObject.activeSelf)
            return;

        if (type == TipDlgType.DimaondCofirmTip)
        {
            Debug.Log("收到砖石购买体力");
            DiamondBuy();
        }
    }
    /// <summary>
    /// 获取体力等级
    /// </summary>
    /// <returns></returns>
    public int GetStrengtPophLimt()
    {
        var tempLevel = 0;
        if (PlayerData.associationMenber != null)
        {
            tempLevel = strenghtLmiit + PlayerData.associationMenber.levelGrade ;
        }
        return tempLevel;
    }
    private int GetStrenghtShow()
    {
    int showCount = 0;
        if (PlayerData.playerDataParame.energy > 5)
        {
            showCount = 5;
        }
        else
        {
    showCount = PlayerData.playerDataParame.energy;
        }
     return showCount;
    }

    private void SetAdbButton(bool state)
    {
        for (int i=0; i<adbImg.Length; i++)
        {
            if (state == false)
            {
                adbImg[i].material = materialNull;
            }
            else
            {
                adbImg[i].material = null;
            }
        }

        if (state == false)
        {
            adbButenOff.gameObject.SetActive(true);
            adbButton.GetComponent<Image>().material = materialNull;

        }
        else
        {

            adbButenOff.gameObject.SetActive(false);
            adbButton.GetComponent<Image>().material = null;
        }
    }

    private void SetBuyButton(bool state)
    {
        for (int i=0; i<buyImg.Length; i++)
        {
            if (state == false)
            {
                buyImg[i].material = materialNull;
            }
            else
            {
                buyImg[i].material = null;
            }
        }

        if (state == false)
        {
            buyButtenoff.gameObject.SetActive(true);
            buyButton.GetComponent<Image>().material = materialNull;
        }
        else
        {
            buyButtenoff.gameObject.SetActive(false);
            buyButton.GetComponent<Image>().material = null;
        }
    }
}