using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AboutPlanel : UiWoPropAbility
{
    private HomeMapUiManager uiManager;

    public void Init(HomeMapUiManager uiManager)
    {
        this.uiManager = uiManager;

    }
    
    public void OnClickTipBtn(int index)
    {
        if (index == 0)
        {
            uiManager.TipDlgManager.OpenTipDlg(TipDlgType.ContimuReward, this);
        }
        else if (index == 1)
        {
            uiManager.TipDlgManager.OpenTipDlg(TipDlgType.Gameleve, this);
        }
        else if (index == 2)
        {
            uiManager.TipDlgManager.OpenTipDlg(TipDlgType.PropIllustrate, this);
        }
        else if (index == 3)
        {
            uiManager.TipDlgManager.OpenTipDlg(TipDlgType.MoneyIllustrate, this);
        }
        else if (index == 4)
        {
            uiManager.TipDlgManager.OpenTipDlg(TipDlgType.KeyIllustrate, this);
        }
    }
}
