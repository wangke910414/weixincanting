using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum BtnStateType : int
{
    Null,
    Normal,
    HighLight,
    Select,
}

public class AssocriationLevelBtn : MonoBehaviour
{
    [SerializeField]
    private Image selectImg;
    //[SerializeField]
    //private Image highLightImg;

    [SerializeField]
    private Sprite highLightSprite;
    [SerializeField]
    private Sprite ordinarySprite;
    
    private BtnStateType btnState = BtnStateType.Null;

    private   bool isSelect = false;

    public BtnStateType BtnStae { get => btnState;  }
    public bool IsSelect { get => isSelect; }

      
    public void SetBtnState(BtnStateType state)
    {
        btnState = state;
        if (btnState == BtnStateType.Normal)
        {
            this.GetComponent<Image>().sprite = ordinarySprite;
        }
        else if (btnState == BtnStateType.HighLight)
        {
            this.GetComponent<Image>().sprite = highLightSprite;
           
            this.GetComponent<Button>().enabled = true;
        }
        else if (btnState == BtnStateType.Select)
        {
            this.GetComponent<Image>().sprite = highLightSprite;
            
            this.GetComponent<Button>().enabled = true;
        }
    }
    public void SetSelect(bool value)
    {
        isSelect = value;
        selectImg.gameObject.SetActive(value);
    }
}
