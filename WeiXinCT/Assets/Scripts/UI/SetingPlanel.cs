using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetingPlanel : UiWoPropAbility
{
    [SerializeField]
    private Image musicOffImg;
    [SerializeField]
    private Image soundOffImg;
    [SerializeField]
    private ContactTip contactTip;

    private void Start()
    {

        InitlalPlanl();

    }

    private void InitlalPlanl()
    {
        AudioManagur.Instance.InitlalValue();

        if (AudioManagur.Instance.AudioSoundIsOff)
            soundOffImg.gameObject.SetActive(false);
        else
            soundOffImg.gameObject.SetActive(true);

        if (AudioManagur.Instance.AudioMusicIsOff)
            musicOffImg.gameObject.SetActive(false);
        else
            musicOffImg.gameObject.SetActive(true);
    }

    public void OnClickAudioMisic()
    {
        if (musicOffImg.gameObject.activeSelf)
        {
            musicOffImg.gameObject.SetActive(false);
        }
        else
        {
            musicOffImg.gameObject.SetActive(true);
        }
        AudioManagur.Instance.SetAudioMusic();
    }

    public void OnClickAudioSound()
    {
        if (soundOffImg.gameObject.activeSelf)
        {
            soundOffImg.gameObject.SetActive(false);
        }
        else
        {
            soundOffImg.gameObject.SetActive(true);
        }
        AudioManagur.Instance.SetAuduioSound();
    }
    
    public void ConttactUs()
    {
        contactTip.OpenDlg();
    }
}
