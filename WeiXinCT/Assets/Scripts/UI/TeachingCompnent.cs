using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TeachingType : int
{
    Null,
    LevelOnClick,
    SelectLevelClick,
    StartGameClick,
    MakeFood,
    FoodMakeCompelt,
    SubmitFood,
    SubmitDrink,
    AddOtherFood,
    DviceUpGradeOne,
    DviceUpGradeTwo,
    DviceUpGradeThree,
    FoodUpGradeOne,
    FoodUpGradeTwo,
    FoodUpGradeThree,
    FoodFire,
}

public class TeachingCompnent : MonoBehaviour
{
    [SerializeField]
    private TeachingType teachingType;
    [SerializeField]
    TeachingPanle teachingPanle;

    public TeachingType TeachingType { get => teachingType; }

    public TeachingPanle TeachingPanle { set => teachingPanle = value;}
    public void Oncklick()
    {
        Time.timeScale = 1;
         CoroutineHandler.StartStaticCoroutine( WaitActive());
    }
    private IEnumerator WaitActive()
    {
        yield return new WaitForSeconds(0.3f);
        gameObject.SetActive(false);
        teachingPanle.OnClikcTeaching();
        EventTeachingMsg mes = new EventTeachingMsg();
        mes.teachingType = teachingType;
        SystemEventManager.TriggerEvent("TeachingMsg", mes);
    }
}
