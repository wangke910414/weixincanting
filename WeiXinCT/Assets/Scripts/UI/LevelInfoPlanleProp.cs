using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class LevelInfoPlanleProp : MonoBehaviour
{

    [SerializeField]
    private Image lockImg;
    [SerializeField]
    private Image addImg;
    [SerializeField]
    private Image bolImg;
    [SerializeField]
    private TextMeshProUGUI numberText;
    [SerializeField]
    private Material material;
    [SerializeField]
    private Image iconImg;
    [SerializeField]
    private Image recommendImg;     //建议使用

    private int unLockLevle;
    private int dbNumber = 0;       //数据数量
    private bool isUnLock = false;
    private LevelInfoPlane levelInfoPlane;

    private GameObject panelParent;

    private bool isSelcet = false;
    private bool isReCommend = false;       //是否推荐使用

    private ConsumePropType consumePropType;

    private PropShowType propShowType;

    private UseType useType;        //使用类型；
    public Image IconImg { get => iconImg; }
    public int UnLockLevle { get => unLockLevle; set => unLockLevle = value; }
    public GameObject PanelParent { get => panelParent; set => panelParent = value; }

    public bool IsReCommend { get => isReCommend; set => isReCommend = value; }
    public bool IsSelcet { get => isSelcet; }
    public TextMeshProUGUI NumberText { get => numberText;  }

    public int DbNumber { get => dbNumber; set => dbNumber = value; }
    public ConsumePropType ConsumePropType { get => consumePropType; set => consumePropType = value; }
    public LevelInfoPlane LevelInfoPlane { get => levelInfoPlane; set => levelInfoPlane = value; }

    public Image  BolImg { get => bolImg; }
    public PropShowType PropShowType { get => propShowType; set => propShowType = value; }

    public UseType PropUseType { set => useType = value; get => useType; }
    public void SetUnLockStart(bool isLock)
    {
        if (isLock) //解锁
        {
            lockImg.gameObject.SetActive(false);
             iconImg.material = null;
            isUnLock = true;
            numberText.gameObject.SetActive(true);

        }
        else
        {
            lockImg.gameObject.SetActive(true);
            iconImg.material = material;
            isUnLock = false;
            numberText.gameObject.SetActive(false);
            addImg.gameObject.SetActive(false);
        }
    }
    public void SsetPropShow()
    {
        if (!isUnLock)
            return;
        if (dbNumber > 0)
        {
            numberText.text = dbNumber.ToString();
            addImg.gameObject.SetActive(false);
            numberText.gameObject.SetActive(true);
        }
        else
        {
            addImg.gameObject.SetActive(true);
            bolImg.gameObject.SetActive(false);
            numberText.gameObject.SetActive(false);
        }
        recommendImg.gameObject.SetActive(isReCommend);
    }

    public void OnClick()
    {
        if (!isUnLock)
        {
            if (levelInfoPlane != null)
            {
                levelInfoPlane.CallPropClick(isUnLock, UnLockLevle);
            }
            return;
        }
        if (dbNumber <= 0)
            return;
        if (!bolImg.gameObject.activeSelf)
            bolImg.gameObject.SetActive(true);
        else
            bolImg.gameObject.SetActive(false);

        isSelcet = bolImg.gameObject.activeSelf;

        iconImg.transform.DOScale(1.4f, 0.2f).SetEase(Ease.Flash).SetLoops(2,LoopType.Yoyo);
    }
    public void OnCkickAddProp()
    {
        if (propShowType == PropShowType.LevelInfoPanel)
        {
            levelInfoPlane.ByConsumeProp();
        }

        else if (propShowType == PropShowType.StopPanel)
        {
            panelParent.GetComponent<StopPanel>().FastBuyProp(iconImg.sprite, consumePropType);
        }
    }
    public void SetGameHomeState()
    {
        bolImg.gameObject.SetActive(false);
        lockImg.gameObject.SetActive(false);
        numberText.gameObject.SetActive(false);
        addImg.gameObject.SetActive(false);
        if (useType == UseType.moment)
        {
            this.gameObject.SetActive(true);
            StartCoroutine(MomentPorp());
        }
    }

    private IEnumerator MomentPorp()
    {
        yield return new WaitForSeconds(2f);
        this.gameObject.SetActive(false);
        if (PlayerData.consumePropDataParameter.consumePropDataDict.ContainsKey(consumePropType.ToString()))
            PlayerData.consumePropDataParameter.consumePropDataDict.Remove(consumePropType.ToString());
    }
}



public enum PropShowType : int
{
   Null,
   LevelInfoPanel,
   GameHomePanel,
   StopPanel,
}