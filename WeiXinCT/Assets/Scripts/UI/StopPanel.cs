using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StopPanel : TipDlg,CTinterface
{
    [SerializeField]
    private Transform propViewPrent;
    //[SerializeField]
    [SerializeField]
    private FastBuyTip fastBuyTip;
    [SerializeField]
    private GameObject consumPropPrefab;

    //道具链表
    private List<LevelInfoPlanleProp> consumePropList = new List<LevelInfoPlanleProp>();
    private void Start()
    {
        InitProp();
    }

    public override void OnClickClose()
    {
        Time.timeScale = 1;
        SetSelelctConsumePropData();
        base.OnClickClose();
    }
    public override void EfficeEnd(bool value)
    {
        if (value)
            Time.timeScale = 0;

        CounsumePropAniamtion();
    }

    private void CounsumePropAniamtion()
    {
        for (int i=0; i< consumePropList.Count; i++)
        {
            consumePropList[i].transform.DOShakeScale(1.1f, 1f).SetUpdate(true).SetEase(Ease.Flash);
        }
    }

    private void InitProp()
    {
        ConsumePropConfigur propConfigur = SystemDataFactoy.instans.GetResourceConfigur("ConsumePropConfigur") as ConsumePropConfigur;
        //var propOperation = SystemDataFactoy.instans.GetYooAssetHandle("Assets/Prefabs/UI/LoodUi/PropItem.prefab");

        for (int i = 0; i < propConfigur.ConsumeProp.Length; i++)
        {
            if (propConfigur.ConsumeProp[i].comsumePropType == ConsumePropType.AddGuest ||
                propConfigur.ConsumeProp[i].comsumePropType == ConsumePropType.InvalidSpecialConditions)
            {
                continue;
            }

            var prop =Instantiate(consumPropPrefab);
            prop.transform.SetParent(propViewPrent);
            prop.transform.localPosition = Vector3.zero;
            prop.transform.localScale = Vector3.one * 2;
            var conpnent = prop.GetComponent<LevelInfoPlanleProp>();

            bool isLock = PlayerData.instance.GetUnLockLevel() >= propConfigur.ConsumeProp[i].unLosckLevel ? true : false;
            conpnent.SetUnLockStart(isLock);
            conpnent.UnLockLevle = propConfigur.ConsumeProp[i].unLosckLevel;
            conpnent.IconImg.sprite = propConfigur.ConsumeProp[i].Icon;
            conpnent.ConsumePropType = propConfigur.ConsumeProp[i].comsumePropType;
            //conpnent.LevelInfoPlane = this;
            conpnent.PanelParent = this.gameObject;
            conpnent.PropShowType = PropShowType.StopPanel;
            conpnent.ConsumePropType = propConfigur.ConsumeProp[i].comsumePropType;
            if (PlayerData.consumePropDataParameter.consumePropDataDict.ContainsKey(conpnent.ConsumePropType.ToString()))
            {
                conpnent.DbNumber =int.Parse( PlayerData.consumePropDataParameter.consumePropDataDict[conpnent.ConsumePropType.ToString()]);
            }
            conpnent.SsetPropShow();
            consumePropList.Add(conpnent);

        }
    }

    private void OnEnable()
    {
        UpdataPropCount();
    }

    public void UpdataPropCount()
    {
        foreach (var prop in consumePropList)
        {
            if (PlayerData.consumePropDataParameter.consumePropDataDict.ContainsKey(prop.ConsumePropType.ToString()))
            {
                prop.DbNumber = int.Parse(PlayerData.consumePropDataParameter.consumePropDataDict[prop.ConsumePropType.ToString()]);
            }
            prop.SsetPropShow();
        }
    }

    public void FastBuyProp(Sprite icon, ConsumePropType propType)
    {
        fastBuyTip.InpuIcon = icon;
        fastBuyTip.PropType = propType;
        fastBuyTip.parentDlg = this;
        fastBuyTip.parentPane = this.gameObject;
        fastBuyTip.OpenDlg();
    }

    //关闭设置选中道具
    private void SetSelelctConsumePropData()
    {
        for (int i=0; i< consumePropList.Count; i++)
        {
            if (consumePropList[i].IsSelcet)
            {

                if (!PlayerData.instance.currSelectPropList.Contains(consumePropList[i].ConsumePropType))
                {
                    PlayerData.instance.currSelectPropList.Add(consumePropList[i].ConsumePropType);
                    var temp = int.Parse(PlayerData.consumePropDataParameter.consumePropDataDict[consumePropList[i].ConsumePropType.ToString()]) - 1;
                    PlayerData.consumePropDataParameter.consumePropDataDict[consumePropList[i].ConsumePropType.ToString()] = temp.ToString();
                }
            }
        }
        var str = JsonMapper.ToJson(PlayerData.consumePropDataParameter.consumePropDataDict);
#if UNITY_EDITOR
        CoroutineHandler.StartStaticCoroutine( PlayerData.instance.SaveData("consumeProp", str));
 #elif !UNITY_EDITOR && UNITY_WEBGL
        CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("UpdateConsumeProp", str));
#endif

        for (int i = 0; i < consumePropList.Count; i++)
        {
            consumePropList[i].BolImg.gameObject.SetActive(false);
        }
       SystemGameManager.UiManager.GameHomePlael.UpdatePropShow();
        SystemGameManager.GuestManager.GameStopPropAction();
        SystemGameManager.OperationManage.GameAddPropAction();
    }

    public void FastBuyTip()
    {
        UpdataPropCount();
    }

    public void ExitGame()
    {
        Time.timeScale = 1;
        PlayerData.instance.continueGame = true;
        UnityEngine.SceneManagement.SceneManager.LoadScene("Start");
       
    }
}
