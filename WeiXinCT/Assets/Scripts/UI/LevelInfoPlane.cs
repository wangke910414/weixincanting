using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;
using WeChatWASM;
using LitJson;
using UnityEngine.SceneManagement;
using System;

public class LevelInfoPlane : UiWoPropAbility
{
    private HomeMapUiManager uiManager;

    [SerializeField]
    private TextMeshProUGUI shopNameText;

    [Header("Prafeb")]
    [SerializeField]
    private GameObject shelfPrefab;
    [SerializeField]
    private GameObject levelShowPrefab;
    [SerializeField]
    private GameObject foodShowPrefab;
    [SerializeField]
    private GameObject deviceShowPrefab;
    [SerializeField]
    private GameObject consumePropPrefab;

    [Header("LevelIfor UI")]
    [SerializeField]
    private ScrollRect levelScroilRect;
    [SerializeField]
    private Button startCookingBtn;
    [SerializeField]
    private Button[]  upGraedAryBtn;

    [SerializeField]
    private GameObject[] aryUpGreadParent;

    [SerializeField]
    private Transform[] scroilViewContent;

    [SerializeField]
    private Transform scroilViewPropContent;

    [SerializeField]
    private Transform[] showPlaneNode;

    [SerializeField]
    private TextMeshProUGUI tragetText;

    [SerializeField]
    private Image[]  targetImg;
    
    [SerializeField]
    private TextMeshProUGUI cionPrizeText;      //金币

    [SerializeField]
    private TextMeshProUGUI keyPrizeText;       //钥匙

    [SerializeField]
    private Button skipLevelpBtn;

    [SerializeField]
    private Button seekBtn;
    [SerializeField]
    private SliderScroll lvelSlidetScroll;          //滑动组件

    private bool onceOpen = true;

    //[SerializeField]
    ///private GameObject selectLevelPrefab;

    [SerializeField]
    private Image selectButImg;


    [SerializeField]
    private TextMeshProUGUI textLevel;
    [SerializeField]
    private TextMeshProUGUI degreeText;

    [Header("FoodInfor UI")]
    [SerializeField]
    private Transform foodShowParent;
    [SerializeField]
    private TextMeshProUGUI foodNameText;
    [SerializeField]
    private Image foodProgesValue;
    [SerializeField]
    private TextMeshProUGUI currPriceText;
    [SerializeField]
    private TextMeshProUGUI maxPriceText;
    [SerializeField]
    private LevelInfoFoodUp currSelectUpFood;
    [SerializeField]
    private TextMeshProUGUI upFoodNeedCionText;
    [SerializeField]
    private Image showFoodImag;
    [SerializeField]
    private GameObject flightFoodUpPrefab;

    private int selectFoodUpIndex = 0;

    [Header("device UI")]
    [SerializeField]
    private Transform deviceParent;
    [SerializeField]
    private LevelInforDeviceUp showDeviceUp;
    [SerializeField]
    private TextMeshProUGUI deviceNameText;
    [SerializeField]
    private Image maxCountImg;
    [SerializeField]
    private Image makeTimeImg;
    [SerializeField]
    private TextMeshProUGUI currCountText;
    [SerializeField]
    private TextMeshProUGUI maxCountText;
    [SerializeField]
    private TextMeshProUGUI currMakeTimeText;
   [SerializeField]
    private TextMeshProUGUI maxMakeTimeText;
    [SerializeField]
    private TextMeshProUGUI deviceUpText;
    [SerializeField]
    private Transform recommendUp;
    [SerializeField]
    private GameObject flightDeviceUpPrefab;

    private LevelInforDeviceUp selectUpDevice;

    private int selectUpDeviceIndex = 0;


 
    private Vector3 progressCountPos = Vector3.zero;

    private Vector3 progressMakeTimePos = Vector3.zero;
    private List<List<LevelData>> levelTableList = new List<List<LevelData>>();


    private bool isDeveiceUpGrade = false;          //设备是否升级
    private bool isFoodUoGrade = false;
    private bool skipLevelRestaurentUnlock = false;      //跳过本关是否又餐厅解锁          
    //等级链表
    private List<GameObject> levlItamList = new List<GameObject>();
    //食物链表
    private List<LevelInfoFoodUp> foodItemList = new List<LevelInfoFoodUp>();
    //设变链表
    private List<LevelInforDeviceUp> deviceList = new List<LevelInforDeviceUp>();
    //道具链表
    private List<LevelInfoPlanleProp> consumePropList = new List<LevelInfoPlanleProp>();
    //架子
    private List<ShowShelf> shelfList = new List<ShowShelf>();
    private int unLockLevelCount = 0;

    private string lastOpenShopName = string.Empty;
    private bool buyPropClose = false;

    //  关卡钥钥匙动画
    private bool keyIsFlightAnimation;       //钥匙是否有飞行动画
    private bool isKeySpanRestaution;            //是否同跨餐厅
    private string previousLevelID = string.Empty;           //前一个店ID
    private int fliykeyCount;                  //飞行钥匙数量
    private LevelData currLevel;                //当前选择的关卡

    Transform flightTarget = null;    //钥匙飞行目标
    public void Init(HomeMapUiManager uiManager)
    {
         this.uiManager = uiManager;

        InitTipDlgOne(0);
    
    }
    public override void OpenUiWo()
    {
 
    unLockLevelCount = 0;
        base.OpenUiWo();
        if (onceOpen)
        {
            onceOpen = false;
               // IntDlg();
            StartCoroutine(IntDlg());
            OpenUpdaPage();
            lastOpenShopName = PlayerData.instance.curPlayeShop;
           
        }
        else if (!onceOpen /*&& lastOpenShopName != PlayerData.instance.curPlayeShop*/)
        {
            InitOpeDlg();
        }
        buyPropClose = false;
   
    }

    private void InitOpeDlg()
    {

        //IntDlg();
        RestPanle();
        StartCoroutine(IntDlg());
        OpenUpdaPage();

        lastOpenShopName = PlayerData.instance.curPlayeShop;
    }

    public override void OpenEnd()
    {
       
        SelectLevelTeaching();
        FoodUpGradeTeaching();
        DeviceUpGradeTeaching();
        CoroutineHandler.StartStaticCoroutine(KeyRunFlighet());
        base.OpenEnd();
    }


    private void KeyFlightAnimation()
    {
        if (!PlayerData.instance.keyIsFlightAnimation)
            return;

    }
    private IEnumerator IntDlg()
    {
        //
        PlayerData.instance.currSingleLevelData.foodTyeUpDit.Clear();
        InitLevelShow();
        InitFoodShew();
        InitDeviceShow();
        InitConsumePropShow();

        var shop = LoadConfigursManager.instans.ShopDict[PlayerData.instance.curPlayeShop];
        var playOnce = 1;
        if (PlayerData.playOnceParme.levelPlayOnceDataDict.ContainsKey(PlayerData.instance.curPlayeShop))
        {
            playOnce = int.Parse(PlayerData.playOnceParme.levelPlayOnceDataDict[PlayerData.instance.curPlayeShop]);
        }
        var levelName = shop.leveData + "_" + playOnce;

        var leveList = LoadConfigursManager.instans.LevelShopDataDuct[levelName];
        cionPrizeText.text = leveList[0].prizeCionNumber.ToString();
        tragetText.text = leveList[0].cionTarget.ToString();
        if (leveList[0].cionTarget != 0)
        {
            targetImg[0].gameObject.SetActive(true);
            targetImg[1].gameObject.SetActive(false);
        }
        else if (leveList[0].compeletFoodNumber != 0)
        {
            targetImg[0].gameObject.SetActive(false);
            targetImg[1].gameObject.SetActive(true);
        }
        keyPrizeText.text = leveList[0].prizeKeyNumber.ToString();
        yield break;
    }

    private  void RestPanle()
    {
         foreach (var item in levlItamList)
        {
            item.SetActive(false);
            Destroy(item);
        }
         foreach (var item in foodItemList)
        {
            item.gameObject.SetActive(false);
            Destroy(item.gameObject);
        }
         foreach(var item in deviceList)
        {
            item.gameObject.SetActive(false);
            Destroy(item.gameObject);
        }
         foreach( var item in consumePropList)
        {
            item.gameObject.SetActive(false);
            Destroy(item.gameObject);
        }
        foreach (var item in shelfList)
        {
            item.gameObject.SetActive(false);
            Destroy(item.gameObject);

        }

        PlayerData.instance.currSelectPropList.Clear();
        levlItamList.Clear();
        foodItemList.Clear();
        deviceList.Clear();
        consumePropList.Clear();
        shelfList.Clear();
        keyIsFlightAnimation = false;
        isKeySpanRestaution = false;
        previousLevelID = string.Empty;
        fliykeyCount = 0;
        unLockLevelCount = 0;
        recommendUp.gameObject.SetActive(false);
    }

    /// <summary>
    /// 更新页面
    /// </summary>
    private void OpenUpdaPage()
    {
        OnClilkUpGread(0);
        levelScroilRect.verticalNormalizedPosition = 1;

            var shop = LoadConfigursManager.instans.ShopDict[PlayerData.instance.curPlayeShop];
        var key = PlayerData.playerDataParame.keyNumber - shop.startCount;
        if (key < 0)
            key = 0;
        Debug.Log("解锁关卡数  " + unLockLevelCount);

        if (CheckRestaurentKeyObtainCompelet())
        {
            skipLevelpBtn.interactable = false;
           // return; 
        }
        var seek = ObtainKeyLevel();
        if (seek == -1)
        {
            Debug.Log("打开查找有钥匙关卡出错！");
            seek = 0;
           /// return;
        }
        SetShowLevelText(seek);
        RecommendPropUse();
    }
   
    /// <summary>
    /// 关卡展示
    /// </summary>
    /// <returns></returns>
    private void InitLevelShow()
    {
        string fileName = PlayerData.instance.curPlayeShop + "_Guest_1";
        if (!LoadConfigursManager.instans.ShopGuestDit.ContainsKey(fileName))
        {
            return;
        }

        var shopGuest = LoadConfigursManager.instans.ShopGuestDit[fileName];
         var leveList = GetLevelDataList(1); //先获取第一论
       /// var levelListTwo = GetLevelDataList(2); //第二轮
       // var levelListThree = GetLevelDataList(3);//第三轮

        var currShop = LoadConfigursManager.instans.ShopDict[PlayerData.instance.curPlayeShop];
        shopNameText.text = currShop.shopDescribleName;

        int levelShelfCount = (int)(leveList.Count / 3);
        if (leveList.Count%3 != 0 )
        {
            levelShelfCount++;
        }
        int levelDataIndex = 0;
        SetLevelInfor(leveList[0]);
        flightTarget = null;    //钥匙飞行目标
        for (int i = 0; i < levelShelfCount; i++)
        {
            var shelf = Instantiate(shelfPrefab);
            shelf.transform.SetParent(scroilViewContent[0], false);
            var parentCompnent = shelf.GetComponent<ShowShelf>();
            shelfList.Add(parentCompnent);
            for (int j = 0; j < 3; j++)
            {
                if (levlItamList.Count >= leveList.Count)
                {
                    break;
                }

                var levelData = leveList[levelDataIndex];
                levelDataIndex++;
                var level = Instantiate(levelShowPrefab);
                level.transform.SetParent(parentCompnent.itemParent[j]);
                levlItamList.Add(level);
                var compnent = level.GetComponent<LevelSelect>();
                compnent.ShelfPort = parentCompnent.transform;
                compnent.LevelPlanel = this;
                compnent.SeLectLevel = levlItamList.Count - 1;
                compnent.UnLockKeyNumber = levelData.unLoackKeyCunnt;
                compnent.LevelID = levelData.levelID;
                compnent.LevelIsShopGruap = int.Parse(levelData.levelGrade);
                compnent.ShopName = levelData.shopName;
                compnent.SetlevelItem();
                level.transform.localPosition = Vector3.zero;
                level.transform.localScale = Vector3.one;
                if (PlayerData.playerDataParame.keyNumber >= levelData.unLoackKeyCunnt || PlayerData.allLevelPlay == 1)
                {
                  ///  Debug.Log("钥匙数目    " + PlayerData.playerDataParame.keyNumber);
                   // Debug.Log("本关需要数目  " + levelData.unLoackKeyCunnt);
                   if (compnent.LevelID == PlayerData.instance.previousLevelID &&
                       PlayerData.instance.keyIsFlightAnimation && 
                       !PlayerData.instance.isKeySpanRestaution &&
                       levlItamList.Count != leveList.Count)
                   {
                        compnent.CreateKeyFlightAniation();
                   }
                    flightTarget = compnent.transform;
                    //本关解锁
                    compnent.IsLock = true;
                    unLockLevelCount++;
                    Debug.Log("unLockLevelCount数目  " + unLockLevelCount);
                    PlayerData.playerDataParame.unLockCount = unLockLevelCount;
                    var grade = 0;
                    if (PlayerData.playOnceParme.levelPlayOnceDataDict.ContainsKey(levelData.levelID))
                    {
                        grade = int.Parse(PlayerData.playOnceParme.levelPlayOnceDataDict[levelData.levelID]);
                    }
                    grade = grade >3 ? 3: grade;

                    compnent.PlayOnce = grade;
                    PlayerData.instance.currLevelPlayOnce = compnent.PlayOnce;
                    var shop = LoadConfigursManager.instans.ShopDict[PlayerData.instance.curPlayeShop];
                    var temp = (grade + 1) > 3 ? 3 : (grade + 1);
                    compnent.LevelName = shop.leveData + "_" + temp;
                    leveList = GetLevelDataList(temp);

                    levelData = leveList[levelDataIndex - 1];
                    compnent.PrizeKeyCunt = levelData.prizeKeyNumber;
                    compnent.PrizeCionCount = levelData.prizeCionNumber;
                }
                else //没有解锁
                {
                    var shop = LoadConfigursManager.instans.ShopDict[PlayerData.instance.curPlayeShop];
                    compnent.LevelName = shop.leveData + "_" + 1;
                }
                if (levelData.cionTarget != 0)
                {
                    compnent.Leveltarget = TargetType.CIonTraget;
                    compnent.TargetCount = levelData.cionTarget;
                }
                else if (levelData.compeletFoodNumber != 0)
                {
                    compnent.Leveltarget = TargetType.CompeletFood;
                    compnent.TargetCount = levelData.compeletFoodNumber;
                }
                compnent.SetLevlGrup();
            }

        }
        if (unLockLevelCount < levlItamList.Count)
            levlItamList[unLockLevelCount].GetComponent<LevelSelect>().KeyImgNode.gameObject.SetActive(true);
       
        if (PlayerData.instance.keyIsFlightAnimation && !PlayerData.instance.isKeySpanRestaution)
        {
            if (flightTarget != null)
            {
                var flightCompnent = flightTarget.GetComponent<LevelSelect>();
                if (flightCompnent != null)
                    flightCompnent.NeedPlayOpenAnimation();
            }
        }
       
    }

    /// <summary>
    /// 钥匙飞行
    /// </summary>
    /// <returns></returns>
    private IEnumerator KeyRunFlighet()
    {
        Debug.Log("运行飞行动画!1");
        if (!PlayerData.instance.keyIsFlightAnimation)
            yield break;
        if (PlayerData.instance.isKeySpanRestaution)
            yield break;

        Debug.Log("运行飞行动画!2");
        yield return new WaitForSeconds(1f);
        for (int i=0; i< levlItamList.Count; i++)
        {
            var compent = levlItamList[i].GetComponent<LevelSelect>();
            if (compent == null)
                yield break;
            if (compent.KeyIsFligth)
            {
                compent.RunKeyFlight(flightTarget);
            }
        }
        PlayerData.instance.KeyFlightReset();
    }
    /// <summary>
    /// 勃发飞行动画完成
    /// </summary>
    public void KeyFlightConpelet()
    {
        if (flightTarget == null)
            return;
        var levelItem = flightTarget.GetComponent<LevelSelect>();
        if (levelItem != null)
            levelItem.PlayOpenAnimation();
    }

    /// <summary>
    /// 食物展示
    /// </summary>
    /// <returns></returns>
    private void InitFoodShew()
    {

        var mainAssets = SystemDataFactoy.instans.GetResourceConfigur("MainFoodAssets") as MainFoodConfigur;
       var otherAssets = SystemDataFactoy.instans.GetResourceConfigur("OtherFoodAssets") as OtherFoodConfigur;

        var mainFood = LoadConfigursManager.instans.MainFoodItemDictionary;
        var otherFood = LoadConfigursManager.instans.OetherFoodDictionary;
        var shopName = PlayerData.instance.curPlayeShop;
     
        List<FoodBose> newfood = new List<FoodBose>();
        foreach (var data in mainFood)
        {
            if (data.Value.foodTypeName == "drink")
                continue;
              if (data.Value.shopName != shopName)
                continue;
            newfood.Add(data.Value);
        }
        foreach (var data in otherFood)
        {
            if (data.Value.shopName != shopName)
                continue;
            newfood.Add(data.Value);
        }
        var length = newfood.Count;
        int levelShelfCount = (int)(length / 3);
        if ( length%3 != 0)
        {
            levelShelfCount++;
        }

        for (int i=0; i< levelShelfCount; i++)
        {
            var shelf = Instantiate(shelfPrefab);
            shelf.transform.SetParent(scroilViewContent[1], false);
            var parentCompnent = shelf.GetComponent<ShowShelf>();
            shelfList.Add(parentCompnent);
            string describleText = string.Empty;
            for (int j=0; j<3; j++)
            {
                if (foodItemList.Count >= newfood.Count)
                    break;
                
               if (newfood[foodItemList.Count].foodTyoe == FoodType.MainFood)//主食操作
                {
                    var foodComponent = newfood[foodItemList.Count] as MainFoodItem;
                    var obj = Instantiate(foodShowPrefab);
                    obj.transform.SetParent(parentCompnent.itemParent[j], false);
                    var compnent = obj.GetComponent<LevelInfoFoodUp>();

                    if (foodComponent.shopName != PlayerData.instance.curPlayeShop)
                        continue;
                    var foodAsset = mainAssets.GetMainFoodAsset(foodComponent.mainFoodName);
                    if (foodAsset == null)
                        continue;
                    compnent.FoodGradeImgList = foodAsset.IconList;
                   describleText = foodAsset.foodDescrible;
               
                    int grade = 1;
                    if (PlayerData.foodDataPramerer.foodDataDict.ContainsKey(foodComponent.mainFoodName))
                    {
                        isFoodUoGrade = true;
                        grade = int.Parse(PlayerData.foodDataPramerer.foodDataDict[foodComponent.mainFoodName]);
                    }

                    compnent.FoodImg.sprite = compnent.FoodGradeImgList[grade - 1];

                    compnent.FoodIndex = foodItemList.Count;
                    compnent.levelInforPlane = this;
                    compnent.CurrFoodGrade = grade;
                    compnent.DisribleText = describleText;
                    compnent.CurrFoodPrice =(int)foodComponent.mainFoodPrice;
                    compnent.UpFoodPrive = foodComponent.upFoodPrice;
                    compnent.FoodName = foodComponent.mainFoodName;
                    compnent.IsLock = GetLevelUnLock( foodComponent.unLoackLevel ) ? true : false;
                    compnent.UnLockLevel = foodComponent.unLoackLevel;
                    compnent.updateFoodGrade();
                    foodItemList.Add(compnent);
                }
                else if(newfood[foodItemList.Count].foodTyoe == FoodType.OtherFood) //配菜操作
                {
                    var foodComponent = newfood[foodItemList.Count] as OtherFoodItem;
                    var obj = Instantiate(foodShowPrefab);
                    obj.transform.SetParent(parentCompnent.itemParent[j], false);
                    var compnent = obj.GetComponent<LevelInfoFoodUp>();

                    if (foodComponent.shopName != PlayerData.instance.curPlayeShop)
                        continue;

                    foreach (var item in otherAssets.OtheFoodCompnentList)
                    {
                        if (foodComponent.OtherFoodName == item.otherFoodName)
                        {
                            compnent.FoodGradeImgList = item.IconList;
                            describleText = item.foodDescrible;
                            break;
                        }
         
                    }
                    int grade = 1;
                    if (PlayerData.foodDataPramerer.foodDataDict.ContainsKey(foodComponent.OtherFoodName))
                    {
                        grade = int.Parse(PlayerData.foodDataPramerer.foodDataDict[foodComponent.OtherFoodName]);
                    }

                    compnent.FoodIndex = foodItemList.Count;
                    compnent.FoodImg.sprite = compnent.FoodGradeImgList[grade - 1];
                    compnent.DisribleText = describleText;
                    compnent.CurrFoodGrade = grade;
                    compnent.CurrFoodPrice = (int)foodComponent.otherFoodPrice;
                    compnent.UpFoodPrive = foodComponent.upFoodPrice;
                    compnent.FoodName = foodComponent.OtherFoodName;
                    compnent.IsLock = GetLevelUnLock(foodComponent.unLoackLevel) ? true : false;
                    compnent.UnLockLevel = foodComponent.unLoackLevel;
                    compnent.levelInforPlane = this;
                    compnent.updateFoodGrade();
                    foodItemList.Add(compnent);
                }

 
            }
        }
        if (foodItemList.Count > 0)
            CallSelectFood(0);
    }

  
    /// <summary>
    /// 设备展示
    /// </summary>
    private void InitDeviceShow()
    {
        var deviceDit = LoadConfigursManager.instans.DeviceDict;
        var deviceConfigur = SystemDataFactoy.instans.GetResourceConfigur("DeviceConfigur") as DeviceConfigur;
        var length = 0;
        List<string> keys = new List<string>(); 
        foreach (var dit in deviceDit)
        {
            if (dit.Value.shopName == PlayerData.instance.curPlayeShop)
            {
                length++;
                keys.Add(dit.Key);
            }

        }
        int levelShelfCount = (int)(length / 3);
        if (deviceDit.Count % 3 != 0)
        {
            levelShelfCount++;
        }

        // int index = 0;
        for (int i = 0; i < levelShelfCount; i++)
        {
               var shelf = Instantiate(shelfPrefab);
            shelf.transform.SetParent(scroilViewContent[2], false);
            var parentCompnent = shelf.GetComponent<ShowShelf>();
            shelfList.Add(parentCompnent);
            for (int j = 0; j < 3; j++)
            {
                if (deviceList.Count >= keys.Count)
                    break;
                var deciceData = deviceDit[keys[deviceList.Count]];
                // var obj = device.InstantiateSync();
                var obj = Instantiate(deviceShowPrefab);
                obj.transform.SetParent(parentCompnent.itemParent[j], false);
                var component = obj.GetComponent<LevelInforDeviceUp>();
                component.levelInforPlane = this;
                component.DeviceIndex = deviceList.Count;
                if(GetLevelUnLock( deciceData.unLoackLevel ))
                    component.IsLock = true;
                component.UnLockLevel = deciceData.unLoackLevel;
                component.DeviceName = deciceData.deviceName;
                component.DeviceUpPrice = deciceData.upPrice;
                component.UnLockLevel = deciceData.unLoackLevel;
                component.CurrDeviceGrade = 1;

              if (PlayerData.deviceDataParameter.deviceDict.ContainsKey(component.DeviceName))
                {
                    isDeveiceUpGrade = true;
                    component.CurrDeviceGrade = int.Parse(PlayerData.deviceDataParameter.deviceDict[component.DeviceName]);
                }

                var deviceAsset = deviceConfigur.GetDevice(deciceData.deviceName);
                if (deviceAsset != null)
                {
                    component.DeviceImg.sprite = deviceAsset.deviceIcon[component.CurrDeviceGrade - 1];
                    component.DeviceImgList = deviceAsset.deviceIcon;
                    component.MakeTime = deviceAsset.makeTime;
                    component.MinCount = deviceAsset.minCount;
                    component.MaxCount = deviceAsset.maxCount;
                    component.Desrible = deviceAsset.describe;
                }

                component.UpdataShow();
                deviceList.Add(component);
            }
        }
        if (deviceList.Count > 0)
            CallSeletDevice(0);
    }

    private void InitConsumePropShow()
    {
        ConsumePropConfigur propConfigur = SystemDataFactoy.instans.GetResourceConfigur("ConsumePropConfigur") as ConsumePropConfigur;

        for (int i = 0; i < propConfigur.ConsumeProp.Length; i++)
        {
            if (propConfigur.ConsumeProp[i].comsumePropType == ConsumePropType.AddGuest ||
                propConfigur.ConsumeProp[i].comsumePropType == ConsumePropType.InvalidSpecialConditions ||
                propConfigur.ConsumeProp[i].comsumePropType == ConsumePropType.GuestPatience)
            {
                continue;
            }
            var prop =Instantiate(consumePropPrefab);
            prop.transform.SetParent(scroilViewPropContent);
            prop.transform.localPosition = Vector3.zero;
            prop.transform.localScale = Vector3.one;
             var conpnent = prop.GetComponent<LevelInfoPlanleProp>();

            bool isLock = propConfigur.ConsumeProp[i].unLosckLevel <= PlayerData.instance.GetUnLockLevel()  ? true : false;
            conpnent.SetUnLockStart(isLock);
            conpnent.UnLockLevle = propConfigur.ConsumeProp[i].unLosckLevel;
            conpnent.IconImg.sprite = propConfigur.ConsumeProp[i].Icon;
            conpnent.ConsumePropType = propConfigur.ConsumeProp[i].comsumePropType;
            conpnent.LevelInfoPlane = this;
            conpnent.PanelParent = this.gameObject;
            conpnent.PropShowType = PropShowType.LevelInfoPanel;
            if (PlayerData.consumePropDataParameter.consumePropDataDict.ContainsKey(conpnent.ConsumePropType.ToString()))
            {
                conpnent.DbNumber = int.Parse(PlayerData.consumePropDataParameter.consumePropDataDict[conpnent.ConsumePropType.ToString()]);
            }
            conpnent.SsetPropShow();
            consumePropList.Add(conpnent);
        }

    }

    public override void CloseUiWo()
    {

        base.CloseUiWo();
    }

    private void OpenInit()
    {
       // PlayerData.instance.currSelectLevel = PlayerData.playerDataParame.maxLevel;
        PlayerData.instance.currSelectLevel = 1;
        string str = ForMatString(PlayerData.instance.currSelectLevel + 1);
        textLevel.text = "关卡" + str;
        int leveCount = LoadConfigursManager.instans.LevelDataList.Count;
    }


    /// <summary>
    /// 开始游戏
    /// </summary>
    public void ClickStartGame()
    {
        uiManager.StartHomeManager.PlayGameLevelCount++;
     
            if (PlayerData.playerDataParame.energy > 0)
            {
                PlayerData.playerDataParame.energy--;
            }
            else
            {
                string str = "当前体力值为零，不能开始游戏！";
                uiManager.TipDlgManager.OpenTipDlg(TipDlgType.SamllTipDlg, this, str);
                return;
            }
      
       
        for (int i=0; i<consumePropList.Count; i++)
        {
            if (consumePropList[i].IsSelcet)
            {
                Debug.Log("选择道具！  " + consumePropList[i].ConsumePropType);
                PlayerData.instance.currSelectPropList.Add(consumePropList[i].ConsumePropType);
                var temp = int.Parse( PlayerData.consumePropDataParameter.consumePropDataDict[consumePropList[i].ConsumePropType.ToString()]) - 1;
                PlayerData.consumePropDataParameter.consumePropDataDict[consumePropList[i].ConsumePropType.ToString()] = temp.ToString();
                
                if (consumePropList[i].ConsumePropType == ConsumePropType.AutomaticSubmitFood)
                    PlayerData.instance.currSingleLevelData.automaticSubmitProp = true;
                
                //每日成就数据
                string strName  = "";
                strName = PlayerData.instance.dailyTaskDataValue.usePropCount;
                PlayerData.instance.dailyTaksValueDit[strName]++;
                if (consumePropList[i].ConsumePropType == ConsumePropType.MomentFood)
                {
                    strName = PlayerData.instance.dailyTaskDataValue.useMomentFoodPropCount;
                }
                else if (consumePropList[i].ConsumePropType == ConsumePropType.FireFood)
                {
                    strName = PlayerData.instance.dailyTaskDataValue.useFireFoodPropCount;
                }
                else if (consumePropList[i].ConsumePropType == ConsumePropType.GuestPatience)
                {
                    strName = PlayerData.instance.dailyTaskDataValue.useGuestPatiencePropCount;
                }
                else if (consumePropList[i].ConsumePropType == ConsumePropType.AddGuest)
                {
                    strName =  PlayerData.instance.dailyTaskDataValue.useAddGuestCount;
                }
                else if (consumePropList[i].ConsumePropType == ConsumePropType.SetlementDoubleCion)
                {
                   strName =  PlayerData.instance.dailyTaskDataValue.useSetlementDoubleCionPropCount;
                }
                else if (consumePropList[i].ConsumePropType == ConsumePropType.AutomaticSubmitFood)
                {
                    strName = PlayerData.instance.dailyTaskDataValue.useAutomaticSubmitFoodCount;
                }
                if (strName != "" )
                {
                    PlayerData.instance.dailyTaksValueDit[strName]++;
                }
                

            }
        }

        PlayerData.instance.keyIsFlightAnimation =  keyIsFlightAnimation;
       PlayerData.instance.isKeySpanRestaution =  isKeySpanRestaution;
        PlayerData.instance.previousLevelID =  previousLevelID;
        PlayerData.instance.fliykeyCount =  fliykeyCount;

        PlayerData.instance.PlayerDataParameSave();
        Debug.Log("当前关卡    " + PlayerData.instance.currPlayeLevel);
#if UNITY_EDITOR
        var daata = JsonMapper.ToJson(PlayerData.consumePropDataParameter.consumePropDataDict);
        Debug.Log("保存道具数据    " + daata);
        CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("consumeProp  ", daata));
#elif !UNITY_EDITOR && UNITY_WEBGL
        var daata = JsonMapper.ToJson(PlayerData.consumePropDataParameter);
        Debug.Log("保存道具数据    " + daata);
       CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("UpdateConsumeProp", daata));
#endif
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("Game", UnityEngine.SceneManagement.LoadSceneMode.Single);
   
    }

    public void ClickExist()
    {
        uiManager.CloseLevelInfoPlane();
        this.CloseUiWo();
    }

    private void InitTipDlgOne(int index)
    {
        for (int i=0; i< upGraedAryBtn.Length; i++)
        {
            if (i == index)
            {
                upGraedAryBtn[i].interactable = false;
                aryUpGreadParent[i].SetActive(true);
                showPlaneNode[i].gameObject.SetActive(true);
                selectButImg.transform.SetParent(upGraedAryBtn[i].transform);
                selectButImg.transform.DOLocalMoveX(0, 0.1f).SetEase(Ease.OutSine);
                selectButImg.transform.SetSiblingIndex(0);
                if (index == 1)
                {
                    FoodRecomendUp();
                }
                else if (index == 2)
                {
                    DeviceRecomendUp();
                }
       
            }
            else
            {
                upGraedAryBtn[i].interactable = true;
                aryUpGreadParent[i].SetActive(false);
                showPlaneNode[i].gameObject.SetActive(false);
            }
        }
        textLevel.text = 1.ToString();

        progressCountPos = maxCountImg.transform.parent.localPosition;
    }

   
    public void OnClilkUpGread( int index )
    {
       InitTipDlgOne(index);
    }

    public void SetShowLevelText(int lvelNum)
    {
        Debug.Log("lvelNum" + lvelNum);
        var component = levlItamList[lvelNum].GetComponent<LevelSelect>();

        //每次滚动
        if (IsSlider(lvelNum))
        {
            var displayIndex = lvelNum / 3;
            var totallCount = levlItamList.Count / 3;
           displayIndex = displayIndex > totallCount - 2 ? displayIndex + 1 : displayIndex;
            lvelSlidetScroll.SlidetViweToElementWayTwo(displayIndex);
        }
        var leveList = LoadConfigursManager.instans.LevelShopDataDuct[component.LevelName];
    
        PlayerData.instance.currSelectLevel = lvelNum+1;
        textLevel.text = "关卡" + ForMatString(lvelNum + 1);

        SetLevelInfor(leveList[lvelNum]);
        RecommendUp(leveList[lvelNum]);
        currLevel = leveList[lvelNum];


        for (int i=0; i<levlItamList.Count; i++)
        {
            if (i != lvelNum) 
            {
                levlItamList[i].GetComponent<LevelSelect>().SelectImg.gameObject.SetActive(false);
            }
            else
            {
                levlItamList[i].GetComponent<LevelSelect>().SelectImg.gameObject.SetActive(true);
                PlayerData.instance.currLevelName = levlItamList[i].GetComponent<LevelSelect>().LevelName;
                PlayerData.instance.currLevelPlayOnce = levlItamList[i].GetComponent<LevelSelect>().PlayOnce;
                degreeText.text = FormatDegree(levlItamList[i].GetComponent<LevelSelect>().PlayOnce);
                var playOnce = levlItamList[i].GetComponent<LevelSelect>().PlayOnce;
               
                if (playOnce! >= 3)
                {
                    skipLevelpBtn.interactable = false;
                    seekBtn.gameObject.SetActive(true);
                }
 
                else if(playOnce < 3)
                {
                    skipLevelpBtn.interactable = true;
                    seekBtn.gameObject.SetActive(false);
                }


            } 
        }
    }
    private void SetLevelInfor(LevelData level)
    {

        PlayerData.instance.currPlayeLevel = level.levelID;
        PlayerData.instance.CurrLevelPrizeCionNumber = level.prizeCionNumber;
        PlayerData.instance.CurrLevelPrizeKeyNumber = level.prizeKeyNumber;

        keyIsFlightAnimation = level.prizeKeyNumber > 0 ? true : false;
        var checkKey = PlayerData.playerDataParame.keyNumber + level.prizeKeyNumber;

        if (CheckRestaurentKeyObtainCompelet(checkKey))
        {
            isKeySpanRestaution = true;
        }
        previousLevelID = level.levelID;
        fliykeyCount = level.prizeKeyNumber;

        if (level.cionTarget != 0)
        {
            PlayerData.instance.targetType = TargetType.CIonTraget;
            PlayerData.instance.targerCount = level.cionTarget;
            targetImg[0].gameObject.SetActive(true);
            targetImg[1].gameObject.SetActive(false);
            targetImg[2].gameObject.SetActive(false);
            tragetText.text = level.cionTarget.ToString();
        }
        else if (level.compeletFoodNumber != 0)
        {
            PlayerData.instance.targetType = TargetType.CompeletFood;
            PlayerData.instance.targerCount = level.compeletFoodNumber;
            targetImg[0].gameObject.SetActive(false);
            targetImg[1].gameObject.SetActive(true);
            targetImg[2].gameObject.SetActive(false);
            tragetText.text = level.compeletFoodNumber.ToString();
        }
        else if (level.goodTarget != 0)
        {
            PlayerData.instance.targetType = TargetType.GoodCommend;
            PlayerData.instance.targerCount = level.goodTarget;
            targetImg[0].gameObject.SetActive(false);
            targetImg[1].gameObject.SetActive(false);
            targetImg[2].gameObject.SetActive(true);
            tragetText.text = level.goodTarget.ToString();
        }
        if (level.gustCount != 0)
        {
            PlayerData.instance.limitType = LimitType.GustCountLimit;
            PlayerData.instance.limitCount = level.gustCount;
        }
        else if (level.levelCompeletTime != 0)
        {
            PlayerData.instance.limitType = LimitType.TimeLimit;
            PlayerData.instance.limitCount = level.levelCompeletTime;
        }
    }
    public void ByConsumeProp()
    {
        buyPropClose = true;
        this.CloseUiWo();
       
    }
    public override void CloseEnd()
    {
        if (buyPropClose)
            uiManager.FastOpenShopDlg(gameObject);
        uiManager.RefreshPanel();
    }

    public void UpdataConsumeProp()
    {
         for (int i=0; i< consumePropList.Count; i++)
         {
            var propName = consumePropList[i].ConsumePropType.ToString();
            if (PlayerData.consumePropDataParameter.consumePropDataDict.ContainsKey(propName))
            {
                consumePropList[i].DbNumber = int.Parse(PlayerData.consumePropDataParameter.consumePropDataDict[propName]);
            }
            consumePropList[i].SsetPropShow();
         }
    }
    public void CallSeletDevice(int index)
    {

        for (int i = 0; i < deviceList.Count; i++)
        {
            if (index == i)
            {
                deviceList[i].SelectImg.gameObject.SetActive(true);
                deviceList[i].IsSelect = true;
            }
            else
            {
                deviceList[i].SelectImg.gameObject.SetActive(false);
                deviceList[i].IsSelect = false;
            }
        }

            var selectDevice = deviceList[index];

        var animation = showDeviceUp.GetComponent<ZoomInAndOutAnimation>();
        if (animation != null)
        {
            animation.PlayerAnimation(0.2f, () =>
            {
                showDeviceUp.DeviceImg.sprite = selectDevice.DeviceImg.sprite;
                showDeviceUp.DeviceImgList = selectDevice.DeviceImgList;
                showDeviceUp.CurrDeviceGrade = selectDevice.CurrDeviceGrade;
                showDeviceUp.IsLock = selectDevice.IsLock;
                showDeviceUp.UpdataShow();
            });
        }

            deviceNameText.text = selectDevice.Desrible;
            currCountText.text = (selectDevice.MinCount + selectDevice.CurrDeviceGrade -1).ToString();
            maxCountText.text = (selectDevice.MaxCount).ToString();

            currMakeTimeText.text = (selectDevice.MakeTime - 2 + selectDevice.CurrDeviceGrade -1).ToString(); 
            maxMakeTimeText.text = selectDevice.MakeTime.ToString();

            deviceUpText.text = "-"+ selectDevice.DeviceUpPrice;

            maxCountImg.fillAmount = (selectDevice.CurrDeviceGrade - 1) / (float)(selectDevice.MaxCount - selectDevice.MinCount);
            makeTimeImg.fillAmount = (selectDevice.CurrDeviceGrade - 1) / (float)2;
            selectUpDeviceIndex = index;
            if (selectDevice.MakeTime == 0)
            {
                maxMakeTimeText.transform.parent.parent.gameObject.SetActive(false);
                maxCountImg.transform.parent.localPosition = new Vector3(progressCountPos.x,-10f, progressCountPos.z);
            }
            else
            {
                maxMakeTimeText.transform.parent.parent.gameObject.SetActive(true);
                maxCountImg.transform.parent.localPosition = new Vector3(progressCountPos.x, 40f, progressCountPos.z);
         
            }
    }

     public void OnClickDeviceUp()
    {
        bool isFind = false;
        var selectDevice = deviceList[selectUpDeviceIndex];
        if (selectDevice.CurrDeviceGrade >= 3)
            return;
        if ((PlayerData.playerDataParame.cions - selectDevice.DeviceUpPrice) < 0)
        {
            string str = "金币不足无法升级！";
            uiManager.TipDlgManager.OpenTipDlg(TipDlgType.SamllTipDlg, this, str);
            return;
        }
        
            PlayerData.playerDataParame.cions -= selectDevice.DeviceUpPrice;
            PlayerData.instance.achieveParmeter.comsumeCionCount += selectDevice.DeviceUpPrice;
            PlayerData.instance.achieveParmeter.comsumeCionCount += selectDevice.DeviceUpPrice;

            string strName = PlayerData.instance.dailyTaskDataValue.consumeCion;
            PlayerData.instance.dailyTaksValueDit[strName] += selectDevice.DeviceUpPrice;

            if (PlayerData.deviceDataParameter.deviceDict.ContainsKey(selectDevice.DeviceName))
            {
                var temp = int.Parse(PlayerData.deviceDataParameter.deviceDict[selectDevice.DeviceName]) + 1;
                temp = temp > 3 ? 3 : temp;
                PlayerData.deviceDataParameter.deviceDict[selectDevice.DeviceName] = temp.ToString();
            }
           else if (!PlayerData.deviceDataParameter.deviceDict.ContainsKey(selectDevice.DeviceName))
            {
                PlayerData.deviceDataParameter.deviceDict.Add(selectDevice.DeviceName,2.ToString());

            }
            selectDevice.CurrDeviceGrade++;
            if (selectDevice.CurrDeviceGrade > 3)
                selectDevice.CurrDeviceGrade = 3;

        
        var animation = showDeviceUp.GetComponent<ZoomInAndOutAnimation>();
        if (animation != null)
        {
            animation.PlayerAnimation(0.2f, () =>
            {

                currCountText.text = (selectDevice.MinCount + selectDevice.CurrDeviceGrade - 1).ToString();
                maxCountImg.fillAmount = (selectDevice.CurrDeviceGrade - 1) / (float)(selectDevice.MaxCount - selectDevice.MinCount);

                currMakeTimeText.text = (selectDevice.MakeTime - 2 + selectDevice.CurrDeviceGrade - 1).ToString();
                makeTimeImg.fillAmount = (selectDevice.CurrDeviceGrade - 1) / (float)2;

                showDeviceUp.CurrDeviceGrade = selectDevice.CurrDeviceGrade;
                showDeviceUp.UpdataShow();
                selectDevice.UpdataShow();
            });
        }

            PlayerData.instance.PlayerDataParameSave();
#if UNITY_EDITOR
            var data = JsonMapper.ToJson(PlayerData.deviceDataParameter);
            Debug.Log("设备升级 " + data);
            CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("DeviceData", data));
#elif !UNITY_EDITOR && UNITY_WEBGL
            var data = JsonMapper.ToJson(PlayerData.deviceDataParameter);
           CoroutineHandler.StartStaticCoroutine( PlayerData.instance.SaveData("UpdateDevice", data));
#endif
            RecommendUp(currLevel);


     }
     public void CallSelectFood(int index)
    {
        for (int i=0; i<foodItemList.Count; i++)
        {
            if (i == index)
                foodItemList[i].SelectImg.gameObject.SetActive(true);
            else
                foodItemList[i].SelectImg.gameObject.SetActive(false);

        }
        var selectShowFood = foodItemList[index];

        var animation = currSelectUpFood.GetComponent<ZoomInAndOutAnimation>();
        if (animation != null)
        {
            animation.PlayerAnimation(0.2f,()=> {

                currSelectUpFood.FoodImg.sprite = selectShowFood.FoodImg.sprite;
                currSelectUpFood.FoodGradeImgList = selectShowFood.FoodGradeImgList;
                currSelectUpFood.CurrFoodGrade = selectShowFood.CurrFoodGrade;
                currSelectUpFood.IsLock = selectShowFood.IsLock;
                currSelectUpFood.updateFoodGrade();
            });
        }
        
        foodNameText.text = selectShowFood.DisribleText;
        currPriceText.text = (selectShowFood.CurrFoodPrice + 2 + (selectShowFood.CurrFoodGrade - 2 - 1)).ToString();
        maxPriceText.text = (selectShowFood.CurrFoodPrice + 2).ToString();

        foodProgesValue.fillAmount = (float)( selectShowFood.CurrFoodGrade -1) / (float) 2;
        upFoodNeedCionText.text = "-" + selectShowFood.UpFoodPrive.ToString();
        selectFoodUpIndex = index;
     }

    public void OnClickFoodUp()
    {
        var selectFood = foodItemList[selectFoodUpIndex];
        if (selectFood.CurrFoodGrade >= 3)
            return;

        if (PlayerData.playerDataParame.cions - selectFood.UpFoodPrive < 0)
        {
            string str = "金币不足无法升级！";
            uiManager.TipDlgManager.OpenTipDlg(TipDlgType.SamllTipDlg, this, str);
            return;
        }
        

        PlayerData.playerDataParame.cions -= selectFood.UpFoodPrive;
        PlayerData.instance.achieveParmeter.comsumeCionCount += selectFood.UpFoodPrive;
        PlayerData.instance.achieveParmeter.comsumeCionCount += selectFood.UpFoodPrive;

        var strName = PlayerData.instance.dailyTaskDataValue.consumeCion;
        PlayerData.instance.dailyTaksValueDit[strName] += selectFood.UpFoodPrive;

        if (PlayerData.foodDataPramerer.foodDataDict.ContainsKey(selectFood.FoodName))
        {
             var temp = int.Parse( PlayerData.foodDataPramerer.foodDataDict[selectFood.FoodName]) +1;
            temp = temp > 3 ? 3 : temp;
            PlayerData.foodDataPramerer.foodDataDict[selectFood.FoodName] = temp.ToString();
        }
        else if (!PlayerData.foodDataPramerer.foodDataDict.ContainsKey(selectFood.FoodName))
        {
            PlayerData.foodDataPramerer.foodDataDict.Add(selectFood.FoodName,2.ToString());
        }

        var data = JsonMapper.ToJson(PlayerData.foodDataPramerer);

#if UNITY_EDITOR
      CoroutineHandler.StartStaticCoroutine( PlayerData.instance.SaveData("FoodUpData", data));
#elif !UNITY_EDITOR && UNITY_WEBGL
             CoroutineHandler.StartStaticCoroutine( PlayerData.instance.SaveData("UpdateFood", data));
#endif
        PlayerData.instance.PlayerDataParameSave();
        selectFood.CurrFoodGrade++;
        if (selectFood.CurrFoodGrade > 3)
            selectFood.CurrFoodGrade = 3;

        var animation = currSelectUpFood.GetComponent<ZoomInAndOutAnimation>();
        if (animation != null)
        {
            animation.PlayerAnimation(0.2f, () => {
                foodProgesValue.fillAmount = (float)(selectFood.CurrFoodGrade - 1) / (float)2;
                currPriceText.text = (selectFood.CurrFoodPrice + selectFood.CurrFoodGrade - 1).ToString();
                selectFood.updateFoodGrade();
                currSelectUpFood.CurrFoodGrade = selectFood.CurrFoodGrade;
                currSelectUpFood.updateFoodGrade();
            });
        }
        

        RecommendUp(currLevel);
    }

    //试用升级
    public void OnClickTryUseUpFood()
    {
        var selectFood = foodItemList[selectFoodUpIndex];
        if (selectFood.CurrFoodGrade >= 3)
            return;

        //试用升级

        var curfoodTry = PlayerData.instance.currSingleLevelData.foodTyeUpDit;
        if (curfoodTry.ContainsKey(selectFood.FoodName))
        {
            var temp = int.Parse(curfoodTry[selectFood.FoodName]) + 1;
            temp = temp > 3 ? 3 : temp;
            curfoodTry[selectFood.FoodName] = temp.ToString();
        }
        else
        {
            curfoodTry.Add(selectFood.FoodName, 2.ToString());
        }

        currSelectUpFood.updateFoodGrade();

        selectFood.CurrFoodGrade++;
        if (selectFood.CurrFoodGrade > 3)
            selectFood.CurrFoodGrade = 3;
        currSelectUpFood.CurrFoodGrade = selectFood.CurrFoodGrade;
        foodProgesValue.fillAmount = (float)(selectFood.CurrFoodGrade - 1) / (float)2;
        currPriceText.text = (selectFood.CurrFoodPrice + selectFood.CurrFoodGrade - 1).ToString();
        selectFood.updateFoodGrade();
        currSelectUpFood.updateFoodGrade();

    }
    /// <summary>
    /// 教学引导
    /// </summary>
    /// <param name="msg"></param>
    public void TeachintMsgOperation(EventTeachingMsg msg)
    {
        if (msg.teachingType == TeachingType.SelectLevelClick)
               CoroutineHandler.StartStaticCoroutine( TeachingStartGame());

       else if (msg.teachingType == TeachingType.StartGameClick)
        {
            ClickStartGame();
        }
    }

    private IEnumerator TeachingStartGame()
    {
        yield return new WaitForSeconds(0.5f);
        if (PlayerData.instance.teachingValue.startGameClick == 0)
            uiManager.TeachingOperation(TeachingType.StartGameClick);
    }

    private void SelectLevelTeaching()
    {
        if (PlayerData.instance.teachingValue.selectLevelClick == 0)
            uiManager.TeachingOperation(TeachingType.SelectLevelClick);
    }
    private void DeviceUpGradeTeaching()
    {
        if (PlayerData.instance.teachingValue.deveiceUpGradeClick == 0 && unLockLevelCount == 6 && !isDeveiceUpGrade)
            uiManager.TeachingOperation(TeachingType.DviceUpGradeOne);
    }
    private void FoodUpGradeTeaching()
    {
        if (PlayerData.instance.teachingValue.foodUpGradeClick == 0 && unLockLevelCount == 4 && !isFoodUoGrade)
            uiManager.TeachingOperation(TeachingType.FoodUpGradeOne);

    }
    private bool GetLevelUnLock(int level)
    {
        Debug.Log("检测等级    " + level);
        if ((level-1) > levlItamList.Count || (level - 1) < 0)
            return false;

        var component = levlItamList[level - 1].GetComponent<LevelSelect>();
        if (component != null)
            return component.IsLock;
        return false;
    }
    private List<LevelData> GetLevelDataList(int grade)
    {
        if (grade == 0)
        {
            Debug.LogError("关卡论数错误！");
            return null;
        }
        levelTableList.Clear();
        var shop = LoadConfigursManager.instans.ShopDict[PlayerData.instance.curPlayeShop];
        var levelName = shop.leveData + "_" + grade;
        Debug.Log("levelName  " + levelName);
        return LoadConfigursManager.instans.LevelShopDataDuct[levelName];
    } 
    private string ForMatString(int number)
    {
        if (number < 10)
            return 0.ToString() + number.ToString();
        else
            return number.ToString();
    }
    private string FormatDegree(int number)
    {
        if (number == 0)
            return "简单";
        else if (number == 1)
            return "一般";
        else
            return "困难";
    }

    public void OnClickAboutPanle()
    {
        uiManager.TipDlgManager.OpenTipDlg(TipDlgType.PropIllustrate, this);
    }
    public void CallPropClick(bool isLock, int lockGrade)
    {
        if (!isLock)
        {
            string str = "该道具将在过关后1-" + lockGrade + "解锁";
            uiManager.TipDlgManager.OpenTipDlg(TipDlgType.SamllTipDlg, this, str);
        }
    }
   

    //跳过本关 UnlockRestaurentTip
    public void OnClickSkipLevel()
    {
        string msg = "确定使用钻石！";
        uiManager.TipDlgManager.OpenTipDlg(TipDlgType.DimaondCofirmTip, this, msg);
    }

    private IEnumerator SkipLevel()
    {

        yield return new WaitForSeconds(0.5f);
        if (PlayerData.playerDataParame.diamond - 50 < 0)
        {
            string str = "当前钻石数量不足，请购买！";
            uiManager.TipDlgManager.OpenTipDlg(TipDlgType.SamllTipDlg, this, str);
            yield break;
        }

        PlayerData.playerDataParame.diamond -= 100;
     
        var currLevel = PlayerData.instance.currSelectLevel;
        var currCompoent = levlItamList[PlayerData.instance.currSelectLevel - 1].GetComponent<LevelSelect>();
        var temp = (currCompoent.PlayOnce + 1) > 3 ? 3 : (currCompoent.PlayOnce + 1);

        var levelList = GetLevelDataList(temp);
        Debug.Log("获取成功！");

        if (currLevel <= levelList.Count)
        {
            if (currCompoent != null)
            {
                currCompoent.PlayOnce = 1;
                currCompoent.SetLevlGrup();
            }
            var lockComponent = levlItamList[PlayerData.instance.currSelectLevel - 1].GetComponent<LevelSelect>();

            if (lockComponent != null)
            {
                lockComponent.IsLock = true;
                lockComponent.SetLevlGrup();
            }
            //因为关卡从1开始   数组从0开始
            /// PlayerData.instance.currPlayeLevel = levelList[PlayerData.instance.currSelectLevel - 1].levelID;
           //保存关卡游玩次数
            if (!PlayerData.playOnceParme.levelPlayOnceDataDict.ContainsKey(PlayerData.instance.currPlayeLevel))
            {
                PlayerData.playOnceParme.levelPlayOnceDataDict[PlayerData.instance.currPlayeLevel] = 1.ToString();

            }
            else
            {
                var gurade = int.Parse(PlayerData.playOnceParme.levelPlayOnceDataDict[PlayerData.instance.currPlayeLevel]) + 1;
                PlayerData.playOnceParme.levelPlayOnceDataDict[PlayerData.instance.currPlayeLevel] = gurade.ToString();

            }
            Debug.Log("[PlayerData.instance.currPlayeLevel   " + PlayerData.instance.currPlayeLevel);
            Debug.Log("当前选择关卡   " + PlayerData.instance.currSelectLevel);
            PlayerData.playerDataParame.keyNumber += levelList[PlayerData.instance.currSelectLevel - 1].prizeKeyNumber;

#if UNITY_WEBGL && !UNITY_EDITOR
                var daat = JsonMapper.ToJson(PlayerData.playOnceParme);
              CoroutineHandler.StartStaticCoroutine(  PlayerData.instance.SaveData("UpdatePlayOnce", daat));
#elif UNITY_EDITOR

            var daat = JsonMapper.ToJson(PlayerData.playOnceParme.levelPlayOnceDataDict);
            CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("PlayeLevelOnce", daat));
#endif
            if (levelList[PlayerData.instance.currSelectLevel - 1].prizeKeyNumber > 0)
            {
          
                lockComponent.OnCklic();
                skipLevelRestaurentUnlock = false;
                TipDlgUnLockRestaurentMsg msgDlg = new TipDlgUnLockRestaurentMsg();
                msgDlg.tipDlgType = TipDlgType.SkipLevelDlg;
                msgDlg.keyCount = levelList[PlayerData.instance.currSelectLevel - 1].prizeKeyNumber;
                msgDlg.needKeyCount = 0;

                uiManager.TipDlgManager.OpenTipDlg(TipDlgType.SkipLevelDlg, this, null, msgDlg);
                PlayerData.instance.isDiamondSkip = true;
                PlayerData.instance.keyIsFlightAnimation = true;
                PlayerData.instance.isKeySpanRestaution = false;
                PlayerData.instance.previousLevelID = levelList[PlayerData.instance.currSelectLevel - 1].levelID;
                PlayerData.instance.fliykeyCount = levelList[PlayerData.instance.currSelectLevel - 1].prizeKeyNumber;


            }
            else
            {
                string str = "本官已跳过！";
                uiManager.TipDlgManager.OpenTipDlg(TipDlgType.SkipLevelNoKeyTip, this, str);
            }
            var b = CheckRestaurentKeyObtainCompelet();
            skipLevelpBtn.interactable = !b;
            PlayerData.instance.PlayerDataValueDiamondOprate(-100);
        }
        yield return null;
    }

    /// <summary>
    /// 跳过关卡弹窗回调
    /// </summary>
    /// <param name="type"></param>
    /// <param name="success"></param>
    public override void TipDlgExitCallBanck(TipDlg tipDlg,TipDlgType type, bool success)
    {
        if (type == TipDlgType.SkipLevelDlg)
        {//
              //  Debug.Log("获得按钮回调！");
              //钥匙全部获得完成
            if (CheckRestaurentKeyObtainCompelet())
            {
                CloseUiWo();
                return;
            }
            InitOpeDlg();
                CoroutineHandler.StartStaticCoroutine( KeyRunFlighet());

        }
        else if (type == TipDlgType.SkipLevelNoKeyTip)
        {
                InitOpeDlg();
        }
        else if (type == TipDlgType.DimaondCofirmTip)
        {
            CoroutineHandler.StartStaticCoroutine(SkipLevel());
        }
    }

    //跳过关卡刷新
    private void SkipLevelRefesh()
    {
        for (int i = 0; i < foodItemList.Count; i++)
        {
            foodItemList[i].IsLock = GetLevelUnLock(foodItemList[i].UnLockLevel) ? true : false;
            foodItemList[i].updateFoodGrade();
        }
        for (int i = 0; i < deviceList.Count; i++)
        {
            deviceList[i].IsLock = GetLevelUnLock(deviceList[i].UnLockLevel) ? true : false;
            deviceList[i].UpdataShow();
        }

    }

    /// <summary>
    /// 查找有钥匙的关卡
    /// </summary>
    /// <returns></returns>
    private int ObtainKeyLevel()
    {
        var leveList = GetLevelDataList(1); //先获取第一论
        var levelListTwo = GetLevelDataList(2); //第二轮
       var levelListThree = GetLevelDataList(3);//第三轮

        //  var lockComponent = levlItamList[PlayerData.instance.currSelectLevel-1].GetComponent<LevelSelect>();

        Debug.Log("配置长度   " + leveList.Count);
        Debug.Log("关卡长度   " + levlItamList.Count);

        if (leveList.Count != levlItamList.Count)
        {
            Debug.LogError("查找关卡关卡错误!");
            return -1;
        }

        for(int i=0; i< levlItamList.Count; i++)
        {
            var component = levlItamList[i].GetComponent<LevelSelect>();
            if (!component.IsLock)
                break;

           if (component.PlayOnce == 0)
            {
                if (leveList[i].prizeKeyNumber > 0)
                    return i;
            }
           else if (component.PlayOnce == 1)
            {
                if (levelListTwo[i].prizeKeyNumber > 0)
                    return i;
            }
            else if (component.PlayOnce == 2)
            {
                if (levelListThree[i].prizeKeyNumber > 0)
                    return i;
            }
        }


        return -1;
    }


    /// <summary>
    /// 查找钥匙关卡
    /// </summary>
    public void OnClickSeekLevel()
    {
        var seek = ObtainKeyLevel();
        if (seek == -1)
        {
            Debug.LogError("打开查找有钥匙关卡出错！");
        }
        SetShowLevelText(seek);
    }
    /// <summary>
    /// 检测当前餐厅钥匙全部获得完成
    /// </summary>
    private bool CheckRestaurentKeyObtainCompelet()
    {
        var shopDit = LoadConfigursManager.instans.ShopDict;
        List<string> keys = new List<string>(shopDit.Keys);
        var leveList = GetLevelDataList(1); //先获取第一论
        var levelListTwo = GetLevelDataList(2); //第二轮
        var levelListThree = GetLevelDataList(3);//第三轮

        if (leveList.Count != levelListTwo.Count || leveList.Count != levelListThree.Count)
        {
            Debug.LogError("轮次关卡配置长度不等！");
        }
            var keyCount = 0;
        for (int i=0; i< leveList.Count; i++)
        {
            keyCount += leveList[i].prizeKeyNumber;
            keyCount += levelListTwo[i].prizeKeyNumber;
            keyCount += levelListThree[i].prizeKeyNumber;

        }
        if (PlayerData.playerDataParame.keyNumber >= keyCount)
        {
            return true;
        }

        return false;
    }
    private bool CheckRestaurentKeyObtainCompelet(int checkKeyCount)
    {
        var shopDit = LoadConfigursManager.instans.ShopDict;
        List<string> keys = new List<string>(shopDit.Keys);
        var leveList = GetLevelDataList(1); //先获取第一论
        var levelListTwo = GetLevelDataList(2); //第二轮
        var levelListThree = GetLevelDataList(3);//第三轮

        if (leveList.Count != levelListTwo.Count || leveList.Count != levelListThree.Count)
        {
            Debug.LogError("轮次关卡配置长度不等！");
        }
        var keyCount = 0;
        for (int i = 0; i < leveList.Count; i++)
        {
            keyCount += leveList[i].prizeKeyNumber;
            keyCount += levelListTwo[i].prizeKeyNumber;
            keyCount += levelListThree[i].prizeKeyNumber;

        }
        if (checkKeyCount >= keyCount)
        {
            return true;
        }

        return false;
    }
    // 3 140 
    // 4 300

    private bool IsSlider(int index)
    {
        var temp = index / 3 + 1;
        var totallCount = levlItamList.Count / 3;
       // Debug.Log("当前架子数量   " + temp);
       // Debug.Log("总架子数量   " + totallCount);
        if (temp > 2  )
            return true;

        return false;
    }
    /// <summary>
    /// 建议升级
    /// </summary>
    /// <param name="level"></param>
    private void RecommendUp(LevelData level)
    {
       // Debug.Log("提示建议操作1");
        recommendUp.gameObject.SetActive(false);
        if (level == null)
            return;
       
        if (level.recommendUpType == 0)
            return;
     
        if (level.recomenUpdName == "null")
            return;
        var foodDit = PlayerData.foodDataPramerer.foodDataDict;
        var deviceDit = PlayerData.deviceDataParameter.deviceDict;
        if (foodDit == null)
            return;
        if (deviceDit == null)
            return;
     
        if (level.recommendUpType == 1) //食物提示
        {
            int grade = 0;
            if (foodDit.ContainsKey(level.recomenUpdName))
            {
                grade = int.Parse(foodDit[level.recomenUpdName]);
            }
           // Debug.Log("当前等级   " + grade);
           // Debug.Log("建议升级到   " + level.toUpGrade);
            if (grade != level.toUpGrade)
            {
                recommendUp.gameObject.SetActive(true);
                var posx = upGraedAryBtn[1].transform.position.x;
                var posy = recommendUp.transform.position.y;
                recommendUp.transform.position = new Vector3(posx, posy, 0);
            }
        }
        else if (level.recommendUpType == 2)    //设别提示
        {
            int grade = 0;
            if (deviceDit.ContainsKey(level.recomenUpdName))
            {
                grade = int.Parse(deviceDit[level.recomenUpdName]);
            }
            if (grade != level.toUpGrade)
            {
                recommendUp.gameObject.SetActive(true);
                   var posx = upGraedAryBtn[2].transform.position.x;
                var posy = recommendUp.transform.position.y;
                recommendUp.transform.position = new Vector3(posx, posy, 0);
            }
        }
    }
    /// <summary>
    /// 食物建议升级
    /// </summary>
    private void FoodRecomendUp()
    {
        if (recommendUp.gameObject.activeSelf == false)
            return;
        if (currLevel == null)
            return;
        if (currLevel.recommendUpType == 0)
            return;
        LevelInfoFoodUp foodUp = null;
        for (int i = 0; i < foodItemList.Count; i++)
        {
            if (currLevel.recomenUpdName == foodItemList[i].FoodName)
            {
                foodUp = foodItemList[i];
                break;
            }
        }
        if (foodUp == null)
            return;
        Button item = upGraedAryBtn[1];
        var moveIcon = Instantiate(flightFoodUpPrefab);
        moveIcon.transform.SetParent(item.transform.parent);
        moveIcon.transform.position = item.transform.position;
        moveIcon.transform.localScale = Vector3.one ;
        moveIcon.gameObject.AddComponent<FlightAnimation>();
        moveIcon.GetComponent<FlightAnimation>().Flight(foodUp.transform, 0.5f,() => {
            Destroy(moveIcon);
        });
    }
    /// <summary>
    /// 建议升级
    /// </summary>
    private void DeviceRecomendUp()
    {
        if (recommendUp.gameObject.activeSelf == false)
            return;
        if (currLevel.recommendUpType == 0)
            return;
        LevelInforDeviceUp deviceUp = null;
        for (int i = 0; i < deviceList.Count; i++)
        {
            if (currLevel.recomenUpdName == deviceList[i].DeviceName)
            {
                deviceUp = deviceList[i];
                break;
            }
        }

        if (deviceUp == null)
            return;
        Debug.Log("设别建议升级  ");
        Button item = upGraedAryBtn[2];
        var moveIcon = Instantiate(flightDeviceUpPrefab);
        moveIcon.transform.SetParent(item.transform.parent);
        moveIcon.transform.position = item.transform.position;
        moveIcon.transform.localScale = Vector3.one;
        moveIcon.gameObject.AddComponent<FlightAnimation>();
        moveIcon.GetComponent<FlightAnimation>().Flight(deviceUp.transform, 0.5f, () => {
            Destroy(moveIcon);
        });
    }
    /// <summary>
    /// 土建道具使用
    /// </summary>
    private void RecommendPropUse()
    {
        if (currLevel == null)
        {
            Debug.Log("currLevel null!");
            return;
        }
        Debug.Log("土建使用道具     " + currLevel.recomendPropType);
        if (currLevel.recomendPropType == ConsumePropType.Null)
            return;

         for (int i=0; i<consumePropList.Count; i++)
        {
            if (currLevel.recomendPropType == consumePropList[i].ConsumePropType)
            {
                Debug.Log("土建使用道具显示");
                consumePropList[i].IsReCommend = true;
                consumePropList[i].SsetPropShow();
                break;

            }
  
        }
    }
  
}
