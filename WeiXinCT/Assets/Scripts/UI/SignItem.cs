using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;



public enum SignItemState : int
{
    Null,
    NoReceive,
    CanReceive,
    ReceiveCompelet,
}

public class SignItem : MonoBehaviour
{
    [SerializeField]
    SignPlanel signPlanel;

    [SerializeField]
    private Image compeletImg;

    [SerializeField]
    private Image itmeBgImg;
    [SerializeField]
    private Image itemInImg;

    [SerializeField]
    private Sprite[] canReceiveSprite;
    [SerializeField]
    private PrizeComponent prize;
    [SerializeField]
    private GhostFlightAnimation animationEffice;
    [SerializeField]
    private DiffuseClosureAnimation deffuseAniamtion;
    [SerializeField]
    private GameObject cionPrefab;
    [SerializeField]
    private Image prizeImg;
    [SerializeField]
    private Transform ligthEffice;

    private int signIndex;
    private SignItemState signItemStart;


   // private PrizeType prizeTyep;

    public Image CompeletImg { get => compeletImg; }
    public SignItemState SignItemStart { get => signItemStart; }

    public SignPlanel SignPlanel { set => signPlanel = value; }

    public int SignIndex { set => signIndex = value; }

    public PrizeComponent  Prize { get => prize; }

    public void OnClick()
    {
       if (signItemStart == SignItemState.CanReceive)
        {
     
            signPlanel.CallOnClick(signIndex);
            this.transform.GetComponent<Button>().interactable = false;
            /// SetCanReceive(SignItemState.ReceiveCompelet);
        }
    }
    /// <summary>
    /// 金币
    /// </summary>
    /// <param name="target"></param>
    /// <param name="back"></param>
    public void PlayObtainAnimation(Transform target,Action back = null)
    {
        if (prize.prizeType == PrizeType.Cion)
        {
            Debug.Log("播放动画！2" + target);
            if (target == null)
                return;
            if (animationEffice == null)
                return;
      
            animationEffice.PlayFlightCionAnimation(6, target, () => {
                SetCanReceive(SignItemState.ReceiveCompelet);
                if (back != null)
                    back.Invoke();
            });
        }
        else if (prize.prizeType == PrizeType.ComsumeProp)
        {
            if (deffuseAniamtion == null)
                return;
             CoroutineHandler.StartStaticCoroutine( deffuseAniamtion.PlayAnimation(prizeImg.gameObject, () =>
             {
                 SetCanReceive(SignItemState.ReceiveCompelet);

             }));
        }
    }
    /// <summary>
    /// 道具
    /// </summary>
    /// <param name="state"></param>
    /// 
    public void PlayObtainParopAniamtion()
    {

    }


    public void SetCanReceive(SignItemState state)
    {
        if (state == SignItemState.CanReceive)
        {
            itmeBgImg.sprite = canReceiveSprite[2];
            itemInImg.sprite = canReceiveSprite[3];
            signItemStart = SignItemState.CanReceive;
            ligthEffice.gameObject.SetActive(true);

        }
        else if(state == SignItemState.NoReceive)
        {
            itmeBgImg.sprite = canReceiveSprite[0];
            itemInImg.sprite = canReceiveSprite[1];
            signItemStart = SignItemState.NoReceive;
            ligthEffice.gameObject.SetActive(false);
        }
        else if (state == SignItemState.ReceiveCompelet)
        {
            compeletImg.gameObject.SetActive(true);
              signItemStart = SignItemState.ReceiveCompelet;
            ligthEffice.gameObject.SetActive(false);
        }
    }
}
