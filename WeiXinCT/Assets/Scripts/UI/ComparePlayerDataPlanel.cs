using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// 本地与远端比较
/// </summary>

public class ComparePlayerDataPlanel : UiWoPropAbility
{
    [SerializeField]
    private TextMeshProUGUI[] cionText;

    

    [SerializeField]
    private TextMeshProUGUI[]  gemText;

    private CloundSavePlayerDaata cloundPlayerData;

    private HomeMapUiManager uiManager;
    public void Confgur(HomeMapUiManager uiManager)
    {
        this.uiManager = uiManager;
    }
    public void SetShowDaata(CloundSavePlayerDaata data)
    {
        cloundPlayerData = data;
        cionText[0].text = "金币  " + PlayerData.playerDataParame.cions.ToString();
        cionText[1].text = "金币  " + data.cion.ToString();

        gemText[0].text = "宝石  " + PlayerData.playerDataParame.diamond.ToString();
        gemText[1].text = "宝石  " + data.gem.ToString();

    }

    public void OnClose()
    {
        CloseUiWo();
    }
   public void OnClickLocadData()
    {
        SaveCloundData();
    }
    public void OnClickCloundData()
    {
        PlayerData.playerDataParame.cions = cloundPlayerData.cion;
       PlayerData.playerDataParame.diamond = cloundPlayerData.gem;
        PlayerData.instance.PlayerDataParameSave();
        CloseUiWo();
       // uiManager.MapHome.UpdataShowData();
    }

    public async void SaveCloundData()
    {
        CloundSavePlayerDaata cloundData = new CloundSavePlayerDaata();
        cloundData.cion = PlayerData.playerDataParame.cions;
        cloundData.gem = PlayerData.playerDataParame.diamond;
        await CloudSaveSystem.instans.ForceSaveObjectData("CloundSavePlayerDaata", cloundData);
       // uiManager.MapHome.UpdataShowData();

        CloseUiWo();
    }

}
