using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
public class ShopItemCell : MonoBehaviour
{
    private ShoppPlanel shopPlanel;
    public Image propImage;
    public TextMeshProUGUI priceText;

    [SerializeField]
    private Image buttnImg;
    [SerializeField]
    private Image gemImg;
    [SerializeField]
    private Image lockImg;
    [SerializeField]
    private Material material;
    [SerializeField]
    private TextMeshProUGUI describeText;
    [SerializeField]
    private Transform startConment;
    [SerializeField]
    private Transform[] jumpTarget;
    [SerializeField]
    private Transform endPostion;

    public int cellIndex = -1;
    private bool isLock;
    private int lockCount = 0;

    public ConsumePropType propType = ConsumePropType.Null;

    public int buyNumber = 1;
    private float rotate = 0;

    private List<GameObject> flightAnimationList = new List<GameObject>();
   
    public ShoppPlanel ShoppPlanel { get => shopPlanel; set => shopPlanel = value; }
    public TextMeshProUGUI DescribeText { get => describeText; }
    public int LockCount { get => lockCount; set => lockCount = value; }
    public void SetUnLockStart(bool isLock)
    {
       this.isLock = isLock;
        if (isLock)
        {
            lockImg.gameObject.SetActive(false);
            buttnImg.material = null;
            propImage.material = null;
            gemImg.material = null;
            buttnImg.GetComponent<Button>().interactable = true;
        }
        else
        {
            lockImg.gameObject.SetActive(true);
            buttnImg.material = material;
            propImage.material = material;
            gemImg.material = material;
            buttnImg.GetComponent<Button>().interactable = false;

        }
    }


    public void OnClickOk()
    {


        var proce = int.Parse(priceText.text.ToString());
        if (PlayerData.playerDataParame.diamond - proce >= 0 )
        {
           // PlayerData.playerDataParame.diamond -= proce;
          //  shopPlanel.PlayerDataValueOprate(-proce);
                shopPlanel.CallPropBut(cellIndex);
            buttnImg.GetComponent<Button>().interactable = false;
        }
        else
        {

            shopPlanel.CallBuyPropFail();
        }

    }
    public void OnClickIcon()
    {
        shopPlanel.PropIntroduce(isLock,lockCount );
    }


    //动画效果
    public IEnumerator PlayAnimation()
    {
       yield return new WaitForSeconds(0.5f);
      rotate += Random.Range(0.0f,360f);
        endPostion.parent.transform.DOLocalRotate(new Vector3(0, 0, rotate), 0.8f);
        Debug.Log("播放动画");
        for (int i = 0; i < 3; i++)
        {
            var item = Instantiate(propImage.gameObject);
            flightAnimationList.Add(item);
            item.transform.SetParent(startConment.parent);
            item.transform.localScale = Vector3.one*0.5f;
            item.transform.position = startConment.position;
            var targetPos = new Vector3(
                jumpTarget[i].position.x + Random.Range(0, 0.7f),
                jumpTarget[i].position.y + Random.Range(0, 0.7f),
                0
                );
            Destroy(item.GetComponent<Button>());

            item.transform.DOJump(targetPos, 0.3f,1,0.4f).onComplete = () => {
                item.transform.DOMove(endPostion.position, 0.5f).onComplete = () => {
                    buttnImg.GetComponent<Button>().interactable = true;
                    ClearAnimationList();
                };
  
            };

        }

    }
    private void ClearAnimationList()
    {
        for (int i=0; i< flightAnimationList.Count; i++)
        {
            Destroy(flightAnimationList[i]);
        }
        flightAnimationList.Clear();
    }
    
}
