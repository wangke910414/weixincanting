using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class LoadinPlanle : UiWoPropAbility
{
    [SerializeField]
    private Image suchduleImage;

    [SerializeField]
    private TextMeshProUGUI loadingText;

    private float maxLoadTime;

    private float laodTime;

    private bool loading;

    private void Start()
    {
        maxLoadTime = 7f;

    }
    private void OnEnable()
    {
        maxLoadTime = 10f;
        maxLoadTime += Random.Range(1, 3f);
        maxLoadTime = Random.Range(6f, 8f);
    }
    private void OnDisable()
    {
        loading = false;
    }
    public override void OpenUiWo()
    {
        //base.OpenUiWo();
        this.gameObject.SetActive(true);
        loading = true;
    }
    public override void CloseUiWo()
    {
 
            SystemGameManager.UiManager.LoadingContinue();
            loading = false;

       // this.gameObject.SetActive(false);
        //laodTime = 0;
        //suchduleImage.fillAmount = 0;
        StartCoroutine(ExtendTimeClose());

    }
    public IEnumerator ExtendTimeClose()
    {
        yield return new WaitForSeconds(1f);
        this.gameObject.SetActive(false);
        laodTime = 0;
        suchduleImage.fillAmount = 0;
    }


    private void Update()
    {
        if (loading)
        {
            laodTime += Time.deltaTime + Random.Range(0.0f, 0.2f);
            if (laodTime < maxLoadTime)
            {
                if (laodTime < maxLoadTime)
                    suchduleImage.fillAmount = laodTime / maxLoadTime;
            }
            else
            {
                CloseUiWo();
                   
            }
        }
    }

}
