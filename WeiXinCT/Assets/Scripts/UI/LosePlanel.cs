using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class LosePlanel : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI targetText;
    [SerializeField]
    private TextMeshProUGUI levelNumberText;

    [SerializeField]
    private Image[] targetImg;

    private void Start()
    {
       targetText.text = PlayerData.instance.targerCount.ToString();
        var once = PlayerData.instance.currLevelPlayOnce + 1;
        levelNumberText.text = PlayerData.instance.currSelectLevel.ToString() + "-"+ once.ToString();
      if (PlayerData.instance.targetType == TargetType.CIonTraget)
      {
            targetImg[0].gameObject.SetActive(true);
            targetImg[1].gameObject.SetActive(false);
       }
      else if (PlayerData.instance.targetType == TargetType.CompeletFood)
      {
            targetImg[0].gameObject.SetActive(false);
            targetImg[1].gameObject.SetActive(true);
      }

    }
    public void OnClickAgain()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
    }

    public void OnClickContiun()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Start");
    }

}
