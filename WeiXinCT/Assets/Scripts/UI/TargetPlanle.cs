using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;
public class TargetPlanle : UiWoPropAbility
{
    [SerializeField]
    private Transform targetBgImage;

    private Tween tween;

    [SerializeField]
    private TextMeshProUGUI targetNumberText;
    [SerializeField]
    private Image[] targetImg;

    [SerializeField]
    private TextMeshProUGUI limitNumberText;

    [SerializeField]
    private Image[] limitImg;
    [SerializeField]
    private TextMeshProUGUI nullText;

    [SerializeField]
    private SpecialLimit[] specialLimitList;

    private int specialLimitCount = 0;
    private int flightCount = 0;
    private int targetIndex = -1;
    private int limitIndex = -1;

    private List<GameObject> flightSpecialLimitList = new List<GameObject>();       //
    private GameObject flightTarget;
    private GameObject flightLimit;
    public override void OpenUiWo()
    {
         transform.gameObject.SetActive(true);

        targetNumberText.text = PlayerData.instance.targerCount.ToString();
        limitNumberText.text = PlayerData.instance.limitCount.ToString();

        if (PlayerData.instance.targetType == TargetType.CIonTraget)
        {
            targetImg[0].gameObject.SetActive(true);
            targetImg[1].gameObject.SetActive(false);
            targetImg[2].gameObject.SetActive(false);
            targetIndex = 0;
        }
        else if (PlayerData.instance.targetType == TargetType.CompeletFood)
        {
            targetImg[0].gameObject.SetActive(false);
            targetImg[1].gameObject.SetActive(true);
            targetImg[2].gameObject.SetActive(false);
            targetIndex = 1;
        }
        else if (PlayerData.instance.targetType == TargetType.GoodCommend)
        {
            targetImg[0].gameObject.SetActive(false);
            targetImg[1].gameObject.SetActive(false);
            targetImg[2].gameObject.SetActive(true);
            targetIndex = 2;
        }

        if (PlayerData.instance.limitType == LimitType.GustCountLimit)
        {
            limitImg[0].gameObject.SetActive(false);
            limitImg[1].gameObject.SetActive(true);
            limitIndex = 1;
        }
        else if (PlayerData.instance.limitType == LimitType.TimeLimit)
        {
            limitImg[0].gameObject.SetActive(true);
            limitImg[1].gameObject.SetActive(false);
            limitIndex = 0;
        }
            targetBgImage.DOScaleY(1, 0.3f).SetEase(Ease.OutSine).onComplete = () => {
                StartCoroutine(CloseTarget());
                // CoroutineHandler.StartStaticCoroutine(FlightSpecialLimit());

            };
        SpecialLimit();


    }
    private IEnumerator CloseTarget()
    {
        yield return new WaitForSeconds(0.5f);
        GetComponent<AniamtionArryHandler>().PlayAnimationScale();
        yield return new WaitForSeconds(2f);

        FlightSpecialLimit();
        FlightTarget();
        FlightLimit();
        targetBgImage.DOScaleY(0, 0.2f).SetEase(Ease.InSine).onComplete = () =>
        {
            CloseUiWo();
        };
     }

    public override void CloseUiWo()
    {
        SystemGameManager.SetGameRun();
        transform.gameObject.SetActive(false);
    }
    private void OnEnable()
    {
        targetBgImage.localScale = new Vector3(1, 0, 1);
    }
    private void OnDisable()
    {
        
    }
    private void SpecialLimit()
    {
        var currLevel = SystemGameManager.LevelManager.GetCurrLevelData();
        var specialLimitConfigur = SystemDataFactoy.instans.GetResourceConfigur("SpecialLimitCongigur") as SpecialLimitCongigur;
        if (currLevel == null || specialLimitConfigur == null)
        {
            return;
        }

        int limitCount = 0;
        for (int i = 0; i < currLevel.specialLimitType.Length; i++)
        {
            specialLimitList[i].gameObject.SetActive(false);
            if (currLevel.specialLimitType[i] != 0)
            {
                limitCount++;
                var temp = (SpecialLimitType)currLevel.specialLimitType[i];
                var item = specialLimitConfigur.GetSpecialLimit(temp);
                if (item != null)
                {
                    specialLimitList[specialLimitCount].gameObject.SetActive(true);
                    specialLimitList[specialLimitCount].GetComponent<Image>().sprite = item.GetComponent<Image>().sprite;
                    specialLimitList[specialLimitCount].GetComponent<Image>().SetNativeSize();
                    specialLimitCount++;
                }
                   
            }
        }
        if (limitCount != 0)
        {
            nullText.gameObject.SetActive(false);
        }
        else
        {
            nullText.gameObject.SetActive(true);
        }
    }
    private void FlightSpecialLimit()
    {
        var target = SystemGameManager.UiManager.GameHomePlael.SpecialLimitTransform;
        for (int i=0; i< specialLimitCount; i++ )
        {
            var flightItme = Instantiate(specialLimitList[i].gameObject);
            flightItme.transform.position = specialLimitList[i].transform.position;
            flightItme.transform.parent = target;
            flightItme.GetComponent<FlightAnimation>().Flight(target, 1f, FlightSpecialLimitBanck);
            flightSpecialLimitList.Add(flightItme);
        }
    }

    private void FlightSpecialLimitBanck()
    {
        flightCount++;
        if (flightCount == specialLimitCount)
        {
            for (int i=0; i<flightSpecialLimitList.Count; i++)
            {
                flightSpecialLimitList[i].SetActive(false);
                Destroy(flightSpecialLimitList[i]);
            }

            SystemGameManager.UiManager.GameHomePlael.SpecialLimitShow();
         }
    }

    private void FlightTarget()
    {
        var index = targetIndex;
        if (index < 0 || index >= targetImg.Length)
            return;
        var target = SystemGameManager.UiManager.GameHomePlael.TargetTransform;
        var flightItem = Instantiate(targetImg[index].gameObject);
        flightItem.transform.position = targetImg[index].transform.position;
        flightItem.transform.parent = target;
        flightItem.AddComponent<FlightAnimation>();
        flightItem.transform .GetComponent<FlightAnimation>().Flight(target,1f, FlightTargetBanck);
        flightTarget = flightItem;
    }

    private void FlightTargetBanck()
    {
        Destroy(flightTarget);
        SystemGameManager.UiManager.GameHomePlael.TargetShow();
    }

    private void FlightLimit()
    {
        var index = limitIndex;
        if (index < 0 || index >= limitImg.Length)
            return;
        var target = SystemGameManager.UiManager.GameHomePlael.LimitTransform;
        var flightItem = Instantiate(limitImg[index].gameObject);
        flightItem.transform.position = limitImg[index].transform.position;
        flightItem.transform.parent = target;
        flightItem.AddComponent<FlightAnimation>();
        flightItem.transform.GetComponent<FlightAnimation>().Flight(target, 1f, FlightLimitBanck);
        flightLimit = flightItem;
    }
    private void FlightLimitBanck()
    {
        Destroy(flightLimit);
        SystemGameManager.UiManager.GameHomePlael.LimitShow();
    }
}
