using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using LitJson;

public class FastBuyTip : TipDlg
{
    [SerializeField]
    private Image iconImg;
    [SerializeField]
    private TextMeshProUGUI numberText;

    [SerializeField]
    private TextMeshProUGUI priceText;

    private GameObject parentPanel;
    private Sprite inpuIcon;

    private ConsumePropType propType;

    private int buyPropCount = 0;
    private int buyPrice = 0;
    public CTinterface parentDlg;

    public Sprite InpuIcon { get => inpuIcon; set => inpuIcon = value; }

    public ConsumePropType PropType { get => propType; set => propType = value; }

    public GameObject parentPane { get => parentPanel; set => parentPanel = value; }
    public void OnClickAddButn()
    {
        var number = int.Parse(numberText.text) + 3;
        numberText.text = number.ToString();
        priceText.text = (number / 3 * 100).ToString();
        buyPropCount = number;
        buyPrice = (number / 3 * 100);
    }
    public override void OpenDlg()
    {
        iconImg.sprite = inpuIcon;

        this.gameObject.SetActive(true);
    }
    public override void OnClickClose()
    {
        numberText.text = 0.ToString();
        priceText.text = 0.ToString();
        buyPropCount = 0;
        buyPrice = 0;
        propType = ConsumePropType.Null;

        this.gameObject.SetActive(false);
    }
    public override void OnClickOk()
    {
       if ((PlayerData.playerDataParame.diamond - buyPrice)>= 0 && propType != ConsumePropType.Null)
        {
 
            if (!PlayerData.consumePropDataParameter.consumePropDataDict.ContainsKey(propType.ToString()))
            {
               /// Debug.Log("增加顾客道具个数    " + buyPropCount.ToString());
           
                PlayerData.consumePropDataParameter.consumePropDataDict.Add(propType.ToString(), buyPropCount.ToString());
            }
            else
            {
               int temo =int.Parse( PlayerData.consumePropDataParameter.consumePropDataDict[propType.ToString()]) + buyPropCount;
                PlayerData.consumePropDataParameter.consumePropDataDict[propType.ToString()] = temo.ToString();
            }

            PlayerData.playerDataParame.diamond -= buyPrice;
            PlayerData.instance.achieveParmeter.comsumeDiamondCount += buyPrice;

            var str = JsonMapper.ToJson(PlayerData.consumePropDataParameter.consumePropDataDict);
#if UNITY_EDITOR
            PlayerData.instance.SaveData("consumeProp", str);
#elif !UNITY_EDITOR && UNITY_WEBGL
            PlayerData.instance.SaveData("UpdateConsumeProp", str);
#endif
            PlayerData.instance.PlayerDataParameSave();
        }
        //CTinterface
        OnClickClose();
        parentDlg.FastBuyTip();
       /// parentPane.GetComponent<StopPanel>().UpdataPropCount();
    }
   
}
