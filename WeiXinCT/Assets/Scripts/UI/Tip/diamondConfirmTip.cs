using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class diamondConfirmTip : TipDlg
{
    private bool isSuccess = false;
    public override void TipDlgMsg(TipDlgMessage msg)
    {
        if (msg == null)
            return;

        TipDiamondComsume buff = msg as TipDiamondComsume;
        isSuccess = buff.isSuccesss;

       base.TipDlgMsg(msg);
    }

    public override bool IsSuccess()
    {
        return isSuccess;
    }
}
