using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TipDlgMessage
{
    public TipDlgType tipDlgType = TipDlgType.Null;
}

public class TipDlgUnLockRestaurentMsg : TipDlgMessage
{
    public int keyCount;        //钥匙数目
    public int needKeyCount;    //需要钥匙数目
    public int isSuccess;        //完成
}

public class TipDiamondComsume : TipDlgMessage
{
    public bool isSuccesss;            //是否成功
    //public int itemIndex;             //点击index
}
public class DiamondStoreTipEvent : TipDlgMessage
{
    public ObatainTipShowType style;
    public int giftCount = 0;
    public List<ConsumePropType> seletPropList = new List<ConsumePropType>();
}
public class ObtainGeneral : TipDlgMessage
{
    public PrizeComponent prize;
}
