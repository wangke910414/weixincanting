using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//失败提示
public class FailTip : TipDlg , CTinterface
{

    public int seleclType;      //选择广告 1， 道具2

    [SerializeField]
    private Transform[] bolParent;
    [SerializeField]
    private Image bolImg;

    [SerializeField]
    private Image propImg;

    [SerializeField]
    private FastBuyTip tipDlg;

    private ConsumePropConfigur propConfigur;

    public override void OnClickClose()
    {
        SystemGameManager.GuestManager.ReturnOperate(false);
        base.OnClickClose();
        
    }
    public override void OnClickOk()
    {
        if (seleclType == 0)
            return;


     
            bolImg.gameObject.SetActive(false);
        if (buyConsumeProp == ConsumePropType.AddGuest)
        {
            SystemGameManager.GuestManager.RturnFailSelceOperate(seleclType);
        }
        else if (buyConsumeProp == ConsumePropType.InvalidSpecialConditions)
        {
            if (!PlayerData.instance.currSelectPropList.Contains(buyConsumeProp))
            {
                PlayerData.instance.currSelectPropList.Add(buyConsumeProp);
                SystemGameManager.UiManager.GameHomePlael.UpdatePropShow();
            }
        }
        base.OnClickOk();
        

    }

    public void OnClickAdb()
    {
        seleclType = 1;
        bolImg.transform.SetParent(bolParent[0]);
        bolImg.transform.localPosition = Vector3.zero;
        bolImg.gameObject.SetActive(true);
    }
    public void OnClickProp()
    {

        bool isHave = false;
        if (PlayerData.consumePropDataParameter.consumePropDataDict.ContainsKey(ConsumePropType.AddGuest.ToString()))
        {
            var number = int.Parse(PlayerData.consumePropDataParameter.consumePropDataDict[ConsumePropType.AddGuest.ToString()]);
            if (number != 0)
            {
                isHave = true;
            }
        }
        if (!isHave)
        {
            tipDlg.PropType = ConsumePropType.AddGuest;
            tipDlg.parentDlg = this;
            tipDlg.parentPane = this.gameObject;
            tipDlg.OpenDlg();
        }
        else
        {
            seleclType = 2;
            bolImg.transform.SetParent(bolParent[1]);
            bolImg.transform.localPosition = Vector3.zero;
            bolImg.gameObject.SetActive(true);
        }
    }

    public void FastBuyTip()
    {
        
    }
    public override void OpenDlg()
    {
        if (gameObject.activeSelf)
            return;
        if (propConfigur == null)
        {
            propConfigur = SystemDataFactoy.instans.GetResourceConfigur("ConsumePropConfigur") as ConsumePropConfigur;
        }
        ConsumeProp prop = null;
        foreach (var item in propConfigur.ConsumeProp)
        {
            if (item.comsumePropType == buyConsumeProp)
            {
                prop = item;
                break;
            }
        }    

        if (prop != null)
        {
            propImg.sprite = prop.Icon;
            tipDlg.buyConsumeProp = buyConsumeProp;
            tipDlg.InpuIcon = prop.Icon;
        }

        base.OpenDlg();
    }
    public override void EfficeEnd(bool value)
    {
       if (value)
        {
           // Time.timeScale = 0;
        }
        else
        {
          //  Time.timeScale = 1;
        }
    }
}
