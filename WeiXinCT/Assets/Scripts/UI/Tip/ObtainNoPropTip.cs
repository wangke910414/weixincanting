using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

//无道具获得
public class ObtainNoPropTip : TipDlg
{
    [SerializeField]
    private Image prizeImg;
    [SerializeField]
    private TextMeshProUGUI countText;
    [SerializeField]
    private Sprite[] prizeSkin;

    public Transform PrizeTransfrom { get => prizeImg.transform; }
    public override void TipDlgMsg(TipDlgMessage msg)
    {
        if (msg.tipDlgType != this.tipDlgType)
            return;
        ObtainGeneral evt = msg as ObtainGeneral;
        if (evt != null)
        {
            RereashPanel(evt);
        }
    }

    private void RereashPanel(ObtainGeneral evt)
    {
        countText.text = evt.prize.PrizeCount.ToString();
        if(evt.prize.prizeType == PrizeType.Cion)
        {
            prizeImg.sprite =prizeSkin[1];
        }
        else if (evt.prize.prizeType == PrizeType.Diamond)
        {
            prizeImg.sprite = prizeSkin[0];
        }
        else if (evt.prize.prizeType == PrizeType.ComsumeProp)
        {
            prizeImg.sprite = evt.prize.prizeImg;
        }
    }
}
