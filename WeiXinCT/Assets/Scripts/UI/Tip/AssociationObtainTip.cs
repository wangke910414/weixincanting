using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AssociationObtainTip : TipDlg
{
    [Header("UI")]
    [SerializeField]
    private TextMeshProUGUI cionText;
    [SerializeField]
    private TextMeshProUGUI diamendText;
    [SerializeField]
    private List<LevelInfoPlanleProp> propListFour ;
    [SerializeField]
    private List<LevelInfoPlanleProp> propListEight;
    [SerializeField]
    private Transform propFourType;
    [SerializeField]
    private Transform propEightType;


    private List<ConsumePropType> displayPropList = new List<ConsumePropType>();


    public TextMeshProUGUI CionText { get => cionText; }

    public TextMeshProUGUI DiamendText { get => diamendText; }

    public List<ConsumePropType> DisplayPropList { get => displayPropList; }

    public List<LevelInfoPlanleProp> PropListFour { get => propListFour; }
    public List<LevelInfoPlanleProp> PropListEight { get => propListEight; }

    public void SetPropCount(int count)
    {
        if (count == 8)
        {
            propEightType.gameObject.SetActive(true);
            propFourType.gameObject.SetActive(false);
        }
        else if (count == 4)
        {
            propEightType.gameObject.SetActive(false);
            propFourType.gameObject.SetActive(true);
        }
    }
}
