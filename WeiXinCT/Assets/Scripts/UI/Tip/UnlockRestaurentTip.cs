using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UnlockRestaurentTip : TipDlg
{
    [SerializeField]
    private TextMeshProUGUI keyCountText;
    [SerializeField]
    private Image progessValueImg;
    [SerializeField]
    private Button lockLeveBtn;
    [SerializeField]
    private Button obtainKeyBtn;

    private int needValue = 0;
    private int currValue = 0;

    public override void TipDlgMsg(TipDlgMessage msg)
    {
        if (msg.tipDlgType != tipDlgType)
            return;

        if (msg != null)
        {
            TipDlgUnLockRestaurentMsg data = msg as TipDlgUnLockRestaurentMsg;
            keyCountText.text = data.keyCount.ToString();
            progessValueImg.fillAmount = (float)data.keyCount / (float)data.needKeyCount;
            needValue = data.needKeyCount;
            currValue = data.keyCount;
            var b = IsSuccess();
            obtainKeyBtn.gameObject.SetActive(!b);
            lockLeveBtn.gameObject.SetActive(b);

        }
    }
    public override bool IsSuccess()
    {
        if (needValue <= currValue)
            return true;
        return false;
    }
}
