using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum ObatainTipShowType : int
{
    Null,
    DiamondStoreOne,           //钻石商店1
    DiamondSroreTwo,        
    DiamondStoreThree,
    ObtainSignPanel,
}


public class ObtainTip : TipDlg
{
    [SerializeField]
    private Image prizeImg;
    [SerializeField]
    private TextMeshProUGUI prizeCountText;
    [SerializeField]
    private Transform styleShowOnePropContent;
    [SerializeField]
    private Transform styleShowTwoPropContent;
    [SerializeField]
    private Transform  syleShowPropSingnPanel;

    [SerializeField]
    private Transform giftContent;

    [SerializeField]
    private List<LevelInfoPlanleProp> propStyleOne;
    //[SerializeField]
    //private List<LevelInfoPlanleProp> propStyleTwo;

    public CTinterface callDlg;

    private ObatainTipShowType style;
    private DiamondStoreTipEvent msgEvent;


    public Image PrizeImg { get => prizeImg; }
    public TextMeshProUGUI PrizeCountText { get => prizeCountText; }

    public void OnClickReceive()
    {
        callDlg.CallBanck();
        OnClickClose();
    }
    public override void TipDlgMsg(TipDlgMessage msg)
    {


        Debug.Log("收到调用获得窗口事件  ");
        DiamondStoreTipEvent buff = msg as DiamondStoreTipEvent;
        if (buff != null)
        {
            RefreshPanel(buff);
        }
  
        base.TipDlgMsg(msg);
    } 

    private void RefreshPanel(DiamondStoreTipEvent msg)
    {
       this.style = msg.style;
        prizeCountText.text = msg.giftCount.ToString();
        if (style == ObatainTipShowType.DiamondStoreOne)
        {
            giftContent.localPosition = new Vector3(0, 70, 0);
            styleShowOnePropContent.gameObject.SetActive(true);
            styleShowTwoPropContent.gameObject.SetActive(false);
            ConsumePropConfigur propConfigur = SystemDataFactoy.instans.GetResourceConfigur("ConsumePropConfigur") as ConsumePropConfigur;
            for (int i=0; i<msg.seletPropList.Count; i++)
            {
                var prop = propConfigur .GetConsumeProp(msg.seletPropList[i]);
                if (prop != null)
                {
                    propStyleOne[i].IconImg.sprite = prop.Icon;
                }
            }
        }
        else if (style == ObatainTipShowType.DiamondSroreTwo)
        {
            giftContent.localPosition = new Vector3(0, 25, 0);
            styleShowOnePropContent.gameObject.SetActive(false);
            styleShowTwoPropContent.gameObject.SetActive(true);
            syleShowPropSingnPanel.gameObject.SetActive(false);
        }
        else if (style == ObatainTipShowType.DiamondStoreThree)
        {
            giftContent.localPosition = new Vector3(0, 25, 0);
            styleShowOnePropContent.gameObject.SetActive(false);
            styleShowTwoPropContent.gameObject.SetActive(false);
            syleShowPropSingnPanel.gameObject.SetActive(false);
        }
        else if (style == ObatainTipShowType.ObtainSignPanel)
        {
            styleShowOnePropContent.gameObject.SetActive(false);
            styleShowTwoPropContent.gameObject.SetActive(false);
            syleShowPropSingnPanel.gameObject.SetActive(true);
        }
    }
}
