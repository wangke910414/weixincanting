using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SkipLevelTip : TipDlg
{
    [SerializeField]
    private TextMeshProUGUI keyCountText;


    private int keyConut = 0;
    public override void TipDlgMsg(TipDlgMessage msg)
    {
        if (msg.tipDlgType != TipDlgType.SkipLevelDlg)
            return;
        var data = msg as TipDlgUnLockRestaurentMsg;
        keyConut = data.keyCount;
        RefeshDlg();
    }

    private void RefeshDlg()
    {
        keyCountText.text = keyConut.ToString();
    }
}
