using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;


public enum TipDlgType : int
{
    Null,
    Gameleve,           //关卡说明
    KeyIllustrate,      //钥匙说明
    IllustrateTip,      //说明
    MoneyIllustrate,   //货币说明
    PropIllustrate,      //道具说明
    ContimuReward,        //连击奖励
    SamllTipDlg,           //普通小弹窗
    SkipLevelDlg,          //跳过本官
    UnLockRestaurentTip,    //解锁餐厅
   SkipLevelNoKeyTip,   //解锁餐厅没有钥匙
   DimaondCofirmTip,     //砖石确认消耗
   ObttainTip,          //获得弹窗
   ObtainNoPropTip,    //无道具获得
   DiamondObtainTip,//钻石获得
}

public class TipDlg : MonoBehaviour
{
    private SystemGameManager systemGameManager;

    private TipDlgManager tipDligManager;

    [SerializeField]
    private CanvasGroup currCanvas;
    [SerializeField]
    private float changeTime = 0.3f;
    [SerializeField]
    private TextMeshProUGUI ilusterateText;     //说明框

    public TipDlgType tipDlgType = TipDlgType.Null;
   

    public CTinterface tInsterface;

    public ConsumePropType buyConsumeProp;
    public SystemGameManager SystemGameManager { get => systemGameManager;}
    public TextMeshProUGUI IlusterateText { get => ilusterateText; }
    public TipDlgManager IipDligManager { set => tipDligManager = value; }
    public void Configur(SystemGameManager systemGameManager)
    {
        this.systemGameManager = systemGameManager;
    }

    // Start is called before the first frame update
    public virtual void OnClickClose()
    {
        // this.gameObject.SetActive(false);
        CloaseDlg();
    }
    public virtual void OnClickOk()
    {
        if (tipDligManager != null)
            tipDligManager.TipDlgOk();
        CloaseDlg();
    }

   public virtual void EfficeEnd(bool value)
    {
        if (value == false && tInsterface != null)
        tInsterface.CallExit();
    }
    public virtual void OpenDlg()
    {
        SetUiEffect(true);
    }
    public virtual void CloaseDlg()
    {
        Time.timeScale = 1;
        SetUiEffect(false);
    }

    private void SetUiEffect(bool value)
    {
        if (value)
        {
            currCanvas.gameObject.SetActive(true);
            currCanvas.alpha = 0;
            currCanvas.transform.localScale = Vector3.one * 0.95f;
            Tween tween1 =  currCanvas.DOFade(1f, changeTime).SetEase(Ease.OutCubic);
            tween1.SetUpdate(true);
            Tween tween2 = currCanvas.transform.DOScale(1f, changeTime).SetEase(Ease.OutCubic);
            tween2.SetUpdate(true);
            tween2.onComplete = () => {
                currCanvas.DOKill();
                EfficeEnd(true);
            };

  
        }
        else
        {
           Tween tween1 =  currCanvas.DOFade(0, changeTime).SetEase(Ease.OutCubic);
            tween1.SetUpdate(true);

            Tween tween2 = currCanvas.transform.DOScale(0.95f, changeTime).SetEase(Ease.OutCubic);
            tween2.SetUpdate(true);
            tween2.OnComplete(() =>
            {
                currCanvas.gameObject.SetActive(false);
                currCanvas.DOKill();
                EfficeEnd(false);
            });
        }
    }
    public virtual void TipDlgMsg(TipDlgMessage msg)
    {

    }
    public virtual bool IsSuccess()
    {
        return false;
    }

}
