using SK.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ContactTip : TipDlg
{
    [SerializeField]
    private TMP_InputField inputText;
    [SerializeField]
    private Material fontMaterial;

    private void Start()
    {
        inputText.fontAsset.material = fontMaterial;
    }
    public void SendBtn()
    {
        var str = inputText.text;
        Debug.Log(" 输入数据  " + str);
#if UNITY_WEBGL && !UNITY_EDITOR

       CloudFunction.instance.SendEmail();
#elif UNITY_EDITOR
        Mailer.Send("邮件标题：用户消息", str, "910665086@qq.com");
#endif
    }
}
