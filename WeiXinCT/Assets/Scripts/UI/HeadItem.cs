using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class HeadItem : MonoBehaviour
{
    private HeadPlanel headPlanel;

    private Tween tween;
    public HeadAssetData headData;

    public void InteHeadItem( HeadPlanel headPlanel, HeadAssetData headData )
    {
        this.headPlanel = headPlanel;
        this.headData = headData;

    }
    public void OnClick()
    {
        if (headPlanel.CurrHeadData.headID == this.headData.headID)
        {
            return ;
        }
        if (tween==null)
        {
            tween = this.transform.DOScale(1.2f, 0.1f).SetEase(Ease.Flash).SetLoops(2,LoopType.Yoyo);
        }
        else if (tween != null && !tween.active)
        {
            tween = this.transform.DOScale(1.2f, 0.1f).SetEase(Ease.Flash).SetLoops(2, LoopType.Yoyo);
        }
        headPlanel.CallSelectHead(headData);
    }
}
