using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class LevelInfoFoodUp : MonoBehaviour
{
    public LevelInfoPlane levelInforPlane;
    private int currFoodGrade;
    private bool isSelect;
    private int foodIndex;
    private bool isLock;
    private int unLockLevel;        //解锁等级
    private string disribleText;
    private string foodName;
    private int currFoodPrice;
    private int upFoodPrive;

    [SerializeField]
    private Image selectImg;
    [SerializeField]
    private Image foodImg;
    [SerializeField]
    private Image[] foodGradeImg;
     [SerializeField]
    private Image unLockImg;
    private List<Sprite> foodGradeImgList = new List<Sprite>();

    private float shekSpanTimeTime = 5f;

    public Image SelectImg { get => selectImg;  }
    public int FoodIndex { get => foodIndex; set => foodIndex = value; }
    public int CurrFoodGrade { get => currFoodGrade; set => currFoodGrade = value; }
    public string DisribleText { get => disribleText; set => disribleText = value; }
    public Image FoodImg { get => foodImg; }
    public int CurrFoodPrice { get => currFoodPrice; set => currFoodPrice = value; }
    public int UpFoodPrive { get => upFoodPrive; set => upFoodPrive = value; }

    public string FoodName { get => foodName; set => foodName = value; }

    public List<Sprite> FoodGradeImgList { get => foodGradeImgList; set => foodGradeImgList = value; }

     public bool IsLock { get => isLock; set => isLock = value; }
    public int UnLockLevel { get => unLockLevel; set => unLockLevel = value; }

    void Start()
    {
        
    }
 
    
    public void OnclickSelect()
    {
        if (!isLock)
            return;

        levelInforPlane.CallSelectFood(foodIndex);
    }

    //更新食物等级
    public void updateFoodGrade()
    {
        for (int i=0; i< foodGradeImg.Length; i++)
        {
            if (i < currFoodGrade)
                foodGradeImg[i].gameObject.SetActive(true);
            else
                foodGradeImg[i].gameObject.SetActive(false);

        }
        foodImg.sprite = foodGradeImgList[currFoodGrade - 1];
        UnLock();
    }
    private void UnLock()
    {
        unLockImg.gameObject.SetActive(!isLock);
    }

    private void Update()
    {
        shekSpanTimeTime -= Time.deltaTime;
        if (shekSpanTimeTime <= 0)
        {
            PlayRegularAnimation();
            shekSpanTimeTime = 5f;
        }
    }

    private void PlayRegularAnimation()
    {
        if (unLockImg.gameObject.activeSelf)
            unLockImg.transform.DOShakePosition(1, new Vector3(10, 5, 0)).SetEase(Ease.Flash);
    }
 

}
