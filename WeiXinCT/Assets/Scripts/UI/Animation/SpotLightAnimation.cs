using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SpotLightAnimation : MonoBehaviour
{
    [SerializeField]
    private Transform[] target;
    [SerializeField]
    private GameObject prefab;
    [SerializeField]
    private GameObject lostPrefab;
    [SerializeField]
    private bool isWin;
   

    private void OnEnable()
    {
        PlayAnimation();
    }
    public void PlayAnimation()
    {
        StartCoroutine(AnimationHandle());
    }
    private IEnumerator AnimationHandle()
    {
        int index = 0;
        while (true)
        {
            GameObject obj = null;
            if (isWin)
                obj = Instantiate(prefab);
            else
                obj = Instantiate(lostPrefab);

            obj.transform.parent = target[index];
            obj.transform.position = transform.position;
            obj.transform.localScale = Vector3.one * UnityEngine.Random.Range(1f,1.3f);
            var pos = new Vector3(
                 target[index].position.x + UnityEngine.Random.Range(0f, 1f),
                 target[index].position.y + UnityEngine.Random.Range(0f, 1f),
                 0
                );
            obj.transform.DOMove(pos, 1).onComplete = () =>
            {
                Destroy(obj);
            };
            //obj.transform.DOJump(pos, 1f,1,0.5f).onComplete = () => {
            //    Destroy(obj);
            //};
            // obj.transform.DOScale(new Vector3(1.2f, 1.2f, 0), 0.5f).SetEase(Ease.Flash).SetLoops(2, LoopType.Yoyo);
            index++;
            if (index >= target.Length)
                index = 0;
            yield return new WaitForSeconds(0.3f);
        }
    }

}
