using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;

//缩小在放大动画
public class ZoomInAndOutAnimation : MonoBehaviour
{
    [SerializeField]
    private Transform opertateCompnent;
    
   public void PlayerAnimation(float time=0.3f, Action back = null)
    {
        if (opertateCompnent == null)
            return;

        opertateCompnent.DOScale(0, time).onComplete = () =>
        {
            if (back != null)
                back.Invoke();
            opertateCompnent.DOScale(1, time).onComplete = () => {
   
            };
        };
   }
}
