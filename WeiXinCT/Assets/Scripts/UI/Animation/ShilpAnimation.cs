using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ShilpAnimation : MonoBehaviour
{
    private void Start()
    {
        PlayAnimation();
    }
    private void PlayAnimation()
    {
        transform.DOMoveY(0f, 2f).SetLoops(-1,LoopType.Yoyo);
    }
}
