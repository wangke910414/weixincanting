using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class ShakeAnimation : MonoBehaviour
{
    [SerializeField]
    private float shakeSpanTiem = 4;
    private float timeCount;

    private void Start()
    {
        timeCount = shakeSpanTiem;
    }
    private void Update()
    {
        timeCount -= Time.deltaTime;
        if (timeCount < 0)
        {
            PlayAniation();
            timeCount = shakeSpanTiem;
        }
    }
    private void PlayAniation()
    { 
            this.transform.DOShakePosition(1, new Vector3(5, 5, 0)).SetEase(Ease.Flash);

    }
}
