using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class SliderScroll : MonoBehaviour
{

    [SerializeField]
    private RectTransform viewPort;
    [SerializeField]
    private RectTransform connent;
    public void SlidetViweToElement(GameObject selected)
    {
        var target = selected.GetComponent<RectTransform>();

        Vector3 sCenter = viewPort.transform.position;
        Debug.Log("Center Pos: " + sCenter);

        Vector3 itemCenterPos = target.transform.position;
        Debug.Log("Item Center Pos: " + itemCenterPos);

        Vector3 difference = sCenter - itemCenterPos;
        Vector3 newPos = connent.transform.position + difference;

        connent.transform.DOMoveY(newPos.y, 1f);
    }
    public void SlidetViweToElementType(GameObject selected)
    {
        //anchoredPosition的移动方法
        void Test2(GameObject selected)
        {
            var target = selected.GetComponent<RectTransform>();

            Debug.Log("转换前物体坐标" + target.anchoredPosition);
            var screenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, target.transform.position);
            //将物体作为scollrect的子物体
            Vector2 rTarget;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(viewPort, screenPoint, Camera.main, out rTarget);

            Debug.Log("转换后物体坐标" + rTarget);
            Vector2 sCenter = viewPort.rect.center;
            Debug.Log("scrollRect中心坐标" + sCenter);

            Vector2 distance = sCenter - rTarget;

            Vector2 newPos = connent.anchoredPosition + distance;

            connent.DOAnchorPosY(newPos.y, 2f);

        }
    }
    public void SlidetViweToElementWayTwo(int index)
    {

        var moveY = index * 70;
        connent.transform.DOLocalMoveY(moveY, 0f);
    }
}
