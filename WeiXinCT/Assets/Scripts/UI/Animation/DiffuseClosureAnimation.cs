using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

//ɢ����£
public class DiffuseClosureAnimation : MonoBehaviour
{
    [SerializeField]
    private Transform startTransfrom;
    [SerializeField]
    private Transform[] jumpTarget;
    [SerializeField]
    private Transform endTransform;

    private List<GameObject> flightAnimationList = new List<GameObject>();
    private float rotate;
    public IEnumerator PlayAnimation(GameObject obj, Action back = null)
    {
        yield return new WaitForSeconds(0.5f);
        rotate += UnityEngine.Random.Range(0.0f, 360f);

        startTransfrom.DOLocalRotate(new Vector3(0, 0, rotate), 0.8f);
        Debug.Log("���Ŷ���");
        for (int i = 0; i < 3; i++)
        {
            var item = Instantiate(obj);
            flightAnimationList.Add(item);
            item.transform.SetParent(this.transform);
            item.transform.localScale = Vector3.one * 0.5f;
            item.transform.position = startTransfrom.position;
            var targetPos = new Vector3(
                jumpTarget[i].position.x + UnityEngine.Random.Range(0, 0.7f),
                jumpTarget[i].position.y + UnityEngine.Random.Range(0, 0.7f),
                0
                );
                      item.transform.DOJump(targetPos, 0.3f, 1, 0.4f).onComplete = () => {
                        item.transform.DOMove(endTransform.position, 0.5f).onComplete = () =>
                        {
                            if (back != null)
                                back.Invoke();
                            ClearList();
                        };

                };

        }

    }

    private void ClearList()
    {
        for (int i=0; i<flightAnimationList.Count; i++)
        {
            Destroy(flightAnimationList[i]);
        }
        flightAnimationList.Clear();
    }
}
