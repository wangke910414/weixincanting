//using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using System.Collections;

/// <summary>
/// 参残影飞行
/// </summary>
public class GhostFlightAnimation : MonoBehaviour
{
    [SerializeField]
    private GameObject ghostPrefab;
    [SerializeField]
    private int ghostCount;
    [SerializeField]
    private Transform efficeShowCionment;
    [SerializeField]
    private Transform[] startTransfrom;

    [SerializeField]
    private Transform targetPostion;
    private float animationTime = 0.8f;

    private List<GameObject> itemList = new List<GameObject>();

    private void Start()
    {
        //StartCoroutine(PlayFlightCionAnimation());
    }


    public void PlayFlightCionAnimation(int count, Transform targetm, Action back = null)
    {

        if (ghostPrefab == null)
        {
            return;
        }
        for (int i = 0; i < count; i++)
        {
            var ghost = Instantiate(ghostPrefab.gameObject);
            ghost.transform.SetParent(efficeShowCionment);
            ghost.transform.localScale = Vector3.one * UnityEngine.Random.Range(0.4f, 0.5f);

            var tempos = startTransfrom[i].position;
            var newPos = new Vector3(
                UnityEngine.Random.Range(i * 0.1f, 0.5f) + tempos.x,
                UnityEngine.Random.Range(i * 0.1f, 0.5f) + tempos.y,
                tempos.z
                );
            ghost.transform.position = newPos;

            itemList.Add(ghost);
            ghost.transform.DOShakeScale(1).SetEase(Ease.Flash);
        }
        StartCoroutine(PlayMoveAniamtion(targetm, back));
    }


    public void  PlayFlightCionAnimation(int count, Action back = null)
    {

        if (ghostPrefab == null)
        {
                return;
        }
        for (int i=0; i< count; i++)
        {
            var ghost = Instantiate(ghostPrefab);
            ghost.transform.SetParent(efficeShowCionment);
            ghost.transform.localScale = Vector3.one * UnityEngine.Random.Range(0.8f, 1.2f);
         
            var tempos =  startTransfrom[i].position;
            var newPos = new Vector3(
                UnityEngine.Random.Range(i*0.1f, 0.5f) + tempos.x,
                UnityEngine.Random.Range(i*0.1f, 0.5f) + tempos.y,
                tempos.z
                );
            ghost.transform.position = newPos;

            itemList.Add(ghost);
            ghost.transform.DOShakeScale(1).SetEase(Ease.Flash);
        }
        StartCoroutine(PlayAnimation(back));
    }
    private IEnumerator PlayAnimation(Action back = null)
    {
        yield return new WaitForSeconds(0.5f);
        for (int i = itemList.Count - 1; i >= 0; i--)
        {
            var ghost = itemList[i].GetComponent<Transform>();
            if (ghost == null)
                continue;
            if (targetPostion == null)
                continue;
            ghost.DOMoveX(targetPostion.position.x, animationTime).SetEase(Ease.InFlash);
            ghost.DOMoveY(targetPostion.position.y, animationTime).SetEase(Ease.InFlash);
            ghost.DOScale(1, animationTime).SetEase(Ease.Flash);
        }
        yield return new WaitForSeconds(1.6f);
        ClearList();
        if (back != null)
            back.Invoke();
        yield return null; 
    }


    public void PlayFlightKeyAnimation(Transform target, int flightCount, Action callBanck = null)
    {
        if (ghostPrefab == null)
            return;

    
        for (int i = 0; i < flightCount; i++)
        {
            var ghost = Instantiate(ghostPrefab);
            ghost.transform.parent = efficeShowCionment;
            ghost.transform.localScale = Vector3.one * UnityEngine.Random.Range(0.8f, 1.2f);
            var tempos = startTransfrom[i].position;
            var newPos = new Vector3(
                    UnityEngine.Random.Range(i * 0.1f, 0.5f) + tempos.x,
                    UnityEngine.Random.Range(i * 0.1f, 0.5f) + tempos.y,
                    tempos.z
                );
            ghost.transform.position = newPos;
            itemList.Add(ghost);
        }
        StartCoroutine(PlayJumpAniamtion(target, callBanck));
        Debug.Log("飞行完成");

    }


    public void PlayFlightAnimation(Transform target, Action callBanck = null)
    {
        if (ghostPrefab == null)
            return;

        Debug.Log("播放钥匙动画4");
        for (int i=0; i<ghostCount; i++)
        {
            var ghost = Instantiate(ghostPrefab);
            ghost.transform.parent = efficeShowCionment;
            ghost.transform.position = this.transform.position;
            ghost.transform.localScale = Vector3.one* 1.7f;
            itemList.Add(ghost);
        }
       StartCoroutine(PlayJumpAniamtion(target, callBanck));
        Debug.Log("飞行完成");

    }
    private IEnumerator PlayJumpAniamtion(Transform target,Action callBanck = null)
    {
        for (int i = itemList.Count-1; i >= 0; i--)
        {
            var ghost = itemList[i].transform;
            ghost.DOJump(target.position,2f,1,1f);
            yield return new WaitForSeconds(0.03f);

        }

        yield return new WaitForSeconds(1.6f);
        ClearList();
        if (callBanck != null)
            callBanck.Invoke();
        yield return null;
    }


    public void PlayFlightAnimation(GameObject prefab, Transform target, int count, Action callBanck = null)
    {
        if (prefab == null)
            return;
        for (int i = 0; i < count; i++)
        {
            var ghost = Instantiate(prefab);
            ghost.transform.parent = this.transform;

           if (startTransfrom.Length != 0 && startTransfrom[i] != null )
                ghost.transform.position = startTransfrom[i].position;
           else
                ghost.transform.position = this.transform.position;

            ghost.transform.localScale = Vector3.one * UnityEngine.Random.Range(0.8f, 1.2f);
            if (i > startTransfrom.Length)
                continue;
            var tempos = startTransfrom[i].position;
            var newPos = new Vector3(
                    UnityEngine.Random.Range(-i * 0.2f, i * 0.2f) + tempos.x,
                    UnityEngine.Random.Range(-i * 0.2f, i * 0.2f) + tempos.y,
                    tempos.z
                );
            ghost.transform.position = newPos;

            itemList.Add(ghost);
        }
        StartCoroutine(PlayMoveAniamtion(target, callBanck));
        Debug.Log("飞行完成");
    }
    private IEnumerator PlayMoveAniamtion(Transform target, Action callBanck = null)
    {
        yield return new WaitForSeconds(0.5f);
        for (int i = itemList.Count - 1; i >= 0; i--)
        {
            var ghost = itemList[i].transform;
            ghost.DOMoveX(target.position.x, 0.6f).SetEase(Ease.OutSine);
            ghost.DOMoveY(target.position.y, 0.6f).SetEase(Ease.OutCirc);
            ghost.DOScale(0.28f, 0.6f);
            yield return new WaitForSeconds(0.03f);

        }

        yield return new WaitForSeconds(1.6f);
       ClearList();
        if (callBanck != null)
            callBanck.Invoke();
        yield return null;
    }


    private void ClearList()
    {
        for (int i = itemList.Count - 1; i >= 0; i--)
        {
            Destroy(itemList[i].gameObject);

        }
        itemList.Clear();
    }
}
