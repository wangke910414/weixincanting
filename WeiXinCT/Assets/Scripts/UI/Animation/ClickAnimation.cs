using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ClickAnimation : MonoBehaviour
{
    public Tween clickTween;
   public void OnClick()
    {
        if (clickTween == null)
        {
            clickTween = this.transform.DOScale(1.1f, 0.15f).SetLoops(2, LoopType.Yoyo);
            clickTween.SetUpdate(true);


        }
        else if (!clickTween.active)
        {

            clickTween = this.transform.DOScale(1.1f, 0.15f).SetLoops(2, LoopType.Yoyo);
            clickTween.SetUpdate(true);


        }
    }
    public void OnClickStyleShake()
    {
        this.transform.DOShakeScale(0.6f, 0.2f, 1).SetEase(Ease.InOutSine);
    }
}
