using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationAnimation : MonoBehaviour
{
    private float rotae = 0f;
    void Update()
    {
        if (this.gameObject.activeSelf)
        {
            rotae += Time.deltaTime * 100;
            this.transform.localEulerAngles = new Vector3(0, 0, rotae);
        }
    }
}
