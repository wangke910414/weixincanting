using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LevelInforDeviceUp : MonoBehaviour
{
    public LevelInfoPlane levelInforPlane; 

    private int currDeviceGrade;
    private bool isSelect;
    private bool isLock;
    private string deviceName;
    private int deviceUpPrice;
    private int unLockLevel;
    private int deviceIndex;
    private string desrible;
    private int minCount;
    private int maxCount;
    private int makeTime;
  
    [SerializeField]
    private Image selectImg;
    [SerializeField]
    private Image[] GradeImg;
    [SerializeField]
    private Image lockImg;
    [SerializeField]
    private Image deviceImg;

    private List<Sprite> deviceImgList;
    
    public int CurrDeviceGrade { get => currDeviceGrade; set => currDeviceGrade = value; }
    public bool IsLock { get => isLock; set => isLock = value; }
    public string DeviceName { get => deviceName; set => deviceName = value; }
    public int DeviceUpPrice { get => deviceUpPrice; set => deviceUpPrice = value; }
    public int UnLockLevel { get => unLockLevel; set => unLockLevel = value; }
    public Image DeviceImg { get => deviceImg; }
    public int DeviceIndex { get => deviceIndex; set => deviceIndex = value; }

    public List<Sprite> DeviceImgList { set => deviceImgList = value; get => deviceImgList; }

    public bool IsSelect { get => isSelect; set => isSelect = value; }

    public Image SelectImg { get => selectImg;  }

    public string Desrible { get => desrible; set => desrible = value; }

    public int MaxCount { get => maxCount; set => maxCount = value; }
    public int MinCount { get => minCount; set => minCount = value; }

    public int MakeTime { get => makeTime; set => makeTime = value; }

    // Start is called before the first frame update

    public void OnclickSelect()
    {
        if (levelInforPlane != null )
        levelInforPlane.CallSeletDevice(deviceIndex);
    }

    private void updateFoodGrade()
    {
        for (int i = 0; i < GradeImg.Length; i++)
        {
            if (i < currDeviceGrade)
                GradeImg[i].gameObject.SetActive(true);
            else
                GradeImg[i].gameObject.SetActive(false);

        }

        deviceImg.sprite = deviceImgList[currDeviceGrade - 1];
    }
    public void UpdataShow()
    {
        if (isLock)
            lockImg.gameObject.SetActive(false);
        else
            lockImg.gameObject.SetActive(true);

        updateFoodGrade();
    }
}
