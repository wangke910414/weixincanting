using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class HomePlanel : MonoBehaviour
{
    private HomeMapUiManager uiManager;
    [SerializeField]
    private Image headImage;
    [SerializeField]
    private GameObject heplBg;

    [Header("Data UI")]
    [SerializeField]
    private TextMeshProUGUI cionText;
    [SerializeField]
    private TextMeshProUGUI diamondText;
    [SerializeField]
    private TextMeshProUGUI energyTimeText;
    [SerializeField]
    private TextMeshProUGUI energyCountText;
    [SerializeField]
    private Image associationSkin;
    [SerializeField]
    private Image associationTextBg;
    [SerializeField]
    private TextMeshProUGUI associationText;
    [SerializeField]
    private Transform cionTransform;
    private float saveStrenghtTime = 0;

    private TimeSpan timeSpan;
    private bool isRefrashTime = true;


    public Transform CionTransform { get => cionTransform; }
    public void Confgur(HomeMapUiManager uiManager)
    {
        this.uiManager = uiManager;
        Init();
    }
    public void Init()
    {
        if (PlayerData.playerDataParame.headID != " ")
        {
            HeadAssetConfigur head = SystemDataFactoy.instans.GetResourceConfigur("HeadAssetConfigur") as HeadAssetConfigur;
            foreach (HeadAssetData item in head.HeadAssetDataList)
            {
                if (item.headID == PlayerData.playerDataParame.headID)
                {
                    headImage.sprite = item.headImage;
                    uiManager.HeadPlanel.CurrHeadData = item;
                }
            }
        }


    }

    private void OnDisable()
    {
        heplBg.SetActive(false);
        isRefrashTime = true;
    }

    public void OnClickDanmondPlanel()
    {
        uiManager.DamonPlane.gameObject.SetActive(true);
    }
    public void OnClickHeadPlanel()
    {
        uiManager.HeadPlanel.OpenUiWo();
    }

    //普通商城
    public void OnClickShoppPlanel()
    {
        uiManager.ShoppPlanel.ShowIndex = 0;
        uiManager.ShoppPlanel.OpenUiWo();
        uiManager.ShoppPlanel.OnClickShowBut(0);
    }
    //砖石
    public void OnClickShoppDiamondPlanel()
    {
        uiManager.ShoppPlanel.ShowIndex = 1;
        uiManager.ShoppPlanel.OpenUiWo();
        uiManager.ShoppPlanel.OnClickShowBut(1);
    }

    //打开开面板显示
    public IEnumerator WaitShow()
    {
        yield return new WaitForSeconds(0.3f);
        UpdateShowText();
    }
    //东动画后更行
    public void RefeshPanel()
    {
        AnimationEndRefash();
    }

    private void AnimationEndRefash()
    {
        Debug.Log("动画刷新面板钻石个数！" + PlayerData.playerDataParame.diamond);
        cionText.text = PlayerData.playerDataParame.cions.ToString();
        diamondText.text = PlayerData.playerDataParame.diamond.ToString(); ;
        energyCountText.text = PlayerData.playerDataParame.energy.ToString();
        var levelGrade = PlayerData.associationMenber.levelGrade;

        associationSkin.sprite = associationSkin.GetComponent<CompomentSkin>().compnentSkin[levelGrade];
        if (levelGrade != 0)
        {
            associationTextBg.sprite = associationTextBg.GetComponent<CompomentSkin>().compnentSkin[levelGrade - 1];

        }
        associationText.text = "Lv" + levelGrade;

    }
    private void UpdateShowText()
    {
        if (PlayerData.playerDataParame == null)
        {
            Debug.Log("退出开始刷新面板   " + PlayerData.playerDataParame.diamond);
            return;
        }
        Debug.Log("开始刷新面板    " + PlayerData.playerDataParame.diamond);

        cionText.text = (PlayerData.playerDataParame.cions - PlayerData.instance.currSingleLevelData.cion).ToString();
        diamondText.text = PlayerData.playerDataParame.diamond.ToString();
        energyCountText.text = PlayerData.playerDataParame.energy.ToString() + "/" + uiManager.StrenghtSupplementPanel.GetStrengtPophLimt().ToString();
        var levelGrade = PlayerData.associationMenber.levelGrade;

        if (PlayerData.instance.strenghtLimitCount != uiManager.StrenghtSupplementPanel.GetStrengtPophLimt())
            PlayerData.instance.strenghtLimitCount = uiManager.StrenghtSupplementPanel.GetStrengtPophLimt();

        associationSkin.sprite = associationSkin.GetComponent<CompomentSkin>().compnentSkin[levelGrade];
        if (levelGrade != 0)
        {
            associationTextBg.sprite = associationTextBg.GetComponent<CompomentSkin>().compnentSkin[levelGrade - 1];
        }
        associationText.text = "Lv" + levelGrade;
        this.gameObject.SetActive(true);

    }
    public void OnClickTaskPlanel()
    {
        uiManager.AchievePanel.OpenUiWo();
    }


    public void SetHeadImage(HeadAssetData headData)
    {
        headImage.sprite = headData.headImage;
        PlayerData.playerDataParame.headID = headData.headID;
        PlayerData.instance.PlayerDataParameSave();
        headImage.SetNativeSize();
    }
    public void OnCkuckHelp()
    {
        if (heplBg.activeSelf == false)
            heplBg.SetActive(true);
        else
            heplBg.SetActive(false);
    }
    public void OnClickAbout()
    {
        uiManager.OpenAboutPlanl();
    }
    public void OnClickSeting()
    {
        uiManager.OpenSetingPlanel();
    }
    public void OnClickSigin()
    {
        uiManager.SignPlanel.OpenUiWo();
    }
    public void OnClickDaily()
    {
        uiManager.DailyPanel.OpenUiWo();
    }
    public void TeachintMsgOperation(EventTeachingMsg msg)
    {

    }
    public void OnClickAssocriationPanel()
    {
        uiManager.AssocriationPanel.OpenUiWo();
    }
    public void OnClickMapPanel()
    {
        uiManager.MiniMapPanel.OpenUiWo();
    }
    public void OnClickStrengthPanel()
    {
        uiManager.StrenghtSupplementPanel.OpenUiWo();
    }


    private void Update()
    {
        StrenghtSupplement();
        if (uiManager.StartHomeManager.PlayGameLevelCount == 0)
        {
            UpdateShowText();
        }

    }

    //时间体力补充

    private void StrenghtSupplement()
    {
        if (PlayerData.playerDataParame == null)
            return;
        if (PlayerData.instance.playerLoacadData == null)
            return;

        if (PlayerData.playerDataParame.energy < uiManager.StrenghtSupplementPanel.GetStrengtPophLimt())
        {
            if (PlayerData.instance.playerLoacadData.strengthUseTime == string.Empty)
            {
                PlayerData.instance.playerLoacadData.strengthUseTime = DateTime.Now.ToString();
            }
///

            timeSpan = DateTime.Now.Subtract(DateTime.Parse(PlayerData.instance.playerLoacadData.strengthUseTime)).Duration();

            if ((timeSpan.Minutes * 60 + timeSpan.Seconds) >= (25 * 60))
            {
                var tmepCount = (int)(timeSpan.Minutes / 25);
                PlayerData.playerDataParame.energy += tmepCount;
                if (PlayerData.playerDataParame.energy > uiManager.StrenghtSupplementPanel.GetStrengtPophLimt())
                    PlayerData.playerDataParame.energy = uiManager.StrenghtSupplementPanel.GetStrengtPophLimt();
               PlayerData.instance.playerLoacadData.strengthUseTime = DateTime.Now.ToString();
            }
            else
            {
                saveStrenghtTime += Time.deltaTime;
                var showTiem = (25 * 60) - (timeSpan.Seconds + timeSpan.Minutes * 60);
             
                int minute = (int)showTiem / 60;
                int second = (int)showTiem % 60;
                var mStr = minute >= 10 ? minute.ToString() : "0" + minute.ToString();
                var sStr = second >= 10 ? second.ToString() : "0" + second.ToString();

                energyTimeText.text = mStr + ":" + sStr;
                if (showTiem <= 0)
                {
                    //PlayerData.instance.playerLoacadData.strengthConsumeTime = 0f;
                    PlayerData.playerDataParame.energy++;
                }

                if (saveStrenghtTime >= 5f)
                {
                    PlayerData.instance.SavePlayLocadData();
                    saveStrenghtTime = 0f;

                }
            }
        }
        if (PlayerData.playerDataParame.energy >= uiManager.StrenghtSupplementPanel.GetStrengtPophLimt())
        {
            PlayerData.instance.playerLoacadData.strengthUseTime = string.Empty;
        }
    }
}