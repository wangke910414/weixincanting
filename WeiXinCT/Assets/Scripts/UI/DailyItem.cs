using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public enum RefreshType
{ 
    Null,
    DiamondRefresh,
    AdbRefresh,
}



public class DailyItem : MonoBehaviour
{
    private DailyPanel dailyPanel;
    private int dailyIndex = 0;

    [SerializeField]
    private TextMeshProUGUI decribleText;
    [SerializeField]
    private Image prizeImg;
    [SerializeField]
    private TextMeshProUGUI prizeCountText;

    [SerializeField]
    private Image progessImg;
    [SerializeField]
    private TextMeshProUGUI progessText;
    [SerializeField]
    private Button[] refreshBtn;
    [SerializeField]
    private Button receiveBtn;
    [SerializeField]
    private Transform compeletImg;
    [SerializeField]
    private DiffuseClosureAnimation animationEffice;
    [SerializeField]
    private Transform LightEffice;

    private int maxtargerCount;
    private PrizeComponent prize;
    private bool isCompelet;

    private DailyTaskBase dailyComponent;

    private RefreshType refreshType;

    private int compeltState = 0;

    public PrizeComponent Prize { get => prize; set => prize = value; }
    public TextMeshProUGUI DecribleText { get => decribleText; }
    public TextMeshProUGUI PrizeCountText { get => prizeCountText; }
    public Image ProgessImg { get => progessImg; set => progessImg = value; }
    public TextMeshProUGUI ProgessText { get => progessText; }
    public Image PrizeImg { get => prizeImg; }
    public int TargerCount { get => maxtargerCount; set => maxtargerCount = value; }

    public DailyPanel DailyPanel { set => dailyPanel = value; }
    public int DailyIndex { get => dailyIndex; set => dailyIndex = value; }
    public DailyTaskBase DailyComponent { get => dailyComponent; set => dailyComponent = value; }
    public RefreshType RefreshType { get => refreshType; set => refreshType = value; }
    public int CompeltState { get => compeltState; set => compeltState = value; }
    public DiffuseClosureAnimation AnimatiopnEffice { get => animationEffice; }

    private void ShowBtn()
    {
        if (isCompelet)
        {
            refreshBtn[0].gameObject.SetActive(false);
            refreshBtn[1].gameObject.SetActive(false);
        }
        else if (!isCompelet)
        {
            // PlayerData
        }
    }

    public void UpdateShowItem()
    {
    
        progessText.text = dailyComponent.currTargessCount + "/" + dailyComponent.targerCount;
        progessImg.fillAmount = (float)dailyComponent.currTargessCount / (float)dailyComponent.targerCount;

        if(compeltState == 0) //不能领取
        {
            UpadateBtn();
            LightEffice.gameObject.SetActive(false);

        }
       else if (compeltState ==1)//科领取
        {
             refreshBtn[0].gameObject.SetActive(false);
             refreshBtn[1].gameObject.SetActive(false);
            LightEffice.gameObject.SetActive(true);
        }
        else if (compeltState == 2) //完成
        {
            compeletImg.gameObject.SetActive(true);
           refreshBtn[0].gameObject.SetActive(true);
           refreshBtn[1].gameObject.SetActive(true);
            LightEffice.gameObject.SetActive(false);
        }
    }
    public void RangeRefesh()
    {
        var randRefreshh = Random.Range(1, 4);
        refreshType = randRefreshh > 2 ? RefreshType.DiamondRefresh : RefreshType.AdbRefresh;
    }

    public void UpadateBtn()
    {
        if (refreshType == RefreshType.AdbRefresh)
        {
            refreshBtn[0].gameObject.SetActive(false);
            refreshBtn[1].gameObject.SetActive(true);
        }
        else if (refreshType == RefreshType.DiamondRefresh)
        {
            refreshBtn[0].gameObject.SetActive(true);
            refreshBtn[1].gameObject.SetActive(false);
        }
    }

    public void RestetDailyItem()
    {
        progessText.text = dailyComponent.currTargessCount + "/" + dailyComponent.targerCount;
        progessImg.fillAmount = (float)dailyComponent.currTargessCount / (float)dailyComponent.targerCount;
        compeletImg.gameObject.SetActive(false);
    }

    public void OnClickRefresh()
    {

        dailyPanel.DiamondRefreshTask(dailyIndex);
        compeletImg.gameObject.SetActive(false);
    }
    public void OnClickAdbRefresh()
    {
        dailyPanel.AdbRefeshDailyItem(dailyIndex);
        compeletImg.gameObject.SetActive(false);
    }
    //
    public void OnClickRecerve()
    {
       // compeletImg.gameObject.SetActive(true);
        UpadateBtn();
        dailyPanel.CallCompeletReceive(dailyIndex);
    }
    public void OnCompeletCallBack()
    {
        compeletImg.gameObject.SetActive(true);
    }
   
}
