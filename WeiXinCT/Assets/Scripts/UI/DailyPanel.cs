using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using LitJson;
using DG.Tweening;
//using System;

public class DailyPanel : UiWoPropAbility,CTinterface
{
    [SerializeField]
    private List<DailyItem> dailyItemList = new List<DailyItem>();
    private List<DailyTaskBase> dailyDataList = new List<DailyTaskBase>();
    private List<int> RangeListIndexList = new List<int>();
    HomeMapUiManager uiManager;
    [SerializeField]
    private Image resetBnt;
    [SerializeField]
    private Image AdbBnt;
    private float showTime = 0;

    public List<DailyTaskBase> DailyDataList { get => dailyDataList; }
    [SerializeField]
    private TextMeshProUGUI timeText;
    [SerializeField]
    private Slider slider;
    [SerializeField]
    private TextMeshProUGUI sliderText;
    //[SerializeField]
    //private ObtainTip obrainTip;
    [SerializeField]
    private Transform cionContent;
    [SerializeField]
    private TextMeshProUGUI cionText;
    [SerializeField]
    private Transform diamondContent;
    [SerializeField]
    private TextMeshProUGUI diamondText;
    [SerializeField]
    private GhostFlightAnimation obtainAnimation;

    private int compeletCount;
    private DailyTaskBase totalTask;
    private DailyItem reciveDayliTstk;
    private int diamondRefreshIndex = -1;
   // private int adbRefreshIndex = -1;
    private bool isFastOpen = false;

    private DailyTaskConfigur dailyConfigur;
    public void Init(HomeMapUiManager uiManager)
    {
        this.uiManager = uiManager;
        //obrainTip.callDlg = this;
       // InitPanle();
    }

    public override void OpenUiWo()
    {
       InitPanle();
        HideDailuItem();
        base.OpenUiWo();
    }


    private void InitPanle()
    {
        cionText.text = PlayerData.playerDataParame.cions.ToString();
        Debug.Log("每日金币 " + PlayerData.playerDataParame.cions);
        diamondText.text = PlayerData.playerDataParame.diamond.ToString();
        Debug.Log("每日钻石  " + PlayerData.playerDataParame.diamond);

        dailyConfigur = SystemDataFactoy.instans.GetResourceConfigur("DailyTaskConfigur") as DailyTaskConfigur;
       if (RestTaksManagerData())
        {
            RangeDailyTask();
            UpdataDaily();
            CheckTask();
            SaveShowItemSave();
            Debug.Log("每日任务1   ");
        }
       else
        {
            Debug.Log("每日任务   " );
            for (int i=0; i<PlayerData.instance.dailyTaskItemList.dailyTaskSaveList.Count; i++)
            {
                var item = PlayerData.instance.dailyTaskItemList.dailyTaskSaveList[i];
                if (item == null)
                    continue;
                int dailyType =(int) item.dailyType;
                int index = item.TakeListIndex;
                var task  = GetDailyTask(dailyType, index);
                if (task != null)
                {
                    dailyDataList.Add(task);
                    RangeListIndexList.Add(dailyType);
                }

                if (i >= dailyItemList.Count)
                    continue;

                dailyItemList[i].DecribleText.text = dailyDataList[i].describle;
                dailyItemList[i].PrizeCountText.text = dailyDataList[i].prize.PrizeCount.ToString();
                dailyItemList[i].TargerCount = dailyDataList[i].targerCount;
                dailyItemList[i].PrizeImg.sprite = dailyDataList[i].prize.prizeImg;
                dailyItemList[i].Prize = dailyDataList[i].prize;
                dailyItemList[i].DailyIndex = i;
                dailyItemList[i].RefreshType = item.refreshType;
                dailyItemList[i].DailyComponent = dailyDataList[i];
                dailyItemList[i].DailyPanel = this;
                dailyItemList[i].CompeltState = item.DailyTaskStart;
                //检查完成
                var isCompelet = dailyItemList[i].DailyComponent.OpretaHandle();

              ///  Debug.Log("每日任务 isCompelet   " + isCompelet +"   " +i );
                if (isCompelet)
                {
                    if (dailyItemList[i].CompeltState == 0)
                        dailyItemList[i].CompeltState = 1;
                   // Debug.Log("成功每日任务   " + dailyItemList[i].CompeltState);
                }
                dailyItemList[i].UpdateShowItem();

                dailyItemList[i].gameObject.SetActive(false);
            }
           // CheckTask(); 意
        }
      
         compeletCount = PlayerData.instance.dailyTaskItemList.compeletCount;
        //compeletCount = 9;
        slider.value = (float)compeletCount / 9;
        sliderText.text = compeletCount.ToString() + "/" + 9.ToString();
        if (slider.value >= 1)
        {
            //OpenObtainDlg();
        }
    }

    public override void OpenEnd()
    {
      
        CoroutineHandler.StartStaticCoroutine(OpenPanelAniamtion());
    }
    private IEnumerator OpenPanelAniamtion()
    {
        for (int i=0; i< dailyItemList.Count; i++)
        {
            dailyItemList[i].gameObject.SetActive(true);
            dailyItemList[i].transform.DOShakeScale(0.2f, 0.2f, 1).SetEase(Ease.Flash);
            yield return  new  WaitForSeconds(0.16f);
        }
    }
    private void HideDailuItem()
    {
        for (int i=0; i<dailyItemList.Count; i++)
        {
            dailyItemList[i].transform.gameObject.SetActive(false);
        }
    }


    private void RangeDailyTask()
    {
        dailyDataList.Clear();
        var rand = Random.Range(1, 6);
        RangeListIndexList.Add(rand);
        var nextIndex = (rand + 1) >= 6 ? 1 : rand + 1;
        RangeListIndexList.Add(nextIndex);
        var nextIndex2 = (nextIndex + 1) >= 6 ? 1 : nextIndex + 1;
        RangeListIndexList.Add(nextIndex2);

        //Debug.Log("任务随机数   " + RangeListIndexList[0] + "    " + RangeListIndexList[1] + "   " + RangeListIndexList[2]);
        for (int i = 0; i < RangeListIndexList.Count; i++)
        {
            var task = GetDailyTask(RangeListIndexList[i]);
            if (task != null)
            {
                dailyDataList.Add(task);
            } 
        }
        PlayerData.instance.SetTaskDitValueNull();
    }
    private void ReaderDailyTaskData()
    {

    }
    private DailyTaskBase GetDailyTask(int typeIndex)
    {

        if (typeIndex == 1)
        {
            var list = dailyConfigur.continuSubmitTakList;
            var index = Random.Range(0, list.Count);
            return list[index];
        }
        else if (typeIndex == 2)
        {
            var list = dailyConfigur.submitFoodTaskLiat;
            var index = Random.Range(0, list.Count);
            return list[index];
        }
        else if (typeIndex == 3)
        {
            var list = dailyConfigur.consumePropTaskList;
            var index = Random.Range(0, list.Count);
            return list[index];
        }
        else if (typeIndex == 4)
        {
            var list = dailyConfigur.levelTaskList;
            var index = Random.Range(0, list.Count);
            return list[index];
        }
        else if (typeIndex == 5)
        {
            var list = dailyConfigur.currencyTaskList;
            var index = Random.Range(0, list.Count);
            return list[index];
        }
        return null;
    }
    private DailyTaskBase GetDailyTask(int dailyTskType, int index)
    {
        if (dailyTskType == 1)
        {
            var list = dailyConfigur.continuSubmitTakList;
            return list[index];
        }
        else if (dailyTskType == 2)
        {
            var list = dailyConfigur.submitFoodTaskLiat;
            return list[index];
        }
        else if (dailyTskType == 3)
        {
            var list = dailyConfigur.consumePropTaskList;
            return list[index];
        }
        else if (dailyTskType == 4)
        {
            var list = dailyConfigur.levelTaskList;
            return list[index];
        }
        else if (dailyTskType == 5)
        {
            var list = dailyConfigur.currencyTaskList;
            return list[index];
        }
        return null;
    }

    private void UpdataDaily()
    {
        for (int i = 0; i < dailyItemList.Count; i++)
        {
            dailyItemList[i].DecribleText.text = dailyDataList[i].describle;
            dailyItemList[i].PrizeCountText.text = dailyDataList[i].prize.PrizeCount.ToString();
            dailyItemList[i].TargerCount = dailyDataList[i].targerCount;
            dailyItemList[i].PrizeImg.sprite = dailyDataList[i].prize.prizeImg;
            dailyItemList[i].Prize = dailyDataList[i].prize;
            dailyItemList[i].DailyIndex = i;
            dailyItemList[i].RangeRefesh();
            dailyItemList[i].DailyComponent = dailyDataList[i];
            dailyItemList[i].DailyPanel = this;
        }
        
    }

    /// <summary>
    /// 刷新每日任务
    /// </summary>

    public void Update()
    {

        showTime += Time.deltaTime;
        if (showTime >= 1)
        {
            if (PlayerData.instance == null)
                return;
            var taskStartTime = PlayerData.instance.dailyTaskItemList.StartTime;
            System.TimeSpan span = System.DateTime.Now.Subtract(System.DateTime.Parse(taskStartTime).AddDays(1)).Duration();

            timeText.text = span.Hours + "时" + span.Minutes + "分";
            showTime = 1;
        }
    }

    //广告刷新数据
    public void AdbRefeshDailyItem(int index)
    {
        bool b = true;   //广告接口

        if (b)
        {
            var nextTask = dailyItemList[index];
            if (nextTask != null)
            {
                var animation = nextTask.transform.GetComponent<ZoomInAndOutAnimation>();
                animation.PlayerAnimation();
            }
            StartCoroutine(WaitTaskRefresh(index));
        }
      
    
    }

    private IEnumerator WaitTaskRefresh(int index)
    {
       
        yield return new WaitForSeconds(0.3f);
  

        List<int> tempIndex = new List<int>();
        for (int i = 1; i < 6; i++)
        {
            if (i != RangeListIndexList[0] && i != RangeListIndexList[1] && i != RangeListIndexList[2])
                tempIndex.Add(i);
        }
        Debug.Log("  tempIndex  " + tempIndex.Count);
        var rand = Random.Range(0, tempIndex.Count);
        var newTask = tempIndex[rand];
        RangeListIndexList[index] = newTask;
        UpdateDailyTask(index, dailyDataList[index]);

        Debug.Log("  newTask  " + newTask);
        var task = GetDailyTask(newTask);
        UpdateDailyTask(index, task);
        SaveShowItemSave(index, task);
    }

    //钻石刷新
    public void DiamondRefreshTask(int index)
    {
        Debug.Log("点击钻石购买   ");
        if ((PlayerData.playerDataParame.diamond - 5) > 0)
        {
            string msg = "确定使用钻石！";
            TipDiamondComsume buff = new TipDiamondComsume();
            buff.tipDlgType = TipDlgType.DimaondCofirmTip;
            buff.isSuccesss = true;
            uiManager.TipDlgManager.OpenTipDlg(TipDlgType.DimaondCofirmTip, this, msg, buff);
            diamondRefreshIndex = index;
        }
        else
        {
            isFastOpen = true;
            CloseUiWo();
        } 
    }

    private void DitermineDiamondRefresh()
    {
        if (diamondRefreshIndex == -1)
            return;

        var nextTask = dailyItemList[diamondRefreshIndex];
        if (nextTask == null)
            return;
        var animation = nextTask.transform.GetComponent<ZoomInAndOutAnimation>();
        animation.PlayerAnimation();

        StartCoroutine(WaitTaskRefresh(diamondRefreshIndex));
        PlayerDataDiamondValueOprate(-5);
        diamondRefreshIndex = -1;
    }

    private void UpdateDailyTask(int index, DailyTaskBase task)
    {
        PlayerData.instance.SetDitElementValueNull(task.dailyKey);
        dailyItemList[index].DecribleText.text = task.describle;
      
        dailyItemList[index].PrizeCountText.text = task.prize.PrizeCount.ToString();
        dailyItemList[index].TargerCount = task.targerCount;
        dailyItemList[index].PrizeImg.sprite = task.prize.prizeImg;
        dailyItemList[index].DailyComponent = task;
        dailyItemList[index].Prize = task.prize;
        dailyItemList[index].RangeRefesh();
        Debug.Log("新的   " + task.dailyKey  + "   " + task.describle);
        PlayerData.instance.SetDitElementValueNull(task.describle);
        dailyItemList[index].DailyComponent.OpretaHandle();
        dailyItemList[index].UpdateShowItem();
        dailyItemList[index].RestetDailyItem();
        dailyItemList[index].UpadateBtn();
        dailyDataList[index] = task;

    }

    public void SaveShowItemSave()
    {
        for (int i=0; i<3; i++)
        {
    
            DailyTaskSaveItem item = new DailyTaskSaveItem();
            item.dailyType =(int) dailyDataList[i].dailyTaskType;
            item.TakeListIndex = dailyDataList[i].daiyTaskID;
            item.refreshType = dailyItemList[i].RefreshType;
            PlayerData.instance.dailyTaskItemList.StartTime = System.DateTime.Now.ToString(); ;
            PlayerData.instance.dailyTaskItemList.dailyTaskSaveList.Add(item);
        }
        PlayerData.instance.SaveTaskListData();
    }
    private void SaveShowItemSave(int index, DailyTaskBase task)
    {
        DailyTaskSaveItem item = new DailyTaskSaveItem();
        PlayerData.instance.dailyTaskItemList.dailyTaskSaveList[index].dailyType = (int)task.dailyTaskType;
        PlayerData.instance.dailyTaskItemList.dailyTaskSaveList[index].TakeListIndex = task.daiyTaskID;
        PlayerData.instance.dailyTaskItemList.dailyTaskSaveList[index].refreshType = dailyItemList[index].RefreshType;
        PlayerData.instance.dailyTaskItemList.dailyTaskSaveList[index].DailyTaskStart = 0;
        PlayerData.instance.SaveTaskListData();
    }

    private bool RestTaksManagerData()
    {
        var taskStartTime = PlayerData.instance.dailyTaskItemList.StartTime;
        if (taskStartTime != null)
        {
            System.TimeSpan span = System.DateTime.Now.Subtract(System.DateTime.Parse(taskStartTime)).Duration();
            if (span.Days >= 1 || span.Hours >= 24)
            {
                return true;
            }
        }
        else if (taskStartTime != "")
        {
            return true;
        }
        return false;
    }

    public void CheckTask()
    {
        for (int i=0; i<3; i++)
        {
            var isCompelet = dailyItemList[i].DailyComponent.OpretaHandle();
            if (isCompelet)
            {

            }
            else
            {

            }
            dailyItemList[i].UpdateShowItem();
        }
    }
    public void CallCompeletReceive(int indexID)
    {
        PlayerData.instance.PlayerDataParameSave();
        compeletCount++;
        PlayerData.instance.dailyTaskItemList.compeletCount = compeletCount;
        slider.value = (float)compeletCount / 9;
        sliderText.text = compeletCount.ToString() + "/" + 9.ToString();
        PlayerData.instance.dailyTaskItemList.dailyTaskSaveList[indexID].DailyTaskStart = 2;
        PlayerData.instance.SaveTaskListData();
        var task = dailyItemList[indexID];

        slider.value = 1;
        if (slider.value >= 1)
        {
            ObtainGeneral ent = new ObtainGeneral();
            ent.tipDlgType = TipDlgType.ObtainNoPropTip;
            ent.prize = task.DailyComponent.prize;
            reciveDayliTstk = task;
            if (uiManager != null)
                uiManager.TipDlgManager.OpenTipDlg(TipDlgType.ObtainNoPropTip, this, null, ent);

        }
    }

    public override void TipDlgExitCallBanck(TipDlg tipDlg, TipDlgType type, bool success)
    {
        if (this.gameObject.activeSelf == false)
            return;
        if (type == TipDlgType.ObtainNoPropTip)
        {
            if (reciveDayliTstk != null )
            {
                ObtainNoPropTip dlg = tipDlg as ObtainNoPropTip;
                if (dlg == null)
                {
                    Debug.LogError("每日任务获得返回报错");
                    return;
                }
                PlayObtainAnimation(dlg);
            }
        }
        else if (type == TipDlgType.DimaondCofirmTip)
        {
            DitermineDiamondRefresh();
        }
    }


    private void PlayObtainAnimation(ObtainNoPropTip dlg)
    {
        ObtainPirzeOprate(reciveDayliTstk.DailyComponent);

        var ghAnimation = obtainAnimation.GetComponent<GhostFlightAnimation>();
        obtainAnimation.transform.position = dlg.PrizeTransfrom.position;

        if (reciveDayliTstk.Prize.prizeType == PrizeType.Cion)
        {
            cionContent.transform.parent. gameObject.SetActive(true);
           obtainAnimation.PlayFlightAnimation(dlg.PrizeTransfrom.gameObject, cionContent,6, () => {
               cionText.text = PlayerData.playerDataParame.cions.ToString();
               StartCoroutine(WaitActiv());
               reciveDayliTstk.OnCompeletCallBack();
           });
        }

        else if (reciveDayliTstk.Prize.prizeType == PrizeType.Diamond)
        {
            diamondContent.transform.parent.gameObject.SetActive(true);
            obtainAnimation.PlayFlightAnimation(dlg.PrizeTransfrom.gameObject, diamondContent,6,() => {
                diamondText.text = PlayerData.playerDataParame.diamond.ToString();
                StartCoroutine(WaitActiv());
                reciveDayliTstk.OnCompeletCallBack();
            });

        }
        else if (reciveDayliTstk.Prize.prizeType == PrizeType.ComsumeProp)
        {
            Debug.Log("播放道具动画！");
            StartCoroutine( reciveDayliTstk.AnimatiopnEffice.PlayAnimation(dlg.PrizeTransfrom.gameObject, ()=> {
                reciveDayliTstk.OnCompeletCallBack();
            }));
        }
    }
    private IEnumerator WaitActiv()
    {
        yield return new WaitForSeconds(1);
        diamondContent.transform.parent.gameObject.SetActive(false);
        cionContent.transform.parent.gameObject.SetActive(false);
    }

    //private void OpenObtainDlg()
    //{
    //    var totalTaskindex = Random.Range(1, 6);
    //        totalTask = GetDailyTask(totalTaskindex);
    //    //obrainTip.PrizeImg.sprite = totalTask.prize.prizeImg;
    //    //obrainTip.PrizeCountText.text = totalTask.prize.PrizeCount.ToString();
    //    //obrainTip.OpenDlg();
    //}

    //public void CallBanck() 
    //{
    //    compeletCount = 0;
    //    slider.value = (float)compeletCount / 9;
    //    ObtainPirzeOprate(totalTask);
    //}

    public override void CloseUiWo()
    {
        uiManager.RefreshPanel();
        base.CloseUiWo();
    }
    public override void CloseEnd()
    {
        if (isFastOpen)
        {
            isFastOpen = false;
            uiManager.FastOpenDiamondShopDlg(this.gameObject);
        }
        base.CloseEnd();
    }

    private void ObtainPirzeOprate(DailyTaskBase task)
    {
        if (task.prize.prizeType == PrizeType.Cion)
        {
            PlayerDataCionVlalueOpertate(task.prize.PrizeCount);
        }
        else if (task.prize.prizeType == PrizeType.Diamond)
        {
            PlayerDataDiamondValueOprate(task.prize.PrizeCount);
            
        }
        else if (task.prize.prizeType == PrizeType.ComsumeProp)
        {
            var str = task.prize.consumePropType.ToString();
            if (PlayerData.consumePropDataParameter.consumePropDataDict.ContainsKey(str))
            {
                var temp = int.Parse(PlayerData.consumePropDataParameter.consumePropDataDict[str]) + 1;
                PlayerData.consumePropDataParameter.consumePropDataDict[str] = temp.ToString();
            }
            else
            {
                PlayerData.consumePropDataParameter.consumePropDataDict.Add(str, 1.ToString());
            }
#if UNITY_EDITOR
            var daata = JsonMapper.ToJson(PlayerData.consumePropDataParameter);
            Debug.Log("保存道具数据    " + daata);
            CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("consumeProp", daata));
#elif !UNITY_EDITOR && UNITY_WEBGL
        var daata = JsonMapper.ToJson(PlayerData.consumePropDataParameter);
        Debug.Log("保存道具数据    " + daata);
       CoroutineHandler.StartStaticCoroutine( PlayerData.instance.SaveData("UpdateConsumeProp", daata));
#endif
        }
    }

    //做事操作
    public void PlayerDataDiamondValueOprate(int number)
    {
        if (number < 0)
        {
            var strName = PlayerData.instance.dailyTaskDataValue.consumeDiamond;
            PlayerData.instance.dailyTaksValueDit[strName] += Mathf.Abs(number);
            PlayerData.instance.achieveParmeter.comsumeDiamondCount += Mathf.Abs(number);
        }

        else if (number > 0)
        {
            var strName = PlayerData.instance.dailyTaskDataValue.obtainDiamond;
            PlayerData.instance.dailyTaksValueDit[strName] += number;
            PlayerData.instance.achieveParmeter.obtanDiamondCount += number;
        }

        PlayerData.playerDataParame.diamond += number;
        PlayerData.instance.PlayerDataParameSave();
        PlayerData.instance.SaveDayliTaskValueData();
        PlayerData.instance.SaveAchieveTaskValueData();

    }

    /// <summary>
    /// 金币操作
    /// </summary>
    /// <param name="number"></param>
    private void PlayerDataCionVlalueOpertate(int number)
    {
        if (number > 0)
        {
            var strName = PlayerData.instance.dailyTaskDataValue.obtainCion;
            PlayerData.instance.dailyTaksValueDit[strName] += number;
            PlayerData.instance.achieveParmeter.obtainCionCount += number;
        }

        PlayerData.playerDataParame.cions += number;
        PlayerData.instance.PlayerDataParameSave();
        PlayerData.instance.SaveDayliTaskValueData();
        PlayerData.instance.SaveAchieveTaskValueData();
    }
}