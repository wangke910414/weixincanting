using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FoodClock : MonoBehaviour
{
    [SerializeField]
    private Transform[] parentNode;

    [SerializeField]
    private Image[] shudleImg;
    private bool isShake = false;

    private float currMakeTime = 0f;

   public void SetClockCoodeState(float value)
    {
        currMakeTime = value;
        if ( currMakeTime >= 0.5f )
        {
            SetShowNode(1);
            shudleImg[1].fillAmount = value;

        }
        else if (currMakeTime < 0.5f )
        {
            SetShowNode(2);
            shudleImg[2].fillAmount = value;
            ShakeAniamtion();
        }
    }
    public void SetClockMakeSate(float value)
    {
        SetShowNode(0);
        shudleImg[0].fillAmount = value;
    }


    private void SetShowNode(int index)
    {
        for (int i=0; i< parentNode.Length; i++)
        {
            if (i==index)
                 parentNode[i].gameObject.SetActive(true);
            else
                parentNode[i].gameObject.SetActive(false);

        }
    }
    private void ShakeAniamtion()
    {
        if (!isShake)
        {
            this.transform.DOShakePosition(1, new Vector3(0, 5, 0));
            isShake = true;

        }
    }
}
