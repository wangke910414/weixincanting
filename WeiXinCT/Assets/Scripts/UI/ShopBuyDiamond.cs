using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;




public class ShopBuyDiamond : MonoBehaviour
{
    private ShoppPlanel shopPanel;
    [SerializeField]
    private TextMeshProUGUI topCountText;
    [SerializeField]
    private TextMeshProUGUI dlgCountText;
    [SerializeField]
    private TextMeshProUGUI priceText;
    public enum BuyType : int
    {
        Null,
        Random,
        Normal,
    }

    [SerializeField]
    private int muliptyCount;
    [SerializeField]
    private BuyType buyType;

    private int count = 1;
    private int muliply = 0;
  
    public ShoppPlanel ShopPanel { set => shopPanel = value; }
    
    
    public void OnClickOk()
    {
        if (buyType == BuyType.Random)
        {
            var diamond = Random.Range(1, 6);
            shopPanel.SmallBtnOnClick(diamond, 0);
        }
       else if (buyType == BuyType.Normal)
        {
            var diamond = int.Parse( topCountText.text);
            var momeny = count * muliply;
            if (shopPanel != null)
            shopPanel.SmallBtnOnClick(diamond, momeny);
            Debug.Log("购买金币数量    " + diamond + "需要人民币   " + momeny) ;
        }
       
    }
    public void ResetItme()
    {
        count = 1;
        if (topCountText != null)
                topCountText.text = (count * muliptyCount).ToString();
        if (dlgCountText != null)
        dlgCountText.text = (count * muliptyCount).ToString();
        muliply = (muliptyCount == 100) ? 30 : 60;
        if (priceText != null)
            priceText.text = "￥" + (count * muliply).ToString();

    }
    public void AddOnClickBtn()
    {
        if (count * muliptyCount > 9000)
        {
            return;
        }
        
        Debug.Log("增加钻石！" + count);
        count++;

        topCountText.text = (count * muliptyCount).ToString();
        dlgCountText.text = (count * muliptyCount).ToString();
        muliply = (muliptyCount == 100) ? 30 : 60;
        priceText.text = "￥" + (count * muliply).ToString();
    }

}
