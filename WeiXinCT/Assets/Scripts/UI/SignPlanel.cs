using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using LitJson;
using UnityEngine.UI;
using DG.Tweening;

using TMPro;
public class SignPlanel : UiWoPropAbility
{ 
    private HomeMapUiManager uiManager;

    [SerializeField]
    private SignItem[] siginItemArray;
    private int singinDayNumber = 0;            //总共签到天数
    private string singinDataTime;              //上次签到时间
    [SerializeField]
    private Image coinImg;
    [SerializeField]
    private TextMeshProUGUI cionText;
    [SerializeField]
    private GhostFlightAnimation obtainAnimation;
    [SerializeField]
    private Transform targetTransfrom;
    [SerializeField]
    private TextMeshProUGUI diamondText;

    

    public void Init(HomeMapUiManager uiManager)
    {
        this.uiManager = uiManager;
        SiginItemReset();
    }
    private void SiginItemReset()
    {
        for (int i=0; i< siginItemArray.Length; i++)
        {
            siginItemArray[i].SignPlanel = this;
            siginItemArray[i].SignIndex = i;
        }
    }
    public override void OpenUiWo()
    {
        ResetHide();
        SetSignInPanelData();

        base.OpenUiWo();
       
    }
    public override void OpenEnd()
    {
        CoroutineHandler.StartStaticCoroutine(ShakOPenAnimation());
    }

    public override void CloseUiWo()
    {
        uiManager.RefreshPanel();
        base.CloseUiWo();
    }

    public void ResetHide()
    {
        for (int i = 0; i < siginItemArray.Length; i++)
        {
            siginItemArray[i].gameObject.SetActive(false);
        }
    }
    private IEnumerator ShakOPenAnimation()
    {
        for (int i = 0; i < siginItemArray.Length; i++)
        {
            siginItemArray[i].gameObject.SetActive(true);
            siginItemArray[i].transform.DOShakeScale(0.2f, 0.2f, 1).SetEase(Ease.Flash);
            yield return new WaitForSeconds(0.06f);
        }
    }

    public void SetSignInPanelData()
    {

        singinDayNumber = PlayerData.playerDataParame.siginDayNumber;
        singinDataTime = PlayerData.playerDataParame.singinDayTime;
        // log.text = singinDayNumber + "   " + singinDataTime;
        if (singinDayNumber >= 7)
        {
            TimeSpan span = DateTime.Now.Subtract(DateTime.Parse(singinDataTime)).Duration();
            if ((int)span.TotalHours >= 24 || (int)span.TotalDays > 0)
            {
                PlayerData.playerDataParame.singinDayTime = "null";
                PlayerData.playerDataParame.siginDayNumber = 0;
                singinDayNumber = 0;
                PlayerData.instance.PlayerDataParameSave();
            }
        }

        for (int i = 0; i < siginItemArray.Length; i++)
        {
            //var b = i < singinDayNumber ? SignItemState.ReceiveCompelet : SignItemState.NoReceive;
            //siginItemArray[i].SetCanReceive(b);
            if (i < singinDayNumber)
            {
                if(PlayerData.playerDataParame.sgindayList[i] == 2)
                {
                    siginItemArray[i].SetCanReceive(SignItemState.ReceiveCompelet);
                }
                if (PlayerData.playerDataParame.sgindayList[i] == 0)
                {
                    siginItemArray[i].SetCanReceive(SignItemState.CanReceive);
                }
            }
        }

        if (singinDataTime == "null")
        {
            siginItemArray[singinDayNumber].SetCanReceive(SignItemState.CanReceive);
        }
        else if (singinDataTime != "null")
        {
            TimeSpan span = DateTime.Now.Subtract(DateTime.Parse(singinDataTime)).Duration();

            TimeSpan currSpan = DateTime.Now.Subtract(DateTime.Parse(singinDataTime)).Duration();
          

            if (Math.Abs((int)currSpan.TotalHours) >= 24 || Math.Abs((int)currSpan.TotalDays) >= 1)
            {
                siginItemArray[singinDayNumber].SetCanReceive(SignItemState.CanReceive);
            }
           Debug.Log("签到时间   " + currSpan);
        }
    }

    public void CallOnClick(int siginIndex)
    {
        PrizeComponent prize = siginItemArray[siginIndex].Prize;
        if (prize.prizeType == PrizeType.Cion)
        {
           // Debug.Log("获得金币  ！" + prize.PrizeCount + "     " + siginIndex);
            cionText.text = PlayerData.playerDataParame.cions.ToString();
            cionText.transform.parent.gameObject.SetActive(true);
            // PlayerData.playerDataParame.cions += prize.PrizeCount;
            PlayerData.instance.PlayerDataValueCionOprate(prize.PrizeCount);
            siginItemArray[siginIndex].PlayObtainAnimation(coinImg.transform,()=> {
                cionText.text = PlayerData.playerDataParame.cions.ToString();
                Invoke("WaitHide", 0.5f);
            });
        }

        else if (prize.prizeType == PrizeType.ComsumeProp)
        {
            Debug.Log("播放道具动画 ");
            var propName = prize.consumePropType.ToString();
            if (PlayerData.consumePropDataParameter.consumePropDataDict.ContainsKey(propName))
            {
                var tempCount = int.Parse(PlayerData.consumePropDataParameter.consumePropDataDict[propName]) + prize.PrizeCount;
                PlayerData.consumePropDataParameter.consumePropDataDict[propName] = tempCount.ToString();
            }
            else
            {
                PlayerData.consumePropDataParameter.consumePropDataDict.Add(propName, prize.PrizeCount.ToString());
            }
            Debug.Log("播放道具动画 0");
            siginItemArray[siginIndex].PlayObtainAnimation(null, () => {

            });
        }
        else if (prize.prizeType == PrizeType.DiamondAddProp)
        {
            //第七天
            var singn = siginItemArray[siginItemArray.Length - 1];
            singn.SetCanReceive(SignItemState.ReceiveCompelet);

            DiamondStoreTipEvent ent = new DiamondStoreTipEvent();
            ent.style = ObatainTipShowType.ObtainSignPanel;
            List<ConsumePropType> prop = new List<ConsumePropType>();
            prop.Add(ConsumePropType.SetlementDoubleCion);
            ent.seletPropList = prop;
            ent.giftCount = 10;
            PlayerData.playerDataParame.cions += 10;
            //随机一个道具
            var rendprop = (ConsumePropType)UnityEngine.Random.Range(1, 9);
            PlayerData.instance.PropOperate(rendprop, 1);

            uiManager.TipDlgManager.OpenTipDlg(TipDlgType.ObttainTip, this,null, ent);
        }

        if  (PlayerData.playerDataParame.sgindayList.Count < singinDayNumber+1)
        {
            PlayerData.playerDataParame.sgindayList.Add(2);
        }
        else
        {
            PlayerData.playerDataParame.sgindayList[singinDayNumber] = 2;
        }
        singinDayNumber++;
        PlayerData.playerDataParame.siginDayNumber = singinDayNumber;
        PlayerData.playerDataParame.singinDayTime = DateTime.Now.ToString();
        PlayerData.instance.PlayerDataParameSave();


    }

    private void WaitHide()
    {
        Debug.Log("进入隐藏");
        if (cionText.transform.parent.gameObject.activeSelf)
             cionText.transform.parent.gameObject.SetActive(false);
        if (diamondText.transform.parent.gameObject.activeSelf)
            diamondText.transform.parent.gameObject.SetActive(false);
    }

    public override void TipDlgExitCallBanck(TipDlg tipDlg, TipDlgType type, bool success)
    {
        Debug.Log("播放动画！1");

        if (!this.gameObject.activeSelf)
            return;

        Debug.Log("播放动画！2  " + type);
        if (type == TipDlgType.ObttainTip)
        {
            ObtainTip dlg = tipDlg as ObtainTip;
            if (dlg == null)
                return;

            PlaySeventhDayAniamtion(dlg);
        }
    }
    private void PlaySeventhDayAniamtion(ObtainTip dlg)
    {
        if (obtainAnimation == null)
            return;
        Debug.Log("播放动画！");
        diamondText.transform.parent.gameObject.SetActive(true);
        var temp = (PlayerData.playerDataParame.diamond - 10 )> 0 ? (PlayerData.playerDataParame.diamond - 10) : 0;
        diamondText.text = temp.ToString();
        obtainAnimation.PlayFlightAnimation(dlg.PrizeImg.gameObject, targetTransfrom, 6,()=>{

            Invoke("WaitHide", 0.5f);

        });
    }
}
