using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class LevelSelect : MonoBehaviour
{
    private int selectLevel = 0;
    // Start is called before the first frame update
    private LevelInfoPlane levelPlanel;

    [Header("UI")]
    [SerializeField]
    private TextMeshProUGUI level_text;

    [SerializeField]
    private int unLockKeyNumber = 0;
    [SerializeField]
    private string levelID;

    [SerializeField]
    private Image selectImg;
    [SerializeField]
    private bool isLock = false;

    [SerializeField]
    private int levelIsShopGruap = 0;

    [SerializeField]
    private Image OffImg;

    [SerializeField]
    private Image OnImg;

    [SerializeField]
    private Transform[] GrupImg;

    [SerializeReference]
    private Transform keyImgNode;
    [SerializeField]
    private Image prizeKeyImg;

    [SerializeField]
    private TextMeshProUGUI subLoackKeyCount;
    private string levelName;
    private string shopName;
    private Transform shelfPort;

    private int playeOnce = 0;

    private TargetType leveltarget;
    private int targetCount;
    private int prizeKeyCunt = 0;
    private int prizeCionCount = 0;
    private GameObject flightKey;       //飞行钥匙
    private bool keyFlightAnimation = false;     //钥匙飞行
    private float shakeKeySpanTiem = 5f;



    public int SeLectLevel { get => selectLevel; set => selectLevel = value; }
    public LevelInfoPlane LevelPlanel { get => levelPlanel; set => levelPlanel = value; }

    public int UnLockKeyNumber { get => unLockKeyNumber; set => unLockKeyNumber = value; }
    public bool IsLock { get => isLock; set => isLock = value; }

    public string LevelID { get => levelID; set => levelID = value; }

    public int LevelIsShopGruap { get => levelIsShopGruap; set => levelIsShopGruap = value; }

    public int PlayOnce { get => playeOnce; set => playeOnce = value; }
    public Image SelectImg { get => selectImg; }

    public TargetType Leveltarget { get => leveltarget; set => leveltarget = value; }
    public int TargetCount { get => targetCount; set => targetCount = value; }

    public string LevelName { get => levelName; set => levelName = value; }

    public string ShopName { get => shopName; set => shopName = value; }

    public int PrizeKeyCunt { get => prizeKeyCunt; set => prizeKeyCunt = value; }

    public int PrizeCionCount { get => prizeCionCount; set => prizeCionCount = value; }
    public Transform ShelfPort { get => shelfPort; set => shelfPort = value; }
    public bool KeyIsFligth { get => keyFlightAnimation; }

    public Transform KeyImgNode { get => keyImgNode; }

    public void SetlevelItem()
    {
        levelIsShopGruap = selectLevel + 1;
        if (levelIsShopGruap < 10)
            level_text.text ="0"+ levelIsShopGruap.ToString();
        else
            level_text.text = levelIsShopGruap.ToString();

    }
    public void SetLevlGrup()
    {
  
        if (playeOnce > 0&& playeOnce<4)
        {
           
            for (int i = 0; i < GrupImg.Length; i++)
            {
                if ((playeOnce - 1) == i)
                {
                    GrupImg[i].gameObject.SetActive(true);
                }
                else
                {
                    GrupImg[i].gameObject.SetActive(false);
                }
            }
        }
        else if (playeOnce >= 4)
        {
            GrupImg[0].gameObject.SetActive(false);
            GrupImg[1].gameObject.SetActive(false);
            GrupImg[2].gameObject.SetActive(true);

        }

        if (isLock)
        {
            keyImgNode.gameObject.SetActive(false);
            OffImg.gameObject.SetActive(false);
            OnImg.gameObject.SetActive(true);
        }
        else
        {
           // keyImgNode.gameObject.SetActive(true);
            subLoackKeyCount.text = (unLockKeyNumber - PlayerData.playerDataParame.keyNumber).ToString();
        }

        var isHaveKey = prizeKeyCunt > 0 ? true : false;
        prizeKeyImg.gameObject.SetActive(isHaveKey);
    }

    public void OnCklic()
    {
        if (!isLock)
            return;


        if (LevelPlanel != null)
        {
            LevelPlanel.SetShowLevelText(selectLevel);
        }
        selectImg.gameObject.SetActive(true);
    }
    public void CreateKeyFlightAniation()
    {
        keyFlightAnimation = true;
        if ( flightKey == null )
        {
            flightKey = Instantiate(prizeKeyImg.gameObject);
            flightKey.transform.SetParent(prizeKeyImg.transform.parent);
            flightKey.transform.localScale = Vector3.one * 2;
            flightKey.transform.position = prizeKeyImg.transform.position;
            flightKey.AddComponent<FlightAnimation>();
        }
    }

    /// <summary>
    /// 需要播放打开动画
    /// </summary>
    public void NeedPlayOpenAnimation()
    {
        if (!keyFlightAnimation)
            return;

        Debug.Log("调用需要动画！");
        keyImgNode.gameObject.SetActive(false);
        prizeKeyImg.gameObject.SetActive(false);
        OffImg.gameObject.SetActive(true);
        OnImg.gameObject.SetActive(true);
    }
    /// <summary>
    /// 打开动画
    /// </summary>
    public void PlayOpenAnimation()
    {
        if (!isLock)
            return;

        prizeKeyImg.gameObject.SetActive(true);
        OffImg.gameObject.SetActive(false);
        OnImg.gameObject.SetActive(true);
        if (prizeKeyCunt != 0)
        {
            prizeKeyImg.gameObject.SetActive(true);
        }

    }

    public void RunKeyFlight(Transform target)
    {
        if (target == null)
        {
            return;
        }
        if (flightKey!= null)
        {
            flightKey.transform.SetParent(target);
            var flight = flightKey.GetComponent<FlightAnimation>();
            if (flight != null)
                flight.FlightJmplityReduction(target,0.5f, FlightEndCallBanck);
        }
    }
    public void FlightEndCallBanck()
    {
        Destroy(flightKey);
        levelPlanel.KeyFlightConpelet();
    }

    private void Update()
    {
        shakeKeySpanTiem -= Time.deltaTime;
        if (shakeKeySpanTiem < 0)
        {
            shakeKeyAnimation();
            shakeKeySpanTiem = 5f;
        }
    }
    private void shakeKeyAnimation()
    {
        if (prizeKeyImg.gameObject.activeSelf)
            prizeKeyImg.transform.DOShakePosition(1, new Vector3(10, 5, 0)).SetEase(Ease.Flash);

        if (keyImgNode.gameObject.activeSelf)
            keyImgNode.transform.DOShakePosition(1, new Vector3(10, 5, 0)).SetEase(Ease.Flash); 
    }

}
