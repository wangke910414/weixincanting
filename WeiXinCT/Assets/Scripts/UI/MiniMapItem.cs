using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniMapItem : MonoBehaviour
{
    [SerializeField]
    private Image selectImg;
    [SerializeField]
    private Image shadowImg;
    [SerializeField]
    private Image lockImg;
    [SerializeField]
    private Transform taskComtent;

    private bool isLock = false;

    public bool IsLock { get => isLock; set => isLock = value; }

   public void SelectState(bool value)
    {
        selectImg.gameObject.SetActive(value);
    }

    public void UpdataMiniMapShow()
    {
        if (isLock)
        {
            shadowImg.gameObject.SetActive(false);
            lockImg.gameObject.SetActive(false);
            taskComtent.gameObject.SetActive(true);
        }
        else
        {
            shadowImg.gameObject.SetActive(true);
            lockImg.gameObject.SetActive(true);
            taskComtent.gameObject.SetActive(false);
        }
    }

}
