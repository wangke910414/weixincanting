using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
//using WeChatWASM;
using WeChatWASM;

public class TextInput : MonoBehaviour, IPointerClickHandler, IPointerExitHandler
{
    public TMP_InputField input;

    public void OnPointerClick(PointerEventData eventData)
    {

#if UNITY_WEBGL && !UNITY_EDITOR
        // 监听点击事件唤起微信输入法ShowKeyboardOption
        WX.ShowKeyboard(new ShowKeyboardOption()
        {
            // 这里的参数根据需要自行设置
            // 参考https://developers.weixin.qq.com/minigame/dev/api/ui/keyboard/wx.showKeyboard.html
            defaultValue = input.text,
            maxLength = 20,
            confirmType = "done"
        });

        //绑定回调
        WX.OnKeyboardConfirm(OnConfirm);
        WX.OnKeyboardComplete(OnComplete);
#endif
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        // 隐藏输入法
#if UNITY_WEBGL && !UNITY_EDITOR
        if (!input.isFocused)
        {
            WX.HideKeyboard(new HideKeyboardOption());
            //删除掉相关事件监听
            WX.OffKeyboardInput(OnInput);
            WX.OffKeyboardConfirm(OnConfirm);
            WX.OffKeyboardComplete(OnComplete);
        }
#endif
    }

    public void OnInput(OnKeyboardInputListenerResult v)
    {
        if (input.isFocused)
        {
        }
    }

    public void OnConfirm(OnKeyboardInputListenerResult v)
    {
        // 输入法confirm回调
        if (input.isFocused)
        {
            input.text = v.value;
        }
    }

    public void OnComplete(OnKeyboardInputListenerResult v)
    {
        // 输入法complete回调
        if (input.isFocused)
        {
            input.text = v.value;
        }
    }
}
