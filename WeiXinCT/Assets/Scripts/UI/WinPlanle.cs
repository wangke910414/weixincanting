using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WinPlanle : MonoBehaviour
{
    [SerializeField]
    private  TextMeshProUGUI prizeKeyText;
    [SerializeField]
    private TextMeshProUGUI prizeCionText;

    private int showCionCount = 0;
    private int showKeyCount = 0;


    public int ShowCionCount { get => showCionCount; set => showCionCount = value; }
    public int ShowKeyCount { get => showKeyCount; set => showKeyCount = value; }


    public void UpdateShow()
   {
        prizeKeyText.text = showKeyCount.ToString();
        prizeCionText.text = showCionCount.ToString();
    }
}
