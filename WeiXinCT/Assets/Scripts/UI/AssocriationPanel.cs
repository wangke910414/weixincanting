using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

//美食协会
public class AssocriationPanel : UiWoPropAbility
{
    private HomeMapUiManager uiManager;
    [SerializeField]
    private List<AssocriationLevelBtn> levelBtnList = new List<AssocriationLevelBtn>();

    [Header("UI")]
    [SerializeField]
    private TextMeshProUGUI progessValueText;
    [SerializeField]
    private Image progessValueImg;
    [SerializeField]
    private Button[] displayBtn;
    [SerializeField]
    private Image sliderImg;
    [SerializeField]
    private Transform[] displayPanle;
    [SerializeField]
    private LevelInfoPlanleProp[] propItemEight;
    [SerializeField]
    private LevelInfoPlanleProp[] propItemFour;
    [SerializeField]
    private Transform scoillViewEight;
    [SerializeField]
    private Transform scoillViewFur;
    [SerializeField]
    private TextMeshProUGUI levelText;
    [SerializeField]
    private TextMeshProUGUI levelDiscirbleText;
    [SerializeField]
    private TextMeshProUGUI nullText;
    [SerializeField]
    private Transform maskingTip;
    [SerializeField]
    private AssociationObtainTip obtainDlg;
    [SerializeField]
    private Button receiveBtn;

    //private int totalDay = 0;
    private int maxGrade = 0;
    private AssociatonMemberConfigur memberConfigur;
    private ConsumePropConfigur propConfigur;
    private int dayCount;
    // private int remainDayCount;
    private TimeSpan spanTime;              //当前时间差
    private TimeSpan nextLookSpanTime;      //上次查看时间
    private int selectSttopLevel = 0;
    private int maxLevelRemainDay = 0;
    public void Init(HomeMapUiManager uiManager)
    {
        this.uiManager = uiManager;

    }


    private void OnEnable()
    {
        InitData();
    }


    public void OnClickLevelBtn(int level)
    {
        for (int i = 0; i < levelBtnList.Count; i++)
        {
            if  (i == level)
            {
                levelBtnList[i].SetSelect(true);

            }
            else
            {
                levelBtnList[i].SetSelect(false);
            }
        }

        SelectDisplay(level);
    }
    public void OnClickReNewBtn()
    {
        if (PlayerData.associationMenber.levelGrade == 4)
            return;

        Debug.Log("总天数 " + memberConfigur.GetTotallDay());
        if (PlayerData.associationMenber.levelGrade == 0)
        {
#if  UNITY_EDITOR
            PlayerData.associationMenber.fristBuyTime = DateTime.Now.ToString();
#elif UNITY_WEBGL && !UNITY_EDITOR
            PlayerData.associationMenber.fristBuyTime = PlayerData.instance.serverNowTime;
            Debug.Log("真假会员服务器当前时间   " + PlayerData.instance.serverNowTime);
#endif
        }


        PlayerData.associationMenber.buyCount++;
        PlayerData.associationMenber.upGradeDayCount = PlayerData.associationMenber.accDayCount - spanTime.Days + 30;
        PlayerData.associationMenber.levelGrade += 1;
        PlayerData.associationMenber.accDayCount += 30;
        var index = PlayerData.associationMenber.levelGrade;

        if ((index - 1) >= 0 && (index - 1) < PlayerData.associationMenber.receiveCountList.Count)
             PlayerData.associationMenber.receiveCountList[index - 1]++;

        if (PlayerData.associationMenber.levelGrade > 4)
            PlayerData.associationMenber.levelGrade = 4;

        Debug.Log("会员天数  " + PlayerData.associationMenber.accDayCount);
        Debug.Log("升级后总天数  " + PlayerData.associationMenber.upGradeDayCount);
        TimeCount();
        var data = JsonMapper.ToJson(PlayerData.associationMenber);
#if UNITY_EDITOR
        CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("AssociationMenberData", data));
#elif UNITY_WEBGL && !UNITY_EDITOR
        CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("UpdateAssoriationMember", data));
#endif
        Debug.Log("操作前等级  " + PlayerData.associationMenber.levelGrade);
        CurrMemberGrade();
        OnClickLevelBtn(PlayerData.associationMenber.levelGrade);
        Debug.Log("操作后等级  " + PlayerData.associationMenber.levelGrade);
    }

    private void InitData()
    {
        memberConfigur = SystemDataFactoy.instans.GetResourceConfigur("AssociatonMemberConfigur") as AssociatonMemberConfigur;
        propConfigur = SystemDataFactoy.instans.GetResourceConfigur("ConsumePropConfigur") as ConsumePropConfigur;
        for (int i = 0; i < levelBtnList.Count; i++)
        {
            levelBtnList[i].SetBtnState(BtnStateType.Normal);
        }
        levelBtnList[0].SetBtnState(BtnStateType.HighLight);

        if (PlayerData.associationMenber.levelGrade <= 0 || PlayerData.associationMenber.fristBuyTime == "")
            return;
        TimeCount();
        DayConsumeCount();


        if (dayCount <= 0)
            return;

        Debug.Log("总天数 " + dayCount);

        var currLevel = LevelGradeCount() + 1;

        if (currLevel > 0)
        {
            if (PlayerData.associationMenber.levelGrade == 0)
                    PlayerData.associationMenber.levelGrade = currLevel;

            Debug.Log("会员等级    " + PlayerData.associationMenber.levelGrade);
            var data = JsonMapper.ToJson(PlayerData.associationMenber);
#if UNITY_EDITOR
            CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("AssociationMenberData", data));
#elif UNITY_WEBGL && !UNITY_EDITOR
        CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("UpdateAssoriationMember", data));
#endif
        }
        CurrMemberGrade();
        OnClickLevelBtn(PlayerData.associationMenber.levelGrade);
    }

    private void CurrMemberGrade()
    {
        if (memberConfigur == null)
            return;

        if (PlayerData.associationMenber.levelGrade == 0)
        {
            //没有会员等级
            return;
        }

        var index = 0;
        maxGrade = index;
        for (int i = 1; i <= PlayerData.associationMenber.levelGrade; i++)
        {
            levelBtnList[i].SetBtnState(BtnStateType.HighLight);
        }
    }

    /// <summary>
    /// 进度条天数计算
    /// </summary>
    /// <param name="btnIndex"></param>
    private void SelectDisplay(int btnIndex)
    {
        selectSttopLevel = btnIndex;
        if (btnIndex == 0)
        {
            levelText.text = "Lv" + 0;
            progessValueText.text = "剩" + 0 + "天";
            progessValueImg.fillAmount = 0;
            nullText.gameObject.SetActive(true);
            displayPanle[0].gameObject.SetActive(false);
            displayPanle[1].gameObject.SetActive(false);
            return;
        }

        if (dayCount == 0)
        {
            for (int i = 0; i < levelBtnList.Count; i++)
            {
                if (i == 0)
                {
                    levelBtnList[i].SetBtnState(BtnStateType.HighLight);
                }
                else
                {
                    levelBtnList[i].SetBtnState(BtnStateType.Normal);
                }

            }
        }

        if (btnIndex < PlayerData.associationMenber.levelGrade)
        {
            progessValueText.text = "剩" + 30 + "天";
            progessValueImg.fillAmount = 1;
          
        }
        else if (btnIndex == PlayerData.associationMenber.levelGrade)
        {
            // Debug.Log("进入循环     " + btnIndex);
            var totalDay = PlayerData.associationMenber.accDayCount;
            var subDay = totalDay - (int)spanTime.Days;
            int nexttoTaoDat = subDay;
            var day = subDay;
            for (int i = 0; i < memberConfigur.memberGiftList.Count; i++)
            {
                day -= memberConfigur.memberGiftList[i].spanDay;
                if (day <= 0)
                {
                    maxLevelRemainDay = nexttoTaoDat;
                    progessValueText.text = "剩" + nexttoTaoDat.ToString() + "天";
                    progessValueImg.fillAmount = (float)nexttoTaoDat / memberConfigur.memberGiftList[i].spanDay;
                    break;
                }
                nexttoTaoDat -= memberConfigur.memberGiftList[i].spanDay;
            }

        }
        else if (btnIndex > PlayerData.associationMenber.levelGrade)
        {
            progessValueText.text = "剩" + 0 + "天";
            progessValueImg.fillAmount = 0;
        }
        levelText.text = "Lv" + selectSttopLevel.ToString();
        DisplayGiftIllstruate();
    }

    //设置礼物面板
    private void DisplayGiftIllstruate()
    {
        if (selectSttopLevel == 0)
            return;

        nullText.gameObject.SetActive(false);
        displayPanle[1].gameObject.SetActive(true);
        displayPanle[1].gameObject.SetActive(true);
        var lvelConfigur = memberConfigur.memberGiftList[selectSttopLevel-1];
            if (selectSttopLevel== 1)
            {
                scoillViewEight.gameObject.SetActive(false);
                scoillViewFur.gameObject.SetActive(true);
        
                for (int i=0; i< lvelConfigur.giftPack.propGiftPakc.Count; i++)
                {
                    var prop = propItemFour[i];
                     var pteype = lvelConfigur.giftPack.propGiftPakc[i].propType;
                    prop.IconImg.sprite = propConfigur.GetConsumeProp(pteype).Icon;
                    obtainDlg.SetPropCount(4);
                    obtainDlg.PropListFour[i].IconImg.sprite = propConfigur.GetConsumeProp(pteype).Icon;
                    obtainDlg.PropListFour[i].NumberText.text = lvelConfigur.giftPack.propGiftPakc[i].giveCount.ToString();
                    Debug.Log("道具个数   "+i +"   " + obtainDlg.PropListFour[i].NumberText.text);   
            }
            levelDiscirbleText.text = lvelConfigur.illustrateText;
        }
            else if (selectSttopLevel != 1)
            {
                scoillViewEight.gameObject.SetActive(true);
                scoillViewFur.gameObject.SetActive(false);

            for (int i = 0; i < lvelConfigur.giftPack.propGiftPakc.Count; i++)
            {
                var prop = propItemEight[i];
                var pteype = lvelConfigur.giftPack.propGiftPakc[i].propType;
                prop.IconImg.sprite = propConfigur.GetConsumeProp(pteype).Icon;
                prop.NumberText.text = lvelConfigur.giftPack.propGiftPakc[i].giveCount.ToString();
                //设置获得面板
                obtainDlg.SetPropCount(8);
                obtainDlg.PropListEight[i].IconImg.sprite = propConfigur.GetConsumeProp(pteype).Icon;
                obtainDlg.PropListEight[i].NumberText.text = lvelConfigur.giftPack.propGiftPakc[i].giveCount.ToString();
            }
            
        }
        obtainDlg.CionText.text = lvelConfigur.giftPack.cionNumber.ToString();
        obtainDlg.DiamendText.text = lvelConfigur.giftPack.diamondNumber.ToString();

        string str = System.Text.RegularExpressions.Regex.Unescape(lvelConfigur.illustrateText);
        levelDiscirbleText.text = str;
        OnClickGiftDiescirbleBtn(0);
    }

    //时间计算  AssociationObtainTip
    private void TimeCount()
    {
        if (PlayerData.associationMenber.levelGrade <= 0 || PlayerData.associationMenber.fristBuyTime == "")
            return;

        DateTime nowTiem;
#if UNITY_EDITOR
        nowTiem = DateTime.Now;
#elif UNITY_WEBGL && !UNITY_EDITOR
        nowTiem = DateTime.Parse( PlayerData.instance.serverNowTime);
#endif
        DateTime startIme = DateTime.Parse(PlayerData.associationMenber.fristBuyTime);

        Debug.Log("首签时间  " + startIme);
        Debug.Log("当前时间  " + nowTiem);

        spanTime = startIme.Subtract(nowTiem).Duration();
        Debug.Log("相差时间   " + spanTime.TotalDays + " 时间   " + spanTime.TotalHours);
        dayCount = PlayerData.associationMenber.accDayCount;
    }

    private void DayConsumeCount()
    {
        if (PlayerData.associationMenber.levelGrade <= 0)
            return;
        if (PlayerData.associationMenber.fristBuyTime == "")
            return;

        var idnex = PlayerData.associationMenber.levelGrade - 1;
        Debug.Log("消耗当前等级    " + PlayerData.associationMenber.levelGrade);

        if (idnex < 0 && idnex > PlayerData.associationMenber.consumeDayList.Count)
            return;
        Debug.Log("上次打开时间   " + PlayerData.associationMenber.nextLookTime);
        if (PlayerData.associationMenber.nextLookTime != "")
        {

            DateTime opent = DateTime.Parse(PlayerData.associationMenber.nextLookTime);
            DateTime curr = DateTime.Parse(PlayerData.associationMenber.fristBuyTime);

            nextLookSpanTime = curr.Subtract(opent).Duration();
        }
        if (nextLookSpanTime.Days != spanTime.Days)
            DiveidConsumeDayCount();

        if (spanTime.Days >= PlayerData.associationMenber.accDayCount)
        {

            PlayerData.associationMenber.ClearMemer();
        }

        Debug.Log("第一次调用更新   " + PlayerData.associationMenber.levelGrade);
        var data = JsonMapper.ToJson(PlayerData.associationMenber);
#if UNITY_EDITOR
        CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("AssociationMenberData", data));
#elif UNITY_WEBGL && !UNITY_EDITOR
        CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("UpdateAssoriationMember", data));
#endif
    }
    private int LevelGradeCount()
    {
        var levelGrade = 0;
        if (spanTime.TotalDays == 0)
            return 0;
        var accDay = 0;
        var nextDat = 0;
        Debug.Log("消耗天数   " + spanTime.TotalDays);
        var ramainDau = dayCount - spanTime.TotalDays;
        for (int i = 0; i < memberConfigur.memberGiftList.Count; i++)
        {
            accDay += memberConfigur.memberGiftList[i].spanDay;
            if (ramainDau > nextDat && ramainDau < accDay)
            {
                return i;
            }
            nextDat += memberConfigur.memberGiftList[i].spanDay;
        }

        return 0;

    }
    //查找等级
    private int SeekLevel(int day)
    {
        var coumsumeDay = PlayerData.associationMenber.accDayCount - day;
        var accDay = 0;
        var nextDat = 0;
        Debug.Log("消耗天数   " + spanTime.TotalDays);
        var ramainDau = dayCount - spanTime.TotalDays;
        for (int i = 0; i < memberConfigur.memberGiftList.Count; i++)
        {
            accDay += memberConfigur.memberGiftList[i].spanDay;
            if (ramainDau > nextDat && ramainDau < accDay)
            {
                return i;
            }
            nextDat += memberConfigur.memberGiftList[i].spanDay;
        }
        return 0;
    }

    //划分消耗天数
    private void DiveidConsumeDayCount()
    {
        //总天数  消耗天数  剩下天数

        var totalDay = PlayerData.associationMenber.accDayCount;
        List<int> dayList = new List<int>();
        List<int> totalList = new List<int>();
        List<int> upGradeDayList = new List<int>();
        List<int> nextDayList = new List<int>();

        var crrDay = totalDay - spanTime.Days;
        int tempDay = 0;
        int nextDay = 0;
        //剩下等级天数
        Debug.Log("剩余天数等级计算！  " + crrDay);
        for (int i = 0; i < memberConfigur.memberGiftList.Count; i++)
        {
            tempDay += memberConfigur.memberGiftList[i].spanDay;

            if ((crrDay - nextDay) > 0)
            {
                if ((crrDay - tempDay) > 0)
                {
                    dayList.Add(memberConfigur.memberGiftList[i].spanDay);
                }
                else
                {
                    dayList.Add(crrDay - nextDay);
                }
                Debug.Log("总长度   " + dayList.Count + " 等级天数    " + dayList[dayList.Count - 1]);
            }
            else
            {
                break;
            }
            nextDay += memberConfigur.memberGiftList[i].spanDay;

        }

        //总等级天数
        tempDay = 0;
        nextDay = 0;
        for (int i = 0; i < memberConfigur.memberGiftList.Count; i++)
        {
            Debug.Log("总天数等级计算！" + totalDay);
            tempDay += memberConfigur.memberGiftList[i].spanDay;

            if ((totalDay - nextDay) > 0)
            {
                if ((totalDay - tempDay) > 0)
                    totalList.Add(memberConfigur.memberGiftList[i].spanDay);
                else
                {
                    totalList.Add(totalDay - nextDay);
                }
                Debug.Log("总长度   " + totalList.Count + "等级天数     " + totalList[totalList.Count - 1]);
            }
            else
            {
                break;
            }
            nextDay += memberConfigur.memberGiftList[i].spanDay;
        }

        //升级后可用总天数
        tempDay = 0;
        nextDay = 0;
        var upGradeDay = PlayerData.associationMenber.upGradeDayCount;

        for (int i = 0; i < memberConfigur.memberGiftList.Count; i++)
        {
            Debug.Log("升级后总天数等级计算！");
            tempDay += memberConfigur.memberGiftList[i].spanDay;

            if ((upGradeDay - nextDay) > 0)
            {
                if ((upGradeDay - tempDay) > 0)
                    upGradeDayList.Add(memberConfigur.memberGiftList[i].spanDay);
                else
                {
                    upGradeDayList.Add(upGradeDay - nextDay);
                }
                Debug.Log("总长度   " + upGradeDayList.Count + "等级天数     " + upGradeDayList[upGradeDayList.Count - 1]);
            }
            else
            {
                break;
            }
            nextDay += memberConfigur.memberGiftList[i].spanDay;
        }


        //上一次打开
        tempDay = 0;
        nextDay = 0;
        var nextOpenDay = PlayerData.associationMenber.upGradeDayCount - nextLookSpanTime.Days;

        Debug.Log("跨度时间  " + nextLookSpanTime.Days);
        for (int i = 0; i < memberConfigur.memberGiftList.Count; i++)
        {
            Debug.Log("上一次打开数据 ！  " + nextOpenDay);
            tempDay += memberConfigur.memberGiftList[i].spanDay;

            if ((nextOpenDay - nextDay) > 0)
            {
                if ((nextOpenDay - tempDay) > 0)
                    nextDayList.Add(memberConfigur.memberGiftList[i].spanDay);
                else
                {
                    nextDayList.Add(nextOpenDay - nextDay);
                }
                Debug.Log("总长度   " + nextDayList.Count + "等级天数     " + nextDayList[nextDayList.Count - 1]);
            }
            else
            {
                break;
            }
            nextDay += memberConfigur.memberGiftList[i].spanDay;
        }

        //计算

        for (int i = 0; i < upGradeDayList.Count; i++)
        {
            if (i < dayList.Count)
            {
                if ((upGradeDayList[i] - dayList[i]) > 0)
                {
                    Debug.Log("有时间差值！");
                    //如果时中途升级
                    if (PlayerData.associationMenber.accDayCount !=
                        PlayerData.associationMenber.upGradeDayCount)
                    {
                        if (PlayerData.associationMenber.consumeDayList[i] != 0)
                        {
                            PlayerData.associationMenber.consumeDayList[i] += upGradeDayList[i] - dayList[i];

                        }
                        else
                        {
                            PlayerData.associationMenber.consumeDayList[i] = upGradeDayList[i] - dayList[i];

                        }
                    }
                    else//中途没有升级
                    {
                        PlayerData.associationMenber.consumeDayList[i] = upGradeDayList[i] - dayList[i];

                    }
                }
            }
            else
            {
                if (PlayerData.associationMenber.accDayCount !=
                        PlayerData.associationMenber.upGradeDayCount)
                {
                    if (PlayerData.associationMenber.consumeDayList[i] > 30)
                    {
                        PlayerData.associationMenber.consumeDayList[i] = upGradeDayList[i];
                    }
                    else
                    {
                        PlayerData.associationMenber.consumeDayList[i] = upGradeDayList[i];
                    }
                }
                else
                {
                    PlayerData.associationMenber.consumeDayList[i] = upGradeDayList[i];
                }
            }
        }


        for (int i = 0; i < PlayerData.associationMenber.consumeDayList.Count; i++)
        {
            Debug.Log("等级  " + i + "    " + "天数   " + PlayerData.associationMenber.consumeDayList[i]);
        }
#if UNITY_WEBGL && !UNITY_EDITOR
        PlayerData.associationMenber.nextLookTime = PlayerData.instance.serverNowTime;
#elif UNITY_EDITOR
        PlayerData.associationMenber.nextLookTime = DateTime.Now.ToString();
#endif
        Debug.Log("保存上次时间    " + PlayerData.associationMenber.nextLookTime);
        CheckReceiveGiftList();
    }

    /// <summary>
    /// 到达30天增加一次领取
    /// </summary>
    private void CheckReceiveGiftList()
    {
        var consumeList = PlayerData.associationMenber.consumeDayList;
        for (int i=0; i< consumeList.Count; i++)
        {
            if (consumeList[i] >= 30)
            {
                PlayerData.associationMenber.receiveCountList[i]++;
                consumeList[i] = consumeList[i] % 30;
            }
        }
    }
    public void OnClickGiftDiescirbleBtn(int btnIndex)
    {
      
        var bgImg = sliderImg.GetComponent<FlightAnimation>(); 
        if (bgImg != null)
        {
            Debug.Log("执行滑动  ！");
            var tr = displayBtn[btnIndex].GetComponent<Transform>();
            bgImg.BgImgSlider(tr, 0.2f);
            Debug.Log("执行滑动结束  ！");

        }
        if (selectSttopLevel == 0)
            return;
        if (btnIndex < 0 || btnIndex > displayBtn.Length)
        {
            return;
        }

        if (btnIndex == 0)
        {
            displayPanle[0].gameObject.SetActive(true);
            displayPanle[1].gameObject.SetActive(false);
        }
       else if (btnIndex == 1)
        {
            displayPanle[0].gameObject.SetActive(false);
            displayPanle[1].gameObject.SetActive(true);
             CheckGiftCanReceive();
        }
    }
    public void OnClickDisappearTip()
    {
        if(selectSttopLevel != PlayerData.associationMenber.levelGrade)
        {
            return;
        }
        if (maxLevelRemainDay >= 10)
        {
            return;
        }

        if(maskingTip.gameObject.activeSelf)
        {
            maskingTip.transform.GetComponent<CanvasGroup>().DOFade(0, 0.3f).onComplete = () =>
            {
                maskingTip.gameObject.SetActive(false);
            };
        }
        else if(!maskingTip.gameObject.activeSelf)
        {
            maskingTip.gameObject.SetActive(true);
            maskingTip.transform.GetComponent<CanvasGroup>().DOFade(1f, 0.3f);
        }
    }

    //检查是狗可以领取奖励
    private void CheckGiftCanReceive()
    {
        Debug.Log("金进入领取页面@");

      Debug.Log("获取领取长度！" + JsonMapper.ToJson( PlayerData.associationMenber.receiveCountList));

        var receiveList = PlayerData.associationMenber.receiveCountList;
        for (int i=0; i<receiveList.Count; i++)
        {
            receiveBtn.interactable = false;

            if (receiveList[i]>0)
            {
                if ((selectSttopLevel-1) == i)
                {
                    receiveBtn.interactable = true;
                    break;
                }
            }
        }
    }
    public void OnClickRecerveBtn()
    {
        if ((selectSttopLevel - 1) < 0 && (selectSttopLevel - 1) >= PlayerData.associationMenber.receiveCountList.Count)
            return;

        if (PlayerData.associationMenber.receiveCountList[selectSttopLevel - 1] > 0)
        {
            PlayerData.associationMenber.receiveCountList[selectSttopLevel - 1]--;

            var data = JsonMapper.ToJson(PlayerData.associationMenber);
            receiveBtn.interactable = false;
#if UNITY_EDITOR
            CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("AssociationMenberData", data));
#elif UNITY_WEBGL && !UNITY_EDITOR
        CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("UpdateAssoriationMember", data));
#endif
        }
       // CheckGiftCanReceive();

        //保存奖品数局
        var MemberPower = memberConfigur.GetAssociationPower(selectSttopLevel - 1);

        PlayerData.playerDataParame.cions += MemberPower.giftPack.cionNumber;
        PlayerData.playerDataParame.diamond += MemberPower.giftPack.diamondNumber;

        var propData = MemberPower.giftPack.propGiftPakc;
        for (int i=0; i< propData.Count; i++)
        {
            var type = propData[i].propType.ToString();
            if (PlayerData.consumePropDataParameter.consumePropDataDict.ContainsKey(type))
            {
                var number = int.Parse(PlayerData.consumePropDataParameter.consumePropDataDict[type]) + propData[i].giveCount;
                PlayerData.consumePropDataParameter.consumePropDataDict[type] = number.ToString();
            }
            else
            {
                PlayerData.consumePropDataParameter.consumePropDataDict.Add(type, propData[i].giveCount.ToString());
            }
        }

        var consumeProp = JsonMapper.ToJson(PlayerData.consumePropDataParameter.consumePropDataDict);

#if UNITY_EDITOR
        CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("consumeProp  ", consumeProp));
#elif UNITY_WEBGL && !UNITY_EDITOR
       CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("UpdateConsumeProp", consumeProp));
#endif
        PlayerData.instance.PlayerDataParameSave();
        obtainDlg.OpenDlg();
    }
}
