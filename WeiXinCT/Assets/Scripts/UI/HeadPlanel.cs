using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeadPlanel : UiWoPropAbility
{
    private HomeMapUiManager uiManager;

    [SerializeField]
    private Transform headParent;

    [SerializeField]
    private GameObject headPrefab;

    [SerializeField]
    private GameObject saveBtn;

    private HeadAssetData currHeadData;

    public void OnClickClose()
    {
        // this.gameObject.SetActive(false);
        CloseUiWo();
    }
    public HeadAssetData CurrHeadData { get => currHeadData; set => currHeadData = value; }

    public void Confgur(HomeMapUiManager uiManager)
    {
        this.uiManager = uiManager;

        // StartCoroutine( SpeapeHead() );
        SpeapeHead();
    }

    private void Start()
    {
     
    }
       
    private void SpeapeHead()
    {
        //yield return SystemDataFactoy.instans;

        HeadAssetConfigur head = SystemDataFactoy.instans.GetResourceConfigur("HeadAssetConfigur") as HeadAssetConfigur;
        for (int i=0; i< head.HeadAssetDataList.Count; i++ )
        {
            var item = Instantiate(headPrefab);
            item.transform.SetParent(headParent);
            item.GetComponent<Image>().sprite = head.HeadAssetDataList[i].headImage;
            item.GetComponent<HeadItem>().InteHeadItem(this, head.HeadAssetDataList[i]);
            item.transform.localScale = Vector3.one;
        }

    }

    public void CallSelectHead(HeadAssetData headData)
    {
        currHeadData = headData;
    }

    public void OnClickSave()
    {
        if (currHeadData != null)
        {
            uiManager.HomePlanel.SetHeadImage(currHeadData);
        }

        CloseUiWo();
    }
}

