using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

//
public class RestaurentAudio : MonoBehaviour
{
    [SerializeField]
    private AudioSource bgMusic;

    [SerializeField]
    private AudioSource guestComeSound;

    [SerializeField]
    private AudioSource foodCompeletSound;

    [SerializeField]
    private AudioSource foodSumbmitSound;

    [SerializeField]
    private AudioSource goodCommendSound;

    [SerializeField]
    private AudioSource poorCommendSound;


    public AudioSource BgMusic { get => bgMusic;  }
    public AudioSource GuestComeSound { get => guestComeSound; }

    public AudioSource FoodCompeletSound { get => foodSumbmitSound; }
    public AudioSource FoodSumbmitSound { get => foodSumbmitSound; }
    public AudioSource GoodCommend { get => goodCommendSound;  }
    AudioSource PoorCommendSound { get => poorCommendSound; }

}
 