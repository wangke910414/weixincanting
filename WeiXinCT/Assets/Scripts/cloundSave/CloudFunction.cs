using LitJson;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WeChatWASM;
public class CloudFunction
{
    static public CloudFunction instance;
    private int functionCount = 0;
    public int FunctionCount { get => functionCount; }
    static public  void Init()
    {
        if (instance == null)
            instance = new CloudFunction();
        // instance.CloundSave();
        instance.CloundSave();
     ///nstance.CallCloundFunction("sfasfsdafsd");
    }

    private void  CloundSave()
    {
        WXBase.cloud.Init(new CallFunctionInitParam()
        {
            env = "cloud1-4gbcohble01bbed3",
            traceUser = false
        });
    }
    /// <summary>
    /// 倮没有创建一个玩家数据
    /// </summary>
    /// <param name="fName"></param>
    /// <returns></returns>
    public string CallCloundCreate(string fName)
    {
        var resuleData = string.Empty;
        WXBase.cloud.CallFunction(new CallFunctionParam()
        {
            name = "PlayerData",
            data = fName,
            success = (res) => {
         
                resuleData = res.result;
            },
           fail = (res) => {
               Debug.Log("加载失败  " + res.errMsg);
           },
           complete =(res) => {
               Debug.Log("加载玩家数据完成！");
           }
        });
        return resuleData;
    }

    /// <summary>
    /// 更新玩家数据
    /// </summary>
    /// <param name="fName"></param>
    /// <returns></returns>
    public string CallCloundUpdate(string fName)
    {
        var resuleData = string.Empty;
        WXBase.cloud.CallFunction(new CallFunctionParam()
        {
            name = "UpdataData",
            // data = JsonUtility.ToJson(fName),
            data = fName,
            success = (res) => {
                resuleData = res.result;
            },
            fail = (res) => {
            },
            complete = (res) => {
            
            }
        });
        Debug.Log("结束 " + JsonUtility.FromJson<PlayerDataParameter>(resuleData));
        return resuleData;
    }


    /// <summary>
    /// 查询地图
    /// </summary>
    public void OnLoadMapAndLevel()
    {
        Debug.Log("加载地图数据！");
        WX.cloud.CallFunction(
            new CallFunctionParam()
            {
                name = "OnLoadMapAndLevel",
                data = JsonUtility.ToJson(PlayerData.playerMapAndLevelParme),
        
                success = (res) =>
                {
                    //云数据保存到本地
                    Debug.Log("地图数据  ：" + res.result);
                    var data = JsonMapper.ToObject(res.result);
                   if (data.ContainsKey("data"))
                    {
                        var gamedata = data["data"];

                        var unLockMapNameList = gamedata["unLockMapNameList"];

                        Debug.Log("unLockMapNameList   " + unLockMapNameList);
                   
                        for (int i=0; i< unLockMapNameList.Count; i++)
                        {
                            PlayerData.playerMapAndLevelParme.unLockMapNameList.Add(unLockMapNameList[i].ToString());
                        }
                    }
                },
                fail = (res) =>
                {
                    Debug.Log("地图数据加载失败！");
                },
                complete = (res) =>
                {
                    Debug.Log("地图数据加载完成！");
                }
            });
    }
    /// <summary>
    /// 更新地图
    /// </summary>
    public void OnUpdateMapAndLevel()
    {
        var p = new PlayerMapAndLevelData();

        WX.cloud.CallFunction(
            new CallFunctionParam()
            {
                name = "UpdataMapAndLevel",
                data = JsonUtility.ToJson(PlayerData.playerMapAndLevelParme),
                success = (res) =>
                {
                    Debug.Log("地图更新成功!");
                },
                fail = (res) =>
                {

                },
                complete = (res) =>
                {
                    Debug.Log("解锁地图完成!");
                }
            });
    }

    /// <summary>
    /// 读取数据
    /// </summary>
    public void OnLoadData()
    {
            var p = new PlayerDataParameter();
       
            WX.cloud.CallFunction(new CallFunctionParam()
            {
                name = "OnLoadData",
                data = JsonMapper.ToJson(PlayerData.playerDataParame),
                success = (res) =>
                {
                    //云数据保存到本地
                    var data = JsonMapper.ToObject(res.result);
  
                    if (data.ContainsKey("data"))
                    {
                        var gamedata = data["data"];
                        Debug.Log("收到玩家数据" + res.result);

                        PlayerData.playerDataParame = JsonMapper.ToObject<PlayerDataParameter>(gamedata.ToJson());
                        // var gamedata = resData["gamedata"];
                        Debug.Log("收到玩家数据 获取后   " + JsonMapper.ToJson(PlayerData.playerDataParame));
                        // Debug.Log("玩家数据" + gamedata.ToJson());
                        //
                        // PlayerData.playerDataParame.cions = int.Parse (gamedata["cions"].ToString());
                        // PlayerData.playerDataParame.diamond = int.Parse( gamedata["diamond"].ToString());
                        // PlayerData.playerDataParame.keyNumber = int.Parse(gamedata["keyNumber"].ToString());
                        // PlayerData.playerDataParame.playerName = gamedata["playerName"].ToString();
                        // PlayerData.playerDataParame.energy = int.Parse( gamedata["energy"].ToString());
                        // PlayerData.playerDataParame.siginDayNumber = int.Parse(gamedata["siginDayNumber"].ToString());
                        // PlayerData.playerDataParame.singinDayTime = gamedata["singinDayTime"].ToString();
                        // PlayerData.playerDataParame.unLockCount = int.Parse( gamedata["unLockCount"].ToString());
                        // PlayerData.playerDataParame.assocriationDayNumber = int.Parse(gamedata["assocriationDayNumber"].ToString());
                        // PlayerData.playerDataParame.openAppCount = int.Parse(gamedata["openAppCount"].ToString());

                        // var siginJson = gamedata["sgindayList"];
                        //PlayerData.playerDataParame.sgindayList = JsonMapper.ToObject<List<int>>(siginJson.ToJson());
                    }
                    else
                    {
                        //数据库无数据
                        Debug.Log("读取无数据！：");
                    }
                },
                fail = (res) =>
                {
                    Debug.Log("获取  fail");
                },
                complete = (res) =>
                {

                }
            });
    }
    
    public void UpdataData(string funNmae, string value)
    {
        Debug.Log("调用更新云函数   " + funNmae + "    " + value);
        WX.cloud.CallFunction(
           new CallFunctionParam()
           {
               name = funNmae,
               data = value,
               success = (res) =>
               {
                   Debug.Log("更新成功!" + value);
               },
               fail = (res) =>
               {
                   Debug.Log("更新失败!" + res);
               },
               complete = (res) =>
               {
                  
               }
           });
    }


    public void OnLoadPlayOnce()
    {
        JsonData jsonData = new JsonData();
        string returnStr = string.Empty;
        WX.cloud.CallFunction(new CallFunctionParam()
        {
            name = "OnLoadLevelPlayOnce",
            data = JsonUtility.ToJson(PlayerData.playOnceParme),
            success = (res) =>
            {
                var data = JsonMapper.ToObject(res.result);
               

                if (data.ContainsKey("data"))
                {
                    Debug.Log("收到游玩次数    " + res.result);
                    var gamedata = data["data"];
                   PlayerData.playOnceParme = JsonMapper.ToObject<LevelPlayOnce>(gamedata.ToJson());
                  
                    Debug.Log("收到游玩次数或缺后    " + JsonUtility.ToJson(PlayerData.playOnceParme.levelPlayOnceDataDict));
             
                }
                else
                {
                    //数据库无数据

                }
            },
            fail = (res) =>
            {
                Debug.Log("获取  失败 fail");
            },
            complete = (res) =>
            {

            }
        });
    }
    public void OnLoadConsumer()
    {
        Debug.Log("客户端调用道具云函数 ！");
        JsonData jsonData = new JsonData();
        string returnStr = string.Empty;
        WX.cloud.CallFunction(new CallFunctionParam()
        {
            name = "onLoadConsumeProp",
            data = JsonMapper.ToJson(PlayerData.consumePropDataParameter),
            success = (res) =>
            {
                var data = JsonMapper.ToObject(res.result);


                if (data.ContainsKey("data"))
                {
                    var gamedata = data["data"];
                    var dict = gamedata["consumePropDataDict"];
                    //Debug.Log("------获取道具数据成功！------" + dict.ToJson());
                    PlayerData.consumePropDataParameter.consumePropDataDict = JsonMapper.ToObject<Dictionary<string, string>>(dict.ToJson());

                    foreach (var item in Enum.GetValues(typeof(ConsumePropType)))
                    {
                        if (!PlayerData.consumePropDataParameter.consumePropDataDict.ContainsKey(item.ToString()))
                        {
                            PlayerData.consumePropDataParameter.consumePropDataDict.Add(item.ToString(), 0.ToString());
                        }
                    }
                }
                else
                {
                    //数据库无数据
                    Debug.Log("客户端调用道具云函数 ！没有值");

                }
            },
            fail = (res) =>
            {
                Debug.Log("获取  失败 fail");
            },
            complete = (res) =>
            {

            }
        });
    }

    public void OnLoadDeviveData()
    {
        Debug.Log("调用设备数据成功 ！");
        JsonData jsonData = new JsonData();
        string returnStr = string.Empty;
        WX.cloud.CallFunction(new CallFunctionParam()
        {
            name = "OnLoadDevice",
            data = JsonMapper.ToJson(PlayerData.deviceDataParameter),
            success = (res) =>
            {
                var data = JsonMapper.ToObject(res.result);


                if (data.ContainsKey("data"))
                {
                    var gamedata = data["data"];
                    var dict = gamedata["deviceDict"];

                    Debug.Log("------获取设备数据成功！------" + dict.ToJson());
                    gamedata.ToJson();
                    PlayerData.deviceDataParameter.deviceDict = JsonMapper.ToObject<Dictionary<string, string>>(dict.ToJson());
                }
                else
                {
                    //数据库无数据
                    Debug.Log("客户端调设备云函数 ！没有值");
                }
            },
            fail = (res) =>
            {
                Debug.Log("获取  设备 失败 fail");
            },
            complete = (res) =>
            {

            }
        });
    }

    public void OnLoadFood()
    {
        Debug.Log("客户端调用食物云函数 ！");
        JsonData jsonData = new JsonData();
        string returnStr = string.Empty;
        WX.cloud.CallFunction(new CallFunctionParam()
        {
            name = "OnLoadFood",
            data = JsonMapper.ToJson(PlayerData.foodDataPramerer),
            success = (res) =>
            {
                var data = JsonMapper.ToObject(res.result);


                if (data.ContainsKey("data"))
                {
                    var gamedata = data["data"];
                    var foodData = gamedata["foodDataDict"];

                    Debug.Log("------获取食物数据成功！------" + foodData.ToJson());
                    gamedata.ToJson();
                    PlayerData.foodDataPramerer.foodDataDict = JsonMapper.ToObject<Dictionary<string, string>>(foodData.ToJson());
                }
                else
                {
                    //数据库无数据
                    Debug.Log("客户端调shi'w食物云函数 ！没有值");
                }
            },
            fail = (res) =>
            {
                Debug.Log("获取  设备 失败 fail");
            },
            complete = (res) =>
            {

            }
        });
    }

    public void SendEmail()
    {
        Debug.Log("客户端调用发送邮件 ！");
        WX.cloud.CallFunction(new CallFunctionParam()
        {
            name = "Email",
            data = JsonMapper.ToJson(PlayerData.playerDataParame),
            success = (res) =>
            {

                Debug.Log("客户端调用发送邮件成功 ！");

            },
            fail = (res) =>
            {
                Debug.Log("客户端调用发送邮件失败 ！");
            },
            complete = (res) =>
            {
                Debug.Log("客户端调用发送邮件完成 ！");
            }
        });
    }


    public void OnLoadAssociationMember()
    {
        Debug.Log("调用会员值 ！");
        JsonData jsonData = new JsonData();
        string returnStr = string.Empty;
        WX.cloud.CallFunction(new CallFunctionParam()
        {
            name = "AssoriationMember",
            data = JsonMapper.ToJson(PlayerData.associationMenber),
            success = (res) =>
            {
             var data = JsonMapper.ToObject(res.result);
              
                Debug.Log("会员数据  " + data.ToJson());

           if (data.ContainsKey("data"))
            {
                    var gamedata = data["data"];

        
                    Debug.Log("读取会员值  " + gamedata.ToJson());
                   
                    PlayerData.associationMenber = JsonMapper.ToObject<AssociationMenber>(gamedata.ToJson());
                for (int i = 0; i < PlayerData.associationMenber.receiveCountList.Count; i++)
                {
                        Debug.Log(" 领取指  "+ i + PlayerData.associationMenber.receiveCountList[i]);
                    }
                }
                else
                {
                    //数据库无数据
                    Debug.Log("读取会员值 ！没有值");
                }
            },
            fail = (res) =>
            {
                Debug.Log("获取  读取会员值 失败 fail");
            },
            complete = (res) =>
            {

            }
        });
    }

    
    /// <summary>
    /// 获取微信服务器时间
    /// </summary>

    public void GetNowDate()
    {
        WX.cloud.CallFunction(new CallFunctionParam()
        {
            name = "NoeTime",
            data = JsonMapper.ToJson(PlayerData.associationMenber),
            success = (res) =>
            {
                Debug.Log("获取时间   " + res.result);
                if (res.result != null)
                {
                    PlayerData.instance.serverNowTime = res.result;
                }
            },
            fail = (res) =>
            {
               // Debug.Log("获取获取时间 fail");
            },
            complete = (res) =>
            {

            }
        }); 
    }

    /// <summary>
    /// 获取餐厅数据 UpdataRestaurentLockData
    /// </summary>
    public void OnLoadRestaurentData()
    {
        Debug.Log("获取餐厅数据");
        WX.cloud.CallFunction(new CallFunctionParam()
        {
            name = "OnLoadRestaurentData",
            data = JsonMapper.ToJson(PlayerData.restaurentUnLock),
            success = (res) =>
            {
                var data = JsonMapper.ToObject(res.result);
                Debug.Log("获取餐厅数据返回    " + res.result);
                if (data.ContainsKey("data"))
                {
                    var gameData = data["data"];
                    Debug.Log("获取餐厅数据返回gameData    " + gameData.ToJson());
                    PlayerData.restaurentUnLock = JsonMapper.ToObject<UnLockRestaurent>(gameData.ToJson());
                   
                    foreach (var item in PlayerData.restaurentUnLock.restaurentUnlockDit)
                    {
                        Debug.Log("加载商店名称  " + item.Key + "   " + item.Value);
                    }
                }
                else
                {
                    Debug.Log("加载餐厅数据失败！");
                }
            },
            fail = (res) =>
            {
                Debug.Log("获取获取餐厅数据 fail");
            },
            complete = (res) =>
            {
                
            }
        });
    }


}
