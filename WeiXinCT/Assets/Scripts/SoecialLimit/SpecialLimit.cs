using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialLimit : MonoBehaviour
{
    [SerializeField]
    private SpecialLimitType specialLimitType;

    public SpecialLimitType specialType { get => specialLimitType; }
}
