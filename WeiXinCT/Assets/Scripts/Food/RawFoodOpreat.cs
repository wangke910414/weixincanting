using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

/// <summary>
/// 生的食物
/// </summary>
///

//是否制作
public enum MakeType : int
{
    Null,
    NeedMak,
    NoMake,
}
public enum FoodSandwichTyoe : int
{
    Null,
    NoSandwich,     //不夹心
    OneSandwich,    //一个夹心   例如 汉堡
    
}
public class RawFoodOpreat : MonoBehaviour
{
    // Start is called before the first frame update 

    public string foodName;
    private Tween steakDTween;
    public Sprite[] foodGradeImg;
    [SerializeField]
    private MakeType makeType;

    [SerializeField]
    private FoodSandwichTyoe foodSandwichTyoe;
    private int rawFoddGrade;
    public string FoodName { get => foodName; set => foodName = value; }
    public int RawFoddGrade { set => rawFoddGrade = value; }
        void Start()
    {
        
    }
    public void EvetClick()
    {
    //    if (PlayerData.instance.teachingValue.MakeFoodClick == 0)
    //        return;

        if (steakDTween == null)
        {
            steakDTween = this.transform.DOScale(1.1f, 0.1f).SetLoops(2, LoopType.Yoyo);
        }
        if (steakDTween != null && !steakDTween.active)
        {

            steakDTween = this.transform.DOScale(1.1f, 0.1f).SetLoops(2, LoopType.Yoyo);

        }
        if (makeType == MakeType.NeedMak)
        {
            EventAddFoodMakeing eventData = new EventAddFoodMakeing();
            eventData.DataObject = this.gameObject;
            eventData.foodNmae = foodName;
            eventData.foodSandwichType = foodSandwichTyoe;
            eventData.foodType = FoodType.MainFood;
            SystemEventManager.TriggerEvent("OperationMakingFood", eventData);

        }
        else if (makeType == MakeType.NoMake)
        {
            EventRawFoodAnd eventParam = new EventRawFoodAnd();
            eventParam.foodNmae = foodName;
            eventParam.makeType = MakeType.NoMake;
            eventParam.foodSandwichTyoe = foodSandwichTyoe;
            eventParam.DataObject = this.gameObject;
            SystemEventManager.TriggerEvent("OperationCookeFood", eventParam);
        }
    
    }
    public void SetFoodSkin()
    {
        this.GetComponent<Image>().sprite = foodGradeImg[rawFoddGrade - 1];
    }
}
