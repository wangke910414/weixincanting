using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GreensFood : OtherFood
{//HamburgBeef
    private Tween tween;
    public void ClickFood()
    {
        if (tween == null)
        {
            tween = this.transform.DOScale(1.1f, 0.1f).SetLoops(2, LoopType.Yoyo);
        }
        else if (!tween.active)
        {
            tween = this.transform.DOScale(1.1f, 0.1f).SetLoops(2, LoopType.Yoyo);
        }
        //OperationAddOtherFiid
        EventAddOtherFood eventMsg = new EventAddOtherFood();
        eventMsg.foodNmae = foodNmae;
        eventMsg.mainFoodName = mainFoodName;
        eventMsg.foodPrice = foodPrice;

        eventMsg.DataObject = this.gameObject;
        SystemEventManager.TriggerEvent("OperationAddOtherFiid", eventMsg);
    }
}
