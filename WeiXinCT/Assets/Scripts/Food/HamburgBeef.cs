using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HamburgBeef : OtherFood
{
    public Tween tween;
    public override void ClickFood()
    {
        //if (PlayerData.instance.teachingValue.addOtherFoodClick == 0) HamburgBeefCompnent
        //return;

        if (tween == null)
        {
            tween = this.transform.DOScale(1.1f, 0.1f).SetLoops(2, LoopType.Yoyo);
        }
        else if (!tween.active)
        {
            tween = this.transform.DOScale(1.1f, 0.1f).SetLoops(2, LoopType.Yoyo);
        }
        //OperationAddOtherFiid

        EventAddFoodMakeing eventData = new EventAddFoodMakeing();
        eventData.DataObject = this.gameObject;
        eventData.foodNmae = foodNmae;
        eventData.foodType = FoodType.OtherFood;
        SystemEventManager.TriggerEvent("OperationMakingFood", eventData);
    }
}
