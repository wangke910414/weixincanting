using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public enum FoodState : int
{
    NullState,
    Raw,
    Making,
    Cooke,          //煮熟
    OnPlate,        //在盘子里
    Burning,        //燃烧
    
}

public enum FooodType : int
{
    MainFood,
    OtherFood,
    Drink
}

/// <summary>
/// 主食类
/// </summary>
public class FoodProperty : MonoBehaviour
{

    [HideInInspector]
    public FoodState foodState = FoodState.NullState;
    [SerializeField]
    protected FoodSandwichTyoe foodSandwichType;       //食物是否夹心
    public Sprite[] foodGradeImg;
    public string foodName;
    [HideInInspector]
    public int mainFoodIndex = 0;
    [SerializeField]
    protected GameObject[] otherFood;
    [SerializeField]
    protected AudioSource makeingSound;

    protected float CookedTime;
   // protected float maxCookedTime;
    
    protected int plateIndex = 0;   //食物在盘子坐标

    [HideInInspector]
    public float conbustionTime;        //烧糊计时

    [HideInInspector]
    public int foodGrade;

    public float foodPrice;

    public Transform timeParent;
    public bool isFireFood = true;         //是否食物会烧糊
    private OperationManage operationManage;
    public string FoodNmae { get => foodName; set => foodName = value; }
    public OperationManage OperationManage { set => operationManage = value; }
    public GameObject[] OtherFood { get => otherFood; }
    public int PlateIndex { get => plateIndex; set => plateIndex = value; }

    public FoodSandwichTyoe FoodSandwichType { get => foodSandwichType; }
    public virtual void RestFood()
    {

    }

    public virtual void ClearFood()
    {
        // DisCardFood();
        operationManage.FoodOnFireClear(mainFoodIndex);
    }

    public virtual bool AddOtherFood(string food)
    {
        return false;
    }

    //点击食物
    public virtual void  ClilkFood()
    {

    }
    //食物瞬间完成
    public virtual void SetFoodMakeTiem(float time)
    {

    }
    //食物不糊道具
    public virtual void SetNoFireFood()
    {

    }

    public virtual void InitFood()
    {

    }

    //主食添加成功
    public virtual bool MainFoodAddSuccess()
    {
        return false;
    }

    //食物制作完成
    public virtual void FoodMakeCompelet()
    {
        if (PlayerData.instance.teachingValue.FoodMakeCompeltClick == 0)
            operationManage.transform.GetComponent<UiManager>().TeachingOperation(TeachingType.FoodMakeCompelt);

    }
    public virtual bool CallFoodAddCompelet()
    {
        return false;
    }

    //食物烧糊操作
    public virtual void FoodFire()
    {
        PlayerData.instance.currSingleLevelData.FoodFireCount++;
        SpecialLimitOpertion(SpecialLimitType.NoFire);
    }
    //食物丢弃操作
   public virtual void DisCardFood()
    {
        PlayerData.instance.currSingleLevelData.disCardFoodCount++;
        SpecialLimitOpertion(SpecialLimitType.NoDisCard);
    }

    private void SpecialLimitOpertion(SpecialLimitType limitType)
    {
        if (PlayerData.instance.specialLimit == limitType)
        {
            if (PlayerData.instance.currSelectPropList.Contains(ConsumePropType.InvalidSpecialConditions))
                return;
            EventSpecialLimit msg = new EventSpecialLimit();
            msg.propType = ConsumePropType.InvalidSpecialConditions;
         
            SystemEventManager.TriggerEvent("SpecialLimit", msg);
        }
    }
}
