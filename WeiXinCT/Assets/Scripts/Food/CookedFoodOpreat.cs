using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 食物操作盘子
/// </summary>
public class CookedFoodOpreat : MonoBehaviour
{
    private OperationManage operationManage;

    [SerializeField]
    private List<GameObject>plateParent = new List<GameObject>();
    [SerializeField]
    private List<GameObject> plateMainFood = new List<GameObject>();

    private List<GameObject> mainFoodList = new List<GameObject>();

    private int originMaxMake;
    private int maxMake = 4;
  //  private int currFoodLenth = 0;
    private int foodSubmintGrade = 0;

    private string[] foodSubmitDeviceName = new string[2];
    public string[] FoodSubnitDeviceName { get => foodSubmitDeviceName; set => foodSubmitDeviceName = value; }
    public List<GameObject> PlateParent { get => plateParent;}
    // private int currAddFoodIndex = 0;
    //所有设备等级
    private Dictionary<string, int> deveiceNameGradeDit = new Dictionary<string, int>();

    public OperationManage OperationManage { set => operationManage = value; }

    public void ResetCookedFood() 
    {
        //currFoodLenth = 0;
    }

    //在盘子生成食物后期显示
    public void  LoadingCookedMinFood(/*MainFoodCompnentAssets foodData, */MainFoodItem foodItem)
    {
        FoodSubmitDeviveGrade();
        for (int i = 0; i < plateParent.Count; i++)
        {
            var tempName = plateParent[i].GetComponent<PlateItem>().ItemName;
            if (deveiceNameGradeDit.ContainsKey(tempName) && deveiceNameGradeDit[tempName] <= 0)
                continue;

            deveiceNameGradeDit[tempName]--;

            var mainFood = plateMainFood[i];
          
            mainFoodList.Add(mainFood);
            mainFood.SetActive(false);
            var mainfoodcompnent = mainFood.GetComponent<FoodProperty>();
            mainfoodcompnent.OperationManage = operationManage;
            mainfoodcompnent.foodState = FoodState.OnPlate;
            mainfoodcompnent.PlateIndex = i;
            if (mainfoodcompnent.timeParent != null)
            mainfoodcompnent.timeParent.gameObject.SetActive(false);
            mainfoodcompnent.mainFoodIndex = mainFoodList.Count-1;
            mainfoodcompnent.foodPrice = foodItem.mainFoodPrice;
            
            int grade = 1;
            //查看等级
            if (PlayerData.foodDataPramerer.foodDataDict.ContainsKey(mainfoodcompnent.FoodNmae))
            {
                grade = int.Parse(PlayerData.foodDataPramerer.foodDataDict[mainfoodcompnent.FoodNmae]);
            }
            //试用升级
            if (PlayerData.instance.currSingleLevelData.foodTyeUpDit.ContainsKey(mainfoodcompnent.foodName))
            {
                grade = int.Parse(PlayerData.instance.currSingleLevelData.foodTyeUpDit[mainfoodcompnent.foodName]);
            }
            mainfoodcompnent.foodGrade = grade;
            mainfoodcompnent.InitFood();

            plateParent[i].gameObject.SetActive(true);
            plateParent[i].GetComponent<PlateItem>().FoodItem = mainfoodcompnent;
            plateParent[i].GetComponent<PlateItem>().FoodNmae = mainfoodcompnent.FoodNmae;
            plateParent[i].GetComponent<PlateItem>().MainFoodPrice = foodItem.mainFoodPrice;
            plateParent[i].GetComponent<PlateItem>().FoodNmae = mainfoodcompnent.FoodNmae;
            plateParent[i].GetComponent<PlateItem>().plateIndex  = i;
            Debug.Log(plateParent[i].name + "   " + mainfoodcompnent.FoodNmae);

            var plateName = plateParent[i].GetComponent<PlateItem>().ItemName;
            int plateGrade = 1;
            if (PlayerData.deviceDataParameter.deviceDict.ContainsKey(plateName))
                plateGrade = int.Parse(PlayerData.deviceDataParameter.deviceDict[plateName]);

            var skin = plateParent[i].GetComponent<CompomentSkin>().compnentSkin;
            plateParent[i].GetComponent<Image>().sprite = skin[plateGrade - 1];
            plateParent[i].GetComponent<PlateItem>().PlateGrade = plateGrade;

        }
    }


    private void FoodSubmitDeviveGrade()
    {
        //var deviceNmae = foodSubmitDeviceName[0];
        //if (PlayerData.deviceDataParameter.deviceDict.ContainsKey(deviceNmae))
        //{
        //    foodSubmintGrade = int.Parse(PlayerData.deviceDataParameter.deviceDict[deviceNmae]);
        //}
        //else
        //{
        //    foodSubmintGrade = 1;
        //}
        //var deviceComfigur = SystemDataFactoy.instans.GetResourceConfigur("DeviceConfigur") as DeviceConfigur;
        //var deviceAsset = deviceComfigur.GetDevice(deviceNmae);
        //if (deviceAsset != null)
        //{
        //    originMaxMake = deviceAsset.maxCount;
        //    maxMake = deviceAsset.maxCount + (foodSubmintGrade - 3);
        //}
        var deviceComfigur = SystemDataFactoy.instans.GetResourceConfigur("DeviceConfigur") as DeviceConfigur;
        for (int i = 0; i < plateParent.Count; i++)
        {
     
            var strName = plateParent[i].GetComponent<PlateItem>().ItemName;
            var deviceAsset = deviceComfigur.GetDevice(strName);
            if (!deveiceNameGradeDit.ContainsKey(strName))
            {
                deveiceNameGradeDit[strName] = 0;
            }
            if (PlayerData.deviceDataParameter.deviceDict.ContainsKey(strName))
            {
                deveiceNameGradeDit[strName] = int.Parse(PlayerData.deviceDataParameter.deviceDict[strName]);
            }
            else
            {
                deveiceNameGradeDit[strName] = 1;
            }

            deveiceNameGradeDit[strName] = deveiceNameGradeDit[strName] + deviceAsset.minCount - 1;


        }
        maxMake = plateParent.Count;
    }

    //添加主食
    public bool AddFoodCooker(string foodName)
    {
       if (GetSubmitDeviceFoodCount() == maxMake)
            return false;

        for (int i = 0; i < mainFoodList.Count; i++)
        {
            var plateCompnent = mainFoodList[i].GetComponent<FoodProperty>();
            if (mainFoodList[i].activeSelf == false )
            {
 
                if (plateCompnent.FoodNmae == foodName)
                {
                    mainFoodList[i].SetActive(true);
                    var mainFoodCompnent = mainFoodList[i].GetComponent<FoodProperty>();
                    mainFoodCompnent.CallFoodAddCompelet();

                    mainFoodCompnent.foodState = FoodState.OnPlate;
                    var foodParent = mainFoodList[i].transform.parent.GetComponent<PlateItem>();
                    if (foodParent != null)
                    {
                        foodParent.FoodNmae = foodName;
                        foodParent.FoodItem = mainFoodCompnent;

                        return true;
                    }
                  
                }
            }
        }
        return false;
    }

    //添加配菜
    public bool AddOtherFood(string mainFood, string oterFood, float price)
    {
        for (int i = 0; i < mainFoodList.Count; i++)
        {
            if (mainFoodList[i].activeSelf == false)
                continue;

            var mainfoodNmae = mainFoodList[i].GetComponent<FoodProperty>().FoodNmae;
            var index = mainFoodList[i].GetComponent<FoodProperty>().PlateIndex;
            var plateItem = plateParent[index].GetComponent<PlateItem>();
            if (mainfoodNmae == mainFood)
            {
                if (plateItem.AddOtherFood(oterFood))
                {
                    return true;
                }
            }
        }
        return false;
    }

    //食物提交完成
    public void SubmitFoodCompelet(int index, Transform targetTransform)
    {
       // mainFoodList[index].SetActive(false);
        var plateCompnent =  plateParent[index].GetComponent<PlateItem>();
        plateCompnent.SubmitSuccess(targetTransform);

    }

    private int  GetSubmitDeviceFoodCount()
    {
        int count = 0;
        for (int i=0; i< mainFoodList.Count; i++)
        {
            if (mainFoodList[i].gameObject.activeSelf)
                count++;
        }
            return count;
    }

    //是否有空
    public  bool HaveIsEmepty()
    {
        return GetSubmitDeviceFoodCount() == maxMake ? false : true;
    }
    //自动提交食物
    public IEnumerator AutomaticSabmitFood()
    {
        

        for (int i = 0; i < plateParent.Count; i++)
        {
            if (i >= maxMake)
                break;
            if (mainFoodList[i].activeSelf == false)
                continue;
            yield return new WaitForSeconds(0.75f);
            var component = plateParent[i].GetComponent<PlateItem>();
            component.AutomaticSubmitFood();
        }
    }
}
