using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;


enum DrinkState : int 
{
    Idle,
    Add,
    Complete

}

public class DrinkOpreat : MonoBehaviour
{
    private SystemGameManager systemGameManager;
    private Tween steakDTween;

    [SerializeField]
    private Image timeSuchdule;
    [SerializeField]
    private GameObject timeCleck;

    [SerializeField]
    private TextMeshProUGUI timeText;

    [SerializeField]
    private GameObject cup;

    [SerializeField]
    private DeviceCup[] deviceCup;
    [SerializeField]
    private DeviceCup[] submitCop;


    
    private DrinkState drinkState = DrinkState.Idle;
    public float addTime = 5f;
    public float indexTime = 0f;
    private string drinkName = string.Empty;
    private string drinkDeviceName = string.Empty;
    private string drinkID = string.Empty;
    private int drinkPrice = 0;
    private float automaticSubmitTime = 1f;
    private float submitTimeCount = 0;
    private int drinkGrade = 0;
    private int fillCupCount = 0;
    private int submitIndex = 0;
    private int addDrinkIndex = 0;
    private float drinKfillCheck;
  
    public string DrinkName { get => drinkName; set => drinkName = value; }
    public string DrinkDeviceName { set => drinkDeviceName = value; }
    public string DrinkID { get => drinkID; set => drinkID = value; }

    public int DrinkPrice { get => drinkPrice; set => drinkPrice = value; }

    private void Start()
    {
        indexTime = 0f;
        timeSuchdule.fillAmount = 0;
        
        timeText.text = string.Format("{0:N1}S", 0.0);
    }

    public void Configuer(SystemGameManager systemGameManager)
    {
        this.systemGameManager = systemGameManager;
        CoroutineHandler.StartStaticCoroutine(SartAddDrink());
    }

    private bool[] fillCup = { false, false, false };
    private void Update()
    {

        if (fillCupCount < drinkGrade)
        {
            int cupEmply = 0;
            drinKfillCheck += Time.deltaTime;
           // if (drinKfillCheck>1)
            //{
                for (int i = 0; i < submitCop.Length; i++)
                {
                    if (i >= drinkGrade)
                        break;

                    if (!submitCop[i].gameObject.activeSelf)
                    {
                        deviceCup[i].gameObject.SetActive(true);
                        deviceCup[i].SetCupEmply();
                        timeCleck.SetActive(true);
                    }
                }
            //}

            if (timeCleck.activeSelf)
            {
                indexTime += Time.deltaTime;
                if (indexTime < addTime)
                {
                    timeSuchdule.fillAmount = indexTime / addTime;
                }
                else
                {
                   timeCleck.SetActive(false);
                    timeSuchdule.fillAmount = 0;
                    indexTime = 0;
                    for (int i = 0; i < deviceCup.Length; i++)
                    {
                        if (deviceCup[i].gameObject.activeSelf)
                        {
                            deviceCup[i].SetCupFill();
                            fillCupCount++;
                        }//
                           
                    }
                   
                   StartCoroutine(DrinkFill());

                }
            }
           
        }

        //自动提交
        if ( PlayerData.instance.currSingleLevelData.automaticSubmitProp && fillCupCount>0)
        {
            submitTimeCount += Time.deltaTime;
            if (submitTimeCount >= automaticSubmitTime)
            {
               for (int i=0; i<submitCop.Length; i++)
                {
                    if (submitCop[i].gameObject.activeSelf)
                        OnClick(i+1);
                }
                submitTimeCount = 0;
            }
        }
    }

    public void OnClick(int index)
    {
        //if (PlayerData.instance.teachingValue.SubmitDrinkClick == 0)
        //    return;

        if (fillCupCount > 0)
        {
            submitIndex = index;

            var mainFoodDct = LoadConfigursManager.instans.MainFoodItemDictionary;

            EventAddDrink eventMsg = new EventAddDrink();
            eventMsg.drinkName = drinkName;
            eventMsg.drinkID = drinkID;
            eventMsg.index = index;
            var drinckGrade = 1;
            if (PlayerData.foodDataPramerer.foodDataDict.ContainsKey(drinkName))
            {
                drinckGrade = int.Parse(PlayerData.foodDataPramerer.foodDataDict[drinkName]);
            }
            eventMsg.drinkPrice = drinkPrice + drinckGrade -1;
            eventMsg.DataObject = this.gameObject;
            eventMsg.drinkIDNum = float.Parse( mainFoodDct[drinkName].mianFoodID);
            SystemEventManager.TriggerEvent("GuestAddDrink", eventMsg);
        }
    }

    private IEnumerator SartAddDrink()
    {
        yield return new WaitForSeconds(5);
        drinkState = DrinkState.Add;
    }

    //对比饮料成功
   public void SubmitDrinkSucess( Transform target,int index)
    {
        drinkState = DrinkState.Add;
        indexTime = 0;
        timeSuchdule.fillAmount = 0;
        timeText.text = string.Format("{0:N1}S", 0.0);
        SucessEffice(target, submitCop[submitIndex - 1].transform);

        submitCop[submitIndex - 1].gameObject.SetActive(false);
        fillCupCount--;
    }
    public void InitDrink()
    {
        var grade = 1;
       if (PlayerData.deviceDataParameter.deviceDict.ContainsKey(drinkDeviceName))
       {
            grade = int.Parse( PlayerData.deviceDataParameter.deviceDict[drinkDeviceName]);
       }
        drinkGrade = grade;
        ShowGradeImg();
    }

    private void ShowGradeImg()
    {
        var drinkDeviceImg = this.GetComponent<Image>();
        var skin = this.GetComponent<CompomentSkin>();
        drinkDeviceImg.sprite = skin.compnentSkin[drinkGrade - 1];

        for (int i=0; i<3; i++)
        {
            if (i >= drinkGrade)
                break;

            //deviceCup[i].gameObject.SetActive(true);
            
            var dEskin = deviceCup[i].emplyImg.GetComponent<CompomentSkin>();
            deviceCup[i].emplyImg.GetComponent<Image>().sprite = dEskin.compnentSkin[drinkGrade - 1];

            var dFskin = deviceCup[i].fillImg.GetComponent<CompomentSkin>();
            deviceCup[i].fillImg.GetComponent<Image>().sprite = dFskin.compnentSkin[drinkGrade - 1];

           // submitCop[i].gameObject.SetActive(true);
            var sEskin = submitCop[i].emplyImg.GetComponent<CompomentSkin>();
            submitCop[i].emplyImg.GetComponent<Image>().sprite = sEskin.compnentSkin[drinkGrade - 1];

            var sFskin = submitCop[i].fillImg.GetComponent<CompomentSkin>();
            submitCop[i].fillImg.GetComponent<Image>().sprite = sFskin.compnentSkin[drinkGrade - 1];
        }
    }
    private IEnumerator DrinkFill()
    {
        yield return new WaitForSeconds(0.5f);


        for (int i = 0; i < deviceCup.Length; i++)
        {
            if (deviceCup[i].gameObject.activeSelf)
            {
                submitCop[i].gameObject.SetActive(true);
                deviceCup[i].gameObject.SetActive(false);
            }
        }
       CoroutineHandler.StartStaticCoroutine( TeachingOPeration());
    }
    private IEnumerator TeachingOPeration()
    {
        yield return new WaitForSeconds(0.5f);
        if (PlayerData.instance.teachingValue.SubmitDrinkClick == 0)
            systemGameManager.UiManager.TeachingOperation(TeachingType.SubmitDrink);
    }

    private void SucessEffice(Transform target,Transform orgin)
    {
        var effice = Instantiate(orgin.gameObject);
        effice.transform.transform.parent = orgin.parent;
        effice.transform.transform.position = orgin.position;
        effice.transform.localScale = Vector3.one * 0.8f;
        effice.transform.DOMoveY(target.transform.position.y, 0.3f).SetEase(Ease.InOutSine);
        effice.transform.DOMoveX(target.transform.position.x, 0.5f).SetEase(Ease.InSine).onComplete = () =>
        {
            effice.gameObject.SetActive(false);
            Destroy(effice);
        };

    }

}
