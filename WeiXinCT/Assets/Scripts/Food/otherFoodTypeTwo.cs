using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class otherFoodTypeTwo : OtherFood
{
    // Start is called before the first frame update
    private Tween tween;
    public override void ClickFood()
    {
        //if (PlayerData.instance.teachingValue.addOtherFoodClick == 0)
            //return;

        if (tween == null)
        {
            tween = this.transform.DOScale(1.1f, 0.1f).SetLoops(2, LoopType.Yoyo);
        }
        else if (!tween.active)
        {
            tween = this.transform.DOScale(1.1f, 0.1f).SetLoops(2, LoopType.Yoyo);
        }
        //OperationAddOtherFiid
        EventAddOtherFood eventMsg = new EventAddOtherFood();
        eventMsg.foodNmae = foodNmae;
        eventMsg.mainFoodName = mainFoodName;
        eventMsg.foodPrice = foodPrice;
        eventMsg.DataObject = this.gameObject;
        SystemEventManager.TriggerEvent("OperationAddOtherFiid", eventMsg);
    }
}
