using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MakeingOtherFoodCompnent : OtherFood
{
    [SerializeField]
    private float makeTime;     //制作时间
    [SerializeField]
    private float maxMakeTime;    //最大制作时间

    [SerializeField]
    private FoodClock foodClock;

    private FoodState foodState;

    private float timeCount;
    private float conbustionTime;
    private Tween tween;
    private int clickCount = 0;
    private float addTime = 0;

    private void OnEnable()
    {
        timeCount = makeTime;
        conbustionTime = maxMakeTime;
        if (foodClock!= null)
        foodClock .gameObject.SetActive(true);
        foodState = FoodState.NullState;
    }

    private void Update()
    {
        if (foodState == FoodState.Making)          //制作状态
        {
            timeCount -= Time.deltaTime;
            float temp = timeCount / (makeTime - addTime);

            foodClock.SetClockMakeSate(temp);
            if (timeCount <= 0f)
            {
                foodState = FoodState.Cooke;
            }
            SetFoodSkin(foodState);
        }
        else if (foodState == FoodState.Cooke)  //完成状态
        {
           
                conbustionTime -= Time.deltaTime;
                float temp = conbustionTime / maxMakeTime;
                foodClock.SetClockCoodeState(temp);
            if (conbustionTime <= 0f)
            {
                foodClock.gameObject.SetActive(false);
                foodState = FoodState.Burning;
                base.OnFoodFire();

            }
            SetFoodSkin(foodState);

        }

        else if (foodState == FoodState.Burning) //烧糊状态
        {


               // if (clickNumber != 0)
            /// {
            //    clickIndex += Time.deltaTime;
            //    if (clickIndex > clickTime && clickNumber >= 2)
            //    {
            //        clickNumber = 0;
            //        clickIndex = 0f;
            //        this.gameObject.SetActive(false);
            //        base.DisCardFood();
            //    }
            //}
            //SetFoodSkin(foodState);
            //base.FoodFire();

        }
    }

    private void SetFoodSkin(FoodState foodState)
    {

    }
    public void OnFoodClick()
    {
        if (tween == null)
        {
            tween = this.transform.DOScale(1.1f, 0.1f).SetLoops(2, LoopType.Yoyo);
        }
        else if (!tween.active)
        {
            tween = this.transform.DOScale(1.1f, 0.1f).SetLoops(2, LoopType.Yoyo);
        }

        if (foodState == FoodState.Cooke)
        {
            EventAddOtherFood eventMsg = new EventAddOtherFood();
            eventMsg.foodNmae = foodNmae;
            eventMsg.mainFoodName = mainFoodName;
            eventMsg.foodPrice = foodPrice;
            eventMsg.DataObject = this.gameObject;
            eventMsg.otherFoodComponent = this;
            SystemEventManager.TriggerEvent("OperationAddOtherFiid", eventMsg);
 
        }
        else if (foodState == FoodState.Burning) //烧糊状态
        {

            clickCount++;
            if (clickCount == 1)
            {
                StartCoroutine(ClickDisCard());
            }
            if (clickCount >= 2)
            {
                clickCount = 0;
                ClearFood();
            }
        }
    }
    public override void AddOtherFoodMekeing()
    {
        foodState = FoodState.Making;
        timeCount = makeTime - addTime;
    }
    private IEnumerator ClickDisCard()
    {
        yield return new WaitForSeconds(1f);
        clickCount = 0;
    }

    public override void ClearFood()
    {
        base.ClearFood();
    }

    public override void HandInFood(bool isSuccess)
    {
        if (isSuccess)
            gameObject.SetActive(false);
        base.HandInFood(isSuccess);
    }
    public override void SetMakeTime(float time)
    {
        addTime = time;
        timeCount = makeTime - addTime;
    }
}


