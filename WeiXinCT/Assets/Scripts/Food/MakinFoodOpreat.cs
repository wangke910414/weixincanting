using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//灶台
public class MakinFoodOpreat : MonoBehaviour
{
    private OperationManage operationManage;
    [SerializeField]
    private List<GameObject> makingFoodListParent = new List<GameObject>(); // 主食烤盘

    [SerializeField]
    private List<GameObject> makeingOtgherFoodParent = new List<GameObject>(); //其他食物制作设备

    [SerializeField]
    private List<GameObject> deviceFood = new List<GameObject>();
    [SerializeField]
    private List<GameObject> deviceOtherFood = new List<GameObject>();

   //private List<GameObject> mainFoodList = new List<GameObject>();
    public OperationManage OperationManage { set => operationManage = value; }

    private List<FoodProperty> foodList = new List<FoodProperty>();
    public List<FoodProperty> FoodList { get => foodList; }
    //所有设备等级
    private Dictionary<string, int> deveiceNameGradeDit = new Dictionary<string, int>();
    private int originMakeCount;
    private int maxMake = 4;
    [SerializeField]
    private int maxDeveiceCount;
    private int currFoodLenth = 0;

    private int foodMakeDeveiceGrade;
    private float originFoodMakeTime;
    private float newFoodMakeTike;

    private string [] foodMakeDeviceNasme = new string[2];
    public string [] FoodMakeDeviceNasme { get => foodMakeDeviceNasme; set => foodMakeDeviceNasme = value; }
    
    // private int currAddFoodIndex = 0;

    //在灶台生成食物后期显示
    public void LoadingMakinMinFood(/*MainFoodCompnentAssets foodData*/)
    {        

       // if ( foodData != null)
        {
            FoodMakeDeviceGrade();

            int maxGrade = 0;
            for (int i=0; i< makingFoodListParent.Count; i++ )
            {
                var tempName = makingFoodListParent[i].GetComponent<DeveiceComponent>().DeveiceName;
                if (deveiceNameGradeDit.ContainsKey(tempName) && deveiceNameGradeDit[tempName] <= 0)
                    continue;

                if (i==0)
                {
                    maxGrade = deveiceNameGradeDit[tempName];
                }
                deveiceNameGradeDit[tempName]--;

                makingFoodListParent[i].SetActive(true);
                var mainFood = deviceFood[i];
                //mainFoodList.Add(deviceFood[i]);
                mainFood.SetActive(false);
           
                  var mainfoodcompnent = mainFood.GetComponent<FoodProperty>();
                int gradeNumber = 1;
                if (foodMakeDeviceNasme.Length > 0 && PlayerData.deviceDataParameter.deviceDict.ContainsKey(foodMakeDeviceNasme[0]))
                    gradeNumber = int.Parse(PlayerData.deviceDataParameter.deviceDict[foodMakeDeviceNasme[0]]);

                int foodGrade = 1;
                if (PlayerData.foodDataPramerer.foodDataDict.ContainsKey(mainfoodcompnent.foodName))
                {
                    foodGrade = int.Parse(PlayerData.foodDataPramerer.foodDataDict[mainfoodcompnent.foodName]);
                    Debug.Log("食物等级  " + foodGrade);
                }
                //试用升级
                if (PlayerData.instance.currSingleLevelData.foodTyeUpDit.ContainsKey(mainfoodcompnent.foodName))
                {
                    foodGrade = int.Parse(PlayerData.instance.currSingleLevelData.foodTyeUpDit[mainfoodcompnent.foodName]);
                    Debug.Log("试用食物等级  " + foodGrade);
                }
                mainfoodcompnent.OperationManage = operationManage;
                mainfoodcompnent.foodGrade = foodGrade;
                mainfoodcompnent.InitFood();
                newFoodMakeTike = maxGrade * 1f;
                mainfoodcompnent.SetFoodMakeTiem(newFoodMakeTike);
                makingFoodListParent[i].GetComponent<Image>().sprite = makingFoodListParent[i].GetComponent<RestaurantDevice>().DeviceGradeSkin[gradeNumber - 1];
                makingFoodListParent[i].GetComponent<DeveiceComponent>().DeveiceGradeUp = gradeNumber;
                makingFoodListParent[i].GetComponent<DeveiceComponent>().SetDeveiceInit();
                ConsumePropRole(mainfoodcompnent);
                mainfoodcompnent.mainFoodIndex = i;
                foodList.Add(mainfoodcompnent);
                currFoodLenth++;
            }
        }


        //其他设备
        if (makeingOtgherFoodParent.Count > 0)
        {
            var gradeNumber = 1;
            var deiveice = makeingOtgherFoodParent[0].GetComponent<DeveiceComponentTypeOne>();
            if (deiveice == null)
            {
                Debug.Log("空其他设备   ");
                return;
            }

            if (PlayerData.deviceDataParameter.deviceDict.ContainsKey(deiveice.DeveiceName))
               gradeNumber = int.Parse(PlayerData.deviceDataParameter.deviceDict[deiveice.DeveiceName]);
            deiveice.DeveiceGradeUp = gradeNumber;
            deiveice.SetDeveiceInit();
        }
    }

    //设备等级操作
    private void FoodMakeDeviceGrade()
    {
        // var deviceNmae = foodMakeDeviceNasme[0];
        //if (PlayerData.deviceDataParameter.deviceDict.ContainsKey(deviceNmae))
        // {
        //     foodMakeDeveiceGrade = int.Parse(PlayerData.deviceDataParameter.deviceDict[deviceNmae]);
        // }
        //else
        // {
        //     foodMakeDeveiceGrade = 1;
        // }


        var deviceComfigur = SystemDataFactoy.instans.GetResourceConfigur("DeviceConfigur") as DeviceConfigur;
 
        for (int i=0; i< makingFoodListParent.Count; i++)
        {
            var strName = makingFoodListParent[i].GetComponent<DeveiceComponent>().DeveiceName;
            if (!deveiceNameGradeDit.ContainsKey(strName))
            {
                deveiceNameGradeDit[strName] = 0;
            }
            if (PlayerData.deviceDataParameter.deviceDict.ContainsKey(strName))
            {
                deveiceNameGradeDit[strName] = int.Parse(PlayerData.deviceDataParameter.deviceDict[strName]);
            }
            else
            {
                deveiceNameGradeDit[strName] = 1;
            }

            var deviceAsset = deviceComfigur.GetDevice(strName);
            deveiceNameGradeDit[strName] = deveiceNameGradeDit[strName] + deviceAsset.minCount - 1;
        }


        //if (deviceAsset != null)
        //{
        //    originFoodMakeTime = deviceAsset.makeTime;
        //    newFoodMakeTike = originFoodMakeTime - foodMakeDeveiceGrade - 1;
        //    originMakeCount = deviceAsset.maxCount;
        //    maxMake = deviceAsset.maxCount + (foodMakeDeveiceGrade - 3);
        //}

    }
   

    private void ConsumePropRole(FoodProperty food)
    {
        
        for (int i=0; i<PlayerData.instance.currSelectPropList.Count; i++)
        {
            if (PlayerData.instance.currSelectPropList[i] == ConsumePropType.MomentFood)
            {
                food.SetFoodMakeTiem(0);
            }
            else if (PlayerData.instance.currSelectPropList[i] == ConsumePropType.FireFood)
            {
                food.SetNoFireFood();
            }
        }
    }



    public void ResetMakinFood()
    {
        currFoodLenth = 0;
    }

    //制作主食
    public void AddFoodMaking(string foodName)
   {
        for (int i=0; i<foodList.Count; i++)
        {
            if (i >= maxMake)
                break;
            var fName = foodList[i].GetComponent<SteakFood>().foodName;
            if (foodName == fName)
            {
                if (foodList[i].gameObject.activeSelf == false)
                {

                    foodList[i].gameObject.SetActive(true);
                    foodList[i].gameObject.GetComponent<FoodProperty>().foodState = FoodState.Making;
                    var device = makingFoodListParent[i].GetComponent<DeveiceComponent>();
                    if (device != null)
                        device.SetDeveiceState(DeveiceState.Makeing);
                    //foodList[i].gameObject.GetComponent<FoodProperty>().CallFoodAddCompelet();
                    return;
                }
            }
        }
    } 

    /// <summary>
    /// 
    /// </summary>
    /// <param name="isCompelet"></param>
    /// <param name="index"></param>
    public void AddMainfoodReceive(bool isCompelet, int index)
    {
        if (isCompelet)
        {
            if (index >= makingFoodListParent.Count || index < 0)
                return;
            var device = makingFoodListParent[index].GetComponent<DeveiceComponent>();
            if (device != null)
                device.SetDeveiceState(DeveiceState.Wait);
        }
    }

    //配菜制作

    public void AddOtherFoodMaking(string foodName)
    {
        var device = makeingOtgherFoodParent[0].GetComponent<DeveiceComponentTypeOne>();
        for (int i=0; i< device.DeveiceGradeUp; i++)
        {
           // if (makeingOtgherFoodParent[i].activeSelf)
            {
                if (!deviceOtherFood[i].activeSelf)
                {
                    deviceOtherFood[i].SetActive(true);
                    deviceOtherFood[i].gameObject.GetComponent<OtherFood>().AddOtherFoodMekeing();
                    deviceOtherFood[i].gameObject.GetComponent<OtherFood>().SetMakeTime(device.DeveiceGradeUp * 1f);
                    return;
                }
            }
        }
    }
    /// <summary>
    /// 添加成功
    /// </summary>
    /// <param name="index"></param>
    /// <param name="active"></param>
    public void SetMainFoodListItem(int index, bool active)
    {
        if (index >= foodList.Count)
            return;
        foodList[index].gameObject.SetActive(active);
    }
}
