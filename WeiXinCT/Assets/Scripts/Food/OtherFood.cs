using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum IsMake
{
    Null,
    Make,
    NoMake,
}

public class OtherFood : MonoBehaviour
{
    public string foodNmae = string.Empty;

    public string mainFoodName = string.Empty;

    [SerializeField]
    protected IsMake isMake;

    public float foodPrice = 0.0f;

    public Sprite[] foodGradeImg;

    public  virtual void ClickFood()
    {
        OnFoodDisCard();
    }
    public virtual void ClearFood()
    {
        gameObject.SetActive(false);
        OnFoodDisCard();
    }
    //添加食物制作
    public virtual void AddOtherFoodMekeing()
    {

    }
    //上交
    public virtual void HandInFood(bool isSuccess)
    {

    }

    //设置时间
    public virtual void SetMakeTime(float time)
    {

    }
    public virtual void OnFoodFire()
    {
        PlayerData.instance.currSingleLevelData.FoodFireCount++;
        SpecialLimitOpertion(SpecialLimitType.NoFire);
    }
    public virtual void OnFoodDisCard()
    {
        PlayerData.instance.currSingleLevelData.disCardFoodCount++;
        SpecialLimitOpertion(SpecialLimitType.NoDisCard);
    }
    private void SpecialLimitOpertion(SpecialLimitType limitType)
    {
        if (PlayerData.instance.specialLimit == limitType)
        {
            if (PlayerData.instance.currSelectPropList.Contains(ConsumePropType.InvalidSpecialConditions))
                return;
            EventSpecialLimit msg = new EventSpecialLimit();
            msg.propType = ConsumePropType.InvalidSpecialConditions;
            SystemEventManager.TriggerEvent("SpecialLimit", msg);
        }
    }
}
