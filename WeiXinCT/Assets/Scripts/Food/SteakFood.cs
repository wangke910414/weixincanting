using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
/// <summary>
/// ţ��
/// </summary>
public class SteakFood : FoodProperty
{
   // private float makeTime;

  // SerializeField]
   /// ivate TextMeshProUGUI timeTxt;

   /// [SerializeField]
   // private Image suchduleImage;

   // [SerializeField]
    //private float maxCombustionTime;

    private int clickNumber = 0;

    private float clickTime = 0.5f;

    private Transform OtherFoodParent;
    [Header("����ʱ��")]
    [SerializeField]
    private float makeTime;

    [Header("�պ�ʱ��")]
    [SerializeField]
    private float fireTime;

    [SerializeField]
    private FoodClock foodClock ;

    [Header("ʳ��״̬")]
    [SerializeField]
    private GameObject[] foodShowSkin;
    private float clickIndex = 0;
    private Tween steakDTween;
    private float addTime = 0f;
   
    public  override void ClilkFood()
    {
        if (foodState == FoodState.Cooke) 
        {
                EventRawFoodAnd eventParam = new EventRawFoodAnd();
            eventParam.foodNmae = foodName;
            eventParam.makeType = MakeType.NeedMak;
                eventParam.DataObject = this.gameObject;
            eventParam.index = mainFoodIndex;
                SystemEventManager.TriggerEvent("OperationCookeFood", eventParam);
            if (makeingSound.isPlaying)
            {
                makeingSound.Stop();
            }
        }
       else if (foodState == FoodState.Burning)
        {
            clickNumber++;
            if (clickNumber == 1)
            {
                StartCoroutine(ClickDisCard());
            }
            if (clickNumber >= 2)
            {
                clickNumber = 0;
                ClearFood();
            }

        }
        if (steakDTween != null && !steakDTween.active)
        {
            steakDTween.Kill();
            steakDTween = this.transform.DOScale(1.1f, 0.1f).SetLoops(2, LoopType.Yoyo);
        }
    }

    private void OnEnable()
    {
        if (foodState != FoodState.OnPlate)
        {
             CookedTime = makeTime - addTime;
            conbustionTime = fireTime;

            if (timeParent != null)
            timeParent.gameObject.SetActive(true);
            clickNumber = 0;
            clickIndex = 0;
        }
        steakDTween = this.transform.DOScale(1.1f, 0.2f).SetLoops(2, LoopType.Yoyo);
    }
   
    private void OnDisable()
    {
        if (foodState != FoodState.OnPlate)
        {
            CookedTime = 0f;
            conbustionTime = 0f;
            foodState = FoodState.Raw;
            if (timeParent != null)
                timeParent.gameObject.SetActive(false);
        }

    }

   public virtual void  UpdateFoodState()
    {

        if (foodState == FoodState.Making)          //����״̬
        {
            if (!makeingSound.isPlaying)
            {
                makeingSound.Play();
            }
            CookedTime -= Time.deltaTime;
            float temp = CookedTime / (makeTime - addTime);

            foodClock.SetClockMakeSate(temp);
            if (CookedTime <= 0f)
            {
                foodState = FoodState.Cooke;
            }
            SetFoodSkin(foodState);
        }
        else if (foodState == FoodState.Cooke)  //���״̬
        {
            if (isFireFood)
            {
                conbustionTime -= Time.deltaTime;
                float temp = conbustionTime / fireTime;
                foodClock.SetClockCoodeState(temp);
                if (conbustionTime <= 0f)
                {
                    timeParent.gameObject.SetActive(false);
                    base.FoodFire();
                    foodState = FoodState.Burning;
                }
            }
            else
            {
                foodClock.gameObject.SetActive(false);
            }
            SetFoodSkin(foodState);

        }

        else if (foodState == FoodState.Burning) //�պ�״̬
        {

        }
    }
    public override void RestFood()
    {
        for (int i=0; i< otherFood.Length; i++)
        {
            otherFood[i].gameObject.SetActive(false);
        }
            base.RestFood();
    }

    public override void ClearFood()
    {
        for (int i = 0; i < otherFood.Length; i++)
        {
            if (otherFood[i]!=null)
                otherFood[i].gameObject.SetActive(false);
        }
        gameObject.SetActive(false);
        base.ClearFood();
    }

    private void Update()
    {
        UpdateFoodState();

    }

    /// <summary>
    /// ��ʾ���
    /// </summary>
    /// <param name="food"></param>
    public  override bool AddOtherFood(string food)
    {
        for (int i = 0; i < otherFood.Length; i++)
        {
            Debug.Log(otherFood[i].gameObject.name  +"   " + food);
            if (otherFood[i].gameObject.name == food)
            {
                if (otherFood[i].gameObject.activeSelf)
                    return false;
                otherFood[i].gameObject.SetActive(true);
                return true;
            }
        }
        return false;      
    }
    public override void SetFoodMakeTiem(float time)
    {
        addTime = time;
        CookedTime = (makeTime - time) < 0 ? 0 : makeTime - time;
    }
    public override void InitFood()
    {
        SetGradeSkin();
        base.InitFood();
    }

    public override void SetNoFireFood()
    {
        isFireFood = false;

        base.SetNoFireFood();
    }

    private void SetFoodSkin(FoodState state)
    {
        if (state == FoodState.Making)
        {
            foodShowSkin[0].gameObject.SetActive(true);
            foodShowSkin[1].gameObject.SetActive(false);
            foodShowSkin[2].gameObject.SetActive(false);
        }
        else if (state == FoodState.Cooke)
        {
            if (foodShowSkin[1].gameObject.activeSelf == false)
            {
                FoodMakeCompelet();
            }
            foodShowSkin[0].gameObject.SetActive(false);
            foodShowSkin[1].gameObject.SetActive(true);
            foodShowSkin[2].gameObject.SetActive(false);
            
        }
        else if (state == FoodState.Burning)
        {
            foodShowSkin[0].gameObject.SetActive(false);
            foodShowSkin[1].gameObject.SetActive(false);
            foodShowSkin[2].gameObject.SetActive(true);
        }
    }
    private void SetGradeSkin()
    {
        if (foodState != FoodState.OnPlate)
        {
            for (int i=0; i<foodShowSkin.Length; i++)
            {
                var skin = foodShowSkin[i].GetComponent<CompomentSkin>();
                foodShowSkin[i].GetComponent<Image>().sprite = skin.compnentSkin[foodGrade - 1];
            }
        }
        else if (foodState == FoodState.OnPlate)
        {
            this.GetComponent<Image>().sprite = foodGradeImg[foodGrade - 1];

            for (int i=0; i<otherFood.Length; i++)
            {
                int grade = 1;
                var otherItemName = otherFood[i].name;
               if (PlayerData.foodDataPramerer.foodDataDict.ContainsKey(otherItemName))
                   grade = int.Parse(PlayerData.foodDataPramerer.foodDataDict[otherItemName]);
                var skin = otherFood[i].GetComponent<CompomentSkin>().compnentSkin;
                otherFood[i].GetComponent<Image>().sprite = skin[grade - 1];
            }
        }   
    }
    private IEnumerator ClickDisCard()
    {
        yield return new WaitForSeconds(1f);
        clickNumber = 0;
    }
}
