using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Audio;

public enum HamburgState : int
{
    Null,
    HamburgOne,
    BeefCompelet,
    AddOther,
    HamburgTwo,


}
public class HamburgFood : FoodProperty
{
    [SerializeField]
    private GameObject leaves;      //��Ҷ
    [SerializeField]
    private GameObject cheese;      //����
    [SerializeField]
    private GameObject beef;        //���
    [SerializeField]
    private GameObject onion;       //���
    [SerializeField]
    private GameObject upHamburg;

    private int layerCount = 0;             //����       
    private HamburgState hanburgSate;
    private Vector3 hanburgUpOriginPos = Vector3.zero;
    //private float  moveY;
    private int[] moveCount = {  20,10,10,10};

    private void Start()
    {
        hanburgUpOriginPos = upHamburg.transform.localPosition;
    }
    public override void ClearFood()
    {
        this.gameObject.SetActive(false);
       upHamburg.transform.localPosition = hanburgUpOriginPos;
        base.ClearFood();
    }

    private void OnEnable()
    {
        upHamburg.transform.localPosition = hanburgUpOriginPos;
        layerCount = 0;
    }


    public override bool CallFoodAddCompelet()
    {
        hanburgSate = HamburgState.HamburgOne;
        UpdateShow();
        return false;

    }
    private void UpdateShow()
    {
        if (hanburgSate == HamburgState.HamburgOne)
        {
            leaves.SetActive(false);
            cheese.SetActive(false);
            beef.SetActive(false);
            onion.SetActive(false);
            upHamburg.SetActive(true);
            makeingSound.Play();
            

        }
        else if (hanburgSate == HamburgState.BeefCompelet)
        {

        }
        else if (hanburgSate == HamburgState.AddOther)
        {

        }
        else if (hanburgSate == HamburgState.HamburgTwo)
        {
            upHamburg.SetActive(true);
        }
    }
    public override bool AddOtherFood(string food)
    {
        for (int i=0; i<otherFood.Length; i++)
        {
            if (otherFood[i].gameObject.name == food && otherFood[i].gameObject.activeSelf == false)
            {
                otherFood[i].gameObject.SetActive(true);
               
                MoveHanburgUp();

                return true;
            } 
        }
        return false;
    }
    private void MoveHanburgUp()
    {
        var moveY = upHamburg.transform.localPosition.y + moveCount[layerCount];
        layerCount++;
        upHamburg.transform.localPosition = new Vector3(hanburgUpOriginPos.x, moveY, hanburgUpOriginPos.z);
        NimbleAnimation();
    }
    private void NimbleAnimation()
    {
        this.transform.DOScale(1.1f, 0.1f).SetEase(Ease.InSine).SetLoops(2, LoopType.Yoyo).onComplete = () => {
            this.transform.localScale = Vector3.one;
        };
    }
}
