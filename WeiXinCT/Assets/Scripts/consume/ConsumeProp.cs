using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ConsumePropType : int
{
    Null = 0,
    [Tooltip("快速制作")]
    MomentFood,                 //快速制作
    [Tooltip("菜不胡")]
    FireFood,                   //菜不胡
    [Tooltip("增加顾客耐心")]
    GuestPatience,              //增加顾客耐心
    [Tooltip("增加顾客三个")]
    AddGuest,                   //增加顾客三个
    [Tooltip("双倍金币")]
    SetlementDoubleCion,         //双倍金币
    [Tooltip("自动提交食物")]
    AutomaticSubmitFood,         //自动提交食物
    [Tooltip("增加连击时间间隔")]
    AddcontinuTimeIntterval,     //增加连击时间间隔
    [Tooltip("特俗限制无效")]
    InvalidSpecialConditions     //特俗限制无效
}

public enum UseType : int
{
    LongTerm,   //长期
    moment,     //瞬间
}

public class ConsumeProp : MonoBehaviour
{
    // Start is called before the first frame update

    public Sprite Icon;
    public int price;
    public string path;
    public int unLosckLevel;    //解锁关卡
    public ConsumePropType comsumePropType;
    public UseType useType;

}
