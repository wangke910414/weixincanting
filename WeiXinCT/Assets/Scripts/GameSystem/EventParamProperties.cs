using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EventParamProperties
{
    private GameObject dataObject;

    public string foodNmae;

    public GameObject DataObject { get => dataObject; set => dataObject = value; }

}

// class EventDeFood

public class EventAddFoodMakeing : EventParamProperties
{
    public FoodType foodType;
    public FoodSandwichTyoe foodSandwichType;
}
public class EventAddOtherFoodMakeing : EventParamProperties
{
   
}
public class EventDeliverCookedFood : EventParamProperties
{
    public string foodID;
    public float foodIDNum;
    public int plateIndex;
    public float totalPrice;
    public Transform needTransform;
}

public class EventAddOtherFood : EventParamProperties
{
    public string foodName;
    public string mainFoodName;
    public float foodPrice;
    public OtherFood otherFoodComponent;
}

public class EventAddDrink : EventParamProperties
{
    public string drinkName;
    public string drinkID;
    public int index;
    public float drinkIDNum;
    public int drinkPrice;
    public Transform needTransform;
}


//教学消息
public class EventTeachingMsg : EventParamProperties
{
    public TeachingType teachingType;
    
}
public class EventRawFoodAnd : EventParamProperties
{
    public MakeType makeType;
    public int index;
    public FoodSandwichTyoe foodSandwichTyoe;
}

//特殊限制消息
public class EventSpecialLimit : EventParamProperties
{
    public ConsumePropType propType;
}



