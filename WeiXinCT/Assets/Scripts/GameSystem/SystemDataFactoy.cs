using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;
using YooAsset;

public class SystemDataFactoy
{
    //private SystemGameManager systemGameManager = null;
    SystmConfigurationManager systmConfigurationManager;

    public static SystemDataFactoy instans;

    [SerializeField]
    private string foldePath = string.Empty;

    private bool yooAssetsLoadCompelet = false;
    public bool YooAssetsLoadCompelet { get => yooAssetsLoadCompelet;  }

    private Dictionary<string, ResourceProfile> dictionaryConfiguirs = new Dictionary<string, ResourceProfile>();

    private AssetBundle assetBundleResource;
    private Dictionary<string, AssetOperationHandle> yooOperationDict = new Dictionary<string, AssetOperationHandle>();
    
    private Dictionary<string, AssetBundle> assetBundleDict = new Dictionary<string, AssetBundle>();

    public Dictionary<string, AssetOperationHandle> YooOperationDict { get => yooOperationDict; }
    public Dictionary<string, ResourceProfile> DictionaryConfiguirs { get => dictionaryConfiguirs; set => dictionaryConfiguirs = value;  } 
    public IEnumerator Configur(SystmConfigurationManager systmConfigurationManager)
    {
        this.systmConfigurationManager = systmConfigurationManager;
        foldePath =systmConfigurationManager.AssetsFileName;
        yield return LoadConfigurs();
        yield return LoadYooAessets();
        yooAssetsLoadCompelet = true;
        yield return true;

    }
     
    public static bool Create()
    {
        if (instans == null)
        {
            instans = new SystemDataFactoy();
            return true;
        }
        return false;
    }

    public  IEnumerator LoadConfigurs()
    {
        ResourceProfile[] tempconfigur = Resources.LoadAll<ResourceProfile>(foldePath);


        foreach (ResourceProfile resourceProfile in tempconfigur)
        {
            if (dictionaryConfiguirs != null)
            {
                dictionaryConfiguirs.Add(resourceProfile.ConfigurName, resourceProfile);
               
            }
        }
        yield return dictionaryConfiguirs;
    }

    public ResourceProfile GetResourceConfigur(string strkey)
    {
        ResourceProfile returnConfigur = null;

        if (dictionaryConfiguirs.ContainsKey(strkey))
        {
            returnConfigur = dictionaryConfiguirs[strkey];
        }
        return returnConfigur;
    }

    private void LoadAssetBundle()
    {
        string path = Application.streamingAssetsPath + "/AssetResources/move.ab";
        //Debug.Log("ab  包路径" + path);

        assetBundleResource = AssetBundle.LoadFromFile(path);
    }
    private UnityWebRequest request;
    private IEnumerator WebglLoadAssetBundle()
    {
        var abNmae = systmConfigurationManager.abName;
        for (int i = 0; i < abNmae.Length; i++)
        {
            string path = Application.streamingAssetsPath + "/AssetResources/" + abNmae[i] + ".ab";
          // Debug.Log("开始加载   " + path);
            request = UnityWebRequestAssetBundle.GetAssetBundle(path);
            yield return request.SendWebRequest();
            var asset = (request.downloadHandler as DownloadHandlerAssetBundle).assetBundle;
            assetBundleDict.Add(abNmae[i], asset);
           // Debug.Log("加载完毕   " + assetBundleResource);
        }
    
    }

    public GameObject GetResource(string resoueceName)
    {
        if (assetBundleResource == null)
        {       
            return null;
        }
        GameObject obj = assetBundleResource.LoadAsset<GameObject>(resoueceName);
        return obj;
    }
    public GameObject GetResource(string  packgName, string resoueceName)
    {
        if (!assetBundleDict.ContainsKey(packgName))
        {
            return null;
        }
        GameObject obj = assetBundleDict[packgName].LoadAsset<GameObject>(resoueceName);
        return obj;
    }

    public IEnumerator LoadYooAessets()
    {
        while (true)
        {
            if (YooResoursceManagur.Instace.IsLoadCompelet)
            {
                Debug.Log("等待网络资源下载！");
                break;
            }
        }
        //加载其他文件
        if (YooResoursceManagur.Instace != null)
        {
            for (int i=0; i< systmConfigurationManager.yooAseets.Length; i++)
            {
                var opeertionHandle = YooResoursceManagur.Instace.GetPackage().LoadAssetAsync<GameObject>(systmConfigurationManager.yooAseets[i]);
                yield return opeertionHandle;
                yooOperationDict.Add(systmConfigurationManager.yooAseets[i], opeertionHandle);
            }
        }
        //加载地图文件
    }
    public AssetOperationHandle GetYooAssetHandle(string key)
    {
        if (yooOperationDict.ContainsKey(key))
        {
            return yooOperationDict[key];
        }
        return null;
    }

}
