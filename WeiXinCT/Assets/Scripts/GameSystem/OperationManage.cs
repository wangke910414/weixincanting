using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using System;
using UnityEngine.UI;
/// <summary>
/// 操作食物管理类
/// </summary>
public class OperationManage : MonoBehaviour
{

    /// <summary>
    /// 制作节点
    /// </summary>
    [SerializeField]
    private Transform foodMakeParent; 
    /// <summary>
    /// 放置节点
    /// </summary>
    private Transform PlateParent;
    /// <summary>
    /// 生食物节点
    /// </summary>
    private Transform rawFoodParent;

    private Transform rawOtherFoodParent;

    private LevelData currlevelData;
    private float continuTime = 0f;
    private float maxcontinuTime = 5f;
    private float propAddContinuTime = 0;
    private int continuCount = 0;
    private bool AddOtherFoodTeachingOnce = true;
    /// <summary>
    /// 当前关卡主食
    /// </summary>
    private Dictionary<string, MainFoodCompnentAssets> currMainFooResdDictionary = new Dictionary<string, MainFoodCompnentAssets>();
    public Dictionary<string, MainFoodCompnentAssets> CurrMainFooResdDictionary { get => currMainFooResdDictionary; }

    [SerializeField]
    private List<GameObject> rwaMainFoodList = new List<GameObject>(); //生主食

    private List<GameObject> reaOtherFoodList = new List<GameObject>(); //配菜
    public List<GameObject> RwaMainFoodList { get => rwaMainFoodList; }

    private SystemGameManager systemGameManager;
    public void Configuer(SystemGameManager systemGameManager)
    {
        this.systemGameManager = systemGameManager;
        Init();
    }
    private void Init()
    {

        SystemEventManager.StartListening("OperationMakingFood", CallMakingFood);
       // SystemEventManager.StartListening("OperationMakingOtherFood", CallMakingOtherFood);
        SystemEventManager.StartListening("OperationCookeFood", CallCoodeFood);
        SystemEventManager.StartListening("OperationSubmitCompeletFood", CallSubmitCompeletFood);
        SystemEventManager.StartListening("OperationSubmitCompeletDrink", CallSubmitCompeletDrink); 
        SystemEventManager.StartListening("OperationAddOtherFiid", CallAddOtherFood);
        SystemEventManager.StartListening("NewComeGuest", CallNewComeGuest);
        SystemEventManager.StartListening("SpecialLimit", CallSpecialLimit);


        GameAddPropAction();
        //LoadingFood();
    }

    private void OnDisable()
    {
        //SystemEventManager.StopListening("OperationMakingFood", CallMakingFood);
        //SystemEventManager.StopListening("OperationCookeFood", CallCoodeFood);
        //SystemEventManager.StopListening("OperationSubmitCompeletFood", CallSubmitCompeletFood);
        //SystemEventManager.StopListening("OperationAddOtherFiid", CallAddOtherFood);
    }

    //跳转关卡
    public IEnumerator ResetManager()
    {
        //currMainFooResdDictionary.Clear();
        //oetherFoodDictionary.Clear();
       
        rwaMainFoodList.Clear();

        foodMakeParent = systemGameManager.LevelManager.CurrRestaurantInfo.FoodMakeParent;
        PlateParent = systemGameManager.LevelManager.CurrRestaurantInfo.PlateParent;
        rawFoodParent = systemGameManager.LevelManager.CurrRestaurantInfo.RawFoodParent;
        rawOtherFoodParent = systemGameManager.LevelManager.CurrRestaurantInfo.otherFoodParent;
        PlateParent.GetComponent<CookedFoodOpreat>().ResetCookedFood();
        foodMakeParent.GetComponent<MakinFoodOpreat>().ResetMakinFood();
        LoadingFood();

        yield return null;
    }

    public void LoadingFood()
    {
        MainFoodConfigur assetsFood = SystemDataFactoy.instans.GetResourceConfigur("MainFoodAssets") as MainFoodConfigur;
   
        var currLeve = systemGameManager.LevelManager.GetCurrLevelData();
        currlevelData = currLeve;
        //foreach (MainFoodCompnentAssets data in assetsFood.foodDataList)
        //{
        //    for (int i=0; i<currLeve.mainFood.Length; i++)
        //    {
        //        if (data.mainFoodNmae == currLeve.mainFood[i])
        //        {
        //            if (!currMainFooResdDictionary.ContainsKey(currLeve.mainFood[i]))
        //            {
        //                currMainFooResdDictionary.Add(currLeve.mainFood[i], data);
        //            }
        //        }
        //    }
        //}
        var foodData = systemGameManager.LevelManager.GetCurrLevelData();
      
        if (foodData.mainFood[0] != "null")
        {
           // var foodAssetData = currMainFooResdDictionary[foodData.mainFood[0]];
            var mainFoodItem = LoadConfigursManager.instans.MainFoodItemDictionary[foodData.mainFood[0]];
            // 制作台设备
            foodMakeParent.GetComponent<MakinFoodOpreat>().FoodMakeDeviceNasme = foodData.foodMakeDeviceName;
            foodMakeParent.GetComponent<MakinFoodOpreat>().OperationManage = this;
            foodMakeParent.GetComponent<MakinFoodOpreat>().LoadingMakinMinFood();

            //提交台
            PlateParent.GetComponent<CookedFoodOpreat>().FoodSubnitDeviceName = foodData.foodSubMitDeviceName;
            PlateParent.GetComponent<CookedFoodOpreat>().OperationManage = this;
            PlateParent.GetComponent<CookedFoodOpreat>().LoadingCookedMinFood( mainFoodItem);

        }

        //主食
        for (int i=0; i< currLeve.mainFood.Length; i++)
        {
            if (currLeve.mainFood[i] == "null")
                continue;
            var restaurantInfo = systemGameManager.LevelManager.CurrRestaurantInfo;
            for (int j=0; j<restaurantInfo.RawMainFoodList.Count; j++)
            {
                var rawCompenent = restaurantInfo.RawMainFoodList[j].GetComponent<RawFoodOpreat>();
                if (rawCompenent != null && rawCompenent.FoodName == currLeve.mainFood[i])
                {
                    restaurantInfo.RawMainFoodList[j].gameObject.SetActive(true);
                    var rawFoodobj = restaurantInfo.RawMainFoodList[j].gameObject;
                    int rawGrade = 1;
                    if (PlayerData.foodDataPramerer.foodDataDict.ContainsKey(rawCompenent.FoodName))
                    {
                        rawGrade = int.Parse(PlayerData.foodDataPramerer.foodDataDict[rawCompenent.FoodName]);
                    }
                    //试用升级
                    if (PlayerData.instance.currSingleLevelData.foodTyeUpDit.ContainsKey(rawCompenent.FoodName))
                    {
                        rawGrade = int.Parse(PlayerData.instance.currSingleLevelData.foodTyeUpDit[rawCompenent.FoodName]);
                    }

                    rawFoodobj.GetComponent<RawFoodOpreat>().RawFoddGrade = rawGrade;
                    rawFoodobj.GetComponent<RawFoodOpreat>().SetFoodSkin();
                    rwaMainFoodList.Add(rawFoodobj);
                }
            }
        }

        //根据主食生成配菜
        var restaurentInfo = systemGameManager.LevelManager.CurrRestaurantInfo;
         for (int i=0; i< currLeve.otherFood.Length; i++)
        {
            if (currLeve.otherFood[i] != "null")
            {
              for (int j=0; j< restaurentInfo.otherFood.Length; j++)
              {
                    var nameTemp = restaurentInfo.otherFood[j].GetComponent<OtherFood>().foodNmae;
                    if (currLeve.otherFood[i] == nameTemp)
                    {
                        restaurentInfo.otherFood[j].gameObject.SetActive(true);
                        int otherFoodGrade = 1;
                        if (PlayerData.foodDataPramerer.foodDataDict.ContainsKey(nameTemp))
                        {
                            otherFoodGrade = int.Parse(PlayerData.foodDataPramerer.foodDataDict[nameTemp]);

                        }
                        //试用升级
                        if (PlayerData.instance.currSingleLevelData.foodTyeUpDit.ContainsKey(nameTemp))
                        {
                            otherFoodGrade = int.Parse(PlayerData.instance.currSingleLevelData.foodTyeUpDit[nameTemp]);
                        }

                        var skin = restaurentInfo.otherFood[j].GetComponent<CompomentSkin>().compnentSkin;
                        restaurentInfo.otherFood[j].GetComponent<Image>().sprite = skin[otherFoodGrade - 1];
                    }
                }

            }
        }
        ///配置饮料
        var drink = systemGameManager.LevelManager.CurrRestaurantInfo.DrinkOpreat;
        if (currLeve.drinkDevice != "null")
        {
            drink.gameObject.SetActive(true);
            drink.DrinkName = currLeve.drinke;
            drink.DrinkDeviceName = currLeve.drinkDevice;
            //var foodMendDicrionary = LoadConfigursManager.instans.FoodMendDicrionary;oriange oriange "orange

            var foodMendDicrionary = LoadConfigursManager.instans.ShopFoodMenuDict[PlayerData.instance.curPlayeShop];
            var drinkData = LoadConfigursManager.instans.MainFoodItemDictionary[drink.DrinkName];
            drink.DrinkID = foodMendDicrionary[drink.DrinkName].foodMenuID;
            drink.DrinkPrice = (int)drinkData.mainFoodPrice;
            drink.InitDrink();
        }
        else
        {
            drink.gameObject.SetActive(false);
        }

    }

    private void CallMakingFood(string eventName, EventParamProperties eventparam)
    {
        if (systemGameManager.GameStat == GameStat.GameRun)
        {
            var eventMsg = eventparam as EventAddFoodMakeing;
            if (eventMsg.foodType == FoodType.MainFood)
                    foodMakeParent.GetComponent<MakinFoodOpreat>().AddFoodMaking(eventparam.foodNmae);
            else if (eventMsg.foodType == FoodType.OtherFood)
                foodMakeParent.GetComponent<MakinFoodOpreat>().AddOtherFoodMaking(eventparam.foodNmae);
        }
    }
    /// <summary>
    /// 收到添加配菜事件
    /// </summary>
     private void CallAddOtherFood(string eventName, EventParamProperties eventparam)
    {
        EventAddOtherFood eventMsg = eventparam as EventAddOtherFood;
        var otherFoodNmae = eventMsg.foodNmae;
        var mainFoodName = eventMsg.mainFoodName;
        var otherFoodPrice = eventMsg.foodPrice;


        var isCompelet = PlateParent.GetComponent<CookedFoodOpreat>().AddOtherFood(mainFoodName, otherFoodNmae, otherFoodPrice);
        if (eventMsg.otherFoodComponent != null)
                eventMsg.otherFoodComponent.HandInFood(isCompelet);
        if (isCompelet && PlayerData.instance.currSingleLevelData.automaticSubmitProp)
        {
            //自动上菜
            StartCoroutine(PlateParent.GetComponent<CookedFoodOpreat>().AutomaticSabmitFood());
        }
    }


    private void AddOtherFoodTeaching()
    {

        if (PlayerData.instance.teachingValue.addOtherFoodClick == 0 && AddOtherFoodTeachingOnce)
        {
            AddOtherFoodTeachingOnce = false;
            var currLeve = systemGameManager.LevelManager.GetCurrLevelData();
            var OtherFoodLength = 0;

            for (int i = 0; i < currLeve.otherFood.Length; i++)
            {
                if (currLeve.otherFood[i] != "null")
                    OtherFoodLength++;
            }

            if (OtherFoodLength > 0)
                CoroutineHandler.StartStaticCoroutine(addOtherFoodTeachingCoroution());
        }    
    }
    private IEnumerator addOtherFoodTeachingCoroution()
    {
        yield return new WaitForSeconds(0.5f);
        systemGameManager.UiManager.TeachingOperation(TeachingType.AddOtherFood);
    }

    /// <summary>
    /// 添加主食事件
    /// </summary>

    private void CallCoodeFood(string eventName, EventParamProperties eventparam)
    {
        var data = eventparam as EventRawFoodAnd;
        if (data.makeType == MakeType.NeedMak)
        {
            var food = eventparam.DataObject.GetComponent<FoodProperty>();
            var isCompelet = PlateParent.GetComponent<CookedFoodOpreat>().AddFoodCooker(food.foodName);
            foodMakeParent.GetComponent<MakinFoodOpreat>().AddMainfoodReceive(isCompelet, data.index);
            if (isCompelet)
            {
               
                foodMakeParent.GetComponent<MakinFoodOpreat>().SetMainFoodListItem(food.mainFoodIndex, false);
                AddOtherFoodTeaching();
                //自动上菜
                if (PlayerData.instance.currSingleLevelData.automaticSubmitProp)
                    StartCoroutine(PlateParent.GetComponent<CookedFoodOpreat>().AutomaticSabmitFood());
            }
        }
        else if (data.makeType == MakeType.NoMake)
        {
            var isCompelet = PlateParent.GetComponent<CookedFoodOpreat>().AddFoodCooker(data.foodNmae);
        }
      
    }
    /// <summary>
    /// 配菜制作
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="eventparam"></param>


    //有新来顾客
    private void CallNewComeGuest(string eventName, EventParamProperties eventparam)
    {
        //有新玩家自动上菜
        if (PlayerData.instance.currSingleLevelData.automaticSubmitProp)
            StartCoroutine(PlateParent.GetComponent<CookedFoodOpreat>().AutomaticSabmitFood());
    }


    //收到特殊限制消息
    private void CallSpecialLimit(string eventName, EventParamProperties eventparam)
    {
        systemGameManager.UiManager.OpenFailTip(ConsumePropType.InvalidSpecialConditions);
    }
   
    /// <summary>
   /// 完成食物提交
   /// </summary>
   /// <param name="eventName"></param>
   /// <param name="eventparam"></param>
    private void CallSubmitCompeletFood(string eventName, EventParamProperties eventparam)
    {
        EventDeliverCookedFood deliverMsg = eventparam as EventDeliverCookedFood;
        PlateParent.GetComponent<CookedFoodOpreat>().SubmitFoodCompelet(deliverMsg.plateIndex, deliverMsg.needTransform);
        PlayerData.instance.getCion += (int)deliverMsg.totalPrice;
        PlayerData.instance.currSingleLevelData.cion += (int)deliverMsg.totalPrice;

        PlayerData.instance.currSingleLevelData.compeleFoodNumber += 1;
       string strName =  PlayerData.instance.dailyTaskDataValue.foodCount;
        PlayerData.instance.dailyTaksValueDit[strName]++;
        systemGameManager.UiManager.GuetNeedComelet();
        PlayerData.instance.currSingleLevelData.submitFoodCount++;
        systemGameManager.GuestManager.CompeletTarger();
        ComtinuSubmitFood();

    }
    /// <summary>
    //饮料提交完成
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="eventparam"></param>
    private void CallSubmitCompeletDrink(string eventName, EventParamProperties eventparam)
    {
        var drink = eventparam as EventAddDrink;
       // PlayerData.playerDataParame.cions += (int)drink.drinkPrice;
        PlayerData.instance.getCion += (int)drink.drinkPrice;
        PlayerData.instance.currSingleLevelData.cion += (int)drink.drinkPrice;
        PlayerData.instance.currSingleLevelData.compeleFoodNumber += 1;
        var strName = PlayerData.instance.dailyTaskDataValue.foodCount;
        PlayerData.instance.dailyTaksValueDit[strName]++;
        ComtinuSubmitFood();
        PlayerData.instance.currSingleLevelData.submitFoodCount++;
        systemGameManager.GuestManager.CompeletTarger();

    }

    //连续提交食物
    private void ComtinuSubmitFood()
    {
        //连击
        if (continuCount == 0)
        {
            continuCount++;
        }
        else if (continuTime <= maxcontinuTime && continuCount != 0)
        {
            continuTime = 0;
            continuCount++;
            if (continuCount > 5)
                continuCount = 5;
            if (continuCount == 3)
            {
                var strName = PlayerData.instance.dailyTaskDataValue.continuFoodCout;
                PlayerData.instance.dailyTaksValueDit[strName]++;
                PlayerData.instance.getCion += 5;
                PlayerData.instance.currSingleLevelData.cion += 5;
                PlayerData.instance.currSingleLevelData.contiunThreeSbmitCount++;
            }
            else if (continuCount == 4)
            {
                string steName =  PlayerData.instance.dailyTaskDataValue.continuFoodCout;
                PlayerData.instance.dailyTaksValueDit[steName]++;
                steName =  PlayerData.instance.dailyTaskDataValue.conpelet4ContinuCount;
                PlayerData.instance.dailyTaksValueDit[steName]++;
                PlayerData.instance.getCion += 10;
                PlayerData.instance.currSingleLevelData.cion += 10;
                PlayerData.instance.currSingleLevelData.contiunFourSbmitCount++;
            }
            else if (continuCount == 5)
            {
               string strName =  PlayerData.instance.dailyTaskDataValue.continuFoodCout;
                PlayerData.instance.dailyTaksValueDit[strName]++;
                strName =  PlayerData.instance.dailyTaskDataValue.conpelet4ContinuCount;
                PlayerData.instance.dailyTaksValueDit[strName]++;
                strName =  PlayerData.instance.dailyTaskDataValue.conpelet5ContinuCount;
                PlayerData.instance.dailyTaksValueDit[strName]++;
                // PlayerData.playerDataParame.cions += 20;
                PlayerData.instance.getCion += 20;
                PlayerData.instance.currSingleLevelData.cion += 20;
                PlayerData.instance.currSingleLevelData.contiunFiveSbmitCount++;
            }
        }
        systemGameManager.UiManager.GameHomePlael.setContinuNumerImg(continuCount);
    }
    private void Update()
    {
        if (continuCount != 0)
        {
            continuTime += Time.deltaTime;
            var valieTemp = continuTime / ( maxcontinuTime + propAddContinuTime);
            if (valieTemp >1f)
            {
                valieTemp = 0;
                continuTime = 0;
                continuCount = 0;
                systemGameManager.UiManager.GameHomePlael.setContinuNumerImg(0);
            }

            systemGameManager.UiManager.GameHomePlael.SetCoutinuValue(valieTemp);
        }
    }
    public void GameAddPropAction()
    {
        for (int i = 0; i < PlayerData.instance.currSelectPropList.Count; i++)
        {
            var prop = PlayerData.instance.currSelectPropList[i];
            if (prop == ConsumePropType.AddcontinuTimeIntterval)
            {
                propAddContinuTime = 5f;
            }
        }
    }

    public void FoodOnFireClear(int index)
    {
        foodMakeParent.GetComponent<MakinFoodOpreat>().AddMainfoodReceive(true, index);

    }

}
