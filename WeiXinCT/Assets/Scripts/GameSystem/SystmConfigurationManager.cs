using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using YooAsset;

public class SystmConfigurationManager : MonoBehaviour
{
    public void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }    
    }

    public static SystmConfigurationManager instance;
    [Header("资源文件名称")]
    public string AssetsFileName;

    public string shopConfigurName;

    public string mapConfigerName;

    [Header("关卡配置文件")]
    public string levelConfigurName;


    [Header("配菜配置文件")]
    public string otherFoodConfigurName;

    [Header("大厅资源名称")]
    public string RestaurantAssetConfigerName;

    [Header("生产顾客间隔时间")]
    public float guestTiem = 2f;

    [Header("游戏做大关卡数")]
    public int maxLevel;

    [Header("AB报名")]
    public string[] abName;

    [Header("资源索引地址")]
    public string[] yooAseets;
    [Header("连几时间")]
    public float contiunTime;
    [Header("加载模式")]
    public EPlayMode PlayMode = EPlayMode.EditorSimulateMode;

}
