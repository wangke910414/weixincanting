using LitJson;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;
using Unity.VisualScripting;

public class LoadConfigursManager 
{
    //SystemGameManager systemGameManager;
    SystmConfigurationManager systmConfigurationManager;

    public static LoadConfigursManager instans;
    private bool isLoadCompelet = false;
    /// <summary>
    /// 关卡配置表
    /// </summary>
    private List<LevelData> levelDataList = new List<LevelData>();
    //关卡文件与商店对应
    private Dictionary<string, List<LevelData>> levelShopDataDuct = new Dictionary<string, List<LevelData>>();
    /// <summary>
    /// 配菜数据
    /// </summary>
    private Dictionary<string, OtherFoodItem> oetherFoodDictionary = new Dictionary<string, OtherFoodItem>();

    /// <summary>
    /// 顾客容器
    /// </summary>
    private Dictionary<string, GuestTable> levelGuestDataDrictionary = new Dictionary<string, GuestTable>();

    private Dictionary<string, Dictionary<string, GuestTable>> shopGuestDict = new Dictionary<string, Dictionary<string, GuestTable>>();
    // <summary>
    /// 所有菜单组合
    /// </summary>
    //private Dictionary<string, FoodMenu> foodMendDicrionary = new Dictionary<string, FoodMenu>();



    private Dictionary<string, Dictionary<string, FoodMenu>> shopFoodMenuDict  =  new Dictionary<string, Dictionary<string, FoodMenu>>();

    /// <summary>
    /// 主食搭配数据
    /// </summary>
    private Dictionary<string, FoodItem> foodItemDictionary = new Dictionary<string, FoodItem>();

    /// <summary>
    /// 设备数据
    /// </summary>
    private Dictionary<string, DeviceData> deviceDict = new Dictionary<string, DeviceData>();
    /// <summary>
    /// 主食数据
    /// </summary>
    private Dictionary<string, MainFoodItem> mainFoodItemDictionary = new Dictionary<string, MainFoodItem>();

    ///ivate List<GameTaskData> taskList = new List<GameTaskData>();
    //商店数据
    private Dictionary<string, shopData> shopDict = new Dictionary<string, shopData>();
    //地图数据
    private List<MapData> mapList = new List<MapData>();

    public List<LevelData> LevelDataList { get => levelDataList; }
   public Dictionary<string, OtherFoodItem> OetherFoodDictionary { get => oetherFoodDictionary; }

    //public Dictionary<string, GuestTable> LevelGuestDataDrictionary { get => levelGuestDataDrictionary; }
    //public Dictionary<string, FoodMenu> FoodMendDicrionary { get => foodMendDicrionary; }
    public Dictionary<string, FoodItem> FoodItemDictionary { get => foodItemDictionary; }
    public Dictionary<string, MainFoodItem> MainFoodItemDictionary { get => mainFoodItemDictionary; }

    public Dictionary<string, List<LevelData>> LevelShopDataDuct { get => levelShopDataDuct; }
    public Dictionary<string, Dictionary<string, GuestTable>> ShopGuestDit { get => shopGuestDict; }
    public Dictionary<string, Dictionary<string, FoodMenu>> ShopFoodMenuDict { get => shopFoodMenuDict; }
    public List<MapData> MapList { get => mapList; }
    public Dictionary<string, DeviceData> DeviceDict { get => deviceDict; }
    public Dictionary<string, shopData> ShopDict { get => shopDict; }

    public bool IsLoadCompelet { get => isLoadCompelet; }



    public static bool CreateLoadData()
    {
        if (instans == null)
        {
            LoadConfigursManager.instans = new LoadConfigursManager();
            return true;
            
        }
        return false;
    }

    public IEnumerator Configuer(SystmConfigurationManager systmConfigurationManager)
    {
        this.systmConfigurationManager = systmConfigurationManager;

        yield return LoadGameData();
        isLoadCompelet = true;
    }

    public IEnumerator LoadGameData()
    {
        yield return LoadMap();
        yield return LoadShop();
        yield return LoadLlvelGuset();
        yield return LoadLevel();
        yield return LoadFoodMenu();
        yield return LoadFoodItem();
        yield return LoadDevice();
        yield return LoadMainFoodItem();
        yield return LoadOtherFoodItem();
       
    }

    private IEnumerator LoadMap()
    {
        var fileNmae = systmConfigurationManager.mapConfigerName + ".json";
        string path = Application.streamingAssetsPath + "/JsonConfigurs/" + fileNmae;

        UnityWebRequest requst = UnityWebRequest.Get(path);
        yield return requst.SendWebRequest();
        JsonData jsonData = JsonMapper.ToObject(requst.downloadHandler.text);
        foreach (JsonData data in jsonData)
        {
            if (data != null)
            {
                MapData mapItem = new MapData();
                mapItem.mapId = data["mapId"].ToSafeString();
                /// mapItem.mapImg = data["mapImg"].ToSafeString();MapCongigur
                mapItem.mapName = data["mapName"].ToSafeString();
              //  mapItem.shoData = data["shoData"].ToSafeString();
                MapList.Add(mapItem);
            }
        }

    }
    private IEnumerator LoadShop()
    {
        var fileNmae = systmConfigurationManager.shopConfigurName + ".json";
        string path = Application.streamingAssetsPath + "/JsonConfigurs/" + fileNmae;

        UnityWebRequest requst = UnityWebRequest.Get(path);
        yield return requst.SendWebRequest();
        JsonData jsonData = JsonMapper.ToObject(requst.downloadHandler.text);
        //shopDict = JsonMapper.ToObject<Dictionary<string, shopData>>(jsonData.ToJson()); 
        foreach (JsonData data in jsonData)
        {

            if (data != null)
            {
                shopData shopItem = new shopData();
                shopItem.shopID = data["shopID"].ToSafeString();
                shopItem.shopName = data["shopName"].ToSafeString();
                shopItem.hallResources = data["shopName"].ToSafeString();
                shopItem.leveData = data["LevelData"].ToSafeString();
                shopItem.mapName = data["MapName"].ToSafeString();
                shopItem.startCount = int.Parse(data["startCount"].ToSafeString());
                shopItem.endCount = int.Parse(data["endCount"].ToSafeString());
                shopItem.foodMenuName = data["foodMenuName"].ToSafeString();
                shopItem.foodMenuName = data["foodMenuName"].ToSafeString();
                shopItem.levelListCount = int.Parse(data["levelListCount"].ToSafeString());
                shopItem.shopDescribleName = data["shopDescribleName"].ToSafeString();
                shopItem.unLockKeyCount = int.Parse(data["unLockKeyCount"].ToSafeString());
                shopItem.isDefaultLock = int.Parse(data["isDefaultLock"].ToSafeString());
                shopDict.Add(shopItem.shopName, shopItem);
            }
        }
    }

    /// <summary>
    /// 读取关卡数据
    /// </summary>
    /// <returns></returns>
    private IEnumerator LoadLevel()
    {
        
        foreach(var shop in shopDict)
        {
            for (int i = 1; i <= 3; i++)
            {
                var levelName = shop.Value.leveData + "_" + i;
                var fileNmae = levelName + ".json";
               // Debug.Log(fileNmae);
                string path = Application.streamingAssetsPath + "/JsonConfigurs/levelData/" + fileNmae;
                List<LevelData> levelList = new List<LevelData>();

                UnityWebRequest requst = UnityWebRequest.Get(path);
                yield return requst.SendWebRequest();
               
                JsonData jsonData = JsonMapper.ToObject(requst.downloadHandler.text);
                foreach (JsonData data in jsonData)
                {
                 
                    if (data != null)
                    {
                        LevelData level = new LevelData();
                        level.levelID = data["levelID"].ToString();
                        level.levelGrade = data["levelGrade"].ToString();
                        level.shopName = levelName;
                        level.mainFood[0] = data["mainFood1"].ToString();
                        level.mainFood[1] = data["mainFood2"].ToString();
                        level.mainFood[2] = data["mainFood3"].ToString();
                        level.otherFood[0] = data["otherFood1"].ToString();
                        level.otherFood[1] = data["otherFood2"].ToString();
                        level.otherFood[2] = data["otherFood3"].ToString();
                        level.otherFood[3] = data["otherFood4"].ToString();
                        level.otherFood[4] = data["otherFood5"].ToString();
                        level.otherFood[5] = data["otherFood6"].ToString();
                        level.otherFood[6] = data["otherFood7"].ToString();

                        level.device[0] = data["device1"].ToString();
                        level.device[1] = data["device2"].ToString();
                        level.device[2] = data["device3"].ToString();
                        level.device[3] = data["device4"].ToString();

                        level.foodMakeDeviceName[0] = data["foodMakeDeviceName1"].ToString();
                        level.foodMakeDeviceName[1] = data["foodMakeDeviceName2"].ToString();

                        level.foodSubMitDeviceName[0] = data["foodSubMitDeviceName1"].ToString();
                        level.foodSubMitDeviceName[1] = data["foodSubMitDeviceName2"].ToString();
                        level.drinkeDeviceName = data["drinkeDeviceName"].ToString();

                        level.drinke = data["drinke"].ToString();
                        level.drinkDevice = data["drinkeDevice"].ToString();
                        level.levelResources = data["levelResources"].ToString();
                        level.levelGuestTablle = data["levelGuest"].ToSafeString();

                        level.unLoackKeyCunnt = int.Parse(data["unLoackKeyCunnt"].ToSafeString());
                        level.chairCount = int.Parse(data["chairCount"].ToSafeString());
                        level.cionTarget = int.Parse(data["levelCionTraget"].ToSafeString());
                        level.compeletFoodNumber = int.Parse(data["compeletFoodNumber"].ToSafeString());
                        level.goodTarget = int.Parse(data["goodTarget"].ToSafeString());
                        level.continuTarget = int.Parse(data["goodTarget"].ToSafeString());

                        level.gustCount = int.Parse(data["gustCount"].ToSafeString());
                        level.levelCompeletTime = int.Parse(data["levelCompeletTime"].ToSafeString());

                        level.prizeCionNumber = int.Parse(data["prizeCionNumber"].ToSafeString());
                        level.prizeKeyNumber = int.Parse(data["prizeKeyNumber"].ToSafeString());
                        level.specialLimitType[0] = int.Parse(data["specialLimitType1"].ToSafeString());
                        level.specialLimitType[1] = int.Parse(data["specialLimitType2"].ToSafeString());
                        level.specialLimitType[2] = int.Parse(data["specialLimitType3"].ToSafeString());
                       
                        level.recommendUpType = int.Parse(data["recommendUpType"].ToSafeString());
                        level.recomenUpdName = data["recomenUpdName"].ToSafeString();
                        level.toUpGrade = int.Parse(data["toUpGrade"].ToSafeString());
                        level.recomendPropType = (ConsumePropType)int.Parse(data["recomendPropType"].ToSafeString());
                      //  Debug.Log("读取提示建议升级    " + level.recommendUpType);
                        levelList.Add(level);
                    }
                }
                levelShopDataDuct.Add(levelName, levelList);
            }
           
        }
    }


    /// <summary>
    /// 加载顾客
    /// </summary>
    /// <returns></returns>
    private IEnumerator LoadLlvelGuset()
    {
        

        var path = Application.streamingAssetsPath + "/JsonConfigurs/lvelGuest/";
       // Debug.Log("shopDict.Count " + shopDict.Count);
       foreach (var item in shopDict)
        {
            for (int i=1; i<= 3; i++)
            {
                string fileName = item.Value.shopName + "_Guest_"+i;
                string url = path + fileName + ".json";

                UnityWebRequest requst = UnityWebRequest.Get(url);
                yield return requst.SendWebRequest();
                JsonData jsonData = JsonMapper.ToObject(requst.downloadHandler.text);
                var dict = JsonMapper.ToObject<Dictionary<string, GuestTable>>(jsonData.ToJson());
                List<string> keys = new List<string>(jsonData.Keys);

                if (!shopGuestDict.ContainsKey(fileName))
                {
                    shopGuestDict.Add(fileName, dict);
                   // Debug.Log("插入文件 " + shopGuestDict.Count);
                }
            }
           
        }
    }
    private IEnumerator LoadLlvelGuset1()
    {
        // var fileNmae = systemGameManager.SystmConfigurationManager.otherFoodConfigurName + ".json";
        var maxLevel = systmConfigurationManager.maxLevel;


        for (int i = 1; i <= maxLevel; i++)
        {
            var path = Application.streamingAssetsPath + "/JsonConfigurs/lvelGuest/";
            string fileName = "leveGuest_" + i;
            path = path + fileName + ".json";
            // Debug.Log(path);
            UnityWebRequest requst = UnityWebRequest.Get(path);
            yield return requst.SendWebRequest();
            JsonData jsonData = JsonMapper.ToObject(requst.downloadHandler.text);
            Debug.Log("插入文件 " + requst.downloadHandler.text);
            GuestTable guestTabel = new GuestTable();

            if (jsonData != null)
            {
                foreach (JsonData data in jsonData)
                {
                    LevelGuestData leveGuest = new LevelGuestData();
                    leveGuest.guestID = data["guestID"].ToSafeString();
                    leveGuest.guestOrder = data["guestOrder"].ToSafeString();
                    leveGuest.fromTime = float.Parse(data["fromTime"].ToSafeString());
                    leveGuest.needFood[0] = data["needFood1"].ToSafeString();
                    leveGuest.needFood[1] = data["needFood2"].ToSafeString();
                    leveGuest.needFood[2] = data["needFood3"].ToSafeString();
                    guestTabel.guestDataList.Add(leveGuest);
                }
                if (!levelGuestDataDrictionary.ContainsKey(fileName))
                {
                    levelGuestDataDrictionary.Add(fileName, guestTabel);
                }
            }

        }

    }


    private IEnumerator LoadDevice()
    {
        var fileNmae = "/JsonConfigurs/deviceData" + ".json";
        var path = Application.streamingAssetsPath + fileNmae;

        UnityWebRequest quest = UnityWebRequest.Get(path);
        yield return quest.SendWebRequest();
        JsonData jsonData = JsonMapper.ToObject(quest.downloadHandler.text);

        foreach (JsonData data in jsonData)
        {
            DeviceData device = new DeviceData();
            device.deviceID = data["deviceID"].ToString();
            device.deviceName = data["deviceName"].ToString();
            device.unLoackLevel = int.Parse(data["unLoackLevel"].ToString());
            device.upPrice = int.Parse( data["upPrice"].ToString());
            device.shopName = data["shopName"].ToString();
            deviceDict.Add(device.deviceName, device);
        }
    }

    private IEnumerator LoadFoodMenu()
    {

        //shop2_FoodMenu
        foreach(var shop in shopDict)
        {
            var fileNmae = "/JsonConfigurs/foodMenu/"+ shop.Value.foodMenuName + ".json";
            var path = Application.streamingAssetsPath + fileNmae;
            // Debug.Log(path);
            UnityWebRequest quest = UnityWebRequest.Get(path);
            yield return quest.SendWebRequest();

            JsonData jsonData = JsonMapper.ToObject(quest.downloadHandler.text);
          
            Dictionary<string, FoodMenu> foodMendDicrionary = new Dictionary<string, FoodMenu>();
            foreach (JsonData data in jsonData)
            {                FoodMenu foodMenu = new FoodMenu();
                foodMenu.foodMenuID = data["foodMenuID"].ToSafeString();
                foodMenu.foodName = data["foodName"].ToSafeString();
                foodMenu.needMainFood = data["needMainFood"].ToSafeString();
                foodMenu.foodType = data["foodType"].ToSafeString();
                foodMenu.needOtherFood[0] = data["needOtherFood1"].ToSafeString();
                foodMenu.needOtherFood[1] = data["needOtherFood2"].ToSafeString();
                foodMenu.needOtherFood[2] = data["needOtherFood3"].ToSafeString();
                foodMenu.needOtherFood[3] = data["needOtherFood4"].ToSafeString();
                foodMenu.classNum = data["classNum"].ToString();
                if (!foodMendDicrionary.ContainsKey(foodMenu.foodName))
                {
                    foodMendDicrionary.Add(foodMenu.foodName, foodMenu);
                }
            }
            shopFoodMenuDict.Add(shop.Value.shopName, foodMendDicrionary);
        }
    }

    private  IEnumerator LoadFoodItem()
    {
        var fileNmae = "/JsonConfigurs/foodItem" + ".json";
        var path = Application.streamingAssetsPath + fileNmae;
        UnityWebRequest quest = UnityWebRequest.Get(path);
        yield return quest.SendWebRequest();
        JsonData jsonData = JsonMapper.ToObject(quest.downloadHandler.text);
        foreach(JsonData data in jsonData)
        {
            if (data != null)
            {
                FoodItem food = new FoodItem();
                food.mainFoodName = data["mainName"].ToString();
                food.otherFoodName[0] = data["otherFoodName1"].ToString();
                food.otherFoodName[1] = data["otherFoodName2"].ToString();
                food.otherFoodName[2] = data["otherFoodName3"].ToString();
                food.otherFoodName[3] = data["otherFoodName4"].ToString();

                if (!foodItemDictionary.ContainsKey(food.mainFoodName))
                {
                    foodItemDictionary.Add(food.mainFoodName, food);
                }
              
            }
        }

    }

    /// <summary>
    /// 加载主菜单件
    /// </summary>
    /// <returns></returns>
    private IEnumerator LoadMainFoodItem()
    {
        var fileNmae = "/JsonConfigurs/mainFoodItem" + ".json";
        var path = Application.streamingAssetsPath + fileNmae;
        UnityWebRequest quest = UnityWebRequest.Get(path);
        yield return quest.SendWebRequest();
        JsonData jsonData = JsonMapper.ToObject(quest.downloadHandler.text);
       foreach(JsonData data in jsonData)
        {
            if (data != null)
            {
                MainFoodItem fooditem = new MainFoodItem();
                fooditem.mianFoodID = data["mianFoodID"].ToSafeString();
                fooditem.mainFoodName = data["mainFoodName"].ToSafeString();
                fooditem.mainFoodPrice =float.Parse( data["mainFoodPrice"].ToSafeString());
                fooditem.upFoodPrice = int.Parse(data["upFoodPrice"].ToSafeString());
                fooditem.unLoackLevel = int.Parse(data["unLoackLevel"].ToSafeString());
                fooditem.foodTypeName = data["foodTypeName"].ToSafeString();
                fooditem.isMake = int.Parse(data["isMake"].ToSafeString());
                fooditem.makeDeviceName = data["makeDeviceName"].ToSafeString();
                fooditem.onParent = data["onParent"].ToString();
                fooditem.shopName = data["shopName"].ToString();

                if (!mainFoodItemDictionary.ContainsKey(fooditem.mainFoodName))
                {
                    mainFoodItemDictionary.Add(fooditem.mainFoodName, fooditem);
                }
 
            }
        }
    }

    /// <summary>
    /// 加载配菜单件
    /// </summary>
    /// <returns></returns>

    private IEnumerator LoadOtherFoodItem()
    {
        var fileNmae = "/JsonConfigurs/otherFoodItem" + ".json";
        var path = Application.streamingAssetsPath + fileNmae;
        UnityWebRequest quest = UnityWebRequest.Get(path);
        yield return quest.SendWebRequest();
        JsonData jsonData = JsonMapper.ToObject(quest.downloadHandler.text);
        foreach (JsonData data in jsonData)
        {
            if (data != null)
            {
                OtherFoodItem fooditem = new OtherFoodItem();
                fooditem.OtherFoodID = data["otherFoodID"].ToSafeString();
                fooditem.OtherFoodName = data["otherFoodNmae"].ToSafeString();
                fooditem.mainFood = data["mainFood"].ToSafeString();
                fooditem.otherFoodPrice = float.Parse(data["otherFoodPrice"].ToSafeString());
                fooditem.upFoodPrice = int.Parse(data["upFoodPrice"].ToSafeString());
                fooditem.unLoackLevel = int.Parse(data["unLoackLevel"].ToSafeString());
                fooditem.shopName = data["shopName"].ToSafeString();
               //fooditem.isMake =  int.Parse(data["isMake"].ToSafeString());
        
                if (!oetherFoodDictionary.ContainsKey(fooditem.OtherFoodName))
                {
                  oetherFoodDictionary.Add(fooditem.OtherFoodName, fooditem);
                }

            }
        }
    }

    
    /// <summary>
    /// 通过UnityWebRequest获取本地StreamingAssets文件夹中的文件
    /// </summary>
    /// <param name="fileName">文件名称</param>
    /// <returns></returns>
    public string UnityWebRequestFile(string fileName)
    {
//        string url;
//        #region 分平台判断 StreamingAssets 路径
//        //如果在编译器或者单机中
//#if UNITY_EDITOR || UNITY_STANDALONE

//        url = "file://" + Application.dataPath + "/StreamingAssets/" + fileName;
//        //否则如果在Iphone下
//#elif UNITY_IPHONE
//        url = "file://" + Application.dataPath + "/Raw/"+ fileName;
//            //否则如果在android下
//#elif UNITY_ANDROID
//        url = "jar:file://" + Application.dataPath + "!/assets/"+ fileName;
//#endif
       // #endregion
        UnityWebRequest request = UnityWebRequest.Get(fileName);
        request.SendWebRequest();//读取数据
       //while (true)
        {
            Debug.Log("读取关卡资源完成！");
            //if (request.downloadHandler.isDone)//是否读取完数据
            {
                return request.downloadHandler.text;
            }

        }
    }
    private bool IsLoaderComepelt()
    {
        return false;
    }
}

