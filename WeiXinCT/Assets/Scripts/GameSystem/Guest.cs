using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.Audio;

public enum GuestState : int
{
    iDle,
    Come,
    WaitFood,
    FailLeave,
    Leave
}

public enum GuestMoodState : int //顾客心情状态
{
    Null,               
    GoodComment,            //好的
    MediumComment,      //一般   
    BadComent,          //差



}

public class Guest : MonoBehaviour
{
    private float maxWaitTime  = 30;

   // private float consumePropAddTime = 0;
    private float waitTimeIndex = 0.0f;

    private float tollWaitTime = 0f;

    private  Tween  comeTween;
    private Tween leaveTween;

    public float fromTime;

    private int foodIndex = 0;


    [SerializeField]
    private List<FoodMenuAsset> neddFootAssetList = new List<FoodMenuAsset>();

    private bool[] isCompeletFood = new bool[3];


    ///[SerializeField]
   // private GameObject needTipParent;
///
    ///[SerializeField]
    //private Transform[] needCompeletSign;

    [SerializeField]
    private Transform[] needFoodItem;

    ///[SerializeField]
    //private Image waitSechduleTimeImage;
    [SerializeField]
    private GuestNeedDlg guestNeedDlg;

    public Transform leavePrant;
    private GuestMoodState guestMoodState = GuestMoodState.Null;


    //private List<FoodProperty> needFoodList = new List<FoodProperty>();

    public LevelGuestData currGuestData  = new LevelGuestData();

    private List<FoodMenu> fooodMenuList = new List<FoodMenu>();

    private List<float> needFoodIDList =  new List<float>();

    private GuestState guestState = GuestState.iDle;

    public GuestState GuestState { get => guestState; }
    public GuestMoodState GuestMoodState { get => guestMoodState; }
    private Transform originTransform;

    private GuestManager guestManager;

    private int chairID = -1;  //等待位置编号

    private int needFoddCount = 0; //顾客需求个数；

    [SerializeField]
    private AudioSource goodComendAudio;
    [SerializeField]
    private AudioSource poorComendAudio;

    public int NeedFoddCount { get => needFoddCount; set => needFoddCount = value; }
    public GuestNeedDlg GuestNeedDlg { get => guestNeedDlg; }
    public int ChairID { get => chairID; }
    private void Start()
    {
        ///GuestRest();
    }


    public void GuestRest()
    {
        for (int i=0; i< isCompeletFood.Length; i++)
        {
            isCompeletFood[i] = false;
        }
        waitTimeIndex = maxWaitTime;

        guestState = GuestState.iDle;
    }
   
    public void SetBalckTrager(Transform trager)
    {
        if (trager != null)
        {
            originTransform = trager;
        }
          
    }

    public void Configur(GuestManager guestManager)
    {
        this.guestManager = guestManager;
    }

    public void GuestInfo(LevelGuestData data)
    {
        currGuestData = data;
    }

    private void CreateNeedList(FoodMenu food)
    {
        guestNeedDlg.AddNedFood(food);
        foodIndex++;
    }
    public void SetFoodMenu(FoodMenu food)
    {
        if (food != null)
        {
            fooodMenuList.Add(food);
            CreateNeedList(food);
        }
       var mainFoodDct = LoadConfigursManager.instans.MainFoodItemDictionary;
        var otherFoodDct = LoadConfigursManager.instans.OetherFoodDictionary;

        float mainFondID =float.Parse( mainFoodDct[food.needMainFood].mianFoodID);
        for (int i=0; i<food.needOtherFood.Length; i++)
        {
            if (food.needOtherFood[i] != "null")
            {
                var id = float.Parse( otherFoodDct[food.needOtherFood[i]].OtherFoodID);
                mainFondID += id;
            }
        }

        needFoodIDList.Add(mainFondID);
    }

    //比对
    public bool ComparisonNeedFoodNum(float foodMenuIDNum, out int outIndex)
    {
        if (guestState == GuestState.WaitFood)
        {
            for (int i = 0; i < needFoodIDList.Count; i++)
            {
                if (needFoodIDList[i] == foodMenuIDNum)
                {
                    if (guestNeedDlg.needFoodParent[i].gameObject.activeSelf != false && !isCompeletFood[i])
                    {

                        //guestNeedDlg.isCompelet[i].gameObject.SetActive(true);
                        //guestNeedDlg.isCompelet[i].transform.localScale = new Vector3(0, 1, 1);

                        //Tween tween = guestNeedDlg.isCompelet[i].transform.DOScaleX(1, 0.5f).SetEase(Ease.InSine);
                        //tween.onComplete = () => {
                        //    //guestNeedDlg.needFoodParent[i].gameObject.SetActive(false);
                        //};
                        isCompeletFood[i] = true;
                        outIndex = i;
                        return true;
                    }

                }
            }
        }
        outIndex = -1;
        return false;
    }


    public void StartGuest(Transform  target, int chairIndex)
    {
        if (guestState == GuestState.iDle)
        {
            guestState = GuestState.Come;
            StartCoroutine(GuestCone(target));
            chairID = chairIndex;
        }
    }

    private IEnumerator GuestCone(Transform target)
    {
        yield return new WaitForSeconds(fromTime);
        this.transform.SetParent(target.transform);
        comeTween = this.transform.DOLocalMoveX(0f, 2f).SetEase(Ease.InSine);

        comeTween.onComplete = GuestWaitStart;
    }

    public void GuestWaitStart()
    {
        var scale = this.transform.localScale;
        if (scale.x < 0)
        {
            scale.x = -scale.x;
            this.transform.localScale = scale;
        }
        guestNeedDlg.gameObject.SetActive(true);
        guestNeedDlg.gameObject.transform.localScale = Vector3.zero;
        Tween teween =  guestNeedDlg.transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f).SetEase(Ease.InSine);

        teween.onComplete = () =>
        {
            guestState = GuestState.WaitFood;
            guestManager.NewComeGuestNeedFood();
        };
    }

    //增加顾客等待时间
    public void AddWaitTime(float time)
    {
        waitTimeIndex = maxWaitTime;
    }

    private void Update()
    {
        if (guestState == GuestState.WaitFood)
        {
            waitTimeIndex -= Time.deltaTime;
            if (waitTimeIndex > 0)
            {
                guestNeedDlg.waitTiemImg.fillAmount = waitTimeIndex / maxWaitTime;
            }
            else if (waitTimeIndex < 0)
            {
             
                GuestWalk();
            }
            if (guestNeedDlg.waitTiemImg.fillAmount < 0.5f)
            {
                guestNeedDlg.waitTiemImg.color = Color.red;
                guestNeedDlg.SetDlgSideRed();
            }
            else
            {
                guestNeedDlg.waitTiemImg.color = Color.white;
            }
        }
    }


    private void GuestComment()
    {
        if (waitTimeIndex > (maxWaitTime - 15))
        {
            guestMoodState = GuestMoodState.GoodComment;
            goodComendAudio.Play();
        }
        else if (waitTimeIndex <= (maxWaitTime - 15) && waitTimeIndex > (maxWaitTime - 30))
        {
            guestMoodState = GuestMoodState.MediumComment;
            poorComendAudio.Play();
           
        }
        else
        {
            guestMoodState = GuestMoodState.BadComent;
           
        }
    }


    public  void SubmitSuccess(int index)
    {
        //打勾动画
        if (index != -1)
        {
            guestNeedDlg.isCompelet[index].gameObject.SetActive(true);
            guestNeedDlg.isCompelet[index].transform.localScale = new Vector3(0, 1, 1);
            Tween tween = guestNeedDlg.isCompelet[index].transform.DOScale(1.2f, 0.3f).SetEase(Ease.Flash).SetLoops(2,LoopType.Yoyo);
            tween.onComplete = () => {

            };
            guestNeedDlg.needFoodParent[index].transform.DOScale(1.2f, 0.3f).SetEase(Ease.Flash).SetLoops(2, LoopType.Yoyo).onComplete = () => {

                guestNeedDlg.needFoodParent[index].gameObject.SetActive(false);
                CompeletNeedFoodCheck(index);
            };
        }
    }

    //检查完成需要离开
    public void CompeletNeedFoodCheck(int index)
    {
       var tempNumber = 0;
        for (int i = 0; i < foodIndex; i++)
        {
            if (isCompeletFood[i])
            {
                tempNumber++;
            }
        }
        if (tempNumber == foodIndex && guestNeedDlg.CompleteNeed())
        {
            GuestWalk();
           
        }
        else
        {
            waitTimeIndex += tollWaitTime;
        }
    }



    //玩家离开
    private void GuestWalk()
    {
        if (guestState == GuestState.Leave)
            return;

        guestState = GuestState.FailLeave;
        this.transform.SetParent(originTransform);

        if (originTransform.localPosition.x < 0)
        {
            if (this.transform.forward == originTransform.forward)
                originTransform.SetSiblingIndex(0);
            {
                var sx = this.transform.localScale.x;
                this.transform.localScale = new Vector3(-sx, sx, sx);
            }
        }
        guestNeedDlg.gameObject.transform.DOScale(new Vector3(0f, 0f, 0f), 0.2f).SetEase(Ease.InSine);
        leaveTween = this.transform.DOLocalMoveX(0, 2f).SetEase(Ease.InSine);
        leaveTween.onComplete = ()=> {
            transform.gameObject.SetActive(false);
        };
        GuestComment();

        guestManager.OneceMoreStateGuest(this);
        guestState = GuestState.Leave;
    }

    /// <summary>
    /// 游戏结束顾客离开
    /// </summary>
    private IEnumerator GameEndGuestLeave()
    {
        //    if (guestState == GuestState.Leave)
        // return;
        yield return new WaitForSeconds(1f);
        guestState = GuestState.FailLeave;
        this.transform.SetParent(originTransform);

        if (originTransform.localPosition.x < 0)
        {
            if (this.transform.forward == originTransform.forward)
            {
                var sx = this.transform.localScale.x;
                this.transform.localScale = new Vector3(-sx, sx, sx);
            }
        }
        guestNeedDlg.gameObject.transform.DOScale(new Vector3(0f, 0f, 0f), 0.2f).SetEase(Ease.InSine);
        leaveTween = this.transform.DOLocalMoveX(0, 2f).SetEase(Ease.InSine);
        leaveTween.onComplete = () => {
            transform.gameObject.SetActive(false);
        };
    }

    public void GustLeave()
    {
        if (GuestState == GuestState.Come)
        {
            if (comeTween!= null && comeTween.IsPlaying())
                comeTween.Kill();
        }
        guestState = GuestState.Leave;
        CoroutineHandler.StartStaticCoroutine( GameEndGuestLeave());
    }
}
