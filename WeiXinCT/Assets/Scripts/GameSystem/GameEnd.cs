using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System;
public class GameEnd : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField]
    private WinPlanle winPlanle;
    [SerializeField]
    private LosePlanel losePlanle;
    private int dubleCion = 1;
    void Start()
    {
        // if (PlayerData.instance.getCion)
        Init();
               
    }

    private void SaveData()
    {
        //yield return null;

        PlayerData.instance.currSingleLevelData.cion = PlayerData.instance.currSingleLevelData.cion * dubleCion;
        PlayerData.playerDataParame.cions += PlayerData.instance.currSingleLevelData.cion;
        var strName = PlayerData.instance.dailyTaskDataValue.obtain10Key;
        PlayerData.instance.dailyTaksValueDit[strName]++;
        strName = PlayerData.instance.dailyTaskDataValue.obtainCion;
        PlayerData.instance.dailyTaksValueDit[strName] += PlayerData.instance.currSingleLevelData.cion * dubleCion;

        PlayerData.instance.isCionFlight = PlayerData.instance.currSingleLevelData.cion > 0 ? true : false;
        PlayerData.instance.CionFlightCount = PlayerData.instance.currSingleLevelData.cion;


        //--------------------成就任务数据-----------------------------
        PlayerData.instance.achieveParmeter.passLevelCount++;

        if (PlayerData.instance.currSingleLevelData.FoodFireCount == 0 )
        {
            PlayerData.instance.achieveParmeter.contiunNoFireFoodCount++; 
        }
        else
        {
            PlayerData.instance.achieveParmeter.contiunNoFireFoodCount = 0;
        }

        if (PlayerData.instance.currSingleLevelData.disCardFoodCount == 0)
        {
            PlayerData.instance.achieveParmeter.continuDisCardFoodCount++;
        }
        else
        {
            PlayerData.instance.achieveParmeter.continuDisCardFoodCount = 0; ;
        }

        //同一天
        var singinDataTime = PlayerData.instance.playerLoacadData.openAppTime;
        TimeSpan span = DateTime.Now.Subtract(DateTime.Parse(singinDataTime)).Duration();
        if ((int)span.TotalHours < 24 || (int)span.TotalDays < 0)
        {
            PlayerData.instance.achieveParmeter.sameDayLevelCount++;
        }
        else
        {
            PlayerData.instance.achieveParmeter.sameDayLevelCount = 1; 
        }

        if (PlayerData.instance.currSingleLevelData.cion >= 1000)
        {
            PlayerData.instance.achieveParmeter.sameLeveinCionCount++;
        }
        if ( PlayerData.instance.currSingleLevelData.continuCount>=30)
        {
            PlayerData.instance.achieveParmeter.sameLevelServiceGuestCount++;
        }
        PlayerData.instance.achieveParmeter.fiveCombosCount += PlayerData.instance.currSingleLevelData.contiunFiveSbmitCount;
        PlayerData.instance.achieveParmeter.fourCombosCount += PlayerData.instance.currSingleLevelData.contiunFourSbmitCount;
        PlayerData.instance.achieveParmeter.threeCombosCount += PlayerData.instance.currSingleLevelData.contiunThreeSbmitCount;

        if (PlayerData.instance.currSelectPropList.Count > 0)
            PlayerData.instance.achieveParmeter.usePropCount += PlayerData.instance.currSelectPropList.Count;
        for (int i = 0; i < PlayerData.instance.currSelectPropList.Count; i++)
        {
            if (PlayerData.instance.currSelectPropList[i] == ConsumePropType.MomentFood)
                PlayerData.instance.achieveParmeter.useFastMakePropCount += 1;
            else if (PlayerData.instance.currSelectPropList[i] == ConsumePropType.FireFood)
                PlayerData.instance.achieveParmeter.useNoFirePropCount += 1;
            else if (PlayerData.instance.currSelectPropList[i] == ConsumePropType.GuestPatience)
                PlayerData.instance.achieveParmeter.usePatiencePropCount += 1;
            else if (PlayerData.instance.currSelectPropList[i] == ConsumePropType.AddGuest)
                PlayerData.instance.achieveParmeter.useAddGuestPropCount += 1;
            else if (PlayerData.instance.currSelectPropList[i] == ConsumePropType.SetlementDoubleCion)
                PlayerData.instance.achieveParmeter.useDubleCionPropCount += 1;
            else if (PlayerData.instance.currSelectPropList[i] == ConsumePropType.AutomaticSubmitFood)
                PlayerData.instance.achieveParmeter.useAutomaticSumbitPropCount += 1;
        }
        PlayerData.instance.achieveParmeter.obtainCionCount += PlayerData.instance.currSingleLevelData.cion;
       PlayerData.instance.achieveParmeter.sameDayObtainCionCount += PlayerData.instance.currSingleLevelData.cion;
        AchieveSprot();
        //--------------------成就任务数据-----------------------------
        bool isFind = false;

        if (PlayerData.playOnceParme.levelPlayOnceDataDict.ContainsKey(PlayerData.instance.currPlayeLevel))
        {
            var value = int.Parse(PlayerData.playOnceParme.levelPlayOnceDataDict[PlayerData.instance.currPlayeLevel]) +1;
            value = value > 3 ? 3 : value;
            PlayerData.playOnceParme.levelPlayOnceDataDict[PlayerData.instance.currPlayeLevel] = value.ToString ();
        }
        else
        {
            PlayerData.playOnceParme.levelPlayOnceDataDict[PlayerData.instance.currPlayeLevel] = 1.ToString();
            PlayerData.playerDataParame.keyNumber += PlayerData.instance.CurrLevelPrizeKeyNumber;
        }
        //for (int i = 0; i < PlayerData.playOnceParme.levelIndexID.Count; i++)
        //{
        //    if (PlayerData.instance.currPlayeLevel.ToString() == PlayerData.playOnceParme.levelIndexID[i])
        //    {
        //        isFind = true;
        //        PlayerData.playOnceParme.levelPlayOnce[i]++;
        //        break;
        //    }
        //}
        //if (!isFind)
        //{
        //    PlayerData.playOnceParme.levelIndexID.Add(PlayerData.instance.currPlayeLevel);
        //    PlayerData.playOnceParme.levelPlayOnce.Add(1);
        //    //玩第一次获得
        //    PlayerData.playerDataParame.keyNumber += PlayerData.instance.CurrLevelPrizeKeyNumber;
        //    PlayerData.instance.achieveParmeter.obtainKeyCount += PlayerData.instance.CurrLevelPrizeKeyNumber;
        //}


#if UNITY_WEBGL && !UNITY_EDITOR
 var daat = JsonMapper.ToJson(PlayerData.playOnceParme);
              CoroutineHandler.StartStaticCoroutine(  PlayerData.instance.SaveData("UpdatePlayOnce", daat));
#elif UNITY_EDITOR
        var daat = JsonMapper.ToJson(PlayerData.playOnceParme.levelPlayOnceDataDict);
        CoroutineHandler.StartStaticCoroutine(PlayerData.instance.SaveData("PlayeLevelOnce", daat));
#endif

    }
    private void Init()
    {

        for (int i = 0; i < PlayerData.instance.currSelectPropList.Count; i++)
        {
            if (PlayerData.instance.currSelectPropList[i] == ConsumePropType.SetlementDoubleCion)
            {
                dubleCion++;
                break;
            }
        }
        bool isWin = false;
        if (PlayerData.instance.targetType == TargetType.CIonTraget)
        {
            if (PlayerData.instance.currSingleLevelData.cion >= PlayerData.instance.targerCount)
            {
                isWin = true;
            }
        }
        else if (PlayerData.instance.targetType == TargetType.CompeletFood)
        {
            if (PlayerData.instance.currSingleLevelData.compeleFoodNumber >= PlayerData.instance.targerCount)
            {
                isWin = true;
            }
        }
        else if (PlayerData.instance.targetType == TargetType.GoodCommend)
        {
            if (PlayerData.instance.currSingleLevelData.goodCommendNumber >= PlayerData.instance.targerCount)
            {
                isWin = true;
            }
        }

        if (isWin)
        { 
            winPlanle.ShowCionCount = PlayerData.instance.currSingleLevelData.cion * dubleCion;
            winPlanle.ShowKeyCount = 1;
            winPlanle.UpdateShow();
            
            winPlanle.gameObject.SetActive(true);
            losePlanle.gameObject.SetActive(false);
            // CoroutineHandler.StartStaticCoroutine(SaveData());
            SaveData();
           var strName =  PlayerData.instance.dailyTaskDataValue.compelet20LevelCount;
            PlayerData.instance.dailyTaksValueDit[strName]++;

            if (PlayerData.instance.currSelectLevel == PlayerData.instance.lastSelectLevel +1)
            {
                strName = PlayerData.instance.dailyTaskDataValue.continu10levelCount;
                PlayerData.instance.dailyTaksValueDit[strName]++;

            }
            else
            {
                strName = PlayerData.instance.dailyTaskDataValue.continu10levelCount;
                PlayerData.instance.dailyTaksValueDit[strName] = 0;
            }

            PlayerData.instance.keyIsFlightAnimation = true;
        }
        else
        {
            winPlanle.gameObject.SetActive(false);
            losePlanle.gameObject.SetActive(true);
        }
        AchieveCompute();
        PlayerData.instance.lastSelectLevel = PlayerData.instance.currSelectLevel;

        PlayerData.instance.PlayerDataParameSave();
        PlayerData.instance.SaveDayliTaskValueData();
        var achieveData = JsonMapper.ToJson(PlayerData.instance.achieveParmeter);
        PlayerData.instance.SavePlayerPrefsValue("AchieveTaskData", achieveData);
    }
    
    /// <summary>
    /// 成就星星计算
    /// </summary>
    private void AchieveSprot()
    {
        var leveList = LoadConfigursManager.instans.LevelShopDataDuct[PlayerData.instance.currLevelName];
        var curResualrent = leveList.Count;

        int oneSpot = 0;
        int tweSpot = 0;
        int threeSpot = 0;

        for (int i=0; i<leveList.Count; i++)
        {
            if (PlayerData.playOnceParme.levelPlayOnceDataDict.ContainsKey(leveList[i].levelID))
            {
                var count = PlayerData.playOnceParme.levelPlayOnceDataDict[leveList[i].levelID];
                if (count == 1.ToString())
                {
                    oneSpot++;
                }
                else if (count == 2.ToString())
                {
                    tweSpot++;
                }
                else if (count == 3.ToString())
                {
                    threeSpot++;
                }
            }
        }

        if (oneSpot >= leveList.Count)
        {
            PlayerData.instance.achieveParmeter.AllLevelOneStarCount++;
        }
        if (tweSpot >= leveList.Count)
        {
            PlayerData.instance.achieveParmeter.AllLevelTweStarCount++;
        }
        if (threeSpot >= leveList.Count)
        {
            PlayerData.instance.achieveParmeter.AllLevelThreeStarCount++;
        }
    }
    private void AchieveCompute()
    {

        PlayerData.instance.achieveParmeter.serveiceGuestCount += PlayerData.instance.currSingleLevelData.comeGuestCount;

        PlayerData.instance.achieveParmeter.totalSubmitFoodCount += PlayerData.instance.currSingleLevelData.submitFoodCount;
        PlayerData.instance.achieveParmeter.totalSubmitDrinkCount += PlayerData.instance.currSingleLevelData.submitDrinkCount;
        PlayerData.instance.achieveParmeter.goodCommentCount += PlayerData.instance.currSingleLevelData.goodCommendNumber;
        PlayerData.instance.achieveParmeter.otherFoodCount += (PlayerData.instance.currSingleLevelData.submitFoodCount + PlayerData.instance.currSingleLevelData.submitDrinkCount);
    }
    public void ClickContuine()
    {
        PlayerData.instance.continueGame = true;
        UnityEngine.SceneManagement.SceneManager.LoadScene("Start");

    }
}
