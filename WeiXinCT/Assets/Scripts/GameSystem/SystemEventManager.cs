using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystemEventManager : MonoBehaviour
{

    private SystemGameManager systemGameManager = null;

    public void ConFigur(SystemGameManager systemGameManager)
    {
        this.systemGameManager = systemGameManager;
    }
    
     private static Dictionary<string, Action<string,EventParamProperties>> singleEventDictionary = new Dictionary<string, Action<string,EventParamProperties>>();

    public static void StartListening(string eventName, Action<string,EventParamProperties> listener)
    {
        Action<string,EventParamProperties> thisEvent;
        //if (singleEventDictionary.TryGetValue(eventName, out thisEvent))
        //{
        //    singleEventDictionary[eventName] = thisEvent;
        //}
        if (singleEventDictionary.ContainsKey(eventName))
        {
            thisEvent = listener;
            singleEventDictionary[eventName] = thisEvent;
        }
        else
        {
            thisEvent = listener;
            singleEventDictionary.Add(eventName, thisEvent);
        }
    }

    public static void TriggerEvent(string eventName, EventParamProperties eventParam)
    {
        Action<string,EventParamProperties> thisEvent;
        if (singleEventDictionary.TryGetValue(eventName, out thisEvent))
        {
            if (thisEvent != null)
            {
                thisEvent.Invoke(eventName, eventParam);
            }
        }
    }

    public static void StopListening(string eventName, Action<string,EventParamProperties> listener)
    {
        Action<string,EventParamProperties> thisEvent;
        if (singleEventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent -= listener;
            singleEventDictionary[eventName] = listener;
        }
    }
    public static void Clear()
    {
        singleEventDictionary.Clear();
    }

}
