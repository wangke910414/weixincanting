using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManagur : MonoBehaviour
{
    static public AudioManagur Instance;
    [SerializeField]
    private AudioMixer audioMixer;

    private bool audioMusicIsOff;
    private bool audioSoundIsOff;

    public bool AudioMusicIsOff { get => audioMusicIsOff; }
    public bool AudioSoundIsOff { get => audioSoundIsOff; }


    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        Instance = this;
        // DontDestroyOnLoad(gameObject);
    }
    private void Start()
    {
        //RestValue();
    }
    public void InitlalValue()
    {
        var volume = 0f;
        audioMixer.GetFloat("SoundVolume", out volume);
        Debug.Log("获取  " + volume);
        if (volume <= -80f)
        {
            audioSoundIsOff = false;
        }
        else
        {
            audioSoundIsOff = true;
        }

        Debug.Log("获取  " + volume);
        audioMixer.GetFloat("MusicVolume", out volume);
        if (volume <= -80f)
        {
            audioMusicIsOff = false;
        }
        else
        {
            audioMusicIsOff = true;
        }
    }

    public void SetAuduioSound()
    {
       
        var volume = 0f;
        audioMixer.GetFloat("SoundVolume", out volume);
        Debug.Log("设置音效  "+volume);
        if (volume <= -80f)
        {
            audioMixer.SetFloat("SoundVolume", 0f);
        }
        else
        {
            audioMixer.SetFloat("SoundVolume", -80f);
        }


    }
    public void SetAudioMusic()
    {
        var volume = 0f;
        audioMixer.GetFloat("MusicVolume", out volume);

        Debug.Log("设置音乐  " + volume);
        if (volume <= -80f)
        {
            audioMixer.SetFloat("MusicVolume", 0f);
        }
        else
        {
            audioMixer.SetFloat("MusicVolume", -80f);
        }
    }
}
