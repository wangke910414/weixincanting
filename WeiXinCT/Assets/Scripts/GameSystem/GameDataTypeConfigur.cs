using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public enum EnemyType : int
{
    Boss,
    Enemy
}

[Serializable]
public enum EnemyPrafebName  : int 
{ 
    Enemy_1,
    Enemy_2,
    Enemy_3,
    Enemy_4,
    Enemy_5,
    Enemy_6,
    Enemy_7,
    Enemy_8,
    Boss_1
}


[Serializable]
public enum ItemType : int
{
    Waepom,
    Gold

}

public enum GameStat : int
{ 
    GameStart,
    GameRun,
    GameStop,
    GameEnd
}

public enum PlayerStat : int
{
    idle,
    Run,
    Stop,
    Attack,
    Die,
}