using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using DG.Tweening;
//UI图片拖拽功能类 脚本功能：UI图片拖拽功能（将脚本挂载在需要拖放的图片上）


public enum DragType
{
    MoveXY,
    MoveX,
    MoveY,
}

public class UIImageDrag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [Header("拖动方式")]
    public DragType dragType;

    [Header("是否精准拖拽")]
    public bool m_isPrecision;

    [Header("是否开启缩放")]
    public bool isCanZoom;//是否开启缩放

    [Tooltip("最大放大倍数")]
    public float m_maxScale = 4;

    [Tooltip("最小缩小倍数")]
    public float m_minScale = 0.4f;

    [Header("表示限制的区域")]
    public RectTransform LimitContainer;

    public Canvas canvas;
    // 最小、最大X、Y坐标
    float minX, maxX, minY, maxY;

    private Vector3 m_offset;

    private Vector3 orginPos;
    //存储当前拖拽图片的RectTransform组件
    private RectTransform m_rt;


    void Start()
    {
        //初始化
        m_rt = gameObject.GetComponent<RectTransform>();
        orginPos = m_rt.position;
        SetDragRange();
    }

    private void Update()
    {
        if (isCanZoom)
        {
            float s = Input.GetAxis("Mouse ScrollWheel");
            if (s > 0)
            {
                Magnify();
            }
            else if (s < 0)
            {
                Shrink();
            }

        }

    }

    /// <summary>
    /// 放大
    /// </summary>
    void Magnify()
    {
        if (m_rt.localScale.x < m_maxScale)
        {
            m_rt.localScale += new Vector3(0.1f, 0.1f, 0.1f);
        }
    }
    /// <summary>
    /// 缩小
    /// </summary>
    void Shrink()
    {
        if (m_rt.localScale.x > m_minScale)
        {
            m_rt.localScale -= new Vector3(0.1f, 0.1f, 0.1f);
        }
    }
    /// <summary>
    /// 重置图片大小位置
    /// </summary>
    public void ImageReset()
    {
        m_rt.localScale = Vector3.one;
        m_rt.localPosition = Vector3.zero;
    }

    //开始拖拽触发
    public void OnBeginDrag(PointerEventData eventData)
    {
        //如果精确拖拽则进行计算偏移量操作
        if (m_isPrecision)
        {
            // 存储点击时的鼠标坐标
            Vector3 tWorldPos;
            //UI屏幕坐标转换为世界坐标
            RectTransformUtility.ScreenPointToWorldPointInRectangle(m_rt, eventData.position, eventData.pressEventCamera, out tWorldPos);
            //计算偏移量
            //m_offset = transform.position - tWorldPos;
           var temp =new Vector3( eventData.position.x, eventData.position.y,0);
            m_offset = transform.localPosition - temp;
        }
        //否则，默认偏移量为0
        else
        {
            m_offset = Vector3.zero;
        }
       // SetDragRange();
        SetDraggedPosition(eventData);
    }

    //拖拽过程中触发
    public void OnDrag(PointerEventData eventData)
    {
        SetDraggedPosition(eventData);
    }

    //结束拖拽触发
    public void OnEndDrag(PointerEventData eventData)
    {
        SetDraggedPosition(eventData);
    }

    /// <summary>
    /// 设置图片位置方法
    /// </summary>
    /// <param name="eventData"></param>
    private void SetDraggedPosition(PointerEventData eventData)
    {
        //存储当前鼠标所在位置
        Vector3 globalMousePos;
        //UI屏幕坐标转换为世界坐标
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(m_rt, eventData.position, eventData.pressEventCamera, out globalMousePos))
        {
            Debug.Log(eventData.pressPosition.x+ "      "+ eventData.pressPosition.y);
            //临时使用
            globalMousePos = new Vector3(eventData.position.x, eventData.position.y, 0);
            var temp = globalMousePos + m_offset;
            if (temp.x > maxX)
            {
                return;
            }
            else if (temp.x < minX)
            {
                return;
            }

            if (dragType == DragType.MoveXY)
            {
                m_rt.transform.DOMove(globalMousePos + m_offset, 0.5f);
            }
            else if (dragType == DragType.MoveX)
            {
                 m_rt.transform.DOLocalMoveX(globalMousePos.x + m_offset.x, 0.3f);
  
            }
            else if (dragType == DragType.MoveY)
            {
                m_rt.transform.DOMoveY(globalMousePos.y + m_offset.y, 0.5f);
            }
        }
    }

    void SetDragRange()
    {
        var crt = canvas.GetComponent<RectTransform>();


        var tempWidth = (m_rt.rect.width - crt.rect.width) / 2 ;
        // var sPos = new Vector3(tempWidth, m_rt.rect.height - crt.rect.height, 0);
        Debug.Log("图片长度   " + m_rt.rect.width  +  "  " + crt.rect.width);
        Debug.Log("计算长度   " + tempWidth);
        minX = transform.localPosition.x - tempWidth;
         maxX = transform.localPosition.x + tempWidth;

        Debug.Log("最大   " + minX + "  最小   " + maxX + "   " + maxX);

    }

    /// <summary>
    /// 限制坐标范围
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    Vector3 DragRangeLimit(Vector3 pos)
    {
        pos.x = Mathf.Clamp(pos.x, minX, maxX);
       // pos.y = Mathf.Clamp(pos.y, minY, maxY);

        return pos;
    }

}