using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public enum PrizeType : int
{ 
    Null,
    Cion,            //金币
    Diamond,        //钻石
    ComsumeProp,    //道具
    DiamondAddProp,     //钻石 + 道具

}

[Serializable]
public class PrizeComponent
{
    public PrizeType prizeType;
    public int PrizeCount;
    [Header("奖品为道具时")]
    public ConsumePropType consumePropType;
    public Sprite prizeImg;
}
