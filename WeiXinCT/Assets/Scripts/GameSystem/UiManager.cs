using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;


public class UiManager : MonoBehaviour
{
    private SystemGameManager systemGameManager = null;

    [SerializeField]
    private GameObject uiManagerPrent;

    [SerializeField]
    private GameObject startBtn;

    [SerializeField]
    private GameObject continueBtn;

    [SerializeField]
    private TextMeshProUGUI textLog;

    [SerializeField]
    private Transform pos1;
    [SerializeField]
    private Transform pos2;

    [SerializeField]
    private OverPlanel overPlanel;

    [SerializeField]
    private LoadinPlanle loadingPlanle;

    [SerializeField]
    private TargetPlanle targetPlanle;

    [SerializeField]
    private GameHomePlanel gameHomePlael;

    [SerializeField]
    private StopPanel stopPanel;

    [SerializeField]
    private FailTip failTip;

    [SerializeField]
    private TeachingPanle teachingPanle;

    public GameHomePlanel GameHomePlael { get => gameHomePlael; }

    public void Configuer(SystemGameManager systemGameManager)
    {
        this.systemGameManager = systemGameManager;
        if (systemGameManager.GameStat == GameStat.GameStart)
        {
         

        }
       // overPlanel.UiConfigur(this);
        loadingPlanle.Configur(systemGameManager);
        targetPlanle.Configur(systemGameManager);
        gameHomePlael.Configur(systemGameManager);
        failTip.Configur(systemGameManager);
        stopPanel.Configur(systemGameManager);
       loadingPlanle.OpenUiWo();
        SystemEventManager.StartListening("TeachingMsg", ReceiveTeachingMsg);
    }

    public void ClickStatrGame()
    {
       
    }
   
    public void CallUiRunGame()
    {
        startBtn.SetActive(false);

    }

    public void ChangeLevelEffice()
    {
        overPlanel.gameObject.SetActive(true);
        overPlanel.OpenUiWo();
    }

    public void LoadingPlanle()
    {
        loadingPlanle.OpenUiWo();
    }

    public void LoadingContinue()
    {
        systemGameManager.ContinueGame();  
    }
    public void OpenTargetDlg()
    {
       // loadingPlanle.CloseUiWo();

       StartCoroutine(TargetOpen());
    }
    private IEnumerator TargetOpen()
    {
        yield return new WaitForSeconds(1f);
        targetPlanle.OpenUiWo();
        gameHomePlael.OpenUiWo();

        yield return new WaitForSeconds(2f);
        if (PlayerData.instance.teachingValue.MakeFoodClick == 0)
            TeachingOperation(TeachingType.MakeFood);
    }
    public void GuetNeedComelet()
    {
        gameHomePlael.SetTraget();

    }
    public void OpenFailTip(ConsumePropType type)
    {
        if (!failTip.gameObject.activeSelf)
            failTip.buyConsumeProp = type;
            failTip.OpenDlg();
    }
    public void OpenStopDlg()
    {
        stopPanel.OpenDlg();
    }
    public void TeachingOperation(TeachingType type)
    {
        return;
        teachingPanle.TeachingOperation(type);
            Time.timeScale = 0;
    }

    private void ReceiveTeachingMsg(string eventName, EventParamProperties msg)
    {
        var receiveMsg = msg as EventTeachingMsg;
        if (receiveMsg.teachingType == TeachingType.MakeFood)
        {
            //提示制作食物
            PlayerData.instance.teachingValue.MakeFoodClick++;
            var food = systemGameManager.OperationManage.RwaMainFoodList[0];
            food.GetComponent<RawFoodOpreat>().EvetClick();
            Time.timeScale = 1;
        }
        else if (receiveMsg.teachingType == TeachingType.FoodMakeCompelt)
        {
            //提示食物传提交台
             var makeParent = systemGameManager.LevelManager.CurrRestaurantInfo.FoodMakeParent;
            PlayerData.instance.teachingValue.FoodMakeCompeltClick++;
            makeParent.GetComponent<MakinFoodOpreat>().FoodList[0].ClilkFood();
            CoroutineHandler.StartStaticCoroutine(TeachingSubmitFood());
            Time.timeScale = 1;
        }
        else if (receiveMsg.teachingType == TeachingType.SubmitFood)
        {
            //提示提交食物
            PlayerData.instance.teachingValue.SubmitFoodClick++;
            var submitOperation = systemGameManager.LevelManager.CurrRestaurantInfo.PlateParent;
           var palet =  submitOperation.GetComponent<CookedFoodOpreat>().PlateParent[0];
            palet.GetComponent<PlateItem>().DeliverFood();
            Time.timeScale = 1;
        }
        else if (receiveMsg.teachingType == TeachingType.SubmitDrink)
        {
            PlayerData.instance.teachingValue.SubmitDrinkClick++;
            Time.timeScale = 1;
            var drink = systemGameManager.LevelManager.CurrRestaurantInfo.DrinkOpreat;
            drink.OnClick(1);
        }
        else if (receiveMsg.teachingType == TeachingType.AddOtherFood)
        {
            PlayerData.instance.teachingValue.addOtherFoodClick++;
            Time.timeScale = 1;
            var restaurentInfo = systemGameManager.LevelManager.CurrRestaurantInfo;
            restaurentInfo.otherFood[0].GetComponent<OtherFood>().ClickFood();
        }
        PlayerData.instance.SaveTeachingData();
    }

    private IEnumerator TeachingSubmitFood()
    {
        yield return new WaitForSeconds(0.5f);
        if (PlayerData.instance.teachingValue.SubmitFoodClick == 0)
        {
            TeachingOperation(TeachingType.SubmitFood);
        }

    }
}
