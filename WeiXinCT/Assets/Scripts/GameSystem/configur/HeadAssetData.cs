using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]
public class HeadAssetData
{
    public string headID;
    public float hedPrice;
    public Sprite headImage;

}
