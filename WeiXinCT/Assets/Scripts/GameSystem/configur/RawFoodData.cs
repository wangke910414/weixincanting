using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class RawFoodData
{
    public string foodName;

    public string resourcesName;

    public FoodMainType isFoodMainType;

    public GameObject rawFoodPrefab;

    public float price = 10;
}


public enum FoodMainType : int
{
    MainFood,
     OtherFood,
}