using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CanTing/SpecialLimitCongigur", fileName = "SpecialLimitCongigur")]
public class SpecialLimitCongigur : ResourceProfile
{
    [SerializeField]
    private List<SpecialLimit> specialLimitList = new List<SpecialLimit>();

    public SpecialLimit GetSpecialLimit(SpecialLimitType type)
    {
        for (int i=0; i<specialLimitList.Count; i++)
        {
            if (specialLimitList[i].specialType == type)
                return specialLimitList[i];
        }
        return null;
    }
}
