using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CanTing/RestaurantResourceConfigur", fileName = "RestaurantResourceConfigur")]
public class RestaurantResourceConfigur : ResourceProfile
{
    [Header("餐厅内各种组件等级图片资源")]
    public List<RestaurantData> restaurantData = new List<RestaurantData>();

    public RestaurantData GetRestaurantData(string name)
    {
        foreach(var data in restaurantData)
        {
            if (data.hotelResoursceNmae == name)
                return data;
        }
        return null;
    }

}
