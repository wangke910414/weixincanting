using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class GuestData
{
    [SerializeField]
    private GameObject guestPrefab;

   public GameObject GuestPrefab { get => guestPrefab; }
}
