using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;



[Serializable]
public class RestaurantData
{
    [Header("餐厅地图名称")]
    public string hotelResoursceNmae;


    [Header("最大制作个数")]
    public int maxManking;


   // [Header("最放置个数")]
  //  public GameObject RestauranPrefab;

}

[Serializable]
public class DrinkComponent
{
    public string drinkId;
    public string dringName;
    public GameObject prefab;
}

[Serializable]
public class RestaurantResourceData
{
    public string componentName;
    public ComponentGradeImg componentGradeImg;
}
 
public class ComponentGradeImg
{
    public Sprite gradeLeve_1;
    public Sprite gradeLeve_2;
    public Sprite gradeLeve_3;
}