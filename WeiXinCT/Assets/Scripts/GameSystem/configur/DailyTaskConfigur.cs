using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CanTing/DailyTaskConfigur", fileName = "DailyTaskConfigur")]
public class DailyTaskConfigur : ResourceProfile
{
    [Header("连击任务")]
    public List<ContinuSubmitDailyTask> continuSubmitTakList = new List<ContinuSubmitDailyTask>();

    [Header("出餐任务")]
    public List<SubmitFoodDailyTask> submitFoodTaskLiat = new List<SubmitFoodDailyTask>();

    [Header("道具任务")]
    public List<ConsumePropDailyTask> consumePropTaskList = new List<ConsumePropDailyTask>();

    [Header("关卡任务")]
    public List<LevelDailyTask> levelTaskList = new List<LevelDailyTask>();

    [Header("货币任务")]
    public List<CurrencyDailyTask> currencyTaskList = new List<CurrencyDailyTask>();
}
