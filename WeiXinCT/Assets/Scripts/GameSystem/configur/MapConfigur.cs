using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
//地图资源类
//

[Serializable]
public class MapConfigurDataItem
{
    public string mapId;        //地图ID
    public string mapName;
    public string resourceName;          //地图名称
    public string restaurentCount;      //包含餐厅数量
    public int startRestaurentID;       //餐厅开始ID(包含)
    public int endRestaurentID;             //餐厅结束ID（包含）
    public Sprite icon;
    GameObject prefab;
}

[CreateAssetMenu(menuName = "CanTing/MapAssets", fileName = "MapConfigur")]
public class MapConfigur : ResourceProfile
{
   
}
