using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu (fileName = "HeadAssetConfigur", menuName = "CanTing/HeadAssetConfigur")]
public class HeadAssetConfigur : ResourceProfile
{

    public List<HeadAssetData> HeadAssetDataList = new List<HeadAssetData>();
}
