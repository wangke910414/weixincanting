using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "CanTing/ConsumePropConfigur", fileName = "ConsumePropConfigur")]

public class ConsumePropConfigur : ResourceProfile
{
    [SerializeField]
    private ConsumeProp[] consumeProp;

    public ConsumeProp[] ConsumeProp { get => consumeProp; }

    public ConsumeProp GetConsumeProp(ConsumePropType cType)
    {
        for (int i=0; i< consumeProp.Length; i++)
        {
            if (consumeProp[i].comsumePropType == cType)
                return consumeProp[i];
        }
        return null;
    }
}
