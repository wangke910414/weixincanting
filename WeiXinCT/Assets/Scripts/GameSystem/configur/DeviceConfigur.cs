using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DeviceConfigur", menuName = "CanTing/DeviceConfigur ")]
public class DeviceConfigur : ResourceProfile
{
    public List<DeviceCompnentAssets> deviceList = new List<DeviceCompnentAssets>();

    public DeviceCompnentAssets GetDevice(string deviceName)
    {
        foreach(var data in deviceList)
        {
            if (data.deviceName == deviceName)
                return data;
        }
        return null;
    }
}
