using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(menuName = "CanTing/MainFoodAssets", fileName = "MainFoodAssets")]
public class MainFoodConfigur : ResourceProfile
{
    [Header("主食资源文件")]

    public  List<MainFoodCompnentAssets> foodDataList = new List<MainFoodCompnentAssets>();


    public MainFoodCompnentAssets GetMainFoodAsset(string foodName)
    {
        foreach (var item in foodDataList)
        {
            if (item.mainFoodNmae == foodName)
                return item;
        }
        return null;
    }
}
