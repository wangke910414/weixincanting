using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu (menuName = "CanTing/RestaurantAsset", fileName = "RestaurantAsset")]
public class RestaurantAssetConfiger : ResourceProfile
{
   public List<RestaurantData> RestaurantDataList = new List<RestaurantData>();
}
