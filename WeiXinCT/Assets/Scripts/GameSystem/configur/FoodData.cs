using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 主食资源文件
/// </summary>
[Serializable]
public class MainFoodCompnentAssets
{
    [Header("主食名称")]
    public string mainFoodNmae;
    [Header("主食预制体")]
    public GameObject mainFoodPrefab;

   // public Sprite foodIcon;
    public List<Sprite> IconList = new List<Sprite>();
    public string foodDescrible;
    //[Header("主食搭配菜")]
    //public List<OtheFoodCompnentAssets> otherFoodList = new List<OtheFoodCompnentAssets>();
}

/// <summary>
/// 配置资源文件  
/// </summary>
[Serializable]
public class OtheFoodCompnentAssets
{
    [Header("配菜名称")]
    public string otherFoodName;
    [Header("配菜预制体")]
    public GameObject otherFoodPrefab;
         public List<Sprite> IconList = new List<Sprite>();
    public string foodDescrible;
}


//设备

[Serializable]
public class DeviceCompnentAssets
{

    [Header("设备名称")]
    public string deviceName;

    public List<Sprite> deviceIcon;
    public int minCount;

    public int maxCount;

    public int makeTime;

    [Header("描述")]
    public string describe;
}

public enum  FoodType: int
{
    MainFood,
    OtherFood,
    Drink
}

public class FoodBose
{
    public FoodType foodTyoe;
}
// < summary >
/// 配菜数据 读取json
/// </summary>
public class OtherFoodItem : FoodBose
{
    public string OtherFoodID;

    public string OtherFoodName;
    public string mainFood;
    public float otherFoodPrice;    //价格
    public int upFoodPrice;
    public int unLoackLevel;
    public string shopName = string.Empty;
    public int isMake;          //事发需要制作
   public OtherFoodItem()
    {
        foodTyoe = FoodType.OtherFood;
    }
}

/// <summary>
/// 主食数据读取json
/// </summary>
public class MainFoodItem : FoodBose
{
    public string mianFoodID;
    public string mainFoodName;
    public string foodTypeName;
    public string onParent;
    public float mainFoodPrice;
    public int unLoackLevel;
    public int upFoodPrice;
    public string shopName = string.Empty;
    public string makeDeviceName;
    public int isMake;          //事发需要制作

    public MainFoodItem()
    {
        foodTyoe = FoodType.MainFood;
    }
}


//设备数据

public class DeviceData
{
    public string deviceID;
    public string deviceName;
    public int unLoackLevel;
    public int upPrice;
    public string shopName;
}

/// <summary>
/// 食物组件 读json
/// </summary>
public class FoodItem
{
    public string mainFoodName;
    public string[] otherFoodName = new string[4];

   //public string otherFoodName2;
   // public string otherFoodName3;
}


[Serializable]
public class FoodMenuAsset
{
    public string foodMunuName;

    public Sprite foodMenuIcon;
}

/// <summary>
/// 菜谱
/// </summary>
public class FoodMenu
{
    public string foodMenuID;
    public string foodName;
    public string foodType;         //drink 饮料    food食物
    public string needMainFood;

    public string[] needOtherFood = new string[4];

    public string classNum;

   // public string needOtherFood2;

    //public string needOtherFood3;
}



[Serializable]
public class GameTaskData
{
    public string taskID;
    public string taskName;
    public string taskDescrioble;
    public int taskReward;
    public int taskMaxGrade;
}
