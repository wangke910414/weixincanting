using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DailyTaskBase
{

    public string DailyTaskName = string.Empty;
    public DailyTakeType dailyTaskType = DailyTakeType.Null;
    public Sprite taskIcon;                 
    public int daiyTaskID;
    public string describle = string.Empty;
    public PrizeComponent prize = new PrizeComponent();
    public int targerCount = 0;
    [HideInInspector]
    public int currTargessCount = 0;
    [HideInInspector]
    public string dailyKey = string.Empty;          //数据Key
    protected Dictionary<string,int> taskValue;     //数据集合

    public virtual bool OpretaHandle()
    {
        return false;
    }
    
}

public enum DailyTakeType : int
{

    Null,
    ContinuSubmitDailyTask,
    SubmitFoodDailyTask,
    ConsumePropDailyTask,
    LevelDailyTask,
    CurrencyDailyTask
}
/// <summary>
/// 连击类型
/// </summary>
[Serializable]
public class ContinuSubmitDailyTask : DailyTaskBase
{

    ContinuSubmitDailyTask()
    {
        base.dailyTaskType = DailyTakeType.ContinuSubmitDailyTask;
      
    }
    public enum OprateType : int
    {

        Null,
        [Tooltip("完成100次连击")]
        Complete_100_Continu,   //完成100次连击
        [Tooltip("完成一次5连击")]
        Complete_1_5Continu,   //完成一次5连击
        [Tooltip("完成一次4连击")]
        Complete_10_4Continu,   //完成一次4连击
    }
    public OprateType oprateType = OprateType.Null;

    public override bool OpretaHandle()
    {
        taskValue = PlayerData.instance.dailyTaksValueDit;

        if (oprateType == OprateType.Complete_100_Continu)
        {
            return Complete_100_Continu();
        }
        else if(oprateType == OprateType.Complete_10_4Continu)
        {
            return Complete_10_4Continu();
        }
        else if (oprateType == OprateType.Complete_1_5Continu)
        {

            return HandleComplete_1_5Continu();
        }
        return false;
    }

    private bool Complete_100_Continu() //完成100次连击
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.continuFoodCout;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
       
        return false;
    }
    private bool HandleComplete_1_5Continu()    //完成一次5连击
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.conpelet5ContinuCount;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
        return false;
    }
    private bool  Complete_10_4Continu()     //完成10四练级
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.conpelet4ContinuCount;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
        return false;
    }
}
/// <summary>
///提交食物类型
/// </summary>
[Serializable]
public class SubmitFoodDailyTask : DailyTaskBase
{
     SubmitFoodDailyTask()
    {
        dailyTaskType = DailyTakeType.SubmitFoodDailyTask;
        targerCount = 0;
        oprateType = OprateType.Null;
    }
    public enum OprateType : int
    {
        Null,
        [Tooltip("完成50个出餐")]
        Complete_150_SubmitFood,   //完成50个出餐
        [Tooltip("完成30个饮料")]
        Complete_30_Drink,   //完成30个饮料
        [Tooltip("获得50个好评")]
        Complete_50_GoodComment,   //获得50个好评
    }
    public OprateType oprateType = OprateType.Null;

    public override bool OpretaHandle()
    {
        taskValue = PlayerData.instance.dailyTaksValueDit;

        if (oprateType == OprateType.Complete_150_SubmitFood)
        {
            return Complete_50_SubmitFood();
        }
        else if (oprateType == OprateType.Complete_30_Drink)
        {
            return Complete_30_Drink();
        }
        else if (oprateType == OprateType.Complete_50_GoodComment)
        {
            return Complete_50_GoodComment();
        }
        return false;
    }


    private bool Complete_50_SubmitFood() //完成50个出餐
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.foodCount;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;

        return false;
    }
    private bool Complete_30_Drink()    //完成30个饮料
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.drinkCount;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
        return false;
    }
    private bool Complete_50_GoodComment()     //获得50个好评
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.goodComment;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
        return false;
    }
}

/// <summary>
/// 道具类型
/// </summary>
[Serializable]
public class ConsumePropDailyTask : DailyTaskBase
{
    ConsumePropDailyTask()
    {

        dailyTaskType = DailyTakeType.ConsumePropDailyTask;
        targerCount = 0;
     }
    public enum OprateType : int
    {
        Null,
        [Tooltip("使用任意一个道具")]
        ArbitrarilyUseProp,   //使用任意一个道具
        [Tooltip("使用快熟制作道具")]
        ArbitrarilyUseMakeProp,   //ArbitrarilyUseProp
        [Tooltip("使用不烧糊道具")]
        UseNoFire,              //使用不烧糊道具
        [Tooltip("使用耐心回复道具")]
        UsePatienceProp,   //使用耐心回复道具
        [Tooltip("使用增加顾客道具")]
        UseAddGuestProp,   //使用增加顾客道具
        [Tooltip("使用双倍金币道具")]
        UseDubleCion,   //使用双倍金币道具
        [Tooltip("使用自动上菜道具")]
        UseAutomaticSubmit,   //使用自动上菜道具
    }

    public OprateType oprateType = OprateType.Null;
    public override bool OpretaHandle()
    {
         taskValue = PlayerData.instance.dailyTaksValueDit;
        if (oprateType == OprateType.ArbitrarilyUseProp)
        {
            return ArbitrarilyUseProp();
        }
        else if (oprateType == OprateType.ArbitrarilyUseMakeProp)
        {
            return ArbitrarilyUseMakeProp();
        }
        else if (oprateType == OprateType.UseNoFire)
        {
            return UseNoFire();
        }
        else if (oprateType == OprateType.UsePatienceProp)
        {
            return UsePatienceProp();
        }
        else if (oprateType == OprateType.UseAddGuestProp)
        {
            return UseAddGuestProp();
        }
        else if (oprateType == OprateType.UseDubleCion)
        {
            return UseDubleCion();
        }
        else if (oprateType == OprateType.UseAutomaticSubmit)
        {
            return UseAutomaticSubmit();
        }
        return false;
    }
    private bool ArbitrarilyUseProp()
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.usePropCount;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
        return false;
    }
    private bool ArbitrarilyUseMakeProp()
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.useMomentFoodPropCount;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
        return false;
    }
    private bool UseNoFire()
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.useFireFoodPropCount;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
        return false;
    }
    private bool UsePatienceProp()
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.useGuestPatiencePropCount;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
        return false;
    }
    private bool UseAddGuestProp()
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.useAddGuestCount;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
        return false;
    }
    private bool UseDubleCion()
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.useSetlementDoubleCionPropCount;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
        return false;
    }
    private bool UseAutomaticSubmit()
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.useAutomaticSubmitFoodCount;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
        return false;
    }
}
    /// <summary>
    /// 关卡
    /// </summary>
    [Serializable]
public class LevelDailyTask : DailyTaskBase
{
    LevelDailyTask()
    {
        dailyTaskType = DailyTakeType.LevelDailyTask;
        targerCount = 0;
    }
    public enum OprateType : int
    {
        Null,
        [Tooltip("完成20个关卡")]
        Complete20Level,   //完成20个关卡
        [Tooltip("连续通过10个关卡")]
        ContunuComplete10Level,   //连续通过10个关卡
        [Tooltip("获得10个关卡钥匙")]
        Obtain10Key,   //获得10个关卡钥匙
    }
    public OprateType oprateType = OprateType.Null;             //完成目标数量
    public override bool OpretaHandle()
    {
        taskValue = PlayerData.instance.dailyTaksValueDit;
        if (oprateType == OprateType.Complete20Level)
        {
            return Complete20Level();
        }
        else if (oprateType == OprateType.ContunuComplete10Level)
        {
            return ContunuComplete10Level();
        }
        else if (oprateType == OprateType.Obtain10Key)
        {
            return Obtain10Key();
        }
        return false;
    }
    private bool Complete20Level()
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.compelet20LevelCount;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
        return false;
    }
    private bool ContunuComplete10Level()
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.continu10levelCount;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
        return false;
    }
    private bool Obtain10Key()
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.obtain10Key;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
        return false;
    }

}

/// <summary>
/// 货币类型
/// </summary>
[Serializable]
public class CurrencyDailyTask : DailyTaskBase
{
    CurrencyDailyTask()
    {
        dailyTaskType = DailyTakeType.CurrencyDailyTask;
        targerCount = 0;
    }

    public enum OprateType : int
    {
        Null,
        [Tooltip("获得2000金币")]
        Obtain2000Cion,   //获得2000金币
        [Tooltip("消耗2000金币")]
        Consume200Cion,   //消耗2000金币
        [Tooltip("获得20钻石")]
        Obtain20iamond,   //获得20钻石
        [Tooltip("消耗30钻石")]
        Consume30iamond,   //消耗30钻石
    }


    public OprateType oprateType = OprateType.Null;
    public override bool OpretaHandle()
    {
        taskValue = PlayerData.instance.dailyTaksValueDit;
        if (oprateType == OprateType.Obtain2000Cion)
        {
           return Obtain200Cion();
        }
        else if (oprateType == OprateType.Consume200Cion)
        {
            return Consume200Cion();
        }
        else if (oprateType == OprateType.Obtain20iamond)
        {
            return Obtain20iamond();
        }
        else if (oprateType == OprateType.Consume30iamond)
        {
            return Consume30iamond();
        }
        return false;
    }

    public bool Obtain200Cion()
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.obtainCion;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
        return false;
    }
    public bool Consume200Cion()
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.consumeCion;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
        return false;
    }
    public bool Obtain20iamond()
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.obtainDiamond;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
        return false;
    }
    public bool Consume30iamond()
    {
        dailyKey = PlayerData.instance.dailyTaskDataValue.consumeDiamond;
        currTargessCount = taskValue[dailyKey];
        if (currTargessCount >= targerCount)
            return true;
        return false;
    }
}