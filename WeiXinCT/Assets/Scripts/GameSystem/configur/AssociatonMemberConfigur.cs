using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName =" AssociatonMemberConfigur", menuName = "CanTing/AssociatonMemberConfigur")]
public class AssociatonMemberConfigur : ResourceProfile
{
    public List<AssociationMemberData> memberGiftList = new List<AssociationMemberData>();

    public int GetTotallDay()
    {
        int count = 0;
        for (int i=0; i< memberGiftList.Count; i++)
        {
            count += memberGiftList[i].spanDay;
        }
        return count;
    }

    public int GetLevelGrade()
    {
        return memberGiftList.Count;
    }
    
    public AssociationMemberData GetAssociationPower(int level)
    {
        if (level < 0 || level > memberGiftList.Count)
            return null;
        return memberGiftList[level];
    }
}


