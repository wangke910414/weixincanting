using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "CanTing/ShoppItemConfigur", fileName = "ShoppItemConfigur")]
public class ShoppItemConfigur : ResourceProfile
{
    public List<ShoppItemPropData> shopItemPropList = new List<ShoppItemPropData>();

    public List<ShoppItemGemData> shopItemGemList = new List<ShoppItemGemData>();
}
