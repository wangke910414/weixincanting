using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum AchieveTaksType
{
    Null,
    LevelAchieveTask,
    ContinuSubmitAchieveTask,
    UpGradationAchieveTask,
    PropAchieveTask,
    CurrencyAchieveTask,
    RestaurentAchieveTask
}

public class AchieveTaskBase
{
    public string acheieveName = string.Empty;
    public string acheveDescrible = string.Empty;
    public AchieveTaksType ahchieveTaskType = AchieveTaksType.Null;
    public int listIndexID = 0;
    public int tagerCount = 0;
   [HideInInspector]
    public int currCount = 0;
    public PrizeComponent prize = new PrizeComponent();

    public virtual bool  OperateHandle()
    {
        return false;
    }
    public virtual void SetCurrValue()
    {

    }

}
//关卡
[Serializable]
public class LevelAchieveTask : AchieveTaskBase
{
    public enum OperateType : int
    {
        Null,
        [Tooltip("总共通过关卡数")]
        TotalPassLevel,
        [Tooltip("总共服务人数")]
        TotalServriceGuest,
        [Tooltip("连续不烧糊食物")]
        ContinuNoFireFood,
        [Tooltip("连续不丢弃食物")]
        ContinuNoDidcardFood,
        [Tooltip("连续同一天通过关卡")]
        SameDayPassLevel,
        [Tooltip("连续同一关获得金币")]
        OneLeveObtainCion,
        [Tooltip("连续同一关服务顾客数目")]
        OneLevelServriceGuest,  
    }
    public OperateType operateType = OperateType.Null;
    public override bool OperateHandle()
    {
        ahchieveTaskType = AchieveTaksType.LevelAchieveTask;
        if (operateType == OperateType.TotalPassLevel)
        {
            return TotalPass100Level();
        }
        else if (operateType == OperateType.TotalServriceGuest)
        {
            return TotalServrice200Guest();
        }
        else if (operateType == OperateType.ContinuNoFireFood)
        {
            return Continu30LevelNoFireFood();
        }
        else if (operateType == OperateType.ContinuNoDidcardFood)
        {
            return Continu30LevelNoDidcardFoodeFood();
        }
        else if (operateType == OperateType.SameDayPassLevel)
        {
            return SameDayPass30Level();
           
        }
        else if (operateType == OperateType.OneLeveObtainCion)
        {
            return OneLeveObtain1000Cion();

        }
        else if (operateType == OperateType.OneLevelServriceGuest)
        {
            return OneLevelServrice30Guest();

        }
        return false;
    }

    public override void SetCurrValue()
    {
        if (operateType == OperateType.TotalPassLevel)
        {
            PlayerData.instance.achieveParmeter.passLevelCount = 0;
        }
        else if (operateType == OperateType.TotalServriceGuest)
        {
            PlayerData.instance.achieveParmeter.serveiceGuestCount = 0;
        }
        else if (operateType == OperateType.ContinuNoFireFood)
        {
            PlayerData.instance.achieveParmeter.contiunNoFireFoodCount = 0;
        }
        else if (operateType == OperateType.ContinuNoDidcardFood)
        {
            PlayerData.instance.achieveParmeter.continuDisCardFoodCount = 0;
        }
        else if (operateType == OperateType.SameDayPassLevel)
        {
            PlayerData.instance.achieveParmeter.sameDayLevelCount = 0;

        }
        else if (operateType == OperateType.OneLeveObtainCion)
        {
            PlayerData.instance.achieveParmeter.sameLeveinCionCount = 0;
        }
        else if (operateType == OperateType.OneLevelServriceGuest)
        {
            PlayerData.instance.achieveParmeter.sameLevelServiceGuestCount = 0;
        }

        base.SetCurrValue();
    }

    public bool TotalPass100Level()
    {
        currCount = PlayerData.instance.achieveParmeter.passLevelCount;
        //currCount = 100;
        if (currCount >= tagerCount)
            return true;

        return false;
    }
    public bool TotalServrice200Guest()
    {
        currCount = PlayerData.instance.achieveParmeter.serveiceGuestCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    public bool Continu30LevelNoFireFood()
    {
        currCount = PlayerData.instance.achieveParmeter.contiunNoFireFoodCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    public bool Continu30LevelNoDidcardFoodeFood()
    {
        currCount = PlayerData.instance.achieveParmeter.continuDisCardFoodCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    public bool SameDayPass30Level()
    {
        currCount = PlayerData.instance.achieveParmeter.sameDayLevelCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    public bool OneLeveObtain1000Cion()
    {
        currCount = PlayerData.instance.achieveParmeter.sameLeveinCionCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    public bool OneLevelServrice30Guest()
    {
        currCount = PlayerData.instance.achieveParmeter.sameLevelServiceGuestCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }

}

//连击OneLeveObtainCion,
[Serializable]
public class ContinuSubmitAchieveTask : AchieveTaskBase
{
    public enum OperateType : int
    {
        Null,
        Continu5Count,
        Continu4Count,
        Countinu3Count,
    }
    public OperateType operateType = OperateType.Null;
    public override bool OperateHandle()
    {
        ahchieveTaskType = AchieveTaksType.ContinuSubmitAchieveTask;
        if (operateType == OperateType.Continu5Count)
        {
            return Continu5Count();
        }
        else if (operateType == OperateType.Continu4Count)
        {

            return Continu4Count();

        }
        else if (operateType == OperateType.Countinu3Count)
        {
            return Countinu3Count();
        }
        return false;
    }

    public override void SetCurrValue()
    {
        if (operateType == OperateType.Continu5Count)
        {
            PlayerData.instance.achieveParmeter.fiveCombosCount = 0;
        }
        else if (operateType == OperateType.Continu4Count)
        {
            PlayerData.instance.achieveParmeter.fourCombosCount = 0;
        }
        else if (operateType == OperateType.Countinu3Count)
        {
            PlayerData.instance.achieveParmeter.threeCombosCount = 0;
        }
    }

        private bool Continu5Count()
    {
        currCount = PlayerData.instance.achieveParmeter.fiveCombosCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool Continu4Count()
    {
        currCount = PlayerData.instance.achieveParmeter.fourCombosCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool Countinu3Count()
    {
        currCount = PlayerData.instance.achieveParmeter.threeCombosCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
}
//升级
[Serializable]
public class UpGradationAchieveTask : AchieveTaskBase
{

    public enum OperateType : int
    {
        Null,
        SameRestaurantFoodUpGradetion, 
        SomeRestaurantDeiveceUpGradetion,
    }

    public OperateType operateType = OperateType.Null;
    public override bool OperateHandle()
    {
        ahchieveTaskType = AchieveTaksType.UpGradationAchieveTask;

        if (operateType == OperateType.SameRestaurantFoodUpGradetion)
        {
            return SameRestaurantFoodUpGradetion();
        }
        else if (operateType == OperateType.SomeRestaurantDeiveceUpGradetion)
        {
            return SomeRestaurantDeiveceUpGradetion();
        }
        return false;
    }

    public override void SetCurrValue()
    {
        if (operateType == OperateType.SameRestaurantFoodUpGradetion)
        {
            PlayerData.instance.achieveParmeter.SameRestaurantFoodUpGradeount = 0;
        }
        else if (operateType == OperateType.SomeRestaurantDeiveceUpGradetion)
        {
            PlayerData.instance.achieveParmeter.SomeRestaurantDeiveceUpGradeCount = 0;
        }
    }
    private bool SameRestaurantFoodUpGradetion()
    {
        currCount = PlayerData.instance.achieveParmeter.SameRestaurantFoodUpGradeount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool SomeRestaurantDeiveceUpGradetion()
    {
        currCount = PlayerData.instance.achieveParmeter.SomeRestaurantDeiveceUpGradeCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
}

//道具
[Serializable]
public class PropAchieveTask : AchieveTaskBase
{
    public enum OperateType : int
    {
        Null,
        [Tooltip("使用任意一个道具")]
        ArbitrarilyUseProp,   //使用任意一个道具
        [Tooltip("使用快熟制作道具")]
        UseFastMakeProp,   //ArbitrarilyUseProp
        [Tooltip("使用不烧糊道具")]
        UseNoFire,              //使用不烧糊道具
        [Tooltip("使用耐心回复道具")]
        UsePatienceProp,   //使用耐心回复道具
        [Tooltip("使用增加顾客道具")]
        UseAddGuestProp,   //使用增加顾客道具
        [Tooltip("使用双倍金币道具")]
        UseDubleCion,   //使用双倍金币道具
        [Tooltip("使用自动上菜道具")]
        UseAutomaticSubmit,   //使用自动上菜道具
    }
    public OperateType oprateType = OperateType.Null;

    public override bool OperateHandle()
    {
        ahchieveTaskType = AchieveTaksType.PropAchieveTask;

        if (oprateType == OperateType.ArbitrarilyUseProp)
        {
            return ArbitrarilyUseProp();
        }
        else if (oprateType == OperateType.UseFastMakeProp)
        {
            return UseFastMakeProp();
        }
        else if (oprateType == OperateType.UseNoFire)
        {
            return UseNoFire();
        }
        else if (oprateType == OperateType.UsePatienceProp)
        {
            return UsePatienceProp();
        }
        else if (oprateType == OperateType.UseAddGuestProp)
        {
            return UseAddGuestProp();
        }
        else if (oprateType == OperateType.UseDubleCion)
        {
            return UseDubleCion();
        }
        else if (oprateType == OperateType.UseAutomaticSubmit)
        {
            return UseAutomaticSubmit();
        }

        return base.OperateHandle();
    }

    public override void SetCurrValue()
    {

        if (oprateType == OperateType.ArbitrarilyUseProp)
        {
            PlayerData.instance.achieveParmeter.usePropCount = 0;
        }
        else if (oprateType == OperateType.UseFastMakeProp)
        {
            PlayerData.instance.achieveParmeter.useFastMakePropCount = 0;
        }
        else if (oprateType == OperateType.UseNoFire)
        {
            PlayerData.instance.achieveParmeter.useNoFirePropCount = 0;
        }
        else if (oprateType == OperateType.UsePatienceProp)
        {
            PlayerData.instance.achieveParmeter.usePatiencePropCount = 0;
        }
        else if (oprateType == OperateType.UseAddGuestProp)
        {
            PlayerData.instance.achieveParmeter.useAddGuestPropCount = 0;
        }
        else if (oprateType == OperateType.UseDubleCion)
        {
            PlayerData.instance.achieveParmeter.useDubleCionPropCount = 0;
        }
        else if (oprateType == OperateType.UseAutomaticSubmit)
        {
            PlayerData.instance.achieveParmeter.useAutomaticSumbitPropCount = 0;
        }
    }
    private bool ArbitrarilyUseProp()
    {
        currCount = PlayerData.instance.achieveParmeter.usePropCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool UseFastMakeProp()
    {
        currCount = PlayerData.instance.achieveParmeter.useFastMakePropCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool UseNoFire()
    {
        currCount = PlayerData.instance.achieveParmeter.useNoFirePropCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool UsePatienceProp()
    {
        currCount = PlayerData.instance.achieveParmeter.usePatiencePropCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool UseAddGuestProp()
    {
        currCount = PlayerData.instance.achieveParmeter.useAddGuestPropCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool UseDubleCion()
    {
        currCount = PlayerData.instance.achieveParmeter.useDubleCionPropCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool UseAutomaticSubmit()
    {
        currCount = PlayerData.instance.achieveParmeter.useAutomaticSumbitPropCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
}

//货币
[Serializable]
public class CurrencyAchieveTask : AchieveTaskBase
{
    public enum OperateType : int
    {
        Null,
        [Tooltip("获得金币")]
        ObtainCion,   
        [Tooltip("消耗2金币")]
        ConsumeCion,  
        [Tooltip("获得钻石")]
        ObtainDiamond,  
        [Tooltip("消耗钻石")]
        ConsumeDiamond,
        [Tooltip("同一天只获得不消耗")]
        SameDayObtainCionNoConsume,
        [Tooltip("获得钥匙")]
        TotalObtainKey
    }
    public OperateType oprateType = OperateType.Null;
    public override bool OperateHandle()
    {
        ahchieveTaskType = AchieveTaksType.CurrencyAchieveTask;
        if (oprateType == OperateType.ObtainCion)
        {
            return ObtainCion();
        }
        else if (oprateType == OperateType.ConsumeCion)
        {
            return ConsumeCion();
        }
        else if (oprateType == OperateType.ObtainDiamond)
        {
            return Obtainiamond();
        }
        else if (oprateType == OperateType.ConsumeDiamond)
        {
            return Consumeiamond();
        }
        else if (oprateType == OperateType.SameDayObtainCionNoConsume)
        {
            return SameDayObtainCionNoConsume();
        }
        else if (oprateType == OperateType.TotalObtainKey)
        {
            return TotalObtainKey();
        }
        return base.OperateHandle();
    }
    public override void SetCurrValue()
    {
        if (oprateType == OperateType.ObtainCion)
        {
            PlayerData.instance.achieveParmeter.obtainCionCount = 0;
        }
        else if (oprateType == OperateType.ConsumeCion)
        {
            PlayerData.instance.achieveParmeter.comsumeCionCount = 0;
        }
        else if (oprateType == OperateType.ObtainDiamond)
        {
            PlayerData.instance.achieveParmeter.obtanDiamondCount = 0;
        }
        else if (oprateType == OperateType.ConsumeDiamond)
        {
            PlayerData.instance.achieveParmeter.comsumeDiamondCount = 0;
        }
        else if (oprateType == OperateType.SameDayObtainCionNoConsume)
        {
            PlayerData.instance.achieveParmeter.sameDayObtainCionNoComsumeCount = 0;
        }
        else if (oprateType == OperateType.TotalObtainKey)
        {
            PlayerData.instance.achieveParmeter.obtainKeyCount = 0;
        }
    }
    private bool ObtainCion()
    {
        currCount = PlayerData.instance.achieveParmeter.obtainCionCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool ConsumeCion()
    {
        currCount = PlayerData.instance.achieveParmeter.comsumeCionCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool Obtainiamond()
    {
        currCount = PlayerData.instance.achieveParmeter.obtanDiamondCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool Consumeiamond()
    {
        currCount = PlayerData.instance.achieveParmeter.comsumeDiamondCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool SameDayObtainCionNoConsume()
    {
        currCount = PlayerData.instance.achieveParmeter.sameDayObtainCionNoComsumeCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool TotalObtainKey()
    {
        currCount = PlayerData.instance.achieveParmeter.obtainKeyCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
}

//餐厅
[Serializable]
public class RestaurentAchieveTask : AchieveTaskBase
{
    public enum OperateType : int
    {
        Null,
      AllLevelOneStar,
        AllLevelTweStar,
        AllLevelThreeStar,
        TotalSubmitFoodCount,
        TotalSubmitDrinkCount,
        GoodComment,
        TotalOtherFoodCount,
    }
    public OperateType oprateType = OperateType.Null;
    public override bool OperateHandle()
    {
        ahchieveTaskType = AchieveTaksType.RestaurentAchieveTask;

        if (oprateType == OperateType.AllLevelOneStar)
        {
            return AllLevelOneStar();
        }
        else if (oprateType == OperateType.AllLevelTweStar)
        {
            return AllLevelTweStar();
        }
        else if (oprateType == OperateType.AllLevelThreeStar)
        {
            return AllLevelThreeStar();
        }
        else if (oprateType == OperateType.TotalSubmitFoodCount)
        {
            return TotalSubmitFoodCount();
        }
     else if (oprateType == OperateType.TotalSubmitDrinkCount)
        {
            return TotalSubmitDrinkCount();
        }
        else if (oprateType == OperateType.GoodComment)
        {
            return GoodComment();
        }
        else if (oprateType == OperateType.TotalOtherFoodCount)
        {
            return TotalOtherFoodCount();
        }
        return base.OperateHandle();
    }
    public override void SetCurrValue()
    {

        if (oprateType == OperateType.AllLevelOneStar)
        {
            PlayerData.instance.achieveParmeter.AllLevelOneStarCount = 0;
        }
        else if (oprateType == OperateType.AllLevelTweStar)
        {
            PlayerData.instance.achieveParmeter.AllLevelTweStarCount = 0;
        }
        else if (oprateType == OperateType.AllLevelThreeStar)
        {
            PlayerData.instance.achieveParmeter.AllLevelThreeStarCount = 0;
        }
        else if (oprateType == OperateType.TotalSubmitFoodCount)
        {
            PlayerData.instance.achieveParmeter.totalSubmitFoodCount = 0;
        }
        else if (oprateType == OperateType.TotalSubmitDrinkCount)
        {
            PlayerData.instance.achieveParmeter.totalSubmitDrinkCount = 0;
        }
        else if (oprateType == OperateType.GoodComment)
        {
            PlayerData.instance.achieveParmeter.goodCommentCount = 0;
        }
        else if (oprateType == OperateType.TotalOtherFoodCount)
        {
            PlayerData.instance.achieveParmeter.otherFoodCount = 0;
        }
       base.SetCurrValue();
    }


    private bool AllLevelOneStar()
    {
        currCount = PlayerData.instance.achieveParmeter.AllLevelOneStarCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool AllLevelTweStar()
    {
        currCount = PlayerData.instance.achieveParmeter.AllLevelTweStarCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool AllLevelThreeStar()
    {
        currCount = PlayerData.instance.achieveParmeter.AllLevelThreeStarCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool TotalSubmitFoodCount()
    {
        currCount = PlayerData.instance.achieveParmeter.totalSubmitFoodCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool TotalSubmitDrinkCount()
    {
        currCount = PlayerData.instance.achieveParmeter.totalSubmitDrinkCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool GoodComment()
    {
        currCount = PlayerData.instance.achieveParmeter.goodCommentCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
    private bool TotalOtherFoodCount()
    {
        currCount = PlayerData.instance.achieveParmeter.otherFoodCount;
        if (currCount >= tagerCount)
            return true;
        return false;
    }
}