using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class AssociationMemberData 
{
    public string menberGradeID;
    public int grade;
    public int energy;                                      //体力
    public int spanDay;                                      //跨度天数
    public int skipAdbNoCount;                           //跳过广告天数
    public float cionMuitipl;                           //结算金币倍数
    public float propConsumeNoCount;                     //道具消耗不计天数
    public int unLockGuest;                             //解锁特顾客
    public AssociationMemberGiftPack giftPack;             //礼包
    public string illustrateText;                          //说明

}

[Serializable]
public class AssociationMemberGiftPack
{
    public int diamondNumber;                   //钻石
    public int cionNumber;                      //金币
    [Header("礼包道具数目")]
    public List<GiftPropPack> propGiftPakc;     //道具包
}

[Serializable]
public class GiftPropPack
{
    [Header("道具类型")]
    public ConsumePropType propType;
    [Header("道具个数")]
    public int giveCount;
}