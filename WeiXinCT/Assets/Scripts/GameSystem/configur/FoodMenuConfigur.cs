using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(fileName = "FoodMenuAsset", menuName = "CanTing / FoodMenuAsset") ]
public class FoodMenuConfigur : ResourceProfile
{
    public List<FoodMenuAsset> foodMenuAssetList = new List< FoodMenuAsset>();
}
