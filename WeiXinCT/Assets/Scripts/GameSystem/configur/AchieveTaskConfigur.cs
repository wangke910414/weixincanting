using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "CanTing/AchieveTaskConfigur", fileName = "AchieveTaskConfigur")]
public class AchieveTaskConfigur : ResourceProfile
{
    [Header("关卡成就")]
    public List<LevelAchieveTask> levelAchievelList = new List<LevelAchieveTask>();
    [Header("连击成就")]
    public List<ContinuSubmitAchieveTask > continuSubmitAchievelList = new List<ContinuSubmitAchieveTask>();
    [Header("升级成就")]
    public List<UpGradationAchieveTask> upGradationAchievelList = new List<UpGradationAchieveTask>();
    
    [Header("道具成就")]
    public List<PropAchieveTask> PropAchievelList = new List<PropAchieveTask>();
    [Header("货币成就")]
    public List<CurrencyAchieveTask> currentcyAchievelList = new List<CurrencyAchieveTask>();
    [Header("餐厅成就")]
    public List<RestaurentAchieveTask> restaurentAchievelList = new List<RestaurentAchieveTask>();
}
