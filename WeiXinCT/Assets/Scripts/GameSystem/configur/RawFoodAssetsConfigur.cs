using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu (menuName = "CanTing/RawFoodAssets" ,fileName = "RawFoodAsset")]
public class RawFoodAssetsConfigur : ResourceProfile
{

    public List<RawFoodData> rawFoodDataList = new List<RawFoodData>();
}
