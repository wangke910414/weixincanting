using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
//using UnityEngine.Purchasing;
using UnityEngine.SceneManagement;

public class SystemGameManager : MonoBehaviour
{
    [SerializeField]
    private SystmConfigurationManager systmConfigurationManager;

    [SerializeField]
    private UiManager uiManager;

    [SerializeField]
    private LevelManager levelManager;

    //[SerializeField]
    //private SystemDataFactoy systemDataFactoy;

   // [SerializeField]
    //private LoadSaveFileManager loadSaveFileManager;

    [SerializeField]
    private OperationManage operationManage;

    [SerializeField]
    private GuestManager guestManager;

    private GameStat gameStat = GameStat.GameStart;



    public SystmConfigurationManager SystmConfigurationManager { get => systmConfigurationManager; }
    public GameStat GameStat { get => gameStat; set => gameStat = value; }

    public LevelManager LevelManager { get => levelManager; set => levelManager = value; }

   /// public SystemDataFactoy SystemDataFactoy { get => systemDataFactoy; }

    public OperationManage OperationManage { get => operationManage; }

    public GuestManager GuestManager { get => guestManager; }

    public GuestManager mGuestManager { get => guestManager; }


    public UiManager UiManager { get => uiManager; }
    // Start is called before the first frame update
    void Start()
    {
        systmConfigurationManager = SystmConfigurationManager.instance;
        Init();
    }
    private void Init()
    {
 
        this.GameStat = GameStat.GameStart;
       SystemEventManager.Clear();
        PlayerData.instance.getCion = 0;
        PlayerData.instance.currSingleLevelData.CleraData();

        StartCoroutine(LoadData());
    }

    private IEnumerator Check()
    {
        while (true)
        {
            if (SystemDataFactoy.instans.YooAssetsLoadCompelet)
            {
               // Debug.Log("等待资源获取！");
                 yield break;
            }

   
        }
    }
    private IEnumerator LoadData()
    {
        yield return Check();
        levelManager.Configur(this);
        operationManage.Configuer(this);
         guestManager.Configuer(this);
        uiManager.Configuer(this);
        ////后期开启
        //Scene scene = SceneManager.GetSceneByName("Start");
        //if (scene != null)
        //{
        //    SceneManager.UnloadSceneAsync(scene);
        //}
    }
    //游戏结束切换结束界面
    public void ChangeLevel()
    {
        if (gameStat != GameStat.GameEnd)
        {
  
            UnityEngine.SceneManagement.SceneManager.LoadScene("End");
            gameStat = GameStat.GameEnd;
        }
    }



    private IEnumerator LoadingLevel()
    {
        yield return new WaitForSeconds(1f);
        uiManager.ChangeLevelEffice();
    }
    public void ContinueGame()
    {
        CoroutineHandler.StartStaticCoroutine(ResetManger());
    }
    private IEnumerator ResetManger()
    {

       yield return levelManager.RestManager();
       yield return operationManage.ResetManager();
       yield return guestManager.ResetManager();
         uiManager.OpenTargetDlg();
    }
    public void SetGameRun()
    {
        gameStat = GameStat.GameRun;
        guestManager.RunGuestManager();
    }
    public void GameEnd()
    {
        gameStat = GameStat.GameEnd;
        guestManager.GuestEnd();
        LoadingLevel();
    }

    public async void SaveCloundData()
    {
        CloundSavePlayerDaata cloundData = new CloundSavePlayerDaata();
        cloundData.cion = PlayerData.playerDataParame.cions;
        cloundData.gem = PlayerData.playerDataParame.diamond;
        await CloudSaveSystem.instans.ForceSaveObjectData("CloundSavePlayerDaata", cloundData);
       
    }

    public void Update()
    {
        if (gameStat == GameStat.GameRun)
        {
            PlayerData.instance.currSingleLevelData.playTime += Time.deltaTime;
        }
    }

    
}

