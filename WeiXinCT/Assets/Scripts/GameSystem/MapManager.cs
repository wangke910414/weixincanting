using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    private StartHomeManager systemGameManager;

    [SerializeField]
    private Transform mapPatent;

    [SerializeField]
    private List<UiMapPlane> mapList = new List<UiMapPlane>();

    public List<UiMapPlane> MapList { get => mapList; }

    private UiMapPlane currMap;

    public UiMapPlane CurrMap { get => currMap; set => currMap = value; }
    public void Configur(StartHomeManager systemGameManager)
    {
        this.systemGameManager = systemGameManager;
        Init();
    }

    private void Init()
    {
        
    }
      
    public void MoveMap()
    {
      
    }
    public void DisPlayMiniMap()
    {

    }
}
