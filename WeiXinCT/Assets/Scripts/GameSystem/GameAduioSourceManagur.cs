using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAduioSourceManagur : MonoBehaviour
{

    [SerializeField]
    private AudioSource bgSource;
    // Start is called before the first frame update
    void Start()
    {
        CoroutineHandler.StartStaticCoroutine(PlayBg());
    }

    public IEnumerator PlayBg()
    {
        yield return new WaitForSeconds(3f);
        bgSource.Play();
    }
}
