using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
//
public class GuestNeedDlg : MonoBehaviour
{
    public Image waitTiemImg;
    public Image[] isCompelet;
    public Transform[] needFoodParent;
    public Transform[] plateParent;
    public Image [] drinkCup;
    public Transform[] needOneFood;
    public Transform[] needTweFood;
    public Transform[] needThreeFood;
    [SerializeField]
    private CanvasGroup redSideImg;

    private bool timeSideRed;


    private List<FoodMenu> foodMendList = new List<FoodMenu>();
    private int foodCount = 0;


    public void AddNedFood(FoodMenu food)
    {
        foodMendList.Add(food);
        MakeMainFood(food);
    }


    private void MakeMainFood(FoodMenu foodItem)
    {
        Debug.Log("顾客需要食物   " + foodItem.needMainFood);
            string path = "Assets/Prefabs/Food/needFood/Need"+ foodItem.needMainFood + ".prefab";
        var gustPrefab = SystemDataFactoy.instans.GetYooAssetHandle(path);
        var food = gustPrefab.InstantiateSync();
        food.SetActive(true);
       food.transform.SetParent(needFoodParent[foodCount]);
       food.transform.localPosition = Vector3.zero;
        food.transform.localScale = Vector3.one;
        foodCount++;
        var foodCompnent = food.GetComponent<NeedFoodComponent>();
        for (int i=0; i< foodItem.needOtherFood.Length; i++)
        {
            if (foodItem.needOtherFood[i] == "null")
                continue;
            foodCompnent.SetFoodShow(foodItem.needOtherFood[i]);
        }
    }

    private IEnumerator MakeDrink(string foodName)
    {
        string path = "Assets/Prefabs/Food/needFood/steak.prefab";
        var gustPrefab = YooResoursceManagur.Instace.GetPackage().LoadAssetAsync<GameObject>(path);
        yield return gustPrefab;
        var food = gustPrefab.InstantiateSync();
        food.transform.SetParent(needFoodParent[foodCount]);
        foodCount++;
    }
    public Transform GetNeedFoodParent(int index)
    {
        if (index > needFoodParent.Length  || index < 0)
            return null;
        return needFoodParent[index];
    }
    public bool CompleteNeed()
    {
        int tempCount = 0;
        for (int i=0; i< isCompelet.Length; i++)
        {
            if (isCompelet[i].gameObject.activeSelf)
                tempCount++;
        }
        if (tempCount == foodCount)
            return true;
        return false;
    }
    public void SetDlgSideRed()
    {
        if (!timeSideRed)
        {
            redSideImg.gameObject.SetActive(true);
            redSideImg.DOFade(0, 0.4f).SetEase(Ease.Flash).SetLoops(-1,LoopType.Yoyo).SetUpdate(true);
            ShakeAnimation();
        }
        timeSideRed = true;

    }
    public void ShakeAnimation()
    {
    transform.DOShakePosition(1,new Vector3(0,5,0)).SetEase(Ease.Flash);
    }
}
