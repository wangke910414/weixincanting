using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// �˿͹���
/// </summary>
public class GuestManager : MonoBehaviour
{

    private List<GameObject> guestList = new List<GameObject>();

    private float guestTime = 0f;

    [SerializeField]
    private Transform guestParent;

    private GuestParentInfo guestParentInfo;

    private int GuestCompelet;          //产生多少个顾客
    private int guestLeaveCount;             //离开多少顾客

    private Transform orignTransform;

    private int guestCount;
    private int addGuestCount;

    //private bool gameEnd = true;
    private bool isWin = false;
  //  private bool isNewAddGuest = false;
    private bool tiemTraget = false;            //时间限制

    private GuestTable guestTable;

    private SystemGameManager systemGameManager;
    public void Configuer(SystemGameManager systemGameManager)
    {
        this.systemGameManager = systemGameManager;
        guestParentInfo = guestParent.GetComponent<GuestParentInfo>();
        Init();
    }

    private void Init()
    {
        guestTime = systemGameManager.SystmConfigurationManager.guestTiem;
        SystemEventManager.StartListening("GuestReceiveFood", CallGuestReceiveFood);
        SystemEventManager.StartListening("GuestAddDrink", CallAddDrink);
        orignTransform = guestParent.transform.parent;
    }
    private void OnDisable()
    {
        SystemEventManager.StopListening("GuestReceiveFood", CallGuestReceiveFood);
    }

    public IEnumerator ResetManager()
    {
        GuestCompelet = 0;
        guestList.Clear();
        SpawnGuest();
        guestParentInfo.ResetGuestParentInfo();
        // StateGuestCome();
        yield return null;
    }

    public int GetCurrLevelGuestCount()
    {
        return guestCount;
    }

    public void RunGuestManager()
    {
        StartCoroutine(StateGuestComeOne());
    }

    public void GuestEnd()
    {
        guestParent.transform.SetParent(orignTransform);
    }

    private void SpawnGuest()
    {

        var rest = systemGameManager.LevelManager.CurrRestaurantInfo;
        guestParent.transform.SetParent(rest.guestPrant);
        guestParent.transform.localPosition = Vector3.zero;
        // RangeGustNeedSpawn();
        ConfigurGustNeedSpawn();
    }

    private void SetGuestMenu(int index, FoodMenu food)
    {
        Debug.Log(guestList.Count + "    "+ index);
        guestList[index].GetComponent<Guest>().SetFoodMenu(food);
    }

    private void ConfigurGustNeedSpawn()
    {

        var gustPrefab = SystemDataFactoy.instans.GetYooAssetHandle("Assets/Prefabs/Guest/Guest_1.prefab"); 
        var rest = systemGameManager.LevelManager.CurrRestaurantInfo;
        var levelData = systemGameManager.LevelManager.GetCurrLevelData();
        guestParentInfo.MaxChairCount = levelData.chairCount;
       
        var temp = (PlayerData.instance.currLevelPlayOnce + 1) > 3 ? 3 : (PlayerData.instance.currLevelPlayOnce + 1);

        string fileName = PlayerData.instance.curPlayeShop + "_Guest_" + temp;
        Debug.Log("guestManager   " + fileName);
        if (!LoadConfigursManager.instans.ShopGuestDit.ContainsKey(fileName))
        {
            return;
        }
        var shopGuest = LoadConfigursManager.instans.ShopGuestDit[fileName];
        if (!shopGuest.ContainsKey(levelData.levelGuestTablle))
        {
            return;
        }
        var gustConfigur = shopGuest[levelData.levelGuestTablle];
        GuestTable guestTable = shopGuest[levelData.levelGuestTablle];

        var foodMenuDit = LoadConfigursManager.instans.ShopFoodMenuDict[PlayerData.instance.curPlayeShop];

        //限制设置
        if (PlayerData.instance.limitType == LimitType.GustCountLimit)
        {
            guestCount = levelData.gustCount;
        }
        else if (PlayerData.instance.limitType == LimitType.TimeLimit)
        {
            guestCount = gustConfigur.guestDataList.Count;
        }

        for (int i = 0; i < guestCount; i++)
        {
            if (i >= gustConfigur.guestDataList.Count)
                break;
            Debug.Log("创建");
            var gustData = gustConfigur.guestDataList[i];
            var guest = gustPrefab.InstantiateSync();
            var guestCompnet = guest.GetComponent<Guest>();
            var randPos = Random.Range(0, 2);
            if (randPos == 0)
            {
                guest.transform.SetParent(guestParentInfo.guestStartPos1);
                guest.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
                guestCompnet.SetBalckTrager(guestParentInfo.guestStartPos2);
            }
            else
            {
                guest.transform.SetParent(guestParentInfo.guestStartPos2);
                guest.transform.localScale = new Vector3(-0.7f, 0.7f, 0.7f);
                guestCompnet.SetBalckTrager(guestParentInfo.guestStartPos1);
            }

            guest.transform.localPosition = Vector3.zero;
            guest.transform.localRotation = Quaternion.identity;

            guestCompnet.Configur(this);
            guest.SetActive(false);
            guestCompnet.fromTime = Random.Range(0.5f, 1f);
           // ConsumePropAction();
            guestCompnet.GuestRest();
            //guestCompnet.AddWaitTime(addWaitTime);

            guestList.Add(guest);
            for (int f = 0; f < gustData.needFood.Length; f++)
            {
                var foodStr = gustData.needFood[f];
                if (foodStr != "null")
                {
                    if (foodMenuDit.ContainsKey(foodStr))
                    {
                        var food = foodMenuDit[foodStr];
                        SetGuestMenu(i, food);
                       // Debug.Log(" 增加食物名字  " + food.foodName);
                    }

                }

            }

        }
    }

    /// <summary>
    /// 时间限制增加顾客
    /// </summary>
    private void TiemLimitAddGuest()
    {

        var gustPrefab = SystemDataFactoy.instans.GetYooAssetHandle("Assets/Prefabs/Guest/Guest_1.prefab"); ;
        var rest = systemGameManager.LevelManager.CurrRestaurantInfo;
        var levelData = systemGameManager.LevelManager.GetCurrLevelData();
        guestParentInfo.MaxChairCount = levelData.chairCount;
        string fileName = PlayerData.instance.curPlayeShop + "_Guest_1";
        if (!LoadConfigursManager.instans.ShopGuestDit.ContainsKey(fileName))
        {
            return;
        }
        var shopGuest = LoadConfigursManager.instans.ShopGuestDit[fileName];
        if (!shopGuest.ContainsKey(levelData.levelGuestTablle))
        {
            return;
        }
        var gustConfigur = shopGuest[levelData.levelGuestTablle];
        GuestTable guestTable = shopGuest[levelData.levelGuestTablle];
        var foodMenuDit = LoadConfigursManager.instans.ShopFoodMenuDict[PlayerData.instance.curPlayeShop];
      
        for (int i = 0; i < gustConfigur.guestDataList.Count; i++)
        {
            if (i >= gustConfigur.guestDataList.Count)
                break;
            var gustData = gustConfigur.guestDataList[i];
            var guest = gustPrefab.InstantiateSync();
            var guestCompnet = guest.GetComponent<Guest>();
            var randPos = Random.Range(0, 2);
            if (randPos == 0)
            {
                guest.transform.SetParent(guestParentInfo.guestStartPos1);
                guest.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
                guestCompnet.SetBalckTrager(guestParentInfo.guestStartPos2);
            }
            else
            {
                guest.transform.SetParent(guestParentInfo.guestStartPos2);
                guest.transform.localScale = new Vector3(-0.7f, 0.7f, 0.7f);
                guestCompnet.SetBalckTrager(guestParentInfo.guestStartPos1);
            }

            guest.transform.localPosition = Vector3.zero;
            guest.transform.localRotation = Quaternion.identity;

            guestCompnet.Configur(this);
            guest.SetActive(false);
            guestCompnet.fromTime = Random.Range(0.5f, 1f);
            //ConsumePropAction();
            guestCompnet.GuestRest();
            //guestCompnet.AddWaitTime(addWaitTime);

            guestList.Add(guest);
         }

        var addIndex = GuestCompelet;
        for (int i = 0; i < gustConfigur.guestDataList.Count; i++)
        {
            if (i >= gustConfigur.guestDataList.Count)
                break;

            var gustData = gustConfigur.guestDataList[i];
            for (int f = 0; f < gustData.needFood.Length; f++)
            {
                var foodStr = gustData.needFood[f];
                if (foodStr != "null")
                {
                    if (foodMenuDit.ContainsKey(foodStr))
                    {
                        var food = foodMenuDit[foodStr];
                        SetGuestMenu(addIndex, food);
                    }
                }

            }
            addIndex++;

        }
    }

    private void RangeGustNeedSpawn()
    {
        //Assets/Prefabs/Guest/Guest_1.prefab
        var rest = systemGameManager.LevelManager.CurrRestaurantInfo;
        var levelData = systemGameManager.LevelManager.GetCurrLevelData();

        GuestConfigur guestAsset = SystemDataFactoy.instans.GetResourceConfigur("GuestAsset") as GuestConfigur;
        var foodMenuDit = LoadConfigursManager.instans.ShopFoodMenuDict[PlayerData.instance.curPlayeShop];

        List<FoodMenu> FoodMenuClassA = new List<FoodMenu>();
        List<FoodMenu> FoodMenuClassB = new List<FoodMenu>();
        List<FoodMenu> FoodMenuClassC = new List<FoodMenu>();

        foreach (var item in foodMenuDit)
        {
            if (item.Value.classNum == "classA")
            {
                if (foodMenuLock(item.Value))
                    FoodMenuClassA.Add(item.Value);
            }
            if (item.Value.classNum == "classB")
            {
                if (foodMenuLock(item.Value))
                    FoodMenuClassB.Add(item.Value);
            }
            if (item.Value.classNum == "classC")
            {
                if (foodMenuLock(item.Value))
                    FoodMenuClassC.Add(item.Value);
            }
        }

        guestCount = levelData.gustCount;
        for (int i = 0; i < levelData.gustCount; i++)
        {
            var guestHandle = SystemDataFactoy.instans.GetYooAssetHandle("Assets/Prefabs/Guest/Guest_1.prefab");
            var guest = guestHandle.InstantiateSync();
            var guestCompnet = guest.GetComponent<Guest>();
            var randPos = Random.Range(0, 2);
            if (randPos == 0)
            {
                guest.transform.SetParent(guestParentInfo.guestStartPos1);
                guest.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
                guestCompnet.SetBalckTrager(guestParentInfo.guestStartPos2);
            }
            else
            {
                guest.transform.SetParent(guestParentInfo.guestStartPos2);
                guest.transform.localScale = new Vector3(-0.7f, 0.7f, 0.7f);
                guestCompnet.SetBalckTrager(guestParentInfo.guestStartPos1);
            }

            guest.transform.localPosition = Vector3.zero;
            guest.transform.localRotation = Quaternion.identity;

            guestCompnet.Configur(this);
            guest.SetActive(false);
            guestCompnet.fromTime = Random.Range(1f, 2f);
            guestList.Add(guest);


            var randNumber = Random.Range(1, 4);
            guestCompnet.NeedFoddCount = randNumber;
            if (randNumber == 3)
            {

                for (int j = 0; j < randNumber; j++)
                {
                    int randtemp = Random.Range(0, 100); ;
                    FoodMenu food = new FoodMenu();


                    if (randtemp < 50)
                    {
                        //一级菜
                        if (FoodMenuClassA.Count > 0)
                        {
                            int rf = Random.Range(0, FoodMenuClassA.Count);
                            food = FoodMenuClassA[rf];
                            Debug.Log(" FoodMenuClassA  " + food.foodName);
                            SetGuestMenu(i, food);
                        }

                    }
                    else if (randtemp >= 50 && randtemp <= 90)
                    {
                        //二级
                        if (FoodMenuClassB.Count > 0)
                        {
                            int rf = Random.Range(0, FoodMenuClassB.Count);
                            food = FoodMenuClassB[rf];
                            Debug.Log(" FoodMenuClassB  " + food.foodName);
                            SetGuestMenu(i, food);
                        }
                    }
                    else
                    {
                        //三级
                        if (FoodMenuClassC.Count > 0)
                        {
                            int rf = Random.Range(0, FoodMenuClassC.Count);
                            food = FoodMenuClassC[rf];
                            Debug.Log(" FoodMenuClassC  " + food.foodName);
                            SetGuestMenu(i, food);
                        }
                    }

                }

            }
            else
            {
                for (int j = 0; j < randNumber; j++)
                {
                    var rand = Random.Range(0, 100);
                    FoodMenu food = new FoodMenu();
                    if (rand < 60)
                    {
                        if (FoodMenuClassA.Count > 0)
                        {
                            var rf = Random.Range(0, FoodMenuClassA.Count);
                            food = FoodMenuClassA[rf];
                        }

                    }
                    else
                    {
                        if (FoodMenuClassB.Count > 0)
                        {
                            var rf = Random.Range(0, FoodMenuClassB.Count);
                            food = FoodMenuClassB[rf];
                        }
                    }
                    SetGuestMenu(i, food);
                }
            }
        }
    }

    ////恢复贵客耐心
    //private void ConsumePropAction()
    //{
    //    for (int i = 0; i < PlayerData.instance.currSelectPropList.Count; i++)
    //    {
    //        var prop = PlayerData.instance.currSelectPropList[i];
    //        if (prop == ConsumePropType.GuestPatience)
    //        {
    //            //addWaitTime = 10;
    //        }
    //    }
    //}

    public void GameStopPropAction()
    {

        for (int i = 0; i < PlayerData.instance.currSelectPropList.Count; i++)
        {
            var prop = PlayerData.instance.currSelectPropList[i];
            if (prop == ConsumePropType.GuestPatience)
            {

                for (int j = 0; j < guestList.Count; j++)
                {
                    guestList[j].GetComponent<Guest>().AddWaitTime(0);
                }
            }
        }

    }
    private bool foodMenuLock(FoodMenu food)
    {
        LevelData levelData = systemGameManager.LevelManager.GetCurrLevelData();

        foreach (string mainfood in levelData.mainFood)
        {
            if (mainfood == food.needMainFood && mainfood != "null")
            {
                int nullCount = 0;
                foreach (string foodother in food.needOtherFood)
                {
                    foreach (string levelOteer in levelData.otherFood)
                    {
                        if (foodother == levelOteer)
                        {
                            nullCount++;
                            break;
                        }
                    }

                }
                if (nullCount >= food.needOtherFood.Length)
                {
                    return true;
                }
            }
            if (food.foodName == levelData.drinke)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// 顾客离开
    /// </summary>
    /// <param name="charID"></param>
    public void OneceMoreStateGuest(Guest guest)
    {
        guestParentInfo.SetHaveGuest(guest.ChairID, false);
        ///Debug.Log( "离开椅子号   " + guest.ChairID);
        if (guest.GuestMoodState == GuestMoodState.GoodComment)
        {
            string strName = PlayerData.instance.dailyTaskDataValue.goodComment;
            PlayerData.instance.dailyTaksValueDit[strName]++;
            PlayerData.instance.currSingleLevelData.goodCommendNumber++;
        }
        else if (guest.GuestMoodState == GuestMoodState.BadComent)
        {
            if (PlayerData.instance.specialLimit == SpecialLimitType.NoGoodComment)
            {
                if (PlayerData.instance.currSelectPropList.Contains(ConsumePropType.InvalidSpecialConditions))
                    return;
                EventSpecialLimit msg = new EventSpecialLimit();
                msg.propType = ConsumePropType.InvalidSpecialConditions;
                SystemEventManager.TriggerEvent("SpecialLimit", msg);
            }
        }

        guestLeaveCount++;
        StateGuestCome();
        if (CompeletTarget() && !guestParentInfo.AllChairIsEmply())
        {
            if (!isWin)
            {
                //Debug.Log("最后一个玩家离开 代开失败");

                StartCoroutine(WaitFailTip());
                return;
            }
        }

     
    }


    /// <summary>
    /// 完成目标检测
    /// </summary>
    public void CompeletTarger()
    {
        if (PlayerData.instance.targetType == TargetType.CIonTraget)
        {
            if (PlayerData.instance.targerCount <= PlayerData.instance.currSingleLevelData.cion)
            {
                isWin = true;
                GurstLeave();
                StartCoroutine(WaitChangeLevel());

            }
        }
        else if (PlayerData.instance.targetType == TargetType.CompeletFood)
        {

            if (PlayerData.instance.targerCount <= PlayerData.instance.currSingleLevelData.compeleFoodNumber)
            {
                isWin = true;
                Debug.Log("游戏胜利  ！");
                GurstLeave();
                StartCoroutine(WaitChangeLevel());
            }
        }
        else if (PlayerData.instance.targetType == TargetType.GoodCommend)
        {

            if (PlayerData.instance.targerCount <= PlayerData.instance.currSingleLevelData.goodCommendNumber)
            {
                isWin = true;
               
                GurstLeave();
                StartCoroutine(WaitChangeLevel());
            }
        }
    }

    /// <summary>
    ///顾客离开后刷新顾客
    /// </summary>
    /// <returns></returns>
    private void StateGuestCome()
    {
  
        if (systemGameManager.GameStat == GameStat.GameRun)
        {
            if (PlayerData.instance.limitType == LimitType.GustCountLimit)
            {
                if (GuestCompelet >= guestList.Count)
                {
                    return;
                }

                 for (int i = 0; i < guestParentInfo.MaxChairCount; i++)
                {
                    var isHaveGuest = guestParentInfo.GetHaveGuest(i);
                    //var count = guestParentInfo.guestWartFoodPos[i].childCount;

                    if (!isHaveGuest)
                    {
                        var traget = guestParentInfo.guestWartFoodPos[i];
                        traget.SetSiblingIndex(2);
                        guestParentInfo.SetHaveGuest(i, true);
                     
                            Guest guestCompnent = guestList[GuestCompelet].GetComponent<Guest>();
                            Debug.Log("GuestCompelet  " + guestList.Count);
                            guestList[GuestCompelet].SetActive(true);
                            guestCompnent.StartGuest(traget, i);
                            GuestCompelet++;
                            PlayerData.instance.currSingleLevelData.comeGuestCount = GuestCompelet;
                            Debug.Log("总长度   " + GuestCompelet + "  增加   " + i);
                            return; 
                    }

                }
            }
            else if(PlayerData.instance.limitType == LimitType.TimeLimit)
            {
                if (tiemTraget)
                    return;
                for (int i = 0; i < guestParentInfo.MaxChairCount; i++)
                {
                    var isHaveGuest = guestParentInfo.GetHaveGuest(i);

                    if (!isHaveGuest)
                    {
                        var traget = guestParentInfo.guestWartFoodPos[i];
                        traget.SetSiblingIndex(2);
                        if (GuestCompelet < guestList.Count)
                        {
                            guestParentInfo.SetHaveGuest(i, true);
                            Guest guestCompnent = guestList[GuestCompelet].GetComponent<Guest>();
                            Debug.Log("GuestCompelet  " + guestList.Count + "    " + GuestCompelet);
                            guestList[GuestCompelet].SetActive(true);
                            guestCompnent.StartGuest(traget, i);
                            GuestCompelet++;
                            PlayerData.instance.currSingleLevelData.comeGuestCount = GuestCompelet;
                            return;
                        }

                        else if (GuestCompelet == guestList.Count)
                        {
                            TiemLimitAddGuest();
                        }
                    }

                }
            }
            
        }
    }
    /// <summary>
    ///开始游戏顾客刷新
    /// </summary>
    /// <returns></returns>
    private IEnumerator StateGuestComeOne()
    {
        if (systemGameManager.GameStat == GameStat.GameRun)
        {
            for (int i = 0; i < guestParentInfo.MaxChairCount; i++)
            {
                var randTime = Random.Range(2f, 3f);
                if (i == 0)
                {
                    randTime = 0;
                }
                yield return new WaitForSeconds(randTime);
                var isHaveGuest = guestParentInfo.GetHaveGuest(i);
                if (isHaveGuest == false)
                {
                    var traget = guestParentInfo.guestWartFoodPos[i];
                    traget.SetSiblingIndex(2);
                    guestParentInfo.SetHaveGuest(i, true);
                    if (GuestCompelet < guestList.Count)
                    {
                        Guest guestCompnent = guestList[GuestCompelet].GetComponent<Guest>();
                        guestList[GuestCompelet].SetActive(true);
                        guestCompnent.StartGuest(traget, i);
                        GuestCompelet++;
                        PlayerData.instance.currSingleLevelData.comeGuestCount = GuestCompelet;
                    }
                }

            }
        }
    }


    /// <summary>
    /// 顾客坐下响应函数
    /// </summary>
    public void NewComeGuestNeedFood()
    {
        EventParamProperties data = new EventParamProperties();
        data.DataObject = this.gameObject;
        data.foodNmae = null;
        SystemEventManager.TriggerEvent("NewComeGuest", data);
        var audio = systemGameManager.LevelManager.CurrRestaurantInfo.restaurentAudio;
        audio.GuestComeSound.Play();
    }

    /// <summary>
    ///玩家离开检测人数显示
    /// </summary>
    /// <returns></returns>
    private bool CompeletTarget()
    {
        if (PlayerData.instance.limitType == LimitType.GustCountLimit)
        {
            Debug.Log(guestLeaveCount + "    guestLeaveCount  " + guestLeaveCount);
    
            if (guestLeaveCount >= ( PlayerData.instance.limitCount + addGuestCount))
            {

                if (PlayerData.instance.targetType == TargetType.CIonTraget &&
                    PlayerData.instance.currSingleLevelData.cion < PlayerData.instance.targerCount)
                {
                    isWin = false; //失败
                    return true;

                }
            
                if (PlayerData.instance.targetType == TargetType.CompeletFood &&
                    PlayerData.instance.currSingleLevelData.compeleFoodNumber < PlayerData.instance.targerCount)
                {

                    isWin = false; //失败
                    return true;
                    Debug.Log("人数出餐失败！");

                }
                if (PlayerData.instance.targetType == TargetType.GoodCommend &&
                   PlayerData.instance.currSingleLevelData.goodCommendNumber < PlayerData.instance.targerCount)
                {

                    isWin = false; //失败
                    return true;

                }
            }
        }
        else if (PlayerData.instance.limitType == LimitType.TimeLimit && tiemTraget)
        {
            Debug.Log("时间关卡限制！");
            if (PlayerData.instance.targetType == TargetType.CIonTraget &&
                    PlayerData.instance.currSingleLevelData.cion < PlayerData.instance.targerCount)
            {
                isWin = false; //失败
                return true;

            }

            if (PlayerData.instance.targetType == TargetType.CompeletFood &&
                PlayerData.instance.currSingleLevelData.compeleFoodNumber < PlayerData.instance.targerCount)
            {

                isWin = false; //失败
                return true;

            }
            if (PlayerData.instance.targetType == TargetType.GoodCommend &&
               PlayerData.instance.currSingleLevelData.goodCommendNumber < PlayerData.instance.targerCount)
            {

                isWin = false; //失败
                return true;
            }
        }
       return false;
    }

    //玩家全部离开
    public void GurstLeave()
    {
        for (int i =0; i<guestList.Count; i++)
        {
            Guest guestCompnent = guestList[i].GetComponent<Guest>();
           
                guestCompnent.GustLeave();
        }
        guestParentInfo.SetWartFoodPos(false);
    }

    private IEnumerator WaitChangeLevel()
    {
        yield return new WaitForSeconds(3f);
        Debug.Log("切换关卡！");
        systemGameManager.ChangeLevel();
    }
   
    /// <summary>
    /// 时间限制结算输
    /// </summary>
    public void GustSetlementLimitTime()
    {
        if (PlayerData.instance.targetType == TargetType.CIonTraget)
        {
            if (PlayerData.instance.currSingleLevelData.cion >= PlayerData.instance.targerCount)
            {
                //成功
                isWin = true;
                GurstLeave();
            }
            else
            {
                //失败
               isWin = false;
               GurstLeave();
                StartCoroutine(WaitFailTip());
            }
        }
        else if (PlayerData.instance.targetType == TargetType.CompeletFood)
        {
            if (PlayerData.instance.currSingleLevelData.compeleFoodNumber >= PlayerData.instance.targerCount)
            {
                isWin = true;
                GurstLeave();
            }
            else
            {
                GurstLeave();
                StartCoroutine(WaitFailTip());
            }
        }
        else if (PlayerData.instance.targetType == TargetType.GoodCommend)
        {
            if (PlayerData.instance.currSingleLevelData.goodCommendNumber >= PlayerData.instance.targerCount)
            {
                isWin = true;
                GurstLeave();
            }
            else
            {
                GurstLeave();
                StartCoroutine(WaitFailTip());
            }
        }
    }

    private IEnumerator WaitFailTip()
    {
        yield return new WaitForSeconds(1f);
        guestList.Clear();
        systemGameManager.UiManager.OpenFailTip(ConsumePropType.AddGuest);
    }

    /// <summary>
    /// 失败提示返回操作
    /// </summary>
    public void  ReturnOperate(bool value)
    {
        if(value == true)
        {

        }
        else
        {
           GurstLeave();
            systemGameManager.ChangeLevel();
        }
        guestParentInfo.SetWartFoodPos(false);
    }


    public void RturnFailSelceOperate(int type)
    {
   
        if (type == 1) //广告
        {
            AddGuest();
        }
        else if (type == 2)//道具
        {
            AddGuest();

            var strName = PlayerData.instance.dailyTaskDataValue.useAddGuestCount;
            PlayerData.instance.dailyTaksValueDit[strName] += 1;
            PlayerData.instance.achieveParmeter.useAddGuestPropCount += 1;
            PlayerData.instance.SaveDayliTaskValueData();
            PlayerData.instance.SaveAchieveTaskValueData();
            PlayerData.instance.PropOperate(ConsumePropType.AddGuest, -1);
        }
    }

    //试用道具增加三顾客
    private void AddGuest()
    {
       
        guestParentInfo.SetWartFoodPos(false);
        //创建3个顾客
        var levelData = systemGameManager.LevelManager.GetCurrLevelData();
        string fileName = PlayerData.instance.curPlayeShop + "_Guest_1";
        var shopGuest = LoadConfigursManager.instans.ShopGuestDit[fileName];
        var gustConfigur = shopGuest[levelData.levelGuestTablle];
        var foodMenuDit = LoadConfigursManager.instans.ShopFoodMenuDict[PlayerData.instance.curPlayeShop];

        guestCount += 3;
            addGuestCount += 3;
        PlayerData.instance.currSingleLevelData.comeGuestCount += 3;
        for (int i = 0; i < 3; i++)
        {

            var guestHandle = SystemDataFactoy.instans.GetYooAssetHandle("Assets/Prefabs/Guest/Guest_1.prefab");
            var guest = guestHandle.InstantiateSync();

            var guestCompnet = guest.GetComponent<Guest>();
            var randPos = Random.Range(0, 2);
            if (randPos == 0)
            {
                guest.transform.SetParent(guestParentInfo.guestStartPos1);
                guest.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
                guestCompnet.SetBalckTrager(guestParentInfo.guestStartPos2);
            }
            else
            {
                guest.transform.SetParent(guestParentInfo.guestStartPos2);
                guest.transform.localScale = new Vector3(-0.7f, 0.7f, 0.7f);
                guestCompnet.SetBalckTrager(guestParentInfo.guestStartPos1);
            }

            guest.transform.localPosition = Vector3.zero;
            guest.transform.localRotation = Quaternion.identity;

            guestCompnet.Configur(this);
            guest.SetActive(true);
            guestCompnet.fromTime = Random.Range(0.5f, 0.8f);
            guestList.Add(guest);
            
            var compnent = guest.GetComponent<Guest>();
            var targer = guestParentInfo.guestWartFoodPos[i];
            compnent.StartGuest(targer, i);
            guestParentInfo.SetHaveGuest(i, true);
            compnent.GuestRest();
            //compnent.AddWaitTime(addWaitTime);
            var rand = Random.Range(0, gustConfigur.guestDataList.Count);
            var gustData = gustConfigur.guestDataList[rand];
            for (int j=0; j< gustData.needFood.Length; j++)
            {
                var foodStr = gustData.needFood[j];
                if (foodStr != "null")
                {
                    if (foodMenuDit.ContainsKey(foodStr))
                    {
                        var food = foodMenuDit[foodStr];
                        SetGuestMenu(i, food);
                    }
                }
            }
        }
    }


    private void Update()
    {

        //时间限制
        if (PlayerData.instance.limitType == LimitType.TimeLimit && !tiemTraget && addGuestCount == 0)
        {

            if (PlayerData.instance.limitCount <= PlayerData.instance.currSingleLevelData.playTime)
            {
               // gameEnd = false;
                tiemTraget = true;
                GustSetlementLimitTime();
            }
        }

    }

   
    private void CallGuestReceiveFood(string key, EventParamProperties eventparam)
    {
        EventDeliverCookedFood deliverFood = eventparam as EventDeliverCookedFood;
        GiveGuestFood(deliverFood.foodID, deliverFood.plateIndex, deliverFood.totalPrice, deliverFood.foodIDNum);
    }

   /// <summary>
   /// 食物对比
   /// </summary>
   /// <param name="foodId"></param>
   /// <param name="plateIndex"></param>
   /// <param name="price"></param>
   /// <param name="IDNum"></param>
    private void  GiveGuestFood(string foodId,int plateIndex, float price,float IDNum)
    {
        foreach(GameObject guest in guestList)
        {
            Guest gustCompnent = guest.GetComponent<Guest>();
            float tempPrice = price;
            int idex = -1;
            if (gustCompnent.ComparisonNeedFoodNum(IDNum,out idex)) //匹配成功
            {
                
                EventDeliverCookedFood eventMsg = new EventDeliverCookedFood();
                eventMsg.foodID = foodId;
                eventMsg.totalPrice = tempPrice;
                eventMsg.plateIndex = plateIndex;
                eventMsg.needTransform = gustCompnent.GuestNeedDlg.GetNeedFoodParent(idex);
                SystemEventManager.TriggerEvent("OperationSubmitCompeletFood", eventMsg);
                StartCoroutine( SubmitSuccesssCheck(gustCompnent, idex));
                return;
            }
        }
    }
    /// <summary>
    ///比对饮料
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="eventparam"></param>
    private void CallAddDrink(string eventName, EventParamProperties eventparam)
    {
        EventAddDrink drink = eventparam as EventAddDrink;
        int idex = -1;
        foreach (GameObject guest in guestList)
        {
            Guest gustCompnent = guest.GetComponent<Guest>();
            if (gustCompnent.ComparisonNeedFoodNum(drink.drinkIDNum, out idex))
            {
                EventAddDrink eventMsg = new EventAddDrink();
                eventMsg.drinkID = drink.drinkID;
                eventMsg.drinkPrice = drink.drinkPrice;
                eventMsg.needTransform = gustCompnent.GuestNeedDlg.GetNeedFoodParent(idex);
                SystemEventManager.TriggerEvent("OperationSubmitCompeletDrink", eventMsg);
                systemGameManager.UiManager.GuetNeedComelet();
                drink.DataObject.GetComponent<DrinkOpreat>().SubmitDrinkSucess(eventMsg.needTransform, drink.index);
                StartCoroutine( SubmitSuccesssCheck(gustCompnent, idex));
                return;
            }
        }
    }
    private IEnumerator SubmitSuccesssCheck(Guest guest,int index)
    {
        yield return new WaitForSeconds(0.6f);
        guest.SubmitSuccess(index);
    }
}
