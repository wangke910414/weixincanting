using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
//using YooAsset;
//using static UnityEditor.Progress;

public class LevelManager : MonoBehaviour
{
    private int currLevel = 0;

    private SystemGameManager systemGameManager = null;

    [SerializeField]
    private GameObject RestaurantPartent;

    //private List<GameObject> LevelList = new List<GameObject>();
    public GameObject currRestaurant;

    private RestaurantInfo currRestaurantInfo;   //挡前关卡信息

    /// <summary>
    /// 当前关卡主食物  
    /// </summary>

    // private Dictionary<string, FoodData> currLevelfoodDictionary = new Dictionary<string, FoodData>();



    public int CurrLevel { get => currLevel; set => currLevel = value; }
    public RestaurantInfo CurrRestaurantInfo { get => currRestaurantInfo; }

    public void Configur(SystemGameManager systemGameManager)
    {

        this.systemGameManager = systemGameManager;
        //SpawnRestaurant();
        // RestManager();
    }

    //public void AddLevel(LevelData level)
    //{
    //    levelDataList.Add(level);
    //}

    public LevelData GetCurrLevelData()
    {
        var LevelDataList = LoadConfigursManager.instans.LevelShopDataDuct[PlayerData.instance.currLevelName];

        Debug.Log(LevelDataList +"   关卡名字  " + PlayerData.instance.currLevelName);
        return LevelDataList[currLevel]; 
    }

    public IEnumerator RestManager()
    {
        if (PlayerData.instance.currSelectLevel != 0)
            currLevel = PlayerData.instance.currSelectLevel - 1;
        yield return SpawnRestauranYoo();
     }

    /// <summary>
    /// 生成大厅地图
    /// </summary>
    private void SpawnRestaurant()
    {
        //currLevel = PlayerData.instance.currSelectLevel;

        //if (currRestaurant == null)
        //{
        //    var restName = systemGameManager.SystmConfigurationManager.RestaurantAssetConfigerName;
        //    RestaurantAssetConfiger restConfigur = SystemDataFactoy.instans.GetResourceConfigur(restName) as RestaurantAssetConfiger;
           
        //    foreach (RestaurantData iitem in restConfigur.RestaurantDataList)
        //    {
        //        var currData = GetCurrLevelData();
        //        if (currData.levelResources == iitem.hotelResoursceNmae)
        //        {
        //            var obj = SystemDataFactoy.instans.GetResource("restaurant", "Restaurant_1");
        //            currRestaurant = Instantiate(obj);
        //            currRestaurant.transform.SetParent(RestaurantPartent.transform, false);
        //            currRestaurant.transform.localPosition = Vector3.zero;
        //            currRestaurant.transform.localRotation = Quaternion.identity;
        //            currRestaurant.transform.localScale = new Vector3(1, 1, 1);
        //            currRestaurantInfo = currRestaurant.GetComponent<RestaurantInfo>();
        //            currRestaurantInfo.levelData = GetCurrLevelData();
        //            currRestaurantInfo.Configuer(systemGameManager);
        //        }
        //    }
        //}    
    }


    private void SpawnRestauranAssetBundle()
    {


        if (currRestaurant == null)
        {
            var restName = systemGameManager.SystmConfigurationManager.RestaurantAssetConfigerName;
            RestaurantAssetConfiger restConfigur = SystemDataFactoy.instans.GetResourceConfigur(restName) as RestaurantAssetConfiger;

            foreach (RestaurantData iitem in restConfigur.RestaurantDataList)
            {
                var currData = GetCurrLevelData();
              //  if (currData.levelResources == iitem.hotelResoursceNmae)
                {
                    var obj = SystemDataFactoy.instans.GetResource("restaurant", currData.levelResources);
                    currRestaurant = Instantiate(obj);        
                    currRestaurant.transform.SetParent(RestaurantPartent.transform, false);
                    currRestaurant.transform.localPosition = Vector3.zero;
                    currRestaurant.transform.localRotation = Quaternion.identity;
                    currRestaurant.transform.localScale = new Vector3(1, 1, 1);
                    currRestaurantInfo = currRestaurant.GetComponent<RestaurantInfo>();
                    currRestaurantInfo.levelData = GetCurrLevelData();
                    currRestaurantInfo.Configuer(systemGameManager);
                }
            }
        }
    }


    private IEnumerator SpawnRestauranYoo()
    {
        if (currRestaurant == null)
        {
            var restName = systemGameManager.SystmConfigurationManager.RestaurantAssetConfigerName;
            RestaurantAssetConfiger restConfigur = SystemDataFactoy.instans.GetResourceConfigur(restName) as RestaurantAssetConfiger;

          //  foreach (RestaurantData iitem in restConfigur.RestaurantDataList)
            {
                var currData = GetCurrLevelData();
               // if (currData.levelResources)
                {

                    var hadle = SystemDataFactoy.instans.GetYooAssetHandle("Assets/Prefabs/Restaurant/" + currData.levelResources + ".prefab");
                    if (hadle == null)
                        yield break;

                    var currRestaurant = hadle.InstantiateSync();

                    currRestaurant.transform.SetParent(RestaurantPartent.transform, false);
                    currRestaurant.transform.localPosition = Vector3.zero;
                    currRestaurant.transform.localRotation = Quaternion.identity;
                    currRestaurant.transform.localScale = Vector3.one;

                    currRestaurantInfo = currRestaurant.GetComponent<RestaurantInfo>();
                    currRestaurantInfo.levelData = GetCurrLevelData();
                    currRestaurantInfo.Configuer(systemGameManager);

                    yield break;
                }
            }
        }
    }
}


