using System;
using System.Net.Mail;

namespace SK.Framework
{
    /// <summary>
    /// 邮件
    /// </summary>
    public sealed class Email : IDisposable
    {
        public MailMessage MailMessage;
        /// <summary>
        /// Smtp Client
        /// Smtp : 简单邮件传输协议（Simple Mail Transfer Protocol）
        /// </summary>
        public SmtpClient SmtpClient;

        public void Dispose()
        {
            MailMessage.Dispose();
            SmtpClient.Dispose();
        }
    }
}