using System;
using System.Net;
using System.Text;
using UnityEngine;
using System.Net.Mail;

namespace SK.Framework
{
    public static class Mailer
    {
        //邮箱服务器主机,根据使用的发送邮箱,使用其对应的服务器主机 例如 QQ邮箱服务器主机 smtp.qq.com
        private readonly static string host = "smtp.qq.com";
        //邮箱服务器主机端口
        private readonly static int port = 587; ///网易163邮箱 smtp.163.com
        //发送邮件所用邮箱
        private static string senderEmail = "910665086@qq.com";
        //发送邮件所用邮箱的密码 （第三方客户端登录授权码）
        private static string password = "tszytkyremsabfeh";

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="subject">主题</param>
        /// <param name="body">主体</param>
        /// <param name="toEmail">接收邮箱</param>
        /// <param name="callback">回调</param>
        public static void Send(string subject, string body, string toEmail, Action callback = null)
        {
            using (var email = new Email()
            {
                MailMessage = new MailMessage()
                {
                    From = new MailAddress(senderEmail),
                    Subject = subject,
                    Body = body,
                    SubjectEncoding = Encoding.UTF8,
                    BodyEncoding = Encoding.UTF8,
                    IsBodyHtml = false,
                },
                SmtpClient = new SmtpClient()
                {
                    Host = host,
                    Port = port,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(senderEmail, password),
                    EnableSsl = false,
                    DeliveryMethod = SmtpDeliveryMethod.Network
                }
            })
            {
                email.MailMessage.To.Add(new MailAddress(toEmail));
                email.SmtpClient.SendCompleted += (s, e) => callback?.Invoke();
                email.SmtpClient.SendMailAsync(email.MailMessage);
                Debug.Log($"发送邮件：接收人 - {toEmail} 主题 - {subject} 内容 - {body}");
            }
        }
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="subject">主题</param>
        /// <param name="body">主体</param>
        /// <param name="toEmails">接收邮箱</param>
        /// <param name="callback">回调</param>
        public static void Send(string subject, string body, string[] toEmails, Action callback = null)
        {
            using (var email = new Email()
            {
                MailMessage = new MailMessage()
                {
                    From = new MailAddress(senderEmail),
                    Subject = subject,
                    Body = body,
                    SubjectEncoding = Encoding.UTF8,
                    BodyEncoding = Encoding.UTF8,
                    IsBodyHtml = false
                },
                SmtpClient = new SmtpClient()
                {
                    Host = host,
                    Port = port,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(senderEmail, password),
                    EnableSsl = false,
                    DeliveryMethod = SmtpDeliveryMethod.Network
                }
            })
            {
                for (int i = 0; i < toEmails.Length; i++)
                {
                    email.MailMessage.To.Add(new MailAddress(toEmails[i]));
                }
                email.SmtpClient.SendCompleted += (s, e) => callback?.Invoke();
                email.SmtpClient.SendMailAsync(email.MailMessage);
                Debug.Log($"群发邮件：主题 - {subject} 内容 - {body}");
            }
        }
    }
}