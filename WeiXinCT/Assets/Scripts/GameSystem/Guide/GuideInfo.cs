using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GuideInfo : MonoBehaviour
{
    public string guideName;
    public int guideID;
    public string startEvent;
    public string clickEvent;
    public bool isShowBG;
    public int nextID;

    public Action eventMag;
    public void ClickEvent()
    {
       if (eventMag != null)
        {
            eventMag();
            this.gameObject.SetActive(false);
        }
    }
}
