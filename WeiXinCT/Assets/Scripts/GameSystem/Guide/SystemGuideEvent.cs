using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class SystemGuideEvent : MonoBehaviour
{
    public static SystemGuideEvent instans;

 


    private Dictionary<string, GuideInfo> guideInfoDict = new Dictionary<string, GuideInfo>();

    public List<GuideInfo> guideInfoLost = new List<GuideInfo> ();


    private void Awake()
    {
        if (instans == null)
        {
            instans = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        AddOnceGame();
    }

    public void AddOnceGame()
    {
        for (int i=0; i< guideInfoLost.Count; i++)
        {
            var guide = guideInfoLost[i].GetComponent<GuideInfo>();
            guideInfoDict.Add(guide.guideName, guide);
        }

    }

    public void ShowGuide(string key,Action action)
    {
       GuideInfo guide = null;
        if (guideInfoDict.ContainsKey(key))
        {
            guide = guideInfoDict[key];
            guide.eventMag = action;
            guide.gameObject.SetActive(true);
        }
    }

}
