using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System;
using UnityEditor;
using LitJson;
using UnityEngine.Networking;
using System.Runtime.Serialization.Formatters.Binary;
using WeChatWASM;
using LitJson;
using System.Linq.Expressions;

[Serializable]
public class PlayerDataParameter
{
    public string playerName;
    public int cions;                //金币
    public int energy;               //体力
    public int keyNumber;               //钥匙数目
    public int diamond ;              //钻石
    public int onceGaceGuide;       //第一次游戏指导 
    public string headID;          //头像
    public int siginDayNumber;      //总共签到天数
    public int unLockCount = 1;         //解锁关卡数目
    public int assocriationDayNumber;    //会员协会天数
    public int openAppCount = 0;              //打开次数
    public List<int> sgindayList = new List<int>();
   //签到状态
    public string singinDayTime = "null";       //上次签到时间
}


//协会成员
public class AssociationMenber
{
    public int buyCount;               //购买次数
    public int levelGrade;             //订购等级
    public int accDayCount;            //总天数
    public int upGradeDayCount;        //升级后最大天数
    public string fristBuyTime = string.Empty;          //为0的时候第一次购买时间
    public string nextLookTime = string.Empty;          //上一次查看时间
   // public string recentlyBuyTime = string.Empty;     //最近一次购买时间
    public List<int> consumeDayList = new List<int>(4); //消耗的天数
    public List<int> receiveCountList = new List<int>(4); //可以领取次数


  public AssociationMenber()
    {
        for (int i = 0; i < 4; i++)
        {
            consumeDayList.Add(0);
            receiveCountList.Add(0);
        }

    }
    //public int GetDayExcept(int index)
    //{
    //    var day = 0;
    //    for (int i=0; i< consumeDayList.Count; i++ )
    //    {
    //        if (i != index)
    //            day += consumeDayList[i];
    //    }    
    //    return day;
    //}
    public void ClearMemer()
    {
        for (int i = 0; i < 4; i++)
        {
            consumeDayList[i] = 0;
            receiveCountList[i] = 0;
        }

        levelGrade = 0;
        accDayCount = 0;
        fristBuyTime = string.Empty;
        nextLookTime = string.Empty;
    }

}

/// <summary>
/// 玩家存放本地数据
/// </summary>
[SerializeField]
public class PlayerLocadData
{
    public string openAppTime = "null";
    public string strengthUseTime = string.Empty;              //体力使用时间
   /// public float strengthConsumeTime = 0f;      //体力消耗时间 
    ///public bool isAssociationMenberStrength;    //会员体力是否不错
}


/// <summary>
/// 教学数据
/// </summary>
[Serializable]
public class TeachingValue
{
    public int levelOnClick;    //点击关卡
    public int selectLevelClick; //教学点击关卡
    public int startGameClick;   //点击开始游戏
    //游戏内
    public int MakeFoodClick;
    public int FoodMakeCompeltClick;
    public int SubmitFoodClick;
    public int SubmitDrinkClick;
    public int addOtherFoodClick;
    public int deveiceUpGradeClick;
    public int foodUpGradeClick;
}

//成就任务数据
public class AchieveParameter
{
    public int tatolCount;        //完成总长度

    public int passLevelCount;  //  过关数目
    public int serveiceGuestCount;  //服务人数
    public int contiunNoFireFoodCount; //连续关卡不糊食物
    public int continuDisCardFoodCount;      //连续关卡不丢弃食物
    public int sameDayLevelCount;            //同一天通关数目
    public int sameLeveinCionCount;              //同一关获得金币数目
    public int sameLevelServiceGuestCount;   //服务顾客数目

    public int fiveCombosCount;         //五连击数目
    public int fourCombosCount;         //四连击数目
    public int threeCombosCount;        //三连击

    public int SameRestaurantFoodUpGradeount;
    public int SomeRestaurantDeiveceUpGradeCount;


    public int usePropCount;                 //使用道具数目
    public int useFastMakePropCount;        //快速制作
    public int useNoFirePropCount;          //使用不糊
    public int usePatiencePropCount;        //使用耐心
    public int useAddGuestPropCount;        //使用添加顾客
    public int useDubleCionPropCount;      //使用双倍金币
    public int useAutomaticSumbitPropCount;     //使用自动提交

    public int obtainCionCount;                 //获得金币
    public int comsumeCionCount;                //消耗金币 no
    public int obtanDiamondCount;                //获得钻石 no
    public int comsumeDiamondCount;              //消耗钻石 no
    public int sameDayObtainCionNoComsumeCount;   //子只获得金币不消耗
    public int sameDayObtainCionCount;               //同一天获得金币
    public int sameDayComsumeCionCount;              //同一天消耗金币
    public int obtainKeyCount;                   //获得钥匙数目

    public int AllLevelOneStarCount;            //完成所有一个店一星
    public int AllLevelTweStarCount;             //完成所有一个店两星
    public int AllLevelThreeStarCount;          //完成所有一个店三星
    public int totalSubmitFoodCount;            //总共提交食物
    public int totalSubmitDrinkCount;           //总饮料提交
    public int goodCommentCount;                //好评
    public int otherFoodCount;                  //累计配菜个数
}

public class PlayerMapAndLevelData
{
    public List<string> unLockMapNameList = new List<string>();       
}

/// <summary>
/// 每个关卡次数
/// </summary>
public class LevelPlayOnce
{
   // public List<string> levelIndexID = new  List<string>();
   // public List<int> levelPlayOnce = new List<int>();
    public Dictionary<string, string> levelPlayOnceDataDict = new Dictionary<string, string>();
}


public class UnLockRestaurent
{
    public Dictionary<string, int> restaurentUnlockDit = new Dictionary<string, int>();                             
}

/// <summary>
///消耗品数据
/// </summary>
public class ConsumePropDataParameter
{
    public Dictionary<string, string> consumePropDataDict = new Dictionary<string, string>();
}
/// <summary>
/// 设别等级数据
/// </summary>
/// 
public class DeviceDataParameter
{
    public  Dictionary<string, string>  deviceDict = new Dictionary<string, string>();
}

/// <summary>
/// 食物等级数据
/// </summary>
/// 
public class FoodDataPramerer
{
    public Dictionary<string, string> foodDataDict = new Dictionary<string, string>();
}

public enum TargetType : int 
{
    Null,
    CIonTraget,         //金币
    CompeletFood,        //完成食物
    GoodCommend         //好评
}

/// <summary>
/// 吸纳之类型
/// </summary>
public enum LimitType : int
{
    Null,
    TimeLimit,          //时间限制
    GustCountLimit      //顾客限制
}

public enum SpecialLimitType : int
{
    Null,
    NoFire,             //不能烧糊  1
    NoDisCard,          //不能丢弃  2
    NoGoodComment,      //不能差评  3
}


//单局获得数据结束清空
public class CurrSingleLevelData
{
    public float playTime;        //游戏时间
    public int cion;            //金币
    public int prizeKeyCount;       //钥匙长度
    public int compeleFoodNumber;   //完成出餐数目
    public int goodCommendNumber;   //好评
    public int fuilGust;            //失败顾客
    public int continuOnce;         //连次数
    public int continuCount;        //连击次数
    public int comeGuestCount;      //来了多少顾客
    public int FoodFireCount;       //不烧食物
    public int disCardFoodCount;       //丢弃食物长度
    public int submitFoodCount;         //提交食物长度
    public int submitDrinkCount;        //饮料提交
    public int contiunFiveSbmitCount;    //五连击
    public int contiunFourSbmitCount;     //四连击
    public int contiunThreeSbmitCount;    //三连击长度；
    public bool automaticSubmitProp = false;  //是否有自动调教道具
    //试用升级保存数据
    public Dictionary<string, string> foodTyeUpDit = new Dictionary<string, string>();
    public void CleraData()
    {
        cion = 0;
        playTime = 0f;
        continuOnce = 0;
        fuilGust = 0;
        prizeKeyCount = 0;
        compeleFoodNumber = 0;
        goodCommendNumber = 0;
        continuCount = 0;
        comeGuestCount = 0;
        FoodFireCount = 0;
        disCardFoodCount = 0;
        submitFoodCount = 0;
        submitDrinkCount = 0;
        contiunFiveSbmitCount = 0;
        contiunFourSbmitCount = 0;
        contiunThreeSbmitCount = 0;
        automaticSubmitProp = false;
    }
}

public class DailyTaksCompeletValuData
{
     public string continuFoodCout = "continuFoodCout";             //连击
    public string conpelet5ContinuCount = "conpelet5ContinuCount";       //完成一次5连击
    public string conpelet4ContinuCount = "conpelet4ContinuCount";       //完成四连击次数

    public string foodCount = "foodCount";
    public string drinkCount = "drinkCount";
    public string goodComment = "goodComment";                 //好评

    public string usePropCount = "usePropCount";                //使用道具计数
    public string useMomentFoodPropCount = "useMomentFoodPropCount";           //使用快速制作道具
    public string useFireFoodPropCount = "useFireFoodPropCount";            //使用不烧糊
    public string useGuestPatiencePropCount= "useGuestPatiencePropCount";       //使用恢复耐心
    public string useAddGuestCount = "useAddGuestCount";                //使用增加顾客;
    public string useSetlementDoubleCionPropCount = "useSetlementDoubleCionPropCount";
    public string useAutomaticSubmitFoodCount = "useAutomaticSubmitFoodCount";    //自动

    public string compelet20LevelCount = "compelet20LevelCount";
    public string continu10levelCount = "continu10levelCount";
    public string obtain10Key = "obtain10Key";

    public string obtainCion = "obtainCion";
    public string consumeCion = "consumeCion";
    public string obtainDiamond = "obtainDiamond";
    public string consumeDiamond = "consumeDiamond";

}


public class DailyTaskSaveItem
{
    public int dailyType;                   //每日类型
    public int TakeListIndex;               //任务下标
    public RefreshType refreshType;         //刷新类型
    public int DailyTaskStart = 0;          // 0 进行   1 可领取  2 领取完成
}

public class DailyTaskItemListData
{
    public string StartTime;
    public int compeletCount;                 //完成个数
    public List<DailyTaskSaveItem> dailyTaskSaveList = new List<DailyTaskSaveItem>();
}

public class PlayerData
{
    static public PlayerData instance;
    public static PlayerDataParameter playerDataParame;         //玩家数据
    public static PlayerMapAndLevelData playerMapAndLevelParme; //解锁地图、关卡、 解锁等级

    public static ConsumePropDataParameter consumePropDataParameter;    //道具数据
    public static UnLockRestaurent restaurentUnLock;                     //餐厅数据
    public static DeviceDataParameter deviceDataParameter;                  //设备等级
    public static FoodDataPramerer foodDataPramerer;                    //食物升级数据
    public static AssociationMenber associationMenber;                  //会员数据
    public string serverNowTime = string.Empty;

    public static int allLevelPlay = 0;                         //所有关卡都可以完

    //任务数据
    public DailyTaskItemListData dailyTaskItemList = new DailyTaskItemListData(); //每日任务的 每天三个任务
    public DailyTaksCompeletValuData dailyTaskDataValue = new DailyTaksCompeletValuData();  //任务数据
    public Dictionary<string, int> dailyTaksValueDit = new Dictionary<string, int>();
    
    //玩家存放本地数据
    public PlayerLocadData playerLoacadData = new PlayerLocadData();        //本地数据
    //关卡游玩次数
    public static LevelPlayOnce playOnceParme;              //没关游玩次数
    //成就数据
    public AchieveParameter achieveParmeter = new AchieveParameter();
    //单局关卡数据
    public CurrSingleLevelData currSingleLevelData = new CurrSingleLevelData();
    //教学数据值
    public TeachingValue teachingValue = new TeachingValue();


    //本剧选择的道具
    public List<ConsumePropType> currSelectPropList = new List<ConsumePropType>();
    public int getCion;

    public string curPlayeShop;
    public string currLevelName;
    public int currLevelPlayOnce = 0;
    public string currPlayeLevel;
      public int currSelectLevel = 1;  //默认第一关
    public int lastSelectLevel = 0;     //上一次选择的餐厅名称
  
    public int CurrLevelPrizeKeyNumber;
    public int CurrLevelPrizeCionNumber;

    public int targerCount;         //目标数目
    public TargetType targetType;   //配置目标数目

    public int limitCount;            //关卡限制
    public LimitType limitType;        //限制类型

    //关卡钥钥匙动画
    public bool isDiamondSkip;     //钻石跳过
    public bool keyIsFlightAnimation;       //钥匙是否有飞行动画
    public bool isKeySpanRestaution;            //是否同跨餐厅
    public string previousLevelID = string.Empty;           //前一个店ID
    public int fliykeyCount;                  //飞行钥匙数量
                                              //关卡钥钥匙动画\

    //游戏结束获得金币动画
    public bool isCionFlight;               //是否获得金币
    public int CionFlightCount;            //获得金币数量
    //游戏结束获得金币动画


    public SpecialLimitType specialLimit;   //特殊限制
    public int unLockLevel = 0;        //解锁关卡数目
    private bool onceLogin = false;     //第一次登陆
    public bool continueGame = false;      //是否继续游戏

    private float saveStrenghtTime = 0;        //体力恢复时间计时器
    public int strenghtLimitCount = 0;         //体力做大长度                                        
    static PlayerData Inastance { get => instance; }
   
    private static string saveFille = "/PlayerData.json";

    private bool isLoaddCompelet = false;
    public bool IsLoaddCompelet { get => isLoaddCompelet; }

    static public void Create()
    {
        if (instance == null)
        {
            instance = new PlayerData();
        }
        if ( playerDataParame == null )
        {

            playerDataParame = new PlayerDataParameter();
            playerDataParame.sgindayList.Add(0);
            playerDataParame.sgindayList.Add(0);
            playerDataParame.sgindayList.Add(0);
            playerDataParame.sgindayList.Add(0);
            playerDataParame.sgindayList.Add(0);
            playerDataParame.sgindayList.Add(0);
            playerDataParame.sgindayList.Add(0);
            instance.GetPlayerData();
        }
        if (playerMapAndLevelParme == null)
        {
            playerMapAndLevelParme =  new  PlayerMapAndLevelData();
        }
        if (playOnceParme == null)
        {
            playOnceParme = new LevelPlayOnce();
            instance.GetPlayeLevelOnceData();
        }
        if(consumePropDataParameter == null)
        {
            consumePropDataParameter = new ConsumePropDataParameter();
            instance.GetPropData();
        }
        if (deviceDataParameter == null)
        {
            deviceDataParameter = new DeviceDataParameter();
            instance.GetDeviceData();

        }
        if (foodDataPramerer == null)
        {
            foodDataPramerer = new FoodDataPramerer();
           instance.GetFoodData();
        }
        if(associationMenber == null)
        {

            associationMenber = new AssociationMenber();
            instance.LoadAssociationMemberData();
        }
        if (restaurentUnLock == null)
        {
            restaurentUnLock = new UnLockRestaurent();
            instance.LoadRestaurentData();
        }
        instance.LoadDailyTaskDataList();
        instance.LoadDayliTaskValueData();
        instance.LoadAchieveTaskValueData();
        instance.PlayLocadData();
        instance.LoadTeachingData();



#if UNITY_WEBGL && UNITY_EDITOR
        instance.isLoaddCompelet = true;
#endif
    }
    public IEnumerator LoaderCloundData()
    {
      ///  var data = JsonUtility.ToJson(playerDataParame);
      //  CloudFunction.instance.CallCloundCreate(data);
        CloudFunction.instance.OnLoadData();      //读玩家数据
        CloudFunction.instance.OnLoadMapAndLevel();   //读玩家地图关卡数据
        CloudFunction.instance.OnLoadRestaurentData();  //餐厅   
        CloudFunction.instance.OnLoadPlayOnce();    //关卡完成次数
        CloudFunction.instance.OnLoadConsumer();    //道具数据
        CloudFunction.instance.OnLoadDeviveData();    //设备数据
        CloudFunction.instance.OnLoadFood();          //食物
        CloudFunction.instance.OnLoadAssociationMember();//协会会员
        CloudFunction.instance.GetNowDate();
        isLoaddCompelet = true;
        yield break;
    }

    public void KeyFlightReset()
    {
        isDiamondSkip = false;
        keyIsFlightAnimation = false;
        isKeySpanRestaution = false;           //是否同跨餐厅
        previousLevelID = string.Empty;
        fliykeyCount = 0;
    }

private void LoadRestaurentData()
    {
#if UNITY_EDITOR
        var data = PlayerPrefs.GetString("LoadRestaurentData");
        if (data != "")
        {
            restaurentUnLock = JsonMapper.ToObject<UnLockRestaurent>(data);
        }
        if (restaurentUnLock.restaurentUnlockDit.ContainsKey("Shop_1"))
        {
            restaurentUnLock.restaurentUnlockDit["Shop_1"] = 1;
        }
        else
        {
            restaurentUnLock.restaurentUnlockDit.Add("Shop_1", 1);
        }
#endif
    }
    //会员数据

    private void LoadAssociationMemberData()
    {
#if UNITY_EDITOR 
        var date = PlayerPrefs.GetString("AssociationMenberData");
        if (date != "")
        {
            associationMenber = JsonMapper.ToObject<AssociationMenber>(date);
        }
        else
        {
            SaveAssociationMemberFata();
        }
#endif
    }
    public void SaveAssociationMemberFata()
    {
        var date  = JsonMapper.ToJson(associationMenber);
        PlayerPrefs.SetString("AssociationMenberData", date);
    }


    //任务数据
    public void SaveTaskListData()
    {
        var data = JsonMapper.ToJson(dailyTaskItemList);
        if (data != "")
        {
            PlayerPrefs.SetString("taskItemList", data);
        }
    }

    
    private void LoadDailyTaskDataList()
    {
        var taskItme = PlayerPrefs.GetString("taskItemList");
        if (taskItme != "")
        {
            var data = JsonMapper.ToObject(taskItme);

            dailyTaskItemList.StartTime = data["StartTime"].ToString();
            dailyTaskItemList.compeletCount = int.Parse (data["compeletCount"].ToString());

            JsonData listData = JsonMapper.ToObject( data["dailyTaskSaveList"].ToJson());
            foreach (JsonData item in listData)
            {
                DailyTaskSaveItem daily = new DailyTaskSaveItem();
                daily.dailyType = int.Parse(item["dailyType"].ToString());
                daily.TakeListIndex = int.Parse(item["TakeListIndex"].ToString());
                daily.refreshType = (RefreshType)int.Parse(item["refreshType"].ToString());
                daily.DailyTaskStart = int.Parse(item["DailyTaskStart"].ToString());
                dailyTaskItemList.dailyTaskSaveList.Add(daily);
            }
        }
    }

    private void LoadTeachingData()
    {

        var data = PlayerPrefs.GetString("TeachingData");
        if (data != "")
        {
            teachingValue = JsonMapper.ToObject<TeachingValue>(data);
            PlayerData.instance.teachingValue.foodUpGradeClick = 0;
        }
    }
    public void SaveTeachingData()
    {
        var data = JsonMapper.ToJson(teachingValue);
        PlayerPrefs.SetString("TeachingData", data);
    }

    //任务数据恢
    public void SaveDayliTaskValueData()
    {
        var data = JsonMapper.ToJson(dailyTaksValueDit);
        PlayerPrefs.SetString("DayliTaskValueData", data);
    }
    public void SavePlayerPrefsValue( string key, string data)
    {
        PlayerPrefs.SetString(key, data);
    }

    private void LoadDayliTaskValueData()
    {
        var data = PlayerPrefs.GetString("DayliTaskValueData");
        if (data != "")
        {
            dailyTaksValueDit = JsonMapper.ToObject<Dictionary<string, int>>(data);
        }
        var strKeys = JsonMapper.ToJson(dailyTaskDataValue);
        Dictionary<string, string> dit = JsonMapper.ToObject<Dictionary<string, string>>(strKeys);
        foreach (var key in dit)
        {
            if (!dailyTaksValueDit.ContainsKey(key.Value))
                dailyTaksValueDit.Add(key.Value, 0);
        }

    }
    private void LoadAchieveTaskValueData()
    {
        var data = PlayerPrefs.GetString("AchieveTaskData");
        if (data == "")
            return;
        achieveParmeter = JsonMapper.ToObject<AchieveParameter>(data);
       // Debug.Log("总长度   "  +  achieveParmeter.sameDayObtainCionNoComsumeCount);
    }

    public void SaveAchieveTaskValueData()
    {
        var achieveData = JsonMapper.ToJson(PlayerData.instance.achieveParmeter);
        PlayerData.instance.SavePlayerPrefsValue("AchieveTaskData", achieveData);
    }

    public void SetTaskDitValueNull()
    {
        List<string> keys = new List<string>(dailyTaksValueDit.Keys);
      for (int i=0; i< keys.Count;i++)
        {
            dailyTaksValueDit[keys[i]] = 0;
        }
         
    }
    public void SetDitElementValueNull(string key)
    { 
         dailyTaksValueDit[key] = 0;
    }

/// <summary>
/// 第一次初始数据
/// </summary>
private void InintPlayerData()
    {
        if(PlayerData.playerDataParame.onceGaceGuide == 0)
        {
            var maoList = LoadConfigursManager.instans.MapList;
            playerMapAndLevelParme.unLockMapNameList.Add(maoList[0].mapName);
        }

    }

    private void InitData()
    {
        var levelDict = LoadConfigursManager.instans.LevelDataList;
        if (levelDict != null)
        {
            foreach(var data in levelDict)
            {
 
            }
        }
    }

    
    private void LoadrPlayerData()
    {
#if UNITY_EDITOR
        var data = PlayerPrefs.GetString("PlayerData");
        if (data != "")
        {
            JsonData jsonData = JsonMapper.ToObject(data);
            if (jsonData != null)
            {
                playerDataParame.cions = int.Parse(jsonData["cions"].ToString());
                playerDataParame.energy = (int)jsonData["strength"];
                playerDataParame.diamond = int.Parse(jsonData["diamond"].ToString());
                playerDataParame.onceGaceGuide = int.Parse(jsonData["onceGaceGuide"].ToString());
                playerDataParame.headID = jsonData["headID"].ToString();
                playerDataParame.keyNumber = int.Parse( jsonData["keyNumber"].ToString());
                playerDataParame.siginDayNumber = int.Parse(jsonData["siginDayNumber"].ToString());
                playerDataParame.singinDayTime = jsonData["singinDayTime"].ToString();
                var siginJson = jsonData["sgindayList"];
                playerDataParame.sgindayList = JsonMapper.ToObject<List<int>>(siginJson.ToJson());
                //playerDataParame.keyNumber = 14;
            }
        }
#endif
    }


    //public IEnumerator Read()
    //{
      
    //    string path = Application.persistentDataPath + saveFille;
    //    UnityWebRequest request = UnityWebRequest.Get(path);
    //    yield return request.SendWebRequest();
        
    //    string keyString = AESUtils.Decrypt(request.downloadHandler.text);
    //    JsonData jsonData = JsonMapper.ToObject(keyString);
    //    if (jsonData != null)
    //    {
    //        playerDataParame.cions = int.Parse(jsonData["cions"].ToString());
    //        playerDataParame.energy =(int) jsonData["strength"];
    //        playerDataParame.diamond = int.Parse(jsonData["diamond"].ToString());
    //        playerDataParame.onceGaceGuide = int.Parse(jsonData["onceGaceGuide"].ToString());
    //        playerDataParame.headID = jsonData["headID"].ToString();
    //    }
    //    else
    //    {
    //        Debug.LogError("PlayerData null ");
    //    }

    //}
    //private void NewSave()
    //{
    //    string path = Application.persistentDataPath;
    //    var jescont = JsonMapper.ToJson(playerDataParame);
    //    using (var seream = new StreamWriter(path + saveFille))
    //    {
    //        string keyStrin = AESUtils.Encrypt(jescont);
    //        seream.Write(keyStrin);
    //    }
    //}

    //保存食物升级数据


     


    //保存设备数据
    public IEnumerator SaveData(string key, string valve)
    {
        yield return new  WaitForEndOfFrame();
#if UNITY_EDITOR
         PlayerPrefs.SetString(key, valve);
#elif UNITY_WEBGL && !UNITY_EDITOR
       CloudFunction.instance.UpdataData(key, valve);
#endif
    }
    private void GetDeviceData()
    {
#if UNITY_EDITOR
        var data = PlayerPrefs.GetString("DeviceData");
        Debug.Log("设别值  " + data);
        if (data != "")
        {
            JsonData datatemp = JsonMapper.ToObject(data);

            deviceDataParameter.deviceDict = JsonMapper.ToObject<Dictionary<string,string>>(datatemp["deviceDict"].ToJson());
            Debug.Log("设别值  " + data);
        }

#endif
    }

    private void GetFoodData()
    {
#if UNITY_EDITOR
        var data = PlayerPrefs.GetString("FoodUpData");
        var jsontemp = JsonMapper.ToObject(data);
       
       if (data != "")
           foodDataPramerer.foodDataDict = JsonMapper.ToObject<Dictionary<string,string>>(jsontemp["foodDataDict"].ToJson());
#endif
    }

    private void GetPlayeLevelOnceData()
    {
#if UNITY_EDITOR
        var data = PlayerPrefs.GetString("PlayeLevelOnce");
        if (data != "")
            playOnceParme.levelPlayOnceDataDict = JsonMapper.ToObject<Dictionary<string,string>>(data);
#endif
    }

    private void GetPropData()
    {
#if UNITY_EDITOR
       var data  = PlayerPrefs.GetString("consumeProp");
        if (data != "")
            consumePropDataParameter = JsonMapper.ToObject<ConsumePropDataParameter>(data);
        
        foreach (var item in Enum.GetValues(typeof( ConsumePropType)))
        {
            if (!consumePropDataParameter.consumePropDataDict.ContainsKey(item.ToString()))
            {
                consumePropDataParameter.consumePropDataDict.Add(item.ToString(), 0.ToString());
            }
        }
#endif
    }
    private void GetPlayerData()
    {
#if UNITY_EDITOR
        var data = PlayerPrefs.GetString("PlayerData");
        if (data != "")
            playerDataParame = JsonMapper.ToObject<PlayerDataParameter>(data);
  
        int  temp = PlayerPrefs.GetInt("allLevelPlay");
        if (data != "")
            allLevelPlay = temp;
#endif
    }

    public void PlayerDataParameSave()
    {

#if UNITY_WEBGL && UNITY_EDITOR
        if (playerDataParame != null)
       {
            var data = JsonMapper.ToJson(playerDataParame);
            PlayerPrefs.SetString("PlayerData", data);
        }
#endif
#if UNITY_WEBGL && !UNITY_EDITOR
        if (playerDataParame != null)
       {
            var data = JsonUtility.ToJson(playerDataParame);
            //CloudFunction.instance.CallCloundUpdate(data);
            CoroutineHandler.StartStaticCoroutine(SaveData("UpdatePlayerData", data));
            //Debug.Log("数据更新");
       }
#endif
    }

    /// <summary>
    /// 编辑器使用
    /// </summary>
    public static  void ClearSave()
   {
        
        PlayerPrefs.SetString("consumeProp","");
        PlayerPrefs.SetString("DeviceData", "");
        PlayerPrefs.SetString("FoodUpData", "");
        PlayerPrefs.SetString("PlayeLevelOnce", "");
        PlayerPrefs.SetString("PlayerData", "");
        PlayerPrefs.SetString("taskItemList", "");
        PlayerPrefs.SetString("TaskValueData", "");
        PlayerPrefs.SetString("AchieveTaskData", "");
        PlayerPrefs.SetString("playerLoacadData", "");
        PlayerPrefs.SetString("TeachingData", "");
        PlayerPrefs.SetInt("allLevelPlay", 0);
        PlayerPrefs.SetString("AssociationMenberData", "");
        PlayerPrefs.SetString("LoadRestaurentData", "");
        
        string path = Application.persistentDataPath;
            playerDataParame = new PlayerDataParameter();
  
        var jescont = JsonMapper.ToJson(playerDataParame);
        using (var seream = new StreamWriter(path + saveFille))
        {
           string keyStrin = AESUtils.Encrypt(jescont);
            seream.Write(keyStrin);
        }
    }

    public static void TextAssociationMenber()
    {
        if (associationMenber == null)
            associationMenber = new AssociationMenber();

         associationMenber.levelGrade += 1;
        associationMenber.fristBuyTime = DateTime.Now.ToString();

        var data = JsonMapper.ToJson(associationMenber);
        PlayerPrefs.SetString("AssociationMenberData",data);

    }

    public static  void AddDiamend()
    {
        if (playerDataParame == null)
        {
            playerDataParame = new PlayerDataParameter();
        }
        PlayerData.playerDataParame.diamond = 10000;
        var data = JsonMapper.ToJson(playerDataParame);
        PlayerPrefs.SetString("PlayerData", data);
    }
    public static void AllLevelPlay()
    {
        PlayerPrefs.SetInt("allLevelPlay", 1);
    }


    public static string GetMemberName<T>(Expression<Func<T>> memberExpression)
    {
        MemberExpression expressionBody = (MemberExpression)memberExpression.Body;
        return expressionBody.Member.Name;
    }

    private void PlayLocadData()
    {
        var data = PlayerPrefs.GetString("playerLoacadData");
        if (data == "")
        {
            playerLoacadData.openAppTime = DateTime.Now.ToString();
            var locadDaata = JsonMapper.ToJson(playerLoacadData);
            PlayerPrefs.SetString("playerLoacadData", locadDaata);
            onceLogin = true;
            return;
        }
        playerLoacadData = JsonMapper.ToObject<PlayerLocadData>(data);

    }
    public void SavePlayLocadData()
    {
        var locadDaata = JsonMapper.ToJson(playerLoacadData);
        PlayerPrefs.SetString("playerLoacadData", locadDaata);
    }

   public void LocadDataOperate()
    {
        OpenAppHandle();
    }
    private void OpenAppHandle()
    {
        

        if (playerLoacadData.openAppTime != "")
        {
            Debug.Log("保存的时间   " + playerLoacadData.openAppTime);
            TimeSpan span = DateTime.Now.Subtract(DateTime.Parse(playerLoacadData.openAppTime)).Duration();

            Debug.Log("保存的时间   " + span.Days  + "     " + span.TotalDays);
            if ((int)span.TotalHours > 24 || (int)span.TotalDays > 0)
            {
                playerLoacadData.openAppTime = DateTime.Now.ToString();
                Debug.Log("进入同一天操作  " + playerLoacadData.openAppTime);

                if (achieveParmeter.sameDayObtainCionCount > 0 && achieveParmeter.sameDayComsumeCionCount == 0)
                {
                    achieveParmeter.sameDayObtainCionNoComsumeCount++;
                }

                achieveParmeter.sameDayObtainCionCount = 0;
                achieveParmeter.sameDayComsumeCionCount = 0;

                playerLoacadData.openAppTime = DateTime.Now.ToString();
                var locadDaata = JsonMapper.ToJson(playerLoacadData);
                PlayerPrefs.SetString("playerLoacadData", locadDaata);

                var achieve = JsonMapper.ToJson(achieveParmeter);
                PlayerPrefs.SetString("AchieveTaskData", achieve);
            }
        }
        if (playerDataParame.openAppCount == 0)
        {
            PlayerData.playerDataParame.energy = 5;
            playerDataParame.openAppCount++;
        }
        playerDataParame.openAppCount++;

    }


    /// <summary>
    /// 大道具操作
    /// </summary>
    /// <param name="pTyoe"></param>
    /// <param name="count"></param>
    /// 
    public void PropOperate(ConsumePropType pTyoe, int count)
    {
        if (count > 0) //增加道具
        {
            if (!PlayerData.consumePropDataParameter.consumePropDataDict.ContainsKey(pTyoe.ToString()))
            {
                PlayerData.consumePropDataParameter.consumePropDataDict.Add(pTyoe.ToString(), count.ToString());
            }
            else
            {
                int temo = int.Parse(PlayerData.consumePropDataParameter.consumePropDataDict[pTyoe.ToString()]) + count;
                PlayerData.consumePropDataParameter.consumePropDataDict[pTyoe.ToString()] = temo.ToString();
            }

        }
        else if (count <0) //使用道具
        {
            if (count != -1)
                count = -1;
           
            if (PlayerData.consumePropDataParameter.consumePropDataDict.ContainsKey(pTyoe.ToString()))
            {
                var temp = int.Parse(PlayerData.consumePropDataParameter.consumePropDataDict[pTyoe.ToString()]) + count;
                if (temp > 0)
                {
                    PlayerData.consumePropDataParameter.consumePropDataDict[pTyoe.ToString()] = temp.ToString();

                }
            }
       
        }

        var str = JsonMapper.ToJson(PlayerData.consumePropDataParameter.consumePropDataDict);
#if UNITY_EDITOR
        PlayerData.instance.SaveData("consumeProp", str);
#elif !UNITY_EDITOR && UNITY_WEBGL
            PlayerData.instance.SaveData("UpdateConsumeProp", str);
#endif
        PlayerData.instance.PlayerDataParameSave();
    }


    /// <summary>
    /// 金币操作
    /// </summary>
    /// <param name="number"></param>

    public void PlayerDataValueCionOprate(int number)
    {
        if (number < 0)
        {
            var strName = PlayerData.instance.dailyTaskDataValue.consumeCion;
            PlayerData.instance.dailyTaksValueDit[strName] += Mathf.Abs(number);
            PlayerData.instance.achieveParmeter.comsumeCionCount += Mathf.Abs(number);
        }
        else if (number > 0)
        {
            var strName = PlayerData.instance.dailyTaskDataValue.obtainCion;
            PlayerData.instance.dailyTaksValueDit[strName] += Mathf.Abs(number);
            PlayerData.instance.achieveParmeter.obtainCionCount += Mathf.Abs(number);
        }

        PlayerData.playerDataParame.cions += number;
        PlayerData.instance.PlayerDataParameSave();
        PlayerData.instance.SaveDayliTaskValueData();
        PlayerData.instance.SaveAchieveTaskValueData();
    }

    /// <summary>
    /// 钻石操作
    /// </summary>
    /// <param name="number"></param>
    public void PlayerDataValueDiamondOprate(int number)
    {
        if (number < 0)
        {
            var strName = PlayerData.instance.dailyTaskDataValue.consumeDiamond;
            PlayerData.instance.dailyTaksValueDit[strName] += Mathf.Abs(number);
            PlayerData.instance.achieveParmeter.comsumeDiamondCount += Mathf.Abs(number);
        }

        else if (number > 0)
        {
            var strName = PlayerData.instance.dailyTaskDataValue.obtainDiamond;
            PlayerData.instance.dailyTaksValueDit[strName] += number;
            PlayerData.instance.achieveParmeter.obtanDiamondCount += number;
        }

        PlayerData.playerDataParame.diamond += number;
        PlayerData.instance.PlayerDataParameSave();
        PlayerData.instance.SaveDayliTaskValueData();
        PlayerData.instance.SaveAchieveTaskValueData();

    }
    /// <summary>
    /// 或缺解锁关卡
    /// </summary>
    /// <returns></returns>
    public int GetUnLockLevel()
    {
        var shopDit = LoadConfigursManager.instans.ShopDict;
        var lvelDit = LoadConfigursManager.instans.LevelShopDataDuct;

        List<string> keyList = new List<string>(shopDit.Keys);
        //int index = 0;
        for (int i = 0; i < keyList.Count; i++)
        {
            var currResaurent = shopDit[keyList[i]];
            var levelName = currResaurent.leveData + "_1";
            var levelData = lvelDit[levelName];

            if (levelData == null)
                return 0;
            for (int j = 0; j < levelData.Count; j++)
            {
                if (PlayerData.playerDataParame.keyNumber <= levelData[j].unLoackKeyCunnt)
                {
                    return j + 1;
                }
            }
        }
        return 0;
    }
}
    

