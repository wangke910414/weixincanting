﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CheckFrame::Update()
extern void CheckFrame_Update_mEDC6AF9CDC48040B8A65B6CFBC6F5CB34B234A45 (void);
// 0x00000002 System.Void CheckFrame::.ctor()
extern void CheckFrame__ctor_m38C73460485C07E03A44FBA6C996F3B51084C346 (void);
// 0x00000003 System.Void HideLoadingPage::OnGameLaunch()
extern void HideLoadingPage_OnGameLaunch_mFFDBC14030320F359B5B080ADCADCA0BD92B32F3 (void);
// 0x00000004 System.Void HideLoadingPage::.ctor()
extern void HideLoadingPage__ctor_mDF4735613B7A6BDE31E2B2F8F50902AE6E883D94 (void);
// 0x00000005 System.Void WXProfileStatsScript::Awake()
extern void WXProfileStatsScript_Awake_mBA25FD9BB43E585AD4BF9EC8C64711A2879D12B2 (void);
// 0x00000006 System.Void WXProfileStatsScript::OnEnable()
extern void WXProfileStatsScript_OnEnable_m0DC14521DFC8AE2093EF06FEDA3C425BF4211507 (void);
// 0x00000007 System.Void WXProfileStatsScript::OnDisable()
extern void WXProfileStatsScript_OnDisable_mB67075A20D85062F63763FF770C61F06D181E0C8 (void);
// 0x00000008 WXProfileStatsScript/ProfValue WXProfileStatsScript::UpdateValue(System.String,System.Single,System.Text.StringBuilder,System.String)
extern void WXProfileStatsScript_UpdateValue_m5917D4314A61471E46C3598801065E4CEF22F73E (void);
// 0x00000009 System.Void WXProfileStatsScript::Update()
extern void WXProfileStatsScript_Update_mEE867FBA174DDE23EB5CACD1AC2B4E4B701423F0 (void);
// 0x0000000A System.Void WXProfileStatsScript::UpdateFps()
extern void WXProfileStatsScript_UpdateFps_mE8A0E4F8C59156A0D4B8EF53AF6850C4E154E82E (void);
// 0x0000000B System.Void WXProfileStatsScript::OnGUI()
extern void WXProfileStatsScript_OnGUI_m0957BACE228A8326FE0DDB5F7438D17B66409E63 (void);
// 0x0000000C System.Void WXProfileStatsScript::OnGameLaunch()
extern void WXProfileStatsScript_OnGameLaunch_mC4C3BDF40B882030E8215E17A9487BB64808599D (void);
// 0x0000000D System.Void WXProfileStatsScript::.ctor()
extern void WXProfileStatsScript__ctor_m55DFD8141CD987DAFF7A9D4F8C28FCC78C880F22 (void);
// 0x0000000E System.Void WXProfileStatsScript/ProfValue::.ctor()
extern void ProfValue__ctor_m879E8E5D78B79A1A1E5062628B1CA75C45D48C62 (void);
// 0x0000000F System.Void TouchData::.ctor()
extern void TouchData__ctor_m79D59C452C436FB2FFDC9765AF7EF605740326AF (void);
// 0x00000010 System.Void WXTouchInputOverride::Awake()
extern void WXTouchInputOverride_Awake_m6DF59EC24461CAD3FD7736B29E97CFB00F0F9844 (void);
// 0x00000011 System.Void WXTouchInputOverride::OnEnable()
extern void WXTouchInputOverride_OnEnable_m6419ABC1106548A51FD09380AB96945AC32CA344 (void);
// 0x00000012 System.Void WXTouchInputOverride::OnDisable()
extern void WXTouchInputOverride_OnDisable_mD4675503E0F4698748B28662DB67B49FD15DE6D5 (void);
// 0x00000013 System.Void WXTouchInputOverride::InitWechatTouchEvents()
extern void WXTouchInputOverride_InitWechatTouchEvents_m9C5FD0A967B37C08F3133656522DC70943AB81AD (void);
// 0x00000014 System.Void WXTouchInputOverride::RegisterWechatTouchEvents()
extern void WXTouchInputOverride_RegisterWechatTouchEvents_mCC8C107407CE833CC1FC49AC2950B2F9E3555C40 (void);
// 0x00000015 System.Void WXTouchInputOverride::UnregisterWechatTouchEvents()
extern void WXTouchInputOverride_UnregisterWechatTouchEvents_mC8699E5BE4275C7C2C15636ABF94440D83619916 (void);
// 0x00000016 System.Void WXTouchInputOverride::OnWxTouchStart(WeChatWASM.OnTouchStartListenerResult)
extern void WXTouchInputOverride_OnWxTouchStart_mE9C8D3A4615BA2D4EDA45F8089CA37F4FB747275 (void);
// 0x00000017 System.Void WXTouchInputOverride::OnWxTouchMove(WeChatWASM.OnTouchStartListenerResult)
extern void WXTouchInputOverride_OnWxTouchMove_m719A9D384E2A865DC2651523AE61AE4A94D29E18 (void);
// 0x00000018 System.Void WXTouchInputOverride::OnWxTouchEnd(WeChatWASM.OnTouchStartListenerResult)
extern void WXTouchInputOverride_OnWxTouchEnd_m159E83F2E3FDD3568DC9439E7DC258973346479F (void);
// 0x00000019 System.Void WXTouchInputOverride::OnWxTouchCancel(WeChatWASM.OnTouchStartListenerResult)
extern void WXTouchInputOverride_OnWxTouchCancel_m4BDE1F8EE7A536B4B55B6F40099F504609516201 (void);
// 0x0000001A System.Void WXTouchInputOverride::LateUpdate()
extern void WXTouchInputOverride_LateUpdate_mEB815DCD3F010A91366A57EAE0F131719329912C (void);
// 0x0000001B System.Void WXTouchInputOverride::RemoveEndedTouches()
extern void WXTouchInputOverride_RemoveEndedTouches_mE4C227CE852E5EBBEFC0344775A08DC6E726466C (void);
// 0x0000001C TouchData WXTouchInputOverride::FindTouchData(System.Int32)
extern void WXTouchInputOverride_FindTouchData_m89FB9695D11408B020F2CE7219910576F16926C3 (void);
// 0x0000001D TouchData WXTouchInputOverride::FindOrCreateTouchData(System.Int32)
extern void WXTouchInputOverride_FindOrCreateTouchData_m1362074AC09041B8CDB033D365DF5B71EC6C0822 (void);
// 0x0000001E System.Void WXTouchInputOverride::UpdateTouchData(TouchData,UnityEngine.Vector2,System.Int64,UnityEngine.TouchPhase)
extern void WXTouchInputOverride_UpdateTouchData_m0FF987CCF54C42C3B5984E4E6D702676A69373DE (void);
// 0x0000001F System.Boolean WXTouchInputOverride::get_touchSupported()
extern void WXTouchInputOverride_get_touchSupported_m92A32C5C051C5A83134AAFA2F8E52E69E850FC99 (void);
// 0x00000020 System.Boolean WXTouchInputOverride::get_mousePresent()
extern void WXTouchInputOverride_get_mousePresent_m504E096CD048CBFAE26587934B88CE14C5D5AC6D (void);
// 0x00000021 System.Int32 WXTouchInputOverride::get_touchCount()
extern void WXTouchInputOverride_get_touchCount_m246DB3B8025BCF06E8929F89E3ABC060F02175FE (void);
// 0x00000022 UnityEngine.Touch WXTouchInputOverride::GetTouch(System.Int32)
extern void WXTouchInputOverride_GetTouch_mAA624EC27C2209D30779CE3FCE4A9E98AB41A0F0 (void);
// 0x00000023 System.Void WXTouchInputOverride::.ctor()
extern void WXTouchInputOverride__ctor_m894F9D76D33FEA917BA8A83519903F38760756AB (void);
// 0x00000024 System.Void WXTouchInputOverride::<InitWechatTouchEvents>b__6_0(System.Int32)
extern void WXTouchInputOverride_U3CInitWechatTouchEventsU3Eb__6_0_mDC43178A105B8AB8FD2EBC9A26746EC302EB9333 (void);
// 0x00000025 System.Void WXTouchInputOverride/<>c::.cctor()
extern void U3CU3Ec__cctor_mFB268D3D3D680B9C7D058F0185E2963D507BC132 (void);
// 0x00000026 System.Void WXTouchInputOverride/<>c::.ctor()
extern void U3CU3Ec__ctor_mBB8D89C9FB98258594AB0366B768D983924C0BCC (void);
// 0x00000027 System.Boolean WXTouchInputOverride/<>c::<RemoveEndedTouches>b__14_0(TouchData)
extern void U3CU3Ec_U3CRemoveEndedTouchesU3Eb__14_0_m3F4195FB13D42C402977ED7E85EEAFEBB70DC117 (void);
// 0x00000028 System.Void WeChatWASM.WX::AddCard(WeChatWASM.AddCardOption)
extern void WX_AddCard_m4EF62A2A285A7F4533E0E7DE752A843370CF5872 (void);
// 0x00000029 System.Void WeChatWASM.WX::AuthPrivateMessage(WeChatWASM.AuthPrivateMessageOption)
extern void WX_AuthPrivateMessage_mB7A227EDFE13A55B1570414EAC30FD9E75A11547 (void);
// 0x0000002A System.Void WeChatWASM.WX::Authorize(WeChatWASM.AuthorizeOption)
extern void WX_Authorize_mBA4B3F57182936833C7CEC4236DD459CBD13E178 (void);
// 0x0000002B System.Void WeChatWASM.WX::CheckIsAddedToMyMiniProgram(WeChatWASM.CheckIsAddedToMyMiniProgramOption)
extern void WX_CheckIsAddedToMyMiniProgram_m09233B992110D4BD7DD5BCAB351821E0B4AEA23E (void);
// 0x0000002C System.Void WeChatWASM.WX::CheckSession(WeChatWASM.CheckSessionOption)
extern void WX_CheckSession_m0802E0E787CC8D37B107ED99ACEF005B8F2A1666 (void);
// 0x0000002D System.Void WeChatWASM.WX::ChooseImage(WeChatWASM.ChooseImageOption)
extern void WX_ChooseImage_m91105C56E3EC0A72D51AEB249A0BBBD712BD2967 (void);
// 0x0000002E System.Void WeChatWASM.WX::ChooseMedia(WeChatWASM.ChooseMediaOption)
extern void WX_ChooseMedia_m407CD403EFB2E5C0B80674E4DDC5AD45A2BEA894 (void);
// 0x0000002F System.Void WeChatWASM.WX::ChooseMessageFile(WeChatWASM.ChooseMessageFileOption)
extern void WX_ChooseMessageFile_mE8FE5B24D3EA5108660AE606F30FFE56BBAD22D8 (void);
// 0x00000030 System.Void WeChatWASM.WX::CloseBLEConnection(WeChatWASM.CloseBLEConnectionOption)
extern void WX_CloseBLEConnection_m35E39012F1E66457DB759C41449E07465E192377 (void);
// 0x00000031 System.Void WeChatWASM.WX::CloseBluetoothAdapter(WeChatWASM.CloseBluetoothAdapterOption)
extern void WX_CloseBluetoothAdapter_m2DBF9DE5A5417D661C9D8EF57297ACAD81FF7C52 (void);
// 0x00000032 System.Void WeChatWASM.WX::CreateBLEConnection(WeChatWASM.CreateBLEConnectionOption)
extern void WX_CreateBLEConnection_mCEFADF9215D46520928366E0D8D3E6E03F3ACFFC (void);
// 0x00000033 System.Void WeChatWASM.WX::CreateBLEPeripheralServer(WeChatWASM.CreateBLEPeripheralServerOption)
extern void WX_CreateBLEPeripheralServer_mAB2B660D269F254D85B6B9C0702EC0D5A1D73B56 (void);
// 0x00000034 System.Void WeChatWASM.WX::ExitMiniProgram(WeChatWASM.ExitMiniProgramOption)
extern void WX_ExitMiniProgram_mF6C76000B3DA25E9463407095B6D7306EB3EE0CC (void);
// 0x00000035 System.Void WeChatWASM.WX::ExitVoIPChat(WeChatWASM.ExitVoIPChatOption)
extern void WX_ExitVoIPChat_m52303BA45E801D69A2558D8483AA634B9A8FF81B (void);
// 0x00000036 System.Void WeChatWASM.WX::FaceDetect(WeChatWASM.FaceDetectOption)
extern void WX_FaceDetect_m91D283AC2CAE313D5822921C23187547A108065E (void);
// 0x00000037 System.Void WeChatWASM.WX::GetAvailableAudioSources(WeChatWASM.GetAvailableAudioSourcesOption)
extern void WX_GetAvailableAudioSources_mCB733D281426211B6E36E63E23945E09452FB61F (void);
// 0x00000038 System.Void WeChatWASM.WX::GetBLEDeviceCharacteristics(WeChatWASM.GetBLEDeviceCharacteristicsOption)
extern void WX_GetBLEDeviceCharacteristics_mAC239878D1D46925CEEEB34B5CB5F53A9A4FC861 (void);
// 0x00000039 System.Void WeChatWASM.WX::GetBLEDeviceRSSI(WeChatWASM.GetBLEDeviceRSSIOption)
extern void WX_GetBLEDeviceRSSI_m93F279B33A2CF6333609326B6833F92F022C6877 (void);
// 0x0000003A System.Void WeChatWASM.WX::GetBLEDeviceServices(WeChatWASM.GetBLEDeviceServicesOption)
extern void WX_GetBLEDeviceServices_mBA33C0210ECF4BBBAE76BB704922BC07648A1B72 (void);
// 0x0000003B System.Void WeChatWASM.WX::GetBLEMTU(WeChatWASM.GetBLEMTUOption)
extern void WX_GetBLEMTU_mC2C8EC5B921A1C69F573CB228794A25DF8579557 (void);
// 0x0000003C System.Void WeChatWASM.WX::GetBatteryInfo(WeChatWASM.GetBatteryInfoOption)
extern void WX_GetBatteryInfo_mF1D200447446584A8200481225D9D8DCD855C664 (void);
// 0x0000003D System.Void WeChatWASM.WX::GetBeacons(WeChatWASM.GetBeaconsOption)
extern void WX_GetBeacons_mD638164376369D6DF68A5FC76FB06B2226AD06A2 (void);
// 0x0000003E System.Void WeChatWASM.WX::GetBluetoothAdapterState(WeChatWASM.GetBluetoothAdapterStateOption)
extern void WX_GetBluetoothAdapterState_m35351B6BE83651467032F784772C010E4C8DC46B (void);
// 0x0000003F System.Void WeChatWASM.WX::GetBluetoothDevices(WeChatWASM.GetBluetoothDevicesOption)
extern void WX_GetBluetoothDevices_mF79DFE2233168BF5158DBB518D7BDF0F8D8D2394 (void);
// 0x00000040 System.Void WeChatWASM.WX::GetChannelsLiveInfo(WeChatWASM.GetChannelsLiveInfoOption)
extern void WX_GetChannelsLiveInfo_mDFC4DFA2DAD97112091039F1C842912EFE240ED2 (void);
// 0x00000041 System.Void WeChatWASM.WX::GetChannelsLiveNoticeInfo(WeChatWASM.GetChannelsLiveNoticeInfoOption)
extern void WX_GetChannelsLiveNoticeInfo_mA18973BA850AFB463928B3C1F460976D9AE76DA9 (void);
// 0x00000042 System.Void WeChatWASM.WX::GetClipboardData(WeChatWASM.GetClipboardDataOption)
extern void WX_GetClipboardData_m9F8E28B4AA77FEF2D46ED30BFBADCEB8C9D227D7 (void);
// 0x00000043 System.Void WeChatWASM.WX::GetConnectedBluetoothDevices(WeChatWASM.GetConnectedBluetoothDevicesOption)
extern void WX_GetConnectedBluetoothDevices_m300BA8A0AE8658BDFA92B8FA5318C3447FB64563 (void);
// 0x00000044 System.Void WeChatWASM.WX::GetExtConfig(WeChatWASM.GetExtConfigOption)
extern void WX_GetExtConfig_m5D346879B34BAAC30F6C9D7DBAC9E6A1459EE8A9 (void);
// 0x00000045 System.Void WeChatWASM.WX::GetGameClubData(WeChatWASM.GetGameClubDataOption)
extern void WX_GetGameClubData_m530E94A705F3CC60B05B28D4D7C12DB4B51E1B17 (void);
// 0x00000046 System.Void WeChatWASM.WX::GetGroupEnterInfo(WeChatWASM.GetGroupEnterInfoOption)
extern void WX_GetGroupEnterInfo_m427BB7E56456C147EC946E909BDA0B34ED531380 (void);
// 0x00000047 System.Void WeChatWASM.WX::GetInferenceEnvInfo(WeChatWASM.GetInferenceEnvInfoOption)
extern void WX_GetInferenceEnvInfo_m1AB30EB398D3F9668484AEE298BFAF660DA28E2D (void);
// 0x00000048 System.Void WeChatWASM.WX::GetLocalIPAddress(WeChatWASM.GetLocalIPAddressOption)
extern void WX_GetLocalIPAddress_mBABA1EC167855FCB32F629B426E1F0DCD8995311 (void);
// 0x00000049 System.Void WeChatWASM.WX::GetLocation(WeChatWASM.GetLocationOption)
extern void WX_GetLocation_m1B11281C169012CBC6BDFA932196031D0B5B8AEA (void);
// 0x0000004A System.Void WeChatWASM.WX::GetNetworkType(WeChatWASM.GetNetworkTypeOption)
extern void WX_GetNetworkType_mF0AD7074F82E3D44ACC9A911697FCD1FCE9CAA8A (void);
// 0x0000004B System.Void WeChatWASM.WX::GetPrivacySetting(WeChatWASM.GetPrivacySettingOption)
extern void WX_GetPrivacySetting_mD884ACAC69B5F60D229FAD2CF872FBE0311F718B (void);
// 0x0000004C System.Void WeChatWASM.WX::GetScreenBrightness(WeChatWASM.GetScreenBrightnessOption)
extern void WX_GetScreenBrightness_m6DED297EA603DC194754C86705CE2E1317557E88 (void);
// 0x0000004D System.Void WeChatWASM.WX::GetSetting(WeChatWASM.GetSettingOption)
extern void WX_GetSetting_mD183520167E8564FAC71FF52E43A459540A03594 (void);
// 0x0000004E System.Void WeChatWASM.WX::GetShareInfo(WeChatWASM.GetShareInfoOption)
extern void WX_GetShareInfo_mBC5D9AC16134CE2D69C07AC73B0BD63A1F39EAB5 (void);
// 0x0000004F System.Void WeChatWASM.WX::GetStorageInfo(WeChatWASM.GetStorageInfoOption)
extern void WX_GetStorageInfo_mC912088C154B358B8739A5A147862052693C3C83 (void);
// 0x00000050 System.Void WeChatWASM.WX::GetSystemInfo(WeChatWASM.GetSystemInfoOption)
extern void WX_GetSystemInfo_m03E0F4954A986C8EFFD0540ADE742537B605B548 (void);
// 0x00000051 System.Void WeChatWASM.WX::GetSystemInfoAsync(WeChatWASM.GetSystemInfoAsyncOption)
extern void WX_GetSystemInfoAsync_mF598CCB8D6AA9DF303DB3352953B1108D7915480 (void);
// 0x00000052 System.Void WeChatWASM.WX::GetUserInfo(WeChatWASM.GetUserInfoOption)
extern void WX_GetUserInfo_mC9192796C337DEEFEAC6F93937DC1C34F3284591 (void);
// 0x00000053 System.Void WeChatWASM.WX::GetUserInteractiveStorage(WeChatWASM.GetUserInteractiveStorageOption)
extern void WX_GetUserInteractiveStorage_m2ECF686D81D0D266F108B8ECD0BF22430A7832C6 (void);
// 0x00000054 System.Void WeChatWASM.WX::GetWeRunData(WeChatWASM.GetWeRunDataOption)
extern void WX_GetWeRunData_mA78F4865175552993F0DD6539EECA6421A386D68 (void);
// 0x00000055 System.Void WeChatWASM.WX::HideKeyboard(WeChatWASM.HideKeyboardOption)
extern void WX_HideKeyboard_mD7E431C98DB3B70B90D64C43DA9A4DBD7E28621D (void);
// 0x00000056 System.Void WeChatWASM.WX::HideLoading(WeChatWASM.HideLoadingOption)
extern void WX_HideLoading_m1A03F19CF336405459E4A310D7ACE302BEA14EEE (void);
// 0x00000057 System.Void WeChatWASM.WX::HideShareMenu(WeChatWASM.HideShareMenuOption)
extern void WX_HideShareMenu_m679D11B1824B59C2461F4F900B0AD5A4ED3B61F7 (void);
// 0x00000058 System.Void WeChatWASM.WX::HideToast(WeChatWASM.HideToastOption)
extern void WX_HideToast_mB0751528AD22C8389F875F9CD4F88DE1169B7E6B (void);
// 0x00000059 System.Void WeChatWASM.WX::InitFaceDetect(WeChatWASM.InitFaceDetectOption)
extern void WX_InitFaceDetect_m59D9B6C173CC4C68104CCD96530B308764B1863F (void);
// 0x0000005A System.Void WeChatWASM.WX::IsBluetoothDevicePaired(WeChatWASM.IsBluetoothDevicePairedOption)
extern void WX_IsBluetoothDevicePaired_mAFDF03BB5AF479CF7D413787DFCF170319710B52 (void);
// 0x0000005B System.Void WeChatWASM.WX::JoinVoIPChat(WeChatWASM.JoinVoIPChatOption)
extern void WX_JoinVoIPChat_m1C2B935ED9281FAD128D396AFA36DECB7D694E38 (void);
// 0x0000005C System.Void WeChatWASM.WX::Login(WeChatWASM.LoginOption)
extern void WX_Login_m567689292667BF1FDCFFF885692F2C968B73AE9B (void);
// 0x0000005D System.Void WeChatWASM.WX::MakeBluetoothPair(WeChatWASM.MakeBluetoothPairOption)
extern void WX_MakeBluetoothPair_m4CF10069DEC408B8B0075C22507E40AB91E031F5 (void);
// 0x0000005E System.Void WeChatWASM.WX::NavigateToMiniProgram(WeChatWASM.NavigateToMiniProgramOption)
extern void WX_NavigateToMiniProgram_mD71EB4720A7AB335219812B61C2BF1D9D8686830 (void);
// 0x0000005F System.Void WeChatWASM.WX::NotifyBLECharacteristicValueChange(WeChatWASM.NotifyBLECharacteristicValueChangeOption)
extern void WX_NotifyBLECharacteristicValueChange_m7E2FDC734E22F56042BC66AA2EA19D7FABE490DE (void);
// 0x00000060 System.Void WeChatWASM.WX::OpenAppAuthorizeSetting(WeChatWASM.OpenAppAuthorizeSettingOption)
extern void WX_OpenAppAuthorizeSetting_mBEB459613AD27A79684762375F88CCAD93573416 (void);
// 0x00000061 System.Void WeChatWASM.WX::OpenBluetoothAdapter(WeChatWASM.OpenBluetoothAdapterOption)
extern void WX_OpenBluetoothAdapter_mFBFF84066F63B431E68A67983033B1FB97EAD2A8 (void);
// 0x00000062 System.Void WeChatWASM.WX::OpenCard(WeChatWASM.OpenCardOption)
extern void WX_OpenCard_mE8D2E5395DA95EE1CB76A27BEBC6446E789E288F (void);
// 0x00000063 System.Void WeChatWASM.WX::OpenChannelsActivity(WeChatWASM.OpenChannelsActivityOption)
extern void WX_OpenChannelsActivity_m9E0441A39633269EAD53E709392C202F049ED271 (void);
// 0x00000064 System.Void WeChatWASM.WX::OpenChannelsEvent(WeChatWASM.OpenChannelsEventOption)
extern void WX_OpenChannelsEvent_m846C1C81E4D51EA8C50D16FD276DDB7EAFF78F85 (void);
// 0x00000065 System.Void WeChatWASM.WX::OpenChannelsLive(WeChatWASM.OpenChannelsLiveOption)
extern void WX_OpenChannelsLive_m2BF5842EA2F0FA9125FF070DAA7B781C09C3CC03 (void);
// 0x00000066 System.Void WeChatWASM.WX::OpenChannelsUserProfile(WeChatWASM.OpenChannelsUserProfileOption)
extern void WX_OpenChannelsUserProfile_mE38D12DE4410ED85D5DED6AF8FDF2F6E7041D9F6 (void);
// 0x00000067 System.Void WeChatWASM.WX::OpenCustomerServiceChat(WeChatWASM.OpenCustomerServiceChatOption)
extern void WX_OpenCustomerServiceChat_mCAD961E8155549B7960121AB77B41647399ADDB4 (void);
// 0x00000068 System.Void WeChatWASM.WX::OpenCustomerServiceConversation(WeChatWASM.OpenCustomerServiceConversationOption)
extern void WX_OpenCustomerServiceConversation_mADA29F7F28614735BB7D9D6748A8DD93AA1E7E2C (void);
// 0x00000069 System.Void WeChatWASM.WX::OpenPrivacyContract(WeChatWASM.OpenPrivacyContractOption)
extern void WX_OpenPrivacyContract_mD81129074C91999CB8B87FAAE1A539122D44AC50 (void);
// 0x0000006A System.Void WeChatWASM.WX::OpenSetting(WeChatWASM.OpenSettingOption)
extern void WX_OpenSetting_mFE69B196162E0C68EBD1B763531C3BBB08BE3851 (void);
// 0x0000006B System.Void WeChatWASM.WX::OpenSystemBluetoothSetting(WeChatWASM.OpenSystemBluetoothSettingOption)
extern void WX_OpenSystemBluetoothSetting_mDFF7CB0D6D8BE237FC174004C849997EBE4CEC89 (void);
// 0x0000006C System.Void WeChatWASM.WX::PreviewImage(WeChatWASM.PreviewImageOption)
extern void WX_PreviewImage_m157D02B943FF18CF499D6881F27CBE911FB74090 (void);
// 0x0000006D System.Void WeChatWASM.WX::PreviewMedia(WeChatWASM.PreviewMediaOption)
extern void WX_PreviewMedia_m321409A69310EE36DB2B71B56399FA37F1B9D483 (void);
// 0x0000006E System.Void WeChatWASM.WX::ReadBLECharacteristicValue(WeChatWASM.ReadBLECharacteristicValueOption)
extern void WX_ReadBLECharacteristicValue_m9CAB4D43007006EF6DDCB21BE7E8A2AD6E869522 (void);
// 0x0000006F System.Void WeChatWASM.WX::RemoveStorage(WeChatWASM.RemoveStorageOption)
extern void WX_RemoveStorage_m9CD196A7A34D71DC91373D83C7B3A88B46A4562B (void);
// 0x00000070 System.Void WeChatWASM.WX::RemoveUserCloudStorage(WeChatWASM.RemoveUserCloudStorageOption)
extern void WX_RemoveUserCloudStorage_m26B25B1BEF0C9C37CC61C0F352402363C0AC6660 (void);
// 0x00000071 System.Void WeChatWASM.WX::ReportScene(WeChatWASM.ReportSceneOption)
extern void WX_ReportScene_m38997DDAB26515C815C80B634DFF5EABC86DC840 (void);
// 0x00000072 System.Void WeChatWASM.WX::RequestMidasFriendPayment(WeChatWASM.RequestMidasFriendPaymentOption)
extern void WX_RequestMidasFriendPayment_m80F96762F4F2721E4451E08DB9E5D5934AE421DB (void);
// 0x00000073 System.Void WeChatWASM.WX::RequestMidasPayment(WeChatWASM.RequestMidasPaymentOption)
extern void WX_RequestMidasPayment_m11C5992F3BA55AFBA712E1A6E8FB8922E1E6718A (void);
// 0x00000074 System.Void WeChatWASM.WX::RequestSubscribeMessage(WeChatWASM.RequestSubscribeMessageOption)
extern void WX_RequestSubscribeMessage_m152F9691C83E1CFDDBE102A0C8AFE9ADDC72906B (void);
// 0x00000075 System.Void WeChatWASM.WX::RequestSubscribeSystemMessage(WeChatWASM.RequestSubscribeSystemMessageOption)
extern void WX_RequestSubscribeSystemMessage_m32FAC05CF7F988D15B84A42F2B9BE57C01F68A21 (void);
// 0x00000076 System.Void WeChatWASM.WX::RequirePrivacyAuthorize(WeChatWASM.RequirePrivacyAuthorizeOption)
extern void WX_RequirePrivacyAuthorize_mCED035EEEA90B241A54B3575017A1AF8BB65EDA1 (void);
// 0x00000077 System.Void WeChatWASM.WX::RestartMiniProgram(WeChatWASM.RestartMiniProgramOption)
extern void WX_RestartMiniProgram_m27595E302BEB7A851150A7B0026E90C21E7AAE1D (void);
// 0x00000078 System.Void WeChatWASM.WX::SaveFileToDisk(WeChatWASM.SaveFileToDiskOption)
extern void WX_SaveFileToDisk_m1EC7D3431D0F5A8E50754D911FE04CEB3C24B8AD (void);
// 0x00000079 System.Void WeChatWASM.WX::SaveImageToPhotosAlbum(WeChatWASM.SaveImageToPhotosAlbumOption)
extern void WX_SaveImageToPhotosAlbum_m04BBAA43B8AD44B7C2F4CFE48675B7FD01CFAA4B (void);
// 0x0000007A System.Void WeChatWASM.WX::ScanCode(WeChatWASM.ScanCodeOption)
extern void WX_ScanCode_m7914319E183F9EB7F1AF879325DC4B8A2703C748 (void);
// 0x0000007B System.Void WeChatWASM.WX::SetBLEMTU(WeChatWASM.SetBLEMTUOption)
extern void WX_SetBLEMTU_m5D5769CAC74D715A245E69C5B4B4F339FCBA0339 (void);
// 0x0000007C System.Void WeChatWASM.WX::SetClipboardData(WeChatWASM.SetClipboardDataOption)
extern void WX_SetClipboardData_mFA64BC61DAA2BF7FA4AF20305E40BD6D7C0F01DE (void);
// 0x0000007D System.Void WeChatWASM.WX::SetDeviceOrientation(WeChatWASM.SetDeviceOrientationOption)
extern void WX_SetDeviceOrientation_m2BF5FC38ACEAF884F398313DFC5E3B7535928CEC (void);
// 0x0000007E System.Void WeChatWASM.WX::SetEnableDebug(WeChatWASM.SetEnableDebugOption)
extern void WX_SetEnableDebug_m9B6280C2AB03F615869A1E66D933DB562DC55B1D (void);
// 0x0000007F System.Void WeChatWASM.WX::SetInnerAudioOption(WeChatWASM.SetInnerAudioOption)
extern void WX_SetInnerAudioOption_mA41D8E4BBB64BDE03FBECE4846538F5E72500540 (void);
// 0x00000080 System.Void WeChatWASM.WX::SetKeepScreenOn(WeChatWASM.SetKeepScreenOnOption)
extern void WX_SetKeepScreenOn_mC5805B646624083BB1B3CE41F403DB305F8B4623 (void);
// 0x00000081 System.Void WeChatWASM.WX::SetMenuStyle(WeChatWASM.SetMenuStyleOption)
extern void WX_SetMenuStyle_mCF4FB4B1F9BA417C75C21E5FFDD1D8D7E23C8E33 (void);
// 0x00000082 System.Void WeChatWASM.WX::SetScreenBrightness(WeChatWASM.SetScreenBrightnessOption)
extern void WX_SetScreenBrightness_m87AD6689F0EAE32B72C2D2425BD992E4541768AD (void);
// 0x00000083 System.Void WeChatWASM.WX::SetStatusBarStyle(WeChatWASM.SetStatusBarStyleOption)
extern void WX_SetStatusBarStyle_m2E7F1C79407AD963E9B8FB1F20EB4DAA816335A4 (void);
// 0x00000084 System.Void WeChatWASM.WX::SetUserCloudStorage(WeChatWASM.SetUserCloudStorageOption)
extern void WX_SetUserCloudStorage_m48082A86EC881A3468D26E16126E2DEB89A63724 (void);
// 0x00000085 System.Void WeChatWASM.WX::ShowActionSheet(WeChatWASM.ShowActionSheetOption)
extern void WX_ShowActionSheet_m81C0F0383C3D9C3D70800E74634B2F3012FC328B (void);
// 0x00000086 System.Void WeChatWASM.WX::ShowKeyboard(WeChatWASM.ShowKeyboardOption)
extern void WX_ShowKeyboard_mA8ECDCA4C622EB999F751E1D9E84EF70833E496D (void);
// 0x00000087 System.Void WeChatWASM.WX::ShowLoading(WeChatWASM.ShowLoadingOption)
extern void WX_ShowLoading_m701A4FF17D8B8DEFE4AB69815FF91002251B2B1D (void);
// 0x00000088 System.Void WeChatWASM.WX::ShowModal(WeChatWASM.ShowModalOption)
extern void WX_ShowModal_mAC00C62F494943AA6BAB838F452B67E9536F2253 (void);
// 0x00000089 System.Void WeChatWASM.WX::ShowShareImageMenu(WeChatWASM.ShowShareImageMenuOption)
extern void WX_ShowShareImageMenu_m71E539E9E338DF80E45D9A360866A5D8DF4BA383 (void);
// 0x0000008A System.Void WeChatWASM.WX::ShowShareMenu(WeChatWASM.ShowShareMenuOption)
extern void WX_ShowShareMenu_m128A46E319A0FA7C94B478E127F29E44C6219C98 (void);
// 0x0000008B System.Void WeChatWASM.WX::ShowToast(WeChatWASM.ShowToastOption)
extern void WX_ShowToast_m3FB2BF57219E0CA1EF681A1BCB4A9A00810AF368 (void);
// 0x0000008C System.Void WeChatWASM.WX::StartAccelerometer(WeChatWASM.StartAccelerometerOption)
extern void WX_StartAccelerometer_m0457619ADA3C56DF082802DF8C201EE436A6A613 (void);
// 0x0000008D System.Void WeChatWASM.WX::StartBeaconDiscovery(WeChatWASM.StartBeaconDiscoveryOption)
extern void WX_StartBeaconDiscovery_m8209CB7249BF9E3720A6CF04BF6A61748A48B480 (void);
// 0x0000008E System.Void WeChatWASM.WX::StartBluetoothDevicesDiscovery(WeChatWASM.StartBluetoothDevicesDiscoveryOption)
extern void WX_StartBluetoothDevicesDiscovery_m97076532A6D8CB26BCAD3BF47E6C48AE72AFE96A (void);
// 0x0000008F System.Void WeChatWASM.WX::StartCompass(WeChatWASM.StartCompassOption)
extern void WX_StartCompass_m33D308E061253C6F729012306FD3CD1745BBA8B5 (void);
// 0x00000090 System.Void WeChatWASM.WX::StartDeviceMotionListening(WeChatWASM.StartDeviceMotionListeningOption)
extern void WX_StartDeviceMotionListening_mAF5A25BCE648AF76B95AA2823FA00C70344BFAD4 (void);
// 0x00000091 System.Void WeChatWASM.WX::StartGyroscope(WeChatWASM.StartGyroscopeOption)
extern void WX_StartGyroscope_m7033AFB2F7BE58BDFB6F490C211C72435F171AF9 (void);
// 0x00000092 System.Void WeChatWASM.WX::StopAccelerometer(WeChatWASM.StopAccelerometerOption)
extern void WX_StopAccelerometer_m2E06CA0F6D4826E1825995FC0D738430E5E38F25 (void);
// 0x00000093 System.Void WeChatWASM.WX::StopBeaconDiscovery(WeChatWASM.StopBeaconDiscoveryOption)
extern void WX_StopBeaconDiscovery_m2C2E0B98DD44EE1934F51B458C603ECB9BCCEED3 (void);
// 0x00000094 System.Void WeChatWASM.WX::StopBluetoothDevicesDiscovery(WeChatWASM.StopBluetoothDevicesDiscoveryOption)
extern void WX_StopBluetoothDevicesDiscovery_m259BA14705CC8BB65D5A5A8658949B41C632D1C8 (void);
// 0x00000095 System.Void WeChatWASM.WX::StopCompass(WeChatWASM.StopCompassOption)
extern void WX_StopCompass_m99A765AE57DE230BACD904EF824990527781FA45 (void);
// 0x00000096 System.Void WeChatWASM.WX::StopDeviceMotionListening(WeChatWASM.StopDeviceMotionListeningOption)
extern void WX_StopDeviceMotionListening_m938B39AAFF397F3FB38D28C53EB159070D39C51C (void);
// 0x00000097 System.Void WeChatWASM.WX::StopFaceDetect(WeChatWASM.StopFaceDetectOption)
extern void WX_StopFaceDetect_m18C070F02940A0256C0F6178A68502ACC55BCA3A (void);
// 0x00000098 System.Void WeChatWASM.WX::StopGyroscope(WeChatWASM.StopGyroscopeOption)
extern void WX_StopGyroscope_m13DC19DCBDA3F7C819BFA5933A68FCA3C0AEFBBA (void);
// 0x00000099 System.Void WeChatWASM.WX::UpdateKeyboard(WeChatWASM.UpdateKeyboardOption)
extern void WX_UpdateKeyboard_mD22267BB0B50FC51E31BC3B80CB695BD923964D4 (void);
// 0x0000009A System.Void WeChatWASM.WX::UpdateShareMenu(WeChatWASM.UpdateShareMenuOption)
extern void WX_UpdateShareMenu_m8E6C41B25BDD9DD9F8EF24C87DAA3673E7DBF6D6 (void);
// 0x0000009B System.Void WeChatWASM.WX::UpdateVoIPChatMuteConfig(WeChatWASM.UpdateVoIPChatMuteConfigOption)
extern void WX_UpdateVoIPChatMuteConfig_mCCBF5A1F3E087F3C0AC650C024E9EE5148C05338 (void);
// 0x0000009C System.Void WeChatWASM.WX::UpdateWeChatApp(WeChatWASM.UpdateWeChatAppOption)
extern void WX_UpdateWeChatApp_mB504E97373EA3D3B3EE214ED9FB3E059C3EDF53E (void);
// 0x0000009D System.Void WeChatWASM.WX::VibrateLong(WeChatWASM.VibrateLongOption)
extern void WX_VibrateLong_mCA90CE14D01C874AAAAA1E54E4899DFA11C8A361 (void);
// 0x0000009E System.Void WeChatWASM.WX::VibrateShort(WeChatWASM.VibrateShortOption)
extern void WX_VibrateShort_mD8B1AB726E39AD5DFE2B0A32AD77447D87455B5F (void);
// 0x0000009F System.Void WeChatWASM.WX::WriteBLECharacteristicValue(WeChatWASM.WriteBLECharacteristicValueOption)
extern void WX_WriteBLECharacteristicValue_m9D710350E6D6A68BF74B3C386EDFEC742E27AD24 (void);
// 0x000000A0 System.Void WeChatWASM.WX::StartGameLive(WeChatWASM.StartGameLiveOption)
extern void WX_StartGameLive_m2A829D8AD5E7F8C57503957CAF583FD6D07697BA (void);
// 0x000000A1 System.Void WeChatWASM.WX::CheckGameLiveEnabled(WeChatWASM.CheckGameLiveEnabledOption)
extern void WX_CheckGameLiveEnabled_mAC7087E01472E4F68ACA5B35B2F41D1E4264DB42 (void);
// 0x000000A2 System.Void WeChatWASM.WX::GetUserCurrentGameliveInfo(WeChatWASM.GetUserCurrentGameliveInfoOption)
extern void WX_GetUserCurrentGameliveInfo_m472AABF0B999E9077616B410C329FBC003E5258C (void);
// 0x000000A3 System.Void WeChatWASM.WX::GetUserRecentGameLiveInfo(WeChatWASM.GetUserRecentGameLiveInfoOption)
extern void WX_GetUserRecentGameLiveInfo_m0B2A2A5165C1CD17E4EDEBE22B0C4553DBB7EC68 (void);
// 0x000000A4 System.Void WeChatWASM.WX::GetUserGameLiveDetails(WeChatWASM.GetUserGameLiveDetailsOption)
extern void WX_GetUserGameLiveDetails_m95D4781C09C31BB369E4C7B49118457EF3CCF553 (void);
// 0x000000A5 System.Void WeChatWASM.WX::OpenChannelsLiveCollection(WeChatWASM.OpenChannelsLiveCollectionOption)
extern void WX_OpenChannelsLiveCollection_m68A56227A2D649920DDCAC624B3FC9B9834A5936 (void);
// 0x000000A6 System.Void WeChatWASM.WX::OpenPage(WeChatWASM.OpenPageOption)
extern void WX_OpenPage_mDCADD860C8F84603B7F3C9929190E4AD0D754BFD (void);
// 0x000000A7 System.Void WeChatWASM.WX::RequestMidasPaymentGameItem(WeChatWASM.RequestMidasPaymentGameItemOption)
extern void WX_RequestMidasPaymentGameItem_mA1CA4836E316842C1076218A239D425D4E31F536 (void);
// 0x000000A8 System.Void WeChatWASM.WX::RequestSubscribeLiveActivity(WeChatWASM.RequestSubscribeLiveActivityOption)
extern void WX_RequestSubscribeLiveActivity_m57C63A804C17B08F99974E2F0271F0286370342B (void);
// 0x000000A9 System.Void WeChatWASM.WX::OperateGameRecorderVideo(WeChatWASM.OperateGameRecorderVideoOption)
extern void WX_OperateGameRecorderVideo_m9E6DDC2798E998C54833F1C990AC2353DA13166A (void);
// 0x000000AA System.Void WeChatWASM.WX::RemoveStorageSync(System.String)
extern void WX_RemoveStorageSync_mBDBF32C3F6E3C5C336613A7FA0A58A8F82D07975 (void);
// 0x000000AB System.Void WeChatWASM.WX::ReportEvent(System.String,T)
// 0x000000AC System.Void WeChatWASM.WX::ReportMonitor(System.String,System.Double)
extern void WX_ReportMonitor_m0E12B8CC7329AB3B752BBED4E7077C65ADACFF30 (void);
// 0x000000AD System.Void WeChatWASM.WX::ReportPerformance(System.Double,System.Double,System.String)
extern void WX_ReportPerformance_m8606B6D0A576F2A667C76E0DB96E60651F88848D (void);
// 0x000000AE System.Void WeChatWASM.WX::ReportUserBehaviorBranchAnalytics(WeChatWASM.ReportUserBehaviorBranchAnalyticsOption)
extern void WX_ReportUserBehaviorBranchAnalytics_m01CAA6494A983AB51C2ED4E43D548E90161B623D (void);
// 0x000000AF System.Void WeChatWASM.WX::ReserveChannelsLive(WeChatWASM.ReserveChannelsLiveOption)
extern void WX_ReserveChannelsLive_m765D9419E48558C87BAC41B7F6AFA3BB52CD5341 (void);
// 0x000000B0 System.Void WeChatWASM.WX::RevokeBufferURL(System.String)
extern void WX_RevokeBufferURL_m43171A0D3AD9D76082E5579F1A2EAD0CB11620C0 (void);
// 0x000000B1 System.Void WeChatWASM.WX::SetPreferredFramesPerSecond(System.Double)
extern void WX_SetPreferredFramesPerSecond_m653A9990E5864AC367B6D0F20D51D58D778B0F76 (void);
// 0x000000B2 System.Void WeChatWASM.WX::SetStorageSync(System.String,T)
// 0x000000B3 System.Void WeChatWASM.WX::ShareAppMessage(WeChatWASM.ShareAppMessageOption)
extern void WX_ShareAppMessage_m17568A390253F08CC37C768FCC63A0474E7FF70F (void);
// 0x000000B4 System.Void WeChatWASM.WX::TriggerGC()
extern void WX_TriggerGC_mDB5E018E301B44511F2E6CB1085F24F137BC5CD9 (void);
// 0x000000B5 System.Void WeChatWASM.WX::OnAccelerometerChange(System.Action`1<WeChatWASM.OnAccelerometerChangeListenerResult>)
extern void WX_OnAccelerometerChange_m69C4E0C86470DFCFC71425748E2B89DED394D206 (void);
// 0x000000B6 System.Void WeChatWASM.WX::OffAccelerometerChange(System.Action`1<WeChatWASM.OnAccelerometerChangeListenerResult>)
extern void WX_OffAccelerometerChange_m5C960EF02F074DFF13E0215DB81891C057FF885B (void);
// 0x000000B7 System.Void WeChatWASM.WX::OnAudioInterruptionBegin(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WX_OnAudioInterruptionBegin_m7FEFD73BE493723F93F1C73143F328B3926FB22E (void);
// 0x000000B8 System.Void WeChatWASM.WX::OffAudioInterruptionBegin(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WX_OffAudioInterruptionBegin_m5CF1468843272B781314FBACA68D1100BEB40A33 (void);
// 0x000000B9 System.Void WeChatWASM.WX::OnAudioInterruptionEnd(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WX_OnAudioInterruptionEnd_m5DE41C091F3E1FFCD88C5627266ABFFC4BD573E4 (void);
// 0x000000BA System.Void WeChatWASM.WX::OffAudioInterruptionEnd(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WX_OffAudioInterruptionEnd_mEBDC5B5BDD867B4901A093237EC0BD7EADE8BC2C (void);
// 0x000000BB System.Void WeChatWASM.WX::OnBLECharacteristicValueChange(System.Action`1<WeChatWASM.OnBLECharacteristicValueChangeListenerResult>)
extern void WX_OnBLECharacteristicValueChange_mCE4F21B6C5CC524D49118212217E51832EBEC0D1 (void);
// 0x000000BC System.Void WeChatWASM.WX::OffBLECharacteristicValueChange(System.Action`1<WeChatWASM.OnBLECharacteristicValueChangeListenerResult>)
extern void WX_OffBLECharacteristicValueChange_mC7B49E4BE9577E0A788CE75021E0B12FFB498DEB (void);
// 0x000000BD System.Void WeChatWASM.WX::OnBLEConnectionStateChange(System.Action`1<WeChatWASM.OnBLEConnectionStateChangeListenerResult>)
extern void WX_OnBLEConnectionStateChange_mA1EDA3ADDD259FAFA49142BF296E3F0265A2A075 (void);
// 0x000000BE System.Void WeChatWASM.WX::OffBLEConnectionStateChange(System.Action`1<WeChatWASM.OnBLEConnectionStateChangeListenerResult>)
extern void WX_OffBLEConnectionStateChange_m80774F9EC7947A59B92844540B09C8ABD548E8AC (void);
// 0x000000BF System.Void WeChatWASM.WX::OnBLEMTUChange(System.Action`1<WeChatWASM.OnBLEMTUChangeListenerResult>)
extern void WX_OnBLEMTUChange_mECE1C9E98ECF7642DBF58670A8CD5EE7E99C2A60 (void);
// 0x000000C0 System.Void WeChatWASM.WX::OffBLEMTUChange(System.Action`1<WeChatWASM.OnBLEMTUChangeListenerResult>)
extern void WX_OffBLEMTUChange_mC2D6D608D72069072F42B8DFDB2D7B3E93AD5806 (void);
// 0x000000C1 System.Void WeChatWASM.WX::OnBLEPeripheralConnectionStateChanged(System.Action`1<WeChatWASM.OnBLEPeripheralConnectionStateChangedListenerResult>)
extern void WX_OnBLEPeripheralConnectionStateChanged_m819225ED6C74C3F3AA4D3BA771CC446A48BC3602 (void);
// 0x000000C2 System.Void WeChatWASM.WX::OffBLEPeripheralConnectionStateChanged(System.Action`1<WeChatWASM.OnBLEPeripheralConnectionStateChangedListenerResult>)
extern void WX_OffBLEPeripheralConnectionStateChanged_m0961F1B5AB77C3CAB3C539B8AEEF6FC2E03EC2FC (void);
// 0x000000C3 System.Void WeChatWASM.WX::OnBeaconServiceChange(System.Action`1<WeChatWASM.OnBeaconServiceChangeListenerResult>)
extern void WX_OnBeaconServiceChange_mA1CDE994590EA9F382E9D7C928EA6F198F49F0B8 (void);
// 0x000000C4 System.Void WeChatWASM.WX::OffBeaconServiceChange(System.Action`1<WeChatWASM.OnBeaconServiceChangeListenerResult>)
extern void WX_OffBeaconServiceChange_m4C0553750CCF39C5CC693FD1D88AE9D93DAF5609 (void);
// 0x000000C5 System.Void WeChatWASM.WX::OnBeaconUpdate(System.Action`1<WeChatWASM.OnBeaconUpdateListenerResult>)
extern void WX_OnBeaconUpdate_mF5C8D6FEB91291B235B3C7B80D5B6E9B0B6A4A9E (void);
// 0x000000C6 System.Void WeChatWASM.WX::OffBeaconUpdate(System.Action`1<WeChatWASM.OnBeaconUpdateListenerResult>)
extern void WX_OffBeaconUpdate_m8DF8354FC4E94A289A6133730A191A35C19618D4 (void);
// 0x000000C7 System.Void WeChatWASM.WX::OnBluetoothAdapterStateChange(System.Action`1<WeChatWASM.OnBluetoothAdapterStateChangeListenerResult>)
extern void WX_OnBluetoothAdapterStateChange_m9A60AA097442D3D9F6C25197D34D033D6F437F00 (void);
// 0x000000C8 System.Void WeChatWASM.WX::OffBluetoothAdapterStateChange(System.Action`1<WeChatWASM.OnBluetoothAdapterStateChangeListenerResult>)
extern void WX_OffBluetoothAdapterStateChange_mF5B3415DB2CB116D8A246C14E67B522144D5B72D (void);
// 0x000000C9 System.Void WeChatWASM.WX::OnBluetoothDeviceFound(System.Action`1<WeChatWASM.OnBluetoothDeviceFoundListenerResult>)
extern void WX_OnBluetoothDeviceFound_m120BF2E4C9B12370849EBF45F2B25D0BF5B8959F (void);
// 0x000000CA System.Void WeChatWASM.WX::OffBluetoothDeviceFound(System.Action`1<WeChatWASM.OnBluetoothDeviceFoundListenerResult>)
extern void WX_OffBluetoothDeviceFound_m6D3F9F56516F8DD759878789B04114C6851012BA (void);
// 0x000000CB System.Void WeChatWASM.WX::OnCompassChange(System.Action`1<WeChatWASM.OnCompassChangeListenerResult>)
extern void WX_OnCompassChange_mA4675988ADE8C3D083B737146A1D4211778A40D7 (void);
// 0x000000CC System.Void WeChatWASM.WX::OffCompassChange(System.Action`1<WeChatWASM.OnCompassChangeListenerResult>)
extern void WX_OffCompassChange_mEFF58A35CB01D11651369991D7BCCB6BC4781009 (void);
// 0x000000CD System.Void WeChatWASM.WX::OnDeviceMotionChange(System.Action`1<WeChatWASM.OnDeviceMotionChangeListenerResult>)
extern void WX_OnDeviceMotionChange_m33DDD0526342B659CF364E4C2001FE2BA0A6D2B5 (void);
// 0x000000CE System.Void WeChatWASM.WX::OffDeviceMotionChange(System.Action`1<WeChatWASM.OnDeviceMotionChangeListenerResult>)
extern void WX_OffDeviceMotionChange_m14254B2592DEF109DF3141860FC0CD4219910638 (void);
// 0x000000CF System.Void WeChatWASM.WX::OnDeviceOrientationChange(System.Action`1<WeChatWASM.OnDeviceOrientationChangeListenerResult>)
extern void WX_OnDeviceOrientationChange_m84295A270850136940E188649E780F7D4A4A550D (void);
// 0x000000D0 System.Void WeChatWASM.WX::OffDeviceOrientationChange(System.Action`1<WeChatWASM.OnDeviceOrientationChangeListenerResult>)
extern void WX_OffDeviceOrientationChange_m44C5F1F5B42572628915080215F814B85285D6C7 (void);
// 0x000000D1 System.Void WeChatWASM.WX::OnError(System.Action`1<WeChatWASM.WxOnErrorCallbackResult>)
extern void WX_OnError_m18B3D38CA286841E0FE4C2090285DD26C5F43430 (void);
// 0x000000D2 System.Void WeChatWASM.WX::OffError(System.Action`1<WeChatWASM.WxOnErrorCallbackResult>)
extern void WX_OffError_m08AA6A8FA19F105833934D8245AB1622B2E9C88F (void);
// 0x000000D3 System.Void WeChatWASM.WX::OnGyroscopeChange(System.Action`1<WeChatWASM.OnGyroscopeChangeListenerResult>)
extern void WX_OnGyroscopeChange_mA83A73F8D217586EC7CF73F9108D9C2870D0162F (void);
// 0x000000D4 System.Void WeChatWASM.WX::OffGyroscopeChange(System.Action`1<WeChatWASM.OnGyroscopeChangeListenerResult>)
extern void WX_OffGyroscopeChange_mE8405E72F39520074BD73AE0A440F8C77B2EE0E5 (void);
// 0x000000D5 System.Void WeChatWASM.WX::OnHide(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WX_OnHide_mB9AB7FF2DDB18B07545B722CC29DB11F9176B0DD (void);
// 0x000000D6 System.Void WeChatWASM.WX::OffHide(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WX_OffHide_mD678A7B6D946B9A8F1287B7329F4D224239772B1 (void);
// 0x000000D7 System.Void WeChatWASM.WX::OnInteractiveStorageModified(System.Action`1<System.String>)
extern void WX_OnInteractiveStorageModified_m9FC9D20E21CDE8CB9C1DD5B95181D0E29412EEFB (void);
// 0x000000D8 System.Void WeChatWASM.WX::OffInteractiveStorageModified(System.Action`1<System.String>)
extern void WX_OffInteractiveStorageModified_m7AB05AE0871C6FE3C965690B910F0EFBE8E855FD (void);
// 0x000000D9 System.Void WeChatWASM.WX::OnKeyDown(System.Action`1<WeChatWASM.OnKeyDownListenerResult>)
extern void WX_OnKeyDown_m8242A49CDED28314AA6CE0616D0AE2826D02D060 (void);
// 0x000000DA System.Void WeChatWASM.WX::OffKeyDown(System.Action`1<WeChatWASM.OnKeyDownListenerResult>)
extern void WX_OffKeyDown_mE44744160A0E57FCD8FE579B47350F51BAE8D8EB (void);
// 0x000000DB System.Void WeChatWASM.WX::OnKeyUp(System.Action`1<WeChatWASM.OnKeyDownListenerResult>)
extern void WX_OnKeyUp_m36C3D4C2B929E4249E58731CC7AA828E49D2AF3D (void);
// 0x000000DC System.Void WeChatWASM.WX::OffKeyUp(System.Action`1<WeChatWASM.OnKeyDownListenerResult>)
extern void WX_OffKeyUp_m94C5EF86B2D8289569F2EB6E267556569BC44636 (void);
// 0x000000DD System.Void WeChatWASM.WX::OnKeyboardComplete(System.Action`1<WeChatWASM.OnKeyboardInputListenerResult>)
extern void WX_OnKeyboardComplete_mB1C0C2D0C057C4FE1D6D2CB770A0A5DBFD34B3C5 (void);
// 0x000000DE System.Void WeChatWASM.WX::OffKeyboardComplete(System.Action`1<WeChatWASM.OnKeyboardInputListenerResult>)
extern void WX_OffKeyboardComplete_mC24563CAD3EB6C40D298D47CC9EB6EF841ED0A73 (void);
// 0x000000DF System.Void WeChatWASM.WX::OnKeyboardConfirm(System.Action`1<WeChatWASM.OnKeyboardInputListenerResult>)
extern void WX_OnKeyboardConfirm_mB9C71394AB63AD4FBDF263BA35A46D59411FEB22 (void);
// 0x000000E0 System.Void WeChatWASM.WX::OffKeyboardConfirm(System.Action`1<WeChatWASM.OnKeyboardInputListenerResult>)
extern void WX_OffKeyboardConfirm_mEEB0404604C77CE4FA0EB82DE942647708734941 (void);
// 0x000000E1 System.Void WeChatWASM.WX::OnKeyboardHeightChange(System.Action`1<WeChatWASM.OnKeyboardHeightChangeListenerResult>)
extern void WX_OnKeyboardHeightChange_m457F6520C7EDFBD607C05371E7335291142D41CB (void);
// 0x000000E2 System.Void WeChatWASM.WX::OffKeyboardHeightChange(System.Action`1<WeChatWASM.OnKeyboardHeightChangeListenerResult>)
extern void WX_OffKeyboardHeightChange_m97A5D45531BD293E16D2A417172CE15A0362605D (void);
// 0x000000E3 System.Void WeChatWASM.WX::OnKeyboardInput(System.Action`1<WeChatWASM.OnKeyboardInputListenerResult>)
extern void WX_OnKeyboardInput_m7BD19745552FA5872F85371A508E53FFAF88220E (void);
// 0x000000E4 System.Void WeChatWASM.WX::OffKeyboardInput(System.Action`1<WeChatWASM.OnKeyboardInputListenerResult>)
extern void WX_OffKeyboardInput_m91305A65C78A90166BB73A903EB1D1F0D69C5404 (void);
// 0x000000E5 System.Void WeChatWASM.WX::OnMemoryWarning(System.Action`1<WeChatWASM.OnMemoryWarningListenerResult>)
extern void WX_OnMemoryWarning_mC863C25664F117554E3073F124C73CBDF674C471 (void);
// 0x000000E6 System.Void WeChatWASM.WX::OffMemoryWarning(System.Action`1<WeChatWASM.OnMemoryWarningListenerResult>)
extern void WX_OffMemoryWarning_m0AA84F6C45E5CA5B689301D8E4B74ED73E598343 (void);
// 0x000000E7 System.Void WeChatWASM.WX::OnMessage(System.Action`1<System.String>)
extern void WX_OnMessage_mCA87915EDA12F7B412F28B08D835A4D7910DA9B0 (void);
// 0x000000E8 System.Void WeChatWASM.WX::OnMouseDown(System.Action`1<WeChatWASM.OnMouseDownListenerResult>)
extern void WX_OnMouseDown_m2A3C98B376110692F898E13F9B6D12D9B18EE50A (void);
// 0x000000E9 System.Void WeChatWASM.WX::OffMouseDown(System.Action`1<WeChatWASM.OnMouseDownListenerResult>)
extern void WX_OffMouseDown_mAD816E59DB8A42B66FE3515F149271DD60E02F2E (void);
// 0x000000EA System.Void WeChatWASM.WX::OnMouseMove(System.Action`1<WeChatWASM.OnMouseMoveListenerResult>)
extern void WX_OnMouseMove_mD7FB7DFB8250162720F3E8B246E54DCF97DB69AC (void);
// 0x000000EB System.Void WeChatWASM.WX::OffMouseMove(System.Action`1<WeChatWASM.OnMouseMoveListenerResult>)
extern void WX_OffMouseMove_m302FB55DFE702EA4E0FDF5F2A8DA067419C5F8DB (void);
// 0x000000EC System.Void WeChatWASM.WX::OnMouseUp(System.Action`1<WeChatWASM.OnMouseDownListenerResult>)
extern void WX_OnMouseUp_m418A30F3F1975DE302733FE641C54A6A326AAC87 (void);
// 0x000000ED System.Void WeChatWASM.WX::OffMouseUp(System.Action`1<WeChatWASM.OnMouseDownListenerResult>)
extern void WX_OffMouseUp_mF2E84D5F13D7B4F5E88943A4DFDDFB7CDEABA838 (void);
// 0x000000EE System.Void WeChatWASM.WX::OnNetworkStatusChange(System.Action`1<WeChatWASM.OnNetworkStatusChangeListenerResult>)
extern void WX_OnNetworkStatusChange_mB4A5A586FA4877CC0ED45DC34E8342FF5B24381E (void);
// 0x000000EF System.Void WeChatWASM.WX::OffNetworkStatusChange(System.Action`1<WeChatWASM.OnNetworkStatusChangeListenerResult>)
extern void WX_OffNetworkStatusChange_m393E80CAA9598D4B875DB36D43DE0C72E83D32D1 (void);
// 0x000000F0 System.Void WeChatWASM.WX::OnNetworkWeakChange(System.Action`1<WeChatWASM.OnNetworkWeakChangeListenerResult>)
extern void WX_OnNetworkWeakChange_m3A5B167579965B52C7EBA82607542ADA005F4A79 (void);
// 0x000000F1 System.Void WeChatWASM.WX::OffNetworkWeakChange(System.Action`1<WeChatWASM.OnNetworkWeakChangeListenerResult>)
extern void WX_OffNetworkWeakChange_mDC13FBA29B128B3AF725C19A46C1E830F9DBBAC7 (void);
// 0x000000F2 System.Void WeChatWASM.WX::OnShareMessageToFriend(System.Action`1<WeChatWASM.OnShareMessageToFriendListenerResult>)
extern void WX_OnShareMessageToFriend_m7C8A8A668A6CD0B6A4CBD6119398487614705DC5 (void);
// 0x000000F3 System.Void WeChatWASM.WX::OnShow(System.Action`1<WeChatWASM.OnShowListenerResult>)
extern void WX_OnShow_m01B8746517D51AE050B72E00B9740EA74DC4BF51 (void);
// 0x000000F4 System.Void WeChatWASM.WX::OffShow(System.Action`1<WeChatWASM.OnShowListenerResult>)
extern void WX_OffShow_mCFA0A06612DBCB4B753C267C0CBFF6ECE62124E8 (void);
// 0x000000F5 System.Void WeChatWASM.WX::OnTouchCancel(System.Action`1<WeChatWASM.OnTouchStartListenerResult>)
extern void WX_OnTouchCancel_m8242CA69B0946D0D21DA6DCFDC0B1FF4058CC060 (void);
// 0x000000F6 System.Void WeChatWASM.WX::OffTouchCancel(System.Action`1<WeChatWASM.OnTouchStartListenerResult>)
extern void WX_OffTouchCancel_m5FBC411E554FB87653D4A1766897F75B24AA91A7 (void);
// 0x000000F7 System.Void WeChatWASM.WX::OnTouchEnd(System.Action`1<WeChatWASM.OnTouchStartListenerResult>)
extern void WX_OnTouchEnd_m5FA63F672192AD520015ACF7C87AB2D15A35EFD0 (void);
// 0x000000F8 System.Void WeChatWASM.WX::OffTouchEnd(System.Action`1<WeChatWASM.OnTouchStartListenerResult>)
extern void WX_OffTouchEnd_m018B481CDCFAEC2869E4212B3283727E4630096F (void);
// 0x000000F9 System.Void WeChatWASM.WX::OnTouchMove(System.Action`1<WeChatWASM.OnTouchStartListenerResult>)
extern void WX_OnTouchMove_m6AE9FA0BF963D9DDF4A4415B446209647F67B85B (void);
// 0x000000FA System.Void WeChatWASM.WX::OffTouchMove(System.Action`1<WeChatWASM.OnTouchStartListenerResult>)
extern void WX_OffTouchMove_m662B872836D8DF86627CA0D64D7CCD6F6CDAD2B8 (void);
// 0x000000FB System.Void WeChatWASM.WX::OnTouchStart(System.Action`1<WeChatWASM.OnTouchStartListenerResult>)
extern void WX_OnTouchStart_m3BF867542C004D9FD8FD375F3BE290F8510AEF5D (void);
// 0x000000FC System.Void WeChatWASM.WX::OffTouchStart(System.Action`1<WeChatWASM.OnTouchStartListenerResult>)
extern void WX_OffTouchStart_m0667C8D8087354EE51EE13B17966F4C1CAA8E770 (void);
// 0x000000FD System.Void WeChatWASM.WX::OnUnhandledRejection(System.Action`1<WeChatWASM.OnUnhandledRejectionListenerResult>)
extern void WX_OnUnhandledRejection_mA5208BBDA9F46182054F6557BD1E2AB88AA28C50 (void);
// 0x000000FE System.Void WeChatWASM.WX::OffUnhandledRejection(System.Action`1<WeChatWASM.OnUnhandledRejectionListenerResult>)
extern void WX_OffUnhandledRejection_m964564268E735486CD5A2C52D68E0B14256E05B9 (void);
// 0x000000FF System.Void WeChatWASM.WX::OnUserCaptureScreen(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WX_OnUserCaptureScreen_m9FD3EF6B1BB33D835237FAC180E59155512B5B0D (void);
// 0x00000100 System.Void WeChatWASM.WX::OffUserCaptureScreen(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WX_OffUserCaptureScreen_m81700EEC63584E59379480DDA4302B0AC14DB530 (void);
// 0x00000101 System.Void WeChatWASM.WX::OnVoIPChatInterrupted(System.Action`1<WeChatWASM.OnVoIPChatInterruptedListenerResult>)
extern void WX_OnVoIPChatInterrupted_m19834EE7E9278E1CB7EFC9A1325A4B2595EDE682 (void);
// 0x00000102 System.Void WeChatWASM.WX::OffVoIPChatInterrupted(System.Action`1<WeChatWASM.OnVoIPChatInterruptedListenerResult>)
extern void WX_OffVoIPChatInterrupted_m88D6B206195635A667F33EFE4E90557D9B0A1664 (void);
// 0x00000103 System.Void WeChatWASM.WX::OnVoIPChatMembersChanged(System.Action`1<WeChatWASM.OnVoIPChatMembersChangedListenerResult>)
extern void WX_OnVoIPChatMembersChanged_mAC5555309B636E32E27582DEE7EC0F14633F5618 (void);
// 0x00000104 System.Void WeChatWASM.WX::OffVoIPChatMembersChanged(System.Action`1<WeChatWASM.OnVoIPChatMembersChangedListenerResult>)
extern void WX_OffVoIPChatMembersChanged_m7C1E4A756BCE2A8CE6BFC827670B58793DC42820 (void);
// 0x00000105 System.Void WeChatWASM.WX::OnVoIPChatSpeakersChanged(System.Action`1<WeChatWASM.OnVoIPChatSpeakersChangedListenerResult>)
extern void WX_OnVoIPChatSpeakersChanged_mBF5CB8B6578864C942BE0F21FFE1F2BBB23B9126 (void);
// 0x00000106 System.Void WeChatWASM.WX::OffVoIPChatSpeakersChanged(System.Action`1<WeChatWASM.OnVoIPChatSpeakersChangedListenerResult>)
extern void WX_OffVoIPChatSpeakersChanged_m1798AC1B67E3A0C9CD11EE364B8EFFC80EC8D7A5 (void);
// 0x00000107 System.Void WeChatWASM.WX::OnVoIPChatStateChanged(System.Action`1<WeChatWASM.OnVoIPChatStateChangedListenerResult>)
extern void WX_OnVoIPChatStateChanged_m6DECB2DD45FCEC3723F36789E6901761A0572B58 (void);
// 0x00000108 System.Void WeChatWASM.WX::OffVoIPChatStateChanged(System.Action`1<WeChatWASM.OnVoIPChatStateChangedListenerResult>)
extern void WX_OffVoIPChatStateChanged_m1129B924C7B69B360FA377F9A1092DBF277FFDFB (void);
// 0x00000109 System.Void WeChatWASM.WX::OnWheel(System.Action`1<WeChatWASM.OnWheelListenerResult>)
extern void WX_OnWheel_m04348C5D0B963B34560D0E6640018961390053ED (void);
// 0x0000010A System.Void WeChatWASM.WX::OffWheel(System.Action`1<WeChatWASM.OnWheelListenerResult>)
extern void WX_OffWheel_m0087C62DCB5C405C825424A036EFA60FA4F1A50D (void);
// 0x0000010B System.Void WeChatWASM.WX::OnWindowResize(System.Action`1<WeChatWASM.OnWindowResizeListenerResult>)
extern void WX_OnWindowResize_mE94DD39829ED7D2152F060D5AA8C3CCA63210FC2 (void);
// 0x0000010C System.Void WeChatWASM.WX::OffWindowResize(System.Action`1<WeChatWASM.OnWindowResizeListenerResult>)
extern void WX_OffWindowResize_m8FF923FB105DB53802235C0325D2905368803266 (void);
// 0x0000010D System.Void WeChatWASM.WX::OnAddToFavorites(System.Action`1<System.Action`1<WeChatWASM.OnAddToFavoritesListenerResult>>)
extern void WX_OnAddToFavorites_mAAF9DF344F271D0747E1F13A9A238B3EEB44D587 (void);
// 0x0000010E System.Void WeChatWASM.WX::OffAddToFavorites(System.Action`1<System.Action`1<WeChatWASM.OnAddToFavoritesListenerResult>>)
extern void WX_OffAddToFavorites_m0FEECC7BB2FB00F474EA5FFDC718F9255A092D1F (void);
// 0x0000010F System.Void WeChatWASM.WX::OnCopyUrl(System.Action`1<System.Action`1<WeChatWASM.OnCopyUrlListenerResult>>)
extern void WX_OnCopyUrl_m3FB90DC29032116C4D11EC59E8BC8750AE422B1C (void);
// 0x00000110 System.Void WeChatWASM.WX::OffCopyUrl(System.Action`1<System.Action`1<WeChatWASM.OnCopyUrlListenerResult>>)
extern void WX_OffCopyUrl_mC1163E4F8A781AB513978928C2BFA6CA3A00FB5A (void);
// 0x00000111 System.Void WeChatWASM.WX::OnHandoff(System.Action`1<System.Action`1<WeChatWASM.OnHandoffListenerResult>>)
extern void WX_OnHandoff_m037FC7FEC1606FD5F9FF72D040442A093733CC99 (void);
// 0x00000112 System.Void WeChatWASM.WX::OffHandoff(System.Action`1<System.Action`1<WeChatWASM.OnHandoffListenerResult>>)
extern void WX_OffHandoff_m29686F8613400BAE5484ABB60EE9D5CFB558A28B (void);
// 0x00000113 System.Void WeChatWASM.WX::OnShareTimeline(System.Action`1<System.Action`1<WeChatWASM.OnShareTimelineListenerResult>>)
extern void WX_OnShareTimeline_m3F2315B1AA3F8D4708E47015B7EDA8D80588B7B9 (void);
// 0x00000114 System.Void WeChatWASM.WX::OffShareTimeline(System.Action`1<System.Action`1<WeChatWASM.OnShareTimelineListenerResult>>)
extern void WX_OffShareTimeline_m1A4D76297068A53ED8E5E883D5BF1D9258967E75 (void);
// 0x00000115 System.Void WeChatWASM.WX::OnGameLiveStateChange(System.Action`2<WeChatWASM.OnGameLiveStateChangeCallbackResult,System.Action`1<WeChatWASM.OnGameLiveStateChangeCallbackResponse>>)
extern void WX_OnGameLiveStateChange_mB0A42C996DD18E24C56B75ED4E8ABA81ADCA67EF (void);
// 0x00000116 System.Void WeChatWASM.WX::OffGameLiveStateChange(System.Action`2<WeChatWASM.OnGameLiveStateChangeCallbackResult,System.Action`1<WeChatWASM.OnGameLiveStateChangeCallbackResponse>>)
extern void WX_OffGameLiveStateChange_m27FCDCB5C91FAB6BB08A88F984D512C60C3411F8 (void);
// 0x00000117 System.Boolean WeChatWASM.WX::SetHandoffQuery(System.String)
extern void WX_SetHandoffQuery_m834FB923359081D4152A73FEF9A4208C233417DE (void);
// 0x00000118 WeChatWASM.AccountInfo WeChatWASM.WX::GetAccountInfoSync()
extern void WX_GetAccountInfoSync_m57272966214D1352F42EA8A39BFE40437167EB8F (void);
// 0x00000119 WeChatWASM.AppAuthorizeSetting WeChatWASM.WX::GetAppAuthorizeSetting()
extern void WX_GetAppAuthorizeSetting_m2896E977060374F148562E53C3E9341BFDE56B37 (void);
// 0x0000011A WeChatWASM.AppBaseInfo WeChatWASM.WX::GetAppBaseInfo()
extern void WX_GetAppBaseInfo_mC25C5A2E713E7CAF64E89E8C6E035B1522911151 (void);
// 0x0000011B WeChatWASM.GetBatteryInfoSyncResult WeChatWASM.WX::GetBatteryInfoSync()
extern void WX_GetBatteryInfoSync_m08BBDF3FA67CB2D5C7D289E26F9B90A483B30E0B (void);
// 0x0000011C WeChatWASM.DeviceInfo WeChatWASM.WX::GetDeviceInfo()
extern void WX_GetDeviceInfo_m0D3A68B2F6C630A323ED8A1AF928438F1AB4E39A (void);
// 0x0000011D WeChatWASM.EnterOptionsGame WeChatWASM.WX::GetEnterOptionsSync()
extern void WX_GetEnterOptionsSync_m318300331B2C17F4B3823FCA25AA1C3CBE69EE78 (void);
// 0x0000011E T WeChatWASM.WX::GetExptInfoSync(System.String[])
// 0x0000011F T WeChatWASM.WX::GetExtConfigSync()
// 0x00000120 WeChatWASM.LaunchOptionsGame WeChatWASM.WX::GetLaunchOptionsSync()
extern void WX_GetLaunchOptionsSync_mE16332D0A709F07149EF18DB238DE73AE726F225 (void);
// 0x00000121 WeChatWASM.ClientRect WeChatWASM.WX::GetMenuButtonBoundingClientRect()
extern void WX_GetMenuButtonBoundingClientRect_m70E226210FE612589D5B1C480FE9770BD0D400E6 (void);
// 0x00000122 WeChatWASM.GetStorageInfoSyncOption WeChatWASM.WX::GetStorageInfoSync()
extern void WX_GetStorageInfoSync_mA72BA5AC5A7148624E2B17F6896D3FD3EB70FC0D (void);
// 0x00000123 WeChatWASM.SystemInfo WeChatWASM.WX::GetSystemInfoSync()
extern void WX_GetSystemInfoSync_m77ABFDC11AA5A3B00CB56546B9AAE6DCA4D03278 (void);
// 0x00000124 WeChatWASM.SystemSetting WeChatWASM.WX::GetSystemSetting()
extern void WX_GetSystemSetting_mC51600C0ACCD81E143C2E9C894ADC52513B8BD9A (void);
// 0x00000125 WeChatWASM.WindowInfo WeChatWASM.WX::GetWindowInfo()
extern void WX_GetWindowInfo_mE6BA26B215C58A1E110EF3143E610D023D178718 (void);
// 0x00000126 WeChatWASM.ImageData WeChatWASM.WX::CreateImageData()
extern void WX_CreateImageData_mA35A11C442E73701D0BAE2141A411174748E4B2B (void);
// 0x00000127 WeChatWASM.Path2D WeChatWASM.WX::CreatePath2D()
extern void WX_CreatePath2D_mC42A18C53051B87EF8DA82A8375F7F9218547C42 (void);
// 0x00000128 System.Boolean WeChatWASM.WX::SetCursor(System.String,System.Double,System.Double)
extern void WX_SetCursor_m1AF4A6E44DC942FFD3A56D10FBC82FA6C4698211 (void);
// 0x00000129 System.Boolean WeChatWASM.WX::SetMessageToFriendQuery(WeChatWASM.SetMessageToFriendQueryOption)
extern void WX_SetMessageToFriendQuery_m44314EB7EC52B976F66AE28EC38B5DDC2B2CB9F7 (void);
// 0x0000012A System.Double WeChatWASM.WX::GetTextLineHeight(WeChatWASM.GetTextLineHeightOption)
extern void WX_GetTextLineHeight_mA1ECDFF1AB37E253DFAD69996AEA357E064B3344 (void);
// 0x0000012B System.String WeChatWASM.WX::LoadFont(System.String)
extern void WX_LoadFont_mE33673A1619E9F577FC84DAF7EB9ADE3899FCE04 (void);
// 0x0000012C WeChatWASM.GameLiveState WeChatWASM.WX::GetGameLiveState()
extern void WX_GetGameLiveState_mEB0949BC28A3C710A42FB5A9768DC9EC782B4FC0 (void);
// 0x0000012D WeChatWASM.WXDownloadTask WeChatWASM.WX::DownloadFile(WeChatWASM.DownloadFileOption)
extern void WX_DownloadFile_m8A662C30E43638AAC0D9C7D29C0A08AECD19C327 (void);
// 0x0000012E WeChatWASM.WXFeedbackButton WeChatWASM.WX::CreateFeedbackButton(WeChatWASM.CreateOpenSettingButtonOption)
extern void WX_CreateFeedbackButton_m7F6AAFCA0E69ABFF84ED7A09FC69FB9D0A257FE7 (void);
// 0x0000012F WeChatWASM.WXLogManager WeChatWASM.WX::GetLogManager(WeChatWASM.GetLogManagerOption)
extern void WX_GetLogManager_m906A05A85E2FCA5E945A2CFE38A79BD30BA0FDFF (void);
// 0x00000130 WeChatWASM.WXRealtimeLogManager WeChatWASM.WX::GetRealtimeLogManager()
extern void WX_GetRealtimeLogManager_m33C3CBC2E2DE7CC53D74A924B71C22A151FDE349 (void);
// 0x00000131 WeChatWASM.WXUpdateManager WeChatWASM.WX::GetUpdateManager()
extern void WX_GetUpdateManager_mF4C32977971241B271D04EE0B941629A981D68B5 (void);
// 0x00000132 WeChatWASM.WXVideoDecoder WeChatWASM.WX::CreateVideoDecoder()
extern void WX_CreateVideoDecoder_m93B078190F2B045C83509478206B4F7B9CAA08A5 (void);
// 0x00000133 System.Void WeChatWASM.WX::.ctor()
extern void WX__ctor_m907D9E1FBAA74BF99F9F0C5D4364D48D8D1030CE (void);
// 0x00000134 WeChatWASM.WXEnv WeChatWASM.WXBase::get_env()
extern void WXBase_get_env_m4C1152C9C0072D9B528093A10DDAD8457F542FE7 (void);
// 0x00000135 WeChatWASM.Cloud WeChatWASM.WXBase::get_cloud()
extern void WXBase_get_cloud_mB152458C8D5BB3FAB903DDF03996177FC7F536CC (void);
// 0x00000136 System.Void WeChatWASM.WXBase::InitSDK(System.Action`1<System.Int32>)
extern void WXBase_InitSDK_mFBD2C6BFBD434B3D162A5AE13EB2680D0178C96A (void);
// 0x00000137 System.Void WeChatWASM.WXBase::ReportGameStart()
extern void WXBase_ReportGameStart_m12F4475E983AD71E3000AD6285B188C05038AB2D (void);
// 0x00000138 System.Void WeChatWASM.WXBase::ReportGameSceneError(System.Int32,System.Int32,System.String,System.String)
extern void WXBase_ReportGameSceneError_m13AF71DFE0FAB60C70410BE7F90B3EAD67AB7CC0 (void);
// 0x00000139 System.Void WeChatWASM.WXBase::WriteLog(System.String)
extern void WXBase_WriteLog_m37D40E4A1750B2F3DE488C2758F154726E8876CB (void);
// 0x0000013A System.Void WeChatWASM.WXBase::WriteWarn(System.String)
extern void WXBase_WriteWarn_m56105BA0D5D410FADDE89992637CB3D03E1CF133 (void);
// 0x0000013B System.Void WeChatWASM.WXBase::HideLoadingPage()
extern void WXBase_HideLoadingPage_m2B48CB9D31757E9D3F6B6F80A6FDD931EA60BA0A (void);
// 0x0000013C System.Void WeChatWASM.WXBase::PreloadConcurrent(System.Int32)
extern void WXBase_PreloadConcurrent_mC7EDD2AADFD5CB505F993C6F6B913F0E054C5AA5 (void);
// 0x0000013D System.Void WeChatWASM.WXBase::ReportUserBehaviorBranchAnalytics(System.String,System.String,System.Int32)
extern void WXBase_ReportUserBehaviorBranchAnalytics_m1D061F4F34A9F9FD3FC2E06C69B9160128F03F7C (void);
// 0x0000013E System.Void WeChatWASM.WXBase::StorageSetIntSync(System.String,System.Int32)
extern void WXBase_StorageSetIntSync_m9E8E068DF053D8C1382290691E35F160C60C79A2 (void);
// 0x0000013F System.Int32 WeChatWASM.WXBase::StorageGetIntSync(System.String,System.Int32)
extern void WXBase_StorageGetIntSync_m240D27D3D013EBD59B567C73F035ECF4DD99A6BE (void);
// 0x00000140 System.Void WeChatWASM.WXBase::StorageSetStringSync(System.String,System.String)
extern void WXBase_StorageSetStringSync_mBBE8796A9F1FC5B06E793AB44A4ED2A7150D1A8D (void);
// 0x00000141 System.String WeChatWASM.WXBase::StorageGetStringSync(System.String,System.String)
extern void WXBase_StorageGetStringSync_m04414187C1FB6E0A8EA9E280F096A6187006A144 (void);
// 0x00000142 System.Void WeChatWASM.WXBase::StorageSetFloatSync(System.String,System.Single)
extern void WXBase_StorageSetFloatSync_mD51B0F1DAC9BBA50C370EA1384D6D792DF63B28C (void);
// 0x00000143 System.Single WeChatWASM.WXBase::StorageGetFloatSync(System.String,System.Single)
extern void WXBase_StorageGetFloatSync_mBF11538E62936F65C474C24415365099EFC70C51 (void);
// 0x00000144 System.Void WeChatWASM.WXBase::StorageDeleteAllSync()
extern void WXBase_StorageDeleteAllSync_m11B0096D39600B47168A012C6F9E66D2C40F3296 (void);
// 0x00000145 System.Void WeChatWASM.WXBase::StorageDeleteKeySync(System.String)
extern void WXBase_StorageDeleteKeySync_m88A9ADDE74802ABA9BE4D8624222BD974067A9CE (void);
// 0x00000146 System.Boolean WeChatWASM.WXBase::StorageHasKeySync(System.String)
extern void WXBase_StorageHasKeySync_m35AA0DE3FB5D076621A65BB6A9C241A778E423E5 (void);
// 0x00000147 WeChatWASM.WXUserInfoButton WeChatWASM.WXBase::CreateUserInfoButton(System.Int32,System.Int32,System.Int32,System.Int32,System.String,System.Boolean)
extern void WXBase_CreateUserInfoButton_mA8572BD3EE589F8E4066A5EC33C88523DB849684 (void);
// 0x00000148 System.Void WeChatWASM.WXBase::OnShareAppMessage(WeChatWASM.WXShareAppMessageParam,System.Action`1<System.Action`1<WeChatWASM.WXShareAppMessageParam>>)
extern void WXBase_OnShareAppMessage_m7A40F3439F6B6052E14C650CEF3358A3B2AA3033 (void);
// 0x00000149 WeChatWASM.WXBannerAd WeChatWASM.WXBase::CreateBannerAd(WeChatWASM.WXCreateBannerAdParam)
extern void WXBase_CreateBannerAd_m3EC0E23CE9D4D8EDF42903DB2C210922B3C1DB8B (void);
// 0x0000014A WeChatWASM.WXBannerAd WeChatWASM.WXBase::CreateFixedBottomMiddleBannerAd(System.String,System.Int32,System.Int32)
extern void WXBase_CreateFixedBottomMiddleBannerAd_m91C627BF8FC464224E55F4D7F8DC9BEBF800CC18 (void);
// 0x0000014B WeChatWASM.WXRewardedVideoAd WeChatWASM.WXBase::CreateRewardedVideoAd(WeChatWASM.WXCreateRewardedVideoAdParam)
extern void WXBase_CreateRewardedVideoAd_m9B23A01C5C921FBBE377E97E2595349BB00E0D9A (void);
// 0x0000014C WeChatWASM.WXInterstitialAd WeChatWASM.WXBase::CreateInterstitialAd(WeChatWASM.WXCreateInterstitialAdParam)
extern void WXBase_CreateInterstitialAd_m126184605652891ED94F3E3B15CF607E1E4264F6 (void);
// 0x0000014D WeChatWASM.WXCustomAd WeChatWASM.WXBase::CreateCustomAd(WeChatWASM.WXCreateCustomAdParam)
extern void WXBase_CreateCustomAd_mA54CD2CD34C4E4CF7FF1AA73A6BBC09EED9D8203 (void);
// 0x0000014E WeChatWASM.WXGameRecorder WeChatWASM.WXBase::GetGameRecorder()
extern void WXBase_GetGameRecorder_m78E3AEB99AF8C9ABB61810AA5A1EED4F8F994171 (void);
// 0x0000014F WeChatWASM.WXCamera WeChatWASM.WXBase::CreateCamera(WeChatWASM.CreateCameraOption)
extern void WXBase_CreateCamera_m28CCF9C4F74A95666371DC514E55CE19E2B4C691 (void);
// 0x00000150 WeChatWASM.WXRecorderManager WeChatWASM.WXBase::GetRecorderManager()
extern void WXBase_GetRecorderManager_mE2CA9AF475636D91BC4BC378C8EC06ADC95DB05A (void);
// 0x00000151 WeChatWASM.WXChat WeChatWASM.WXBase::CreateMiniGameChat(WeChatWASM.WXChatOptions)
extern void WXBase_CreateMiniGameChat_m9018D0AD0605C369B4AEFD25CE0074BAB4BD3208 (void);
// 0x00000152 WeChatWASM.WXUploadTask WeChatWASM.WXBase::UploadFile(WeChatWASM.UploadFileOption)
extern void WXBase_UploadFile_mCC5606D20EAAE98BA246A96F2BA6150E7FDFC8C7 (void);
// 0x00000153 WeChatWASM.WXFileSystemManager WeChatWASM.WXBase::GetFileSystemManager()
extern void WXBase_GetFileSystemManager_m11846F545591ACBA9ADF7F71DA6815341CBD0C46 (void);
// 0x00000154 WeChatWASM.WXOpenDataContext WeChatWASM.WXBase::GetOpenDataContext()
extern void WXBase_GetOpenDataContext_m2EA9ED06374FD6EAF573091ADD5E6F651077DBE7 (void);
// 0x00000155 System.Void WeChatWASM.WXBase::ShowOpenData(UnityEngine.Texture,System.Int32,System.Int32,System.Int32,System.Int32)
extern void WXBase_ShowOpenData_m862903D7AA2836AE83EA4B0A95643EA491475529 (void);
// 0x00000156 System.Void WeChatWASM.WXBase::HideOpenData()
extern void WXBase_HideOpenData_m6840242E9A69AA5EB6C57C6083F1D45598ED656C (void);
// 0x00000157 WeChatWASM.WXInnerAudioContext WeChatWASM.WXBase::CreateInnerAudioContext(WeChatWASM.InnerAudioContextParam)
extern void WXBase_CreateInnerAudioContext_m28578E41F69B90FF0811C5652EFE50F02B0DD54A (void);
// 0x00000158 System.Void WeChatWASM.WXBase::PreDownloadAudios(System.String[],System.Action`1<System.Int32>)
extern void WXBase_PreDownloadAudios_m8DFF05EB18C4DDFA6B705EECFFAA85F91113FAA6 (void);
// 0x00000159 WeChatWASM.WXVideo WeChatWASM.WXBase::CreateVideo(WeChatWASM.WXCreateVideoParam)
extern void WXBase_CreateVideo_mEB07EDEE4E10B053AC3A38A11D58D9343A3F7B02 (void);
// 0x0000015A System.UInt32 WeChatWASM.WXBase::GetTotalMemorySize()
extern void WXBase_GetTotalMemorySize_m6217F33824BC6AE3D4D385144CED4E0224C13C0F (void);
// 0x0000015B System.UInt32 WeChatWASM.WXBase::GetTotalStackSize()
extern void WXBase_GetTotalStackSize_m6770A0765F2F4A68964C44CCF6E45C70B4FD8FAB (void);
// 0x0000015C System.UInt32 WeChatWASM.WXBase::GetStaticMemorySize()
extern void WXBase_GetStaticMemorySize_m5BADD1F0339C961DFB71577EF1CA31C874D7D465 (void);
// 0x0000015D System.UInt32 WeChatWASM.WXBase::GetDynamicMemorySize()
extern void WXBase_GetDynamicMemorySize_mC6448D08728A2F34C1652367294D44A2C82B8FD2 (void);
// 0x0000015E System.UInt32 WeChatWASM.WXBase::GetUsedMemorySize()
extern void WXBase_GetUsedMemorySize_mC007B61826D09EBCDA7B45DB29F537949CEDDBB0 (void);
// 0x0000015F System.UInt32 WeChatWASM.WXBase::GetUnAllocatedMemorySize()
extern void WXBase_GetUnAllocatedMemorySize_m180CD69D6B46A0B89A053DFDF866BE28535EEE78 (void);
// 0x00000160 System.Void WeChatWASM.WXBase::LogUnityHeapMem()
extern void WXBase_LogUnityHeapMem_mFBBEC721CF5310E25F4ECCC701B1E55A9B69B80B (void);
// 0x00000161 System.UInt32 WeChatWASM.WXBase::GetBundleNumberInMemory()
extern void WXBase_GetBundleNumberInMemory_mD4FBB7653BBBF9C373E001C118E96F0ABA31528C (void);
// 0x00000162 System.UInt32 WeChatWASM.WXBase::GetBundleNumberOnDisk()
extern void WXBase_GetBundleNumberOnDisk_m893B6EA6AB2B8DB90DA8C6BEDE66329576D0B7D5 (void);
// 0x00000163 System.UInt32 WeChatWASM.WXBase::GetBundleSizeInMemory()
extern void WXBase_GetBundleSizeInMemory_m579A7FB9508914FC5038B751D0047E96E55F1E20 (void);
// 0x00000164 System.UInt32 WeChatWASM.WXBase::GetBundleSizeOnDisk()
extern void WXBase_GetBundleSizeOnDisk_m898EBF7AE802FCF3CAC9A4AC3B53FEA6582978E4 (void);
// 0x00000165 System.Void WeChatWASM.WXBase::OpenProfileStats()
extern void WXBase_OpenProfileStats_mBAD5CF57D51C918A37DBFF6BC11107C8A31F8A42 (void);
// 0x00000166 System.Void WeChatWASM.WXBase::ProfilingMemoryDump()
extern void WXBase_ProfilingMemoryDump_m0E5BF72F05CAF2208F1BBE51556F12856F44DE79 (void);
// 0x00000167 System.Void WeChatWASM.WXBase::LogManagerDebug(System.String)
extern void WXBase_LogManagerDebug_m9F4587A42C51811C50B4D7B36BDEDA8804A4AED2 (void);
// 0x00000168 System.Void WeChatWASM.WXBase::LogManagerInfo(System.String)
extern void WXBase_LogManagerInfo_m59C3ED5ABCA4C37B1DE3CBB774193589788B5F73 (void);
// 0x00000169 System.Void WeChatWASM.WXBase::LogManagerLog(System.String)
extern void WXBase_LogManagerLog_m06251F555BF957E2D2C6C67C908E37D757175C22 (void);
// 0x0000016A System.Void WeChatWASM.WXBase::LogManagerWarn(System.String)
extern void WXBase_LogManagerWarn_m77E1BF39EBF01F6C61C9EF32680977DA68A28F27 (void);
// 0x0000016B System.Boolean WeChatWASM.WXBase::IsCloudTest()
extern void WXBase_IsCloudTest_mFBA682A37175FBBB4071E1FE8068DEE50A97D178 (void);
// 0x0000016C System.Void WeChatWASM.WXBase::CleanAllFileCache(System.Action`1<System.Boolean>)
extern void WXBase_CleanAllFileCache_m500240C22FD124122A04108983C3A8368FB6E7DB (void);
// 0x0000016D System.Void WeChatWASM.WXBase::CleanFileCache(System.Int32,System.Action`1<WeChatWASM.ReleaseResult>)
extern void WXBase_CleanFileCache_m0793EFFCE2E0E5EBD40BBDA2485006C46930C6F3 (void);
// 0x0000016E System.Void WeChatWASM.WXBase::RemoveFile(System.String,System.Action`1<System.Boolean>)
extern void WXBase_RemoveFile_mCCF96D2F374DF8A8183DDD87EC34AD25F1A27329 (void);
// 0x0000016F System.String WeChatWASM.WXBase::get_PluginCachePath()
extern void WXBase_get_PluginCachePath_m16EFD4383916E10E4959567DC7E43BA86D4C432B (void);
// 0x00000170 System.String WeChatWASM.WXBase::GetCachePath(System.String)
extern void WXBase_GetCachePath_m488E2133F1DF458269961D39CFF4B4409F4446C2 (void);
// 0x00000171 System.Void WeChatWASM.WXBase::OnLaunchProgress(System.Action`1<WeChatWASM.LaunchEvent>)
extern void WXBase_OnLaunchProgress_m80C0517D266CB8663A4511A7D5F687D81BA7A459 (void);
// 0x00000172 System.Void WeChatWASM.WXBase::UncaughtException()
extern void WXBase_UncaughtException_m3D9F95660E0CE211CCADB67A3C7C980B6BA4BA8F (void);
// 0x00000173 WeChatWASM.WXGameClubButton WeChatWASM.WXBase::CreateGameClubButton(WeChatWASM.WXCreateGameClubButtonParam)
extern void WXBase_CreateGameClubButton_mF3295ECEF32B69D2F733392FC68F7797DB98DAE9 (void);
// 0x00000174 System.Void WeChatWASM.WXBase::OnNeedPrivacyAuthorization(System.Action`1<System.String>)
extern void WXBase_OnNeedPrivacyAuthorization_mAA392F8A899B36FC1D901A5789B93353A6D93A18 (void);
// 0x00000175 System.Void WeChatWASM.WXBase::PrivacyAuthorizeResolve(WeChatWASM.PrivacyAuthorizeResolveOption)
extern void WXBase_PrivacyAuthorizeResolve_mE3359C78EE7C207D2245BE7B591639303DDE11BC (void);
// 0x00000176 System.Int32 WeChatWASM.WXBase::CreateUDPSocket(System.String,System.Int32,System.Int32)
extern void WXBase_CreateUDPSocket_m3C786153D6366B2D1DBDFD8510EACB2816C5F90C (void);
// 0x00000177 System.Void WeChatWASM.WXBase::CloseUDPSocket(System.Int32)
extern void WXBase_CloseUDPSocket_m6086B7AFD265054FECC97FEDDECA01F564FA5A30 (void);
// 0x00000178 System.Void WeChatWASM.WXBase::SendUDPSocket(System.Int32,System.Byte[],System.Int32,System.Int32)
extern void WXBase_SendUDPSocket_mD13610F42778207D231F5A4113DD0E452EC5BF88 (void);
// 0x00000179 System.Void WeChatWASM.WXBase::SetDataCDN(System.String)
extern void WXBase_SetDataCDN_m6D9E65074FB5E965C18A5B01AEABED0B212767E8 (void);
// 0x0000017A System.Void WeChatWASM.WXBase::SetPreloadList(System.String[])
extern void WXBase_SetPreloadList_mA91CA72D04C1602ED35085231CEC3C6C97090A02 (void);
// 0x0000017B System.Void WeChatWASM.WXBase::GetWXFont(System.String,System.Action`1<UnityEngine.Font>)
extern void WXBase_GetWXFont_m033FCEF5D4CA8249DCE68D63F5C1943F787F2DDA (void);
// 0x0000017C System.Void WeChatWASM.WXBase::.ctor()
extern void WXBase__ctor_m29AEEA990554608AD99DE5754A9EB556CB245C3D (void);
// 0x0000017D System.Void WeChatWASM.WXBase/<>c__DisplayClass74_0::.ctor()
extern void U3CU3Ec__DisplayClass74_0__ctor_mFED584ADB8384261E7F3548114251E6CD1162F28 (void);
// 0x0000017E System.Void WeChatWASM.WXBase/<>c__DisplayClass74_0::<GetWXFont>b__0(WeChatWASM.GetFontResponse)
extern void U3CU3Ec__DisplayClass74_0_U3CGetWXFontU3Eb__0_m8A459751D3491E5B1B5BF99DFA1A6C0F0CD4458A (void);
// 0x0000017F System.Void WeChatWASM.WXBase/<>c__DisplayClass74_0::<GetWXFont>b__1(WeChatWASM.GetFontResponse)
extern void U3CU3Ec__DisplayClass74_0_U3CGetWXFontU3Eb__1_m265EC0DD7B7352F0BA32E3B78ED8102DBDEE7C6E (void);
static Il2CppMethodPointer s_methodPointers[383] = 
{
	CheckFrame_Update_mEDC6AF9CDC48040B8A65B6CFBC6F5CB34B234A45,
	CheckFrame__ctor_m38C73460485C07E03A44FBA6C996F3B51084C346,
	HideLoadingPage_OnGameLaunch_mFFDBC14030320F359B5B080ADCADCA0BD92B32F3,
	HideLoadingPage__ctor_mDF4735613B7A6BDE31E2B2F8F50902AE6E883D94,
	WXProfileStatsScript_Awake_mBA25FD9BB43E585AD4BF9EC8C64711A2879D12B2,
	WXProfileStatsScript_OnEnable_m0DC14521DFC8AE2093EF06FEDA3C425BF4211507,
	WXProfileStatsScript_OnDisable_mB67075A20D85062F63763FF770C61F06D181E0C8,
	WXProfileStatsScript_UpdateValue_m5917D4314A61471E46C3598801065E4CEF22F73E,
	WXProfileStatsScript_Update_mEE867FBA174DDE23EB5CACD1AC2B4E4B701423F0,
	WXProfileStatsScript_UpdateFps_mE8A0E4F8C59156A0D4B8EF53AF6850C4E154E82E,
	WXProfileStatsScript_OnGUI_m0957BACE228A8326FE0DDB5F7438D17B66409E63,
	WXProfileStatsScript_OnGameLaunch_mC4C3BDF40B882030E8215E17A9487BB64808599D,
	WXProfileStatsScript__ctor_m55DFD8141CD987DAFF7A9D4F8C28FCC78C880F22,
	ProfValue__ctor_m879E8E5D78B79A1A1E5062628B1CA75C45D48C62,
	TouchData__ctor_m79D59C452C436FB2FFDC9765AF7EF605740326AF,
	WXTouchInputOverride_Awake_m6DF59EC24461CAD3FD7736B29E97CFB00F0F9844,
	WXTouchInputOverride_OnEnable_m6419ABC1106548A51FD09380AB96945AC32CA344,
	WXTouchInputOverride_OnDisable_mD4675503E0F4698748B28662DB67B49FD15DE6D5,
	WXTouchInputOverride_InitWechatTouchEvents_m9C5FD0A967B37C08F3133656522DC70943AB81AD,
	WXTouchInputOverride_RegisterWechatTouchEvents_mCC8C107407CE833CC1FC49AC2950B2F9E3555C40,
	WXTouchInputOverride_UnregisterWechatTouchEvents_mC8699E5BE4275C7C2C15636ABF94440D83619916,
	WXTouchInputOverride_OnWxTouchStart_mE9C8D3A4615BA2D4EDA45F8089CA37F4FB747275,
	WXTouchInputOverride_OnWxTouchMove_m719A9D384E2A865DC2651523AE61AE4A94D29E18,
	WXTouchInputOverride_OnWxTouchEnd_m159E83F2E3FDD3568DC9439E7DC258973346479F,
	WXTouchInputOverride_OnWxTouchCancel_m4BDE1F8EE7A536B4B55B6F40099F504609516201,
	WXTouchInputOverride_LateUpdate_mEB815DCD3F010A91366A57EAE0F131719329912C,
	WXTouchInputOverride_RemoveEndedTouches_mE4C227CE852E5EBBEFC0344775A08DC6E726466C,
	WXTouchInputOverride_FindTouchData_m89FB9695D11408B020F2CE7219910576F16926C3,
	WXTouchInputOverride_FindOrCreateTouchData_m1362074AC09041B8CDB033D365DF5B71EC6C0822,
	WXTouchInputOverride_UpdateTouchData_m0FF987CCF54C42C3B5984E4E6D702676A69373DE,
	WXTouchInputOverride_get_touchSupported_m92A32C5C051C5A83134AAFA2F8E52E69E850FC99,
	WXTouchInputOverride_get_mousePresent_m504E096CD048CBFAE26587934B88CE14C5D5AC6D,
	WXTouchInputOverride_get_touchCount_m246DB3B8025BCF06E8929F89E3ABC060F02175FE,
	WXTouchInputOverride_GetTouch_mAA624EC27C2209D30779CE3FCE4A9E98AB41A0F0,
	WXTouchInputOverride__ctor_m894F9D76D33FEA917BA8A83519903F38760756AB,
	WXTouchInputOverride_U3CInitWechatTouchEventsU3Eb__6_0_mDC43178A105B8AB8FD2EBC9A26746EC302EB9333,
	U3CU3Ec__cctor_mFB268D3D3D680B9C7D058F0185E2963D507BC132,
	U3CU3Ec__ctor_mBB8D89C9FB98258594AB0366B768D983924C0BCC,
	U3CU3Ec_U3CRemoveEndedTouchesU3Eb__14_0_m3F4195FB13D42C402977ED7E85EEAFEBB70DC117,
	WX_AddCard_m4EF62A2A285A7F4533E0E7DE752A843370CF5872,
	WX_AuthPrivateMessage_mB7A227EDFE13A55B1570414EAC30FD9E75A11547,
	WX_Authorize_mBA4B3F57182936833C7CEC4236DD459CBD13E178,
	WX_CheckIsAddedToMyMiniProgram_m09233B992110D4BD7DD5BCAB351821E0B4AEA23E,
	WX_CheckSession_m0802E0E787CC8D37B107ED99ACEF005B8F2A1666,
	WX_ChooseImage_m91105C56E3EC0A72D51AEB249A0BBBD712BD2967,
	WX_ChooseMedia_m407CD403EFB2E5C0B80674E4DDC5AD45A2BEA894,
	WX_ChooseMessageFile_mE8FE5B24D3EA5108660AE606F30FFE56BBAD22D8,
	WX_CloseBLEConnection_m35E39012F1E66457DB759C41449E07465E192377,
	WX_CloseBluetoothAdapter_m2DBF9DE5A5417D661C9D8EF57297ACAD81FF7C52,
	WX_CreateBLEConnection_mCEFADF9215D46520928366E0D8D3E6E03F3ACFFC,
	WX_CreateBLEPeripheralServer_mAB2B660D269F254D85B6B9C0702EC0D5A1D73B56,
	WX_ExitMiniProgram_mF6C76000B3DA25E9463407095B6D7306EB3EE0CC,
	WX_ExitVoIPChat_m52303BA45E801D69A2558D8483AA634B9A8FF81B,
	WX_FaceDetect_m91D283AC2CAE313D5822921C23187547A108065E,
	WX_GetAvailableAudioSources_mCB733D281426211B6E36E63E23945E09452FB61F,
	WX_GetBLEDeviceCharacteristics_mAC239878D1D46925CEEEB34B5CB5F53A9A4FC861,
	WX_GetBLEDeviceRSSI_m93F279B33A2CF6333609326B6833F92F022C6877,
	WX_GetBLEDeviceServices_mBA33C0210ECF4BBBAE76BB704922BC07648A1B72,
	WX_GetBLEMTU_mC2C8EC5B921A1C69F573CB228794A25DF8579557,
	WX_GetBatteryInfo_mF1D200447446584A8200481225D9D8DCD855C664,
	WX_GetBeacons_mD638164376369D6DF68A5FC76FB06B2226AD06A2,
	WX_GetBluetoothAdapterState_m35351B6BE83651467032F784772C010E4C8DC46B,
	WX_GetBluetoothDevices_mF79DFE2233168BF5158DBB518D7BDF0F8D8D2394,
	WX_GetChannelsLiveInfo_mDFC4DFA2DAD97112091039F1C842912EFE240ED2,
	WX_GetChannelsLiveNoticeInfo_mA18973BA850AFB463928B3C1F460976D9AE76DA9,
	WX_GetClipboardData_m9F8E28B4AA77FEF2D46ED30BFBADCEB8C9D227D7,
	WX_GetConnectedBluetoothDevices_m300BA8A0AE8658BDFA92B8FA5318C3447FB64563,
	WX_GetExtConfig_m5D346879B34BAAC30F6C9D7DBAC9E6A1459EE8A9,
	WX_GetGameClubData_m530E94A705F3CC60B05B28D4D7C12DB4B51E1B17,
	WX_GetGroupEnterInfo_m427BB7E56456C147EC946E909BDA0B34ED531380,
	WX_GetInferenceEnvInfo_m1AB30EB398D3F9668484AEE298BFAF660DA28E2D,
	WX_GetLocalIPAddress_mBABA1EC167855FCB32F629B426E1F0DCD8995311,
	WX_GetLocation_m1B11281C169012CBC6BDFA932196031D0B5B8AEA,
	WX_GetNetworkType_mF0AD7074F82E3D44ACC9A911697FCD1FCE9CAA8A,
	WX_GetPrivacySetting_mD884ACAC69B5F60D229FAD2CF872FBE0311F718B,
	WX_GetScreenBrightness_m6DED297EA603DC194754C86705CE2E1317557E88,
	WX_GetSetting_mD183520167E8564FAC71FF52E43A459540A03594,
	WX_GetShareInfo_mBC5D9AC16134CE2D69C07AC73B0BD63A1F39EAB5,
	WX_GetStorageInfo_mC912088C154B358B8739A5A147862052693C3C83,
	WX_GetSystemInfo_m03E0F4954A986C8EFFD0540ADE742537B605B548,
	WX_GetSystemInfoAsync_mF598CCB8D6AA9DF303DB3352953B1108D7915480,
	WX_GetUserInfo_mC9192796C337DEEFEAC6F93937DC1C34F3284591,
	WX_GetUserInteractiveStorage_m2ECF686D81D0D266F108B8ECD0BF22430A7832C6,
	WX_GetWeRunData_mA78F4865175552993F0DD6539EECA6421A386D68,
	WX_HideKeyboard_mD7E431C98DB3B70B90D64C43DA9A4DBD7E28621D,
	WX_HideLoading_m1A03F19CF336405459E4A310D7ACE302BEA14EEE,
	WX_HideShareMenu_m679D11B1824B59C2461F4F900B0AD5A4ED3B61F7,
	WX_HideToast_mB0751528AD22C8389F875F9CD4F88DE1169B7E6B,
	WX_InitFaceDetect_m59D9B6C173CC4C68104CCD96530B308764B1863F,
	WX_IsBluetoothDevicePaired_mAFDF03BB5AF479CF7D413787DFCF170319710B52,
	WX_JoinVoIPChat_m1C2B935ED9281FAD128D396AFA36DECB7D694E38,
	WX_Login_m567689292667BF1FDCFFF885692F2C968B73AE9B,
	WX_MakeBluetoothPair_m4CF10069DEC408B8B0075C22507E40AB91E031F5,
	WX_NavigateToMiniProgram_mD71EB4720A7AB335219812B61C2BF1D9D8686830,
	WX_NotifyBLECharacteristicValueChange_m7E2FDC734E22F56042BC66AA2EA19D7FABE490DE,
	WX_OpenAppAuthorizeSetting_mBEB459613AD27A79684762375F88CCAD93573416,
	WX_OpenBluetoothAdapter_mFBFF84066F63B431E68A67983033B1FB97EAD2A8,
	WX_OpenCard_mE8D2E5395DA95EE1CB76A27BEBC6446E789E288F,
	WX_OpenChannelsActivity_m9E0441A39633269EAD53E709392C202F049ED271,
	WX_OpenChannelsEvent_m846C1C81E4D51EA8C50D16FD276DDB7EAFF78F85,
	WX_OpenChannelsLive_m2BF5842EA2F0FA9125FF070DAA7B781C09C3CC03,
	WX_OpenChannelsUserProfile_mE38D12DE4410ED85D5DED6AF8FDF2F6E7041D9F6,
	WX_OpenCustomerServiceChat_mCAD961E8155549B7960121AB77B41647399ADDB4,
	WX_OpenCustomerServiceConversation_mADA29F7F28614735BB7D9D6748A8DD93AA1E7E2C,
	WX_OpenPrivacyContract_mD81129074C91999CB8B87FAAE1A539122D44AC50,
	WX_OpenSetting_mFE69B196162E0C68EBD1B763531C3BBB08BE3851,
	WX_OpenSystemBluetoothSetting_mDFF7CB0D6D8BE237FC174004C849997EBE4CEC89,
	WX_PreviewImage_m157D02B943FF18CF499D6881F27CBE911FB74090,
	WX_PreviewMedia_m321409A69310EE36DB2B71B56399FA37F1B9D483,
	WX_ReadBLECharacteristicValue_m9CAB4D43007006EF6DDCB21BE7E8A2AD6E869522,
	WX_RemoveStorage_m9CD196A7A34D71DC91373D83C7B3A88B46A4562B,
	WX_RemoveUserCloudStorage_m26B25B1BEF0C9C37CC61C0F352402363C0AC6660,
	WX_ReportScene_m38997DDAB26515C815C80B634DFF5EABC86DC840,
	WX_RequestMidasFriendPayment_m80F96762F4F2721E4451E08DB9E5D5934AE421DB,
	WX_RequestMidasPayment_m11C5992F3BA55AFBA712E1A6E8FB8922E1E6718A,
	WX_RequestSubscribeMessage_m152F9691C83E1CFDDBE102A0C8AFE9ADDC72906B,
	WX_RequestSubscribeSystemMessage_m32FAC05CF7F988D15B84A42F2B9BE57C01F68A21,
	WX_RequirePrivacyAuthorize_mCED035EEEA90B241A54B3575017A1AF8BB65EDA1,
	WX_RestartMiniProgram_m27595E302BEB7A851150A7B0026E90C21E7AAE1D,
	WX_SaveFileToDisk_m1EC7D3431D0F5A8E50754D911FE04CEB3C24B8AD,
	WX_SaveImageToPhotosAlbum_m04BBAA43B8AD44B7C2F4CFE48675B7FD01CFAA4B,
	WX_ScanCode_m7914319E183F9EB7F1AF879325DC4B8A2703C748,
	WX_SetBLEMTU_m5D5769CAC74D715A245E69C5B4B4F339FCBA0339,
	WX_SetClipboardData_mFA64BC61DAA2BF7FA4AF20305E40BD6D7C0F01DE,
	WX_SetDeviceOrientation_m2BF5FC38ACEAF884F398313DFC5E3B7535928CEC,
	WX_SetEnableDebug_m9B6280C2AB03F615869A1E66D933DB562DC55B1D,
	WX_SetInnerAudioOption_mA41D8E4BBB64BDE03FBECE4846538F5E72500540,
	WX_SetKeepScreenOn_mC5805B646624083BB1B3CE41F403DB305F8B4623,
	WX_SetMenuStyle_mCF4FB4B1F9BA417C75C21E5FFDD1D8D7E23C8E33,
	WX_SetScreenBrightness_m87AD6689F0EAE32B72C2D2425BD992E4541768AD,
	WX_SetStatusBarStyle_m2E7F1C79407AD963E9B8FB1F20EB4DAA816335A4,
	WX_SetUserCloudStorage_m48082A86EC881A3468D26E16126E2DEB89A63724,
	WX_ShowActionSheet_m81C0F0383C3D9C3D70800E74634B2F3012FC328B,
	WX_ShowKeyboard_mA8ECDCA4C622EB999F751E1D9E84EF70833E496D,
	WX_ShowLoading_m701A4FF17D8B8DEFE4AB69815FF91002251B2B1D,
	WX_ShowModal_mAC00C62F494943AA6BAB838F452B67E9536F2253,
	WX_ShowShareImageMenu_m71E539E9E338DF80E45D9A360866A5D8DF4BA383,
	WX_ShowShareMenu_m128A46E319A0FA7C94B478E127F29E44C6219C98,
	WX_ShowToast_m3FB2BF57219E0CA1EF681A1BCB4A9A00810AF368,
	WX_StartAccelerometer_m0457619ADA3C56DF082802DF8C201EE436A6A613,
	WX_StartBeaconDiscovery_m8209CB7249BF9E3720A6CF04BF6A61748A48B480,
	WX_StartBluetoothDevicesDiscovery_m97076532A6D8CB26BCAD3BF47E6C48AE72AFE96A,
	WX_StartCompass_m33D308E061253C6F729012306FD3CD1745BBA8B5,
	WX_StartDeviceMotionListening_mAF5A25BCE648AF76B95AA2823FA00C70344BFAD4,
	WX_StartGyroscope_m7033AFB2F7BE58BDFB6F490C211C72435F171AF9,
	WX_StopAccelerometer_m2E06CA0F6D4826E1825995FC0D738430E5E38F25,
	WX_StopBeaconDiscovery_m2C2E0B98DD44EE1934F51B458C603ECB9BCCEED3,
	WX_StopBluetoothDevicesDiscovery_m259BA14705CC8BB65D5A5A8658949B41C632D1C8,
	WX_StopCompass_m99A765AE57DE230BACD904EF824990527781FA45,
	WX_StopDeviceMotionListening_m938B39AAFF397F3FB38D28C53EB159070D39C51C,
	WX_StopFaceDetect_m18C070F02940A0256C0F6178A68502ACC55BCA3A,
	WX_StopGyroscope_m13DC19DCBDA3F7C819BFA5933A68FCA3C0AEFBBA,
	WX_UpdateKeyboard_mD22267BB0B50FC51E31BC3B80CB695BD923964D4,
	WX_UpdateShareMenu_m8E6C41B25BDD9DD9F8EF24C87DAA3673E7DBF6D6,
	WX_UpdateVoIPChatMuteConfig_mCCBF5A1F3E087F3C0AC650C024E9EE5148C05338,
	WX_UpdateWeChatApp_mB504E97373EA3D3B3EE214ED9FB3E059C3EDF53E,
	WX_VibrateLong_mCA90CE14D01C874AAAAA1E54E4899DFA11C8A361,
	WX_VibrateShort_mD8B1AB726E39AD5DFE2B0A32AD77447D87455B5F,
	WX_WriteBLECharacteristicValue_m9D710350E6D6A68BF74B3C386EDFEC742E27AD24,
	WX_StartGameLive_m2A829D8AD5E7F8C57503957CAF583FD6D07697BA,
	WX_CheckGameLiveEnabled_mAC7087E01472E4F68ACA5B35B2F41D1E4264DB42,
	WX_GetUserCurrentGameliveInfo_m472AABF0B999E9077616B410C329FBC003E5258C,
	WX_GetUserRecentGameLiveInfo_m0B2A2A5165C1CD17E4EDEBE22B0C4553DBB7EC68,
	WX_GetUserGameLiveDetails_m95D4781C09C31BB369E4C7B49118457EF3CCF553,
	WX_OpenChannelsLiveCollection_m68A56227A2D649920DDCAC624B3FC9B9834A5936,
	WX_OpenPage_mDCADD860C8F84603B7F3C9929190E4AD0D754BFD,
	WX_RequestMidasPaymentGameItem_mA1CA4836E316842C1076218A239D425D4E31F536,
	WX_RequestSubscribeLiveActivity_m57C63A804C17B08F99974E2F0271F0286370342B,
	WX_OperateGameRecorderVideo_m9E6DDC2798E998C54833F1C990AC2353DA13166A,
	WX_RemoveStorageSync_mBDBF32C3F6E3C5C336613A7FA0A58A8F82D07975,
	NULL,
	WX_ReportMonitor_m0E12B8CC7329AB3B752BBED4E7077C65ADACFF30,
	WX_ReportPerformance_m8606B6D0A576F2A667C76E0DB96E60651F88848D,
	WX_ReportUserBehaviorBranchAnalytics_m01CAA6494A983AB51C2ED4E43D548E90161B623D,
	WX_ReserveChannelsLive_m765D9419E48558C87BAC41B7F6AFA3BB52CD5341,
	WX_RevokeBufferURL_m43171A0D3AD9D76082E5579F1A2EAD0CB11620C0,
	WX_SetPreferredFramesPerSecond_m653A9990E5864AC367B6D0F20D51D58D778B0F76,
	NULL,
	WX_ShareAppMessage_m17568A390253F08CC37C768FCC63A0474E7FF70F,
	WX_TriggerGC_mDB5E018E301B44511F2E6CB1085F24F137BC5CD9,
	WX_OnAccelerometerChange_m69C4E0C86470DFCFC71425748E2B89DED394D206,
	WX_OffAccelerometerChange_m5C960EF02F074DFF13E0215DB81891C057FF885B,
	WX_OnAudioInterruptionBegin_m7FEFD73BE493723F93F1C73143F328B3926FB22E,
	WX_OffAudioInterruptionBegin_m5CF1468843272B781314FBACA68D1100BEB40A33,
	WX_OnAudioInterruptionEnd_m5DE41C091F3E1FFCD88C5627266ABFFC4BD573E4,
	WX_OffAudioInterruptionEnd_mEBDC5B5BDD867B4901A093237EC0BD7EADE8BC2C,
	WX_OnBLECharacteristicValueChange_mCE4F21B6C5CC524D49118212217E51832EBEC0D1,
	WX_OffBLECharacteristicValueChange_mC7B49E4BE9577E0A788CE75021E0B12FFB498DEB,
	WX_OnBLEConnectionStateChange_mA1EDA3ADDD259FAFA49142BF296E3F0265A2A075,
	WX_OffBLEConnectionStateChange_m80774F9EC7947A59B92844540B09C8ABD548E8AC,
	WX_OnBLEMTUChange_mECE1C9E98ECF7642DBF58670A8CD5EE7E99C2A60,
	WX_OffBLEMTUChange_mC2D6D608D72069072F42B8DFDB2D7B3E93AD5806,
	WX_OnBLEPeripheralConnectionStateChanged_m819225ED6C74C3F3AA4D3BA771CC446A48BC3602,
	WX_OffBLEPeripheralConnectionStateChanged_m0961F1B5AB77C3CAB3C539B8AEEF6FC2E03EC2FC,
	WX_OnBeaconServiceChange_mA1CDE994590EA9F382E9D7C928EA6F198F49F0B8,
	WX_OffBeaconServiceChange_m4C0553750CCF39C5CC693FD1D88AE9D93DAF5609,
	WX_OnBeaconUpdate_mF5C8D6FEB91291B235B3C7B80D5B6E9B0B6A4A9E,
	WX_OffBeaconUpdate_m8DF8354FC4E94A289A6133730A191A35C19618D4,
	WX_OnBluetoothAdapterStateChange_m9A60AA097442D3D9F6C25197D34D033D6F437F00,
	WX_OffBluetoothAdapterStateChange_mF5B3415DB2CB116D8A246C14E67B522144D5B72D,
	WX_OnBluetoothDeviceFound_m120BF2E4C9B12370849EBF45F2B25D0BF5B8959F,
	WX_OffBluetoothDeviceFound_m6D3F9F56516F8DD759878789B04114C6851012BA,
	WX_OnCompassChange_mA4675988ADE8C3D083B737146A1D4211778A40D7,
	WX_OffCompassChange_mEFF58A35CB01D11651369991D7BCCB6BC4781009,
	WX_OnDeviceMotionChange_m33DDD0526342B659CF364E4C2001FE2BA0A6D2B5,
	WX_OffDeviceMotionChange_m14254B2592DEF109DF3141860FC0CD4219910638,
	WX_OnDeviceOrientationChange_m84295A270850136940E188649E780F7D4A4A550D,
	WX_OffDeviceOrientationChange_m44C5F1F5B42572628915080215F814B85285D6C7,
	WX_OnError_m18B3D38CA286841E0FE4C2090285DD26C5F43430,
	WX_OffError_m08AA6A8FA19F105833934D8245AB1622B2E9C88F,
	WX_OnGyroscopeChange_mA83A73F8D217586EC7CF73F9108D9C2870D0162F,
	WX_OffGyroscopeChange_mE8405E72F39520074BD73AE0A440F8C77B2EE0E5,
	WX_OnHide_mB9AB7FF2DDB18B07545B722CC29DB11F9176B0DD,
	WX_OffHide_mD678A7B6D946B9A8F1287B7329F4D224239772B1,
	WX_OnInteractiveStorageModified_m9FC9D20E21CDE8CB9C1DD5B95181D0E29412EEFB,
	WX_OffInteractiveStorageModified_m7AB05AE0871C6FE3C965690B910F0EFBE8E855FD,
	WX_OnKeyDown_m8242A49CDED28314AA6CE0616D0AE2826D02D060,
	WX_OffKeyDown_mE44744160A0E57FCD8FE579B47350F51BAE8D8EB,
	WX_OnKeyUp_m36C3D4C2B929E4249E58731CC7AA828E49D2AF3D,
	WX_OffKeyUp_m94C5EF86B2D8289569F2EB6E267556569BC44636,
	WX_OnKeyboardComplete_mB1C0C2D0C057C4FE1D6D2CB770A0A5DBFD34B3C5,
	WX_OffKeyboardComplete_mC24563CAD3EB6C40D298D47CC9EB6EF841ED0A73,
	WX_OnKeyboardConfirm_mB9C71394AB63AD4FBDF263BA35A46D59411FEB22,
	WX_OffKeyboardConfirm_mEEB0404604C77CE4FA0EB82DE942647708734941,
	WX_OnKeyboardHeightChange_m457F6520C7EDFBD607C05371E7335291142D41CB,
	WX_OffKeyboardHeightChange_m97A5D45531BD293E16D2A417172CE15A0362605D,
	WX_OnKeyboardInput_m7BD19745552FA5872F85371A508E53FFAF88220E,
	WX_OffKeyboardInput_m91305A65C78A90166BB73A903EB1D1F0D69C5404,
	WX_OnMemoryWarning_mC863C25664F117554E3073F124C73CBDF674C471,
	WX_OffMemoryWarning_m0AA84F6C45E5CA5B689301D8E4B74ED73E598343,
	WX_OnMessage_mCA87915EDA12F7B412F28B08D835A4D7910DA9B0,
	WX_OnMouseDown_m2A3C98B376110692F898E13F9B6D12D9B18EE50A,
	WX_OffMouseDown_mAD816E59DB8A42B66FE3515F149271DD60E02F2E,
	WX_OnMouseMove_mD7FB7DFB8250162720F3E8B246E54DCF97DB69AC,
	WX_OffMouseMove_m302FB55DFE702EA4E0FDF5F2A8DA067419C5F8DB,
	WX_OnMouseUp_m418A30F3F1975DE302733FE641C54A6A326AAC87,
	WX_OffMouseUp_mF2E84D5F13D7B4F5E88943A4DFDDFB7CDEABA838,
	WX_OnNetworkStatusChange_mB4A5A586FA4877CC0ED45DC34E8342FF5B24381E,
	WX_OffNetworkStatusChange_m393E80CAA9598D4B875DB36D43DE0C72E83D32D1,
	WX_OnNetworkWeakChange_m3A5B167579965B52C7EBA82607542ADA005F4A79,
	WX_OffNetworkWeakChange_mDC13FBA29B128B3AF725C19A46C1E830F9DBBAC7,
	WX_OnShareMessageToFriend_m7C8A8A668A6CD0B6A4CBD6119398487614705DC5,
	WX_OnShow_m01B8746517D51AE050B72E00B9740EA74DC4BF51,
	WX_OffShow_mCFA0A06612DBCB4B753C267C0CBFF6ECE62124E8,
	WX_OnTouchCancel_m8242CA69B0946D0D21DA6DCFDC0B1FF4058CC060,
	WX_OffTouchCancel_m5FBC411E554FB87653D4A1766897F75B24AA91A7,
	WX_OnTouchEnd_m5FA63F672192AD520015ACF7C87AB2D15A35EFD0,
	WX_OffTouchEnd_m018B481CDCFAEC2869E4212B3283727E4630096F,
	WX_OnTouchMove_m6AE9FA0BF963D9DDF4A4415B446209647F67B85B,
	WX_OffTouchMove_m662B872836D8DF86627CA0D64D7CCD6F6CDAD2B8,
	WX_OnTouchStart_m3BF867542C004D9FD8FD375F3BE290F8510AEF5D,
	WX_OffTouchStart_m0667C8D8087354EE51EE13B17966F4C1CAA8E770,
	WX_OnUnhandledRejection_mA5208BBDA9F46182054F6557BD1E2AB88AA28C50,
	WX_OffUnhandledRejection_m964564268E735486CD5A2C52D68E0B14256E05B9,
	WX_OnUserCaptureScreen_m9FD3EF6B1BB33D835237FAC180E59155512B5B0D,
	WX_OffUserCaptureScreen_m81700EEC63584E59379480DDA4302B0AC14DB530,
	WX_OnVoIPChatInterrupted_m19834EE7E9278E1CB7EFC9A1325A4B2595EDE682,
	WX_OffVoIPChatInterrupted_m88D6B206195635A667F33EFE4E90557D9B0A1664,
	WX_OnVoIPChatMembersChanged_mAC5555309B636E32E27582DEE7EC0F14633F5618,
	WX_OffVoIPChatMembersChanged_m7C1E4A756BCE2A8CE6BFC827670B58793DC42820,
	WX_OnVoIPChatSpeakersChanged_mBF5CB8B6578864C942BE0F21FFE1F2BBB23B9126,
	WX_OffVoIPChatSpeakersChanged_m1798AC1B67E3A0C9CD11EE364B8EFFC80EC8D7A5,
	WX_OnVoIPChatStateChanged_m6DECB2DD45FCEC3723F36789E6901761A0572B58,
	WX_OffVoIPChatStateChanged_m1129B924C7B69B360FA377F9A1092DBF277FFDFB,
	WX_OnWheel_m04348C5D0B963B34560D0E6640018961390053ED,
	WX_OffWheel_m0087C62DCB5C405C825424A036EFA60FA4F1A50D,
	WX_OnWindowResize_mE94DD39829ED7D2152F060D5AA8C3CCA63210FC2,
	WX_OffWindowResize_m8FF923FB105DB53802235C0325D2905368803266,
	WX_OnAddToFavorites_mAAF9DF344F271D0747E1F13A9A238B3EEB44D587,
	WX_OffAddToFavorites_m0FEECC7BB2FB00F474EA5FFDC718F9255A092D1F,
	WX_OnCopyUrl_m3FB90DC29032116C4D11EC59E8BC8750AE422B1C,
	WX_OffCopyUrl_mC1163E4F8A781AB513978928C2BFA6CA3A00FB5A,
	WX_OnHandoff_m037FC7FEC1606FD5F9FF72D040442A093733CC99,
	WX_OffHandoff_m29686F8613400BAE5484ABB60EE9D5CFB558A28B,
	WX_OnShareTimeline_m3F2315B1AA3F8D4708E47015B7EDA8D80588B7B9,
	WX_OffShareTimeline_m1A4D76297068A53ED8E5E883D5BF1D9258967E75,
	WX_OnGameLiveStateChange_mB0A42C996DD18E24C56B75ED4E8ABA81ADCA67EF,
	WX_OffGameLiveStateChange_m27FCDCB5C91FAB6BB08A88F984D512C60C3411F8,
	WX_SetHandoffQuery_m834FB923359081D4152A73FEF9A4208C233417DE,
	WX_GetAccountInfoSync_m57272966214D1352F42EA8A39BFE40437167EB8F,
	WX_GetAppAuthorizeSetting_m2896E977060374F148562E53C3E9341BFDE56B37,
	WX_GetAppBaseInfo_mC25C5A2E713E7CAF64E89E8C6E035B1522911151,
	WX_GetBatteryInfoSync_m08BBDF3FA67CB2D5C7D289E26F9B90A483B30E0B,
	WX_GetDeviceInfo_m0D3A68B2F6C630A323ED8A1AF928438F1AB4E39A,
	WX_GetEnterOptionsSync_m318300331B2C17F4B3823FCA25AA1C3CBE69EE78,
	NULL,
	NULL,
	WX_GetLaunchOptionsSync_mE16332D0A709F07149EF18DB238DE73AE726F225,
	WX_GetMenuButtonBoundingClientRect_m70E226210FE612589D5B1C480FE9770BD0D400E6,
	WX_GetStorageInfoSync_mA72BA5AC5A7148624E2B17F6896D3FD3EB70FC0D,
	WX_GetSystemInfoSync_m77ABFDC11AA5A3B00CB56546B9AAE6DCA4D03278,
	WX_GetSystemSetting_mC51600C0ACCD81E143C2E9C894ADC52513B8BD9A,
	WX_GetWindowInfo_mE6BA26B215C58A1E110EF3143E610D023D178718,
	WX_CreateImageData_mA35A11C442E73701D0BAE2141A411174748E4B2B,
	WX_CreatePath2D_mC42A18C53051B87EF8DA82A8375F7F9218547C42,
	WX_SetCursor_m1AF4A6E44DC942FFD3A56D10FBC82FA6C4698211,
	WX_SetMessageToFriendQuery_m44314EB7EC52B976F66AE28EC38B5DDC2B2CB9F7,
	WX_GetTextLineHeight_mA1ECDFF1AB37E253DFAD69996AEA357E064B3344,
	WX_LoadFont_mE33673A1619E9F577FC84DAF7EB9ADE3899FCE04,
	WX_GetGameLiveState_mEB0949BC28A3C710A42FB5A9768DC9EC782B4FC0,
	WX_DownloadFile_m8A662C30E43638AAC0D9C7D29C0A08AECD19C327,
	WX_CreateFeedbackButton_m7F6AAFCA0E69ABFF84ED7A09FC69FB9D0A257FE7,
	WX_GetLogManager_m906A05A85E2FCA5E945A2CFE38A79BD30BA0FDFF,
	WX_GetRealtimeLogManager_m33C3CBC2E2DE7CC53D74A924B71C22A151FDE349,
	WX_GetUpdateManager_mF4C32977971241B271D04EE0B941629A981D68B5,
	WX_CreateVideoDecoder_m93B078190F2B045C83509478206B4F7B9CAA08A5,
	WX__ctor_m907D9E1FBAA74BF99F9F0C5D4364D48D8D1030CE,
	WXBase_get_env_m4C1152C9C0072D9B528093A10DDAD8457F542FE7,
	WXBase_get_cloud_mB152458C8D5BB3FAB903DDF03996177FC7F536CC,
	WXBase_InitSDK_mFBD2C6BFBD434B3D162A5AE13EB2680D0178C96A,
	WXBase_ReportGameStart_m12F4475E983AD71E3000AD6285B188C05038AB2D,
	WXBase_ReportGameSceneError_m13AF71DFE0FAB60C70410BE7F90B3EAD67AB7CC0,
	WXBase_WriteLog_m37D40E4A1750B2F3DE488C2758F154726E8876CB,
	WXBase_WriteWarn_m56105BA0D5D410FADDE89992637CB3D03E1CF133,
	WXBase_HideLoadingPage_m2B48CB9D31757E9D3F6B6F80A6FDD931EA60BA0A,
	WXBase_PreloadConcurrent_mC7EDD2AADFD5CB505F993C6F6B913F0E054C5AA5,
	WXBase_ReportUserBehaviorBranchAnalytics_m1D061F4F34A9F9FD3FC2E06C69B9160128F03F7C,
	WXBase_StorageSetIntSync_m9E8E068DF053D8C1382290691E35F160C60C79A2,
	WXBase_StorageGetIntSync_m240D27D3D013EBD59B567C73F035ECF4DD99A6BE,
	WXBase_StorageSetStringSync_mBBE8796A9F1FC5B06E793AB44A4ED2A7150D1A8D,
	WXBase_StorageGetStringSync_m04414187C1FB6E0A8EA9E280F096A6187006A144,
	WXBase_StorageSetFloatSync_mD51B0F1DAC9BBA50C370EA1384D6D792DF63B28C,
	WXBase_StorageGetFloatSync_mBF11538E62936F65C474C24415365099EFC70C51,
	WXBase_StorageDeleteAllSync_m11B0096D39600B47168A012C6F9E66D2C40F3296,
	WXBase_StorageDeleteKeySync_m88A9ADDE74802ABA9BE4D8624222BD974067A9CE,
	WXBase_StorageHasKeySync_m35AA0DE3FB5D076621A65BB6A9C241A778E423E5,
	WXBase_CreateUserInfoButton_mA8572BD3EE589F8E4066A5EC33C88523DB849684,
	WXBase_OnShareAppMessage_m7A40F3439F6B6052E14C650CEF3358A3B2AA3033,
	WXBase_CreateBannerAd_m3EC0E23CE9D4D8EDF42903DB2C210922B3C1DB8B,
	WXBase_CreateFixedBottomMiddleBannerAd_m91C627BF8FC464224E55F4D7F8DC9BEBF800CC18,
	WXBase_CreateRewardedVideoAd_m9B23A01C5C921FBBE377E97E2595349BB00E0D9A,
	WXBase_CreateInterstitialAd_m126184605652891ED94F3E3B15CF607E1E4264F6,
	WXBase_CreateCustomAd_mA54CD2CD34C4E4CF7FF1AA73A6BBC09EED9D8203,
	WXBase_GetGameRecorder_m78E3AEB99AF8C9ABB61810AA5A1EED4F8F994171,
	WXBase_CreateCamera_m28CCF9C4F74A95666371DC514E55CE19E2B4C691,
	WXBase_GetRecorderManager_mE2CA9AF475636D91BC4BC378C8EC06ADC95DB05A,
	WXBase_CreateMiniGameChat_m9018D0AD0605C369B4AEFD25CE0074BAB4BD3208,
	WXBase_UploadFile_mCC5606D20EAAE98BA246A96F2BA6150E7FDFC8C7,
	WXBase_GetFileSystemManager_m11846F545591ACBA9ADF7F71DA6815341CBD0C46,
	WXBase_GetOpenDataContext_m2EA9ED06374FD6EAF573091ADD5E6F651077DBE7,
	WXBase_ShowOpenData_m862903D7AA2836AE83EA4B0A95643EA491475529,
	WXBase_HideOpenData_m6840242E9A69AA5EB6C57C6083F1D45598ED656C,
	WXBase_CreateInnerAudioContext_m28578E41F69B90FF0811C5652EFE50F02B0DD54A,
	WXBase_PreDownloadAudios_m8DFF05EB18C4DDFA6B705EECFFAA85F91113FAA6,
	WXBase_CreateVideo_mEB07EDEE4E10B053AC3A38A11D58D9343A3F7B02,
	WXBase_GetTotalMemorySize_m6217F33824BC6AE3D4D385144CED4E0224C13C0F,
	WXBase_GetTotalStackSize_m6770A0765F2F4A68964C44CCF6E45C70B4FD8FAB,
	WXBase_GetStaticMemorySize_m5BADD1F0339C961DFB71577EF1CA31C874D7D465,
	WXBase_GetDynamicMemorySize_mC6448D08728A2F34C1652367294D44A2C82B8FD2,
	WXBase_GetUsedMemorySize_mC007B61826D09EBCDA7B45DB29F537949CEDDBB0,
	WXBase_GetUnAllocatedMemorySize_m180CD69D6B46A0B89A053DFDF866BE28535EEE78,
	WXBase_LogUnityHeapMem_mFBBEC721CF5310E25F4ECCC701B1E55A9B69B80B,
	WXBase_GetBundleNumberInMemory_mD4FBB7653BBBF9C373E001C118E96F0ABA31528C,
	WXBase_GetBundleNumberOnDisk_m893B6EA6AB2B8DB90DA8C6BEDE66329576D0B7D5,
	WXBase_GetBundleSizeInMemory_m579A7FB9508914FC5038B751D0047E96E55F1E20,
	WXBase_GetBundleSizeOnDisk_m898EBF7AE802FCF3CAC9A4AC3B53FEA6582978E4,
	WXBase_OpenProfileStats_mBAD5CF57D51C918A37DBFF6BC11107C8A31F8A42,
	WXBase_ProfilingMemoryDump_m0E5BF72F05CAF2208F1BBE51556F12856F44DE79,
	WXBase_LogManagerDebug_m9F4587A42C51811C50B4D7B36BDEDA8804A4AED2,
	WXBase_LogManagerInfo_m59C3ED5ABCA4C37B1DE3CBB774193589788B5F73,
	WXBase_LogManagerLog_m06251F555BF957E2D2C6C67C908E37D757175C22,
	WXBase_LogManagerWarn_m77E1BF39EBF01F6C61C9EF32680977DA68A28F27,
	WXBase_IsCloudTest_mFBA682A37175FBBB4071E1FE8068DEE50A97D178,
	WXBase_CleanAllFileCache_m500240C22FD124122A04108983C3A8368FB6E7DB,
	WXBase_CleanFileCache_m0793EFFCE2E0E5EBD40BBDA2485006C46930C6F3,
	WXBase_RemoveFile_mCCF96D2F374DF8A8183DDD87EC34AD25F1A27329,
	WXBase_get_PluginCachePath_m16EFD4383916E10E4959567DC7E43BA86D4C432B,
	WXBase_GetCachePath_m488E2133F1DF458269961D39CFF4B4409F4446C2,
	WXBase_OnLaunchProgress_m80C0517D266CB8663A4511A7D5F687D81BA7A459,
	WXBase_UncaughtException_m3D9F95660E0CE211CCADB67A3C7C980B6BA4BA8F,
	WXBase_CreateGameClubButton_mF3295ECEF32B69D2F733392FC68F7797DB98DAE9,
	WXBase_OnNeedPrivacyAuthorization_mAA392F8A899B36FC1D901A5789B93353A6D93A18,
	WXBase_PrivacyAuthorizeResolve_mE3359C78EE7C207D2245BE7B591639303DDE11BC,
	WXBase_CreateUDPSocket_m3C786153D6366B2D1DBDFD8510EACB2816C5F90C,
	WXBase_CloseUDPSocket_m6086B7AFD265054FECC97FEDDECA01F564FA5A30,
	WXBase_SendUDPSocket_mD13610F42778207D231F5A4113DD0E452EC5BF88,
	WXBase_SetDataCDN_m6D9E65074FB5E965C18A5B01AEABED0B212767E8,
	WXBase_SetPreloadList_mA91CA72D04C1602ED35085231CEC3C6C97090A02,
	WXBase_GetWXFont_m033FCEF5D4CA8249DCE68D63F5C1943F787F2DDA,
	WXBase__ctor_m29AEEA990554608AD99DE5754A9EB556CB245C3D,
	U3CU3Ec__DisplayClass74_0__ctor_mFED584ADB8384261E7F3548114251E6CD1162F28,
	U3CU3Ec__DisplayClass74_0_U3CGetWXFontU3Eb__0_m8A459751D3491E5B1B5BF99DFA1A6C0F0CD4458A,
	U3CU3Ec__DisplayClass74_0_U3CGetWXFontU3Eb__1_m265EC0DD7B7352F0BA32E3B78ED8102DBDEE7C6E,
};
static const int32_t s_InvokerIndices[383] = 
{
	1766,
	1766,
	3572,
	1766,
	1766,
	1766,
	1766,
	372,
	1766,
	1766,
	1766,
	3572,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1576,
	1576,
	1576,
	1576,
	1766,
	1766,
	1455,
	1455,
	2401,
	1668,
	1668,
	1698,
	1500,
	1766,
	1571,
	3572,
	1766,
	1314,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	0,
	3127,
	2683,
	3487,
	3487,
	3487,
	3481,
	0,
	3487,
	3572,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3487,
	3203,
	3543,
	3543,
	3543,
	3543,
	3543,
	3543,
	0,
	0,
	3543,
	3543,
	3543,
	3543,
	3543,
	3543,
	3543,
	3543,
	2456,
	3203,
	3248,
	3341,
	3543,
	3341,
	3341,
	3341,
	3543,
	3543,
	3543,
	1766,
	3543,
	3543,
	3487,
	3572,
	2356,
	3487,
	3487,
	3572,
	3483,
	2720,
	3128,
	2923,
	3131,
	2981,
	3135,
	3032,
	3572,
	3487,
	3203,
	1919,
	3131,
	3341,
	2582,
	3341,
	3341,
	3341,
	3543,
	3341,
	3543,
	3341,
	3341,
	3543,
	3543,
	2104,
	3572,
	3341,
	3131,
	3341,
	3567,
	3567,
	3567,
	3567,
	3567,
	3567,
	3572,
	3567,
	3567,
	3567,
	3567,
	3572,
	3572,
	3487,
	3487,
	3487,
	3487,
	3522,
	3487,
	3113,
	3131,
	3543,
	3341,
	3487,
	3572,
	3341,
	3487,
	3487,
	2523,
	3483,
	2360,
	3487,
	3487,
	3131,
	1766,
	1766,
	1576,
	1576,
};
static const Il2CppTokenRangePair s_rgctxIndices[4] = 
{
	{ 0x060000AB, { 0, 2 } },
	{ 0x060000B2, { 2, 2 } },
	{ 0x0600011E, { 4, 2 } },
	{ 0x0600011F, { 6, 2 } },
};
extern const uint32_t g_rgctx_T_t1CB7DC9361C523555DC4157D1D0FB7A082F351A6;
extern const uint32_t g_rgctx_WXSDKManagerHandler_ReportEvent_TisT_t1CB7DC9361C523555DC4157D1D0FB7A082F351A6_m7B130508C4815D850A52745CADF0D9F8858063DB;
extern const uint32_t g_rgctx_T_tF69F68004BDFDC1D9BDE6944B6976D0E1990CCE3;
extern const uint32_t g_rgctx_WXSDKManagerHandler_SetStorageSync_TisT_tF69F68004BDFDC1D9BDE6944B6976D0E1990CCE3_m192B0EA266FB436E8D69EA04EFA7BF6199E3965E;
extern const uint32_t g_rgctx_WXSDKManagerHandler_GetExptInfoSync_TisT_tBEDF3064FD010879A77C42DCBDF1F1778AC9F08D_m7F6395BD936864476951DF939E8F1022982D6EB3;
extern const uint32_t g_rgctx_T_tBEDF3064FD010879A77C42DCBDF1F1778AC9F08D;
extern const uint32_t g_rgctx_WXSDKManagerHandler_GetExtConfigSync_TisT_t058C3A7FE96C94D2618761FE64BEAFEDC926326D_m4A1DE92B25714FB4312A40437CD19950A1B50103;
extern const uint32_t g_rgctx_T_t058C3A7FE96C94D2618761FE64BEAFEDC926326D;
static const Il2CppRGCTXDefinition s_rgctxValues[8] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t1CB7DC9361C523555DC4157D1D0FB7A082F351A6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_WXSDKManagerHandler_ReportEvent_TisT_t1CB7DC9361C523555DC4157D1D0FB7A082F351A6_m7B130508C4815D850A52745CADF0D9F8858063DB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tF69F68004BDFDC1D9BDE6944B6976D0E1990CCE3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_WXSDKManagerHandler_SetStorageSync_TisT_tF69F68004BDFDC1D9BDE6944B6976D0E1990CCE3_m192B0EA266FB436E8D69EA04EFA7BF6199E3965E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_WXSDKManagerHandler_GetExptInfoSync_TisT_tBEDF3064FD010879A77C42DCBDF1F1778AC9F08D_m7F6395BD936864476951DF939E8F1022982D6EB3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tBEDF3064FD010879A77C42DCBDF1F1778AC9F08D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_WXSDKManagerHandler_GetExtConfigSync_TisT_t058C3A7FE96C94D2618761FE64BEAFEDC926326D_m4A1DE92B25714FB4312A40437CD19950A1B50103 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t058C3A7FE96C94D2618761FE64BEAFEDC926326D },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Wx_CodeGenModule;
const Il2CppCodeGenModule g_Wx_CodeGenModule = 
{
	"Wx.dll",
	383,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	4,
	s_rgctxIndices,
	8,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
