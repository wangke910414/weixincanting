﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif





// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_tE8693FF0E67CDBA52BAFB211BFF1844D076ABAFB;
// System.Action`1<System.Boolean>
struct Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C;
// System.Action`1<UnityEngine.Camera>
struct Action_1_t268986DA4CF361AC17B40338506A83AFB35832EA;
// System.Action`1<UnityEngine.Font>
struct Action_1_tD91E4D0ED3C2E385D3BDD4B3EA48B5F99D39F1DC;
// System.Action`1<System.Int32>
struct Action_1_tD69A6DC9FBE94131E52F5A73B2A9D4AB51EEC404;
// System.Action`1<System.IntPtr>
struct Action_1_t2DF1ED40E3084E997390FF52F462390882271FE2;
// System.Action`1<WeChatWASM.WXCloudCallFunctionResponse>
struct Action_1_tA67329F55CC37955DC2C1A2ACDB7B1816C26D26D;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_tD7438462601D3939500ED67463331FE00CFFBDB8;
// System.Action`2<UnityEngine.PhysicsScene,Unity.Collections.NativeArray`1<UnityEngine.ModifiableContactPair>>
struct Action_2_t70E17A6F8F03189031C560482454FE2D87F496F2;
// System.Action`2<UnityEngine.Vector2,LitJson.JsonWriter>
struct Action_2_tE5C1059CE8ACEDAC4083A88C76E66DE3E15B8105;
// System.Action`2<UnityEngine.Vector3,LitJson.JsonWriter>
struct Action_2_t3CFBD6C56B087A7CE032945C0950C5716540158A;
// System.Action`3<System.Boolean,System.Boolean,System.Int32>
struct Action_3_t4730167C8E7EB19F1E0034580790A915D549F6CB;
// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t6AC338B3CEB934A66B363042F19213FE666F6818;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
struct Dictionary_2_tD74A089D3CFE69E54B1617003276B07F5B82B598;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,EventParamProperties>>
struct Dictionary_2_t33B19FEC6E64560F9692E9EEFF3D7968ECF0F0E1;
// System.Collections.Generic.Dictionary`2<System.String,FoodItem>
struct Dictionary_2_tF2DE9E763810A0184A1249A7A7DBFFDB56E12654;
// System.Collections.Generic.Dictionary`2<System.String,FoodMenu>
struct Dictionary_2_t36BB2B8FA33F99D093898CA851F7F69E5932A791;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_tEFC8016EC28460E6CE058A5F413FAB656883AA5F;
// System.Collections.Generic.Dictionary`2<System.String,GuestTable>
struct Dictionary_2_tC04ADB196C9D53B46FA973F626B7B8DD297EE170;
// System.Collections.Generic.Dictionary`2<System.String,GuideInfo>
struct Dictionary_2_t1A6B95EB52FB5C6A88974817699958F8C173DA64;
// System.Collections.Generic.Dictionary`2<System.String,MainFoodCompnentAssets>
struct Dictionary_2_t64FA1E93FCA2400B5F74FBF9D1D3A2A63D27742E;
// System.Collections.Generic.Dictionary`2<System.String,MainFoodItem>
struct Dictionary_2_t4B8935993E427E3606E2289FCEDC209328425484;
// System.Collections.Generic.Dictionary`2<System.String,OtherFoodItem>
struct Dictionary_2_t6CB4CCB4E14D05AC8639EA8C504CF7F532EE3031;
// System.Collections.Generic.Dictionary`2<System.String,YooAsset.PackageAsset>
struct Dictionary_2_tD8ABFB42C548277409CBF9A8DD0E155EFFABDBEF;
// System.Collections.Generic.Dictionary`2<System.String,YooAsset.PackageBundle>
struct Dictionary_2_t6EA1538A43358C8E44D5F0C1F2E23EFB0DCDEE6D;
// System.Collections.Generic.Dictionary`2<System.String,ResourceProfile>
struct Dictionary_2_t10507DB9B26DC4E3E709E064D33BECCADCACC8E3;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83;
// System.Collections.Generic.Dictionary`2<System.String,WXProfileStatsScript/ProfValue>
struct Dictionary_2_tC63C4A4A3B816DD32DF40BB25346F9AC6D342BAB;
// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Glyph>
struct Dictionary_2_tC61348D10610A6B3D7B65102D82AC3467D59EAA7;
// LitJson.ExporterFunc`1<UnityEngine.Color>
struct ExporterFunc_1_t8DEC56EC9C315918327B728EF33F55CE4D9E0204;
// LitJson.ExporterFunc`1<UnityEngine.Color32>
struct ExporterFunc_1_tAD0CADBFAA1F3669790D9F40D2D389420D24C825;
// LitJson.ExporterFunc`1<UnityEngine.Quaternion>
struct ExporterFunc_1_t0BDB0977957B702E553434D63D76FD4CBBF79E9D;
// LitJson.ExporterFunc`1<UnityEngine.Rect>
struct ExporterFunc_1_t396C38F979C2517DA37B85C04250C0FB38012D32;
// LitJson.ExporterFunc`1<UnityEngine.RectOffset>
struct ExporterFunc_1_t51D2BA9CE9D91E588EEC4288A798B6F13E55C90B;
// LitJson.ExporterFunc`1<System.Type>
struct ExporterFunc_1_t66337A5BC3E84B1D048E2D5A3E6B7DA41671BC95;
// LitJson.ExporterFunc`1<UnityEngine.Vector4>
struct ExporterFunc_1_t68203C504BE983A9A387E0769D39E83F63B0F234;
// System.Func`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Vector2>>
struct Func_1_tF5F7F5DCF1679E08B2536581A6E1EEF5529155C9;
// System.Func`1<System.Boolean>
struct Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457;
// System.Func`2<System.Exception,System.Boolean>
struct Func_2_tDDBE08B46BEFDD869DE0B97D023CB9C89674FED6;
// System.Func`3<System.Int32,System.IntPtr,System.Boolean>
struct Func_3_t2376B3D8C7A437FC32F21C4C4E4B3E7D2302007C;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_tEFC6605F7DE53F71946C33FD371E53C3100F2178;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>>
struct IDictionary_2_t9503DE0744217071D554CC415B2890C454070024;
// System.Collections.Generic.IDictionary`2<System.String,LitJson.JsonData>
struct IDictionary_2_t8B67C0BACED3ABBE0BB390A264074F9245F22842;
// System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>
struct IDictionary_2_tC6EEF4DE19255D76EDF497CDE277E5578CB81EB2;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>
struct IDictionary_2_t0BD8FD9B6F60BD6B18C6D1CD2BE215E7F97D3D63;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>
struct IDictionary_2_tE7FFBD71D86A280E4B101DF2F8E59C9FFFAA4C29;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IList`1<LitJson.PropertyMetadata>>
struct IDictionary_2_t02C6BF8BB03B58DD552B193E9CABACD0AF9CCFA4;
// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ArrayMetadata>
struct IDictionary_2_t8918FFC362E99797BB0E47D0D787D923EC3C8AAA;
// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>
struct IDictionary_2_t74600F66BA04DC423A37DB24919A3C9D56D75B43;
// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ObjectMetadata>
struct IDictionary_2_tBDC061B9374E6628F4296689FB32A9C598E45C4B;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t349E66EC5F09B881A8E52EE40A1AB9EC60E08E44;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>
struct IEnumerator_1_tA8D764B99B6196299ECE08BCBA02AB2EE733CB1D;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>
struct IList_1_tCE0C7B37A9126CC0CB3277FBC9C4B9161B866C32;
// System.Collections.Generic.IList`1<LitJson.JsonData>
struct IList_1_t0E22BD75314F60626B98C2C1FA5B2634BA7F5A82;
// LitJson.ImporterFunc`2<System.String,System.Type>
struct ImporterFunc_2_tECF771648EC0EB35785D9EE3003167BEBB8C8008;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t01207CE5982A7640E56B1F9F672A06F96B09367A;
// System.Collections.Generic.List`1<FoodMenu>
struct List_1_tC3DE54B9D2C3A344BC846F8CBE284432FA16F496;
// System.Collections.Generic.List`1<FoodMenuAsset>
struct List_1_t4F1D474EA64688149310819BB63A0150C3F2E05A;
// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
struct List_1_tA5BCD116CC751A5F35C7D3D7B96DC3A5D22B9C82;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tB951CE80B58D1BF9650862451D8DAD8C231F207B;
// System.Collections.Generic.List`1<GameTaskData>
struct List_1_tACBAE0673F1BCE8E607A86B035A8608EF1BB7589;
// System.Collections.Generic.List`1<GuestData>
struct List_1_t2876763590C4FEA8FB181927604F91536995879F;
// System.Collections.Generic.List`1<GuideInfo>
struct List_1_tD2AB7134E6C9D42A3393505AE4AFE4D76C397EBF;
// System.Collections.Generic.List`1<HeadAssetData>
struct List_1_t8F88710524483B00806CC60AFF4A3236AF0B9C78;
// System.Collections.Generic.List`1<LevelData>
struct List_1_t5D51C2F548F7E4364A14BB81D41C0CAE0A900E0A;
// System.Collections.Generic.List`1<LevelGuestData>
struct List_1_t1A68216FCF6D253E3E40657E99C290E07D0413A2;
// System.Collections.Generic.List`1<MainFoodCompnentAssets>
struct List_1_t3363445FC8694C84D8D8A916236F50B706E93E42;
// System.Collections.Generic.List`1<OtheFoodCompnentAssets>
struct List_1_t990D89EFEC6E608F0D9D16CE57058E1671FC610B;
// System.Collections.Generic.List`1<YooAsset.PackageAsset>
struct List_1_t1F7449D98AADDC78A54D629F7AF999996F0A527E;
// System.Collections.Generic.List`1<YooAsset.PackageBundle>
struct List_1_t77C2174B5012E5EF8FE5C6F908B221D380B6853D;
// System.Collections.Generic.List`1<RawFoodData>
struct List_1_t1A705E9D66D0956C99CA98474E7D4C64B0B5CDBD;
// System.Collections.Generic.List`1<YooAsset.ResourcePackage>
struct List_1_t34A5F992A0C261FD9E072E04F6EB044CC633B43A;
// System.Collections.Generic.List`1<RestaurantData>
struct List_1_t6D46605B0341B96D804BAD55EB4985B0826CA52A;
// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
struct List_1_tCD5F926D25FC8BFAF39E4BE6F879C1FA11501C76;
// System.Collections.Generic.List`1<ShoppItemGemData>
struct List_1_t992A3C63FF431498EDD99CB83B368E4015B111E5;
// System.Collections.Generic.List`1<ShoppItemPropData>
struct List_1_tA59155E7EFCE6269614D91A3BFB182B4F6E7044C;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t0D1C46FD8DDDE974D93CA4F3474EEC05AF950918;
// System.Collections.Generic.List`1<System.String>
struct List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD;
// System.Collections.Generic.List`1<TouchData>
struct List_1_tD85647D7BC0A47DAC4D5BF624DAAC00B6D45763C;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t67A1600A303BB89506DFD21B59687088B7E0675B;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t9209B29AC606399207E97BDCD817DEA5B6C63CA5;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t09F8990ACE8783E311B473B0090859BA9C00FC2A;
// System.Collections.Generic.List`1<UnityEngine.Yoga.YogaNode>
struct List_1_t84B666107A8A3ECB0F5A24B0243137D056DA9165;
// System.Predicate`1<TouchData>
struct Predicate_1_t52132B0333B4BB28E1E9EC424EAED5B7E2B648C6;
// System.Collections.Generic.Stack`1<System.Int32>
struct Stack_1_t3197E0F5EA36E611B259A88751D31FC2396FE4B6;
// System.Collections.Generic.Stack`1<LitJson.WriterContext>
struct Stack_1_t2CDC562A4A6FD1FE4DE0FC63A08FC106C6A84EFD;
// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>
struct Task_1_tE41CFF640EB7C045550D9D0D92BE67533B084C17;
// System.Boolean[]
struct BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t0D512FF2FF5A72DAC04754F2C10182F850328BEF;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// UnityEngine.Camera[]
struct CameraU5BU5D_t1506EBA524A07AD1066D6DD4D7DFC6721F1AC26B;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411;
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t427621BF8902AE33C86E7BF384D9B2B5B781F949;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t1BA4BCF4D4D32DF07E9B84F1750D964DF33B0FEC;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF;
// UnityEngine.TextCore.Glyph[]
struct GlyphU5BU5D_t345CEC8703A6C650639C40DB7D35269A2D467FC5;
// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct[]
struct GlyphMarshallingStructU5BU5D_t9424A4B1FAAD615472A9346208026B1B9E22069E;
// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord[]
struct GlyphPairAdjustmentRecordU5BU5D_tD5DD2A739A4CA745E7F28ECCB2CD0BD0A65A38F7;
// UnityEngine.TextCore.GlyphRect[]
struct GlyphRectU5BU5D_t494B690215E3F3F42B6F216930A461256CE2CC70;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// System.Single[]
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// TMPro.TextMeshProUGUI[]
struct TextMeshProUGUIU5BU5D_tCAB9404D43876DF686DFBB4613543479CC1CC24A;
// UnityEngine.Transform[]
struct TransformU5BU5D_tBB9C5F5686CAE82E3D97D43DF0F3D68ABF75EC24;
// System.UInt32[]
struct UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
// LitJson.Lexer/StateHandler[]
struct StateHandlerU5BU5D_tA4E04C837AF2BDFE9B351DF6813E6AB954722B93;
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_tDAE7DF0D2B0BE3EB2FD25FB4418704E27A2BF1D5;
// b/c[]
struct cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E;
// b/d[]
struct dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066;
// c/a[]
struct aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2;
// c/c[]
struct cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF;
// System.Action
struct Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07;
// UnityEngine.AnimationState
struct AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE;
// System.Collections.ArrayList
struct ArrayList_t7A8E5AF0C4378015B5731ABE2BED8F2782FEEF8A;
// UnityEngine.AssetBundle
struct AssetBundle_tB38418819A49060CD738CB21541649340F082943;
// YooAsset.AssetOperationHandle
struct AssetOperationHandle_t3C4EE34040385828202FA37D72CC91D7668FB32B;
// YooAsset.AssetSystemImpl
struct AssetSystemImpl_t81D235BF692751145D6B2C3F3431BC7D8290BD38;
// System.AsyncCallback
struct AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C;
// UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_tE2E8D79241711CDF9AB428C7FB96A35D80E40B04;
// UnityEngine.Experimental.Audio.AudioSampleProvider
struct AudioSampleProvider_t602353124A2F6F2AEC38E56C3C21932344F712E2;
// UnityEngine.AudioSource
struct AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299;
// UnityEngine.Yoga.BaselineFunction
struct BaselineFunction_t13AFADEF52F63320B2159C237635948AEB801679;
// System.IO.BinaryReader
struct BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158;
// System.IO.BinaryWriter
struct BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E;
// UnityEngine.UI.Button
struct Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098;
// UnityEngine.Camera
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184;
// UnityEngine.CanvasGroup
struct CanvasGroup_t048C1461B14628CFAEBE6E7353093ADB04EBC094;
// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804;
// UnityEngine.CharacterController
struct CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A;
// CloudSaveSystem
struct CloudSaveSystem_t419542980442B82C4BB089FE4FFEDD48855C1AA6;
// CloundSavePlayerDaata
struct CloundSavePlayerDaata_t1F9D694D8D690B33A0868EDCE49D4C83F8C77FD8;
// UnityEngine.Collider
struct Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76;
// ComparePlayerDataPlanel
struct ComparePlayerDataPlanel_t2A3BBF964761B874A51BB23F98AFD96D7C7D48B6;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0;
// DamondPlane
struct DamondPlane_tBE7F8A5FAAC8C3AFDD5C480FC51C5C229D9DE93B;
// DataPlanel
struct DataPlanel_t63B93F225509A11432BB74BCC752CB1F1846EE66;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB;
// DrinkOpreat
struct DrinkOpreat_tE3BB22B195D920181FB3F2361BCDC241DEC2FF15;
// LitJson.ExporterFunc
struct ExporterFunc_t0E0FD6EE483CD6380339D2D7AC616079B1D5ED8A;
// FireEffect
struct FireEffect_t3405E939E4E39C43487E73C9DA6799E6680567B8;
// UnityEngine.Font
struct Font_tC95270EA3198038970422D78B74A7F2E218A96B6;
// FoodProperty
struct FoodProperty_tAAA816769EC7DC405C1AABDF794168ED46D9AF4E;
// LitJson.FsmContext
struct FsmContext_t039D467FB708DBF74426E9076FDA22760B0D30D1;
// UnityEngine.GUIContent
struct GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2;
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F;
// UnityEngine.GUILayoutGroup
struct GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D;
// UnityEngine.GUISettings
struct GUISettings_tF2CA7E8B9F62F1FC013BFF053B5FA2709EBA3847;
// UnityEngine.GUISkin
struct GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9;
// UnityEngine.GUIStyle
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580;
// UnityEngine.GUIStyleState
struct GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95;
// GameHomePlanel
struct GameHomePlanel_t8A036D21BEF99C8AE3853851AB94592D7821F08C;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// UnityEngineInternal.GenericStack
struct GenericStack_t1FB49AB7D847C97ABAA97AB232CA416CABD24C49;
// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931;
// Guest
struct Guest_t7C87006A91E9A72C586B6082DB0B7192FB76B4F3;
// GuestManager
struct GuestManager_t08BB44CBB90850DB991994981F09DF507D6E47E7;
// GuestParentInfo
struct GuestParentInfo_t89532795C9D4D6B5B3F4BB06EA6F6732125B48F9;
// HeadAssetData
struct HeadAssetData_tAB6CEFDAB592FB132D8A492EF5DE6324840FFB0B;
// HeadPlanel
struct HeadPlanel_t30E55A397C5AC7D53D584544BB98F6BA41F6AC96;
// HomeMapUiManager
struct HomeMapUiManager_tED39D923B951D28CA26D7E8BBB2F0251C8A3E9F5;
// HomePlanel
struct HomePlanel_tC54E997184482AEDEE52C73099DB871A611E87A2;
// System.IAsyncResult
struct IAsyncResult_t7B9B5A0ECB35DCEC31B8A8122C37D687369253B5;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_t0680C7F905C553076B552D5A1A6E39E2F0F36AA2;
// YooAsset.IBuildinQueryServices
struct IBuildinQueryServices_t1A4A3727F9AA2279230FD999EAD4FE9CF76473DC;
// YooAsset.IBundleServices
struct IBundleServices_t83ED8DA733B42205E9C88F694E45ADB313530B86;
// YooAsset.IDeliveryQueryServices
struct IDeliveryQueryServices_t9E9B70536E405866C146F4F720178F02E6071D87;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.IFormatProvider
struct IFormatProvider_tC202922D43BFF3525109ABF3FB79625F5646AB52;
// LitJson.IJsonWrapper
struct IJsonWrapper_t447B39837883BA1281A17C3D6BA122AD22837DD9;
// YooAsset.ILogger
struct ILogger_tDF24C2D094417567E0429274CBA0896EF501517C;
// YooAsset.IPlayModeServices
struct IPlayModeServices_tAA52527E1B8B45CA174AECC67517825F92790C7A;
// YooAsset.IRemoteServices
struct IRemoteServices_t85350DE6972E304B67CE4A9BB60479DC1DDF3E56;
// UnityEngine.UI.Image
struct Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E;
// LitJson.ImporterFunc
struct ImporterFunc_t3D41CCB293D2761DC6862831F8BC45D580EB6AA5;
// YooAsset.InitializationOperation
struct InitializationOperation_t6B527F5DF87FBB08463CE61CDFBA82190F5CE66F;
// LitJson.JsonWriter
struct JsonWriter_t303427DE92107A0D92969763C95C5C20DB2D6F8F;
// UnityEngine.UI.LayoutElement
struct LayoutElement_tB1F24CC11AF4AA87015C8D8EE06D22349C5BF40A;
// LevelData
struct LevelData_tE6EAE053C277D3E4ED073363F0BF24BDBADFC4D5;
// LevelGuestData
struct LevelGuestData_t02E2B626BF5CBDA7A4186527CF21191B18AF1F76;
// LevelInfoPlane
struct LevelInfoPlane_tB71B01496603F22E8E44451B5A1EC9ACDABBFAC7;
// LevelManager
struct LevelManager_t8405886BBC5A0ACBB1CC210E25D5DA1C72D16530;
// LitJson.Lexer
struct Lexer_t8EA7E771DBFBCCC71E85B400AF902FC3F482EBC9;
// LoadConfigursManager
struct LoadConfigursManager_tB9C80FC9048CAE6F0BD82463288DCA90FF0D3895;
// LoadinPlanle
struct LoadinPlanle_tF6494D52114639C6DB4E80385577677753F498C0;
// UnityEngine.Yoga.Logger
struct Logger_t092B1218ED93DD47180692D5761559B2054234A0;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// UnityEngine.Yoga.MeasureFunction
struct MeasureFunction_t60EBED1328F5328D4FA7E26335967E59E73B4D09;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t8E26808B202927FEBF9064FCFEEA4D6E076E6472;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// OperationManage
struct OperationManage_tFF184A4A7A2B8B93259740624B38E08B84CB6FC0;
// UnityEngine.UI.Outline
struct Outline_t9CF146E077DC65F441EDEC463AA6710374108084;
// OverPlanel
struct OverPlanel_t3BD8295CC1BDBF33D0900B2F4059249914D6F9A0;
// YooAsset.PackageManifest
struct PackageManifest_tA9B8D10262EB5900AEB4329BF3B22F0353554CB5;
// PlayerDataParameter
struct PlayerDataParameter_t99940E8382048E611BEA20091478EA46C9E604CA;
// UnityEngine.RectOffset
struct RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5;
// UnityEngine.RectTransform
struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5;
// System.Text.RegularExpressions.Regex
struct Regex_tE773142C2BE45C5D362B0F815AFF831707A51772;
// YooAsset.ResourcePackage
struct ResourcePackage_t6B28B6B3A6DEAB641E6CBB06F383D7B947198022;
// RestaurantData
struct RestaurantData_t7D6C56717DA6F4BB4202FC873005799FA46C9F9F;
// RestaurantInfo
struct RestaurantInfo_t5A071066135E99AEC87FFC53FA192968999F8A19;
// UnityEngine.Rigidbody
struct Rigidbody_t268697F5A994213ED97393309870968BC1C7393C;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F;
// YooAsset.SafeProxy
struct SafeProxy_tB77642C8A36A3783EB9DFAC1415459DE6B11CF7E;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t17D2F2939CA8953110180DF53164CFC3DC88D70E;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t5C292A12062F24027A98492F52ECFE9802AA6F0E;
// DG.Tweening.Sequence
struct Sequence_tEADBE56D6ED2E9EE8FB2E5459C3E57131EC0545C;
// ShoppItemConfigur
struct ShoppItemConfigur_tB9BC6996C5A7913E1EEF4282BB2E238538D9F82C;
// ShoppPlanel
struct ShoppPlanel_t07BFA47DF40CE73299500CC5B2D0334F2F1353BF;
// UnityEngine.UI.Slider
struct Slider_t87EA570E3D6556CABF57456C2F3873FFD86E652F;
// UnityEngine.Sprite
struct Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B;
// UnityEngine.EventSystems.StandaloneInputModule
struct StandaloneInputModule_tD8B581E4A0A2A25B99EB002FF669C4EEED350530;
// StartHomeManager
struct StartHomeManager_tB9F9914830AFB48632C8B356435C243F2865B490;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.SynchronizationContext
struct SynchronizationContext_tCDB842BBE53B050802CBBB59C6E6DC45B5B06DC0;
// SystemDataFactoy
struct SystemDataFactoy_t0F295EDA05C547BBA48910AB76B4238A905BCFDB;
// SystemGameManager
struct SystemGameManager_tEBED10FE7ED1E6F2223FCAD4CB6FFFD68712293D;
// SystmConfigurationManager
struct SystmConfigurationManager_tC1C970C67458E76BA7D6A62A62A7B7DD818C1B89;
// TargetPlanle
struct TargetPlanle_t6A8EBC369B27AE5A05189F09204256FCF600578E;
// System.Threading.Tasks.Task
struct Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572;
// TaskPlanel
struct TaskPlanel_t885BA63C37C30C820EC675FCCE61B39C24BE96B6;
// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957;
// System.IO.TextReader
struct TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7;
// System.IO.TextWriter
struct TextWriter_tA9E5461506CF806E17B6BBBF2119359DEDA3F0F3;
// UnityEngine.Texture
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_tE87B78A3DAED69816B44C99270A734682E093E7A;
// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1;
// DG.Tweening.Tween
struct Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C;
// DG.Tweening.TweenCallback
struct TweenCallback_t7C8B8A38E7B30905FF1B83C943256EF23617BB24;
// System.Type
struct Type_t;
// UiManager
struct UiManager_tD893C8D36318160D6F63738C3B33FE3C22D9688F;
// UiMapPlane
struct UiMapPlane_t14D8BC95B1C4C0407390DC6A79DD55B3315B61A3;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F;
// UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6;
// System.Uri
struct Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// System.Threading.WaitCallback
struct WaitCallback_tFB2C7FD58D024BBC2B0333DC7A4CB63B8DEBD5D3;
// System.WeakReference
struct WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E;
// LitJson.WrapperFactory
struct WrapperFactory_tB4894CC7D3A41BE3FF6BA079B4490DF5FC5C7BDD;
// LitJson.WriterContext
struct WriterContext_t5E6D8EFF41F41F382870C9F0452344B25B6E666F;
// UnityEngine.Yoga.YogaConfig
struct YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345;
// UnityEngine.Yoga.YogaNode
struct YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA;
// YooAsset.YooAssetSettings
struct YooAssetSettings_tAA143F140144A8EE80D18FB10D26A93A841E6EE4;
// YooResoursceManagur
struct YooResoursceManagur_t41268F368AF5E66702DC6C69966EBCF528661E8B;
// lockPlanel
struct lockPlanel_tA610DCB63E2BDA659C0462950078BC088F970148;
// UnityEngine.Analytics.AnalyticsSessionInfo/IdentityTokenChanged
struct IdentityTokenChanged_tE8CB0DAB5F6E640A847803F582E6CB6237742395;
// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct SessionStateChanged_t1180FB66E702B635CAD9316DC661D931277B2A0C;
// UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback
struct OnOverrideControllerDirtyCallback_tDC67F7661A27502AD804BDE0B696955AFD4A44D5;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t3396D9613664F0AFF65FB91018FD0F901CC16F1E;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t8D7135A2FB40647CAEC93F5254AD59E18DEB6072;
// UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler
struct SampleFramesHandler_tFE84FF9BBCEFB880D46227188F375BEF680AAA30;
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_tE071B0CBA3B3A77D3E41F5FCB65B4017885B3177;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t76D5E172DF8AA57E67763D453AAC40F0961D09B1;
// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_tF844B2FAD6933B78FD5EFEBDE0529BCBAC19BA60;
// UnityEngine.GUISkin/SkinChangedDelegate
struct SkinChangedDelegate_tA6D456E853D58AD2EF8A599F543C7E5BA8E94B98;
// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_tB0D5A5BA322FE093894992C29DCF51E7E12579C4;

struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_com;
struct ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801;
struct ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com;
struct GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke;
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_marshaled_com;
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_marshaled_pinvoke;
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com;
struct RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_com;
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_com;
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_pinvoke;
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_com;
struct a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6;
struct c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_com;
struct c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tBB65183F1134474D09FF49B95625D25472B9BA8B 
{
};

// <Module>
struct U3CModuleU3E_tF062866229C4952B8051AD32AB6E9D931142CC95 
{
};

// <Module>
struct U3CModuleU3E_t625F4EE40CDF76E63929DA3A0864C5AEF068DCF1 
{
};

// <Module>
struct U3CModuleU3E_t90149EF90407715CC46EB5A9704669888393F1DE 
{
};

// <Module>
struct U3CModuleU3E_tDA285F13E9413BF3B79A99D6E310BE9AF3444EEB 
{
};

// <Module>
struct U3CModuleU3E_t4791F64F4B6411D4D033A002CAD365D597AA2451 
{
};

// <Module>
struct U3CModuleU3E_t88546BFE60862623CAD588FB9164795ED5BF96FF 
{
};

// <Module>
struct U3CModuleU3E_t853A105E2E1595E463CC860AFEE0FB13A177A12C 
{
};

// <Module>
struct U3CModuleU3E_t8B36B9B16FF72CF5A0EBA03D2FA162E77C86534C 
{
};

// <Module>
struct U3CModuleU3E_tAE2FD82E48B7893673000630524705C468010525 
{
};

// <Module>
struct U3CModuleU3E_t462BCCFB9B78348533823E0754F65F52A5348F89 
{
};

// <Module>
struct U3CModuleU3E_t2C7BF608494A5C8FB8C8C4D318FB27BCF6CE322A 
{
};

// <Module>
struct U3CModuleU3E_tB500DDB8D220627F9BFAF448763790F027856F11 
{
};

// <Module>
struct U3CModuleU3E_t941B0EB06FD57B79F043CCA70C8AA4C0B3FB68E7 
{
};

// <Module>
struct U3CModuleU3E_t2F9091E403B25A5364AE8A6B2C249E31D405E3F4 
{
};

// <Module>
struct U3CModuleU3E_tD4D8152B1CC10B76FF3BD3BF122F926B6BF0D3EE 
{
};

// <Module>
struct U3CModuleU3E_t0643977EA9107777E6F2E30DC5F5326A467F5F6B 
{
};

// <Module>
struct U3CModuleU3E_tCFCF033B61CFCC76C69180CF9A7B07EED67725EA 
{
};

// <Module>
struct U3CModuleU3E_t416C1B54F702B9F0B5C7C848BFDFA85A9E90F443 
{
};

// <Module>
struct U3CModuleU3E_t4BC86AB59C63F2CC6EB51BE560C15CFCAE821BC7 
{
};

// <Module>
struct U3CModuleU3E_t3B74AF9E7E84B3C57D4687184E31363228069DF2 
{
};

// <Module>
struct U3CModuleU3E_t806C4A82D63BA5BEE007D75772441609D967BADA 
{
};

// <Module>
struct U3CModuleU3E_t5E8190EE43F4DF5D80E8A6651A0469A8FD445F94 
{
};

// <Module>
struct U3CModuleU3E_tCFDAF3CE34E8117DEABC58BB3EBDB7B80EA66F5A 
{
};

// <Module>
struct U3CModuleU3E_t7A1E3DF1BFD27FA828B031F2A96909F13C3F170B 
{
};

// <Module>
struct U3CModuleU3E_t6B4A7D64487421A1C7A9ACB5578F8A35510E2A0C 
{
};

// <Module>
struct U3CModuleU3E_tA3EED36B70F256012F5BB9BC014BD75FF7BF2C66 
{
};

// <Module>
struct U3CModuleU3E_t42F165DEA2597BD5AB2C914FCF80349ECF878162 
{
};

// <Module>
struct U3CModuleU3E_t9BF3BFAB7D61D65692779A81BB8D587D68A78414 
{
};

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_tCA0A4120E1B13462A402E739CE2DD9CA72BAC713  : public RuntimeObject
{
};

// Mono.Security.ASN1
struct ASN1_t33549D58797C9C33AA83F13AD184EAA00C584A6F  : public RuntimeObject
{
	// System.Byte Mono.Security.ASN1::m_nTag
	uint8_t ___m_nTag_0;
	// System.Byte[] Mono.Security.ASN1::m_aValue
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___m_aValue_1;
	// System.Collections.ArrayList Mono.Security.ASN1::elist
	ArrayList_t7A8E5AF0C4378015B5731ABE2BED8F2782FEEF8A* ___elist_2;
};

// Mono.Security.ASN1Convert
struct ASN1Convert_tDA6D2B7710D7868F3D559D5BE7F2C7816BB50AB6  : public RuntimeObject
{
};

// UnityEngine.Analytics.AnalyticsSessionInfo
struct AnalyticsSessionInfo_tDE8F7A9E13EF9723E2D975F76E916753DA61AD76  : public RuntimeObject
{
};

// System.Attribute
struct Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA  : public RuntimeObject
{
};

// UnityEngine.Experimental.Audio.AudioSampleProvider
struct AudioSampleProvider_t602353124A2F6F2AEC38E56C3C21932344F712E2  : public RuntimeObject
{
	// UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler UnityEngine.Experimental.Audio.AudioSampleProvider::sampleFramesAvailable
	SampleFramesHandler_tFE84FF9BBCEFB880D46227188F375BEF680AAA30* ___sampleFramesAvailable_0;
	// UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler UnityEngine.Experimental.Audio.AudioSampleProvider::sampleFramesOverflow
	SampleFramesHandler_tFE84FF9BBCEFB880D46227188F375BEF680AAA30* ___sampleFramesOverflow_1;
};

// UnityEngine.AudioSettings
struct AudioSettings_t66C4BCA1E463B061E2EC9063FB882ACED20D47BD  : public RuntimeObject
{
};

// Mono.Security.BitConverterLE
struct BitConverterLE_tB6EF365ED05024FCC12DA3939B10FDEBDB29E1BD  : public RuntimeObject
{
};

// YooAsset.BufferReader
struct BufferReader_t058D020315958551EDFFB188DDAB89E05EF5112A  : public RuntimeObject
{
	// System.Byte[] YooAsset.BufferReader::_buffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____buffer_0;
	// System.Int32 YooAsset.BufferReader::_index
	int32_t ____index_1;
};

// YooAsset.BufferWriter
struct BufferWriter_tB11C275753FDCC1BA6675F7676FA91387456F886  : public RuntimeObject
{
	// System.Byte[] YooAsset.BufferWriter::_buffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____buffer_0;
	// System.Int32 YooAsset.BufferWriter::_index
	int32_t ____index_1;
};

// UnityEngine.CameraRaycastHelper
struct CameraRaycastHelper_tEF8B5EE50B6F5141652EAAF44A77E8B3621FE455  : public RuntimeObject
{
};

// CloudFunction
struct CloudFunction_t1007F8C7EAF212A9FD19D0FC2F0C71E348DBD958  : public RuntimeObject
{
};

// CloundSaveGameData
struct CloundSaveGameData_t1538A5337C49FA37E6F8A02F9F0F1BB2942011A6  : public RuntimeObject
{
};

// CloundSavePlayerDaata
struct CloundSavePlayerDaata_t1F9D694D8D690B33A0868EDCE49D4C83F8C77FD8  : public RuntimeObject
{
	// System.Int32 CloundSavePlayerDaata::cion
	int32_t ___cion_0;
	// System.Int32 CloundSavePlayerDaata::gem
	int32_t ___gem_1;
};

// System.Configuration.ConfigurationElement
struct ConfigurationElement_tAE3EE71C256825472831FFBB7F491275DFAF089E  : public RuntimeObject
{
};

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t1DEB95D3283BB11A46B862E9D13710ED698B6C93  : public RuntimeObject
{
};

// System.Configuration.ConfigurationSectionGroup
struct ConfigurationSectionGroup_tE7948C2D31B193F4BA8828947ED3094B952C7863  : public RuntimeObject
{
};

// UnityEngine.Analytics.ContinuousEvent
struct ContinuousEvent_t71122F6F65BF7EA8490EA664A55D5C03790CB6CF  : public RuntimeObject
{
};

// Mono.Security.Cryptography.CryptoConvert
struct CryptoConvert_t676AC22DA6332E9936696ECC97197AB7B1BC7252  : public RuntimeObject
{
};

// UnityEngine.CustomYieldInstruction
struct CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617  : public RuntimeObject
{
};

// DG.Tweening.DOTweenCYInstruction
struct DOTweenCYInstruction_tCB7C6C1E61447CF998FF1A85EDB5949ED08C303B  : public RuntimeObject
{
};

// DG.Tweening.DOTweenModuleAudio
struct DOTweenModuleAudio_tCF19013F0C086697BB9D373975D877E26BD2CE09  : public RuntimeObject
{
};

// DG.Tweening.DOTweenModulePhysics
struct DOTweenModulePhysics_t288D427B27A061E96FAD8BB322D3640CA9EC1C14  : public RuntimeObject
{
};

// DG.Tweening.DOTweenModulePhysics2D
struct DOTweenModulePhysics2D_t9F08692BD09A9DC4646782951FD80D8A529905F4  : public RuntimeObject
{
};

// DG.Tweening.DOTweenModuleSprite
struct DOTweenModuleSprite_tF147E9559811CF3DC449BEEBFF0EF43A5DB9BCD7  : public RuntimeObject
{
};

// DG.Tweening.DOTweenModuleUI
struct DOTweenModuleUI_t4988700FDB42C34ACA64236BD53E96D3049D86FE  : public RuntimeObject
{
};

// DG.Tweening.DOTweenModuleUnityVersion
struct DOTweenModuleUnityVersion_t028EE91988A2A48FC2EEACA97E9BA03468B748F6  : public RuntimeObject
{
};

// DG.Tweening.DOTweenModuleUtils
struct DOTweenModuleUtils_t5554865584F951A4A4E5DD282E6EBC60F5CEC6E9  : public RuntimeObject
{
};

// DrinkComponent
struct DrinkComponent_t0FF6886CB1698DB8190763330E6271F7C861F849  : public RuntimeObject
{
	// System.String DrinkComponent::drinkId
	String_t* ___drinkId_0;
	// System.String DrinkComponent::dringName
	String_t* ___dringName_1;
	// UnityEngine.GameObject DrinkComponent::prefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___prefab_2;
};

// YooAsset.EditorSimulateModeHelper
struct EditorSimulateModeHelper_tF14016CBA4F82CC8D27F07AB549E166097A0A73C  : public RuntimeObject
{
};

// YooAsset.EditorSimulateModeImpl
struct EditorSimulateModeImpl_t6FFD2482D3EE3B0A9FB7D870E4921BE917BB07F5  : public RuntimeObject
{
	// YooAsset.PackageManifest YooAsset.EditorSimulateModeImpl::_activeManifest
	PackageManifest_tA9B8D10262EB5900AEB4329BF3B22F0353554CB5* ____activeManifest_0;
};

// EventParamProperties
struct EventParamProperties_t05019706F891EA1BAF0BB69C8B1C245342D6D50F  : public RuntimeObject
{
	// UnityEngine.GameObject EventParamProperties::dataObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___dataObject_0;
	// System.String EventParamProperties::foodNmae
	String_t* ___foodNmae_1;
};

// LitJson.Extensions.Extensions
struct Extensions_tFC09F69054DCDDA5BCB44DE0C1773B9DD74D1C1D  : public RuntimeObject
{
};

// YooAsset.FileUtility
struct FileUtility_t5449A663405CBC365480B6DCAC03B177EA2C383B  : public RuntimeObject
{
};

// UnityEngine.TextCore.LowLevel.FontEngine
struct FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A  : public RuntimeObject
{
};

// FoodItem
struct FoodItem_tCA6A5E54C159F99EFA435A20E6779479F92712C2  : public RuntimeObject
{
	// System.String FoodItem::mainFoodName
	String_t* ___mainFoodName_0;
	// System.String[] FoodItem::otherFoodName
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___otherFoodName_1;
};

// FoodMenu
struct FoodMenu_tEAFBFBC26FF1204FF859BD39B4906A08FB567169  : public RuntimeObject
{
	// System.String FoodMenu::foodMenuID
	String_t* ___foodMenuID_0;
	// System.String FoodMenu::foodName
	String_t* ___foodName_1;
	// System.String FoodMenu::needMainFood
	String_t* ___needMainFood_2;
	// System.String[] FoodMenu::needOtherFood
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___needOtherFood_3;
	// System.String FoodMenu::classNum
	String_t* ___classNum_4;
};

// FoodMenuAsset
struct FoodMenuAsset_t4C00E06AE0EF2B9634E1875B98D90E24D0431CD3  : public RuntimeObject
{
	// System.String FoodMenuAsset::foodMunuName
	String_t* ___foodMunuName_0;
	// UnityEngine.Sprite FoodMenuAsset::foodMenuIcon
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___foodMenuIcon_1;
};

// LitJson.FsmContext
struct FsmContext_t039D467FB708DBF74426E9076FDA22760B0D30D1  : public RuntimeObject
{
	// System.Boolean LitJson.FsmContext::Return
	bool ___Return_0;
	// System.Int32 LitJson.FsmContext::NextState
	int32_t ___NextState_1;
	// LitJson.Lexer LitJson.FsmContext::L
	Lexer_t8EA7E771DBFBCCC71E85B400AF902FC3F482EBC9* ___L_2;
	// System.Int32 LitJson.FsmContext::StateStack
	int32_t ___StateStack_3;
};

// UnityEngine.GUIClip
struct GUIClip_t6049AB1B245065014011639ADCF204EE3668221B  : public RuntimeObject
{
};

// UnityEngine.GUIContent
struct GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2  : public RuntimeObject
{
	// System.String UnityEngine.GUIContent::m_Text
	String_t* ___m_Text_0;
	// UnityEngine.Texture UnityEngine.GUIContent::m_Image
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___m_Image_1;
	// System.String UnityEngine.GUIContent::m_Tooltip
	String_t* ___m_Tooltip_2;
};
// Native definition for P/Invoke marshalling of UnityEngine.GUIContent
struct GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2_marshaled_pinvoke
{
	char* ___m_Text_0;
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___m_Image_1;
	char* ___m_Tooltip_2;
};
// Native definition for COM marshalling of UnityEngine.GUIContent
struct GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2_marshaled_com
{
	Il2CppChar* ___m_Text_0;
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___m_Image_1;
	Il2CppChar* ___m_Tooltip_2;
};

// UnityEngine.GUILayout
struct GUILayout_tB26F0D6938B9B2AD04633B1DF56A1E52F3E6D177  : public RuntimeObject
{
};

// UnityEngine.GUILayoutOption
struct GUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14  : public RuntimeObject
{
	// UnityEngine.GUILayoutOption/Type UnityEngine.GUILayoutOption::type
	int32_t ___type_0;
	// System.Object UnityEngine.GUILayoutOption::value
	RuntimeObject* ___value_1;
};

// UnityEngine.GUIUtility
struct GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A  : public RuntimeObject
{
};

// GameQueryServices
struct GameQueryServices_tC8FF0FEB9480CFE5F968BA6B8FF938A4F0ECB842  : public RuntimeObject
{
};

// GameTaskData
struct GameTaskData_t9FC52812F4AA9149D5DEF13A4D84295D9C5084D0  : public RuntimeObject
{
	// System.String GameTaskData::taskID
	String_t* ___taskID_0;
	// System.String GameTaskData::taskName
	String_t* ___taskName_1;
	// System.String GameTaskData::taskDescrioble
	String_t* ___taskDescrioble_2;
	// System.Int32 GameTaskData::taskReward
	int32_t ___taskReward_3;
	// System.Int32 GameTaskData::taskMaxGrade
	int32_t ___taskMaxGrade_4;
};

// GuestData
struct GuestData_tA90EBFB7E86D53F6A8F38985DE74B5F9DC04A473  : public RuntimeObject
{
	// UnityEngine.GameObject GuestData::guestPrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___guestPrefab_0;
};

// GuestNeedFoodData
struct GuestNeedFoodData_t9129C6F34A34F0999113B706D151DDD20C290EDF  : public RuntimeObject
{
	// System.Int32 GuestNeedFoodData::maxNeddFood
	int32_t ___maxNeddFood_0;
	// System.Collections.Generic.List`1<System.String> GuestNeedFoodData::needFoodList
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___needFoodList_1;
};

// GuestTable
struct GuestTable_t773854D36ADB091AC6769C229D86C334DCE32D7D  : public RuntimeObject
{
	// System.String GuestTable::levelName
	String_t* ___levelName_0;
	// System.Collections.Generic.List`1<LevelGuestData> GuestTable::guestDataList
	List_1_t1A68216FCF6D253E3E40657E99C290E07D0413A2* ___guestDataList_1;
};

// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t299ECE61BBF4582B1F75734D43A96DDEC9B2004D  : public RuntimeObject
{
	// System.Boolean System.Security.Cryptography.HashAlgorithm::_disposed
	bool ____disposed_0;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::HashSizeValue
	int32_t ___HashSizeValue_1;
	// System.Byte[] System.Security.Cryptography.HashAlgorithm::HashValue
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___HashValue_2;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::State
	int32_t ___State_3;
};

// YooAsset.HashUtility
struct HashUtility_tC80280D7F13E8274BB51D195E5E00FC87C5AC83D  : public RuntimeObject
{
};

// HeadAssetData
struct HeadAssetData_tAB6CEFDAB592FB132D8A492EF5DE6324840FFB0B  : public RuntimeObject
{
	// System.String HeadAssetData::headID
	String_t* ___headID_0;
	// System.Single HeadAssetData::hedPrice
	float ___hedPrice_1;
	// UnityEngine.Sprite HeadAssetData::headImage
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___headImage_2;
};

// YooAsset.HostPlayModeImpl
struct HostPlayModeImpl_tF24BE63DEB824F68227C036C6E13D63717D7D468  : public RuntimeObject
{
	// YooAsset.PackageManifest YooAsset.HostPlayModeImpl::_activeManifest
	PackageManifest_tA9B8D10262EB5900AEB4329BF3B22F0353554CB5* ____activeManifest_0;
	// System.String YooAsset.HostPlayModeImpl::_packageName
	String_t* ____packageName_1;
	// YooAsset.IBuildinQueryServices YooAsset.HostPlayModeImpl::_buildinQueryServices
	RuntimeObject* ____buildinQueryServices_2;
	// YooAsset.IDeliveryQueryServices YooAsset.HostPlayModeImpl::_deliveryQueryServices
	RuntimeObject* ____deliveryQueryServices_3;
	// YooAsset.IRemoteServices YooAsset.HostPlayModeImpl::_remoteServices
	RuntimeObject* ____remoteServices_4;
};

// UnityEngine.Input
struct Input_t47D83E2A50E6AF7F8A47AA06FBEF9EBE6BBC22BB  : public RuntimeObject
{
};

// LitJson.JsonData
struct JsonData_t3C51D89A19D30A47A74617107D861959322307F1  : public RuntimeObject
{
	// System.Collections.Generic.IList`1<LitJson.JsonData> LitJson.JsonData::inst_array
	RuntimeObject* ___inst_array_0;
	// System.Boolean LitJson.JsonData::inst_boolean
	bool ___inst_boolean_1;
	// System.Double LitJson.JsonData::inst_double
	double ___inst_double_2;
	// System.Int32 LitJson.JsonData::inst_int
	int32_t ___inst_int_3;
	// System.Int64 LitJson.JsonData::inst_long
	int64_t ___inst_long_4;
	// System.Collections.Generic.IDictionary`2<System.String,LitJson.JsonData> LitJson.JsonData::inst_object
	RuntimeObject* ___inst_object_5;
	// System.String LitJson.JsonData::inst_string
	String_t* ___inst_string_6;
	// System.String LitJson.JsonData::json
	String_t* ___json_7;
	// LitJson.JsonType LitJson.JsonData::type
	int32_t ___type_8;
	// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>> LitJson.JsonData::object_list
	RuntimeObject* ___object_list_9;
};

// LitJson.JsonMapper
struct JsonMapper_t1D8418D75E295325E817EEBABCF549B289BDD24A  : public RuntimeObject
{
};

// LitJson.JsonMockWrapper
struct JsonMockWrapper_t1307E083A859AFEC66C78C143D33726957B3E8DB  : public RuntimeObject
{
};

// LitJson.JsonReader
struct JsonReader_t848C81F0C12BBE7E0135F28350F6635928FAAABD  : public RuntimeObject
{
	// System.Collections.Generic.Stack`1<System.Int32> LitJson.JsonReader::automaton_stack
	Stack_1_t3197E0F5EA36E611B259A88751D31FC2396FE4B6* ___automaton_stack_1;
	// System.Int32 LitJson.JsonReader::current_input
	int32_t ___current_input_2;
	// System.Int32 LitJson.JsonReader::current_symbol
	int32_t ___current_symbol_3;
	// System.Boolean LitJson.JsonReader::end_of_json
	bool ___end_of_json_4;
	// System.Boolean LitJson.JsonReader::end_of_input
	bool ___end_of_input_5;
	// LitJson.Lexer LitJson.JsonReader::lexer
	Lexer_t8EA7E771DBFBCCC71E85B400AF902FC3F482EBC9* ___lexer_6;
	// System.Boolean LitJson.JsonReader::parser_in_string
	bool ___parser_in_string_7;
	// System.Boolean LitJson.JsonReader::parser_return
	bool ___parser_return_8;
	// System.Boolean LitJson.JsonReader::read_started
	bool ___read_started_9;
	// System.IO.TextReader LitJson.JsonReader::reader
	TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7* ___reader_10;
	// System.Boolean LitJson.JsonReader::reader_is_owned
	bool ___reader_is_owned_11;
	// System.Boolean LitJson.JsonReader::skip_non_members
	bool ___skip_non_members_12;
	// System.Object LitJson.JsonReader::token_value
	RuntimeObject* ___token_value_13;
	// LitJson.JsonToken LitJson.JsonReader::token
	int32_t ___token_14;
};

// UnityEngine.JsonUtility
struct JsonUtility_t731013D97E03B7EDAE6186D6D6826A53B85F7197  : public RuntimeObject
{
};

// LitJson.JsonWriter
struct JsonWriter_t303427DE92107A0D92969763C95C5C20DB2D6F8F  : public RuntimeObject
{
	// LitJson.WriterContext LitJson.JsonWriter::context
	WriterContext_t5E6D8EFF41F41F382870C9F0452344B25B6E666F* ___context_1;
	// System.Collections.Generic.Stack`1<LitJson.WriterContext> LitJson.JsonWriter::ctx_stack
	Stack_1_t2CDC562A4A6FD1FE4DE0FC63A08FC106C6A84EFD* ___ctx_stack_2;
	// System.Boolean LitJson.JsonWriter::has_reached_end
	bool ___has_reached_end_3;
	// System.Char[] LitJson.JsonWriter::hex_seq
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___hex_seq_4;
	// System.Int32 LitJson.JsonWriter::indentation
	int32_t ___indentation_5;
	// System.Int32 LitJson.JsonWriter::indent_value
	int32_t ___indent_value_6;
	// System.Text.StringBuilder LitJson.JsonWriter::inst_string_builder
	StringBuilder_t* ___inst_string_builder_7;
	// System.Boolean LitJson.JsonWriter::pretty_print
	bool ___pretty_print_8;
	// System.Boolean LitJson.JsonWriter::validate
	bool ___validate_9;
	// System.Boolean LitJson.JsonWriter::lower_case_properties
	bool ___lower_case_properties_10;
	// System.IO.TextWriter LitJson.JsonWriter::writer
	TextWriter_tA9E5461506CF806E17B6BBBF2119359DEDA3F0F3* ___writer_11;
};

// LevelData
struct LevelData_tE6EAE053C277D3E4ED073363F0BF24BDBADFC4D5  : public RuntimeObject
{
	// System.String LevelData::levelID
	String_t* ___levelID_0;
	// System.String LevelData::levelGrade
	String_t* ___levelGrade_1;
	// System.String[] LevelData::mainFood
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___mainFood_2;
	// System.String[] LevelData::otherFood
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___otherFood_3;
	// System.String LevelData::drinke
	String_t* ___drinke_4;
	// System.String LevelData::levelResources
	String_t* ___levelResources_5;
	// System.String LevelData::levelGuestTablle
	String_t* ___levelGuestTablle_6;
	// System.Int32 LevelData::cionTarget
	int32_t ___cionTarget_7;
	// System.Int32 LevelData::gustCount
	int32_t ___gustCount_8;
};

// LevelGuestData
struct LevelGuestData_t02E2B626BF5CBDA7A4186527CF21191B18AF1F76  : public RuntimeObject
{
	// System.String LevelGuestData::guestID
	String_t* ___guestID_0;
	// System.String LevelGuestData::guestOrder
	String_t* ___guestOrder_1;
	// System.Single LevelGuestData::fromTime
	float ___fromTime_2;
	// System.String[] LevelGuestData::needFood
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___needFood_3;
};

// LitJson.Lexer
struct Lexer_t8EA7E771DBFBCCC71E85B400AF902FC3F482EBC9  : public RuntimeObject
{
	// System.Boolean LitJson.Lexer::allow_comments
	bool ___allow_comments_2;
	// System.Boolean LitJson.Lexer::allow_single_quoted_strings
	bool ___allow_single_quoted_strings_3;
	// System.Boolean LitJson.Lexer::end_of_input
	bool ___end_of_input_4;
	// LitJson.FsmContext LitJson.Lexer::fsm_context
	FsmContext_t039D467FB708DBF74426E9076FDA22760B0D30D1* ___fsm_context_5;
	// System.Int32 LitJson.Lexer::input_buffer
	int32_t ___input_buffer_6;
	// System.Int32 LitJson.Lexer::input_char
	int32_t ___input_char_7;
	// System.IO.TextReader LitJson.Lexer::reader
	TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7* ___reader_8;
	// System.Int32 LitJson.Lexer::state
	int32_t ___state_9;
	// System.Text.StringBuilder LitJson.Lexer::string_buffer
	StringBuilder_t* ___string_buffer_10;
	// System.String LitJson.Lexer::string_value
	String_t* ___string_value_11;
	// System.Int32 LitJson.Lexer::token
	int32_t ___token_12;
	// System.Int32 LitJson.Lexer::unichar
	int32_t ___unichar_13;
};

// LoadConfigursManager
struct LoadConfigursManager_tB9C80FC9048CAE6F0BD82463288DCA90FF0D3895  : public RuntimeObject
{
	// SystmConfigurationManager LoadConfigursManager::systmConfigurationManager
	SystmConfigurationManager_tC1C970C67458E76BA7D6A62A62A7B7DD818C1B89* ___systmConfigurationManager_0;
	// System.Collections.Generic.List`1<LevelData> LoadConfigursManager::levelDataList
	List_1_t5D51C2F548F7E4364A14BB81D41C0CAE0A900E0A* ___levelDataList_2;
	// System.Collections.Generic.Dictionary`2<System.String,OtherFoodItem> LoadConfigursManager::oetherFoodDictionary
	Dictionary_2_t6CB4CCB4E14D05AC8639EA8C504CF7F532EE3031* ___oetherFoodDictionary_3;
	// System.Collections.Generic.Dictionary`2<System.String,GuestTable> LoadConfigursManager::levelGuestDataDrictionary
	Dictionary_2_tC04ADB196C9D53B46FA973F626B7B8DD297EE170* ___levelGuestDataDrictionary_4;
	// System.Collections.Generic.Dictionary`2<System.String,FoodMenu> LoadConfigursManager::foodMendDicrionary
	Dictionary_2_t36BB2B8FA33F99D093898CA851F7F69E5932A791* ___foodMendDicrionary_5;
	// System.Collections.Generic.Dictionary`2<System.String,FoodItem> LoadConfigursManager::foodItemDictionary
	Dictionary_2_tF2DE9E763810A0184A1249A7A7DBFFDB56E12654* ___foodItemDictionary_6;
	// System.Collections.Generic.Dictionary`2<System.String,MainFoodItem> LoadConfigursManager::mainFoodItemDictionary
	Dictionary_2_t4B8935993E427E3606E2289FCEDC209328425484* ___mainFoodItemDictionary_7;
	// System.Collections.Generic.List`1<GameTaskData> LoadConfigursManager::taskList
	List_1_tACBAE0673F1BCE8E607A86B035A8608EF1BB7589* ___taskList_8;
	// TMPro.TextMeshProUGUI LoadConfigursManager::Lost_text
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___Lost_text_9;
};

// MainFoodCompnentAssets
struct MainFoodCompnentAssets_t87F293C0A1B943F716C3856FF73BA7D0CD16E97E  : public RuntimeObject
{
	// System.String MainFoodCompnentAssets::mainFoodNmae
	String_t* ___mainFoodNmae_0;
	// UnityEngine.GameObject MainFoodCompnentAssets::mainFoodPrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___mainFoodPrefab_1;
};

// MainFoodItem
struct MainFoodItem_t4298F65045B01EA50DB6B2DE0A9BE141C82C89FB  : public RuntimeObject
{
	// System.String MainFoodItem::mianFoodID
	String_t* ___mianFoodID_0;
	// System.String MainFoodItem::mainFoodName
	String_t* ___mainFoodName_1;
	// System.String MainFoodItem::onParent
	String_t* ___onParent_2;
	// System.Single MainFoodItem::mainFoodPrice
	float ___mainFoodPrice_3;
};

// UnityEngine.Yoga.MeasureOutput
struct MeasureOutput_t6C4FCF151309F81DF23561CF3FF1777445FBD84E  : public RuntimeObject
{
};

// UnityEngine.Yoga.Native
struct Native_t97ADC11284398663A27E9214C13A84F868A25614  : public RuntimeObject
{
};

// YooAsset.OfflinePlayModeImpl
struct OfflinePlayModeImpl_t9AC55276869B54227773FCA129929444BF8AAF1C  : public RuntimeObject
{
	// YooAsset.PackageManifest YooAsset.OfflinePlayModeImpl::_activeManifest
	PackageManifest_tA9B8D10262EB5900AEB4329BF3B22F0353554CB5* ____activeManifest_0;
};

// LitJson.OrderedDictionaryEnumerator
struct OrderedDictionaryEnumerator_t557577F1E3C3DC7E5A3BFA6B12F2E0F7921D9C18  : public RuntimeObject
{
	// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>> LitJson.OrderedDictionaryEnumerator::list_enumerator
	RuntimeObject* ___list_enumerator_0;
};

// OtheFoodCompnentAssets
struct OtheFoodCompnentAssets_t001F6F26B1B0C787CF334C742B74DE349CDAE2F9  : public RuntimeObject
{
	// System.String OtheFoodCompnentAssets::otherFoodName
	String_t* ___otherFoodName_0;
	// UnityEngine.GameObject OtheFoodCompnentAssets::otherFoodPrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___otherFoodPrefab_1;
};

// OtherFoodItem
struct OtherFoodItem_tD99CC1A3EC67BA2574653D701C8E00A03CCC667E  : public RuntimeObject
{
	// System.String OtherFoodItem::OtherFoodID
	String_t* ___OtherFoodID_0;
	// System.String OtherFoodItem::OtherFoodName
	String_t* ___OtherFoodName_1;
	// System.String OtherFoodItem::mainFood
	String_t* ___mainFood_2;
	// System.Single OtherFoodItem::otherFoodPrice
	float ___otherFoodPrice_3;
};

// YooAsset.PackageBundle
struct PackageBundle_t9B92F4FF721DB05C09F5CF9931F0E3FB837E8497  : public RuntimeObject
{
	// System.String YooAsset.PackageBundle::BundleName
	String_t* ___BundleName_0;
	// System.UInt32 YooAsset.PackageBundle::UnityCRC
	uint32_t ___UnityCRC_1;
	// System.String YooAsset.PackageBundle::FileHash
	String_t* ___FileHash_2;
	// System.String YooAsset.PackageBundle::FileCRC
	String_t* ___FileCRC_3;
	// System.Int64 YooAsset.PackageBundle::FileSize
	int64_t ___FileSize_4;
	// System.Boolean YooAsset.PackageBundle::IsRawFile
	bool ___IsRawFile_5;
	// System.Byte YooAsset.PackageBundle::LoadMethod
	uint8_t ___LoadMethod_6;
	// System.String[] YooAsset.PackageBundle::Tags
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___Tags_7;
	// System.Int32[] YooAsset.PackageBundle::ReferenceIDs
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___ReferenceIDs_8;
	// System.String YooAsset.PackageBundle::<PackageName>k__BackingField
	String_t* ___U3CPackageNameU3Ek__BackingField_9;
	// System.String YooAsset.PackageBundle::_cachedDataFilePath
	String_t* ____cachedDataFilePath_10;
	// System.String YooAsset.PackageBundle::_cachedInfoFilePath
	String_t* ____cachedInfoFilePath_11;
	// System.String YooAsset.PackageBundle::_tempDataFilePath
	String_t* ____tempDataFilePath_12;
	// System.String YooAsset.PackageBundle::_streamingFilePath
	String_t* ____streamingFilePath_13;
	// System.String YooAsset.PackageBundle::_fileName
	String_t* ____fileName_14;
	// System.String YooAsset.PackageBundle::_fileExtension
	String_t* ____fileExtension_15;
};

// YooAsset.PackageManifest
struct PackageManifest_tA9B8D10262EB5900AEB4329BF3B22F0353554CB5  : public RuntimeObject
{
	// System.String YooAsset.PackageManifest::FileVersion
	String_t* ___FileVersion_0;
	// System.Boolean YooAsset.PackageManifest::EnableAddressable
	bool ___EnableAddressable_1;
	// System.Boolean YooAsset.PackageManifest::LocationToLower
	bool ___LocationToLower_2;
	// System.Boolean YooAsset.PackageManifest::IncludeAssetGUID
	bool ___IncludeAssetGUID_3;
	// System.Int32 YooAsset.PackageManifest::OutputNameStyle
	int32_t ___OutputNameStyle_4;
	// System.String YooAsset.PackageManifest::PackageName
	String_t* ___PackageName_5;
	// System.String YooAsset.PackageManifest::PackageVersion
	String_t* ___PackageVersion_6;
	// System.Collections.Generic.List`1<YooAsset.PackageAsset> YooAsset.PackageManifest::AssetList
	List_1_t1F7449D98AADDC78A54D629F7AF999996F0A527E* ___AssetList_7;
	// System.Collections.Generic.List`1<YooAsset.PackageBundle> YooAsset.PackageManifest::BundleList
	List_1_t77C2174B5012E5EF8FE5C6F908B221D380B6853D* ___BundleList_8;
	// System.Collections.Generic.Dictionary`2<System.String,YooAsset.PackageBundle> YooAsset.PackageManifest::BundleDic
	Dictionary_2_t6EA1538A43358C8E44D5F0C1F2E23EFB0DCDEE6D* ___BundleDic_9;
	// System.Collections.Generic.Dictionary`2<System.String,YooAsset.PackageAsset> YooAsset.PackageManifest::AssetDic
	Dictionary_2_tD8ABFB42C548277409CBF9A8DD0E155EFFABDBEF* ___AssetDic_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> YooAsset.PackageManifest::AssetPathMapping1
	Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* ___AssetPathMapping1_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> YooAsset.PackageManifest::AssetPathMapping2
	Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* ___AssetPathMapping2_12;
	// System.Collections.Generic.HashSet`1<System.String> YooAsset.PackageManifest::CacheGUIDs
	HashSet_1_tEFC6605F7DE53F71946C33FD371E53C3100F2178* ___CacheGUIDs_13;
};

// YooAsset.PathUtility
struct PathUtility_t820434FFD3142C7DEBC33F8C6DA67F3C5068BBD7  : public RuntimeObject
{
};

// UnityEngine.Physics
struct Physics_t1244C2983AEAFA149425AFFC3DF53BC91C18ED56  : public RuntimeObject
{
};

// UnityEngine.Physics2D
struct Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D  : public RuntimeObject
{
};

// PlayerData
struct PlayerData_t04178AFFCFAF35DA6472B839034F672FFE5EB64A  : public RuntimeObject
{
	// System.Int32 PlayerData::getCion
	int32_t ___getCion_2;
	// System.Int32 PlayerData::currSelectLevel
	int32_t ___currSelectLevel_3;
};

// PlayerDataParameter
struct PlayerDataParameter_t99940E8382048E611BEA20091478EA46C9E604CA  : public RuntimeObject
{
	// System.String PlayerDataParameter::playerName
	String_t* ___playerName_0;
	// System.Int32 PlayerDataParameter::cions
	int32_t ___cions_1;
	// System.String PlayerDataParameter::strength
	String_t* ___strength_2;
	// System.Int32 PlayerDataParameter::diamond
	int32_t ___diamond_3;
	// System.Int32 PlayerDataParameter::maxLevel
	int32_t ___maxLevel_4;
	// System.Int32 PlayerDataParameter::onceGaceGuide
	int32_t ___onceGaceGuide_5;
	// System.String PlayerDataParameter::headID
	String_t* ___headID_6;
};

// RawFoodData
struct RawFoodData_t080FC4DCA16683EDC4F60AC44FA39210A029BEED  : public RuntimeObject
{
	// System.String RawFoodData::foodName
	String_t* ___foodName_0;
	// FoodMainType RawFoodData::isFoodMainType
	int32_t ___isFoodMainType_1;
	// UnityEngine.GameObject RawFoodData::rawFoodPrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___rawFoodPrefab_2;
	// System.Single RawFoodData::price
	float ___price_3;
};

// UnityEngine.RectTransformUtility
struct RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244  : public RuntimeObject
{
};

// UnityEngine.RemoteConfigSettingsHelper
struct RemoteConfigSettingsHelper_t29B2673892F8181388B45FFEEE354B3773629588  : public RuntimeObject
{
};

// RemoteServices
struct RemoteServices_t1C3C2F5B3D4D6F99D184D692B3EBAB379E94C935  : public RuntimeObject
{
	// System.String RemoteServices::_defaultHostServer
	String_t* ____defaultHostServer_0;
	// System.String RemoteServices::_fallbackHostServer
	String_t* ____fallbackHostServer_1;
};

// UnityEngine.RemoteSettings
struct RemoteSettings_t9DFFC747AB3E7A39DF4527F245B529A407427250  : public RuntimeObject
{
};

// YooAsset.ResourcePackage
struct ResourcePackage_t6B28B6B3A6DEAB641E6CBB06F383D7B947198022  : public RuntimeObject
{
	// System.Boolean YooAsset.ResourcePackage::_isInitialize
	bool ____isInitialize_0;
	// System.String YooAsset.ResourcePackage::_initializeError
	String_t* ____initializeError_1;
	// YooAsset.EOperationStatus YooAsset.ResourcePackage::_initializeStatus
	int32_t ____initializeStatus_2;
	// YooAsset.EPlayMode YooAsset.ResourcePackage::_playMode
	int32_t ____playMode_3;
	// YooAsset.IBundleServices YooAsset.ResourcePackage::_bundleServices
	RuntimeObject* ____bundleServices_4;
	// YooAsset.IPlayModeServices YooAsset.ResourcePackage::_playModeServices
	RuntimeObject* ____playModeServices_5;
	// YooAsset.AssetSystemImpl YooAsset.ResourcePackage::_assetSystemImpl
	AssetSystemImpl_t81D235BF692751145D6B2C3F3431BC7D8290BD38* ____assetSystemImpl_6;
	// System.String YooAsset.ResourcePackage::<PackageName>k__BackingField
	String_t* ___U3CPackageNameU3Ek__BackingField_7;
};

// RestaurantData
struct RestaurantData_t7D6C56717DA6F4BB4202FC873005799FA46C9F9F  : public RuntimeObject
{
	// System.String RestaurantData::hotelResoursceNmae
	String_t* ___hotelResoursceNmae_0;
	// System.Int32 RestaurantData::maxManking
	int32_t ___maxManking_1;
	// UnityEngine.GameObject RestaurantData::RestauranPrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___RestauranPrefab_2;
};

// YooAsset.SafeProxy
struct SafeProxy_tB77642C8A36A3783EB9DFAC1415459DE6B11CF7E  : public RuntimeObject
{
	// System.UInt32[] YooAsset.SafeProxy::_table
	UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA* ____table_1;
};

// UnityEngine.ScrollViewState
struct ScrollViewState_t004FCCBFB6795BD76582385D6D308D8F9ECF41B6  : public RuntimeObject
{
};

// ShoppItemGemData
struct ShoppItemGemData_tFCC886B3BBA5CCFC47016F64DAAC64494E643A01  : public RuntimeObject
{
	// System.String ShoppItemGemData::GemShoppName
	String_t* ___GemShoppName_0;
	// System.Int32 ShoppItemGemData::GemShopppPrice
	int32_t ___GemShopppPrice_1;
	// UnityEngine.Sprite ShoppItemGemData::GemIcon
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___GemIcon_2;
};

// ShoppItemPropData
struct ShoppItemPropData_tF139CF2D87625695C9FADCABCD28CCDACBF369F3  : public RuntimeObject
{
	// System.String ShoppItemPropData::propShoppName
	String_t* ___propShoppName_0;
	// System.Int32 ShoppItemPropData::propPrice
	int32_t ___propPrice_1;
	// UnityEngine.Sprite ShoppItemPropData::propIcon
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___propIcon_2;
};

// UnityEngine.SliderState
struct SliderState_t7BBFAEF918BAA1EE6116C3979993E4EC7DC54FC8  : public RuntimeObject
{
};

// StreamingAssetsDefine
struct StreamingAssetsDefine_tCBF2C51B50E4332F48F0455040BC89B0B32E1108  : public RuntimeObject
{
};

// StreamingAssetsHelper
struct StreamingAssetsHelper_tEBE3CCE7BF20D9F8E7B22F20F6DCFBB686158685  : public RuntimeObject
{
};

// YooAsset.StringUtility
struct StringUtility_tA17F666E86C899041F159E0C2A19E43DDE1A6686  : public RuntimeObject
{
};

// SystemDataFactoy
struct SystemDataFactoy_t0F295EDA05C547BBA48910AB76B4238A905BCFDB  : public RuntimeObject
{
	// SystmConfigurationManager SystemDataFactoy::systmConfigurationManager
	SystmConfigurationManager_tC1C970C67458E76BA7D6A62A62A7B7DD818C1B89* ___systmConfigurationManager_0;
	// System.String SystemDataFactoy::foldePath
	String_t* ___foldePath_2;
	// System.Collections.Generic.Dictionary`2<System.String,ResourceProfile> SystemDataFactoy::dictionaryConfiguirs
	Dictionary_2_t10507DB9B26DC4E3E709E064D33BECCADCACC8E3* ___dictionaryConfiguirs_3;
	// UnityEngine.AssetBundle SystemDataFactoy::assetBundleResource
	AssetBundle_tB38418819A49060CD738CB21541649340F082943* ___assetBundleResource_4;
};

// UnityEngine.UIElements.TextNative
struct TextNative_t463AA48470CE96DB270F55A6F73EF2D90401C00C  : public RuntimeObject
{
};

// UnityEngine.UIElements.UIElementsRuntimeUtilityNative
struct UIElementsRuntimeUtilityNative_t9DE2C23158D553BB693212D0D8AEAE8594E75938  : public RuntimeObject
{
};

// UnityEngine.UISystemProfilerApi
struct UISystemProfilerApi_t891AC4E16D3C12EAFD2748AE04F7A070F632396A  : public RuntimeObject
{
};

// Unity.FontABTool.UnityFontABTool
struct UnityFontABTool_t29C54DB12CF17A7849B7DA9EA1D87475ADA05FFA  : public RuntimeObject
{
};

// UnityEngine.UnityString
struct UnityString_tEB81DAFE75C642A9472D9FEDA7C2EC19A7B672B6  : public RuntimeObject
{
};

// LitJson.UnityTypeBindings
struct UnityTypeBindings_t8B29FCFD0144021BCAE930AB4077026676F186BA  : public RuntimeObject
{
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// UnityEngine.WWWForm
struct WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045  : public RuntimeObject
{
};

// UnityEngine.WWWTranscoder
struct WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B  : public RuntimeObject
{
};

// WeChatWASM.WXBase
struct WXBase_t6D33401602504B1D3E788C43C9B9DFFCD0B3DD23  : public RuntimeObject
{
};

// YooAsset.WebPlayModeImpl
struct WebPlayModeImpl_t6CCCA49CBDF02BC94394630444EBEA208669491E  : public RuntimeObject
{
	// YooAsset.PackageManifest YooAsset.WebPlayModeImpl::_activeManifest
	PackageManifest_tA9B8D10262EB5900AEB4329BF3B22F0353554CB5* ____activeManifest_0;
	// System.String YooAsset.WebPlayModeImpl::_packageName
	String_t* ____packageName_1;
	// YooAsset.IBuildinQueryServices YooAsset.WebPlayModeImpl::_buildinQueryServices
	RuntimeObject* ____buildinQueryServices_2;
	// YooAsset.IRemoteServices YooAsset.WebPlayModeImpl::_remoteServices
	RuntimeObject* ____remoteServices_3;
};

// UnityEngineInternal.WebRequestUtils
struct WebRequestUtils_t23F1FB533DBFDA3BE5624D901D535B4C6EFAD443  : public RuntimeObject
{
};

// LitJson.WriterContext
struct WriterContext_t5E6D8EFF41F41F382870C9F0452344B25B6E666F  : public RuntimeObject
{
	// System.Int32 LitJson.WriterContext::Count
	int32_t ___Count_0;
	// System.Boolean LitJson.WriterContext::InArray
	bool ___InArray_1;
	// System.Boolean LitJson.WriterContext::InObject
	bool ___InObject_2;
	// System.Boolean LitJson.WriterContext::ExpectingValue
	bool ___ExpectingValue_3;
	// System.Int32 LitJson.WriterContext::Padding
	int32_t ___Padding_4;
};

// System.Xml.XmlNode
struct XmlNode_t3180B9B3D5C36CD58F5327D9F13458E3B3F030AF  : public RuntimeObject
{
};

// System.Xml.XmlReader
struct XmlReader_t4C709DEF5F01606ECB60B638F1BD6F6E0A9116FD  : public RuntimeObject
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
};

// UnityEngine.Yoga.YogaConstants
struct YogaConstants_tE52AB48288567AEF285EDE0C8884AFD803AD9D3C  : public RuntimeObject
{
};

// YooAsset.YooAssetSettingsData
struct YooAssetSettingsData_tEDE002C0DE1A3DC02CCDB4E5C425CD65BF5D12D1  : public RuntimeObject
{
};

// YooAsset.YooAssets
struct YooAssets_tD00B5B9911F87CF8AC643076BF1ECB1F10DEBA56  : public RuntimeObject
{
};

// YooAsset.YooLogger
struct YooLogger_t94D44592A92415CB9ED28A4DAA5F52890FAD3CAF  : public RuntimeObject
{
};

// YooResoursceManagur
struct YooResoursceManagur_t41268F368AF5E66702DC6C69966EBCF528661E8B  : public RuntimeObject
{
	// YooAsset.ResourcePackage YooResoursceManagur::package
	ResourcePackage_t6B28B6B3A6DEAB641E6CBB06F383D7B947198022* ___package_1;
	// StartHomeManager YooResoursceManagur::startHomeManager
	StartHomeManager_tB9F9914830AFB48632C8B356435C243F2865B490* ___startHomeManager_2;
};

// System.__Il2CppComDelegate
struct __Il2CppComDelegate_tD0DD2BBA6AC8F151D32B6DFD02F6BDA339F8DC4D  : public Il2CppComObject
{
};

// CloudFunction/<>c
struct U3CU3Ec_tD31045D24431D327D10D187D599853D003138D58  : public RuntimeObject
{
};

// DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_t59F50543B6FEFF5A3083DDBD0014F90AF0F88A43  : public RuntimeObject
{
	// UnityEngine.AudioSource DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass0_0::target
	AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* ___target_0;
};

// DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass1_0
struct U3CU3Ec__DisplayClass1_0_tEAB34591A3D9422D47089AF3756769B92EA68A23  : public RuntimeObject
{
	// UnityEngine.AudioSource DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass1_0::target
	AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* ___target_0;
};

// DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t983A17B7EA42860742F49575E50045E560CC39B9  : public RuntimeObject
{
	// UnityEngine.Audio.AudioMixer DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass2_0::target
	AudioMixer_tE2E8D79241711CDF9AB428C7FB96A35D80E40B04* ___target_0;
	// System.String DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass2_0::floatName
	String_t* ___floatName_1;
};

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_t7A8E83B59527B3FEAE3B44C39ED05E6008A297C3  : public RuntimeObject
{
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass0_0::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0
struct U3CU3Ec__DisplayClass10_0_tB8A336F0E7DC9883C0E326A2DFE75AB01145E4BF  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::trans
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___trans_0;
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_1;
};

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass1_0
struct U3CU3Ec__DisplayClass1_0_t3A78B607DFF930B7D43BCC15854BA73BFD4287A8  : public RuntimeObject
{
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass1_0::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t8C1AE406019D344EE0395C065C3EC1A3154FEB7A  : public RuntimeObject
{
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass2_0::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_tD6D6E53B37AFAC161D55DEB2BCFA4F353A7932B9  : public RuntimeObject
{
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass3_0::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t9E669FCCA49892C3EB54A986B376D01225F5C32F  : public RuntimeObject
{
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass4_0::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_tCE2250763F6C0A955A4B7F95159E527AECDCCDCC  : public RuntimeObject
{
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass5_0::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_t838318FF7AD89F850DE26411D3961E68C2B8507A  : public RuntimeObject
{
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass7_0::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_t2C10088D8C53CEFD5E5012FBFC4B65019A1989EF  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass8_0::trans
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___trans_0;
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass8_0::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_1;
};

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0
struct U3CU3Ec__DisplayClass9_0_t249DDBCB23E8308D09D0B03AB7A0B2CCB4F854E3  : public RuntimeObject
{
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_tE3BC3F80330AED5085BA4E033B920916AE2EE75C  : public RuntimeObject
{
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass0_0::target
	Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass1_0
struct U3CU3Ec__DisplayClass1_0_t1193F41F38E742B3C5DC7DB01E9714978DDCBD01  : public RuntimeObject
{
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass1_0::target
	Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t449E13E54A99AFAE4984651C67700DEC993B98ED  : public RuntimeObject
{
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass2_0::target
	Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t892F716A8A1CDF8AFF37E5CA93703B60ADC4B9F3  : public RuntimeObject
{
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass3_0::target
	Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t5952AFF296950978CAEC258447E9176598BA17EB  : public RuntimeObject
{
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass5_0::target
	Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_tF7A18B8A6B9E46A956B38BDD1913A5AC7CCCD0E6  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass6_0::trans
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___trans_0;
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass6_0::target
	Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F* ___target_1;
};

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_tAD72DF3E41D581C965CFD52335B1FFAA457EAF96  : public RuntimeObject
{
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass7_0::target
	Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_t9C697E36D5D0AF152BCACC17C1D020869C34C0B8  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass8_0::trans
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___trans_0;
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass8_0::target
	Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F* ___target_1;
};

// DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_tCF08126DC44C5C3B34BACA4E22626734E523AA4E  : public RuntimeObject
{
	// UnityEngine.SpriteRenderer DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass0_0::target
	SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* ___target_0;
};

// DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass1_0
struct U3CU3Ec__DisplayClass1_0_tDB440B05588765695FD264B656234EFBF64155DA  : public RuntimeObject
{
	// UnityEngine.SpriteRenderer DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass1_0::target
	SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_tB18B0BBCEBB20AFFAEF4A5DC6E774BB71965CB5D  : public RuntimeObject
{
	// UnityEngine.CanvasGroup DG.Tweening.DOTweenModuleUI/<>c__DisplayClass0_0::target
	CanvasGroup_t048C1461B14628CFAEBE6E7353093ADB04EBC094* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass10_0
struct U3CU3Ec__DisplayClass10_0_tCC949A20CDD228AC8F8B3A8CD5753ADA411636B3  : public RuntimeObject
{
	// UnityEngine.UI.Outline DG.Tweening.DOTweenModuleUI/<>c__DisplayClass10_0::target
	Outline_t9CF146E077DC65F441EDEC463AA6710374108084* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass11_0
struct U3CU3Ec__DisplayClass11_0_t65D5FA900D6220B15D4F61001E4C0761C72A6796  : public RuntimeObject
{
	// UnityEngine.UI.Outline DG.Tweening.DOTweenModuleUI/<>c__DisplayClass11_0::target
	Outline_t9CF146E077DC65F441EDEC463AA6710374108084* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass12_0
struct U3CU3Ec__DisplayClass12_0_tCD751A3A64E63AA8CAAFAF76B7A47FB79314B30B  : public RuntimeObject
{
	// UnityEngine.UI.Outline DG.Tweening.DOTweenModuleUI/<>c__DisplayClass12_0::target
	Outline_t9CF146E077DC65F441EDEC463AA6710374108084* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass13_0
struct U3CU3Ec__DisplayClass13_0_tA7908A005DAFADE5E96F672B1A0AC21105DC4A84  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass13_0::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass14_0
struct U3CU3Ec__DisplayClass14_0_t14BD385BD495A01BD6C66D2D92360407933E64E9  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass14_0::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass15_0
struct U3CU3Ec__DisplayClass15_0_tD42752F57E0F92A701F49E8A350FDCE98CD5B0F8  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass15_0::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass16_0
struct U3CU3Ec__DisplayClass16_0_t19F6A4F2A285615B2EFC11553DF0D96149166E7A  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass16_0::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass17_0
struct U3CU3Ec__DisplayClass17_0_t284A2013A9CF39A877F17C22914B8003908BAF2A  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass17_0::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass18_0
struct U3CU3Ec__DisplayClass18_0_t8041ED4DE7248F565E86D82594DDD9BD74602121  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass18_0::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass19_0
struct U3CU3Ec__DisplayClass19_0_tE0338A49FF6F541CD74258CC0E361949F17279F8  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass19_0::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass1_0
struct U3CU3Ec__DisplayClass1_0_tD8156063570C9F9EDAFB40925044950112A26793  : public RuntimeObject
{
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI/<>c__DisplayClass1_0::target
	Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass20_0
struct U3CU3Ec__DisplayClass20_0_t984E360099B0FDF4B884478FC49D03EC7CC5FC62  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass20_0::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass21_0
struct U3CU3Ec__DisplayClass21_0_t103F15D006C0D1B8C5D8BF3FB799D36CD246036A  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass21_0::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass22_0
struct U3CU3Ec__DisplayClass22_0_t5C323042506D23A90B58A769162DBBF0BC76B035  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass22_0::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass23_0
struct U3CU3Ec__DisplayClass23_0_t3DFCA163008250F066061F9E3ED061A2BCFA666F  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass23_0::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass24_0
struct U3CU3Ec__DisplayClass24_0_t04F97A805A71B33C86A4F3A2F05635361548FD88  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass24_0::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass25_0
struct U3CU3Ec__DisplayClass25_0_t4DA808F4CFC3EA9D61AC01DF1B84F0601D03DABB  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass25_0::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass26_0
struct U3CU3Ec__DisplayClass26_0_t124EF2B4B70F54D478CDCCFB5BCD1975BB607407  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass26_0::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass27_0
struct U3CU3Ec__DisplayClass27_0_t5DF692DEA21E1F075A801B6B88FD7309CD5F86B7  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass27_0::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass28_0
struct U3CU3Ec__DisplayClass28_0_tD0958576900019D44F20769282634622310A6335  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass28_0::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t51A1D3D91AFC969F0DAF516721DDD2B73F01ED0D  : public RuntimeObject
{
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI/<>c__DisplayClass2_0::target
	Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass30_0
struct U3CU3Ec__DisplayClass30_0_tEC2889D7CADE796A4FFB321716EC8FE47DEEBC46  : public RuntimeObject
{
	// UnityEngine.UI.ScrollRect DG.Tweening.DOTweenModuleUI/<>c__DisplayClass30_0::target
	ScrollRect_t17D2F2939CA8953110180DF53164CFC3DC88D70E* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass31_0
struct U3CU3Ec__DisplayClass31_0_tA83E5335930C3B73B36584C0C49AEF4D60F994FA  : public RuntimeObject
{
	// UnityEngine.UI.ScrollRect DG.Tweening.DOTweenModuleUI/<>c__DisplayClass31_0::target
	ScrollRect_t17D2F2939CA8953110180DF53164CFC3DC88D70E* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass32_0
struct U3CU3Ec__DisplayClass32_0_tC0A04FC69DFA3D09F95256B3260E19CDF93CFFAB  : public RuntimeObject
{
	// UnityEngine.UI.ScrollRect DG.Tweening.DOTweenModuleUI/<>c__DisplayClass32_0::target
	ScrollRect_t17D2F2939CA8953110180DF53164CFC3DC88D70E* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass33_0
struct U3CU3Ec__DisplayClass33_0_t5E307A3D92BFAC7CC6F0D5285761865A1041D7A8  : public RuntimeObject
{
	// UnityEngine.UI.Slider DG.Tweening.DOTweenModuleUI/<>c__DisplayClass33_0::target
	Slider_t87EA570E3D6556CABF57456C2F3873FFD86E652F* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass34_0
struct U3CU3Ec__DisplayClass34_0_t18D864D495A67B8E3723FF168949A00B63EE2EC5  : public RuntimeObject
{
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/<>c__DisplayClass34_0::target
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0
struct U3CU3Ec__DisplayClass35_0_tB39D8ABA66B038B793C44B42BF240EA543CB3427  : public RuntimeObject
{
	// System.Int32 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::v
	int32_t ___v_0;
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::target
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___target_1;
	// System.Boolean DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::addThousandsSeparator
	bool ___addThousandsSeparator_2;
	// System.Globalization.CultureInfo DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::cInfo
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___cInfo_3;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0
struct U3CU3Ec__DisplayClass36_0_tAE460456A155A97552A76CF67AF3D3BE866F30C8  : public RuntimeObject
{
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::target
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass37_0
struct U3CU3Ec__DisplayClass37_0_tEB346CC740290347A0EAF5C4DBE0B8B55EC9B6D0  : public RuntimeObject
{
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/<>c__DisplayClass37_0::target
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t2C69FBF013ADB32163CC4F67A146C846E181C18B  : public RuntimeObject
{
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI/<>c__DisplayClass3_0::target
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass41_0
struct U3CU3Ec__DisplayClass41_0_t7022E6DD68301100D1A7392EF5170CF153EDAD93  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass41_0::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t8053081EDEC54E0DFD42D923A1A91BC995ACC579  : public RuntimeObject
{
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::target
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t97A2D1591F4437C9A3355B842244B21EEDFC7D3C  : public RuntimeObject
{
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI/<>c__DisplayClass5_0::target
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_t6825EE9800F145C3DC6520DEBF5CCD211D160E49  : public RuntimeObject
{
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI/<>c__DisplayClass7_0::target
	LayoutElement_tB1F24CC11AF4AA87015C8D8EE06D22349C5BF40A* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_t20FEDD5A66AA07E40BC3977804178A67426F6D3D  : public RuntimeObject
{
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI/<>c__DisplayClass8_0::target
	LayoutElement_tB1F24CC11AF4AA87015C8D8EE06D22349C5BF40A* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass9_0
struct U3CU3Ec__DisplayClass9_0_tE85C8192601740DA12B6A252D29726E9A7F06E5C  : public RuntimeObject
{
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI/<>c__DisplayClass9_0::target
	LayoutElement_tB1F24CC11AF4AA87015C8D8EE06D22349C5BF40A* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/Utils
struct Utils_t3635A7E5B7FAC073A571339320CB18B71E25ABD8  : public RuntimeObject
{
};

// DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_t841A32590F7E9B5CE337F1E7576F44ACC997C081  : public RuntimeObject
{
	// UnityEngine.Material DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_0;
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0::propertyID
	int32_t ___propertyID_1;
};

// DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0
struct U3CU3Ec__DisplayClass9_0_t67C62AE5CAF58BCB265D7E092190857BFBCCEC86  : public RuntimeObject
{
	// UnityEngine.Material DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_0;
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0::propertyID
	int32_t ___propertyID_1;
};

// DG.Tweening.DOTweenModuleUtils/Physics
struct Physics_t835AE7329FBFA61BA3A2271950FD3F7F6A18770A  : public RuntimeObject
{
};

// DrinkOpreat/<SartAddDrink>d__24
struct U3CSartAddDrinkU3Ed__24_t89B870583E664E392F9E3C8940FDF7DC2C31228D  : public RuntimeObject
{
	// System.Int32 DrinkOpreat/<SartAddDrink>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DrinkOpreat/<SartAddDrink>d__24::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// DrinkOpreat DrinkOpreat/<SartAddDrink>d__24::<>4__this
	DrinkOpreat_tE3BB22B195D920181FB3F2361BCDC241DEC2FF15* ___U3CU3E4__this_2;
};

// FireEffect/<BehindEffcet>d__13
struct U3CBehindEffcetU3Ed__13_tD205B745568DF7D32408E8EF906602C79E4378A0  : public RuntimeObject
{
	// System.Int32 FireEffect/<BehindEffcet>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object FireEffect/<BehindEffcet>d__13::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// FireEffect FireEffect/<BehindEffcet>d__13::<>4__this
	FireEffect_t3405E939E4E39C43487E73C9DA6799E6680567B8* ___U3CU3E4__this_2;
};

// FireEffect/<CreateFireEffect>d__12
struct U3CCreateFireEffectU3Ed__12_t5F5E278766D7A4483ADDCAA7C4A864D2F8CF9C0D  : public RuntimeObject
{
	// System.Int32 FireEffect/<CreateFireEffect>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object FireEffect/<CreateFireEffect>d__12::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// FireEffect FireEffect/<CreateFireEffect>d__12::<>4__this
	FireEffect_t3405E939E4E39C43487E73C9DA6799E6680567B8* ___U3CU3E4__this_2;
};

// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_tF844B2FAD6933B78FD5EFEBDE0529BCBAC19BA60  : public RuntimeObject
{
	// System.Int32 UnityEngine.GUILayoutUtility/LayoutCache::<id>k__BackingField
	int32_t ___U3CidU3Ek__BackingField_0;
	// UnityEngine.GUILayoutGroup UnityEngine.GUILayoutUtility/LayoutCache::topLevel
	GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D* ___topLevel_1;
	// UnityEngineInternal.GenericStack UnityEngine.GUILayoutUtility/LayoutCache::layoutGroups
	GenericStack_t1FB49AB7D847C97ABAA97AB232CA416CABD24C49* ___layoutGroups_2;
	// UnityEngine.GUILayoutGroup UnityEngine.GUILayoutUtility/LayoutCache::windows
	GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D* ___windows_3;
};

// Guest/<>c__DisplayClass31_0
struct U3CU3Ec__DisplayClass31_0_t1325CEF32530E3439EB4B791077F546321D876AB  : public RuntimeObject
{
	// Guest Guest/<>c__DisplayClass31_0::<>4__this
	Guest_t7C87006A91E9A72C586B6082DB0B7192FB76B4F3* ___U3CU3E4__this_0;
	// System.Int32 Guest/<>c__DisplayClass31_0::i
	int32_t ___i_1;
	// DG.Tweening.TweenCallback Guest/<>c__DisplayClass31_0::<>9__0
	TweenCallback_t7C8B8A38E7B30905FF1B83C943256EF23617BB24* ___U3CU3E9__0_2;
};

// Guest/<GuestCone>d__33
struct U3CGuestConeU3Ed__33_tB16FEEB33B2EFC605767E9465C2A3D4C7397FCF7  : public RuntimeObject
{
	// System.Int32 Guest/<GuestCone>d__33::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Guest/<GuestCone>d__33::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// Guest Guest/<GuestCone>d__33::<>4__this
	Guest_t7C87006A91E9A72C586B6082DB0B7192FB76B4F3* ___U3CU3E4__this_2;
	// UnityEngine.Transform Guest/<GuestCone>d__33::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_3;
};

// GuestManager/<ResetManager>d__11
struct U3CResetManagerU3Ed__11_tBA77C3A2638110F88B208A2BC73D89EEAA3E2E8F  : public RuntimeObject
{
	// System.Int32 GuestManager/<ResetManager>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GuestManager/<ResetManager>d__11::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// GuestManager GuestManager/<ResetManager>d__11::<>4__this
	GuestManager_t08BB44CBB90850DB991994981F09DF507D6E47E7* ___U3CU3E4__this_2;
};

// GuestManager/<StateGuestComeOne>d__20
struct U3CStateGuestComeOneU3Ed__20_t1F08BB7CE53B86ECF7CE47C6777F42A850612D73  : public RuntimeObject
{
	// System.Int32 GuestManager/<StateGuestComeOne>d__20::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GuestManager/<StateGuestComeOne>d__20::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// GuestManager GuestManager/<StateGuestComeOne>d__20::<>4__this
	GuestManager_t08BB44CBB90850DB991994981F09DF507D6E47E7* ___U3CU3E4__this_2;
	// System.Int32 GuestManager/<StateGuestComeOne>d__20::<i>5__2
	int32_t ___U3CiU3E5__2_3;
	// System.Single GuestManager/<StateGuestComeOne>d__20::<randTime>5__3
	float ___U3CrandTimeU3E5__3_4;
};

// LitJson.JsonMapper/<>c
struct U3CU3Ec_t908A12620392AFC75CA4D67D039F5FD576166F2D  : public RuntimeObject
{
};

// LevelManager/<RestManager>d__12
struct U3CRestManagerU3Ed__12_t00998E403C1D4F73CBE03A9F47CF790C0BA0A6C6  : public RuntimeObject
{
	// System.Int32 LevelManager/<RestManager>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LevelManager/<RestManager>d__12::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// LevelManager LevelManager/<RestManager>d__12::<>4__this
	LevelManager_t8405886BBC5A0ACBB1CC210E25D5DA1C72D16530* ___U3CU3E4__this_2;
};

// LoadConfigursManager/<Configuer>d__25
struct U3CConfiguerU3Ed__25_tFE845128D9108774B2F57AF8F3FD3D1E86B5FA2E  : public RuntimeObject
{
	// System.Int32 LoadConfigursManager/<Configuer>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadConfigursManager/<Configuer>d__25::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// LoadConfigursManager LoadConfigursManager/<Configuer>d__25::<>4__this
	LoadConfigursManager_tB9C80FC9048CAE6F0BD82463288DCA90FF0D3895* ___U3CU3E4__this_2;
	// SystmConfigurationManager LoadConfigursManager/<Configuer>d__25::systmConfigurationManager
	SystmConfigurationManager_tC1C970C67458E76BA7D6A62A62A7B7DD818C1B89* ___systmConfigurationManager_3;
};

// LoadConfigursManager/<LoadFoodItem>d__30
struct U3CLoadFoodItemU3Ed__30_t3A90454C579729453B97C3C26B9F222E83F69EBE  : public RuntimeObject
{
	// System.Int32 LoadConfigursManager/<LoadFoodItem>d__30::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadConfigursManager/<LoadFoodItem>d__30::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// LoadConfigursManager LoadConfigursManager/<LoadFoodItem>d__30::<>4__this
	LoadConfigursManager_tB9C80FC9048CAE6F0BD82463288DCA90FF0D3895* ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest LoadConfigursManager/<LoadFoodItem>d__30::<quest>5__2
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* ___U3CquestU3E5__2_3;
};

// LoadConfigursManager/<LoadFoodMenu>d__29
struct U3CLoadFoodMenuU3Ed__29_t6389133771D1E6B97C82374354E0A4478CD59F95  : public RuntimeObject
{
	// System.Int32 LoadConfigursManager/<LoadFoodMenu>d__29::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadConfigursManager/<LoadFoodMenu>d__29::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// LoadConfigursManager LoadConfigursManager/<LoadFoodMenu>d__29::<>4__this
	LoadConfigursManager_tB9C80FC9048CAE6F0BD82463288DCA90FF0D3895* ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest LoadConfigursManager/<LoadFoodMenu>d__29::<quest>5__2
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* ___U3CquestU3E5__2_3;
};

// LoadConfigursManager/<LoadGameData>d__26
struct U3CLoadGameDataU3Ed__26_t5E15D3EDB0A2FB2ADCA3A5B2085FD8E7D29B9F77  : public RuntimeObject
{
	// System.Int32 LoadConfigursManager/<LoadGameData>d__26::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadConfigursManager/<LoadGameData>d__26::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// LoadConfigursManager LoadConfigursManager/<LoadGameData>d__26::<>4__this
	LoadConfigursManager_tB9C80FC9048CAE6F0BD82463288DCA90FF0D3895* ___U3CU3E4__this_2;
};

// LoadConfigursManager/<LoadLevel>d__27
struct U3CLoadLevelU3Ed__27_t124598B6DC3C543629AB229B969A6EC331985E44  : public RuntimeObject
{
	// System.Int32 LoadConfigursManager/<LoadLevel>d__27::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadConfigursManager/<LoadLevel>d__27::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// LoadConfigursManager LoadConfigursManager/<LoadLevel>d__27::<>4__this
	LoadConfigursManager_tB9C80FC9048CAE6F0BD82463288DCA90FF0D3895* ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest LoadConfigursManager/<LoadLevel>d__27::<requst>5__2
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* ___U3CrequstU3E5__2_3;
};

// LoadConfigursManager/<LoadLlvelGuset>d__28
struct U3CLoadLlvelGusetU3Ed__28_tB662826A322C81A25CC394EB78B06A2779B13895  : public RuntimeObject
{
	// System.Int32 LoadConfigursManager/<LoadLlvelGuset>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadConfigursManager/<LoadLlvelGuset>d__28::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// LoadConfigursManager LoadConfigursManager/<LoadLlvelGuset>d__28::<>4__this
	LoadConfigursManager_tB9C80FC9048CAE6F0BD82463288DCA90FF0D3895* ___U3CU3E4__this_2;
	// System.Int32 LoadConfigursManager/<LoadLlvelGuset>d__28::<maxLevel>5__2
	int32_t ___U3CmaxLevelU3E5__2_3;
	// System.Int32 LoadConfigursManager/<LoadLlvelGuset>d__28::<i>5__3
	int32_t ___U3CiU3E5__3_4;
	// System.String LoadConfigursManager/<LoadLlvelGuset>d__28::<fileName>5__4
	String_t* ___U3CfileNameU3E5__4_5;
	// UnityEngine.Networking.UnityWebRequest LoadConfigursManager/<LoadLlvelGuset>d__28::<requst>5__5
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* ___U3CrequstU3E5__5_6;
};

// LoadConfigursManager/<LoadMainFoodItem>d__31
struct U3CLoadMainFoodItemU3Ed__31_tC45837233C9F61CE52C064BA91DAC05C90A33357  : public RuntimeObject
{
	// System.Int32 LoadConfigursManager/<LoadMainFoodItem>d__31::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadConfigursManager/<LoadMainFoodItem>d__31::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// LoadConfigursManager LoadConfigursManager/<LoadMainFoodItem>d__31::<>4__this
	LoadConfigursManager_tB9C80FC9048CAE6F0BD82463288DCA90FF0D3895* ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest LoadConfigursManager/<LoadMainFoodItem>d__31::<quest>5__2
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* ___U3CquestU3E5__2_3;
};

// LoadConfigursManager/<LoadOtherFoodItem>d__32
struct U3CLoadOtherFoodItemU3Ed__32_tEB7C145BABEE7292A53E27ED42CD146A3947935B  : public RuntimeObject
{
	// System.Int32 LoadConfigursManager/<LoadOtherFoodItem>d__32::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadConfigursManager/<LoadOtherFoodItem>d__32::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// LoadConfigursManager LoadConfigursManager/<LoadOtherFoodItem>d__32::<>4__this
	LoadConfigursManager_tB9C80FC9048CAE6F0BD82463288DCA90FF0D3895* ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest LoadConfigursManager/<LoadOtherFoodItem>d__32::<quest>5__2
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* ___U3CquestU3E5__2_3;
};

// LoadConfigursManager/<LoadTaskData>d__33
struct U3CLoadTaskDataU3Ed__33_tCA5898AFBBBB8C01BFDA62918550DDD61EB31D77  : public RuntimeObject
{
	// System.Int32 LoadConfigursManager/<LoadTaskData>d__33::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadConfigursManager/<LoadTaskData>d__33::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// LoadConfigursManager LoadConfigursManager/<LoadTaskData>d__33::<>4__this
	LoadConfigursManager_tB9C80FC9048CAE6F0BD82463288DCA90FF0D3895* ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest LoadConfigursManager/<LoadTaskData>d__33::<quest>5__2
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* ___U3CquestU3E5__2_3;
};

// OperationManage/<ResetManager>d__14
struct U3CResetManagerU3Ed__14_tC2768FCFC7A24A95BB1FC52C259EC838B3B91F9E  : public RuntimeObject
{
	// System.Int32 OperationManage/<ResetManager>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object OperationManage/<ResetManager>d__14::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// OperationManage OperationManage/<ResetManager>d__14::<>4__this
	OperationManage_tFF184A4A7A2B8B93259740624B38E08B84CB6FC0* ___U3CU3E4__this_2;
};

// OverPlanel/<CionScore>d__4
struct U3CCionScoreU3Ed__4_t9A1CB285821A6BBB62B51FB7D304471B81EBD6CB  : public RuntimeObject
{
	// System.Int32 OverPlanel/<CionScore>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object OverPlanel/<CionScore>d__4::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// OverPlanel OverPlanel/<CionScore>d__4::<>4__this
	OverPlanel_t3BD8295CC1BDBF33D0900B2F4059249914D6F9A0* ___U3CU3E4__this_2;
};

// PlayerData/<Read>d__9
struct U3CReadU3Ed__9_t4FED0EE05B8FCAB64F752801282B54A6979CEAB3  : public RuntimeObject
{
	// System.Int32 PlayerData/<Read>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PlayerData/<Read>d__9::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// UnityEngine.Networking.UnityWebRequest PlayerData/<Read>d__9::<request>5__2
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* ___U3CrequestU3E5__2_2;
};

// ShoppPlanel/<ShowItemGem>d__17
struct U3CShowItemGemU3Ed__17_tCE23953C6F6F7EBD2703E88D404B88BD9FADA89E  : public RuntimeObject
{
	// System.Int32 ShoppPlanel/<ShowItemGem>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ShoppPlanel/<ShowItemGem>d__17::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// ShoppPlanel ShoppPlanel/<ShowItemGem>d__17::<>4__this
	ShoppPlanel_t07BFA47DF40CE73299500CC5B2D0334F2F1353BF* ___U3CU3E4__this_2;
	// System.Int32 ShoppPlanel/<ShowItemGem>d__17::<childLengt>5__2
	int32_t ___U3CchildLengtU3E5__2_3;
	// System.Int32 ShoppPlanel/<ShowItemGem>d__17::<i>5__3
	int32_t ___U3CiU3E5__3_4;
};

// ShoppPlanel/<ShowItemProp>d__15
struct U3CShowItemPropU3Ed__15_tC74846BAC048DF4CDE72610F91EF6200C2D92B2D  : public RuntimeObject
{
	// System.Int32 ShoppPlanel/<ShowItemProp>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ShoppPlanel/<ShowItemProp>d__15::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// ShoppPlanel ShoppPlanel/<ShowItemProp>d__15::<>4__this
	ShoppPlanel_t07BFA47DF40CE73299500CC5B2D0334F2F1353BF* ___U3CU3E4__this_2;
	// System.Int32 ShoppPlanel/<ShowItemProp>d__15::<i>5__2
	int32_t ___U3CiU3E5__2_3;
};

// StartHomeManager/<LoadConfigur>d__6
struct U3CLoadConfigurU3Ed__6_tBBA8EA0059B963EF7054B688242391A5E7BC77ED  : public RuntimeObject
{
	// System.Int32 StartHomeManager/<LoadConfigur>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object StartHomeManager/<LoadConfigur>d__6::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// StartHomeManager StartHomeManager/<LoadConfigur>d__6::<>4__this
	StartHomeManager_tB9F9914830AFB48632C8B356435C243F2865B490* ___U3CU3E4__this_2;
};

// SystemDataFactoy/<Configur>d__8
struct U3CConfigurU3Ed__8_t5F945BAA2189F8822DD1E29409A89EC333884F2F  : public RuntimeObject
{
	// System.Int32 SystemDataFactoy/<Configur>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SystemDataFactoy/<Configur>d__8::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// SystemDataFactoy SystemDataFactoy/<Configur>d__8::<>4__this
	SystemDataFactoy_t0F295EDA05C547BBA48910AB76B4238A905BCFDB* ___U3CU3E4__this_2;
	// SystmConfigurationManager SystemDataFactoy/<Configur>d__8::systmConfigurationManager
	SystmConfigurationManager_tC1C970C67458E76BA7D6A62A62A7B7DD818C1B89* ___systmConfigurationManager_3;
};

// SystemDataFactoy/<LoadConfigurs>d__10
struct U3CLoadConfigursU3Ed__10_t17BD05602176250432B67280FD6455CF7EB07305  : public RuntimeObject
{
	// System.Int32 SystemDataFactoy/<LoadConfigurs>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SystemDataFactoy/<LoadConfigurs>d__10::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// SystemDataFactoy SystemDataFactoy/<LoadConfigurs>d__10::<>4__this
	SystemDataFactoy_t0F295EDA05C547BBA48910AB76B4238A905BCFDB* ___U3CU3E4__this_2;
};

// SystemGameManager/<LoadData>d__24
struct U3CLoadDataU3Ed__24_t6EFA0692B3238C766DC5A6517BD833394891B861  : public RuntimeObject
{
	// System.Int32 SystemGameManager/<LoadData>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SystemGameManager/<LoadData>d__24::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// SystemGameManager SystemGameManager/<LoadData>d__24::<>4__this
	SystemGameManager_tEBED10FE7ED1E6F2223FCAD4CB6FFFD68712293D* ___U3CU3E4__this_2;
};

// SystemGameManager/<LoadingLevel>d__26
struct U3CLoadingLevelU3Ed__26_t476BA2DF47915057102D79DD6CB80352000EEF6D  : public RuntimeObject
{
	// System.Int32 SystemGameManager/<LoadingLevel>d__26::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SystemGameManager/<LoadingLevel>d__26::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// SystemGameManager SystemGameManager/<LoadingLevel>d__26::<>4__this
	SystemGameManager_tEBED10FE7ED1E6F2223FCAD4CB6FFFD68712293D* ___U3CU3E4__this_2;
};

// SystemGameManager/<ResetManger>d__28
struct U3CResetMangerU3Ed__28_t1FE305717A3F1D2336122D8640D24E3B87FCBF73  : public RuntimeObject
{
	// System.Int32 SystemGameManager/<ResetManger>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SystemGameManager/<ResetManger>d__28::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// SystemGameManager SystemGameManager/<ResetManger>d__28::<>4__this
	SystemGameManager_tEBED10FE7ED1E6F2223FCAD4CB6FFFD68712293D* ___U3CU3E4__this_2;
};

// TargetPlanle/<CloseTarget>d__4
struct U3CCloseTargetU3Ed__4_t361763AA681089C44C1EE487F32C71C4B88A6104  : public RuntimeObject
{
	// System.Int32 TargetPlanle/<CloseTarget>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TargetPlanle/<CloseTarget>d__4::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// TargetPlanle TargetPlanle/<CloseTarget>d__4::<>4__this
	TargetPlanle_t6A8EBC369B27AE5A05189F09204256FCF600578E* ___U3CU3E4__this_2;
};

// TaskPlanel/<CreaAchieveTaks>d__6
struct U3CCreaAchieveTaksU3Ed__6_t09C47A859EEA6C654555154BE7B672B4E52180AC  : public RuntimeObject
{
	// System.Int32 TaskPlanel/<CreaAchieveTaks>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TaskPlanel/<CreaAchieveTaks>d__6::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// TaskPlanel TaskPlanel/<CreaAchieveTaks>d__6::<>4__this
	TaskPlanel_t885BA63C37C30C820EC675FCCE61B39C24BE96B6* ___U3CU3E4__this_2;
};

// UiManager/<TargetOpen>d__17
struct U3CTargetOpenU3Ed__17_t665AA827813FAA7F050E4C681F457C33196B2399  : public RuntimeObject
{
	// System.Int32 UiManager/<TargetOpen>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UiManager/<TargetOpen>d__17::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// UiManager UiManager/<TargetOpen>d__17::<>4__this
	UiManager_tD893C8D36318160D6F63738C3B33FE3C22D9688F* ___U3CU3E4__this_2;
};

// UiMapPlane/<InitData>d__8
struct U3CInitDataU3Ed__8_tA803BE6DD6574338ED9428EBFAEE0877E2FFB391  : public RuntimeObject
{
	// System.Int32 UiMapPlane/<InitData>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UiMapPlane/<InitData>d__8::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// UiMapPlane UiMapPlane/<InitData>d__8::<>4__this
	UiMapPlane_t14D8BC95B1C4C0407390DC6A79DD55B3315B61A3* ___U3CU3E4__this_2;
};

// LitJson.UnityTypeBindings/<>c
struct U3CU3Ec_t3AAEE6BBEC4C519EEB27C34FC45DFB4EB1584CE2  : public RuntimeObject
{
};

// LitJson.UnityTypeBindings/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t6F9528147EBA1501BA7DC4A28B00FEDC10940082  : public RuntimeObject
{
	// System.Action`2<UnityEngine.Vector2,LitJson.JsonWriter> LitJson.UnityTypeBindings/<>c__DisplayClass2_0::writeVector2
	Action_2_tE5C1059CE8ACEDAC4083A88C76E66DE3E15B8105* ___writeVector2_0;
	// System.Action`2<UnityEngine.Vector3,LitJson.JsonWriter> LitJson.UnityTypeBindings/<>c__DisplayClass2_0::writeVector3
	Action_2_t3CFBD6C56B087A7CE032945C0950C5716540158A* ___writeVector3_1;
};

// WeChatWASM.WXBase/<>c__DisplayClass74_0
struct U3CU3Ec__DisplayClass74_0_tD5EC8BD28644403EAA9A867BF36E91EE2B8D01D9  : public RuntimeObject
{
	// System.Action`1<UnityEngine.Font> WeChatWASM.WXBase/<>c__DisplayClass74_0::callback
	Action_1_tD91E4D0ED3C2E385D3BDD4B3EA48B5F99D39F1DC* ___callback_0;
};

// WXProfileStatsScript/ProfValue
struct ProfValue_t5AE6537628F2F452404571B15FF3E9EB015608BD  : public RuntimeObject
{
	// System.Single WXProfileStatsScript/ProfValue::current
	float ___current_0;
	// System.Single WXProfileStatsScript/ProfValue::max
	float ___max_1;
	// System.Single WXProfileStatsScript/ProfValue::min
	float ___min_2;
};

// WXTouchInputOverride/<>c
struct U3CU3Ec_tB5CF89FA9196447842235712836756FE02414173  : public RuntimeObject
{
};

// YooResoursceManagur/<InintCuble>d__6
struct U3CInintCubleU3Ed__6_t4C543808971F450CE5717919F31BADDC73D90E22  : public RuntimeObject
{
	// System.Int32 YooResoursceManagur/<InintCuble>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YooResoursceManagur/<InintCuble>d__6::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// YooResoursceManagur YooResoursceManagur/<InintCuble>d__6::<>4__this
	YooResoursceManagur_t41268F368AF5E66702DC6C69966EBCF528661E8B* ___U3CU3E4__this_2;
	// YooAsset.InitializationOperation YooResoursceManagur/<InintCuble>d__6::<initOperation>5__2
	InitializationOperation_t6B527F5DF87FBB08463CE61CDFBA82190F5CE66F* ___U3CinitOperationU3E5__2_3;
};

// System.Collections.Generic.List`1/Enumerator<RestaurantData>
struct Enumerator_tB0FE3237BD1DCDF4972626EA92AF452520944420 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_t6D46605B0341B96D804BAD55EB4985B0826CA52A* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	RestaurantData_t7D6C56717DA6F4BB4202FC873005799FA46C9F9F* ____current_3;
};

// Unity.Collections.NativeArray`1<System.Byte>
struct NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF 
{
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;
};

// UnityEngine.AnimatorClipInfo
struct AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03 
{
	// System.Int32 UnityEngine.AnimatorClipInfo::m_ClipInstanceID
	int32_t ___m_ClipInstanceID_0;
	// System.Single UnityEngine.AnimatorClipInfo::m_Weight
	float ___m_Weight_1;
};

// UnityEngine.AnimatorStateInfo
struct AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2 
{
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;
};

// UnityEngine.AnimatorTransitionInfo
struct AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD 
{
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_FullPath
	int32_t ___m_FullPath_0;
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_UserName
	int32_t ___m_UserName_1;
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_Name
	int32_t ___m_Name_2;
	// System.Boolean UnityEngine.AnimatorTransitionInfo::m_HasFixedDuration
	bool ___m_HasFixedDuration_3;
	// System.Single UnityEngine.AnimatorTransitionInfo::m_Duration
	float ___m_Duration_4;
	// System.Single UnityEngine.AnimatorTransitionInfo::m_NormalizedTime
	float ___m_NormalizedTime_5;
	// System.Boolean UnityEngine.AnimatorTransitionInfo::m_AnyState
	bool ___m_AnyState_6;
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_TransitionType
	int32_t ___m_TransitionType_7;
};
// Native definition for P/Invoke marshalling of UnityEngine.AnimatorTransitionInfo
struct AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD_marshaled_pinvoke
{
	int32_t ___m_FullPath_0;
	int32_t ___m_UserName_1;
	int32_t ___m_Name_2;
	int32_t ___m_HasFixedDuration_3;
	float ___m_Duration_4;
	float ___m_NormalizedTime_5;
	int32_t ___m_AnyState_6;
	int32_t ___m_TransitionType_7;
};
// Native definition for COM marshalling of UnityEngine.AnimatorTransitionInfo
struct AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD_marshaled_com
{
	int32_t ___m_FullPath_0;
	int32_t ___m_UserName_1;
	int32_t ___m_Name_2;
	int32_t ___m_HasFixedDuration_3;
	float ___m_Duration_4;
	float ___m_NormalizedTime_5;
	int32_t ___m_AnyState_6;
	int32_t ___m_TransitionType_7;
};

// LitJson.ArrayMetadata
struct ArrayMetadata_tACCCBD1B8815EDDC21759548BA3FD120086DAA7D 
{
	// System.Type LitJson.ArrayMetadata::element_type
	Type_t* ___element_type_0;
	// System.Boolean LitJson.ArrayMetadata::is_array
	bool ___is_array_1;
	// System.Boolean LitJson.ArrayMetadata::is_list
	bool ___is_list_2;
};
// Native definition for P/Invoke marshalling of LitJson.ArrayMetadata
struct ArrayMetadata_tACCCBD1B8815EDDC21759548BA3FD120086DAA7D_marshaled_pinvoke
{
	Type_t* ___element_type_0;
	int32_t ___is_array_1;
	int32_t ___is_list_2;
};
// Native definition for COM marshalling of LitJson.ArrayMetadata
struct ArrayMetadata_tACCCBD1B8815EDDC21759548BA3FD120086DAA7D_marshaled_com
{
	Type_t* ___element_type_0;
	int32_t ___is_array_1;
	int32_t ___is_list_2;
};

// UnityEngine.AssetFileNameExtensionAttribute
struct AssetFileNameExtensionAttribute_tEA86B663DC42BB5C4F9A2A081CD7D28845D9D056  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.AssetFileNameExtensionAttribute::<preferredExtension>k__BackingField
	String_t* ___U3CpreferredExtensionU3Ek__BackingField_0;
	// System.Collections.Generic.IEnumerable`1<System.String> UnityEngine.AssetFileNameExtensionAttribute::<otherExtensions>k__BackingField
	RuntimeObject* ___U3CotherExtensionsU3Ek__BackingField_1;
};

// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_tD5ABB3A2536319A3345B32A5481E37E23DD8CEDF 
{
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___m_defaultContextAction_1;
};
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_tD5ABB3A2536319A3345B32A5481E37E23DD8CEDF_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_tD5ABB3A2536319A3345B32A5481E37E23DD8CEDF_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

// YooAsset.CRC32Algorithm
struct CRC32Algorithm_t586EF2FEB80231B16D50813BE2ED0DC1DFD2014A  : public HashAlgorithm_t299ECE61BBF4582B1F75734D43A96DDEC9B2004D
{
	// System.UInt32 YooAsset.CRC32Algorithm::_currentCrc
	uint32_t ____currentCrc_4;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// UnityEngine.Color32
struct Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};
};

// System.Configuration.ConfigurationCollectionAttribute
struct ConfigurationCollectionAttribute_t1D7DBAAB4908B6B8F26EA1C66106A67BDE949558  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// System.Configuration.ConfigurationElementCollection
struct ConfigurationElementCollection_t56E8398661A85A59616301BADF13026FB1492606  : public ConfigurationElement_tAE3EE71C256825472831FFBB7F491275DFAF089E
{
};

// System.Configuration.ConfigurationSection
struct ConfigurationSection_t0BC609F0151B160A4FAB8226679B62AF22539C3E  : public ConfigurationElement_tAE3EE71C256825472831FFBB7F491275DFAF089E
{
};

// System.DateTime
struct DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D 
{
	// System.UInt64 System.DateTime::_dateData
	uint64_t ____dateData_46;
};

// YooAsset.DecryptFileInfo
struct DecryptFileInfo_tFE2E173F2F6F4CFC1FF46B133FD856B9EA382321 
{
	// System.String YooAsset.DecryptFileInfo::BundleName
	String_t* ___BundleName_0;
	// System.String YooAsset.DecryptFileInfo::FilePath
	String_t* ___FilePath_1;
};
// Native definition for P/Invoke marshalling of YooAsset.DecryptFileInfo
struct DecryptFileInfo_tFE2E173F2F6F4CFC1FF46B133FD856B9EA382321_marshaled_pinvoke
{
	char* ___BundleName_0;
	char* ___FilePath_1;
};
// Native definition for COM marshalling of YooAsset.DecryptFileInfo
struct DecryptFileInfo_tFE2E173F2F6F4CFC1FF46B133FD856B9EA382321_marshaled_com
{
	Il2CppChar* ___BundleName_0;
	Il2CppChar* ___FilePath_1;
};

// YooAsset.DeliveryFileInfo
struct DeliveryFileInfo_tBCAABBA12CED034E89403C0AEA3BF4D4EA823E7C 
{
	// System.String YooAsset.DeliveryFileInfo::DeliveryFilePath
	String_t* ___DeliveryFilePath_0;
	// System.UInt64 YooAsset.DeliveryFileInfo::DeliveryFileOffset
	uint64_t ___DeliveryFileOffset_1;
};
// Native definition for P/Invoke marshalling of YooAsset.DeliveryFileInfo
struct DeliveryFileInfo_tBCAABBA12CED034E89403C0AEA3BF4D4EA823E7C_marshaled_pinvoke
{
	char* ___DeliveryFilePath_0;
	uint64_t ___DeliveryFileOffset_1;
};
// Native definition for COM marshalling of YooAsset.DeliveryFileInfo
struct DeliveryFileInfo_tBCAABBA12CED034E89403C0AEA3BF4D4EA823E7C_marshaled_com
{
	Il2CppChar* ___DeliveryFilePath_0;
	uint64_t ___DeliveryFileOffset_1;
};

// UnityEngine.Animations.DiscreteEvaluationAttribute
struct DiscreteEvaluationAttribute_tF23FCB5AB01B394BF5BD84623364A965C90F8BB9  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// DotfuscatorAttribute
struct DotfuscatorAttribute_t90A6A71AB7691DF0BFCD2AC365104BF68425B9E7  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String DotfuscatorAttribute::a
	String_t* ___a_0;
	// System.Int32 DotfuscatorAttribute::c
	int32_t ___c_1;
};

// UnityEngine.UIElements.UIR.DrawBufferRange
struct DrawBufferRange_t684F255F5C954760B12F6689F84E78811040C7A4 
{
	// System.Int32 UnityEngine.UIElements.UIR.DrawBufferRange::firstIndex
	int32_t ___firstIndex_0;
	// System.Int32 UnityEngine.UIElements.UIR.DrawBufferRange::indexCount
	int32_t ___indexCount_1;
	// System.Int32 UnityEngine.UIElements.UIR.DrawBufferRange::minIndexVal
	int32_t ___minIndexVal_2;
	// System.Int32 UnityEngine.UIElements.UIR.DrawBufferRange::vertsReferenced
	int32_t ___vertsReferenced_3;
};

// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_t4F9AD0F906A21B3318CFA9466B076A467E16305F  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// YooAsset.EncryptFileInfo
struct EncryptFileInfo_tDAA38B9CEBCA459FF3F31DB5DA2B6103979FA18B 
{
	// System.String YooAsset.EncryptFileInfo::BundleName
	String_t* ___BundleName_0;
	// System.String YooAsset.EncryptFileInfo::FilePath
	String_t* ___FilePath_1;
};
// Native definition for P/Invoke marshalling of YooAsset.EncryptFileInfo
struct EncryptFileInfo_tDAA38B9CEBCA459FF3F31DB5DA2B6103979FA18B_marshaled_pinvoke
{
	char* ___BundleName_0;
	char* ___FilePath_1;
};
// Native definition for COM marshalling of YooAsset.EncryptFileInfo
struct EncryptFileInfo_tDAA38B9CEBCA459FF3F31DB5DA2B6103979FA18B_marshaled_com
{
	Il2CppChar* ___BundleName_0;
	Il2CppChar* ___FilePath_1;
};

// YooAsset.EncryptResult
struct EncryptResult_tF9D7E801F11C360586AA064C31197C0BAC9DB26D 
{
	// YooAsset.EBundleLoadMethod YooAsset.EncryptResult::LoadMethod
	int32_t ___LoadMethod_0;
	// System.Byte[] YooAsset.EncryptResult::EncryptedData
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___EncryptedData_1;
};
// Native definition for P/Invoke marshalling of YooAsset.EncryptResult
struct EncryptResult_tF9D7E801F11C360586AA064C31197C0BAC9DB26D_marshaled_pinvoke
{
	int32_t ___LoadMethod_0;
	Il2CppSafeArray/*NONE*/* ___EncryptedData_1;
};
// Native definition for COM marshalling of YooAsset.EncryptResult
struct EncryptResult_tF9D7E801F11C360586AA064C31197C0BAC9DB26D_marshaled_com
{
	int32_t ___LoadMethod_0;
	Il2CppSafeArray/*NONE*/* ___EncryptedData_1;
};

// EventAddDrink
struct EventAddDrink_t9B8F92DBFD9602EC76DDE5FE3D868A797DB01FFD  : public EventParamProperties_t05019706F891EA1BAF0BB69C8B1C245342D6D50F
{
	// System.String EventAddDrink::drinkName
	String_t* ___drinkName_2;
	// System.String EventAddDrink::drinkID
	String_t* ___drinkID_3;
	// System.Single EventAddDrink::drinkIDNum
	float ___drinkIDNum_4;
	// System.Int32 EventAddDrink::drinkPrice
	int32_t ___drinkPrice_5;
};

// EventAddOtherFood
struct EventAddOtherFood_t7F43F34FEB093FD2DA41695FEFC1CD8A22C784A4  : public EventParamProperties_t05019706F891EA1BAF0BB69C8B1C245342D6D50F
{
	// System.Single EventAddOtherFood::foodPrice
	float ___foodPrice_2;
};

// EventDeliverCookedFood
struct EventDeliverCookedFood_t879D541E341FFFFA2AB6729482C48BE3C7121AE9  : public EventParamProperties_t05019706F891EA1BAF0BB69C8B1C245342D6D50F
{
	// System.String EventDeliverCookedFood::foodID
	String_t* ___foodID_2;
	// System.Single EventDeliverCookedFood::foodIDNum
	float ___foodIDNum_3;
	// System.Int32 EventDeliverCookedFood::plateIndex
	int32_t ___plateIndex_4;
	// System.Single EventDeliverCookedFood::totalPrice
	float ___totalPrice_5;
};

// UnityEngine.EventInterests
struct EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8 
{
	// System.Boolean UnityEngine.EventInterests::<wantsMouseMove>k__BackingField
	bool ___U3CwantsMouseMoveU3Ek__BackingField_0;
	// System.Boolean UnityEngine.EventInterests::<wantsMouseEnterLeaveWindow>k__BackingField
	bool ___U3CwantsMouseEnterLeaveWindowU3Ek__BackingField_1;
	// System.Boolean UnityEngine.EventInterests::<wantsLessLayoutEvents>k__BackingField
	bool ___U3CwantsLessLayoutEventsU3Ek__BackingField_2;
};
// Native definition for P/Invoke marshalling of UnityEngine.EventInterests
struct EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8_marshaled_pinvoke
{
	int32_t ___U3CwantsMouseMoveU3Ek__BackingField_0;
	int32_t ___U3CwantsMouseEnterLeaveWindowU3Ek__BackingField_1;
	int32_t ___U3CwantsLessLayoutEventsU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.EventInterests
struct EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8_marshaled_com
{
	int32_t ___U3CwantsMouseMoveU3Ek__BackingField_0;
	int32_t ___U3CwantsMouseEnterLeaveWindowU3Ek__BackingField_1;
	int32_t ___U3CwantsLessLayoutEventsU3Ek__BackingField_2;
};

// UnityEngine.TextCore.FaceInfo
struct FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756 
{
	// System.Int32 UnityEngine.TextCore.FaceInfo::m_FaceIndex
	int32_t ___m_FaceIndex_0;
	// System.String UnityEngine.TextCore.FaceInfo::m_FamilyName
	String_t* ___m_FamilyName_1;
	// System.String UnityEngine.TextCore.FaceInfo::m_StyleName
	String_t* ___m_StyleName_2;
	// System.Int32 UnityEngine.TextCore.FaceInfo::m_PointSize
	int32_t ___m_PointSize_3;
	// System.Single UnityEngine.TextCore.FaceInfo::m_Scale
	float ___m_Scale_4;
	// System.Int32 UnityEngine.TextCore.FaceInfo::m_UnitsPerEM
	int32_t ___m_UnitsPerEM_5;
	// System.Single UnityEngine.TextCore.FaceInfo::m_LineHeight
	float ___m_LineHeight_6;
	// System.Single UnityEngine.TextCore.FaceInfo::m_AscentLine
	float ___m_AscentLine_7;
	// System.Single UnityEngine.TextCore.FaceInfo::m_CapLine
	float ___m_CapLine_8;
	// System.Single UnityEngine.TextCore.FaceInfo::m_MeanLine
	float ___m_MeanLine_9;
	// System.Single UnityEngine.TextCore.FaceInfo::m_Baseline
	float ___m_Baseline_10;
	// System.Single UnityEngine.TextCore.FaceInfo::m_DescentLine
	float ___m_DescentLine_11;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SuperscriptOffset
	float ___m_SuperscriptOffset_12;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SuperscriptSize
	float ___m_SuperscriptSize_13;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SubscriptOffset
	float ___m_SubscriptOffset_14;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SubscriptSize
	float ___m_SubscriptSize_15;
	// System.Single UnityEngine.TextCore.FaceInfo::m_UnderlineOffset
	float ___m_UnderlineOffset_16;
	// System.Single UnityEngine.TextCore.FaceInfo::m_UnderlineThickness
	float ___m_UnderlineThickness_17;
	// System.Single UnityEngine.TextCore.FaceInfo::m_StrikethroughOffset
	float ___m_StrikethroughOffset_18;
	// System.Single UnityEngine.TextCore.FaceInfo::m_StrikethroughThickness
	float ___m_StrikethroughThickness_19;
	// System.Single UnityEngine.TextCore.FaceInfo::m_TabWidth
	float ___m_TabWidth_20;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.FaceInfo
struct FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756_marshaled_pinvoke
{
	int32_t ___m_FaceIndex_0;
	char* ___m_FamilyName_1;
	char* ___m_StyleName_2;
	int32_t ___m_PointSize_3;
	float ___m_Scale_4;
	int32_t ___m_UnitsPerEM_5;
	float ___m_LineHeight_6;
	float ___m_AscentLine_7;
	float ___m_CapLine_8;
	float ___m_MeanLine_9;
	float ___m_Baseline_10;
	float ___m_DescentLine_11;
	float ___m_SuperscriptOffset_12;
	float ___m_SuperscriptSize_13;
	float ___m_SubscriptOffset_14;
	float ___m_SubscriptSize_15;
	float ___m_UnderlineOffset_16;
	float ___m_UnderlineThickness_17;
	float ___m_StrikethroughOffset_18;
	float ___m_StrikethroughThickness_19;
	float ___m_TabWidth_20;
};
// Native definition for COM marshalling of UnityEngine.TextCore.FaceInfo
struct FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756_marshaled_com
{
	int32_t ___m_FaceIndex_0;
	Il2CppChar* ___m_FamilyName_1;
	Il2CppChar* ___m_StyleName_2;
	int32_t ___m_PointSize_3;
	float ___m_Scale_4;
	int32_t ___m_UnitsPerEM_5;
	float ___m_LineHeight_6;
	float ___m_AscentLine_7;
	float ___m_CapLine_8;
	float ___m_MeanLine_9;
	float ___m_Baseline_10;
	float ___m_DescentLine_11;
	float ___m_SuperscriptOffset_12;
	float ___m_SuperscriptSize_13;
	float ___m_SubscriptOffset_14;
	float ___m_SubscriptSize_15;
	float ___m_UnderlineOffset_16;
	float ___m_UnderlineThickness_17;
	float ___m_StrikethroughOffset_18;
	float ___m_StrikethroughThickness_19;
	float ___m_TabWidth_20;
};

// UnityEngine.TextCore.LowLevel.FontEngineUtilities
struct FontEngineUtilities_t08D8707F6F929B42407961E303FD339A793E5BBB 
{
	union
	{
		struct
		{
		};
		uint8_t FontEngineUtilities_t08D8707F6F929B42407961E303FD339A793E5BBB__padding[1];
	};
};

// UnityEngine.TextCore.LowLevel.FontReference
struct FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172 
{
	// System.String UnityEngine.TextCore.LowLevel.FontReference::familyName
	String_t* ___familyName_0;
	// System.String UnityEngine.TextCore.LowLevel.FontReference::styleName
	String_t* ___styleName_1;
	// System.Int32 UnityEngine.TextCore.LowLevel.FontReference::faceIndex
	int32_t ___faceIndex_2;
	// System.String UnityEngine.TextCore.LowLevel.FontReference::filePath
	String_t* ___filePath_3;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.LowLevel.FontReference
struct FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172_marshaled_pinvoke
{
	char* ___familyName_0;
	char* ___styleName_1;
	int32_t ___faceIndex_2;
	char* ___filePath_3;
};
// Native definition for COM marshalling of UnityEngine.TextCore.LowLevel.FontReference
struct FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172_marshaled_com
{
	Il2CppChar* ___familyName_0;
	Il2CppChar* ___styleName_1;
	int32_t ___faceIndex_2;
	Il2CppChar* ___filePath_3;
};

// UnityEngine.GUITargetAttribute
struct GUITargetAttribute_t3F08CE7E00D79CB555B94AA7FA1BCB4B144B922B  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Int32 UnityEngine.GUITargetAttribute::displayMask
	int32_t ___displayMask_0;
};

// UnityEngine.TextCore.GlyphMetrics
struct GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A 
{
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_Width
	float ___m_Width_0;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_Height
	float ___m_Height_1;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_HorizontalBearingX
	float ___m_HorizontalBearingX_2;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_HorizontalBearingY
	float ___m_HorizontalBearingY_3;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_HorizontalAdvance
	float ___m_HorizontalAdvance_4;
};

// UnityEngine.TextCore.GlyphRect
struct GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D 
{
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_Y
	int32_t ___m_Y_1;
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_Width
	int32_t ___m_Width_2;
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_Height
	int32_t ___m_Height_3;
};

// UnityEngine.TextCore.LowLevel.GlyphValueRecord
struct GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E 
{
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_XPlacement
	float ___m_XPlacement_0;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_YPlacement
	float ___m_YPlacement_1;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_XAdvance
	float ___m_XAdvance_2;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_YAdvance
	float ___m_YAdvance_3;
};

// UnityEngine.Bindings.IgnoreAttribute
struct IgnoreAttribute_tAB3F6C4808BA16CD585D60A6353B3E0599DFCE4D  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Boolean UnityEngine.Bindings.IgnoreAttribute::<DoesNotContributeToSize>k__BackingField
	bool ___U3CDoesNotContributeToSizeU3Ek__BackingField_0;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.Int64
struct Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3 
{
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_t537BA5EEEDB652AA60A00E4CEA6B73710FBDD806  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// LitJson.Extensions.JsonIgnore
struct JsonIgnore_t3FF65951D3F8D995E69E2E396F84F51967463103  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.LayerMask
struct LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB 
{
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;
};

// UnityEngine.Bindings.NativeAsStructAttribute
struct NativeAsStructAttribute_t48549F0E2D38CC0251B7BF2780E434EA141DF2D8  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.NativeClassAttribute
struct NativeClassAttribute_t774C48B9F745C9B0FD2FA82F9B42D4A18E162FA7  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.NativeClassAttribute::<QualifiedNativeName>k__BackingField
	String_t* ___U3CQualifiedNativeNameU3Ek__BackingField_0;
	// System.String UnityEngine.NativeClassAttribute::<Declaration>k__BackingField
	String_t* ___U3CDeclarationU3Ek__BackingField_1;
};

// UnityEngine.Bindings.NativeConditionalAttribute
struct NativeConditionalAttribute_tB722B3ED350E82853F8CEFF672A6CDC4B6B362CA  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeConditionalAttribute::<Condition>k__BackingField
	String_t* ___U3CConditionU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Bindings.NativeConditionalAttribute::<Enabled>k__BackingField
	bool ___U3CEnabledU3Ek__BackingField_1;
};

// UnityEngine.Bindings.NativeHeaderAttribute
struct NativeHeaderAttribute_t35DDAA41C31EEE4C94D2586F33D3EB26C0EA6F51  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeHeaderAttribute::<Header>k__BackingField
	String_t* ___U3CHeaderU3Ek__BackingField_0;
};

// UnityEngine.Bindings.NativeMethodAttribute
struct NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeMethodAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Bindings.NativeMethodAttribute::<IsThreadSafe>k__BackingField
	bool ___U3CIsThreadSafeU3Ek__BackingField_1;
	// System.Boolean UnityEngine.Bindings.NativeMethodAttribute::<IsFreeFunction>k__BackingField
	bool ___U3CIsFreeFunctionU3Ek__BackingField_2;
	// System.Boolean UnityEngine.Bindings.NativeMethodAttribute::<ThrowsException>k__BackingField
	bool ___U3CThrowsExceptionU3Ek__BackingField_3;
	// System.Boolean UnityEngine.Bindings.NativeMethodAttribute::<HasExplicitThis>k__BackingField
	bool ___U3CHasExplicitThisU3Ek__BackingField_4;
};

// UnityEngine.Bindings.NativeNameAttribute
struct NativeNameAttribute_t222751782B5418807DFE2A88CA0B24CA691B8621  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeNameAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
};

// UnityEngine.Bindings.NativeThrowsAttribute
struct NativeThrowsAttribute_t211CE8D047A8D45676C9ED399D5AA3B4A2C3E625  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Boolean UnityEngine.Bindings.NativeThrowsAttribute::<ThrowsException>k__BackingField
	bool ___U3CThrowsExceptionU3Ek__BackingField_0;
};

// UnityEngine.Bindings.NativeTypeAttribute
struct NativeTypeAttribute_tB60F1675F1F20B6CB1B871FDDD067D672F75B8D1  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeTypeAttribute::<Header>k__BackingField
	String_t* ___U3CHeaderU3Ek__BackingField_0;
	// System.String UnityEngine.Bindings.NativeTypeAttribute::<IntermediateScriptingStructName>k__BackingField
	String_t* ___U3CIntermediateScriptingStructNameU3Ek__BackingField_1;
	// UnityEngine.Bindings.CodegenOptions UnityEngine.Bindings.NativeTypeAttribute::<CodegenOptions>k__BackingField
	int32_t ___U3CCodegenOptionsU3Ek__BackingField_2;
};

// UnityEngine.Animations.NotKeyableAttribute
struct NotKeyableAttribute_tDDB6B25B26F649E3CED893EE1E63B6DE66844483  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.Bindings.NotNullAttribute
struct NotNullAttribute_t2E29B7802E8ED55CEA04EC4A6C254C6B60272DF7  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NotNullAttribute::<Exception>k__BackingField
	String_t* ___U3CExceptionU3Ek__BackingField_0;
};

// LitJson.ObjectMetadata
struct ObjectMetadata_t02B03E2889142EBF02504FC09D146A753E9A1661 
{
	// System.Type LitJson.ObjectMetadata::element_type
	Type_t* ___element_type_0;
	// System.Boolean LitJson.ObjectMetadata::is_dictionary
	bool ___is_dictionary_1;
	// System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata> LitJson.ObjectMetadata::properties
	RuntimeObject* ___properties_2;
};
// Native definition for P/Invoke marshalling of LitJson.ObjectMetadata
struct ObjectMetadata_t02B03E2889142EBF02504FC09D146A753E9A1661_marshaled_pinvoke
{
	Type_t* ___element_type_0;
	int32_t ___is_dictionary_1;
	RuntimeObject* ___properties_2;
};
// Native definition for COM marshalling of LitJson.ObjectMetadata
struct ObjectMetadata_t02B03E2889142EBF02504FC09D146A753E9A1661_marshaled_com
{
	Type_t* ___element_type_0;
	int32_t ___is_dictionary_1;
	RuntimeObject* ___properties_2;
};

// UnityEngine.PhysicsScene
struct PhysicsScene_t55222DD37072E8560EE054A07C0E3FE391D9D9DE 
{
	// System.Int32 UnityEngine.PhysicsScene::m_Handle
	int32_t ___m_Handle_0;
};

// UnityEngine.PhysicsScene2D
struct PhysicsScene2D_t550D023B9E77BE6844564BB4F9FA291EEA10FDC9 
{
	// System.Int32 UnityEngine.PhysicsScene2D::m_Handle
	int32_t ___m_Handle_0;
};

// UnityEngine.Bindings.PreventReadOnlyInstanceModificationAttribute
struct PreventReadOnlyInstanceModificationAttribute_t7FBCFCBA855C80F9E87486C8A6B4DDBA47B78415  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// Unity.Profiling.ProfilerRecorder
struct ProfilerRecorder_t363D18B531351FF6D7A09072564EB5D8FC60E613 
{
	// System.UInt64 Unity.Profiling.ProfilerRecorder::handle
	uint64_t ___handle_0;
};

// LitJson.PropertyMetadata
struct PropertyMetadata_t39488BB7A97631357ACA1E4409FA30D92DC4564D 
{
	// System.Reflection.MemberInfo LitJson.PropertyMetadata::Info
	MemberInfo_t* ___Info_0;
	// System.Boolean LitJson.PropertyMetadata::IsField
	bool ___IsField_1;
	// System.Type LitJson.PropertyMetadata::Type
	Type_t* ___Type_2;
};
// Native definition for P/Invoke marshalling of LitJson.PropertyMetadata
struct PropertyMetadata_t39488BB7A97631357ACA1E4409FA30D92DC4564D_marshaled_pinvoke
{
	MemberInfo_t* ___Info_0;
	int32_t ___IsField_1;
	Type_t* ___Type_2;
};
// Native definition for COM marshalling of LitJson.PropertyMetadata
struct PropertyMetadata_t39488BB7A97631357ACA1E4409FA30D92DC4564D_marshaled_com
{
	MemberInfo_t* ___Info_0;
	int32_t ___IsField_1;
	Type_t* ___Type_2;
};

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;
};

// UnityEngine.Rect
struct Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D 
{
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;
};

// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct RequiredByNativeCodeAttribute_t86B11F2BA12BB463CE3258E64E16B43484014FCA  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Scripting.RequiredByNativeCodeAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Scripting.RequiredByNativeCodeAttribute::<Optional>k__BackingField
	bool ___U3COptionalU3Ek__BackingField_1;
	// System.Boolean UnityEngine.Scripting.RequiredByNativeCodeAttribute::<GenerateProxy>k__BackingField
	bool ___U3CGenerateProxyU3Ek__BackingField_2;
};

// UnityEngine.SharedBetweenAnimatorsAttribute
struct SharedBetweenAnimatorsAttribute_t44FFD5D3B5AEBB394182D66E2198FA398087449C  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// UnityEngine.Bindings.StaticAccessorAttribute
struct StaticAccessorAttribute_tDE194716AED7A414D473DC570B2E0035A5CE130A  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.StaticAccessorAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// UnityEngine.Bindings.StaticAccessorType UnityEngine.Bindings.StaticAccessorAttribute::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_1;
};

// System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t9B661AC8C2EFA6BAB94C77BB24A5DDA82D61F833 
{
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.TaskAwaiter::m_task
	Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572* ___m_task_0;
};
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t9B661AC8C2EFA6BAB94C77BB24A5DDA82D61F833_marshaled_pinvoke
{
	Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572* ___m_task_0;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t9B661AC8C2EFA6BAB94C77BB24A5DDA82D61F833_marshaled_com
{
	Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572* ___m_task_0;
};

// UnityEngine.ThreadAndSerializationSafeAttribute
struct ThreadAndSerializationSafeAttribute_t819C12E8106F42E7493B11DDA93C36F6FB864357  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.UILineInfo
struct UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC 
{
	// System.Int32 UnityEngine.UILineInfo::startCharIdx
	int32_t ___startCharIdx_0;
	// System.Int32 UnityEngine.UILineInfo::height
	int32_t ___height_1;
	// System.Single UnityEngine.UILineInfo::topY
	float ___topY_2;
	// System.Single UnityEngine.UILineInfo::leading
	float ___leading_3;
};

// System.UInt32
struct UInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B 
{
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;
};

// System.UIntPtr
struct UIntPtr_t 
{
	// System.Void* System.UIntPtr::_pointer
	void* ____pointer_1;
};

// UnityEngine.UnityEngineModuleAssembly
struct UnityEngineModuleAssembly_tB6587DA5BA2569921894019758C4D69095012710  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.Bindings.UnmarshalledAttribute
struct UnmarshalledAttribute_t3D645C3393EF99EED2893026413D4F5B489CD13B  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct UsedByNativeCodeAttribute_t3FE9A7CDCC6A3A4122D8BF44F1D0A37BB38894C1  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

// UnityEngine.Bindings.VisibleToOtherModulesAttribute
struct VisibleToOtherModulesAttribute_tE7803AC6A0462A18B7EEF17C4A1036DEE993B489  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// WeChatWASM.WX
struct WX_t60FFD778F96FE09EE88AE242EA3EFB9AC865BA05  : public WXBase_t6D33401602504B1D3E788C43C9B9DFFCD0B3DD23
{
};

// UnityEngine.WritableAttribute
struct WritableAttribute_t7D85DADDFD6751C94E2E9594E562AD281A3B6E7B  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.Yoga.YogaSize
struct YogaSize_tA276812CB1E90E7AA2028A9474EA6EA46B3B38EA 
{
	// System.Single UnityEngine.Yoga.YogaSize::width
	float ___width_0;
	// System.Single UnityEngine.Yoga.YogaSize::height
	float ___height_1;
};

// UnityEngine.Yoga.YogaValue
struct YogaValue_t9066126971BFC18D9B4A8AB11435557F19598F8C 
{
	// System.Single UnityEngine.Yoga.YogaValue::value
	float ___value_0;
	// UnityEngine.Yoga.YogaUnit UnityEngine.Yoga.YogaValue::unit
	int32_t ___unit_1;
};

// a
struct a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C 
{
	// System.UInt32 a::a
	uint32_t ___a_0;
	// System.UInt32 a::b
	uint32_t ___b_1;
	// System.UInt32 a::c
	uint32_t ___c_2;
	// System.UInt32 a::d
	uint32_t ___d_3;
};

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=112
struct __StaticArrayInitTypeSizeU3D112_t3634DC8000B09BC9B0CCFCCA5B7FF92AE5FB2971 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D112_t3634DC8000B09BC9B0CCFCCA5B7FF92AE5FB2971__padding[112];
	};
};

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12
struct __StaticArrayInitTypeSizeU3D12_t2075943EB4AAA12F6AE68BBCD8EFE67B74570C3B 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t2075943EB4AAA12F6AE68BBCD8EFE67B74570C3B__padding[12];
	};
};

// DG.Tweening.DOTweenCYInstruction/WaitForCompletion
struct WaitForCompletion_tC84400E0FA4E28B95AA56DF28973D5FFDA16AFA0  : public CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617
{
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForCompletion::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_0;
};

// DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops
struct WaitForElapsedLoops_t24C0691B408798B4BE5CCC92DC8B4D40430717BC  : public CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617
{
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_0;
	// System.Int32 DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops::elapsedLoops
	int32_t ___elapsedLoops_1;
};

// DG.Tweening.DOTweenCYInstruction/WaitForKill
struct WaitForKill_t532BDFE32D7C3892E01BF80054F95A9A5C1C24DE  : public CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617
{
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForKill::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_0;
};

// DG.Tweening.DOTweenCYInstruction/WaitForPosition
struct WaitForPosition_t302B4F4C6FC89426E08DDC65543F45785B20B84B  : public CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617
{
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForPosition::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_0;
	// System.Single DG.Tweening.DOTweenCYInstruction/WaitForPosition::position
	float ___position_1;
};

// DG.Tweening.DOTweenCYInstruction/WaitForRewind
struct WaitForRewind_t2ABB006386A81D361C36B476044786442726743D  : public CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617
{
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForRewind::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_0;
};

// DG.Tweening.DOTweenCYInstruction/WaitForStart
struct WaitForStart_t4448F8E46F59EE599CA8DCEF52FC706221093F30  : public CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617
{
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForStart::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_0;
};

// UnityEngine.GUIClip/ParentClipScope
struct ParentClipScope_tDAB1300C623213518730D926A970098BECFD9C52 
{
	// System.Boolean UnityEngine.GUIClip/ParentClipScope::m_Disposed
	bool ___m_Disposed_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.GUIClip/ParentClipScope
struct ParentClipScope_tDAB1300C623213518730D926A970098BECFD9C52_marshaled_pinvoke
{
	int32_t ___m_Disposed_0;
};
// Native definition for COM marshalling of UnityEngine.GUIClip/ParentClipScope
struct ParentClipScope_tDAB1300C623213518730D926A970098BECFD9C52_marshaled_com
{
	int32_t ___m_Disposed_0;
};

// UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t34AF939575E1C059D581AB7ED8F039BCFFC70314 
{
	// UnityEngine.GameObject UnityEngine.SendMouseEvents/HitInfo::target
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___target_0;
	// UnityEngine.Camera UnityEngine.SendMouseEvents/HitInfo::camera
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___camera_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t34AF939575E1C059D581AB7ED8F039BCFFC70314_marshaled_pinvoke
{
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___target_0;
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___camera_1;
};
// Native definition for COM marshalling of UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t34AF939575E1C059D581AB7ED8F039BCFFC70314_marshaled_com
{
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___target_0;
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___camera_1;
};

// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter
struct YieldAwaiter_t5F0A81DC85227C01FFC38D53139B5C19D920B52A 
{
	union
	{
		struct
		{
		};
		uint8_t YieldAwaiter_t5F0A81DC85227C01FFC38D53139B5C19D920B52A__padding[1];
	};
};

// b/a
struct a_tC1CB14265A54FD08AC6F722A13BAD61141D148AF 
{
	// System.UInt32 b/a::a
	uint32_t ___a_0;
	// System.UInt32 b/a::b
	uint32_t ___b_1;
	// System.UInt32 b/a::c
	uint32_t ___c_2;
	// System.UInt32 b/a::d
	uint32_t ___d_3;
	// System.Byte b/a::e
	uint8_t ___e_4;
};

// b/b
struct b_tFB66B901E947622DFAF3E315886F1089C51D6893 
{
	// System.UInt64 b/b::a
	uint64_t ___a_0;
	// System.UInt32 b/b::b
	uint32_t ___b_1;
	// System.UInt32 b/b::c
	uint32_t ___c_2;
	// System.UInt64 b/b::d
	uint64_t ___d_3;
	// System.UInt64 b/b::e
	uint64_t ___e_4;
	// System.UInt64 b/b::f
	uint64_t ___f_5;
	// System.Byte b/b::g
	uint8_t ___g_6;
};

// b/d
struct d_t2D0D74CB19250389E57D70E733F007725D5AA1F2 
{
	// System.UInt64 b/d::a
	uint64_t ___a_0;
	// System.UInt32 b/d::b
	uint32_t ___b_1;
	// System.UInt32 b/d::c
	uint32_t ___c_2;
};

// b/e
struct e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024 
{
	// System.String b/e::a
	String_t* ___a_0;
	// System.Single b/e::b
	float ___b_1;
	// System.Single b/e::c
	float ___c_2;
	// System.Byte[] b/e::d
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___d_3;
	// System.Single b/e::e
	float ___e_4;
	// System.Single b/e::f
	float ___f_5;
};
// Native definition for P/Invoke marshalling of b/e
struct e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024_marshaled_pinvoke
{
	char* ___a_0;
	float ___b_1;
	float ___c_2;
	Il2CppSafeArray/*NONE*/* ___d_3;
	float ___e_4;
	float ___f_5;
};
// Native definition for COM marshalling of b/e
struct e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024_marshaled_com
{
	Il2CppChar* ___a_0;
	float ___b_1;
	float ___c_2;
	Il2CppSafeArray/*NONE*/* ___d_3;
	float ___e_4;
	float ___f_5;
};

// c/a
struct a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6 
{
	// System.UInt32 c/a::a
	uint32_t ___a_0;
	// System.UInt32 c/a::b
	uint32_t ___b_1;
	// System.UInt16 c/a::c
	uint16_t ___c_2;
};

// c/c
struct c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0 
{
	// System.UInt64 c/c::a
	uint64_t ___a_0;
	// System.UInt64 c/c::b
	uint64_t ___b_1;
	// System.UInt32 c/c::c
	uint32_t ___c_2;
	// System.String c/c::d
	String_t* ___d_3;
};
// Native definition for P/Invoke marshalling of c/c
struct c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_pinvoke
{
	uint64_t ___a_0;
	uint64_t ___b_1;
	uint32_t ___c_2;
	char* ___d_3;
};
// Native definition for COM marshalling of c/c
struct c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_com
{
	uint64_t ___a_0;
	uint64_t ___b_1;
	uint32_t ___c_2;
	Il2CppChar* ___d_3;
};

// c/d
struct d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2 
{
	// System.UInt32 c/d::a
	uint32_t ___a_0;
	// c/c[] c/d::b
	cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF* ___b_1;
};
// Native definition for P/Invoke marshalling of c/d
struct d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2_marshaled_pinvoke
{
	uint32_t ___a_0;
	c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_pinvoke* ___b_1;
};
// Native definition for COM marshalling of c/d
struct d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2_marshaled_com
{
	uint32_t ___a_0;
	c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_com* ___b_1;
};

// c/e
struct e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777 
{
	// System.String c/e::a
	String_t* ___a_0;
	// System.UInt32 c/e::b
	uint32_t ___b_1;
	// System.String c/e::c
	String_t* ___c_2;
	// System.String c/e::d
	String_t* ___d_3;
	// System.UInt64 c/e::e
	uint64_t ___e_4;
	// System.UInt32 c/e::f
	uint32_t ___f_5;
	// System.UInt32 c/e::g
	uint32_t ___g_6;
	// System.UInt32 c/e::h
	uint32_t ___h_7;
};
// Native definition for P/Invoke marshalling of c/e
struct e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777_marshaled_pinvoke
{
	char* ___a_0;
	uint32_t ___b_1;
	char* ___c_2;
	char* ___d_3;
	uint64_t ___e_4;
	uint32_t ___f_5;
	uint32_t ___g_6;
	uint32_t ___h_7;
};
// Native definition for COM marshalling of c/e
struct e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777_marshaled_com
{
	Il2CppChar* ___a_0;
	uint32_t ___b_1;
	Il2CppChar* ___c_2;
	Il2CppChar* ___d_3;
	uint64_t ___e_4;
	uint32_t ___f_5;
	uint32_t ___g_6;
	uint32_t ___h_7;
};

// d/a
struct a_tC905F9A47B8F42EC7D8702A0FDD3F296EF74457B 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t a_tC905F9A47B8F42EC7D8702A0FDD3F296EF74457B__padding[46];
	};
};

// d/b
struct b_tF461D840050343BC580EE7A263DE9FE625E47B84 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t b_tF461D840050343BC580EE7A263DE9FE625E47B84__padding[72];
	};
};

// d/c
struct c_t7A9F326BDE1D1BFC4A56A02D80580068ABFC2798 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t c_t7A9F326BDE1D1BFC4A56A02D80580068ABFC2798__padding[7950];
	};
};

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult>
struct AsyncTaskMethodBuilder_1_tE88892A6B2F97B5D44B7C3EE2DBEED85743412AC 
{
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_tD5ABB3A2536319A3345B32A5481E37E23DD8CEDF ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_tE41CFF640EB7C045550D9D0D92BE67533B084C17* ___m_task_2;
};

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t9B0B2A8DD3C1F36C25925A26EB3ED0CB0DF8189E  : public RuntimeObject
{
};

// UnityEngine.Animations.AnimationHumanStream
struct AnimationHumanStream_t31E8EAD3F7C2C29CAE7B4EFB87AA84ECC6DCC6EC 
{
	// System.IntPtr UnityEngine.Animations.AnimationHumanStream::stream
	intptr_t ___stream_0;
};

// UnityEngine.Animations.AnimationStream
struct AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A 
{
	// System.UInt32 UnityEngine.Animations.AnimationStream::m_AnimatorBindingsVersion
	uint32_t ___m_AnimatorBindingsVersion_0;
	// System.IntPtr UnityEngine.Animations.AnimationStream::constant
	intptr_t ___constant_1;
	// System.IntPtr UnityEngine.Animations.AnimationStream::input
	intptr_t ___input_2;
	// System.IntPtr UnityEngine.Animations.AnimationStream::output
	intptr_t ___output_3;
	// System.IntPtr UnityEngine.Animations.AnimationStream::workspace
	intptr_t ___workspace_4;
	// System.IntPtr UnityEngine.Animations.AnimationStream::inputStreamAccessor
	intptr_t ___inputStreamAccessor_5;
	// System.IntPtr UnityEngine.Animations.AnimationStream::animationHandleBinder
	intptr_t ___animationHandleBinder_6;
};

// UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_tE8693FF0E67CDBA52BAFB211BFF1844D076ABAFB* ___m_completeCallback_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};

// System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_t253E37B63E7E7B504878AE6563347C147F98EF2D 
{
	// System.Threading.SynchronizationContext System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_synchronizationContext
	SynchronizationContext_tCDB842BBE53B050802CBBB59C6E6DC45B5B06DC0* ___m_synchronizationContext_0;
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_coreState
	AsyncMethodBuilderCore_tD5ABB3A2536319A3345B32A5481E37E23DD8CEDF ___m_coreState_1;
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_task
	Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572* ___m_task_2;
};
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_t253E37B63E7E7B504878AE6563347C147F98EF2D_marshaled_pinvoke
{
	SynchronizationContext_tCDB842BBE53B050802CBBB59C6E6DC45B5B06DC0* ___m_synchronizationContext_0;
	AsyncMethodBuilderCore_tD5ABB3A2536319A3345B32A5481E37E23DD8CEDF_marshaled_pinvoke ___m_coreState_1;
	Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572* ___m_task_2;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_t253E37B63E7E7B504878AE6563347C147F98EF2D_marshaled_com
{
	SynchronizationContext_tCDB842BBE53B050802CBBB59C6E6DC45B5B06DC0* ___m_synchronizationContext_0;
	AsyncMethodBuilderCore_tD5ABB3A2536319A3345B32A5481E37E23DD8CEDF_marshaled_com ___m_coreState_1;
	Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572* ___m_task_2;
};

// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.CertificateHandler::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Collision
struct Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0  : public RuntimeObject
{
	// UnityEngine.Vector3 UnityEngine.Collision::m_Impulse
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Impulse_0;
	// UnityEngine.Vector3 UnityEngine.Collision::m_RelativeVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_RelativeVelocity_1;
	// UnityEngine.Component UnityEngine.Collision::m_Body
	Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* ___m_Body_2;
	// UnityEngine.Collider UnityEngine.Collision::m_Collider
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_3;
	// System.Int32 UnityEngine.Collision::m_ContactCount
	int32_t ___m_ContactCount_4;
	// UnityEngine.ContactPoint[] UnityEngine.Collision::m_ReusedContacts
	ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* ___m_ReusedContacts_5;
	// UnityEngine.ContactPoint[] UnityEngine.Collision::m_LegacyContacts
	ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* ___m_LegacyContacts_6;
};
// Native definition for P/Invoke marshalling of UnityEngine.Collision
struct Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0_marshaled_pinvoke
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Impulse_0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_RelativeVelocity_1;
	Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* ___m_Body_2;
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_3;
	int32_t ___m_ContactCount_4;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_ReusedContacts_5;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_LegacyContacts_6;
};
// Native definition for COM marshalling of UnityEngine.Collision
struct Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0_marshaled_com
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Impulse_0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_RelativeVelocity_1;
	Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* ___m_Body_2;
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_3;
	int32_t ___m_ContactCount_4;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_ReusedContacts_5;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_LegacyContacts_6;
};

// UnityEngine.Collision2D
struct Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B  : public RuntimeObject
{
	// System.Int32 UnityEngine.Collision2D::m_Collider
	int32_t ___m_Collider_0;
	// System.Int32 UnityEngine.Collision2D::m_OtherCollider
	int32_t ___m_OtherCollider_1;
	// System.Int32 UnityEngine.Collision2D::m_Rigidbody
	int32_t ___m_Rigidbody_2;
	// System.Int32 UnityEngine.Collision2D::m_OtherRigidbody
	int32_t ___m_OtherRigidbody_3;
	// UnityEngine.Vector2 UnityEngine.Collision2D::m_RelativeVelocity
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_RelativeVelocity_4;
	// System.Int32 UnityEngine.Collision2D::m_Enabled
	int32_t ___m_Enabled_5;
	// System.Int32 UnityEngine.Collision2D::m_ContactCount
	int32_t ___m_ContactCount_6;
	// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::m_ReusedContacts
	ContactPoint2DU5BU5D_t427621BF8902AE33C86E7BF384D9B2B5B781F949* ___m_ReusedContacts_7;
	// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::m_LegacyContacts
	ContactPoint2DU5BU5D_t427621BF8902AE33C86E7BF384D9B2B5B781F949* ___m_LegacyContacts_8;
};
// Native definition for P/Invoke marshalling of UnityEngine.Collision2D
struct Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B_marshaled_pinvoke
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_RelativeVelocity_4;
	int32_t ___m_Enabled_5;
	int32_t ___m_ContactCount_6;
	ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801* ___m_ReusedContacts_7;
	ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801* ___m_LegacyContacts_8;
};
// Native definition for COM marshalling of UnityEngine.Collision2D
struct Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B_marshaled_com
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_RelativeVelocity_4;
	int32_t ___m_Enabled_5;
	int32_t ___m_ContactCount_6;
	ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801* ___m_ReusedContacts_7;
	ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801* ___m_LegacyContacts_8;
};

// UnityEngine.ContactFilter2D
struct ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14 
{
	// System.Boolean UnityEngine.ContactFilter2D::useTriggers
	bool ___useTriggers_0;
	// System.Boolean UnityEngine.ContactFilter2D::useLayerMask
	bool ___useLayerMask_1;
	// System.Boolean UnityEngine.ContactFilter2D::useDepth
	bool ___useDepth_2;
	// System.Boolean UnityEngine.ContactFilter2D::useOutsideDepth
	bool ___useOutsideDepth_3;
	// System.Boolean UnityEngine.ContactFilter2D::useNormalAngle
	bool ___useNormalAngle_4;
	// System.Boolean UnityEngine.ContactFilter2D::useOutsideNormalAngle
	bool ___useOutsideNormalAngle_5;
	// UnityEngine.LayerMask UnityEngine.ContactFilter2D::layerMask
	LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___layerMask_6;
	// System.Single UnityEngine.ContactFilter2D::minDepth
	float ___minDepth_7;
	// System.Single UnityEngine.ContactFilter2D::maxDepth
	float ___maxDepth_8;
	// System.Single UnityEngine.ContactFilter2D::minNormalAngle
	float ___minNormalAngle_9;
	// System.Single UnityEngine.ContactFilter2D::maxNormalAngle
	float ___maxNormalAngle_10;
};
// Native definition for P/Invoke marshalling of UnityEngine.ContactFilter2D
struct ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14_marshaled_pinvoke
{
	int32_t ___useTriggers_0;
	int32_t ___useLayerMask_1;
	int32_t ___useDepth_2;
	int32_t ___useOutsideDepth_3;
	int32_t ___useNormalAngle_4;
	int32_t ___useOutsideNormalAngle_5;
	LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___layerMask_6;
	float ___minDepth_7;
	float ___maxDepth_8;
	float ___minNormalAngle_9;
	float ___maxNormalAngle_10;
};
// Native definition for COM marshalling of UnityEngine.ContactFilter2D
struct ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14_marshaled_com
{
	int32_t ___useTriggers_0;
	int32_t ___useLayerMask_1;
	int32_t ___useDepth_2;
	int32_t ___useOutsideDepth_3;
	int32_t ___useNormalAngle_4;
	int32_t ___useOutsideNormalAngle_5;
	LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___layerMask_6;
	float ___minDepth_7;
	float ___maxDepth_8;
	float ___minNormalAngle_9;
	float ___maxNormalAngle_10;
};

// UnityEngine.ContactPoint
struct ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9 
{
	// UnityEngine.Vector3 UnityEngine.ContactPoint::m_Point
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.ContactPoint::m_Normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_1;
	// System.Int32 UnityEngine.ContactPoint::m_ThisColliderInstanceID
	int32_t ___m_ThisColliderInstanceID_2;
	// System.Int32 UnityEngine.ContactPoint::m_OtherColliderInstanceID
	int32_t ___m_OtherColliderInstanceID_3;
	// System.Single UnityEngine.ContactPoint::m_Separation
	float ___m_Separation_4;
};

// UnityEngine.ContactPoint2D
struct ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801 
{
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_Point
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Point_0;
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_Normal
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Normal_1;
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_RelativeVelocity
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_RelativeVelocity_2;
	// System.Single UnityEngine.ContactPoint2D::m_Separation
	float ___m_Separation_3;
	// System.Single UnityEngine.ContactPoint2D::m_NormalImpulse
	float ___m_NormalImpulse_4;
	// System.Single UnityEngine.ContactPoint2D::m_TangentImpulse
	float ___m_TangentImpulse_5;
	// System.Int32 UnityEngine.ContactPoint2D::m_Collider
	int32_t ___m_Collider_6;
	// System.Int32 UnityEngine.ContactPoint2D::m_OtherCollider
	int32_t ___m_OtherCollider_7;
	// System.Int32 UnityEngine.ContactPoint2D::m_Rigidbody
	int32_t ___m_Rigidbody_8;
	// System.Int32 UnityEngine.ContactPoint2D::m_OtherRigidbody
	int32_t ___m_OtherRigidbody_9;
	// System.Int32 UnityEngine.ContactPoint2D::m_Enabled
	int32_t ___m_Enabled_10;
};

// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92  : public RuntimeObject
{
	// UnityEngine.CharacterController UnityEngine.ControllerColliderHit::m_Controller
	CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* ___m_Controller_0;
	// UnityEngine.Collider UnityEngine.ControllerColliderHit::m_Collider
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_1;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_Point
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_2;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_Normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_3;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_MoveDirection
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_MoveDirection_4;
	// System.Single UnityEngine.ControllerColliderHit::m_MoveLength
	float ___m_MoveLength_5;
	// System.Int32 UnityEngine.ControllerColliderHit::m_Push
	int32_t ___m_Push_6;
};
// Native definition for P/Invoke marshalling of UnityEngine.ControllerColliderHit
struct ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92_marshaled_pinvoke
{
	CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* ___m_Controller_0;
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_2;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_3;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_MoveDirection_4;
	float ___m_MoveLength_5;
	int32_t ___m_Push_6;
};
// Native definition for COM marshalling of UnityEngine.ControllerColliderHit
struct ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92_marshaled_com
{
	CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* ___m_Controller_0;
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_2;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_3;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_MoveDirection_4;
	float ___m_MoveLength_5;
	int32_t ___m_Push_6;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Event
struct Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Event::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Event
struct Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Event
struct Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// UnityEngine.Bindings.FreeFunctionAttribute
struct FreeFunctionAttribute_t1200571BEDF64167E58F976FB7374AEA5D9BCBB6  : public NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270
{
};

// UnityEngine.GUI
struct GUI_tA9CDB3D69DB13D51AD83ABDB587EF95947EC2D2A  : public RuntimeObject
{
};

// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F  : public RuntimeObject
{
	// System.Single UnityEngine.GUILayoutEntry::minWidth
	float ___minWidth_0;
	// System.Single UnityEngine.GUILayoutEntry::maxWidth
	float ___maxWidth_1;
	// System.Single UnityEngine.GUILayoutEntry::minHeight
	float ___minHeight_2;
	// System.Single UnityEngine.GUILayoutEntry::maxHeight
	float ___maxHeight_3;
	// UnityEngine.Rect UnityEngine.GUILayoutEntry::rect
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___rect_4;
	// System.Int32 UnityEngine.GUILayoutEntry::stretchWidth
	int32_t ___stretchWidth_5;
	// System.Int32 UnityEngine.GUILayoutEntry::stretchHeight
	int32_t ___stretchHeight_6;
	// System.Boolean UnityEngine.GUILayoutEntry::consideredForMargin
	bool ___consideredForMargin_7;
	// UnityEngine.GUIStyle UnityEngine.GUILayoutEntry::m_Style
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_Style_8;
};

// UnityEngine.GUILayoutUtility
struct GUILayoutUtility_t48D00CD11CFC1E09E8EC2E51D59E735F5D24836F  : public RuntimeObject
{
};

// UnityEngine.GUISettings
struct GUISettings_tF2CA7E8B9F62F1FC013BFF053B5FA2709EBA3847  : public RuntimeObject
{
	// System.Boolean UnityEngine.GUISettings::m_DoubleClickSelectsWord
	bool ___m_DoubleClickSelectsWord_0;
	// System.Boolean UnityEngine.GUISettings::m_TripleClickSelectsLine
	bool ___m_TripleClickSelectsLine_1;
	// UnityEngine.Color UnityEngine.GUISettings::m_CursorColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_CursorColor_2;
	// System.Single UnityEngine.GUISettings::m_CursorFlashSpeed
	float ___m_CursorFlashSpeed_3;
	// UnityEngine.Color UnityEngine.GUISettings::m_SelectionColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_SelectionColor_4;
};

// UnityEngine.GUIStyleState
struct GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95  : public RuntimeObject
{
	// System.IntPtr UnityEngine.GUIStyleState::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyle UnityEngine.GUIStyleState::m_SourceStyle
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_SourceStyle_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyleState
struct GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_marshaled_pinvoke* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.GUIStyleState
struct GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_marshaled_com* ___m_SourceStyle_1;
};

// UnityEngine.UIElements.UIR.GfxUpdateBufferRange
struct GfxUpdateBufferRange_tC47258BCB472B0727B4FCE21A2A53506644C1A97 
{
	// System.UInt32 UnityEngine.UIElements.UIR.GfxUpdateBufferRange::offsetFromWriteStart
	uint32_t ___offsetFromWriteStart_0;
	// System.UInt32 UnityEngine.UIElements.UIR.GfxUpdateBufferRange::size
	uint32_t ___size_1;
	// System.UIntPtr UnityEngine.UIElements.UIR.GfxUpdateBufferRange::source
	uintptr_t ___source_2;
};

// UnityEngine.TextCore.Glyph
struct Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F  : public RuntimeObject
{
	// System.UInt32 UnityEngine.TextCore.Glyph::m_Index
	uint32_t ___m_Index_0;
	// UnityEngine.TextCore.GlyphMetrics UnityEngine.TextCore.Glyph::m_Metrics
	GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A ___m_Metrics_1;
	// UnityEngine.TextCore.GlyphRect UnityEngine.TextCore.Glyph::m_GlyphRect
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___m_GlyphRect_2;
	// System.Single UnityEngine.TextCore.Glyph::m_Scale
	float ___m_Scale_3;
	// System.Int32 UnityEngine.TextCore.Glyph::m_AtlasIndex
	int32_t ___m_AtlasIndex_4;
	// UnityEngine.TextCore.GlyphClassDefinitionType UnityEngine.TextCore.Glyph::m_ClassDefinitionType
	int32_t ___m_ClassDefinitionType_5;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.Glyph
struct Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F_marshaled_pinvoke
{
	uint32_t ___m_Index_0;
	GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A ___m_Metrics_1;
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___m_GlyphRect_2;
	float ___m_Scale_3;
	int32_t ___m_AtlasIndex_4;
	int32_t ___m_ClassDefinitionType_5;
};
// Native definition for COM marshalling of UnityEngine.TextCore.Glyph
struct Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F_marshaled_com
{
	uint32_t ___m_Index_0;
	GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A ___m_Metrics_1;
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___m_GlyphRect_2;
	float ___m_Scale_3;
	int32_t ___m_AtlasIndex_4;
	int32_t ___m_ClassDefinitionType_5;
};

// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord
struct GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7 
{
	// System.UInt32 UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord::m_GlyphIndex
	uint32_t ___m_GlyphIndex_0;
	// UnityEngine.TextCore.LowLevel.GlyphValueRecord UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord::m_GlyphValueRecord
	GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E ___m_GlyphValueRecord_1;
};

// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct
struct GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C 
{
	// System.UInt32 UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::index
	uint32_t ___index_0;
	// UnityEngine.TextCore.GlyphMetrics UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::metrics
	GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A ___metrics_1;
	// UnityEngine.TextCore.GlyphRect UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::glyphRect
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___glyphRect_2;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::scale
	float ___scale_3;
	// System.Int32 UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::atlasIndex
	int32_t ___atlasIndex_4;
	// UnityEngine.TextCore.GlyphClassDefinitionType UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::classDefinitionType
	int32_t ___classDefinitionType_5;
};

// UnityEngine.HumanLimit
struct HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E 
{
	// UnityEngine.Vector3 UnityEngine.HumanLimit::m_Min
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Min_0;
	// UnityEngine.Vector3 UnityEngine.HumanLimit::m_Max
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Max_1;
	// UnityEngine.Vector3 UnityEngine.HumanLimit::m_Center
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Center_2;
	// System.Single UnityEngine.HumanLimit::m_AxisLength
	float ___m_AxisLength_3;
	// System.Int32 UnityEngine.HumanLimit::m_UseDefaultValues
	int32_t ___m_UseDefaultValues_4;
};

// System.Configuration.IgnoreSection
struct IgnoreSection_t43A7C33C0083D18639AA3CC3D75DD93FCF1C5D97  : public ConfigurationSection_t0BC609F0151B160A4FAB8226679B62AF22539C3E
{
};

// UnityEngine.ModifiableContactPair
struct ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960 
{
	// System.IntPtr UnityEngine.ModifiableContactPair::actor
	intptr_t ___actor_0;
	// System.IntPtr UnityEngine.ModifiableContactPair::otherActor
	intptr_t ___otherActor_1;
	// System.IntPtr UnityEngine.ModifiableContactPair::shape
	intptr_t ___shape_2;
	// System.IntPtr UnityEngine.ModifiableContactPair::otherShape
	intptr_t ___otherShape_3;
	// UnityEngine.Quaternion UnityEngine.ModifiableContactPair::rotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_4;
	// UnityEngine.Vector3 UnityEngine.ModifiableContactPair::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_5;
	// UnityEngine.Quaternion UnityEngine.ModifiableContactPair::otherRotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___otherRotation_6;
	// UnityEngine.Vector3 UnityEngine.ModifiableContactPair::otherPosition
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___otherPosition_7;
	// System.Int32 UnityEngine.ModifiableContactPair::numContacts
	int32_t ___numContacts_8;
	// System.IntPtr UnityEngine.ModifiableContactPair::contacts
	intptr_t ___contacts_9;
};

// UnityEngine.Bindings.NativePropertyAttribute
struct NativePropertyAttribute_tAF7FB03BF7FFE9E8AB0E75B0F842FC0AA22AE607  : public NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270
{
	// UnityEngine.Bindings.TargetType UnityEngine.Bindings.NativePropertyAttribute::<TargetType>k__BackingField
	int32_t ___U3CTargetTypeU3Ek__BackingField_5;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.ObjectGUIState
struct ObjectGUIState_t7BE88DC8B9C7187A77D63BBCBE9DB7B674863C15  : public RuntimeObject
{
	// System.IntPtr UnityEngine.ObjectGUIState::m_Ptr
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Playables.PlayableHandle
struct PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 
{
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableHandle::m_Version
	uint32_t ___m_Version_1;
};

// UnityEngine.Playables.PlayableOutputHandle
struct PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883 
{
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	uint32_t ___m_Version_1;
};

// Unity.Profiling.ProfilerMarker
struct ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD 
{
	// System.IntPtr Unity.Profiling.ProfilerMarker::m_Ptr
	intptr_t ___m_Ptr_0;
};

// UnityEngine.RaycastHit
struct RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5 
{
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;
};

// UnityEngine.RaycastHit2D
struct RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA 
{
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;
};

// UnityEngine.RectOffset
struct RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5  : public RuntimeObject
{
	// System.IntPtr UnityEngine.RectOffset::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Object UnityEngine.RectOffset::m_SourceStyle
	RuntimeObject* ___m_SourceStyle_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.RectOffset
struct RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.RectOffset
struct RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};

// UnityEngine.RemoteConfigSettings
struct RemoteConfigSettings_tC979947EE51355162B3241B9F80D95A8FD25FE52  : public RuntimeObject
{
	// System.IntPtr UnityEngine.RemoteConfigSettings::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<System.Boolean> UnityEngine.RemoteConfigSettings::Updated
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* ___Updated_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.RemoteConfigSettings
struct RemoteConfigSettings_tC979947EE51355162B3241B9F80D95A8FD25FE52_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___Updated_1;
};
// Native definition for COM marshalling of UnityEngine.RemoteConfigSettings
struct RemoteConfigSettings_tC979947EE51355162B3241B9F80D95A8FD25FE52_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___Updated_1;
};

// UnityEngine.SendMouseEvents
struct SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39  : public RuntimeObject
{
};

// UnityEngine.SkeletonBone
struct SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126 
{
	// System.String UnityEngine.SkeletonBone::name
	String_t* ___name_0;
	// System.String UnityEngine.SkeletonBone::parentName
	String_t* ___parentName_1;
	// UnityEngine.Vector3 UnityEngine.SkeletonBone::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_2;
	// UnityEngine.Quaternion UnityEngine.SkeletonBone::rotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_3;
	// UnityEngine.Vector3 UnityEngine.SkeletonBone::scale
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale_4;
};
// Native definition for P/Invoke marshalling of UnityEngine.SkeletonBone
struct SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126_marshaled_pinvoke
{
	char* ___name_0;
	char* ___parentName_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_2;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_3;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale_4;
};
// Native definition for COM marshalling of UnityEngine.SkeletonBone
struct SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppChar* ___parentName_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_2;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_3;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale_4;
};

// UnityEngine.TextEditor
struct TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27  : public RuntimeObject
{
	// UnityEngine.TouchScreenKeyboard UnityEngine.TextEditor::keyboardOnScreen
	TouchScreenKeyboard_tE87B78A3DAED69816B44C99270A734682E093E7A* ___keyboardOnScreen_0;
	// System.Int32 UnityEngine.TextEditor::controlID
	int32_t ___controlID_1;
	// UnityEngine.GUIStyle UnityEngine.TextEditor::style
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___style_2;
	// System.Boolean UnityEngine.TextEditor::multiline
	bool ___multiline_3;
	// System.Boolean UnityEngine.TextEditor::hasHorizontalCursorPos
	bool ___hasHorizontalCursorPos_4;
	// System.Boolean UnityEngine.TextEditor::isPasswordField
	bool ___isPasswordField_5;
	// System.Boolean UnityEngine.TextEditor::m_HasFocus
	bool ___m_HasFocus_6;
	// UnityEngine.Vector2 UnityEngine.TextEditor::scrollOffset
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___scrollOffset_7;
	// UnityEngine.GUIContent UnityEngine.TextEditor::m_Content
	GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2* ___m_Content_8;
	// UnityEngine.Rect UnityEngine.TextEditor::m_Position
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___m_Position_9;
	// System.Int32 UnityEngine.TextEditor::m_CursorIndex
	int32_t ___m_CursorIndex_10;
	// System.Int32 UnityEngine.TextEditor::m_SelectIndex
	int32_t ___m_SelectIndex_11;
	// System.Boolean UnityEngine.TextEditor::m_RevealCursor
	bool ___m_RevealCursor_12;
	// UnityEngine.Vector2 UnityEngine.TextEditor::graphicalCursorPos
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___graphicalCursorPos_13;
	// UnityEngine.Vector2 UnityEngine.TextEditor::graphicalSelectCursorPos
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___graphicalSelectCursorPos_14;
	// System.Boolean UnityEngine.TextEditor::m_MouseDragSelectsWholeWords
	bool ___m_MouseDragSelectsWholeWords_15;
	// System.Int32 UnityEngine.TextEditor::m_DblClickInitPos
	int32_t ___m_DblClickInitPos_16;
	// UnityEngine.TextEditor/DblClickSnapping UnityEngine.TextEditor::m_DblClickSnap
	uint8_t ___m_DblClickSnap_17;
	// System.Boolean UnityEngine.TextEditor::m_bJustSelected
	bool ___m_bJustSelected_18;
	// System.Int32 UnityEngine.TextEditor::m_iAltCursorPos
	int32_t ___m_iAltCursorPos_19;
	// System.String UnityEngine.TextEditor::oldText
	String_t* ___oldText_20;
	// System.Int32 UnityEngine.TextEditor::oldPos
	int32_t ___oldPos_21;
	// System.Int32 UnityEngine.TextEditor::oldSelectPos
	int32_t ___oldSelectPos_22;
};

// UnityEngine.TextGenerationSettings
struct TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3 
{
	// UnityEngine.Font UnityEngine.TextGenerationSettings::font
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_0;
	// UnityEngine.Color UnityEngine.TextGenerationSettings::color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_1;
	// System.Int32 UnityEngine.TextGenerationSettings::fontSize
	int32_t ___fontSize_2;
	// System.Single UnityEngine.TextGenerationSettings::lineSpacing
	float ___lineSpacing_3;
	// System.Boolean UnityEngine.TextGenerationSettings::richText
	bool ___richText_4;
	// System.Single UnityEngine.TextGenerationSettings::scaleFactor
	float ___scaleFactor_5;
	// UnityEngine.FontStyle UnityEngine.TextGenerationSettings::fontStyle
	int32_t ___fontStyle_6;
	// UnityEngine.TextAnchor UnityEngine.TextGenerationSettings::textAnchor
	int32_t ___textAnchor_7;
	// System.Boolean UnityEngine.TextGenerationSettings::alignByGeometry
	bool ___alignByGeometry_8;
	// System.Boolean UnityEngine.TextGenerationSettings::resizeTextForBestFit
	bool ___resizeTextForBestFit_9;
	// System.Int32 UnityEngine.TextGenerationSettings::resizeTextMinSize
	int32_t ___resizeTextMinSize_10;
	// System.Int32 UnityEngine.TextGenerationSettings::resizeTextMaxSize
	int32_t ___resizeTextMaxSize_11;
	// System.Boolean UnityEngine.TextGenerationSettings::updateBounds
	bool ___updateBounds_12;
	// UnityEngine.VerticalWrapMode UnityEngine.TextGenerationSettings::verticalOverflow
	int32_t ___verticalOverflow_13;
	// UnityEngine.HorizontalWrapMode UnityEngine.TextGenerationSettings::horizontalOverflow
	int32_t ___horizontalOverflow_14;
	// UnityEngine.Vector2 UnityEngine.TextGenerationSettings::generationExtents
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___generationExtents_15;
	// UnityEngine.Vector2 UnityEngine.TextGenerationSettings::pivot
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___pivot_16;
	// System.Boolean UnityEngine.TextGenerationSettings::generateOutOfBounds
	bool ___generateOutOfBounds_17;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextGenerationSettings
struct TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3_marshaled_pinvoke
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_0;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_1;
	int32_t ___fontSize_2;
	float ___lineSpacing_3;
	int32_t ___richText_4;
	float ___scaleFactor_5;
	int32_t ___fontStyle_6;
	int32_t ___textAnchor_7;
	int32_t ___alignByGeometry_8;
	int32_t ___resizeTextForBestFit_9;
	int32_t ___resizeTextMinSize_10;
	int32_t ___resizeTextMaxSize_11;
	int32_t ___updateBounds_12;
	int32_t ___verticalOverflow_13;
	int32_t ___horizontalOverflow_14;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___generationExtents_15;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___pivot_16;
	int32_t ___generateOutOfBounds_17;
};
// Native definition for COM marshalling of UnityEngine.TextGenerationSettings
struct TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3_marshaled_com
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_0;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_1;
	int32_t ___fontSize_2;
	float ___lineSpacing_3;
	int32_t ___richText_4;
	float ___scaleFactor_5;
	int32_t ___fontStyle_6;
	int32_t ___textAnchor_7;
	int32_t ___alignByGeometry_8;
	int32_t ___resizeTextForBestFit_9;
	int32_t ___resizeTextMinSize_10;
	int32_t ___resizeTextMaxSize_11;
	int32_t ___updateBounds_12;
	int32_t ___verticalOverflow_13;
	int32_t ___horizontalOverflow_14;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___generationExtents_15;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___pivot_16;
	int32_t ___generateOutOfBounds_17;
};

// UnityEngine.UIElements.TextNativeSettings
struct TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062 
{
	// System.String UnityEngine.UIElements.TextNativeSettings::text
	String_t* ___text_0;
	// UnityEngine.Font UnityEngine.UIElements.TextNativeSettings::font
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_1;
	// System.Int32 UnityEngine.UIElements.TextNativeSettings::size
	int32_t ___size_2;
	// System.Single UnityEngine.UIElements.TextNativeSettings::scaling
	float ___scaling_3;
	// UnityEngine.FontStyle UnityEngine.UIElements.TextNativeSettings::style
	int32_t ___style_4;
	// UnityEngine.Color UnityEngine.UIElements.TextNativeSettings::color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_5;
	// UnityEngine.TextAnchor UnityEngine.UIElements.TextNativeSettings::anchor
	int32_t ___anchor_6;
	// System.Boolean UnityEngine.UIElements.TextNativeSettings::wordWrap
	bool ___wordWrap_7;
	// System.Single UnityEngine.UIElements.TextNativeSettings::wordWrapWidth
	float ___wordWrapWidth_8;
	// System.Boolean UnityEngine.UIElements.TextNativeSettings::richText
	bool ___richText_9;
};
// Native definition for P/Invoke marshalling of UnityEngine.UIElements.TextNativeSettings
struct TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062_marshaled_pinvoke
{
	char* ___text_0;
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_1;
	int32_t ___size_2;
	float ___scaling_3;
	int32_t ___style_4;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_5;
	int32_t ___anchor_6;
	int32_t ___wordWrap_7;
	float ___wordWrapWidth_8;
	int32_t ___richText_9;
};
// Native definition for COM marshalling of UnityEngine.UIElements.TextNativeSettings
struct TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062_marshaled_com
{
	Il2CppChar* ___text_0;
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_1;
	int32_t ___size_2;
	float ___scaling_3;
	int32_t ___style_4;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_5;
	int32_t ___anchor_6;
	int32_t ___wordWrap_7;
	float ___wordWrapWidth_8;
	int32_t ___richText_9;
};

// UnityEngine.UIElements.TextVertex
struct TextVertex_tF56662BA585F7DD34D71971F1AA1D2E767946CF3 
{
	// UnityEngine.Vector3 UnityEngine.UIElements.TextVertex::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_0;
	// UnityEngine.Color32 UnityEngine.UIElements.TextVertex::color
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___color_1;
	// UnityEngine.Vector2 UnityEngine.UIElements.TextVertex::uv0
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___uv0_2;
};

// UnityEngine.Bindings.ThreadSafeAttribute
struct ThreadSafeAttribute_t2535A209D57BDA2FF398C4CA766059277FC349FE  : public NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270
{
};

// UnityEngine.Touch
struct Touch_t03E51455ED508492B3F278903A0114FA0E87B417 
{
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;
};

// UnityEngine.TrackedReference
struct TrackedReference_tF35FF4FB6E89ACD81C24469FAF0CA6FFF29262A2  : public RuntimeObject
{
	// System.IntPtr UnityEngine.TrackedReference::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.TrackedReference
struct TrackedReference_tF35FF4FB6E89ACD81C24469FAF0CA6FFF29262A2_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.TrackedReference
struct TrackedReference_tF35FF4FB6E89ACD81C24469FAF0CA6FFF29262A2_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.UICharInfo
struct UICharInfo_t24C2EA0F2F3A938100C271891D9DEB015ABA5FBD 
{
	// UnityEngine.Vector2 UnityEngine.UICharInfo::cursorPos
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___cursorPos_0;
	// System.Single UnityEngine.UICharInfo::charWidth
	float ___charWidth_1;
};

// UnityEngine.UIVertex
struct UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207 
{
	// UnityEngine.Vector3 UnityEngine.UIVertex::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_0;
	// UnityEngine.Vector3 UnityEngine.UIVertex::normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___normal_1;
	// UnityEngine.Vector4 UnityEngine.UIVertex::tangent
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___tangent_2;
	// UnityEngine.Color32 UnityEngine.UIVertex::color
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___color_3;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv0
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv0_4;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv1
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv1_5;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv2
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv2_6;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv3
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv3_7;
};

// UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.UploadHandler::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Yoga.YogaConfig
struct YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Yoga.YogaConfig::_ygConfig
	intptr_t ____ygConfig_1;
	// UnityEngine.Yoga.Logger UnityEngine.Yoga.YogaConfig::_logger
	Logger_t092B1218ED93DD47180692D5761559B2054234A0* ____logger_2;
};

// UnityEngine.Yoga.YogaNode
struct YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Yoga.YogaNode::_ygNode
	intptr_t ____ygNode_0;
	// UnityEngine.Yoga.YogaConfig UnityEngine.Yoga.YogaNode::_config
	YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345* ____config_1;
	// System.WeakReference UnityEngine.Yoga.YogaNode::_parent
	WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E* ____parent_2;
	// System.Collections.Generic.List`1<UnityEngine.Yoga.YogaNode> UnityEngine.Yoga.YogaNode::_children
	List_1_t84B666107A8A3ECB0F5A24B0243137D056DA9165* ____children_3;
	// UnityEngine.Yoga.MeasureFunction UnityEngine.Yoga.YogaNode::_measureFunction
	MeasureFunction_t60EBED1328F5328D4FA7E26335967E59E73B4D09* ____measureFunction_4;
	// UnityEngine.Yoga.BaselineFunction UnityEngine.Yoga.YogaNode::_baselineFunction
	BaselineFunction_t13AFADEF52F63320B2159C237635948AEB801679* ____baselineFunction_5;
	// System.Object UnityEngine.Yoga.YogaNode::_data
	RuntimeObject* ____data_6;
};

// b
struct b_t972FAFF471676E1BF6780CEF716F7509668D82DA  : public RuntimeObject
{
	// System.Int64 b::d
	int64_t ___d_3;
	// System.IO.BinaryReader b::e
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___e_4;
	// System.IO.BinaryWriter b::f
	BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* ___f_5;
	// b/b b::g
	b_tFB66B901E947622DFAF3E315886F1089C51D6893 ___g_6;
	// b/c[] b::h
	cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E* ___h_7;
	// b/d[] b::i
	dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* ___i_8;
	// b/e b::j
	e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024 ___j_9;
	// System.Byte[] b::k
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___k_10;
};

// d
struct d_t7410D4303AF1B382C8A19061C6DA00D2360AB7AC  : public RuntimeObject
{
};

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_tF676EF1983CD3B91B739A19ABEEAB2A5B12A30CA  : public RuntimeObject
{
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_0;
	// System.Single DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::offsetYSet
	bool ___offsetYSet_2;
	// System.Single DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::offsetY
	float ___offsetY_3;
	// DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::s
	Sequence_tEADBE56D6ED2E9EE8FB2E5459C3E57131EC0545C* ___s_4;
	// UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::endValue
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___endValue_5;
	// DG.Tweening.Tween DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::yTween
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___yTween_6;
};

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t5ED7ACF8AF501EC38AC580536C22E8222A74CD9A  : public RuntimeObject
{
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::target
	Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F* ___target_0;
	// System.Single DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::offsetYSet
	bool ___offsetYSet_2;
	// System.Single DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::offsetY
	float ___offsetY_3;
	// DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::s
	Sequence_tEADBE56D6ED2E9EE8FB2E5459C3E57131EC0545C* ___s_4;
	// UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::endValue
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___endValue_5;
	// DG.Tweening.Tween DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::yTween
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___yTween_6;
};

// DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_tC6A3C24ECE56869CB1D6AB3A4FE842EF0C4297E1  : public RuntimeObject
{
	// UnityEngine.Color DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0::to
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___to_0;
	// UnityEngine.SpriteRenderer DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0::target
	SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* ___target_1;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0
struct U3CU3Ec__DisplayClass29_0_tAF3D1F414662AB44D54A1823AC81F9067EB039C5  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
	// System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::offsetYSet
	bool ___offsetYSet_2;
	// System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::offsetY
	float ___offsetY_3;
	// DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::s
	Sequence_tEADBE56D6ED2E9EE8FB2E5459C3E57131EC0545C* ___s_4;
	// UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::endValue
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___endValue_5;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0
struct U3CU3Ec__DisplayClass38_0_t267FF01517518B123F4EB11811FAF5BEDB2CB83D  : public RuntimeObject
{
	// UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0::to
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___to_0;
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0::target
	Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* ___target_1;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0
struct U3CU3Ec__DisplayClass39_0_t38D78B606D82F31C19D959AF12B5C4F1D4AF65C2  : public RuntimeObject
{
	// UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0::to
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___to_0;
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0::target
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___target_1;
};

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass40_0
struct U3CU3Ec__DisplayClass40_0_t0E7A83E8D1C93359BF9EB9E6A9AF3AA18311CD24  : public RuntimeObject
{
	// UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass40_0::to
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___to_0;
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/<>c__DisplayClass40_0::target
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___target_1;
};

// LevelManager/<SpawnRestauranYoo>d__14
struct U3CSpawnRestauranYooU3Ed__14_tD41954C93BAEB40872A8B4B06483338C1DDE34E3  : public RuntimeObject
{
	// System.Int32 LevelManager/<SpawnRestauranYoo>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LevelManager/<SpawnRestauranYoo>d__14::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// LevelManager LevelManager/<SpawnRestauranYoo>d__14::<>4__this
	LevelManager_t8405886BBC5A0ACBB1CC210E25D5DA1C72D16530* ___U3CU3E4__this_2;
	// System.Collections.Generic.List`1/Enumerator<RestaurantData> LevelManager/<SpawnRestauranYoo>d__14::<>7__wrap1
	Enumerator_tB0FE3237BD1DCDF4972626EA92AF452520944420 ___U3CU3E7__wrap1_3;
	// YooAsset.AssetOperationHandle LevelManager/<SpawnRestauranYoo>d__14::<hadle>5__3
	AssetOperationHandle_t3C4EE34040385828202FA37D72CC91D7668FB32B* ___U3ChadleU3E5__3_4;
};

// UnityEngine.ParticleSystem/Particle
struct Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D 
{
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Position_0;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Velocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Velocity_1;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AnimatedVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_AnimatedVelocity_2;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_InitialVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_InitialVelocity_3;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AxisOfRotation
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_AxisOfRotation_4;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Rotation
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Rotation_5;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AngularVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_AngularVelocity_6;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_StartSize
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_StartSize_7;
	// UnityEngine.Color32 UnityEngine.ParticleSystem/Particle::m_StartColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_StartColor_8;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_RandomSeed
	uint32_t ___m_RandomSeed_9;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_ParentRandomSeed
	uint32_t ___m_ParentRandomSeed_10;
	// System.Single UnityEngine.ParticleSystem/Particle::m_Lifetime
	float ___m_Lifetime_11;
	// System.Single UnityEngine.ParticleSystem/Particle::m_StartLifetime
	float ___m_StartLifetime_12;
	// System.Int32 UnityEngine.ParticleSystem/Particle::m_MeshIndex
	int32_t ___m_MeshIndex_13;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator0
	float ___m_EmitAccumulator0_14;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator1
	float ___m_EmitAccumulator1_15;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_Flags
	uint32_t ___m_Flags_16;
};

// b/c
struct c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D 
{
	// System.Int32 b/c::a
	int32_t ___a_0;
	// System.Boolean b/c::b
	bool ___b_1;
	// System.Int16 b/c::c
	int16_t ___c_2;
	// a b/c::d
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C ___d_3;
	// a b/c::e
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C ___e_4;
	// System.UInt32 b/c::f
	uint32_t ___f_5;
	// System.UInt32 b/c::g
	uint32_t ___g_6;
};
// Native definition for P/Invoke marshalling of b/c
struct c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D_marshaled_pinvoke
{
	int32_t ___a_0;
	int32_t ___b_1;
	int16_t ___c_2;
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C ___d_3;
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C ___e_4;
	uint32_t ___f_5;
	uint32_t ___g_6;
};
// Native definition for COM marshalling of b/c
struct c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D_marshaled_com
{
	int32_t ___a_0;
	int32_t ___b_1;
	int16_t ___c_2;
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C ___d_3;
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C ___e_4;
	uint32_t ___f_5;
	uint32_t ___g_6;
};

// c/b
struct b_t377CCBB7B97AADE524489D5766C28A547BBAB31B 
{
	// a c/b::a
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C ___a_0;
	// System.UInt32 c/b::b
	uint32_t ___b_1;
	// c/a[] c/b::c
	aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2* ___c_2;
};
// Native definition for P/Invoke marshalling of c/b
struct b_t377CCBB7B97AADE524489D5766C28A547BBAB31B_marshaled_pinvoke
{
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C ___a_0;
	uint32_t ___b_1;
	a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6* ___c_2;
};
// Native definition for COM marshalling of c/b
struct b_t377CCBB7B97AADE524489D5766C28A547BBAB31B_marshaled_com
{
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C ___a_0;
	uint32_t ___b_1;
	a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6* ___c_2;
};

// UnityEngine.Animations.AnimationClipPlayable
struct AnimationClipPlayable_t54CEA0DD315B1674C2BD49E681005C4271D73969 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationClipPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.AnimationEvent
struct AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174  : public RuntimeObject
{
	// System.Single UnityEngine.AnimationEvent::m_Time
	float ___m_Time_0;
	// System.String UnityEngine.AnimationEvent::m_FunctionName
	String_t* ___m_FunctionName_1;
	// System.String UnityEngine.AnimationEvent::m_StringParameter
	String_t* ___m_StringParameter_2;
	// UnityEngine.Object UnityEngine.AnimationEvent::m_ObjectReferenceParameter
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___m_ObjectReferenceParameter_3;
	// System.Single UnityEngine.AnimationEvent::m_FloatParameter
	float ___m_FloatParameter_4;
	// System.Int32 UnityEngine.AnimationEvent::m_IntParameter
	int32_t ___m_IntParameter_5;
	// System.Int32 UnityEngine.AnimationEvent::m_MessageOptions
	int32_t ___m_MessageOptions_6;
	// UnityEngine.AnimationEventSource UnityEngine.AnimationEvent::m_Source
	int32_t ___m_Source_7;
	// UnityEngine.AnimationState UnityEngine.AnimationEvent::m_StateSender
	AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE* ___m_StateSender_8;
	// UnityEngine.AnimatorStateInfo UnityEngine.AnimationEvent::m_AnimatorStateInfo
	AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2 ___m_AnimatorStateInfo_9;
	// UnityEngine.AnimatorClipInfo UnityEngine.AnimationEvent::m_AnimatorClipInfo
	AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03 ___m_AnimatorClipInfo_10;
};
// Native definition for P/Invoke marshalling of UnityEngine.AnimationEvent
struct AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174_marshaled_pinvoke
{
	float ___m_Time_0;
	char* ___m_FunctionName_1;
	char* ___m_StringParameter_2;
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke ___m_ObjectReferenceParameter_3;
	float ___m_FloatParameter_4;
	int32_t ___m_IntParameter_5;
	int32_t ___m_MessageOptions_6;
	int32_t ___m_Source_7;
	AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE* ___m_StateSender_8;
	AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2 ___m_AnimatorStateInfo_9;
	AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03 ___m_AnimatorClipInfo_10;
};
// Native definition for COM marshalling of UnityEngine.AnimationEvent
struct AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174_marshaled_com
{
	float ___m_Time_0;
	Il2CppChar* ___m_FunctionName_1;
	Il2CppChar* ___m_StringParameter_2;
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com* ___m_ObjectReferenceParameter_3;
	float ___m_FloatParameter_4;
	int32_t ___m_IntParameter_5;
	int32_t ___m_MessageOptions_6;
	int32_t ___m_Source_7;
	AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE* ___m_StateSender_8;
	AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2 ___m_AnimatorStateInfo_9;
	AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03 ___m_AnimatorClipInfo_10;
};

// UnityEngine.Animations.AnimationLayerMixerPlayable
struct AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationLayerMixerPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationMixerPlayable
struct AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMixerPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationMotionXToDeltaPlayable
struct AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMotionXToDeltaPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationOffsetPlayable
struct AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationOffsetPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationPlayableOutput
struct AnimationPlayableOutput_t753AC95DC826789BC537D18449E93114777DDF4E 
{
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Animations.AnimationPlayableOutput::m_Handle
	PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationPosePlayable
struct AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationPosePlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationRemoveScalePlayable
struct AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationRemoveScalePlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationScriptPlayable
struct AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationScriptPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.AnimationState
struct AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE  : public TrackedReference_tF35FF4FB6E89ACD81C24469FAF0CA6FFF29262A2
{
};

// UnityEngine.Animations.AnimatorControllerPlayable
struct AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimatorControllerPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// System.ApplicationException
struct ApplicationException_tA744BED4E90266BD255285CD4CF909BAB3EE811A  : public Exception_t
{
};

// UnityEngine.AssetBundle
struct AssetBundle_tB38418819A49060CD738CB21541649340F082943  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t73B8714B9459A01540E091C3770A408E67188CF6  : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C
{
};
// Native definition for P/Invoke marshalling of UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t73B8714B9459A01540E091C3770A408E67188CF6_marshaled_pinvoke : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t73B8714B9459A01540E091C3770A408E67188CF6_marshaled_com : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_com
{
};

// UnityEngine.AssetBundleRecompressOperation
struct AssetBundleRecompressOperation_tFDA1FB5AE1E072FC6CAC1CF0064C13D77F87CDDE  : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C
{
};
// Native definition for P/Invoke marshalling of UnityEngine.AssetBundleRecompressOperation
struct AssetBundleRecompressOperation_tFDA1FB5AE1E072FC6CAC1CF0064C13D77F87CDDE_marshaled_pinvoke : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.AssetBundleRecompressOperation
struct AssetBundleRecompressOperation_tFDA1FB5AE1E072FC6CAC1CF0064C13D77F87CDDE_marshaled_com : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_com
{
};

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06 
{
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder::m_builder
	AsyncTaskMethodBuilder_1_tE88892A6B2F97B5D44B7C3EE2DBEED85743412AC ___m_builder_1;
};
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06_marshaled_pinvoke
{
	AsyncTaskMethodBuilder_1_tE88892A6B2F97B5D44B7C3EE2DBEED85743412AC ___m_builder_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06_marshaled_com
{
	AsyncTaskMethodBuilder_1_tE88892A6B2F97B5D44B7C3EE2DBEED85743412AC ___m_builder_1;
};

// UnityEngine.AudioClip
struct AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t3396D9613664F0AFF65FB91018FD0F901CC16F1E* ___m_PCMReaderCallback_4;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t8D7135A2FB40647CAEC93F5254AD59E18DEB6072* ___m_PCMSetPositionCallback_5;
};

// UnityEngine.Audio.AudioClipPlayable
struct AudioClipPlayable_tD4B758E68CAE03CB0CD31F90C8A3E603B97143A0 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioClipPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Audio.AudioMixer
struct AudioMixer_tE2E8D79241711CDF9AB428C7FB96A35D80E40B04  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Audio.AudioMixerPlayable
struct AudioMixerPlayable_t6AADDF0C53DF1B4C17969EC24B3B4E4975F3A56C 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioMixerPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Audio.AudioPlayableOutput
struct AudioPlayableOutput_tC3DFF8095F429D90129A367EAB98A24F6D6ADF20 
{
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Audio.AudioPlayableOutput::m_Handle
	PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883 ___m_Handle_0;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Networking.DownloadHandlerAssetBundle
struct DownloadHandlerAssetBundle_tCD9D8BA067912469251677D16DFCADD13CAD510C  : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB
{
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerAssetBundle
struct DownloadHandlerAssetBundle_tCD9D8BA067912469251677D16DFCADD13CAD510C_marshaled_pinvoke : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerAssetBundle
struct DownloadHandlerAssetBundle_tCD9D8BA067912469251677D16DFCADD13CAD510C_marshaled_com : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com
{
};

// UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974  : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB
{
	// Unity.Collections.NativeArray`1<System.Byte> UnityEngine.Networking.DownloadHandlerBuffer::m_NativeData
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_NativeData_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974_marshaled_pinvoke : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke
{
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_NativeData_1;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974_marshaled_com : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com
{
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_NativeData_1;
};

// UnityEngine.Networking.DownloadHandlerFile
struct DownloadHandlerFile_tD1342A7B8173C9ECC7B3BB9E1A7631D7AEFBD902  : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB
{
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerFile
struct DownloadHandlerFile_tD1342A7B8173C9ECC7B3BB9E1A7631D7AEFBD902_marshaled_pinvoke : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerFile
struct DownloadHandlerFile_tD1342A7B8173C9ECC7B3BB9E1A7631D7AEFBD902_marshaled_com : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com
{
};

// UnityEngine.Networking.DownloadHandlerScript
struct DownloadHandlerScript_t42FD7363F738391BB1EA2552FF18F9FA7C0EE38B  : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB
{
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerScript
struct DownloadHandlerScript_t42FD7363F738391BB1EA2552FF18F9FA7C0EE38B_marshaled_pinvoke : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerScript
struct DownloadHandlerScript_t42FD7363F738391BB1EA2552FF18F9FA7C0EE38B_marshaled_com : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com
{
};

// UnityEngine.ExitGUIException
struct ExitGUIException_tFF2EEEBACD9E5684D6112478EEF754B74D154549  : public Exception_t
{
};

// UnityEngine.Font
struct Font_tC95270EA3198038970422D78B74A7F2E218A96B6  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
	// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::m_FontTextureRebuildCallback
	FontTextureRebuildCallback_t76D5E172DF8AA57E67763D453AAC40F0961D09B1* ___m_FontTextureRebuildCallback_5;
};

// UnityEngine.GUILayoutGroup
struct GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D  : public GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F
{
	// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry> UnityEngine.GUILayoutGroup::entries
	List_1_tA5BCD116CC751A5F35C7D3D7B96DC3A5D22B9C82* ___entries_11;
	// System.Boolean UnityEngine.GUILayoutGroup::isVertical
	bool ___isVertical_12;
	// System.Boolean UnityEngine.GUILayoutGroup::resetCoords
	bool ___resetCoords_13;
	// System.Single UnityEngine.GUILayoutGroup::spacing
	float ___spacing_14;
	// System.Boolean UnityEngine.GUILayoutGroup::sameSize
	bool ___sameSize_15;
	// System.Boolean UnityEngine.GUILayoutGroup::isWindow
	bool ___isWindow_16;
	// System.Int32 UnityEngine.GUILayoutGroup::windowID
	int32_t ___windowID_17;
	// System.Int32 UnityEngine.GUILayoutGroup::m_Cursor
	int32_t ___m_Cursor_18;
	// System.Int32 UnityEngine.GUILayoutGroup::m_StretchableCountX
	int32_t ___m_StretchableCountX_19;
	// System.Int32 UnityEngine.GUILayoutGroup::m_StretchableCountY
	int32_t ___m_StretchableCountY_20;
	// System.Boolean UnityEngine.GUILayoutGroup::m_UserSpecifiedWidth
	bool ___m_UserSpecifiedWidth_21;
	// System.Boolean UnityEngine.GUILayoutGroup::m_UserSpecifiedHeight
	bool ___m_UserSpecifiedHeight_22;
	// System.Single UnityEngine.GUILayoutGroup::m_ChildMinWidth
	float ___m_ChildMinWidth_23;
	// System.Single UnityEngine.GUILayoutGroup::m_ChildMaxWidth
	float ___m_ChildMaxWidth_24;
	// System.Single UnityEngine.GUILayoutGroup::m_ChildMinHeight
	float ___m_ChildMinHeight_25;
	// System.Single UnityEngine.GUILayoutGroup::m_ChildMaxHeight
	float ___m_ChildMaxHeight_26;
	// System.Int32 UnityEngine.GUILayoutGroup::m_MarginLeft
	int32_t ___m_MarginLeft_27;
	// System.Int32 UnityEngine.GUILayoutGroup::m_MarginRight
	int32_t ___m_MarginRight_28;
	// System.Int32 UnityEngine.GUILayoutGroup::m_MarginTop
	int32_t ___m_MarginTop_29;
	// System.Int32 UnityEngine.GUILayoutGroup::m_MarginBottom
	int32_t ___m_MarginBottom_30;
};

// UnityEngine.GUIStyle
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580  : public RuntimeObject
{
	// System.IntPtr UnityEngine.GUIStyle::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Normal
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_Normal_1;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Hover
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_Hover_2;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Active
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_Active_3;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Focused
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_Focused_4;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnNormal
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_OnNormal_5;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnHover
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_OnHover_6;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnActive
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_OnActive_7;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnFocused
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_OnFocused_8;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Border
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5* ___m_Border_9;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Padding
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5* ___m_Padding_10;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Margin
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5* ___m_Margin_11;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Overflow
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5* ___m_Overflow_12;
	// System.String UnityEngine.GUIStyle::m_Name
	String_t* ___m_Name_13;
};
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyle
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_Normal_1;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_Hover_2;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_Active_3;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_Focused_4;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_OnNormal_5;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_OnHover_6;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_OnActive_7;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_OnFocused_8;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_pinvoke ___m_Border_9;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_pinvoke ___m_Padding_10;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_pinvoke ___m_Margin_11;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_pinvoke ___m_Overflow_12;
	char* ___m_Name_13;
};
// Native definition for COM marshalling of UnityEngine.GUIStyle
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_Normal_1;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_Hover_2;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_Active_3;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_Focused_4;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_OnNormal_5;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_OnHover_6;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_OnActive_7;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_OnFocused_8;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_com* ___m_Border_9;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_com* ___m_Padding_10;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_com* ___m_Margin_11;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_com* ___m_Overflow_12;
	Il2CppChar* ___m_Name_13;
};

// UnityEngine.GUIWordWrapSizer
struct GUIWordWrapSizer_t915CE8588C443243630F7737947CA87B42965A36  : public GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F
{
	// UnityEngine.GUIContent UnityEngine.GUIWordWrapSizer::m_Content
	GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2* ___m_Content_11;
	// System.Single UnityEngine.GUIWordWrapSizer::m_ForcedMinHeight
	float ___m_ForcedMinHeight_12;
	// System.Single UnityEngine.GUIWordWrapSizer::m_ForcedMaxHeight
	float ___m_ForcedMaxHeight_13;
};

// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord
struct GlyphPairAdjustmentRecord_t6E4295094D349DBF22BC59116FBC8F22EA55420E 
{
	// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord::m_FirstAdjustmentRecord
	GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7 ___m_FirstAdjustmentRecord_0;
	// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord::m_SecondAdjustmentRecord
	GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7 ___m_SecondAdjustmentRecord_1;
	// UnityEngine.TextCore.LowLevel.FontFeatureLookupFlags UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord::m_FeatureLookupFlags
	int32_t ___m_FeatureLookupFlags_2;
};

// UnityEngine.HumanBone
struct HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8 
{
	// System.String UnityEngine.HumanBone::m_BoneName
	String_t* ___m_BoneName_0;
	// System.String UnityEngine.HumanBone::m_HumanName
	String_t* ___m_HumanName_1;
	// UnityEngine.HumanLimit UnityEngine.HumanBone::limit
	HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E ___limit_2;
};
// Native definition for P/Invoke marshalling of UnityEngine.HumanBone
struct HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8_marshaled_pinvoke
{
	char* ___m_BoneName_0;
	char* ___m_HumanName_1;
	HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E ___limit_2;
};
// Native definition for COM marshalling of UnityEngine.HumanBone
struct HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8_marshaled_com
{
	Il2CppChar* ___m_BoneName_0;
	Il2CppChar* ___m_HumanName_1;
	HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E ___limit_2;
};

// UnityEngine.Motion
struct Motion_tBCD49FBF5608AD21FC03B63C8182FABCEF2707AC  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// UnityEngine.ResourceRequest
struct ResourceRequest_tE6953FBA45EAAEFE866C635B9E7852044E62D868  : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C
{
	// System.String UnityEngine.ResourceRequest::m_Path
	String_t* ___m_Path_2;
	// System.Type UnityEngine.ResourceRequest::m_Type
	Type_t* ___m_Type_3;
};
// Native definition for P/Invoke marshalling of UnityEngine.ResourceRequest
struct ResourceRequest_tE6953FBA45EAAEFE866C635B9E7852044E62D868_marshaled_pinvoke : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_pinvoke
{
	char* ___m_Path_2;
	Type_t* ___m_Type_3;
};
// Native definition for COM marshalling of UnityEngine.ResourceRequest
struct ResourceRequest_tE6953FBA45EAAEFE866C635B9E7852044E62D868_marshaled_com : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_com
{
	Il2CppChar* ___m_Path_2;
	Type_t* ___m_Type_3;
};

// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t6F7C753402B42EC23C163099CF935C5E0D7A7254  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_pinvoke : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_com : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC  : public RuntimeObject
{
	// System.IntPtr UnityEngine.TextGenerator::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.String UnityEngine.TextGenerator::m_LastString
	String_t* ___m_LastString_1;
	// UnityEngine.TextGenerationSettings UnityEngine.TextGenerator::m_LastSettings
	TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3 ___m_LastSettings_2;
	// System.Boolean UnityEngine.TextGenerator::m_HasGenerated
	bool ___m_HasGenerated_3;
	// UnityEngine.TextGenerationError UnityEngine.TextGenerator::m_LastValid
	int32_t ___m_LastValid_4;
	// System.Collections.Generic.List`1<UnityEngine.UIVertex> UnityEngine.TextGenerator::m_Verts
	List_1_t09F8990ACE8783E311B473B0090859BA9C00FC2A* ___m_Verts_5;
	// System.Collections.Generic.List`1<UnityEngine.UICharInfo> UnityEngine.TextGenerator::m_Characters
	List_1_t67A1600A303BB89506DFD21B59687088B7E0675B* ___m_Characters_6;
	// System.Collections.Generic.List`1<UnityEngine.UILineInfo> UnityEngine.TextGenerator::m_Lines
	List_1_t9209B29AC606399207E97BDCD817DEA5B6C63CA5* ___m_Lines_7;
	// System.Boolean UnityEngine.TextGenerator::m_CachedVerts
	bool ___m_CachedVerts_8;
	// System.Boolean UnityEngine.TextGenerator::m_CachedCharacters
	bool ___m_CachedCharacters_9;
	// System.Boolean UnityEngine.TextGenerator::m_CachedLines
	bool ___m_CachedLines_10;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	char* ___m_LastString_1;
	TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3_marshaled_pinvoke ___m_LastSettings_2;
	int32_t ___m_HasGenerated_3;
	int32_t ___m_LastValid_4;
	List_1_t09F8990ACE8783E311B473B0090859BA9C00FC2A* ___m_Verts_5;
	List_1_t67A1600A303BB89506DFD21B59687088B7E0675B* ___m_Characters_6;
	List_1_t9209B29AC606399207E97BDCD817DEA5B6C63CA5* ___m_Lines_7;
	int32_t ___m_CachedVerts_8;
	int32_t ___m_CachedCharacters_9;
	int32_t ___m_CachedLines_10;
};
// Native definition for COM marshalling of UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppChar* ___m_LastString_1;
	TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3_marshaled_com ___m_LastSettings_2;
	int32_t ___m_HasGenerated_3;
	int32_t ___m_LastValid_4;
	List_1_t09F8990ACE8783E311B473B0090859BA9C00FC2A* ___m_Verts_5;
	List_1_t67A1600A303BB89506DFD21B59687088B7E0675B* ___m_Characters_6;
	List_1_t9209B29AC606399207E97BDCD817DEA5B6C63CA5* ___m_Lines_7;
	int32_t ___m_CachedVerts_8;
	int32_t ___m_CachedCharacters_9;
	int32_t ___m_CachedLines_10;
};

// TouchData
struct TouchData_tBFC18681FF77A826E4D5823C44B9F68E1A823A18  : public RuntimeObject
{
	// UnityEngine.Touch TouchData::touch
	Touch_t03E51455ED508492B3F278903A0114FA0E87B417 ___touch_0;
	// System.Int64 TouchData::timeStamp
	int64_t ___timeStamp_1;
};

// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.UnityWebRequest::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::m_DownloadHandler
	DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB* ___m_DownloadHandler_1;
	// UnityEngine.Networking.UploadHandler UnityEngine.Networking.UnityWebRequest::m_UploadHandler
	UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6* ___m_UploadHandler_2;
	// UnityEngine.Networking.CertificateHandler UnityEngine.Networking.UnityWebRequest::m_CertificateHandler
	CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804* ___m_CertificateHandler_3;
	// System.Uri UnityEngine.Networking.UnityWebRequest::m_Uri
	Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E* ___m_Uri_4;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeCertificateHandlerOnDispose>k__BackingField
	bool ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeDownloadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeUploadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke ___m_DownloadHandler_1;
	UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_pinvoke ___m_UploadHandler_2;
	CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_pinvoke ___m_CertificateHandler_3;
	Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E* ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_com
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com* ___m_DownloadHandler_1;
	UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_com* ___m_UploadHandler_2;
	CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_com* ___m_CertificateHandler_3;
	Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E* ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};

// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C  : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C
{
	// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAsyncOperation::<webRequest>k__BackingField
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* ___U3CwebRequestU3Ek__BackingField_2;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C_marshaled_pinvoke : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_pinvoke
{
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_pinvoke* ___U3CwebRequestU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C_marshaled_com : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_com
{
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_com* ___U3CwebRequestU3Ek__BackingField_2;
};

// UnityEngine.UIElements.UIR.Utility
struct Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD  : public RuntimeObject
{
};

// c
struct c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57  : public RuntimeObject
{
	// c/e c::b
	e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777 ___b_1;
	// c/b c::c
	b_t377CCBB7B97AADE524489D5766C28A547BBAB31B ___c_2;
	// c/d c::d
	d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2 ___d_3;
	// System.IO.BinaryWriter c::e
	BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* ___e_4;
};

// CloudSaveSystem/<Awake>d__1
struct U3CAwakeU3Ed__1_t0A8F4279E9A8075D9D3064405CFEB1DC38A6944A 
{
	// System.Int32 CloudSaveSystem/<Awake>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncVoidMethodBuilder CloudSaveSystem/<Awake>d__1::<>t__builder
	AsyncVoidMethodBuilder_t253E37B63E7E7B504878AE6563347C147F98EF2D ___U3CU3Et__builder_1;
	// CloudSaveSystem CloudSaveSystem/<Awake>d__1::<>4__this
	CloudSaveSystem_t419542980442B82C4BB089FE4FFEDD48855C1AA6* ___U3CU3E4__this_2;
};

// ComparePlayerDataPlanel/<SaveCloundData>d__9
struct U3CSaveCloundDataU3Ed__9_t82051DDCDC50EC3A559ABD9C5B2FBDDD0156DCB6 
{
	// System.Int32 ComparePlayerDataPlanel/<SaveCloundData>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncVoidMethodBuilder ComparePlayerDataPlanel/<SaveCloundData>d__9::<>t__builder
	AsyncVoidMethodBuilder_t253E37B63E7E7B504878AE6563347C147F98EF2D ___U3CU3Et__builder_1;
	// ComparePlayerDataPlanel ComparePlayerDataPlanel/<SaveCloundData>d__9::<>4__this
	ComparePlayerDataPlanel_t2A3BBF964761B874A51BB23F98AFD96D7C7D48B6* ___U3CU3E4__this_2;
	// System.Runtime.CompilerServices.TaskAwaiter ComparePlayerDataPlanel/<SaveCloundData>d__9::<>u__1
	TaskAwaiter_t9B661AC8C2EFA6BAB94C77BB24A5DDA82D61F833 ___U3CU3Eu__1_3;
};

// UnityEngine.ParticleSystem/EmitParams
struct EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0 
{
	// UnityEngine.ParticleSystem/Particle UnityEngine.ParticleSystem/EmitParams::m_Particle
	Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D ___m_Particle_0;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_PositionSet
	bool ___m_PositionSet_1;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_VelocitySet
	bool ___m_VelocitySet_2;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_AxisOfRotationSet
	bool ___m_AxisOfRotationSet_3;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_RotationSet
	bool ___m_RotationSet_4;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_AngularVelocitySet
	bool ___m_AngularVelocitySet_5;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_StartSizeSet
	bool ___m_StartSizeSet_6;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_StartColorSet
	bool ___m_StartColorSet_7;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_RandomSeedSet
	bool ___m_RandomSeedSet_8;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_StartLifetimeSet
	bool ___m_StartLifetimeSet_9;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_MeshIndexSet
	bool ___m_MeshIndexSet_10;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_ApplyShapeToPosition
	bool ___m_ApplyShapeToPosition_11;
};
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/EmitParams
struct EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0_marshaled_pinvoke
{
	Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D ___m_Particle_0;
	int32_t ___m_PositionSet_1;
	int32_t ___m_VelocitySet_2;
	int32_t ___m_AxisOfRotationSet_3;
	int32_t ___m_RotationSet_4;
	int32_t ___m_AngularVelocitySet_5;
	int32_t ___m_StartSizeSet_6;
	int32_t ___m_StartColorSet_7;
	int32_t ___m_RandomSeedSet_8;
	int32_t ___m_StartLifetimeSet_9;
	int32_t ___m_MeshIndexSet_10;
	int32_t ___m_ApplyShapeToPosition_11;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/EmitParams
struct EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0_marshaled_com
{
	Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D ___m_Particle_0;
	int32_t ___m_PositionSet_1;
	int32_t ___m_VelocitySet_2;
	int32_t ___m_AxisOfRotationSet_3;
	int32_t ___m_RotationSet_4;
	int32_t ___m_AngularVelocitySet_5;
	int32_t ___m_StartSizeSet_6;
	int32_t ___m_StartColorSet_7;
	int32_t ___m_RandomSeedSet_8;
	int32_t ___m_StartLifetimeSet_9;
	int32_t ___m_MeshIndexSet_10;
	int32_t ___m_ApplyShapeToPosition_11;
};

// SystemGameManager/<SaveCloundData>d__31
struct U3CSaveCloundDataU3Ed__31_tE45FED7C47BFA6B90878786427B102C1547FEF46 
{
	// System.Int32 SystemGameManager/<SaveCloundData>d__31::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncVoidMethodBuilder SystemGameManager/<SaveCloundData>d__31::<>t__builder
	AsyncVoidMethodBuilder_t253E37B63E7E7B504878AE6563347C147F98EF2D ___U3CU3Et__builder_1;
	// System.Runtime.CompilerServices.TaskAwaiter SystemGameManager/<SaveCloundData>d__31::<>u__1
	TaskAwaiter_t9B661AC8C2EFA6BAB94C77BB24A5DDA82D61F833 ___U3CU3Eu__1_2;
};

// UnityEngine.AnimationClip
struct AnimationClip_t00BD2F131D308A4AD2C6B0BF66644FC25FECE712  : public Motion_tBCD49FBF5608AD21FC03B63C8182FABCEF2707AC
{
};

// UnityEngine.AnimatorOverrideController
struct AnimatorOverrideController_tF78BD58B30BB0D767E7A96F8428EA66F2DFD5493  : public RuntimeAnimatorController_t6F7C753402B42EC23C163099CF935C5E0D7A7254
{
	// UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback UnityEngine.AnimatorOverrideController::OnOverrideControllerDirty
	OnOverrideControllerDirtyCallback_tDC67F7661A27502AD804BDE0B696955AFD4A44D5* ___OnOverrideControllerDirty_4;
};

// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_tED9F5504E75ED1BCFF8DA9B51F5C7356617E6621  : public ResourceRequest_tE6953FBA45EAAEFE866C635B9E7852044E62D868
{
};
// Native definition for P/Invoke marshalling of UnityEngine.AssetBundleRequest
struct AssetBundleRequest_tED9F5504E75ED1BCFF8DA9B51F5C7356617E6621_marshaled_pinvoke : public ResourceRequest_tE6953FBA45EAAEFE866C635B9E7852044E62D868_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.AssetBundleRequest
struct AssetBundleRequest_tED9F5504E75ED1BCFF8DA9B51F5C7356617E6621_marshaled_com : public ResourceRequest_tE6953FBA45EAAEFE866C635B9E7852044E62D868_marshaled_com
{
};

// UnityEngine.Yoga.BaselineFunction
struct BaselineFunction_t13AFADEF52F63320B2159C237635948AEB801679  : public MulticastDelegate_t
{
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// BuildinFileManifest
struct BuildinFileManifest_tC1B882410A653ED35F30219FCA0359884F2935C1  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// System.Collections.Generic.List`1<System.String> BuildinFileManifest::BuildinFiles
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___BuildinFiles_4;
};

// UnityEngine.CanvasRenderer
struct CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
	// System.Boolean UnityEngine.CanvasRenderer::<isMask>k__BackingField
	bool ___U3CisMaskU3Ek__BackingField_4;
};

// UnityEngine.Collider
struct Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// LitJson.ExporterFunc
struct ExporterFunc_t0E0FD6EE483CD6380339D2D7AC616079B1D5ED8A  : public MulticastDelegate_t
{
};

// UnityEngine.GUIScrollGroup
struct GUIScrollGroup_t4D7230655A7D01ED9BD95916958E34AF09B21FE5  : public GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D
{
	// System.Single UnityEngine.GUIScrollGroup::calcMinWidth
	float ___calcMinWidth_32;
	// System.Single UnityEngine.GUIScrollGroup::calcMaxWidth
	float ___calcMaxWidth_33;
	// System.Single UnityEngine.GUIScrollGroup::calcMinHeight
	float ___calcMinHeight_34;
	// System.Single UnityEngine.GUIScrollGroup::calcMaxHeight
	float ___calcMaxHeight_35;
	// System.Single UnityEngine.GUIScrollGroup::clientWidth
	float ___clientWidth_36;
	// System.Single UnityEngine.GUIScrollGroup::clientHeight
	float ___clientHeight_37;
	// System.Boolean UnityEngine.GUIScrollGroup::allowHorizontalScroll
	bool ___allowHorizontalScroll_38;
	// System.Boolean UnityEngine.GUIScrollGroup::allowVerticalScroll
	bool ___allowVerticalScroll_39;
	// System.Boolean UnityEngine.GUIScrollGroup::needsHorizontalScrollbar
	bool ___needsHorizontalScrollbar_40;
	// System.Boolean UnityEngine.GUIScrollGroup::needsVerticalScrollbar
	bool ___needsVerticalScrollbar_41;
	// UnityEngine.GUIStyle UnityEngine.GUIScrollGroup::horizontalScrollbar
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___horizontalScrollbar_42;
	// UnityEngine.GUIStyle UnityEngine.GUIScrollGroup::verticalScrollbar
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___verticalScrollbar_43;
};

// UnityEngine.GUISkin
struct GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// UnityEngine.Font UnityEngine.GUISkin::m_Font
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___m_Font_4;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_box
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_box_5;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_button
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_button_6;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_toggle
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_toggle_7;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_label
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_label_8;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textField
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_textField_9;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textArea
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_textArea_10;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_window
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_window_11;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSlider
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalSlider_12;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSliderThumb
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalSliderThumb_13;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSliderThumbExtent
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalSliderThumbExtent_14;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSlider
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalSlider_15;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSliderThumb
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalSliderThumb_16;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSliderThumbExtent
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalSliderThumbExtent_17;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_SliderMixed
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_SliderMixed_18;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbar
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalScrollbar_19;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarThumb
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalScrollbarThumb_20;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarLeftButton
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalScrollbarLeftButton_21;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarRightButton
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalScrollbarRightButton_22;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbar
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalScrollbar_23;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarThumb
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalScrollbarThumb_24;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarUpButton
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalScrollbarUpButton_25;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarDownButton
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalScrollbarDownButton_26;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_ScrollView
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_ScrollView_27;
	// UnityEngine.GUIStyle[] UnityEngine.GUISkin::m_CustomStyles
	GUIStyleU5BU5D_t1BA4BCF4D4D32DF07E9B84F1750D964DF33B0FEC* ___m_CustomStyles_28;
	// UnityEngine.GUISettings UnityEngine.GUISkin::m_Settings
	GUISettings_tF2CA7E8B9F62F1FC013BFF053B5FA2709EBA3847* ___m_Settings_29;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle> UnityEngine.GUISkin::m_Styles
	Dictionary_2_tEFC8016EC28460E6CE058A5F413FAB656883AA5F* ___m_Styles_31;
};

// LitJson.ImporterFunc
struct ImporterFunc_t3D41CCB293D2761DC6862831F8BC45D580EB6AA5  : public MulticastDelegate_t
{
};

// System.InvalidOperationException
struct InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// LitJson.JsonException
struct JsonException_tA6D9726FBCE77AF0E9B652ABE7C01E4F1E55FA76  : public ApplicationException_tA744BED4E90266BD255285CD4CF909BAB3EE811A
{
};

// UnityEngine.Yoga.Logger
struct Logger_t092B1218ED93DD47180692D5761559B2054234A0  : public MulticastDelegate_t
{
};

// UnityEngine.Yoga.MeasureFunction
struct MeasureFunction_t60EBED1328F5328D4FA7E26335967E59E73B4D09  : public MulticastDelegate_t
{
};

// UnityEngine.ParticleSystem
struct ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Renderer
struct Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// ResourceProfile
struct ResourceProfile_tA69CC048C726FE55EC1932C33125A2AD2899E932  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// System.String ResourceProfile::configurName
	String_t* ___configurName_4;
};

// UnityEngine.Rigidbody
struct Rigidbody_t268697F5A994213ED97393309870968BC1C7393C  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.StateMachineBehaviour
struct StateMachineBehaviour_t59C5685227B06222F5AF7027E2DA530AB99AFDF7  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
};

// LitJson.WrapperFactory
struct WrapperFactory_tB4894CC7D3A41BE3FF6BA079B4490DF5FC5C7BDD  : public MulticastDelegate_t
{
};

// YooAsset.YooAssetSettings
struct YooAssetSettings_tAA143F140144A8EE80D18FB10D26A93A841E6EE4  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// System.String YooAsset.YooAssetSettings::ManifestFileName
	String_t* ___ManifestFileName_4;
};

// UnityEngine.Analytics.AnalyticsSessionInfo/IdentityTokenChanged
struct IdentityTokenChanged_tE8CB0DAB5F6E640A847803F582E6CB6237742395  : public MulticastDelegate_t
{
};

// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct SessionStateChanged_t1180FB66E702B635CAD9316DC661D931277B2A0C  : public MulticastDelegate_t
{
};

// UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback
struct OnOverrideControllerDirtyCallback_tDC67F7661A27502AD804BDE0B696955AFD4A44D5  : public MulticastDelegate_t
{
};

// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t3396D9613664F0AFF65FB91018FD0F901CC16F1E  : public MulticastDelegate_t
{
};

// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t8D7135A2FB40647CAEC93F5254AD59E18DEB6072  : public MulticastDelegate_t
{
};

// UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler
struct SampleFramesHandler_tFE84FF9BBCEFB880D46227188F375BEF680AAA30  : public MulticastDelegate_t
{
};

// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_tE071B0CBA3B3A77D3E41F5FCB65B4017885B3177  : public MulticastDelegate_t
{
};

// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC  : public MulticastDelegate_t
{
};

// CloudSaveSystem/<ForceSaveSingleData>d__4
struct U3CForceSaveSingleDataU3Ed__4_t8CA291371724E60F1D66E9C1B4A7BA6BF74FB1BB 
{
	// System.Int32 CloudSaveSystem/<ForceSaveSingleData>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder CloudSaveSystem/<ForceSaveSingleData>d__4::<>t__builder
	AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06 ___U3CU3Et__builder_1;
};

// CloudSaveSystem/<ListAllKeys>d__3
struct U3CListAllKeysU3Ed__3_t17C0A4E525733224D96B7ABE2F6986C077429D0B 
{
	// System.Int32 CloudSaveSystem/<ListAllKeys>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder CloudSaveSystem/<ListAllKeys>d__3::<>t__builder
	AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06 ___U3CU3Et__builder_1;
};

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10
struct U3CAsyncWaitForCompletionU3Ed__10_tC84049D47EAD23B14384BDEF646D532785ECBF0E 
{
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10::<>t__builder
	AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06 ___U3CU3Et__builder_1;
	// DG.Tweening.Tween DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_2;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10::<>u__1
	YieldAwaiter_t5F0A81DC85227C01FFC38D53139B5C19D920B52A ___U3CU3Eu__1_3;
};

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13
struct U3CAsyncWaitForElapsedLoopsU3Ed__13_tC7B431C2393096ACD1A6BA0EAFEA84EE62DAF825 
{
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::<>t__builder
	AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06 ___U3CU3Et__builder_1;
	// DG.Tweening.Tween DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_2;
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::elapsedLoops
	int32_t ___elapsedLoops_3;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::<>u__1
	YieldAwaiter_t5F0A81DC85227C01FFC38D53139B5C19D920B52A ___U3CU3Eu__1_4;
};

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12
struct U3CAsyncWaitForKillU3Ed__12_t6EA9E2438625431E39D01AB7EEBB4501D6B5E54E 
{
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12::<>t__builder
	AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06 ___U3CU3Et__builder_1;
	// DG.Tweening.Tween DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_2;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12::<>u__1
	YieldAwaiter_t5F0A81DC85227C01FFC38D53139B5C19D920B52A ___U3CU3Eu__1_3;
};

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14
struct U3CAsyncWaitForPositionU3Ed__14_tA6006769EC53BBEBA0665ECA79096B606FDA8A4A 
{
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::<>t__builder
	AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06 ___U3CU3Et__builder_1;
	// DG.Tweening.Tween DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_2;
	// System.Single DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::position
	float ___position_3;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::<>u__1
	YieldAwaiter_t5F0A81DC85227C01FFC38D53139B5C19D920B52A ___U3CU3Eu__1_4;
};

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11
struct U3CAsyncWaitForRewindU3Ed__11_tC8D7C20224797881A037D09DA8079ECCC3E518FE 
{
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11::<>t__builder
	AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06 ___U3CU3Et__builder_1;
	// DG.Tweening.Tween DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_2;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11::<>u__1
	YieldAwaiter_t5F0A81DC85227C01FFC38D53139B5C19D920B52A ___U3CU3Eu__1_3;
};

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15
struct U3CAsyncWaitForStartU3Ed__15_tB4B1CE199FE822B67BCF87301159986D9D50961B 
{
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15::<>t__builder
	AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06 ___U3CU3Et__builder_1;
	// DG.Tweening.Tween DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_2;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15::<>u__1
	YieldAwaiter_t5F0A81DC85227C01FFC38D53139B5C19D920B52A ___U3CU3Eu__1_3;
};

// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t76D5E172DF8AA57E67763D453AAC40F0961D09B1  : public MulticastDelegate_t
{
};

// UnityEngine.GUI/WindowFunction
struct WindowFunction_t0067B6F174FD5BEC3E869A38C2319BA8EE85D550  : public MulticastDelegate_t
{
};

// UnityEngine.GUISkin/SkinChangedDelegate
struct SkinChangedDelegate_tA6D456E853D58AD2EF8A599F543C7E5BA8E94B98  : public MulticastDelegate_t
{
};

// LitJson.Lexer/StateHandler
struct StateHandler_t96934EB5CE1BA6A1DE19972E93F1CA6D31CD3603  : public MulticastDelegate_t
{
};

// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_tB0D5A5BA322FE093894992C29DCF51E7E12579C4  : public MulticastDelegate_t
{
};

// UnityEngine.Animator
struct Animator_t8A52E42AE54F76681838FE9E632683EF3952E883  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.AudioBehaviour
struct AudioBehaviour_t2DC0BEF7B020C952F3D2DA5AAAC88501C7EEB941  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.BoxCollider
struct BoxCollider_tFA5D239388334D6DE0B8FFDAD6825C5B03786E23  : public Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76
{
};

// UnityEngine.Canvas
struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.CanvasGroup
struct CanvasGroup_t048C1461B14628CFAEBE6E7353093ADB04EBC094  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.CapsuleCollider
struct CapsuleCollider_t3A1671C74F0836ABEF5D01A7470B5B2BE290A808  : public Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76
{
};

// UnityEngine.CharacterController
struct CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A  : public Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76
{
};

// UnityEngine.Collider2D
struct Collider2D_t6A17BA7734600EF3F26588E9ED903617D5B8EB52  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// FoodMenuConfigur
struct FoodMenuConfigur_t69F14619F474736AFCB21E169581D223F1D08F24  : public ResourceProfile_tA69CC048C726FE55EC1932C33125A2AD2899E932
{
	// System.Collections.Generic.List`1<FoodMenuAsset> FoodMenuConfigur::foodMenuAssetList
	List_1_t4F1D474EA64688149310819BB63A0150C3F2E05A* ___foodMenuAssetList_5;
};

// UnityEngine.GridLayout
struct GridLayout_tAD661B1E1E57C16BE21C8C13432EA04FE1F0418B  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// GuestConfigur
struct GuestConfigur_t6D87BAFAA5DF6A5CA5DB24EF8EAE6ADF069D7217  : public ResourceProfile_tA69CC048C726FE55EC1932C33125A2AD2899E932
{
	// System.Collections.Generic.List`1<GuestData> GuestConfigur::guestDataLlis
	List_1_t2876763590C4FEA8FB181927604F91536995879F* ___guestDataLlis_5;
};

// HeadAssetConfigur
struct HeadAssetConfigur_t23598354D1904A8F584FBCAE35A9860E627E301A  : public ResourceProfile_tA69CC048C726FE55EC1932C33125A2AD2899E932
{
	// System.Collections.Generic.List`1<HeadAssetData> HeadAssetConfigur::HeadAssetDataList
	List_1_t8F88710524483B00806CC60AFF4A3236AF0B9C78* ___HeadAssetDataList_5;
};

// UnityEngine.Joint2D
struct Joint2D_tFA088656425446CDA98555EC8A0E5FE25945F843  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// MainFoodConfigur
struct MainFoodConfigur_tD086692154785F49C555D4B1BAAD460C3DAD7C0E  : public ResourceProfile_tA69CC048C726FE55EC1932C33125A2AD2899E932
{
	// System.Collections.Generic.List`1<MainFoodCompnentAssets> MainFoodConfigur::foodDataList
	List_1_t3363445FC8694C84D8D8A916236F50B706E93E42* ___foodDataList_5;
};

// UnityEngine.MeshCollider
struct MeshCollider_tB525E4DDE383252364ED0BDD32CF2B53914EE455  : public Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// System.ObjectDisposedException
struct ObjectDisposedException_tC5FB29E8E980E2010A2F6A5B9B791089419F89EB  : public InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB
{
	// System.String System.ObjectDisposedException::_objectName
	String_t* ____objectName_18;
};

// OtherFoodConfigur
struct OtherFoodConfigur_tA6EB87E684A6B737D8558213CED42AB67114DEAC  : public ResourceProfile_tA69CC048C726FE55EC1932C33125A2AD2899E932
{
	// System.Collections.Generic.List`1<OtheFoodCompnentAssets> OtherFoodConfigur::OtheFoodCompnentList
	List_1_t990D89EFEC6E608F0D9D16CE57058E1671FC610B* ___OtheFoodCompnentList_5;
};

// UnityEngine.ParticleSystemRenderer
struct ParticleSystemRenderer_t576C271A363A738A6C576D4C6AEFB3B5B23E46C4  : public Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF
{
};

// RawFoodAssetsConfigur
struct RawFoodAssetsConfigur_t83A906F04B0587BF55C52F61E377F48EF92683E3  : public ResourceProfile_tA69CC048C726FE55EC1932C33125A2AD2899E932
{
	// System.Collections.Generic.List`1<RawFoodData> RawFoodAssetsConfigur::rawFoodDataList
	List_1_t1A705E9D66D0956C99CA98474E7D4C64B0B5CDBD* ___rawFoodDataList_5;
};

// RestaurantAssetConfiger
struct RestaurantAssetConfiger_tF6D49FD5F5A651D5FC4EBE0BD12CDCDFD08396AC  : public ResourceProfile_tA69CC048C726FE55EC1932C33125A2AD2899E932
{
	// System.Collections.Generic.List`1<RestaurantData> RestaurantAssetConfiger::RestaurantDataList
	List_1_t6D46605B0341B96D804BAD55EB4985B0826CA52A* ___RestaurantDataList_5;
};

// ShoppItemConfigur
struct ShoppItemConfigur_tB9BC6996C5A7913E1EEF4282BB2E238538D9F82C  : public ResourceProfile_tA69CC048C726FE55EC1932C33125A2AD2899E932
{
	// System.Collections.Generic.List`1<ShoppItemPropData> ShoppItemConfigur::shopItemPropList
	List_1_tA59155E7EFCE6269614D91A3BFB182B4F6E7044C* ___shopItemPropList_5;
	// System.Collections.Generic.List`1<ShoppItemGemData> ShoppItemConfigur::shopItemGemList
	List_1_t992A3C63FF431498EDD99CB83B368E4015B111E5* ___shopItemGemList_6;
};

// UnityEngine.SphereCollider
struct SphereCollider_tBA111C542CE97F6873DE742757213D6265C7D275  : public Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76
{
};

// UnityEngine.U2D.SpriteShapeRenderer
struct SpriteShapeRenderer_tE998BB73CF661079736CCC23617E597AB230A4AC  : public Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF
{
};

// UnityEngine.Tilemaps.TilemapRenderer
struct TilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB  : public Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF
{
};

// AESUtils
struct AESUtils_t73B52974E9A888C2EC72EF4E9DD9DB74BB710704  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// UnityEngine.AudioListener
struct AudioListener_t1D629CE9BC079C8ECDE8F822616E8A8E319EAE35  : public AudioBehaviour_t2DC0BEF7B020C952F3D2DA5AAAC88501C7EEB941
{
};

// UnityEngine.AudioSource
struct AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299  : public AudioBehaviour_t2DC0BEF7B020C952F3D2DA5AAAC88501C7EEB941
{
};

// CheckFrame
struct CheckFrame_tC52E353B564D9F0D14E116565C13913C8BD05EBF  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 CheckFrame::frameCnt
	int32_t ___frameCnt_4;
};

// CloudSaveSystem
struct CloudSaveSystem_t419542980442B82C4BB089FE4FFEDD48855C1AA6  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// ControllManager
struct ControllManager_t2642B57FBA4F0FAB7193ED564F8F575C7B993A8B  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// SystemGameManager ControllManager::systemGameManager
	SystemGameManager_tEBED10FE7ED1E6F2223FCAD4CB6FFFD68712293D* ___systemGameManager_4;
};

// CookedFoodOpreat
struct CookedFoodOpreat_tF677CBA731E07C5C26A4BB47865F87113856CA8C  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CookedFoodOpreat::plateParent
	List_1_tB951CE80B58D1BF9650862451D8DAD8C231F207B* ___plateParent_4;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CookedFoodOpreat::mainFoodList
	List_1_tB951CE80B58D1BF9650862451D8DAD8C231F207B* ___mainFoodList_5;
	// System.Int32 CookedFoodOpreat::maxMake
	int32_t ___maxMake_6;
	// System.Int32 CookedFoodOpreat::currFoodLenth
	int32_t ___currFoodLenth_7;
};

// DamondPlane
struct DamondPlane_tBE7F8A5FAAC8C3AFDD5C480FC51C5C229D9DE93B  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// HomeMapUiManager DamondPlane::uiManager
	HomeMapUiManager_tED39D923B951D28CA26D7E8BBB2F0251C8A3E9F5* ___uiManager_4;
};

// DataPlanel
struct DataPlanel_t63B93F225509A11432BB74BCC752CB1F1846EE66  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// TMPro.TextMeshProUGUI DataPlanel::cionText
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___cionText_4;
	// TMPro.TextMeshProUGUI DataPlanel::gemText
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___gemText_5;
	// TMPro.TextMeshProUGUI DataPlanel::StrengthText
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___StrengthText_6;
	// HomeMapUiManager DataPlanel::uiManager
	HomeMapUiManager_tED39D923B951D28CA26D7E8BBB2F0251C8A3E9F5* ___uiManager_7;
};

// DrinkOpreat
struct DrinkOpreat_tE3BB22B195D920181FB3F2361BCDC241DEC2FF15  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// SystemGameManager DrinkOpreat::systemGameManager
	SystemGameManager_tEBED10FE7ED1E6F2223FCAD4CB6FFFD68712293D* ___systemGameManager_4;
	// DG.Tweening.Tween DrinkOpreat::steakDTween
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___steakDTween_5;
	// UnityEngine.UI.Image DrinkOpreat::timeSuchdule
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___timeSuchdule_6;
	// TMPro.TextMeshProUGUI DrinkOpreat::timeText
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___timeText_7;
	// UnityEngine.GameObject DrinkOpreat::cup
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___cup_8;
	// DrinkState DrinkOpreat::drinkState
	int32_t ___drinkState_9;
	// System.Single DrinkOpreat::addTime
	float ___addTime_10;
	// System.Single DrinkOpreat::indexTime
	float ___indexTime_11;
	// System.String DrinkOpreat::drinkName
	String_t* ___drinkName_12;
	// System.String DrinkOpreat::drinkID
	String_t* ___drinkID_13;
	// System.Int32 DrinkOpreat::drinkPrice
	int32_t ___drinkPrice_14;
};

// FireEffect
struct FireEffect_t3405E939E4E39C43487E73C9DA6799E6680567B8  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Transform FireEffect::fireEffectParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___fireEffectParent_4;
	// UnityEngine.GameObject FireEffect::fireEffectPrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___fireEffectPrefab_5;
	// System.Int32 FireEffect::maxFire
	int32_t ___maxFire_6;
	// System.Single FireEffect::maxPlayTime
	float ___maxPlayTime_7;
	// UnityEngine.GameObject FireEffect::ContuinueBtn
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ContuinueBtn_8;
	// System.Single FireEffect::playedTime
	float ___playedTime_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> FireEffect::effcetList
	List_1_tB951CE80B58D1BF9650862451D8DAD8C231F207B* ___effcetList_10;
	// UnityEngine.GameObject FireEffect::successsImage
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___successsImage_11;
	// UnityEngine.GameObject FireEffect::manImage
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___manImage_12;
	// UnityEngine.GameObject FireEffect::CoinImage
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___CoinImage_13;
	// TMPro.TextMeshProUGUI FireEffect::CoinText
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___CoinText_14;
};

// FoodIDGenerate
struct FoodIDGenerate_tFE01E21DCD4963DB387E6E5EA90C10F120135164  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// FoodProperty
struct FoodProperty_tAAA816769EC7DC405C1AABDF794168ED46D9AF4E  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// FoodState FoodProperty::foodState
	int32_t ___foodState_4;
	// System.Int32 FoodProperty::mainFoodIndex
	int32_t ___mainFoodIndex_5;
	// FooodType FoodProperty::FooodType
	int32_t ___FooodType_6;
	// System.String FoodProperty::foodName
	String_t* ___foodName_7;
	// System.Single FoodProperty::CookedTime
	float ___CookedTime_8;
	// System.Single FoodProperty::conbustionTime
	float ___conbustionTime_9;
	// System.Single FoodProperty::foodPrice
	float ___foodPrice_10;
	// UnityEngine.Transform FoodProperty::timeParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___timeParent_11;
};

// Guest
struct Guest_t7C87006A91E9A72C586B6082DB0B7192FB76B4F3  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single Guest::maxWaitTime
	float ___maxWaitTime_4;
	// System.Single Guest::waitTimeIndex
	float ___waitTimeIndex_5;
	// System.Single Guest::tollWaitTime
	float ___tollWaitTime_6;
	// DG.Tweening.Tween Guest::doTween
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___doTween_7;
	// System.Single Guest::fromTime
	float ___fromTime_8;
	// System.Int32 Guest::foodIndex
	int32_t ___foodIndex_9;
	// System.Collections.Generic.List`1<FoodMenuAsset> Guest::neddFootAssetList
	List_1_t4F1D474EA64688149310819BB63A0150C3F2E05A* ___neddFootAssetList_10;
	// System.Boolean[] Guest::isCompeletFood
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___isCompeletFood_11;
	// UnityEngine.GameObject Guest::needTipParent
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___needTipParent_12;
	// UnityEngine.Transform[] Guest::needCompeletSign
	TransformU5BU5D_tBB9C5F5686CAE82E3D97D43DF0F3D68ABF75EC24* ___needCompeletSign_13;
	// UnityEngine.Transform[] Guest::needFoodItem
	TransformU5BU5D_tBB9C5F5686CAE82E3D97D43DF0F3D68ABF75EC24* ___needFoodItem_14;
	// UnityEngine.UI.Image Guest::waitSechduleTimeImage
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___waitSechduleTimeImage_15;
	// UnityEngine.Transform Guest::leavePrant
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___leavePrant_16;
	// LevelGuestData Guest::currGuestData
	LevelGuestData_t02E2B626BF5CBDA7A4186527CF21191B18AF1F76* ___currGuestData_17;
	// System.Collections.Generic.List`1<FoodMenu> Guest::fooodMenuList
	List_1_tC3DE54B9D2C3A344BC846F8CBE284432FA16F496* ___fooodMenuList_18;
	// System.Collections.Generic.List`1<System.Single> Guest::needFoodIDList
	List_1_t0D1C46FD8DDDE974D93CA4F3474EEC05AF950918* ___needFoodIDList_19;
	// GuestState Guest::guestState
	int32_t ___guestState_20;
	// UnityEngine.Transform Guest::originTransform
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___originTransform_21;
	// GuestManager Guest::guestManager
	GuestManager_t08BB44CBB90850DB991994981F09DF507D6E47E7* ___guestManager_22;
	// System.Int32 Guest::chairID
	int32_t ___chairID_23;
	// System.Int32 Guest::needFoddCount
	int32_t ___needFoddCount_24;
};

// GuestManager
struct GuestManager_t08BB44CBB90850DB991994981F09DF507D6E47E7  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GuestManager::guestList
	List_1_tB951CE80B58D1BF9650862451D8DAD8C231F207B* ___guestList_4;
	// System.Single GuestManager::guestTime
	float ___guestTime_5;
	// UnityEngine.Transform GuestManager::guestParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___guestParent_6;
	// GuestParentInfo GuestManager::guestParentInfo
	GuestParentInfo_t89532795C9D4D6B5B3F4BB06EA6F6732125B48F9* ___guestParentInfo_7;
	// System.Int32 GuestManager::GuestCompelet
	int32_t ___GuestCompelet_8;
	// UnityEngine.Transform GuestManager::orignTransform
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___orignTransform_9;
	// System.Int32 GuestManager::guestCount
	int32_t ___guestCount_10;
	// SystemGameManager GuestManager::systemGameManager
	SystemGameManager_tEBED10FE7ED1E6F2223FCAD4CB6FFFD68712293D* ___systemGameManager_11;
};

// GuestParentInfo
struct GuestParentInfo_t89532795C9D4D6B5B3F4BB06EA6F6732125B48F9  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Collections.Generic.List`1<UnityEngine.Transform> GuestParentInfo::guestWartFoodPos
	List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D* ___guestWartFoodPos_4;
	// UnityEngine.Transform GuestParentInfo::guestStartPos1
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___guestStartPos1_5;
	// UnityEngine.Transform GuestParentInfo::guestStartPos2
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___guestStartPos2_6;
	// System.Collections.Generic.List`1<System.Boolean> GuestParentInfo::isHaveGuestLIst
	List_1_t01207CE5982A7640E56B1F9F672A06F96B09367A* ___isHaveGuestLIst_7;
};

// GuideInfo
struct GuideInfo_tA179C11095A9336906CCD3B1AB6749C242EA4A78  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.String GuideInfo::guideName
	String_t* ___guideName_4;
	// System.Int32 GuideInfo::guideID
	int32_t ___guideID_5;
	// System.String GuideInfo::startEvent
	String_t* ___startEvent_6;
	// System.String GuideInfo::clickEvent
	String_t* ___clickEvent_7;
	// System.Boolean GuideInfo::isShowBG
	bool ___isShowBG_8;
	// System.Int32 GuideInfo::nextID
	int32_t ___nextID_9;
	// System.Action GuideInfo::eventMag
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___eventMag_10;
};

// HeadItem
struct HeadItem_t312518C0502789BA1EB4486CDB4B1DE9D091BCE7  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// HeadPlanel HeadItem::headPlanel
	HeadPlanel_t30E55A397C5AC7D53D584544BB98F6BA41F6AC96* ___headPlanel_4;
	// DG.Tweening.Tween HeadItem::tween
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___tween_5;
	// HeadAssetData HeadItem::headData
	HeadAssetData_tAB6CEFDAB592FB132D8A492EF5DE6324840FFB0B* ___headData_6;
};

// HideLoadingPage
struct HideLoadingPage_tEF54F8AA9B26E57CF291BB457A0ABCBC91BC182D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// HomeMapUiManager
struct HomeMapUiManager_tED39D923B951D28CA26D7E8BBB2F0251C8A3E9F5  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// StartHomeManager HomeMapUiManager::startHomeManager
	StartHomeManager_tB9F9914830AFB48632C8B356435C243F2865B490* ___startHomeManager_4;
	// UiMapPlane HomeMapUiManager::mapHome
	UiMapPlane_t14D8BC95B1C4C0407390DC6A79DD55B3315B61A3* ___mapHome_5;
	// LevelInfoPlane HomeMapUiManager::levelInfoPlane
	LevelInfoPlane_tB71B01496603F22E8E44451B5A1EC9ACDABBFAC7* ___levelInfoPlane_6;
	// lockPlanel HomeMapUiManager::lockPlanel
	lockPlanel_tA610DCB63E2BDA659C0462950078BC088F970148* ___lockPlanel_7;
	// DamondPlane HomeMapUiManager::damonPlane
	DamondPlane_tBE7F8A5FAAC8C3AFDD5C480FC51C5C229D9DE93B* ___damonPlane_8;
	// HomePlanel HomeMapUiManager::homePlanel
	HomePlanel_tC54E997184482AEDEE52C73099DB871A611E87A2* ___homePlanel_9;
	// HeadPlanel HomeMapUiManager::headPlanel
	HeadPlanel_t30E55A397C5AC7D53D584544BB98F6BA41F6AC96* ___headPlanel_10;
	// ShoppPlanel HomeMapUiManager::shopPlanel
	ShoppPlanel_t07BFA47DF40CE73299500CC5B2D0334F2F1353BF* ___shopPlanel_11;
	// TaskPlanel HomeMapUiManager::taskPlanel
	TaskPlanel_t885BA63C37C30C820EC675FCCE61B39C24BE96B6* ___taskPlanel_12;
	// DataPlanel HomeMapUiManager::dataPlanel
	DataPlanel_t63B93F225509A11432BB74BCC752CB1F1846EE66* ___dataPlanel_13;
	// ComparePlayerDataPlanel HomeMapUiManager::comparePlayerDataPlanel
	ComparePlayerDataPlanel_t2A3BBF964761B874A51BB23F98AFD96D7C7D48B6* ___comparePlayerDataPlanel_14;
};

// HomePlanel
struct HomePlanel_tC54E997184482AEDEE52C73099DB871A611E87A2  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// HomeMapUiManager HomePlanel::uiManager
	HomeMapUiManager_tED39D923B951D28CA26D7E8BBB2F0251C8A3E9F5* ___uiManager_4;
	// UnityEngine.UI.Image HomePlanel::headImage
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___headImage_5;
};

// LevelManager
struct LevelManager_t8405886BBC5A0ACBB1CC210E25D5DA1C72D16530  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 LevelManager::currLevel
	int32_t ___currLevel_4;
	// SystemGameManager LevelManager::systemGameManager
	SystemGameManager_tEBED10FE7ED1E6F2223FCAD4CB6FFFD68712293D* ___systemGameManager_5;
	// UnityEngine.GameObject LevelManager::RestaurantPartent
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___RestaurantPartent_6;
	// UnityEngine.GameObject LevelManager::currRestaurant
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___currRestaurant_7;
	// RestaurantInfo LevelManager::currRestaurantInfo
	RestaurantInfo_t5A071066135E99AEC87FFC53FA192968999F8A19* ___currRestaurantInfo_8;
};

// LevelSelect
struct LevelSelect_tBC6DA72BFDF1D19B21377EFF8C8BA66197DB356F  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 LevelSelect::selectLevel
	int32_t ___selectLevel_4;
	// LevelInfoPlane LevelSelect::levelPlanel
	LevelInfoPlane_tB71B01496603F22E8E44451B5A1EC9ACDABBFAC7* ___levelPlanel_5;
};

// MakinFoodOpreat
struct MakinFoodOpreat_tBF381637FA4B7DB6072DBC27EBE3CE50BCE312EB  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MakinFoodOpreat::makingFoodListParent
	List_1_tB951CE80B58D1BF9650862451D8DAD8C231F207B* ___makingFoodListParent_4;
	// System.Collections.Generic.List`1<UnityEngine.Transform> MakinFoodOpreat::foodPlacePlatePreaList
	List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D* ___foodPlacePlatePreaList_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MakinFoodOpreat::mainFoodList
	List_1_tB951CE80B58D1BF9650862451D8DAD8C231F207B* ___mainFoodList_6;
	// System.Int32 MakinFoodOpreat::maxMake
	int32_t ___maxMake_7;
	// System.Int32 MakinFoodOpreat::currFoodLenth
	int32_t ___currFoodLenth_8;
};

// MapManager
struct MapManager_t05F548D377024861E72CA6B6CC2D15681AA25F53  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// SystemGameManager MapManager::systemGameManager
	SystemGameManager_tEBED10FE7ED1E6F2223FCAD4CB6FFFD68712293D* ___systemGameManager_4;
	// UnityEngine.GameObject MapManager::mapPrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___mapPrefab_5;
	// UnityEngine.Transform MapManager::mapPatent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___mapPatent_6;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MapManager::mapLists
	List_1_tB951CE80B58D1BF9650862451D8DAD8C231F207B* ___mapLists_7;
};

// NewBehaviourScript
struct NewBehaviourScript_t89363A736D121ACCCE70DC36A66775D2F1D42CF7  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// OperationManage
struct OperationManage_tFF184A4A7A2B8B93259740624B38E08B84CB6FC0  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Transform OperationManage::foodMakeParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___foodMakeParent_4;
	// UnityEngine.Transform OperationManage::PlateParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___PlateParent_5;
	// UnityEngine.Transform OperationManage::rawFoodParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___rawFoodParent_6;
	// UnityEngine.Transform OperationManage::rawOtherFoodParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___rawOtherFoodParent_7;
	// LevelData OperationManage::currlevelData
	LevelData_tE6EAE053C277D3E4ED073363F0BF24BDBADFC4D5* ___currlevelData_8;
	// System.Collections.Generic.Dictionary`2<System.String,MainFoodCompnentAssets> OperationManage::currMainFooResdDictionary
	Dictionary_2_t64FA1E93FCA2400B5F74FBF9D1D3A2A63D27742E* ___currMainFooResdDictionary_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> OperationManage::rwaMainFoodList
	List_1_tB951CE80B58D1BF9650862451D8DAD8C231F207B* ___rwaMainFoodList_10;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> OperationManage::reaOtherFoodList
	List_1_tB951CE80B58D1BF9650862451D8DAD8C231F207B* ___reaOtherFoodList_11;
	// SystemGameManager OperationManage::systemGameManager
	SystemGameManager_tEBED10FE7ED1E6F2223FCAD4CB6FFFD68712293D* ___systemGameManager_12;
};

// OtherFood
struct OtherFood_tF914BF6DC534D3268B2469631B625A84FA77A57D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.String OtherFood::foodNmae
	String_t* ___foodNmae_4;
	// System.Single OtherFood::foodPrice
	float ___foodPrice_5;
};

// PlateItem
struct PlateItem_tF7BA2EA5B617D317A76FED2604C037ECD6399943  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.String PlateItem::ItemName
	String_t* ___ItemName_4;
	// System.String PlateItem::mainfood
	String_t* ___mainfood_5;
	// System.String[] PlateItem::otherFood
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___otherFood_6;
	// System.String PlateItem::foodID
	String_t* ___foodID_7;
	// System.Int32 PlateItem::plateIndex
	int32_t ___plateIndex_8;
	// DG.Tweening.Tween PlateItem::clickTween
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___clickTween_9;
	// FoodProperty PlateItem::foodItem
	FoodProperty_tAAA816769EC7DC405C1AABDF794168ED46D9AF4E* ___foodItem_10;
	// System.Single PlateItem::allPrice
	float ___allPrice_11;
	// System.Single PlateItem::mainFoodPrice
	float ___mainFoodPrice_12;
};

// RawFoodOpreat
struct RawFoodOpreat_t5A84416D8152DDDD497DE3321E335734D55BDB6E  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.String RawFoodOpreat::foodName
	String_t* ___foodName_4;
	// DG.Tweening.Tween RawFoodOpreat::steakDTween
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___steakDTween_5;
};

// RestaurantInfo
struct RestaurantInfo_t5A071066135E99AEC87FFC53FA192968999F8A19  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Transform RestaurantInfo::PlateParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___PlateParent_4;
	// UnityEngine.Transform RestaurantInfo::FoodMakeParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___FoodMakeParent_5;
	// UnityEngine.Transform RestaurantInfo::RawFoodParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___RawFoodParent_6;
	// LevelData RestaurantInfo::levelData
	LevelData_tE6EAE053C277D3E4ED073363F0BF24BDBADFC4D5* ___levelData_7;
	// UnityEngine.Transform RestaurantInfo::guestPrant
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___guestPrant_8;
	// UnityEngine.Transform RestaurantInfo::otherFoodParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___otherFoodParent_9;
	// UnityEngine.Transform RestaurantInfo::drinkCup
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___drinkCup_10;
	// UnityEngine.Transform[] RestaurantInfo::drinkPlate
	TransformU5BU5D_tBB9C5F5686CAE82E3D97D43DF0F3D68ABF75EC24* ___drinkPlate_11;
	// SystemGameManager RestaurantInfo::systemGameManager
	SystemGameManager_tEBED10FE7ED1E6F2223FCAD4CB6FFFD68712293D* ___systemGameManager_12;
	// DrinkOpreat RestaurantInfo::drinkOpreat
	DrinkOpreat_tE3BB22B195D920181FB3F2361BCDC241DEC2FF15* ___drinkOpreat_13;
};

// StartHomeManager
struct StartHomeManager_tB9F9914830AFB48632C8B356435C243F2865B490  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// HomeMapUiManager StartHomeManager::uiManager
	HomeMapUiManager_tED39D923B951D28CA26D7E8BBB2F0251C8A3E9F5* ___uiManager_4;
	// SystmConfigurationManager StartHomeManager::systmConfigurationManager
	SystmConfigurationManager_tC1C970C67458E76BA7D6A62A62A7B7DD818C1B89* ___systmConfigurationManager_5;
	// YooAsset.EPlayMode StartHomeManager::PlayMode
	int32_t ___PlayMode_6;
	// System.Boolean StartHomeManager::runOnce
	bool ___runOnce_7;
};

// SystemEventManager
struct SystemEventManager_t7316E370F40FC805D47CEFFCBE61D389BB43726D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// SystemGameManager SystemEventManager::systemGameManager
	SystemGameManager_tEBED10FE7ED1E6F2223FCAD4CB6FFFD68712293D* ___systemGameManager_4;
};

// SystemGameManager
struct SystemGameManager_tEBED10FE7ED1E6F2223FCAD4CB6FFFD68712293D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// SystmConfigurationManager SystemGameManager::systmConfigurationManager
	SystmConfigurationManager_tC1C970C67458E76BA7D6A62A62A7B7DD818C1B89* ___systmConfigurationManager_4;
	// UiManager SystemGameManager::uiManager
	UiManager_tD893C8D36318160D6F63738C3B33FE3C22D9688F* ___uiManager_5;
	// LevelManager SystemGameManager::levelManager
	LevelManager_t8405886BBC5A0ACBB1CC210E25D5DA1C72D16530* ___levelManager_6;
	// OperationManage SystemGameManager::operationManage
	OperationManage_tFF184A4A7A2B8B93259740624B38E08B84CB6FC0* ___operationManage_7;
	// GuestManager SystemGameManager::guestManager
	GuestManager_t08BB44CBB90850DB991994981F09DF507D6E47E7* ___guestManager_8;
	// GameStat SystemGameManager::gameStat
	int32_t ___gameStat_9;
};

// SystemGuideEvent
struct SystemGuideEvent_t94EA5FAE785DE9FBADF02F5688797C234FFBBA09  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Collections.Generic.Dictionary`2<System.String,GuideInfo> SystemGuideEvent::guideInfoDict
	Dictionary_2_t1A6B95EB52FB5C6A88974817699958F8C173DA64* ___guideInfoDict_5;
	// System.Collections.Generic.List`1<GuideInfo> SystemGuideEvent::guideInfoLost
	List_1_tD2AB7134E6C9D42A3393505AE4AFE4D76C397EBF* ___guideInfoLost_6;
};

// SystmConfigurationManager
struct SystmConfigurationManager_tC1C970C67458E76BA7D6A62A62A7B7DD818C1B89  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.String SystmConfigurationManager::AssetsFileName
	String_t* ___AssetsFileName_4;
	// System.String SystmConfigurationManager::levelConfigurName
	String_t* ___levelConfigurName_5;
	// System.String SystmConfigurationManager::otherFoodConfigurName
	String_t* ___otherFoodConfigurName_6;
	// System.String SystmConfigurationManager::RestaurantAssetConfigerName
	String_t* ___RestaurantAssetConfigerName_7;
	// System.Single SystmConfigurationManager::guestTiem
	float ___guestTiem_8;
	// System.Int32 SystmConfigurationManager::maxLevel
	int32_t ___maxLevel_9;
};

// TaskItem
struct TaskItem_tEB8E8BD8BCE9F38E36B33A0A4E863E3A74EF50BB  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// TMPro.TextMeshProUGUI TaskItem::taskName
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___taskName_4;
	// TMPro.TextMeshProUGUI TaskItem::taskDescribe
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___taskDescribe_5;
	// TMPro.TextMeshProUGUI TaskItem::taskReward
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___taskReward_6;
};

// Unity.ThrowStub
struct ThrowStub_t9161280E38728A40D9B1A975AEE62E89C379E400  : public ObjectDisposedException_tC5FB29E8E980E2010A2F6A5B9B791089419F89EB
{
};

// UnityEngine.Tilemaps.Tilemap
struct Tilemap_t18C4166D0AC702D5BFC0C411FA73C4B61D9D1751  : public GridLayout_tAD661B1E1E57C16BE21C8C13432EA04FE1F0418B
{
};

// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// UIText
struct UIText_t28F680F7B1090866318E4896D50C299BD1FE7971  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// OverPlanel UIText::planel
	OverPlanel_t3BD8295CC1BDBF33D0900B2F4059249914D6F9A0* ___planel_4;
};

// UiLoadIngSlider
struct UiLoadIngSlider_t46E44ECE6F645B402AD3E8ED6CFA4C54FDA9D3AD  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.UI.Slider UiLoadIngSlider::slider
	Slider_t87EA570E3D6556CABF57456C2F3873FFD86E652F* ___slider_4;
};

// UiManager
struct UiManager_tD893C8D36318160D6F63738C3B33FE3C22D9688F  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// SystemGameManager UiManager::systemGameManager
	SystemGameManager_tEBED10FE7ED1E6F2223FCAD4CB6FFFD68712293D* ___systemGameManager_4;
	// UnityEngine.GameObject UiManager::uiManagerPrent
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___uiManagerPrent_5;
	// UnityEngine.GameObject UiManager::startBtn
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___startBtn_6;
	// UnityEngine.GameObject UiManager::continueBtn
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___continueBtn_7;
	// TMPro.TextMeshProUGUI UiManager::textLog
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___textLog_8;
	// UnityEngine.Transform UiManager::pos1
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___pos1_9;
	// UnityEngine.Transform UiManager::pos2
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___pos2_10;
	// OverPlanel UiManager::overPlanel
	OverPlanel_t3BD8295CC1BDBF33D0900B2F4059249914D6F9A0* ___overPlanel_11;
	// LoadinPlanle UiManager::loadingPlanle
	LoadinPlanle_tF6494D52114639C6DB4E80385577677753F498C0* ___loadingPlanle_12;
	// TargetPlanle UiManager::targetPlanle
	TargetPlanle_t6A8EBC369B27AE5A05189F09204256FCF600578E* ___targetPlanle_13;
	// GameHomePlanel UiManager::gameHomePlael
	GameHomePlanel_t8A036D21BEF99C8AE3853851AB94592D7821F08C* ___gameHomePlael_14;
};

// UiWoPropAbility
struct UiWoPropAbility_t64D4B3E5A3130C108CC67FE84351386EE0E3AE3F  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// SystemGameManager UiWoPropAbility::systemGameManager
	SystemGameManager_tEBED10FE7ED1E6F2223FCAD4CB6FFFD68712293D* ___systemGameManager_4;
	// UnityEngine.CanvasGroup UiWoPropAbility::targetGroup
	CanvasGroup_t048C1461B14628CFAEBE6E7353093ADB04EBC094* ___targetGroup_5;
	// UnityEngine.CanvasGroup UiWoPropAbility::canvasHomeGroup
	CanvasGroup_t048C1461B14628CFAEBE6E7353093ADB04EBC094* ___canvasHomeGroup_6;
	// System.Single UiWoPropAbility::changeTime
	float ___changeTime_7;
};

// WXProfileStatsScript
struct WXProfileStatsScript_t45573420CB5800F0D44EF926BAC238BAC9805CC2  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.String WXProfileStatsScript::statsText
	String_t* ___statsText_4;
	// Unity.Profiling.ProfilerRecorder WXProfileStatsScript::m_setPassCallsRecorder
	ProfilerRecorder_t363D18B531351FF6D7A09072564EB5D8FC60E613 ___m_setPassCallsRecorder_5;
	// Unity.Profiling.ProfilerRecorder WXProfileStatsScript::m_drawCallsRecorder
	ProfilerRecorder_t363D18B531351FF6D7A09072564EB5D8FC60E613 ___m_drawCallsRecorder_6;
	// Unity.Profiling.ProfilerRecorder WXProfileStatsScript::m_verticesRecorder
	ProfilerRecorder_t363D18B531351FF6D7A09072564EB5D8FC60E613 ___m_verticesRecorder_7;
	// System.Int32 WXProfileStatsScript::m_fpsCount
	int32_t ___m_fpsCount_8;
	// System.Single WXProfileStatsScript::m_fpsDeltaTime
	float ___m_fpsDeltaTime_9;
	// System.Int32 WXProfileStatsScript::fps
	int32_t ___fps_10;
	// UnityEngine.GUIStyle WXProfileStatsScript::m_bgStyle
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_bgStyle_11;
	// System.Boolean WXProfileStatsScript::m_isShow
	bool ___m_isShow_12;
	// System.Collections.Generic.Dictionary`2<System.String,WXProfileStatsScript/ProfValue> WXProfileStatsScript::profValues
	Dictionary_2_tC63C4A4A3B816DD32DF40BB25346F9AC6D342BAB* ___profValues_13;
};

// YooAsset.YooAssetsDriver
struct YooAssetsDriver_tB0C2B38D94EC1806129CAEA70F4385E5D34F81D5  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// UnityEngine.EventSystems.BaseInput
struct BaseInput_t69C46B0AA3C24F1CA842A0D03CACACC4EC788622  : public UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D
{
};

// ComparePlayerDataPlanel
struct ComparePlayerDataPlanel_t2A3BBF964761B874A51BB23F98AFD96D7C7D48B6  : public UiWoPropAbility_t64D4B3E5A3130C108CC67FE84351386EE0E3AE3F
{
	// TMPro.TextMeshProUGUI[] ComparePlayerDataPlanel::cionText
	TextMeshProUGUIU5BU5D_tCAB9404D43876DF686DFBB4613543479CC1CC24A* ___cionText_8;
	// TMPro.TextMeshProUGUI[] ComparePlayerDataPlanel::gemText
	TextMeshProUGUIU5BU5D_tCAB9404D43876DF686DFBB4613543479CC1CC24A* ___gemText_9;
	// CloundSavePlayerDaata ComparePlayerDataPlanel::cloundPlayerData
	CloundSavePlayerDaata_t1F9D694D8D690B33A0868EDCE49D4C83F8C77FD8* ___cloundPlayerData_10;
	// HomeMapUiManager ComparePlayerDataPlanel::uiManager
	HomeMapUiManager_tED39D923B951D28CA26D7E8BBB2F0251C8A3E9F5* ___uiManager_11;
};

// GameHomePlanel
struct GameHomePlanel_t8A036D21BEF99C8AE3853851AB94592D7821F08C  : public UiWoPropAbility_t64D4B3E5A3130C108CC67FE84351386EE0E3AE3F
{
	// TMPro.TextMeshProUGUI GameHomePlanel::cionTargetText
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___cionTargetText_8;
	// TMPro.TextMeshProUGUI GameHomePlanel::manText
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___manText_9;
};

// GreensFood
struct GreensFood_tB28A2EE0649DB3E268B2B7810E9A8AC2F88E9C58  : public OtherFood_tF914BF6DC534D3268B2469631B625A84FA77A57D
{
	// DG.Tweening.Tween GreensFood::tween
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___tween_6;
};

// HeadPlanel
struct HeadPlanel_t30E55A397C5AC7D53D584544BB98F6BA41F6AC96  : public UiWoPropAbility_t64D4B3E5A3130C108CC67FE84351386EE0E3AE3F
{
	// HomeMapUiManager HeadPlanel::uiManager
	HomeMapUiManager_tED39D923B951D28CA26D7E8BBB2F0251C8A3E9F5* ___uiManager_8;
	// UnityEngine.Transform HeadPlanel::headParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___headParent_9;
	// UnityEngine.GameObject HeadPlanel::headPrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___headPrefab_10;
	// UnityEngine.GameObject HeadPlanel::saveBtn
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___saveBtn_11;
	// HeadAssetData HeadPlanel::currHeadData
	HeadAssetData_tAB6CEFDAB592FB132D8A492EF5DE6324840FFB0B* ___currHeadData_12;
};

// LevelInfoPlane
struct LevelInfoPlane_tB71B01496603F22E8E44451B5A1EC9ACDABBFAC7  : public UiWoPropAbility_t64D4B3E5A3130C108CC67FE84351386EE0E3AE3F
{
	// HomeMapUiManager LevelInfoPlane::uiManager
	HomeMapUiManager_tED39D923B951D28CA26D7E8BBB2F0251C8A3E9F5* ___uiManager_8;
	// UnityEngine.UI.Button LevelInfoPlane::startCookingBtn
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___startCookingBtn_9;
	// UnityEngine.UI.Button[] LevelInfoPlane::upGraedAryBtn
	ButtonU5BU5D_t0D512FF2FF5A72DAC04754F2C10182F850328BEF* ___upGraedAryBtn_10;
	// UnityEngine.GameObject[] LevelInfoPlane::aryUpGreadParent
	GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* ___aryUpGreadParent_11;
	// UnityEngine.Transform[] LevelInfoPlane::scroilViewContent
	TransformU5BU5D_tBB9C5F5686CAE82E3D97D43DF0F3D68ABF75EC24* ___scroilViewContent_12;
	// UnityEngine.GameObject LevelInfoPlane::selectLevelPrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___selectLevelPrefab_13;
	// TMPro.TextMeshProUGUI LevelInfoPlane::textLevel
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___textLevel_14;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> LevelInfoPlane::levlItamList
	List_1_tB951CE80B58D1BF9650862451D8DAD8C231F207B* ___levlItamList_15;
};

// LoadinPlanle
struct LoadinPlanle_tF6494D52114639C6DB4E80385577677753F498C0  : public UiWoPropAbility_t64D4B3E5A3130C108CC67FE84351386EE0E3AE3F
{
	// UnityEngine.UI.Image LoadinPlanle::suchduleImage
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___suchduleImage_8;
	// TMPro.TextMeshProUGUI LoadinPlanle::loadingText
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___loadingText_9;
	// System.Single LoadinPlanle::maxLoadTime
	float ___maxLoadTime_10;
	// System.Single LoadinPlanle::laodTime
	float ___laodTime_11;
	// System.Boolean LoadinPlanle::loading
	bool ___loading_12;
};

// OverPlanel
struct OverPlanel_t3BD8295CC1BDBF33D0900B2F4059249914D6F9A0  : public UiWoPropAbility_t64D4B3E5A3130C108CC67FE84351386EE0E3AE3F
{
	// UiManager OverPlanel::uiManager
	UiManager_tD893C8D36318160D6F63738C3B33FE3C22D9688F* ___uiManager_8;
	// UnityEngine.Transform OverPlanel::cionBg
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___cionBg_9;
};

// ShoppPlanel
struct ShoppPlanel_t07BFA47DF40CE73299500CC5B2D0334F2F1353BF  : public UiWoPropAbility_t64D4B3E5A3130C108CC67FE84351386EE0E3AE3F
{
	// UnityEngine.UI.Button[] ShoppPlanel::buttonArray
	ButtonU5BU5D_t0D512FF2FF5A72DAC04754F2C10182F850328BEF* ___buttonArray_8;
	// UnityEngine.GameObject[] ShoppPlanel::conentParent
	GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* ___conentParent_9;
	// UnityEngine.GameObject[] ShoppPlanel::scrollView
	GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* ___scrollView_10;
	// ShoppItemConfigur ShoppPlanel::shopItemConfigur
	ShoppItemConfigur_tB9BC6996C5A7913E1EEF4282BB2E238538D9F82C* ___shopItemConfigur_11;
	// UnityEngine.GameObject ShoppPlanel::shopItemPrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___shopItemPrefab_12;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ShoppPlanel::itemPropList
	List_1_tB951CE80B58D1BF9650862451D8DAD8C231F207B* ___itemPropList_13;
	// System.Int32 ShoppPlanel::butonIndex
	int32_t ___butonIndex_14;
};

// SteakFood
struct SteakFood_t68A60966560EBD8C29AAAECE8DB315B51E17015B  : public FoodProperty_tAAA816769EC7DC405C1AABDF794168ED46D9AF4E
{
	// System.Single SteakFood::makeTime
	float ___makeTime_12;
	// TMPro.TextMeshProUGUI SteakFood::timeTxt
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___timeTxt_13;
	// UnityEngine.UI.Image SteakFood::suchduleImage
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___suchduleImage_14;
	// System.Single SteakFood::maxCookedTime
	float ___maxCookedTime_15;
	// System.Single SteakFood::maxCombustionTime
	float ___maxCombustionTime_16;
	// System.Int32 SteakFood::clickNumber
	int32_t ___clickNumber_17;
	// System.Single SteakFood::clickTime
	float ___clickTime_18;
	// UnityEngine.Transform SteakFood::OtherFoodParent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___OtherFoodParent_19;
	// UnityEngine.GameObject[] SteakFood::otherFood
	GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* ___otherFood_20;
	// System.Single SteakFood::clickIndex
	float ___clickIndex_21;
	// DG.Tweening.Tween SteakFood::steakDTween
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___steakDTween_22;
};

// TargetPlanle
struct TargetPlanle_t6A8EBC369B27AE5A05189F09204256FCF600578E  : public UiWoPropAbility_t64D4B3E5A3130C108CC67FE84351386EE0E3AE3F
{
	// UnityEngine.Transform TargetPlanle::targetBgImage
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___targetBgImage_8;
	// DG.Tweening.Tween TargetPlanle::tween
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___tween_9;
	// TMPro.TextMeshProUGUI TargetPlanle::cionText
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___cionText_10;
};

// TaskPlanel
struct TaskPlanel_t885BA63C37C30C820EC675FCCE61B39C24BE96B6  : public UiWoPropAbility_t64D4B3E5A3130C108CC67FE84351386EE0E3AE3F
{
	// UnityEngine.UI.Button[] TaskPlanel::ArrayBtn
	ButtonU5BU5D_t0D512FF2FF5A72DAC04754F2C10182F850328BEF* ___ArrayBtn_8;
	// UnityEngine.GameObject[] TaskPlanel::contentParent
	GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* ___contentParent_9;
	// UnityEngine.GameObject[] TaskPlanel::scrollView
	GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* ___scrollView_10;
	// UnityEngine.GameObject TaskPlanel::taskItemPrafab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___taskItemPrafab_11;
};

// UiMapPlane
struct UiMapPlane_t14D8BC95B1C4C0407390DC6A79DD55B3315B61A3  : public UiWoPropAbility_t64D4B3E5A3130C108CC67FE84351386EE0E3AE3F
{
	// UnityEngine.AsyncOperation UiMapPlane::asyncOper
	AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C* ___asyncOper_8;
	// LoadinPlanle UiMapPlane::loadPlanle
	LoadinPlanle_tF6494D52114639C6DB4E80385577677753F498C0* ___loadPlanle_9;
	// TMPro.TextMeshProUGUI UiMapPlane::cionText
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___cionText_10;
	// TMPro.TextMeshProUGUI UiMapPlane::strengthText
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___strengthText_11;
	// TMPro.TextMeshProUGUI UiMapPlane::diamondText
	TextMeshProUGUI_t101091AF4B578BB534C92E9D1EEAF0611636D957* ___diamondText_12;
	// UnityEngine.UI.Button UiMapPlane::levelBut
	Button_t6786514A57F7AFDEE5431112FEA0CAB24F5AE098* ___levelBut_13;
	// HomeMapUiManager UiMapPlane::uiManager
	HomeMapUiManager_tED39D923B951D28CA26D7E8BBB2F0251C8A3E9F5* ___uiManager_14;
};

// lockPlanel
struct lockPlanel_tA610DCB63E2BDA659C0462950078BC088F970148  : public UiWoPropAbility_t64D4B3E5A3130C108CC67FE84351386EE0E3AE3F
{
};

// otherFoodTypeTwo
struct otherFoodTypeTwo_tA921E2CF1C74C4C68005FF1673535CBB46E9BE21  : public OtherFood_tF914BF6DC534D3268B2469631B625A84FA77A57D
{
	// DG.Tweening.Tween otherFoodTypeTwo::tween
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___tween_6;
};

// WXTouchInputOverride
struct WXTouchInputOverride_t537346190BF9A183BE089711BE35EB7E4ADE5A8D  : public BaseInput_t69C46B0AA3C24F1CA842A0D03CACACC4EC788622
{
	// System.Boolean WXTouchInputOverride::_isInitWechatSDK
	bool ____isInitWechatSDK_4;
	// System.Collections.Generic.List`1<TouchData> WXTouchInputOverride::_touches
	List_1_tD85647D7BC0A47DAC4D5BF624DAAC00B6D45763C* ____touches_5;
	// UnityEngine.EventSystems.StandaloneInputModule WXTouchInputOverride::_standaloneInputModule
	StandaloneInputModule_tD8B581E4A0A2A25B99EB002FF669C4EEED350530* ____standaloneInputModule_6;
};

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <PrivateImplementationDetails>

// <PrivateImplementationDetails>

// Mono.Security.ASN1

// Mono.Security.ASN1

// Mono.Security.ASN1Convert

// Mono.Security.ASN1Convert

// UnityEngine.Analytics.AnalyticsSessionInfo
struct AnalyticsSessionInfo_tDE8F7A9E13EF9723E2D975F76E916753DA61AD76_StaticFields
{
	// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged UnityEngine.Analytics.AnalyticsSessionInfo::sessionStateChanged
	SessionStateChanged_t1180FB66E702B635CAD9316DC661D931277B2A0C* ___sessionStateChanged_0;
	// UnityEngine.Analytics.AnalyticsSessionInfo/IdentityTokenChanged UnityEngine.Analytics.AnalyticsSessionInfo::identityTokenChanged
	IdentityTokenChanged_tE8CB0DAB5F6E640A847803F582E6CB6237742395* ___identityTokenChanged_1;
};

// UnityEngine.Analytics.AnalyticsSessionInfo

// UnityEngine.Experimental.Audio.AudioSampleProvider

// UnityEngine.Experimental.Audio.AudioSampleProvider

// UnityEngine.AudioSettings
struct AudioSettings_t66C4BCA1E463B061E2EC9063FB882ACED20D47BD_StaticFields
{
	// UnityEngine.AudioSettings/AudioConfigurationChangeHandler UnityEngine.AudioSettings::OnAudioConfigurationChanged
	AudioConfigurationChangeHandler_tE071B0CBA3B3A77D3E41F5FCB65B4017885B3177* ___OnAudioConfigurationChanged_0;
	// System.Action UnityEngine.AudioSettings::OnAudioSystemShuttingDown
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___OnAudioSystemShuttingDown_1;
	// System.Action UnityEngine.AudioSettings::OnAudioSystemStartedUp
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___OnAudioSystemStartedUp_2;
};

// UnityEngine.AudioSettings

// Mono.Security.BitConverterLE

// Mono.Security.BitConverterLE

// YooAsset.BufferReader

// YooAsset.BufferReader

// YooAsset.BufferWriter

// YooAsset.BufferWriter

// UnityEngine.CameraRaycastHelper

// UnityEngine.CameraRaycastHelper

// CloudFunction
struct CloudFunction_t1007F8C7EAF212A9FD19D0FC2F0C71E348DBD958_StaticFields
{
	// CloudFunction CloudFunction::instance
	CloudFunction_t1007F8C7EAF212A9FD19D0FC2F0C71E348DBD958* ___instance_0;
};

// CloudFunction

// CloundSaveGameData

// CloundSaveGameData

// CloundSavePlayerDaata

// CloundSavePlayerDaata

// System.Configuration.ConfigurationElement

// System.Configuration.ConfigurationElement

// System.Configuration.ConfigurationPropertyCollection

// System.Configuration.ConfigurationPropertyCollection

// System.Configuration.ConfigurationSectionGroup

// System.Configuration.ConfigurationSectionGroup

// UnityEngine.Analytics.ContinuousEvent

// UnityEngine.Analytics.ContinuousEvent

// Mono.Security.Cryptography.CryptoConvert

// Mono.Security.Cryptography.CryptoConvert

// DG.Tweening.DOTweenCYInstruction

// DG.Tweening.DOTweenCYInstruction

// DG.Tweening.DOTweenModuleAudio

// DG.Tweening.DOTweenModuleAudio

// DG.Tweening.DOTweenModulePhysics

// DG.Tweening.DOTweenModulePhysics

// DG.Tweening.DOTweenModulePhysics2D

// DG.Tweening.DOTweenModulePhysics2D

// DG.Tweening.DOTweenModuleSprite

// DG.Tweening.DOTweenModuleSprite

// DG.Tweening.DOTweenModuleUI

// DG.Tweening.DOTweenModuleUI

// DG.Tweening.DOTweenModuleUnityVersion

// DG.Tweening.DOTweenModuleUnityVersion

// DG.Tweening.DOTweenModuleUtils
struct DOTweenModuleUtils_t5554865584F951A4A4E5DD282E6EBC60F5CEC6E9_StaticFields
{
	// System.Boolean DG.Tweening.DOTweenModuleUtils::_initialized
	bool ____initialized_0;
};

// DG.Tweening.DOTweenModuleUtils

// DrinkComponent

// DrinkComponent

// YooAsset.EditorSimulateModeHelper

// YooAsset.EditorSimulateModeHelper

// YooAsset.EditorSimulateModeImpl

// YooAsset.EditorSimulateModeImpl

// EventParamProperties

// EventParamProperties

// LitJson.Extensions.Extensions

// LitJson.Extensions.Extensions

// YooAsset.FileUtility

// YooAsset.FileUtility

// UnityEngine.TextCore.LowLevel.FontEngine
struct FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields
{
	// UnityEngine.TextCore.Glyph[] UnityEngine.TextCore.LowLevel.FontEngine::s_Glyphs
	GlyphU5BU5D_t345CEC8703A6C650639C40DB7D35269A2D467FC5* ___s_Glyphs_0;
	// System.UInt32[] UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphIndexes_MarshallingArray_A
	UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA* ___s_GlyphIndexes_MarshallingArray_A_1;
	// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct[] UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphMarshallingStruct_IN
	GlyphMarshallingStructU5BU5D_t9424A4B1FAAD615472A9346208026B1B9E22069E* ___s_GlyphMarshallingStruct_IN_2;
	// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct[] UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphMarshallingStruct_OUT
	GlyphMarshallingStructU5BU5D_t9424A4B1FAAD615472A9346208026B1B9E22069E* ___s_GlyphMarshallingStruct_OUT_3;
	// UnityEngine.TextCore.GlyphRect[] UnityEngine.TextCore.LowLevel.FontEngine::s_FreeGlyphRects
	GlyphRectU5BU5D_t494B690215E3F3F42B6F216930A461256CE2CC70* ___s_FreeGlyphRects_4;
	// UnityEngine.TextCore.GlyphRect[] UnityEngine.TextCore.LowLevel.FontEngine::s_UsedGlyphRects
	GlyphRectU5BU5D_t494B690215E3F3F42B6F216930A461256CE2CC70* ___s_UsedGlyphRects_5;
	// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord[] UnityEngine.TextCore.LowLevel.FontEngine::s_PairAdjustmentRecords_MarshallingArray
	GlyphPairAdjustmentRecordU5BU5D_tD5DD2A739A4CA745E7F28ECCB2CD0BD0A65A38F7* ___s_PairAdjustmentRecords_MarshallingArray_6;
	// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Glyph> UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphLookupDictionary
	Dictionary_2_tC61348D10610A6B3D7B65102D82AC3467D59EAA7* ___s_GlyphLookupDictionary_7;
};

// UnityEngine.TextCore.LowLevel.FontEngine

// FoodItem

// FoodItem

// FoodMenu

// FoodMenu

// FoodMenuAsset

// FoodMenuAsset

// LitJson.FsmContext

// LitJson.FsmContext

// UnityEngine.GUIClip

// UnityEngine.GUIClip

// UnityEngine.GUIContent
struct GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2_StaticFields
{
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_Text
	GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2* ___s_Text_3;
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_Image
	GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2* ___s_Image_4;
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_TextImage
	GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2* ___s_TextImage_5;
	// UnityEngine.GUIContent UnityEngine.GUIContent::none
	GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2* ___none_6;
};

// UnityEngine.GUIContent

// UnityEngine.GUILayout

// UnityEngine.GUILayout

// UnityEngine.GUILayoutOption

// UnityEngine.GUILayoutOption

// UnityEngine.GUIUtility
struct GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A_StaticFields
{
	// System.Int32 UnityEngine.GUIUtility::s_ControlCount
	int32_t ___s_ControlCount_0;
	// System.Int32 UnityEngine.GUIUtility::s_SkinMode
	int32_t ___s_SkinMode_1;
	// System.Int32 UnityEngine.GUIUtility::s_OriginalID
	int32_t ___s_OriginalID_2;
	// System.Action UnityEngine.GUIUtility::takeCapture
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___takeCapture_3;
	// System.Action UnityEngine.GUIUtility::releaseCapture
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___releaseCapture_4;
	// System.Func`3<System.Int32,System.IntPtr,System.Boolean> UnityEngine.GUIUtility::processEvent
	Func_3_t2376B3D8C7A437FC32F21C4C4E4B3E7D2302007C* ___processEvent_5;
	// System.Action UnityEngine.GUIUtility::cleanupRoots
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___cleanupRoots_6;
	// System.Func`2<System.Exception,System.Boolean> UnityEngine.GUIUtility::endContainerGUIFromException
	Func_2_tDDBE08B46BEFDD869DE0B97D023CB9C89674FED6* ___endContainerGUIFromException_7;
	// System.Action UnityEngine.GUIUtility::guiChanged
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___guiChanged_8;
	// System.Boolean UnityEngine.GUIUtility::<guiIsExiting>k__BackingField
	bool ___U3CguiIsExitingU3Ek__BackingField_9;
	// System.Func`1<System.Boolean> UnityEngine.GUIUtility::s_HasCurrentWindowKeyFocusFunc
	Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* ___s_HasCurrentWindowKeyFocusFunc_10;
};

// UnityEngine.GUIUtility

// GameQueryServices

// GameQueryServices

// GameTaskData

// GameTaskData

// GuestData

// GuestData

// GuestNeedFoodData

// GuestNeedFoodData

// GuestTable

// GuestTable

// YooAsset.HashUtility

// YooAsset.HashUtility

// HeadAssetData

// HeadAssetData

// YooAsset.HostPlayModeImpl

// YooAsset.HostPlayModeImpl

// UnityEngine.Input

// UnityEngine.Input

// LitJson.JsonData

// LitJson.JsonData

// LitJson.JsonMapper
struct JsonMapper_t1D8418D75E295325E817EEBABCF549B289BDD24A_StaticFields
{
	// System.Int32 LitJson.JsonMapper::max_nesting_depth
	int32_t ___max_nesting_depth_0;
	// System.IFormatProvider LitJson.JsonMapper::datetime_format
	RuntimeObject* ___datetime_format_1;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc> LitJson.JsonMapper::base_exporters_table
	RuntimeObject* ___base_exporters_table_2;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc> LitJson.JsonMapper::custom_exporters_table
	RuntimeObject* ___custom_exporters_table_3;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>> LitJson.JsonMapper::base_importers_table
	RuntimeObject* ___base_importers_table_4;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>> LitJson.JsonMapper::custom_importers_table
	RuntimeObject* ___custom_importers_table_5;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ArrayMetadata> LitJson.JsonMapper::array_metadata
	RuntimeObject* ___array_metadata_6;
	// System.Object LitJson.JsonMapper::array_metadata_lock
	RuntimeObject* ___array_metadata_lock_7;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>> LitJson.JsonMapper::conv_ops
	RuntimeObject* ___conv_ops_8;
	// System.Object LitJson.JsonMapper::conv_ops_lock
	RuntimeObject* ___conv_ops_lock_9;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ObjectMetadata> LitJson.JsonMapper::object_metadata
	RuntimeObject* ___object_metadata_10;
	// System.Object LitJson.JsonMapper::object_metadata_lock
	RuntimeObject* ___object_metadata_lock_11;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IList`1<LitJson.PropertyMetadata>> LitJson.JsonMapper::type_properties
	RuntimeObject* ___type_properties_12;
	// System.Object LitJson.JsonMapper::type_properties_lock
	RuntimeObject* ___type_properties_lock_13;
	// LitJson.JsonWriter LitJson.JsonMapper::static_writer
	JsonWriter_t303427DE92107A0D92969763C95C5C20DB2D6F8F* ___static_writer_14;
	// System.Object LitJson.JsonMapper::static_writer_lock
	RuntimeObject* ___static_writer_lock_15;
};

// LitJson.JsonMapper

// LitJson.JsonMockWrapper

// LitJson.JsonMockWrapper

// LitJson.JsonReader
struct JsonReader_t848C81F0C12BBE7E0135F28350F6635928FAAABD_StaticFields
{
	// System.Collections.Generic.IDictionary`2<System.Int32,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>> LitJson.JsonReader::parse_table
	RuntimeObject* ___parse_table_0;
};

// LitJson.JsonReader

// UnityEngine.JsonUtility

// UnityEngine.JsonUtility

// LitJson.JsonWriter
struct JsonWriter_t303427DE92107A0D92969763C95C5C20DB2D6F8F_StaticFields
{
	// System.Globalization.NumberFormatInfo LitJson.JsonWriter::number_format
	NumberFormatInfo_t8E26808B202927FEBF9064FCFEEA4D6E076E6472* ___number_format_0;
};

// LitJson.JsonWriter

// LevelData

// LevelData

// LevelGuestData

// LevelGuestData

// LitJson.Lexer
struct Lexer_t8EA7E771DBFBCCC71E85B400AF902FC3F482EBC9_StaticFields
{
	// System.Int32[] LitJson.Lexer::fsm_return_table
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___fsm_return_table_0;
	// LitJson.Lexer/StateHandler[] LitJson.Lexer::fsm_handler_table
	StateHandlerU5BU5D_tA4E04C837AF2BDFE9B351DF6813E6AB954722B93* ___fsm_handler_table_1;
};

// LitJson.Lexer

// LoadConfigursManager
struct LoadConfigursManager_tB9C80FC9048CAE6F0BD82463288DCA90FF0D3895_StaticFields
{
	// LoadConfigursManager LoadConfigursManager::instans
	LoadConfigursManager_tB9C80FC9048CAE6F0BD82463288DCA90FF0D3895* ___instans_1;
};

// LoadConfigursManager

// MainFoodCompnentAssets

// MainFoodCompnentAssets

// MainFoodItem

// MainFoodItem

// UnityEngine.Yoga.MeasureOutput

// UnityEngine.Yoga.MeasureOutput

// UnityEngine.Yoga.Native

// UnityEngine.Yoga.Native

// YooAsset.OfflinePlayModeImpl

// YooAsset.OfflinePlayModeImpl

// LitJson.OrderedDictionaryEnumerator

// LitJson.OrderedDictionaryEnumerator

// OtheFoodCompnentAssets

// OtheFoodCompnentAssets

// OtherFoodItem

// OtherFoodItem

// YooAsset.PackageBundle

// YooAsset.PackageBundle

// YooAsset.PackageManifest

// YooAsset.PackageManifest

// YooAsset.PathUtility

// YooAsset.PathUtility

// UnityEngine.Physics
struct Physics_t1244C2983AEAFA149425AFFC3DF53BC91C18ED56_StaticFields
{
	// System.Action`2<UnityEngine.PhysicsScene,Unity.Collections.NativeArray`1<UnityEngine.ModifiableContactPair>> UnityEngine.Physics::ContactModifyEvent
	Action_2_t70E17A6F8F03189031C560482454FE2D87F496F2* ___ContactModifyEvent_0;
	// System.Action`2<UnityEngine.PhysicsScene,Unity.Collections.NativeArray`1<UnityEngine.ModifiableContactPair>> UnityEngine.Physics::ContactModifyEventCCD
	Action_2_t70E17A6F8F03189031C560482454FE2D87F496F2* ___ContactModifyEventCCD_1;
};

// UnityEngine.Physics

// UnityEngine.Physics2D
struct Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D_StaticFields
{
	// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D> UnityEngine.Physics2D::m_LastDisabledRigidbody2D
	List_1_tCD5F926D25FC8BFAF39E4BE6F879C1FA11501C76* ___m_LastDisabledRigidbody2D_0;
};

// UnityEngine.Physics2D

// PlayerData
struct PlayerData_t04178AFFCFAF35DA6472B839034F672FFE5EB64A_StaticFields
{
	// PlayerData PlayerData::instance
	PlayerData_t04178AFFCFAF35DA6472B839034F672FFE5EB64A* ___instance_0;
	// PlayerDataParameter PlayerData::playerDataParame
	PlayerDataParameter_t99940E8382048E611BEA20091478EA46C9E604CA* ___playerDataParame_1;
	// System.String PlayerData::saveFille
	String_t* ___saveFille_4;
};

// PlayerData

// PlayerDataParameter

// PlayerDataParameter

// RawFoodData

// RawFoodData

// UnityEngine.RectTransformUtility
struct RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244_StaticFields
{
	// UnityEngine.Vector3[] UnityEngine.RectTransformUtility::s_Corners
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___s_Corners_0;
};

// UnityEngine.RectTransformUtility

// UnityEngine.RemoteConfigSettingsHelper

// UnityEngine.RemoteConfigSettingsHelper

// RemoteServices

// RemoteServices

// UnityEngine.RemoteSettings
struct RemoteSettings_t9DFFC747AB3E7A39DF4527F245B529A407427250_StaticFields
{
	// UnityEngine.RemoteSettings/UpdatedEventHandler UnityEngine.RemoteSettings::Updated
	UpdatedEventHandler_tB0D5A5BA322FE093894992C29DCF51E7E12579C4* ___Updated_0;
	// System.Action UnityEngine.RemoteSettings::BeforeFetchFromServer
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___BeforeFetchFromServer_1;
	// System.Action`3<System.Boolean,System.Boolean,System.Int32> UnityEngine.RemoteSettings::Completed
	Action_3_t4730167C8E7EB19F1E0034580790A915D549F6CB* ___Completed_2;
};

// UnityEngine.RemoteSettings

// YooAsset.ResourcePackage

// YooAsset.ResourcePackage

// RestaurantData

// RestaurantData

// YooAsset.SafeProxy

// YooAsset.SafeProxy

// UnityEngine.ScrollViewState

// UnityEngine.ScrollViewState

// ShoppItemGemData

// ShoppItemGemData

// ShoppItemPropData

// ShoppItemPropData

// UnityEngine.SliderState

// UnityEngine.SliderState

// StreamingAssetsDefine

// StreamingAssetsDefine

// StreamingAssetsHelper
struct StreamingAssetsHelper_tEBE3CCE7BF20D9F8E7B22F20F6DCFBB686158685_StaticFields
{
	// System.Boolean StreamingAssetsHelper::_isInit
	bool ____isInit_0;
	// System.Collections.Generic.HashSet`1<System.String> StreamingAssetsHelper::_cacheData
	HashSet_1_tEFC6605F7DE53F71946C33FD371E53C3100F2178* ____cacheData_1;
};

// StreamingAssetsHelper

// YooAsset.StringUtility

// YooAsset.StringUtility
struct StringUtility_tA17F666E86C899041F159E0C2A19E43DDE1A6686_ThreadStaticFields
{
	// System.Text.StringBuilder YooAsset.StringUtility::_cacheBuilder
	StringBuilder_t* ____cacheBuilder_0;
};

// SystemDataFactoy
struct SystemDataFactoy_t0F295EDA05C547BBA48910AB76B4238A905BCFDB_StaticFields
{
	// SystemDataFactoy SystemDataFactoy::instans
	SystemDataFactoy_t0F295EDA05C547BBA48910AB76B4238A905BCFDB* ___instans_1;
};

// SystemDataFactoy

// UnityEngine.UIElements.TextNative

// UnityEngine.UIElements.TextNative

// UnityEngine.UIElements.UIElementsRuntimeUtilityNative
struct UIElementsRuntimeUtilityNative_t9DE2C23158D553BB693212D0D8AEAE8594E75938_StaticFields
{
	// System.Action UnityEngine.UIElements.UIElementsRuntimeUtilityNative::RepaintOverlayPanelsCallback
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___RepaintOverlayPanelsCallback_0;
	// System.Action UnityEngine.UIElements.UIElementsRuntimeUtilityNative::UpdateRuntimePanelsCallback
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___UpdateRuntimePanelsCallback_1;
};

// UnityEngine.UIElements.UIElementsRuntimeUtilityNative

// UnityEngine.UISystemProfilerApi

// UnityEngine.UISystemProfilerApi

// Unity.FontABTool.UnityFontABTool

// Unity.FontABTool.UnityFontABTool

// UnityEngine.UnityString

// UnityEngine.UnityString

// LitJson.UnityTypeBindings
struct UnityTypeBindings_t8B29FCFD0144021BCAE930AB4077026676F186BA_StaticFields
{
	// System.Boolean LitJson.UnityTypeBindings::registerd
	bool ___registerd_0;
};

// LitJson.UnityTypeBindings

// UnityEngine.WWWForm
struct WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045_StaticFields
{
	// System.Byte[] UnityEngine.WWWForm::dDash
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___dDash_0;
	// System.Byte[] UnityEngine.WWWForm::crlf
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___crlf_1;
	// System.Byte[] UnityEngine.WWWForm::contentTypeHeader
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___contentTypeHeader_2;
	// System.Byte[] UnityEngine.WWWForm::dispositionHeader
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___dispositionHeader_3;
	// System.Byte[] UnityEngine.WWWForm::endQuote
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___endQuote_4;
	// System.Byte[] UnityEngine.WWWForm::fileNameField
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___fileNameField_5;
	// System.Byte[] UnityEngine.WWWForm::ampersand
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___ampersand_6;
	// System.Byte[] UnityEngine.WWWForm::equal
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___equal_7;
};

// UnityEngine.WWWForm

// UnityEngine.WWWTranscoder
struct WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B_StaticFields
{
	// System.Byte[] UnityEngine.WWWTranscoder::ucHexChars
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___ucHexChars_0;
	// System.Byte[] UnityEngine.WWWTranscoder::lcHexChars
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___lcHexChars_1;
	// System.Byte UnityEngine.WWWTranscoder::urlEscapeChar
	uint8_t ___urlEscapeChar_2;
	// System.Byte[] UnityEngine.WWWTranscoder::urlSpace
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___urlSpace_3;
	// System.Byte[] UnityEngine.WWWTranscoder::dataSpace
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___dataSpace_4;
	// System.Byte[] UnityEngine.WWWTranscoder::urlForbidden
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___urlForbidden_5;
	// System.Byte UnityEngine.WWWTranscoder::qpEscapeChar
	uint8_t ___qpEscapeChar_6;
	// System.Byte[] UnityEngine.WWWTranscoder::qpSpace
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___qpSpace_7;
	// System.Byte[] UnityEngine.WWWTranscoder::qpForbidden
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___qpForbidden_8;
};

// UnityEngine.WWWTranscoder

// WeChatWASM.WXBase

// WeChatWASM.WXBase

// YooAsset.WebPlayModeImpl

// YooAsset.WebPlayModeImpl

// UnityEngineInternal.WebRequestUtils
struct WebRequestUtils_t23F1FB533DBFDA3BE5624D901D535B4C6EFAD443_StaticFields
{
	// System.Text.RegularExpressions.Regex UnityEngineInternal.WebRequestUtils::domainRegex
	Regex_tE773142C2BE45C5D362B0F815AFF831707A51772* ___domainRegex_0;
};

// UnityEngineInternal.WebRequestUtils

// LitJson.WriterContext

// LitJson.WriterContext

// System.Xml.XmlNode

// System.Xml.XmlNode

// System.Xml.XmlReader
struct XmlReader_t4C709DEF5F01606ECB60B638F1BD6F6E0A9116FD_StaticFields
{
	// System.UInt32 System.Xml.XmlReader::IsTextualNodeBitmap
	uint32_t ___IsTextualNodeBitmap_0;
	// System.UInt32 System.Xml.XmlReader::CanReadContentAsBitmap
	uint32_t ___CanReadContentAsBitmap_1;
	// System.UInt32 System.Xml.XmlReader::HasValueBitmap
	uint32_t ___HasValueBitmap_2;
};

// System.Xml.XmlReader

// UnityEngine.Yoga.YogaConstants

// UnityEngine.Yoga.YogaConstants

// YooAsset.YooAssetSettingsData
struct YooAssetSettingsData_tEDE002C0DE1A3DC02CCDB4E5C425CD65BF5D12D1_StaticFields
{
	// YooAsset.YooAssetSettings YooAsset.YooAssetSettingsData::_setting
	YooAssetSettings_tAA143F140144A8EE80D18FB10D26A93A841E6EE4* ____setting_0;
};

// YooAsset.YooAssetSettingsData

// YooAsset.YooAssets
struct YooAssets_tD00B5B9911F87CF8AC643076BF1ECB1F10DEBA56_StaticFields
{
	// System.Boolean YooAsset.YooAssets::_isInitialize
	bool ____isInitialize_0;
	// UnityEngine.GameObject YooAsset.YooAssets::_driver
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ____driver_1;
	// System.Collections.Generic.List`1<YooAsset.ResourcePackage> YooAsset.YooAssets::_packages
	List_1_t34A5F992A0C261FD9E072E04F6EB044CC633B43A* ____packages_2;
	// YooAsset.ResourcePackage YooAsset.YooAssets::_defaultPackage
	ResourcePackage_t6B28B6B3A6DEAB641E6CBB06F383D7B947198022* ____defaultPackage_3;
};

// YooAsset.YooAssets

// YooAsset.YooLogger
struct YooLogger_t94D44592A92415CB9ED28A4DAA5F52890FAD3CAF_StaticFields
{
	// YooAsset.ILogger YooAsset.YooLogger::Logger
	RuntimeObject* ___Logger_0;
};

// YooAsset.YooLogger

// YooResoursceManagur
struct YooResoursceManagur_t41268F368AF5E66702DC6C69966EBCF528661E8B_StaticFields
{
	// YooResoursceManagur YooResoursceManagur::instace
	YooResoursceManagur_t41268F368AF5E66702DC6C69966EBCF528661E8B* ___instace_0;
};

// YooResoursceManagur

// System.__Il2CppComDelegate

// System.__Il2CppComDelegate

// CloudFunction/<>c
struct U3CU3Ec_tD31045D24431D327D10D187D599853D003138D58_StaticFields
{
	// CloudFunction/<>c CloudFunction/<>c::<>9
	U3CU3Ec_tD31045D24431D327D10D187D599853D003138D58* ___U3CU3E9_0;
	// System.Action`1<WeChatWASM.WXCloudCallFunctionResponse> CloudFunction/<>c::<>9__3_0
	Action_1_tA67329F55CC37955DC2C1A2ACDB7B1816C26D26D* ___U3CU3E9__3_0_1;
	// System.Action`1<WeChatWASM.WXCloudCallFunctionResponse> CloudFunction/<>c::<>9__3_1
	Action_1_tA67329F55CC37955DC2C1A2ACDB7B1816C26D26D* ___U3CU3E9__3_1_2;
	// System.Action`1<WeChatWASM.WXCloudCallFunctionResponse> CloudFunction/<>c::<>9__3_2
	Action_1_tA67329F55CC37955DC2C1A2ACDB7B1816C26D26D* ___U3CU3E9__3_2_3;
};

// CloudFunction/<>c

// DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass0_0

// DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass0_0

// DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass1_0

// DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass1_0

// DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass2_0

// DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass2_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass0_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass0_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass1_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass1_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass2_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass2_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass3_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass3_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass4_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass4_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass5_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass5_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass7_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass7_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass8_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass8_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass0_0

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass0_0

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass1_0

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass1_0

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass2_0

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass2_0

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass3_0

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass3_0

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass5_0

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass5_0

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass6_0

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass6_0

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass7_0

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass7_0

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass8_0

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass8_0

// DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass0_0

// DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass0_0

// DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass1_0

// DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass1_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass0_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass0_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass10_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass10_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass11_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass11_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass12_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass12_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass13_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass13_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass14_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass14_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass15_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass15_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass16_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass16_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass17_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass17_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass18_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass18_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass19_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass19_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass1_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass1_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass20_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass20_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass21_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass21_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass22_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass22_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass23_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass23_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass24_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass24_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass25_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass25_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass26_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass26_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass27_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass27_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass28_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass28_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass2_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass2_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass30_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass30_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass31_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass31_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass32_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass32_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass33_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass33_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass34_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass34_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass37_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass37_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass3_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass3_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass41_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass41_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass5_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass5_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass7_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass7_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass8_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass8_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass9_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass9_0

// DG.Tweening.DOTweenModuleUI/Utils

// DG.Tweening.DOTweenModuleUI/Utils

// DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0

// DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0

// DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0

// DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0

// DG.Tweening.DOTweenModuleUtils/Physics

// DG.Tweening.DOTweenModuleUtils/Physics

// DrinkOpreat/<SartAddDrink>d__24

// DrinkOpreat/<SartAddDrink>d__24

// FireEffect/<BehindEffcet>d__13

// FireEffect/<BehindEffcet>d__13

// FireEffect/<CreateFireEffect>d__12

// FireEffect/<CreateFireEffect>d__12

// UnityEngine.GUILayoutUtility/LayoutCache

// UnityEngine.GUILayoutUtility/LayoutCache

// Guest/<>c__DisplayClass31_0

// Guest/<>c__DisplayClass31_0

// Guest/<GuestCone>d__33

// Guest/<GuestCone>d__33

// GuestManager/<ResetManager>d__11

// GuestManager/<ResetManager>d__11

// GuestManager/<StateGuestComeOne>d__20

// GuestManager/<StateGuestComeOne>d__20

// LitJson.JsonMapper/<>c
struct U3CU3Ec_t908A12620392AFC75CA4D67D039F5FD576166F2D_StaticFields
{
	// LitJson.JsonMapper/<>c LitJson.JsonMapper/<>c::<>9
	U3CU3Ec_t908A12620392AFC75CA4D67D039F5FD576166F2D* ___U3CU3E9_0;
	// LitJson.WrapperFactory LitJson.JsonMapper/<>c::<>9__23_0
	WrapperFactory_tB4894CC7D3A41BE3FF6BA079B4490DF5FC5C7BDD* ___U3CU3E9__23_0_1;
	// LitJson.ExporterFunc LitJson.JsonMapper/<>c::<>9__24_0
	ExporterFunc_t0E0FD6EE483CD6380339D2D7AC616079B1D5ED8A* ___U3CU3E9__24_0_2;
	// LitJson.ExporterFunc LitJson.JsonMapper/<>c::<>9__24_1
	ExporterFunc_t0E0FD6EE483CD6380339D2D7AC616079B1D5ED8A* ___U3CU3E9__24_1_3;
	// LitJson.ExporterFunc LitJson.JsonMapper/<>c::<>9__24_2
	ExporterFunc_t0E0FD6EE483CD6380339D2D7AC616079B1D5ED8A* ___U3CU3E9__24_2_4;
	// LitJson.ExporterFunc LitJson.JsonMapper/<>c::<>9__24_3
	ExporterFunc_t0E0FD6EE483CD6380339D2D7AC616079B1D5ED8A* ___U3CU3E9__24_3_5;
	// LitJson.ExporterFunc LitJson.JsonMapper/<>c::<>9__24_4
	ExporterFunc_t0E0FD6EE483CD6380339D2D7AC616079B1D5ED8A* ___U3CU3E9__24_4_6;
	// LitJson.ExporterFunc LitJson.JsonMapper/<>c::<>9__24_5
	ExporterFunc_t0E0FD6EE483CD6380339D2D7AC616079B1D5ED8A* ___U3CU3E9__24_5_7;
	// LitJson.ExporterFunc LitJson.JsonMapper/<>c::<>9__24_6
	ExporterFunc_t0E0FD6EE483CD6380339D2D7AC616079B1D5ED8A* ___U3CU3E9__24_6_8;
	// LitJson.ExporterFunc LitJson.JsonMapper/<>c::<>9__24_7
	ExporterFunc_t0E0FD6EE483CD6380339D2D7AC616079B1D5ED8A* ___U3CU3E9__24_7_9;
	// LitJson.ExporterFunc LitJson.JsonMapper/<>c::<>9__24_8
	ExporterFunc_t0E0FD6EE483CD6380339D2D7AC616079B1D5ED8A* ___U3CU3E9__24_8_10;
	// LitJson.ExporterFunc LitJson.JsonMapper/<>c::<>9__24_9
	ExporterFunc_t0E0FD6EE483CD6380339D2D7AC616079B1D5ED8A* ___U3CU3E9__24_9_11;
	// LitJson.ImporterFunc LitJson.JsonMapper/<>c::<>9__25_0
	ImporterFunc_t3D41CCB293D2761DC6862831F8BC45D580EB6AA5* ___U3CU3E9__25_0_12;
	// LitJson.ImporterFunc LitJson.JsonMapper/<>c::<>9__25_1
	ImporterFunc_t3D41CCB293D2761DC6862831F8BC45D580EB6AA5* ___U3CU3E9__25_1_13;
	// LitJson.ImporterFunc LitJson.JsonMapper/<>c::<>9__25_2
	ImporterFunc_t3D41CCB293D2761DC6862831F8BC45D580EB6AA5* ___U3CU3E9__25_2_14;
	// LitJson.ImporterFunc LitJson.JsonMapper/<>c::<>9__25_3
	ImporterFunc_t3D41CCB293D2761DC6862831F8BC45D580EB6AA5* ___U3CU3E9__25_3_15;
	// LitJson.ImporterFunc LitJson.JsonMapper/<>c::<>9__25_4
	ImporterFunc_t3D41CCB293D2761DC6862831F8BC45D580EB6AA5* ___U3CU3E9__25_4_16;
	// LitJson.ImporterFunc LitJson.JsonMapper/<>c::<>9__25_5
	ImporterFunc_t3D41CCB293D2761DC6862831F8BC45D580EB6AA5* ___U3CU3E9__25_5_17;
	// LitJson.ImporterFunc LitJson.JsonMapper/<>c::<>9__25_6
	ImporterFunc_t3D41CCB293D2761DC6862831F8BC45D580EB6AA5* ___U3CU3E9__25_6_18;
	// LitJson.ImporterFunc LitJson.JsonMapper/<>c::<>9__25_7
	ImporterFunc_t3D41CCB293D2761DC6862831F8BC45D580EB6AA5* ___U3CU3E9__25_7_19;
	// LitJson.ImporterFunc LitJson.JsonMapper/<>c::<>9__25_8
	ImporterFunc_t3D41CCB293D2761DC6862831F8BC45D580EB6AA5* ___U3CU3E9__25_8_20;
	// LitJson.ImporterFunc LitJson.JsonMapper/<>c::<>9__25_9
	ImporterFunc_t3D41CCB293D2761DC6862831F8BC45D580EB6AA5* ___U3CU3E9__25_9_21;
	// LitJson.ImporterFunc LitJson.JsonMapper/<>c::<>9__25_10
	ImporterFunc_t3D41CCB293D2761DC6862831F8BC45D580EB6AA5* ___U3CU3E9__25_10_22;
	// LitJson.ImporterFunc LitJson.JsonMapper/<>c::<>9__25_11
	ImporterFunc_t3D41CCB293D2761DC6862831F8BC45D580EB6AA5* ___U3CU3E9__25_11_23;
	// LitJson.ImporterFunc LitJson.JsonMapper/<>c::<>9__25_12
	ImporterFunc_t3D41CCB293D2761DC6862831F8BC45D580EB6AA5* ___U3CU3E9__25_12_24;
	// LitJson.ImporterFunc LitJson.JsonMapper/<>c::<>9__25_13
	ImporterFunc_t3D41CCB293D2761DC6862831F8BC45D580EB6AA5* ___U3CU3E9__25_13_25;
	// LitJson.ImporterFunc LitJson.JsonMapper/<>c::<>9__25_14
	ImporterFunc_t3D41CCB293D2761DC6862831F8BC45D580EB6AA5* ___U3CU3E9__25_14_26;
	// LitJson.WrapperFactory LitJson.JsonMapper/<>c::<>9__30_0
	WrapperFactory_tB4894CC7D3A41BE3FF6BA079B4490DF5FC5C7BDD* ___U3CU3E9__30_0_27;
	// LitJson.WrapperFactory LitJson.JsonMapper/<>c::<>9__31_0
	WrapperFactory_tB4894CC7D3A41BE3FF6BA079B4490DF5FC5C7BDD* ___U3CU3E9__31_0_28;
	// LitJson.WrapperFactory LitJson.JsonMapper/<>c::<>9__32_0
	WrapperFactory_tB4894CC7D3A41BE3FF6BA079B4490DF5FC5C7BDD* ___U3CU3E9__32_0_29;
};

// LitJson.JsonMapper/<>c

// LevelManager/<RestManager>d__12

// LevelManager/<RestManager>d__12

// LoadConfigursManager/<Configuer>d__25

// LoadConfigursManager/<Configuer>d__25

// LoadConfigursManager/<LoadFoodItem>d__30

// LoadConfigursManager/<LoadFoodItem>d__30

// LoadConfigursManager/<LoadFoodMenu>d__29

// LoadConfigursManager/<LoadFoodMenu>d__29

// LoadConfigursManager/<LoadGameData>d__26

// LoadConfigursManager/<LoadGameData>d__26

// LoadConfigursManager/<LoadLevel>d__27

// LoadConfigursManager/<LoadLevel>d__27

// LoadConfigursManager/<LoadLlvelGuset>d__28

// LoadConfigursManager/<LoadLlvelGuset>d__28

// LoadConfigursManager/<LoadMainFoodItem>d__31

// LoadConfigursManager/<LoadMainFoodItem>d__31

// LoadConfigursManager/<LoadOtherFoodItem>d__32

// LoadConfigursManager/<LoadOtherFoodItem>d__32

// LoadConfigursManager/<LoadTaskData>d__33

// LoadConfigursManager/<LoadTaskData>d__33

// OperationManage/<ResetManager>d__14

// OperationManage/<ResetManager>d__14

// OverPlanel/<CionScore>d__4

// OverPlanel/<CionScore>d__4

// PlayerData/<Read>d__9

// PlayerData/<Read>d__9

// ShoppPlanel/<ShowItemGem>d__17

// ShoppPlanel/<ShowItemGem>d__17

// ShoppPlanel/<ShowItemProp>d__15

// ShoppPlanel/<ShowItemProp>d__15

// StartHomeManager/<LoadConfigur>d__6

// StartHomeManager/<LoadConfigur>d__6

// SystemDataFactoy/<Configur>d__8

// SystemDataFactoy/<Configur>d__8

// SystemDataFactoy/<LoadConfigurs>d__10

// SystemDataFactoy/<LoadConfigurs>d__10

// SystemGameManager/<LoadData>d__24

// SystemGameManager/<LoadData>d__24

// SystemGameManager/<LoadingLevel>d__26

// SystemGameManager/<LoadingLevel>d__26

// SystemGameManager/<ResetManger>d__28

// SystemGameManager/<ResetManger>d__28

// TargetPlanle/<CloseTarget>d__4

// TargetPlanle/<CloseTarget>d__4

// TaskPlanel/<CreaAchieveTaks>d__6

// TaskPlanel/<CreaAchieveTaks>d__6

// UiManager/<TargetOpen>d__17

// UiManager/<TargetOpen>d__17

// UiMapPlane/<InitData>d__8

// UiMapPlane/<InitData>d__8

// LitJson.UnityTypeBindings/<>c
struct U3CU3Ec_t3AAEE6BBEC4C519EEB27C34FC45DFB4EB1584CE2_StaticFields
{
	// LitJson.UnityTypeBindings/<>c LitJson.UnityTypeBindings/<>c::<>9
	U3CU3Ec_t3AAEE6BBEC4C519EEB27C34FC45DFB4EB1584CE2* ___U3CU3E9_0;
	// LitJson.ExporterFunc`1<System.Type> LitJson.UnityTypeBindings/<>c::<>9__2_0
	ExporterFunc_1_t66337A5BC3E84B1D048E2D5A3E6B7DA41671BC95* ___U3CU3E9__2_0_1;
	// LitJson.ImporterFunc`2<System.String,System.Type> LitJson.UnityTypeBindings/<>c::<>9__2_1
	ImporterFunc_2_tECF771648EC0EB35785D9EE3003167BEBB8C8008* ___U3CU3E9__2_1_2;
	// System.Action`2<UnityEngine.Vector2,LitJson.JsonWriter> LitJson.UnityTypeBindings/<>c::<>9__2_2
	Action_2_tE5C1059CE8ACEDAC4083A88C76E66DE3E15B8105* ___U3CU3E9__2_2_3;
	// System.Action`2<UnityEngine.Vector3,LitJson.JsonWriter> LitJson.UnityTypeBindings/<>c::<>9__2_4
	Action_2_t3CFBD6C56B087A7CE032945C0950C5716540158A* ___U3CU3E9__2_4_4;
	// LitJson.ExporterFunc`1<UnityEngine.Vector4> LitJson.UnityTypeBindings/<>c::<>9__2_6
	ExporterFunc_1_t68203C504BE983A9A387E0769D39E83F63B0F234* ___U3CU3E9__2_6_5;
	// LitJson.ExporterFunc`1<UnityEngine.Quaternion> LitJson.UnityTypeBindings/<>c::<>9__2_7
	ExporterFunc_1_t0BDB0977957B702E553434D63D76FD4CBBF79E9D* ___U3CU3E9__2_7_6;
	// LitJson.ExporterFunc`1<UnityEngine.Color> LitJson.UnityTypeBindings/<>c::<>9__2_8
	ExporterFunc_1_t8DEC56EC9C315918327B728EF33F55CE4D9E0204* ___U3CU3E9__2_8_7;
	// LitJson.ExporterFunc`1<UnityEngine.Color32> LitJson.UnityTypeBindings/<>c::<>9__2_9
	ExporterFunc_1_tAD0CADBFAA1F3669790D9F40D2D389420D24C825* ___U3CU3E9__2_9_8;
	// LitJson.ExporterFunc`1<UnityEngine.Rect> LitJson.UnityTypeBindings/<>c::<>9__2_11
	ExporterFunc_1_t396C38F979C2517DA37B85C04250C0FB38012D32* ___U3CU3E9__2_11_9;
	// LitJson.ExporterFunc`1<UnityEngine.RectOffset> LitJson.UnityTypeBindings/<>c::<>9__2_12
	ExporterFunc_1_t51D2BA9CE9D91E588EEC4288A798B6F13E55C90B* ___U3CU3E9__2_12_10;
};

// LitJson.UnityTypeBindings/<>c

// LitJson.UnityTypeBindings/<>c__DisplayClass2_0

// LitJson.UnityTypeBindings/<>c__DisplayClass2_0

// WeChatWASM.WXBase/<>c__DisplayClass74_0

// WeChatWASM.WXBase/<>c__DisplayClass74_0

// WXProfileStatsScript/ProfValue

// WXProfileStatsScript/ProfValue

// WXTouchInputOverride/<>c
struct U3CU3Ec_tB5CF89FA9196447842235712836756FE02414173_StaticFields
{
	// WXTouchInputOverride/<>c WXTouchInputOverride/<>c::<>9
	U3CU3Ec_tB5CF89FA9196447842235712836756FE02414173* ___U3CU3E9_0;
	// System.Predicate`1<TouchData> WXTouchInputOverride/<>c::<>9__14_0
	Predicate_1_t52132B0333B4BB28E1E9EC424EAED5B7E2B648C6* ___U3CU3E9__14_0_1;
};

// WXTouchInputOverride/<>c

// YooResoursceManagur/<InintCuble>d__6

// YooResoursceManagur/<InintCuble>d__6

// UnityEngine.AnimatorClipInfo

// UnityEngine.AnimatorClipInfo

// UnityEngine.AnimatorStateInfo

// UnityEngine.AnimatorStateInfo

// UnityEngine.AnimatorTransitionInfo

// UnityEngine.AnimatorTransitionInfo

// LitJson.ArrayMetadata

// LitJson.ArrayMetadata

// UnityEngine.AssetFileNameExtensionAttribute

// UnityEngine.AssetFileNameExtensionAttribute

// YooAsset.CRC32Algorithm
struct CRC32Algorithm_t586EF2FEB80231B16D50813BE2ED0DC1DFD2014A_StaticFields
{
	// YooAsset.SafeProxy YooAsset.CRC32Algorithm::_proxy
	SafeProxy_tB77642C8A36A3783EB9DFAC1415459DE6B11CF7E* ____proxy_5;
};

// YooAsset.CRC32Algorithm

// System.Configuration.ConfigurationCollectionAttribute

// System.Configuration.ConfigurationCollectionAttribute

// System.Configuration.ConfigurationElementCollection

// System.Configuration.ConfigurationElementCollection

// System.Configuration.ConfigurationSection

// System.Configuration.ConfigurationSection

// YooAsset.DecryptFileInfo

// YooAsset.DecryptFileInfo

// YooAsset.DeliveryFileInfo

// YooAsset.DeliveryFileInfo

// UnityEngine.Animations.DiscreteEvaluationAttribute

// UnityEngine.Animations.DiscreteEvaluationAttribute

// DotfuscatorAttribute

// DotfuscatorAttribute

// UnityEngine.UIElements.UIR.DrawBufferRange

// UnityEngine.UIElements.UIR.DrawBufferRange

// Microsoft.CodeAnalysis.EmbeddedAttribute

// Microsoft.CodeAnalysis.EmbeddedAttribute

// YooAsset.EncryptFileInfo

// YooAsset.EncryptFileInfo

// YooAsset.EncryptResult

// YooAsset.EncryptResult

// EventAddDrink

// EventAddDrink

// EventAddOtherFood

// EventAddOtherFood

// EventDeliverCookedFood

// EventDeliverCookedFood

// UnityEngine.EventInterests

// UnityEngine.EventInterests

// UnityEngine.TextCore.FaceInfo

// UnityEngine.TextCore.FaceInfo

// UnityEngine.TextCore.LowLevel.FontEngineUtilities

// UnityEngine.TextCore.LowLevel.FontEngineUtilities

// UnityEngine.TextCore.LowLevel.FontReference

// UnityEngine.TextCore.LowLevel.FontReference

// UnityEngine.GUITargetAttribute

// UnityEngine.GUITargetAttribute

// UnityEngine.TextCore.GlyphMetrics

// UnityEngine.TextCore.GlyphMetrics

// UnityEngine.TextCore.GlyphRect
struct GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D_StaticFields
{
	// UnityEngine.TextCore.GlyphRect UnityEngine.TextCore.GlyphRect::s_ZeroGlyphRect
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___s_ZeroGlyphRect_4;
};

// UnityEngine.TextCore.GlyphRect

// UnityEngine.TextCore.LowLevel.GlyphValueRecord

// UnityEngine.TextCore.LowLevel.GlyphValueRecord

// UnityEngine.Bindings.IgnoreAttribute

// UnityEngine.Bindings.IgnoreAttribute

// System.Runtime.CompilerServices.IsReadOnlyAttribute

// System.Runtime.CompilerServices.IsReadOnlyAttribute

// LitJson.Extensions.JsonIgnore

// LitJson.Extensions.JsonIgnore

// UnityEngine.Bindings.NativeAsStructAttribute

// UnityEngine.Bindings.NativeAsStructAttribute

// UnityEngine.NativeClassAttribute

// UnityEngine.NativeClassAttribute

// UnityEngine.Bindings.NativeConditionalAttribute

// UnityEngine.Bindings.NativeConditionalAttribute

// UnityEngine.Bindings.NativeHeaderAttribute

// UnityEngine.Bindings.NativeHeaderAttribute

// UnityEngine.Bindings.NativeMethodAttribute

// UnityEngine.Bindings.NativeMethodAttribute

// UnityEngine.Bindings.NativeNameAttribute

// UnityEngine.Bindings.NativeNameAttribute

// UnityEngine.Bindings.NativeThrowsAttribute

// UnityEngine.Bindings.NativeThrowsAttribute

// UnityEngine.Bindings.NativeTypeAttribute

// UnityEngine.Bindings.NativeTypeAttribute

// UnityEngine.Animations.NotKeyableAttribute

// UnityEngine.Animations.NotKeyableAttribute

// UnityEngine.Bindings.NotNullAttribute

// UnityEngine.Bindings.NotNullAttribute

// LitJson.ObjectMetadata

// LitJson.ObjectMetadata

// UnityEngine.PhysicsScene

// UnityEngine.PhysicsScene

// UnityEngine.PhysicsScene2D

// UnityEngine.PhysicsScene2D

// UnityEngine.Bindings.PreventReadOnlyInstanceModificationAttribute

// UnityEngine.Bindings.PreventReadOnlyInstanceModificationAttribute

// LitJson.PropertyMetadata

// LitJson.PropertyMetadata

// UnityEngine.Scripting.RequiredByNativeCodeAttribute

// UnityEngine.Scripting.RequiredByNativeCodeAttribute

// UnityEngine.SharedBetweenAnimatorsAttribute

// UnityEngine.SharedBetweenAnimatorsAttribute

// UnityEngine.Bindings.StaticAccessorAttribute

// UnityEngine.Bindings.StaticAccessorAttribute

// UnityEngine.ThreadAndSerializationSafeAttribute

// UnityEngine.ThreadAndSerializationSafeAttribute

// UnityEngine.UILineInfo

// UnityEngine.UILineInfo

// UnityEngine.UnityEngineModuleAssembly

// UnityEngine.UnityEngineModuleAssembly

// UnityEngine.Bindings.UnmarshalledAttribute

// UnityEngine.Bindings.UnmarshalledAttribute

// UnityEngine.Scripting.UsedByNativeCodeAttribute

// UnityEngine.Scripting.UsedByNativeCodeAttribute

// UnityEngine.Bindings.VisibleToOtherModulesAttribute

// UnityEngine.Bindings.VisibleToOtherModulesAttribute

// WeChatWASM.WX

// WeChatWASM.WX

// UnityEngine.WritableAttribute

// UnityEngine.WritableAttribute

// UnityEngine.Yoga.YogaSize

// UnityEngine.Yoga.YogaSize

// UnityEngine.Yoga.YogaValue

// UnityEngine.Yoga.YogaValue

// a

// a

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=112

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=112

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12

// DG.Tweening.DOTweenCYInstruction/WaitForCompletion

// DG.Tweening.DOTweenCYInstruction/WaitForCompletion

// DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops

// DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops

// DG.Tweening.DOTweenCYInstruction/WaitForKill

// DG.Tweening.DOTweenCYInstruction/WaitForKill

// DG.Tweening.DOTweenCYInstruction/WaitForPosition

// DG.Tweening.DOTweenCYInstruction/WaitForPosition

// DG.Tweening.DOTweenCYInstruction/WaitForRewind

// DG.Tweening.DOTweenCYInstruction/WaitForRewind

// DG.Tweening.DOTweenCYInstruction/WaitForStart

// DG.Tweening.DOTweenCYInstruction/WaitForStart

// UnityEngine.GUIClip/ParentClipScope

// UnityEngine.GUIClip/ParentClipScope

// UnityEngine.SendMouseEvents/HitInfo

// UnityEngine.SendMouseEvents/HitInfo

// b/a

// b/a

// b/b

// b/b

// b/d

// b/d

// b/e

// b/e

// c/a

// c/a

// c/c

// c/c

// c/d

// c/d

// c/e

// c/e

// d/a

// d/a

// d/b

// d/b

// d/c

// d/c

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t9B0B2A8DD3C1F36C25925A26EB3ED0CB0DF8189E_StaticFields
{
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::59B7E757844D3BF9299877BD1C17451611BFBAB493374D6B18D973FDE534151A
	__StaticArrayInitTypeSizeU3D12_t2075943EB4AAA12F6AE68BBCD8EFE67B74570C3B ___59B7E757844D3BF9299877BD1C17451611BFBAB493374D6B18D973FDE534151A_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::5B979F69B96A61586A09DD4ED26F20534C629B08732AE32FA34B6F8A0049ACDD
	__StaticArrayInitTypeSizeU3D12_t2075943EB4AAA12F6AE68BBCD8EFE67B74570C3B ___5B979F69B96A61586A09DD4ED26F20534C629B08732AE32FA34B6F8A0049ACDD_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::7FCA397EF25DB53B2C58A05F9BBFA4E8E0685FDBC8AAF39536123447056895C0
	__StaticArrayInitTypeSizeU3D12_t2075943EB4AAA12F6AE68BBCD8EFE67B74570C3B ___7FCA397EF25DB53B2C58A05F9BBFA4E8E0685FDBC8AAF39536123447056895C0_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::B257E85A5D3CAB5E738D5A0FD1A7AE96624BFE92CB7915726CBBE1518C3225CF
	__StaticArrayInitTypeSizeU3D12_t2075943EB4AAA12F6AE68BBCD8EFE67B74570C3B ___B257E85A5D3CAB5E738D5A0FD1A7AE96624BFE92CB7915726CBBE1518C3225CF_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::B7A918F6F138CA8137025633559198B529D28CCEC5A51B005376CC69A5B83D85
	__StaticArrayInitTypeSizeU3D12_t2075943EB4AAA12F6AE68BBCD8EFE67B74570C3B ___B7A918F6F138CA8137025633559198B529D28CCEC5A51B005376CC69A5B83D85_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=112 <PrivateImplementationDetails>::C84227140A8A7787B30DFF8BD5693C19AA5A430C4E89FFD0256D7F77B3FEAD82
	__StaticArrayInitTypeSizeU3D112_t3634DC8000B09BC9B0CCFCCA5B7FF92AE5FB2971 ___C84227140A8A7787B30DFF8BD5693C19AA5A430C4E89FFD0256D7F77B3FEAD82_5;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::FC03ECB1D62767A5B31230F548FA4D4C02F01E3DA8CA1FC66C7C332EA16BF206
	__StaticArrayInitTypeSizeU3D12_t2075943EB4AAA12F6AE68BBCD8EFE67B74570C3B ___FC03ECB1D62767A5B31230F548FA4D4C02F01E3DA8CA1FC66C7C332EA16BF206_6;
};

// <PrivateImplementationDetails>

// UnityEngine.Animations.AnimationHumanStream

// UnityEngine.Animations.AnimationHumanStream

// UnityEngine.Animations.AnimationStream

// UnityEngine.Animations.AnimationStream

// UnityEngine.Networking.CertificateHandler

// UnityEngine.Networking.CertificateHandler

// UnityEngine.Collision

// UnityEngine.Collision

// UnityEngine.Collision2D

// UnityEngine.Collision2D

// UnityEngine.ContactFilter2D

// UnityEngine.ContactFilter2D

// UnityEngine.ContactPoint

// UnityEngine.ContactPoint

// UnityEngine.ContactPoint2D

// UnityEngine.ContactPoint2D

// UnityEngine.ControllerColliderHit

// UnityEngine.ControllerColliderHit

// UnityEngine.Networking.DownloadHandler

// UnityEngine.Networking.DownloadHandler

// UnityEngine.Event
struct Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB_StaticFields
{
	// UnityEngine.Event UnityEngine.Event::s_Current
	Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB* ___s_Current_1;
	// UnityEngine.Event UnityEngine.Event::s_MasterEvent
	Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB* ___s_MasterEvent_2;
};

// UnityEngine.Event

// UnityEngine.Bindings.FreeFunctionAttribute

// UnityEngine.Bindings.FreeFunctionAttribute

// UnityEngine.GUI
struct GUI_tA9CDB3D69DB13D51AD83ABDB587EF95947EC2D2A_StaticFields
{
	// System.Int32 UnityEngine.GUI::s_HotTextField
	int32_t ___s_HotTextField_0;
	// System.Int32 UnityEngine.GUI::s_BoxHash
	int32_t ___s_BoxHash_1;
	// System.Int32 UnityEngine.GUI::s_ButonHash
	int32_t ___s_ButonHash_2;
	// System.Int32 UnityEngine.GUI::s_RepeatButtonHash
	int32_t ___s_RepeatButtonHash_3;
	// System.Int32 UnityEngine.GUI::s_ToggleHash
	int32_t ___s_ToggleHash_4;
	// System.Int32 UnityEngine.GUI::s_ButtonGridHash
	int32_t ___s_ButtonGridHash_5;
	// System.Int32 UnityEngine.GUI::s_SliderHash
	int32_t ___s_SliderHash_6;
	// System.Int32 UnityEngine.GUI::s_BeginGroupHash
	int32_t ___s_BeginGroupHash_7;
	// System.Int32 UnityEngine.GUI::s_ScrollviewHash
	int32_t ___s_ScrollviewHash_8;
	// System.DateTime UnityEngine.GUI::<nextScrollStepTime>k__BackingField
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___U3CnextScrollStepTimeU3Ek__BackingField_9;
	// UnityEngine.GUISkin UnityEngine.GUI::s_Skin
	GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9* ___s_Skin_10;
	// UnityEngine.Rect UnityEngine.GUI::s_ToolTipRect
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___s_ToolTipRect_11;
	// UnityEngineInternal.GenericStack UnityEngine.GUI::<scrollViewStates>k__BackingField
	GenericStack_t1FB49AB7D847C97ABAA97AB232CA416CABD24C49* ___U3CscrollViewStatesU3Ek__BackingField_12;
};

// UnityEngine.GUI

// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F_StaticFields
{
	// UnityEngine.Rect UnityEngine.GUILayoutEntry::kDummyRect
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___kDummyRect_9;
	// System.Int32 UnityEngine.GUILayoutEntry::indent
	int32_t ___indent_10;
};

// UnityEngine.GUILayoutEntry

// UnityEngine.GUILayoutUtility
struct GUILayoutUtility_t48D00CD11CFC1E09E8EC2E51D59E735F5D24836F_StaticFields
{
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache> UnityEngine.GUILayoutUtility::s_StoredLayouts
	Dictionary_2_tD74A089D3CFE69E54B1617003276B07F5B82B598* ___s_StoredLayouts_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache> UnityEngine.GUILayoutUtility::s_StoredWindows
	Dictionary_2_tD74A089D3CFE69E54B1617003276B07F5B82B598* ___s_StoredWindows_1;
	// UnityEngine.GUILayoutUtility/LayoutCache UnityEngine.GUILayoutUtility::current
	LayoutCache_tF844B2FAD6933B78FD5EFEBDE0529BCBAC19BA60* ___current_2;
	// UnityEngine.Rect UnityEngine.GUILayoutUtility::kDummyRect
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___kDummyRect_3;
	// System.Int32 UnityEngine.GUILayoutUtility::<unbalancedgroupscount>k__BackingField
	int32_t ___U3CunbalancedgroupscountU3Ek__BackingField_4;
};

// UnityEngine.GUILayoutUtility

// UnityEngine.GUISettings

// UnityEngine.GUISettings

// UnityEngine.GUIStyleState

// UnityEngine.GUIStyleState

// UnityEngine.UIElements.UIR.GfxUpdateBufferRange

// UnityEngine.UIElements.UIR.GfxUpdateBufferRange

// UnityEngine.TextCore.Glyph

// UnityEngine.TextCore.Glyph

// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord

// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord

// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct

// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct

// UnityEngine.HumanLimit

// UnityEngine.HumanLimit

// System.Configuration.IgnoreSection

// System.Configuration.IgnoreSection

// UnityEngine.ModifiableContactPair

// UnityEngine.ModifiableContactPair

// UnityEngine.Bindings.NativePropertyAttribute

// UnityEngine.Bindings.NativePropertyAttribute

// UnityEngine.ObjectGUIState

// UnityEngine.ObjectGUIState

// UnityEngine.RaycastHit

// UnityEngine.RaycastHit

// UnityEngine.RaycastHit2D

// UnityEngine.RaycastHit2D

// UnityEngine.RemoteConfigSettings

// UnityEngine.RemoteConfigSettings

// UnityEngine.SendMouseEvents
struct SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields
{
	// System.Boolean UnityEngine.SendMouseEvents::s_MouseUsed
	bool ___s_MouseUsed_0;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_LastHit
	HitInfoU5BU5D_tDAE7DF0D2B0BE3EB2FD25FB4418704E27A2BF1D5* ___m_LastHit_1;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_MouseDownHit
	HitInfoU5BU5D_tDAE7DF0D2B0BE3EB2FD25FB4418704E27A2BF1D5* ___m_MouseDownHit_2;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_CurrentHit
	HitInfoU5BU5D_tDAE7DF0D2B0BE3EB2FD25FB4418704E27A2BF1D5* ___m_CurrentHit_3;
	// UnityEngine.Camera[] UnityEngine.SendMouseEvents::m_Cameras
	CameraU5BU5D_t1506EBA524A07AD1066D6DD4D7DFC6721F1AC26B* ___m_Cameras_4;
	// System.Func`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Vector2>> UnityEngine.SendMouseEvents::s_GetMouseState
	Func_1_tF5F7F5DCF1679E08B2536581A6E1EEF5529155C9* ___s_GetMouseState_5;
	// UnityEngine.Vector2 UnityEngine.SendMouseEvents::s_MousePosition
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___s_MousePosition_6;
	// System.Boolean UnityEngine.SendMouseEvents::s_MouseButtonPressedThisFrame
	bool ___s_MouseButtonPressedThisFrame_7;
	// System.Boolean UnityEngine.SendMouseEvents::s_MouseButtonIsPressed
	bool ___s_MouseButtonIsPressed_8;
};

// UnityEngine.SendMouseEvents

// UnityEngine.SkeletonBone

// UnityEngine.SkeletonBone

// UnityEngine.TextEditor
struct TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27_StaticFields
{
	// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp> UnityEngine.TextEditor::s_Keyactions
	Dictionary_2_t6AC338B3CEB934A66B363042F19213FE666F6818* ___s_Keyactions_23;
};

// UnityEngine.TextEditor

// UnityEngine.TextGenerationSettings

// UnityEngine.TextGenerationSettings

// UnityEngine.UIElements.TextNativeSettings

// UnityEngine.UIElements.TextNativeSettings

// UnityEngine.UIElements.TextVertex

// UnityEngine.UIElements.TextVertex

// UnityEngine.Bindings.ThreadSafeAttribute

// UnityEngine.Bindings.ThreadSafeAttribute

// UnityEngine.Touch

// UnityEngine.Touch

// UnityEngine.UICharInfo

// UnityEngine.UICharInfo

// UnityEngine.UIVertex
struct UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207_StaticFields
{
	// UnityEngine.Color32 UnityEngine.UIVertex::s_DefaultColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___s_DefaultColor_8;
	// UnityEngine.Vector4 UnityEngine.UIVertex::s_DefaultTangent
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___s_DefaultTangent_9;
	// UnityEngine.UIVertex UnityEngine.UIVertex::simpleVert
	UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207 ___simpleVert_10;
};

// UnityEngine.UIVertex

// UnityEngine.Networking.UploadHandler

// UnityEngine.Networking.UploadHandler

// UnityEngine.Yoga.YogaConfig
struct YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345_StaticFields
{
	// UnityEngine.Yoga.YogaConfig UnityEngine.Yoga.YogaConfig::Default
	YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345* ___Default_0;
};

// UnityEngine.Yoga.YogaConfig

// UnityEngine.Yoga.YogaNode

// UnityEngine.Yoga.YogaNode

// b
struct b_t972FAFF471676E1BF6780CEF716F7509668D82DA_StaticFields
{
	// System.Byte[] b::l
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___l_11;
};

// b

// d
struct d_t7410D4303AF1B382C8A19061C6DA00D2360AB7AC_StaticFields
{
	// System.Int64 d::a
	int64_t ___a_0;
	// d/c d::b
	c_t7A9F326BDE1D1BFC4A56A02D80580068ABFC2798 ___b_1;
	// d/b d::c
	b_tF461D840050343BC580EE7A263DE9FE625E47B84 ___c_2;
	// d/a d::d
	a_tC905F9A47B8F42EC7D8702A0FDD3F296EF74457B ___d_3;
};

// d

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0

// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0

// DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0

// DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass40_0

// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass40_0

// LevelManager/<SpawnRestauranYoo>d__14

// LevelManager/<SpawnRestauranYoo>d__14

// UnityEngine.ParticleSystem/Particle

// UnityEngine.ParticleSystem/Particle

// b/c

// b/c

// c/b

// c/b

// UnityEngine.Animations.AnimationClipPlayable

// UnityEngine.Animations.AnimationClipPlayable

// UnityEngine.AnimationEvent

// UnityEngine.AnimationEvent

// UnityEngine.Animations.AnimationLayerMixerPlayable
struct AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D_StaticFields
{
	// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Animations.AnimationLayerMixerPlayable::m_NullPlayable
	AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationLayerMixerPlayable

// UnityEngine.Animations.AnimationMixerPlayable
struct AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0_StaticFields
{
	// UnityEngine.Animations.AnimationMixerPlayable UnityEngine.Animations.AnimationMixerPlayable::m_NullPlayable
	AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0 ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationMixerPlayable

// UnityEngine.Animations.AnimationMotionXToDeltaPlayable
struct AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18_StaticFields
{
	// UnityEngine.Animations.AnimationMotionXToDeltaPlayable UnityEngine.Animations.AnimationMotionXToDeltaPlayable::m_NullPlayable
	AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18 ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationMotionXToDeltaPlayable

// UnityEngine.Animations.AnimationOffsetPlayable
struct AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4_StaticFields
{
	// UnityEngine.Animations.AnimationOffsetPlayable UnityEngine.Animations.AnimationOffsetPlayable::m_NullPlayable
	AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4 ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationOffsetPlayable

// UnityEngine.Animations.AnimationPlayableOutput

// UnityEngine.Animations.AnimationPlayableOutput

// UnityEngine.Animations.AnimationPosePlayable
struct AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C_StaticFields
{
	// UnityEngine.Animations.AnimationPosePlayable UnityEngine.Animations.AnimationPosePlayable::m_NullPlayable
	AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationPosePlayable

// UnityEngine.Animations.AnimationRemoveScalePlayable
struct AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD_StaticFields
{
	// UnityEngine.Animations.AnimationRemoveScalePlayable UnityEngine.Animations.AnimationRemoveScalePlayable::m_NullPlayable
	AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationRemoveScalePlayable

// UnityEngine.Animations.AnimationScriptPlayable
struct AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127_StaticFields
{
	// UnityEngine.Animations.AnimationScriptPlayable UnityEngine.Animations.AnimationScriptPlayable::m_NullPlayable
	AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127 ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationScriptPlayable

// UnityEngine.AnimationState

// UnityEngine.AnimationState

// UnityEngine.Animations.AnimatorControllerPlayable
struct AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A_StaticFields
{
	// UnityEngine.Animations.AnimatorControllerPlayable UnityEngine.Animations.AnimatorControllerPlayable::m_NullPlayable
	AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimatorControllerPlayable

// UnityEngine.AssetBundle

// UnityEngine.AssetBundle

// UnityEngine.AssetBundleCreateRequest

// UnityEngine.AssetBundleCreateRequest

// UnityEngine.AssetBundleRecompressOperation

// UnityEngine.AssetBundleRecompressOperation

// UnityEngine.AudioClip

// UnityEngine.AudioClip

// UnityEngine.Audio.AudioClipPlayable

// UnityEngine.Audio.AudioClipPlayable

// UnityEngine.Audio.AudioMixer

// UnityEngine.Audio.AudioMixer

// UnityEngine.Audio.AudioMixerPlayable

// UnityEngine.Audio.AudioMixerPlayable

// UnityEngine.Audio.AudioPlayableOutput

// UnityEngine.Audio.AudioPlayableOutput

// UnityEngine.Networking.DownloadHandlerAssetBundle

// UnityEngine.Networking.DownloadHandlerAssetBundle

// UnityEngine.Networking.DownloadHandlerBuffer

// UnityEngine.Networking.DownloadHandlerBuffer

// UnityEngine.Networking.DownloadHandlerFile

// UnityEngine.Networking.DownloadHandlerFile

// UnityEngine.Networking.DownloadHandlerScript

// UnityEngine.Networking.DownloadHandlerScript

// UnityEngine.ExitGUIException

// UnityEngine.ExitGUIException

// UnityEngine.Font
struct Font_tC95270EA3198038970422D78B74A7F2E218A96B6_StaticFields
{
	// System.Action`1<UnityEngine.Font> UnityEngine.Font::textureRebuilt
	Action_1_tD91E4D0ED3C2E385D3BDD4B3EA48B5F99D39F1DC* ___textureRebuilt_4;
};

// UnityEngine.Font

// UnityEngine.GUILayoutGroup
struct GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D_StaticFields
{
	// UnityEngine.GUILayoutEntry UnityEngine.GUILayoutGroup::none
	GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F* ___none_31;
};

// UnityEngine.GUILayoutGroup

// UnityEngine.GUIStyle
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_StaticFields
{
	// System.Boolean UnityEngine.GUIStyle::showKeyboardFocus
	bool ___showKeyboardFocus_14;
	// UnityEngine.GUIStyle UnityEngine.GUIStyle::s_None
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___s_None_15;
};

// UnityEngine.GUIStyle

// UnityEngine.GUIWordWrapSizer

// UnityEngine.GUIWordWrapSizer

// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord

// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord

// UnityEngine.HumanBone

// UnityEngine.HumanBone

// UnityEngine.Motion

// UnityEngine.Motion

// UnityEngine.RuntimeAnimatorController

// UnityEngine.RuntimeAnimatorController

// UnityEngine.TextGenerator

// UnityEngine.TextGenerator

// TouchData

// TouchData

// UnityEngine.Networking.UnityWebRequest

// UnityEngine.Networking.UnityWebRequest

// UnityEngine.Networking.UnityWebRequestAsyncOperation

// UnityEngine.Networking.UnityWebRequestAsyncOperation

// UnityEngine.UIElements.UIR.Utility
struct Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields
{
	// System.Action`1<System.Boolean> UnityEngine.UIElements.UIR.Utility::GraphicsResourcesRecreate
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* ___GraphicsResourcesRecreate_0;
	// System.Action UnityEngine.UIElements.UIR.Utility::EngineUpdate
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___EngineUpdate_1;
	// System.Action UnityEngine.UIElements.UIR.Utility::FlushPendingResources
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___FlushPendingResources_2;
	// System.Action`1<UnityEngine.Camera> UnityEngine.UIElements.UIR.Utility::RegisterIntermediateRenderers
	Action_1_t268986DA4CF361AC17B40338506A83AFB35832EA* ___RegisterIntermediateRenderers_3;
	// System.Action`1<System.IntPtr> UnityEngine.UIElements.UIR.Utility::RenderNodeAdd
	Action_1_t2DF1ED40E3084E997390FF52F462390882271FE2* ___RenderNodeAdd_4;
	// System.Action`1<System.IntPtr> UnityEngine.UIElements.UIR.Utility::RenderNodeExecute
	Action_1_t2DF1ED40E3084E997390FF52F462390882271FE2* ___RenderNodeExecute_5;
	// System.Action`1<System.IntPtr> UnityEngine.UIElements.UIR.Utility::RenderNodeCleanup
	Action_1_t2DF1ED40E3084E997390FF52F462390882271FE2* ___RenderNodeCleanup_6;
	// Unity.Profiling.ProfilerMarker UnityEngine.UIElements.UIR.Utility::s_MarkerRaiseEngineUpdate
	ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD ___s_MarkerRaiseEngineUpdate_7;
};

// UnityEngine.UIElements.UIR.Utility

// c

// c

// CloudSaveSystem/<Awake>d__1

// CloudSaveSystem/<Awake>d__1

// ComparePlayerDataPlanel/<SaveCloundData>d__9

// ComparePlayerDataPlanel/<SaveCloundData>d__9

// UnityEngine.ParticleSystem/EmitParams

// UnityEngine.ParticleSystem/EmitParams

// SystemGameManager/<SaveCloundData>d__31

// SystemGameManager/<SaveCloundData>d__31

// UnityEngine.AnimationClip

// UnityEngine.AnimationClip

// UnityEngine.AnimatorOverrideController

// UnityEngine.AnimatorOverrideController

// UnityEngine.AssetBundleRequest

// UnityEngine.AssetBundleRequest

// UnityEngine.Yoga.BaselineFunction

// UnityEngine.Yoga.BaselineFunction

// BuildinFileManifest

// BuildinFileManifest

// UnityEngine.CanvasRenderer

// UnityEngine.CanvasRenderer

// UnityEngine.Collider

// UnityEngine.Collider

// LitJson.ExporterFunc

// LitJson.ExporterFunc

// UnityEngine.GUIScrollGroup

// UnityEngine.GUIScrollGroup

// UnityEngine.GUISkin
struct GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9_StaticFields
{
	// UnityEngine.GUIStyle UnityEngine.GUISkin::ms_Error
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___ms_Error_30;
	// UnityEngine.GUISkin/SkinChangedDelegate UnityEngine.GUISkin::m_SkinChanged
	SkinChangedDelegate_tA6D456E853D58AD2EF8A599F543C7E5BA8E94B98* ___m_SkinChanged_32;
	// UnityEngine.GUISkin UnityEngine.GUISkin::current
	GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9* ___current_33;
};

// UnityEngine.GUISkin

// LitJson.ImporterFunc

// LitJson.ImporterFunc

// LitJson.JsonException

// LitJson.JsonException

// UnityEngine.Yoga.Logger

// UnityEngine.Yoga.Logger

// UnityEngine.Yoga.MeasureFunction

// UnityEngine.Yoga.MeasureFunction

// UnityEngine.ParticleSystem

// UnityEngine.ParticleSystem

// ResourceProfile

// ResourceProfile

// UnityEngine.Rigidbody

// UnityEngine.Rigidbody

// UnityEngine.Rigidbody2D

// UnityEngine.Rigidbody2D

// UnityEngine.StateMachineBehaviour

// UnityEngine.StateMachineBehaviour

// LitJson.WrapperFactory

// LitJson.WrapperFactory

// YooAsset.YooAssetSettings

// YooAsset.YooAssetSettings

// UnityEngine.Analytics.AnalyticsSessionInfo/IdentityTokenChanged

// UnityEngine.Analytics.AnalyticsSessionInfo/IdentityTokenChanged

// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged

// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged

// UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback

// UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback

// UnityEngine.AudioClip/PCMReaderCallback

// UnityEngine.AudioClip/PCMReaderCallback

// UnityEngine.AudioClip/PCMSetPositionCallback

// UnityEngine.AudioClip/PCMSetPositionCallback

// UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler

// UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler

// UnityEngine.AudioSettings/AudioConfigurationChangeHandler

// UnityEngine.AudioSettings/AudioConfigurationChangeHandler

// UnityEngine.Canvas/WillRenderCanvases

// UnityEngine.Canvas/WillRenderCanvases

// CloudSaveSystem/<ForceSaveSingleData>d__4

// CloudSaveSystem/<ForceSaveSingleData>d__4

// CloudSaveSystem/<ListAllKeys>d__3

// CloudSaveSystem/<ListAllKeys>d__3

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15

// UnityEngine.Font/FontTextureRebuildCallback

// UnityEngine.Font/FontTextureRebuildCallback

// UnityEngine.GUI/WindowFunction

// UnityEngine.GUI/WindowFunction

// UnityEngine.GUISkin/SkinChangedDelegate

// UnityEngine.GUISkin/SkinChangedDelegate

// LitJson.Lexer/StateHandler

// LitJson.Lexer/StateHandler

// UnityEngine.RemoteSettings/UpdatedEventHandler

// UnityEngine.RemoteSettings/UpdatedEventHandler

// UnityEngine.Animator

// UnityEngine.Animator

// UnityEngine.AudioBehaviour

// UnityEngine.AudioBehaviour

// UnityEngine.BoxCollider

// UnityEngine.BoxCollider

// UnityEngine.Canvas
struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_StaticFields
{
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::preWillRenderCanvases
	WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC* ___preWillRenderCanvases_4;
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC* ___willRenderCanvases_5;
	// System.Action`1<System.Int32> UnityEngine.Canvas::<externBeginRenderOverlays>k__BackingField
	Action_1_tD69A6DC9FBE94131E52F5A73B2A9D4AB51EEC404* ___U3CexternBeginRenderOverlaysU3Ek__BackingField_6;
	// System.Action`2<System.Int32,System.Int32> UnityEngine.Canvas::<externRenderOverlaysBefore>k__BackingField
	Action_2_tD7438462601D3939500ED67463331FE00CFFBDB8* ___U3CexternRenderOverlaysBeforeU3Ek__BackingField_7;
	// System.Action`1<System.Int32> UnityEngine.Canvas::<externEndRenderOverlays>k__BackingField
	Action_1_tD69A6DC9FBE94131E52F5A73B2A9D4AB51EEC404* ___U3CexternEndRenderOverlaysU3Ek__BackingField_8;
};

// UnityEngine.Canvas

// UnityEngine.CanvasGroup

// UnityEngine.CanvasGroup

// UnityEngine.CapsuleCollider

// UnityEngine.CapsuleCollider

// UnityEngine.CharacterController

// UnityEngine.CharacterController

// UnityEngine.Collider2D

// UnityEngine.Collider2D

// FoodMenuConfigur

// FoodMenuConfigur

// UnityEngine.GridLayout

// UnityEngine.GridLayout

// GuestConfigur

// GuestConfigur

// HeadAssetConfigur

// HeadAssetConfigur

// UnityEngine.Joint2D

// UnityEngine.Joint2D

// MainFoodConfigur

// MainFoodConfigur

// UnityEngine.MeshCollider

// UnityEngine.MeshCollider

// OtherFoodConfigur

// OtherFoodConfigur

// UnityEngine.ParticleSystemRenderer

// UnityEngine.ParticleSystemRenderer

// RawFoodAssetsConfigur

// RawFoodAssetsConfigur

// RestaurantAssetConfiger

// RestaurantAssetConfiger

// ShoppItemConfigur

// ShoppItemConfigur

// UnityEngine.SphereCollider

// UnityEngine.SphereCollider

// UnityEngine.U2D.SpriteShapeRenderer

// UnityEngine.U2D.SpriteShapeRenderer

// UnityEngine.Tilemaps.TilemapRenderer

// UnityEngine.Tilemaps.TilemapRenderer

// AESUtils

// AESUtils

// UnityEngine.AudioListener

// UnityEngine.AudioListener

// UnityEngine.AudioSource

// UnityEngine.AudioSource

// CheckFrame

// CheckFrame

// CloudSaveSystem
struct CloudSaveSystem_t419542980442B82C4BB089FE4FFEDD48855C1AA6_StaticFields
{
	// CloudSaveSystem CloudSaveSystem::instans
	CloudSaveSystem_t419542980442B82C4BB089FE4FFEDD48855C1AA6* ___instans_4;
};

// CloudSaveSystem

// ControllManager

// ControllManager

// CookedFoodOpreat

// CookedFoodOpreat

// DamondPlane

// DamondPlane

// DataPlanel

// DataPlanel

// DrinkOpreat

// DrinkOpreat

// FireEffect

// FireEffect

// FoodIDGenerate

// FoodIDGenerate

// FoodProperty

// FoodProperty

// Guest

// Guest

// GuestManager

// GuestManager

// GuestParentInfo

// GuestParentInfo

// GuideInfo

// GuideInfo

// HeadItem

// HeadItem

// HideLoadingPage

// HideLoadingPage

// HomeMapUiManager

// HomeMapUiManager

// HomePlanel

// HomePlanel

// LevelManager

// LevelManager

// LevelSelect

// LevelSelect

// MakinFoodOpreat

// MakinFoodOpreat

// MapManager

// MapManager

// NewBehaviourScript

// NewBehaviourScript

// OperationManage

// OperationManage

// OtherFood

// OtherFood

// PlateItem

// PlateItem

// RawFoodOpreat

// RawFoodOpreat

// RestaurantInfo

// RestaurantInfo

// StartHomeManager

// StartHomeManager

// SystemEventManager
struct SystemEventManager_t7316E370F40FC805D47CEFFCBE61D389BB43726D_StaticFields
{
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`2<System.String,EventParamProperties>> SystemEventManager::singleEventDictionary
	Dictionary_2_t33B19FEC6E64560F9692E9EEFF3D7968ECF0F0E1* ___singleEventDictionary_5;
};

// SystemEventManager

// SystemGameManager

// SystemGameManager

// SystemGuideEvent
struct SystemGuideEvent_t94EA5FAE785DE9FBADF02F5688797C234FFBBA09_StaticFields
{
	// SystemGuideEvent SystemGuideEvent::instans
	SystemGuideEvent_t94EA5FAE785DE9FBADF02F5688797C234FFBBA09* ___instans_4;
};

// SystemGuideEvent

// SystmConfigurationManager

// SystmConfigurationManager

// TaskItem

// TaskItem

// Unity.ThrowStub

// Unity.ThrowStub

// UnityEngine.Tilemaps.Tilemap

// UnityEngine.Tilemaps.Tilemap

// UIText

// UIText

// UiLoadIngSlider

// UiLoadIngSlider

// UiManager

// UiManager

// UiWoPropAbility

// UiWoPropAbility

// WXProfileStatsScript

// WXProfileStatsScript

// YooAsset.YooAssetsDriver
struct YooAssetsDriver_tB0C2B38D94EC1806129CAEA70F4385E5D34F81D5_StaticFields
{
	// System.Int32 YooAsset.YooAssetsDriver::LastestUpdateFrame
	int32_t ___LastestUpdateFrame_4;
};

// YooAsset.YooAssetsDriver

// ComparePlayerDataPlanel

// ComparePlayerDataPlanel

// GameHomePlanel

// GameHomePlanel

// GreensFood

// GreensFood

// HeadPlanel

// HeadPlanel

// LevelInfoPlane

// LevelInfoPlane

// LoadinPlanle

// LoadinPlanle

// OverPlanel

// OverPlanel

// ShoppPlanel

// ShoppPlanel

// SteakFood

// SteakFood

// TargetPlanle

// TargetPlanle

// TaskPlanel

// TaskPlanel

// UiMapPlane

// UiMapPlane

// lockPlanel

// lockPlanel

// otherFoodTypeTwo

// otherFoodTypeTwo

// WXTouchInputOverride

// WXTouchInputOverride
#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5400;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5400 = { sizeof(PackageBundle_t9B92F4FF721DB05C09F5CF9931F0E3FB837E8497), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5401;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5401 = { sizeof(PackageManifest_tA9B8D10262EB5900AEB4329BF3B22F0353554CB5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5402;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5402 = { sizeof(EditorSimulateModeHelper_tF14016CBA4F82CC8D27F07AB549E166097A0A73C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5403;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5403 = { sizeof(EditorSimulateModeImpl_t6FFD2482D3EE3B0A9FB7D870E4921BE917BB07F5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5404;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5404 = { sizeof(HostPlayModeImpl_tF24BE63DEB824F68227C036C6E13D63717D7D468), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5405;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5405 = { sizeof(OfflinePlayModeImpl_t9AC55276869B54227773FCA129929444BF8AAF1C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5406;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5406 = { sizeof(WebPlayModeImpl_t6CCCA49CBDF02BC94394630444EBEA208669491E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5407;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5407 = { sizeof(ResourcePackage_t6B28B6B3A6DEAB641E6CBB06F383D7B947198022), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5408;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5408 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5409;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5409 = { sizeof(DecryptFileInfo_tFE2E173F2F6F4CFC1FF46B133FD856B9EA382321)+ sizeof(RuntimeObject), sizeof(DecryptFileInfo_tFE2E173F2F6F4CFC1FF46B133FD856B9EA382321_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5410;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5410 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5411;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5411 = { sizeof(DeliveryFileInfo_tBCAABBA12CED034E89403C0AEA3BF4D4EA823E7C)+ sizeof(RuntimeObject), sizeof(DeliveryFileInfo_tBCAABBA12CED034E89403C0AEA3BF4D4EA823E7C_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5412;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5412 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5413;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5413 = { sizeof(EncryptResult_tF9D7E801F11C360586AA064C31197C0BAC9DB26D)+ sizeof(RuntimeObject), sizeof(EncryptResult_tF9D7E801F11C360586AA064C31197C0BAC9DB26D_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5414;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5414 = { sizeof(EncryptFileInfo_tDAA38B9CEBCA459FF3F31DB5DA2B6103979FA18B)+ sizeof(RuntimeObject), sizeof(EncryptFileInfo_tDAA38B9CEBCA459FF3F31DB5DA2B6103979FA18B_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5415;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5415 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5416;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5416 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5417;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5417 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5418;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5418 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5419;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5419 = { sizeof(YooAssetSettings_tAA143F140144A8EE80D18FB10D26A93A841E6EE4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5420;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5420 = { sizeof(YooAssetSettingsData_tEDE002C0DE1A3DC02CCDB4E5C425CD65BF5D12D1), -1, sizeof(YooAssetSettingsData_tEDE002C0DE1A3DC02CCDB4E5C425CD65BF5D12D1_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5421;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5421 = { sizeof(BufferReader_t058D020315958551EDFFB188DDAB89E05EF5112A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5422;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5422 = { sizeof(BufferWriter_tB11C275753FDCC1BA6675F7676FA91387456F886), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5423;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5423 = { sizeof(SafeProxy_tB77642C8A36A3783EB9DFAC1415459DE6B11CF7E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5424;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5424 = { sizeof(CRC32Algorithm_t586EF2FEB80231B16D50813BE2ED0DC1DFD2014A), -1, sizeof(CRC32Algorithm_t586EF2FEB80231B16D50813BE2ED0DC1DFD2014A_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5425;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5425 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5426;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5426 = { sizeof(YooLogger_t94D44592A92415CB9ED28A4DAA5F52890FAD3CAF), -1, sizeof(YooLogger_t94D44592A92415CB9ED28A4DAA5F52890FAD3CAF_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5427;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5427 = { sizeof(PathUtility_t820434FFD3142C7DEBC33F8C6DA67F3C5068BBD7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5428;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5428 = { sizeof(StringUtility_tA17F666E86C899041F159E0C2A19E43DDE1A6686), -1, 0, sizeof(StringUtility_tA17F666E86C899041F159E0C2A19E43DDE1A6686_ThreadStaticFields) };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5429;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5429 = { sizeof(FileUtility_t5449A663405CBC365480B6DCAC03B177EA2C383B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5430;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5430 = { sizeof(HashUtility_tC80280D7F13E8274BB51D195E5E00FC87C5AC83D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5431;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5431 = { sizeof(YooAssets_tD00B5B9911F87CF8AC643076BF1ECB1F10DEBA56), -1, sizeof(YooAssets_tD00B5B9911F87CF8AC643076BF1ECB1F10DEBA56_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5432;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5432 = { sizeof(YooAssetsDriver_tB0C2B38D94EC1806129CAEA70F4385E5D34F81D5), -1, sizeof(YooAssetsDriver_tB0C2B38D94EC1806129CAEA70F4385E5D34F81D5_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5433;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5433 = { sizeof(U3CModuleU3E_tB500DDB8D220627F9BFAF448763790F027856F11), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5434;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5434 = { sizeof(EmbeddedAttribute_t4F9AD0F906A21B3318CFA9466B076A467E16305F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5435;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5435 = { sizeof(IsReadOnlyAttribute_t537BA5EEEDB652AA60A00E4CEA6B73710FBDD806), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5436;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5436 = { sizeof(Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB), sizeof(Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB_marshaled_pinvoke), sizeof(Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5437;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5437 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5438;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5438 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5439;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5439 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5440;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5440 = { sizeof(EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8)+ sizeof(RuntimeObject), sizeof(EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5441;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5441 = { sizeof(WindowFunction_t0067B6F174FD5BEC3E869A38C2319BA8EE85D550), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5442;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5442 = { sizeof(GUI_tA9CDB3D69DB13D51AD83ABDB587EF95947EC2D2A), -1, sizeof(GUI_tA9CDB3D69DB13D51AD83ABDB587EF95947EC2D2A_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5443;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5443 = { sizeof(ParentClipScope_tDAB1300C623213518730D926A970098BECFD9C52)+ sizeof(RuntimeObject), sizeof(ParentClipScope_tDAB1300C623213518730D926A970098BECFD9C52_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5444;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5444 = { sizeof(GUIClip_t6049AB1B245065014011639ADCF204EE3668221B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5445;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5445 = { sizeof(GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2), -1, sizeof(GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5446;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5446 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5447;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5447 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5448;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5448 = { sizeof(GUILayout_tB26F0D6938B9B2AD04633B1DF56A1E52F3E6D177), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5449;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5449 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5450;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5450 = { sizeof(GUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5451;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5451 = { sizeof(LayoutCache_tF844B2FAD6933B78FD5EFEBDE0529BCBAC19BA60), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5452;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5452 = { sizeof(GUILayoutUtility_t48D00CD11CFC1E09E8EC2E51D59E735F5D24836F), -1, sizeof(GUILayoutUtility_t48D00CD11CFC1E09E8EC2E51D59E735F5D24836F_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5453;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5453 = { sizeof(GUISettings_tF2CA7E8B9F62F1FC013BFF053B5FA2709EBA3847), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5454;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5454 = { sizeof(SkinChangedDelegate_tA6D456E853D58AD2EF8A599F543C7E5BA8E94B98), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5455;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5455 = { sizeof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9), -1, sizeof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5456;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5456 = { sizeof(GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5457;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5457 = { sizeof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580), -1, sizeof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5458;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5458 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5459;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5459 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5460;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5460 = { sizeof(GUITargetAttribute_t3F08CE7E00D79CB555B94AA7FA1BCB4B144B922B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5461;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5461 = { sizeof(GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A), -1, sizeof(GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5462;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5462 = { sizeof(ExitGUIException_tFF2EEEBACD9E5684D6112478EEF754B74D154549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5463;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5463 = { sizeof(GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F), -1, sizeof(GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5464;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5464 = { sizeof(GUIWordWrapSizer_t915CE8588C443243630F7737947CA87B42965A36), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5465;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5465 = { sizeof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D), -1, sizeof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5466;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5466 = { sizeof(GUIScrollGroup_t4D7230655A7D01ED9BD95916958E34AF09B21FE5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5467;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5467 = { sizeof(ObjectGUIState_t7BE88DC8B9C7187A77D63BBCBE9DB7B674863C15), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5468;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5468 = { sizeof(ScrollViewState_t004FCCBFB6795BD76582385D6D308D8F9ECF41B6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5469;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5469 = { sizeof(SliderState_t7BBFAEF918BAA1EE6116C3979993E4EC7DC54FC8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5470;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5470 = { sizeof(uint8_t)+ sizeof(RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5471;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5471 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5472;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5472 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5473;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5473 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5474;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5474 = { sizeof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27), -1, sizeof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5475;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5475 = { sizeof(U3CPrivateImplementationDetailsU3E_tCA0A4120E1B13462A402E739CE2DD9CA72BAC713), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5476;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5476 = { sizeof(U3CModuleU3E_tBB65183F1134474D09FF49B95625D25472B9BA8B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5477;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5477 = { sizeof(U3CU3Ec_tD31045D24431D327D10D187D599853D003138D58), -1, sizeof(U3CU3Ec_tD31045D24431D327D10D187D599853D003138D58_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5478;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5478 = { sizeof(CloudFunction_t1007F8C7EAF212A9FD19D0FC2F0C71E348DBD958), -1, sizeof(CloudFunction_t1007F8C7EAF212A9FD19D0FC2F0C71E348DBD958_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5479;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5479 = { sizeof(CloundSavePlayerDaata_t1F9D694D8D690B33A0868EDCE49D4C83F8C77FD8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5480;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5480 = { sizeof(U3CAwakeU3Ed__1_t0A8F4279E9A8075D9D3064405CFEB1DC38A6944A)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5481;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5481 = { sizeof(U3CListAllKeysU3Ed__3_t17C0A4E525733224D96B7ABE2F6986C077429D0B)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5482;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5482 = { sizeof(U3CForceSaveSingleDataU3Ed__4_t8CA291371724E60F1D66E9C1B4A7BA6BF74FB1BB)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5483;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5483 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5484;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5484 = { sizeof(CloudSaveSystem_t419542980442B82C4BB089FE4FFEDD48855C1AA6), -1, sizeof(CloudSaveSystem_t419542980442B82C4BB089FE4FFEDD48855C1AA6_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5485;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5485 = { sizeof(U3CCreateFireEffectU3Ed__12_t5F5E278766D7A4483ADDCAA7C4A864D2F8CF9C0D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5486;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5486 = { sizeof(U3CBehindEffcetU3Ed__13_tD205B745568DF7D32408E8EF906602C79E4378A0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5487;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5487 = { sizeof(FireEffect_t3405E939E4E39C43487E73C9DA6799E6680567B8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5488;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5488 = { sizeof(CookedFoodOpreat_tF677CBA731E07C5C26A4BB47865F87113856CA8C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5489;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5489 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5490;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5490 = { sizeof(U3CSartAddDrinkU3Ed__24_t89B870583E664E392F9E3C8940FDF7DC2C31228D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5491;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5491 = { sizeof(DrinkOpreat_tE3BB22B195D920181FB3F2361BCDC241DEC2FF15), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5492;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5492 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5493;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5493 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5494;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5494 = { sizeof(FoodProperty_tAAA816769EC7DC405C1AABDF794168ED46D9AF4E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5495;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5495 = { sizeof(GreensFood_tB28A2EE0649DB3E268B2B7810E9A8AC2F88E9C58), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5496;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5496 = { sizeof(MakinFoodOpreat_tBF381637FA4B7DB6072DBC27EBE3CE50BCE312EB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5497;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5497 = { sizeof(OtherFood_tF914BF6DC534D3268B2469631B625A84FA77A57D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5498;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5498 = { sizeof(otherFoodTypeTwo_tA921E2CF1C74C4C68005FF1673535CBB46E9BE21), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5499;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5499 = { sizeof(RawFoodOpreat_t5A84416D8152DDDD497DE3321E335734D55BDB6E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5500;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5500 = { sizeof(SteakFood_t68A60966560EBD8C29AAAECE8DB315B51E17015B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5501;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5501 = { sizeof(FoodIDGenerate_tFE01E21DCD4963DB387E6E5EA90C10F120135164), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5502;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5502 = { sizeof(AESUtils_t73B52974E9A888C2EC72EF4E9DD9DB74BB710704), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5503;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5503 = { sizeof(CloundSaveGameData_t1538A5337C49FA37E6F8A02F9F0F1BB2942011A6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5504;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5504 = { sizeof(MainFoodCompnentAssets_t87F293C0A1B943F716C3856FF73BA7D0CD16E97E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5505;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5505 = { sizeof(OtheFoodCompnentAssets_t001F6F26B1B0C787CF334C742B74DE349CDAE2F9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5506;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5506 = { sizeof(OtherFoodItem_tD99CC1A3EC67BA2574653D701C8E00A03CCC667E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5507;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5507 = { sizeof(MainFoodItem_t4298F65045B01EA50DB6B2DE0A9BE141C82C89FB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5508;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5508 = { sizeof(FoodItem_tCA6A5E54C159F99EFA435A20E6779479F92712C2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5509;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5509 = { sizeof(FoodMenuAsset_t4C00E06AE0EF2B9634E1875B98D90E24D0431CD3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5510;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5510 = { sizeof(FoodMenu_tEAFBFBC26FF1204FF859BD39B4906A08FB567169), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5511;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5511 = { sizeof(GameTaskData_t9FC52812F4AA9149D5DEF13A4D84295D9C5084D0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5512;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5512 = { sizeof(FoodMenuConfigur_t69F14619F474736AFCB21E169581D223F1D08F24), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5513;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5513 = { sizeof(GuestConfigur_t6D87BAFAA5DF6A5CA5DB24EF8EAE6ADF069D7217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5514;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5514 = { sizeof(GuestData_tA90EBFB7E86D53F6A8F38985DE74B5F9DC04A473), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5515;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5515 = { sizeof(HeadAssetConfigur_t23598354D1904A8F584FBCAE35A9860E627E301A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5516;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5516 = { sizeof(HeadAssetData_tAB6CEFDAB592FB132D8A492EF5DE6324840FFB0B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5517;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5517 = { sizeof(MainFoodConfigur_tD086692154785F49C555D4B1BAAD460C3DAD7C0E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5518;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5518 = { sizeof(OtherFoodConfigur_tA6EB87E684A6B737D8558213CED42AB67114DEAC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5519;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5519 = { sizeof(RawFoodAssetsConfigur_t83A906F04B0587BF55C52F61E377F48EF92683E3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5520;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5520 = { sizeof(RawFoodData_t080FC4DCA16683EDC4F60AC44FA39210A029BEED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5521;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5521 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5522;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5522 = { sizeof(RestaurantAssetConfiger_tF6D49FD5F5A651D5FC4EBE0BD12CDCDFD08396AC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5523;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5523 = { sizeof(RestaurantData_t7D6C56717DA6F4BB4202FC873005799FA46C9F9F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5524;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5524 = { sizeof(DrinkComponent_t0FF6886CB1698DB8190763330E6271F7C861F849), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5525;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5525 = { sizeof(ShoppItemConfigur_tB9BC6996C5A7913E1EEF4282BB2E238538D9F82C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5526;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5526 = { sizeof(ShoppItemPropData_tF139CF2D87625695C9FADCABCD28CCDACBF369F3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5527;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5527 = { sizeof(ShoppItemGemData_tFCC886B3BBA5CCFC47016F64DAAC64494E643A01), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5528;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5528 = { sizeof(ControllManager_t2642B57FBA4F0FAB7193ED564F8F575C7B993A8B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5529;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5529 = { sizeof(EventParamProperties_t05019706F891EA1BAF0BB69C8B1C245342D6D50F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5530;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5530 = { sizeof(EventDeliverCookedFood_t879D541E341FFFFA2AB6729482C48BE3C7121AE9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5531;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5531 = { sizeof(EventAddOtherFood_t7F43F34FEB093FD2DA41695FEFC1CD8A22C784A4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5532;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5532 = { sizeof(EventAddDrink_t9B8F92DBFD9602EC76DDE5FE3D868A797DB01FFD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5533;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5533 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5534;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5534 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5535;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5535 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5536;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5536 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5537;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5537 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5538;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5538 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5539;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5539 = { sizeof(U3CU3Ec__DisplayClass31_0_t1325CEF32530E3439EB4B791077F546321D876AB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5540;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5540 = { sizeof(U3CGuestConeU3Ed__33_tB16FEEB33B2EFC605767E9465C2A3D4C7397FCF7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5541;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5541 = { sizeof(Guest_t7C87006A91E9A72C586B6082DB0B7192FB76B4F3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5542;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5542 = { sizeof(U3CResetManagerU3Ed__11_tBA77C3A2638110F88B208A2BC73D89EEAA3E2E8F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5543;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5543 = { sizeof(U3CStateGuestComeOneU3Ed__20_t1F08BB7CE53B86ECF7CE47C6777F42A850612D73), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5544;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5544 = { sizeof(GuestManager_t08BB44CBB90850DB991994981F09DF507D6E47E7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5545;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5545 = { sizeof(GuideInfo_tA179C11095A9336906CCD3B1AB6749C242EA4A78), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5546;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5546 = { sizeof(SystemGuideEvent_t94EA5FAE785DE9FBADF02F5688797C234FFBBA09), -1, sizeof(SystemGuideEvent_t94EA5FAE785DE9FBADF02F5688797C234FFBBA09_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5547;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5547 = { sizeof(U3CRestManagerU3Ed__12_t00998E403C1D4F73CBE03A9F47CF790C0BA0A6C6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5548;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5548 = { sizeof(U3CSpawnRestauranYooU3Ed__14_tD41954C93BAEB40872A8B4B06483338C1DDE34E3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5549;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5549 = { sizeof(LevelManager_t8405886BBC5A0ACBB1CC210E25D5DA1C72D16530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5550;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5550 = { sizeof(U3CConfiguerU3Ed__25_tFE845128D9108774B2F57AF8F3FD3D1E86B5FA2E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5551;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5551 = { sizeof(U3CLoadGameDataU3Ed__26_t5E15D3EDB0A2FB2ADCA3A5B2085FD8E7D29B9F77), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5552;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5552 = { sizeof(U3CLoadLevelU3Ed__27_t124598B6DC3C543629AB229B969A6EC331985E44), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5553;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5553 = { sizeof(U3CLoadLlvelGusetU3Ed__28_tB662826A322C81A25CC394EB78B06A2779B13895), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5554;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5554 = { sizeof(U3CLoadFoodMenuU3Ed__29_t6389133771D1E6B97C82374354E0A4478CD59F95), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5555;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5555 = { sizeof(U3CLoadFoodItemU3Ed__30_t3A90454C579729453B97C3C26B9F222E83F69EBE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5556;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5556 = { sizeof(U3CLoadMainFoodItemU3Ed__31_tC45837233C9F61CE52C064BA91DAC05C90A33357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5557;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5557 = { sizeof(U3CLoadOtherFoodItemU3Ed__32_tEB7C145BABEE7292A53E27ED42CD146A3947935B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5558;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5558 = { sizeof(U3CLoadTaskDataU3Ed__33_tCA5898AFBBBB8C01BFDA62918550DDD61EB31D77), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5559;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5559 = { sizeof(LoadConfigursManager_tB9C80FC9048CAE6F0BD82463288DCA90FF0D3895), -1, sizeof(LoadConfigursManager_tB9C80FC9048CAE6F0BD82463288DCA90FF0D3895_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5560;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5560 = { sizeof(MapManager_t05F548D377024861E72CA6B6CC2D15681AA25F53), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5561;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5561 = { sizeof(U3CResetManagerU3Ed__14_tC2768FCFC7A24A95BB1FC52C259EC838B3B91F9E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5562;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5562 = { sizeof(OperationManage_tFF184A4A7A2B8B93259740624B38E08B84CB6FC0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5563;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5563 = { sizeof(PlayerDataParameter_t99940E8382048E611BEA20091478EA46C9E604CA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5564;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5564 = { sizeof(U3CReadU3Ed__9_t4FED0EE05B8FCAB64F752801282B54A6979CEAB3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5565;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5565 = { sizeof(PlayerData_t04178AFFCFAF35DA6472B839034F672FFE5EB64A), -1, sizeof(PlayerData_t04178AFFCFAF35DA6472B839034F672FFE5EB64A_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5566;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5566 = { sizeof(ResourceProfile_tA69CC048C726FE55EC1932C33125A2AD2899E932), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5567;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5567 = { sizeof(U3CConfigurU3Ed__8_t5F945BAA2189F8822DD1E29409A89EC333884F2F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5568;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5568 = { sizeof(U3CLoadConfigursU3Ed__10_t17BD05602176250432B67280FD6455CF7EB07305), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5569;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5569 = { sizeof(SystemDataFactoy_t0F295EDA05C547BBA48910AB76B4238A905BCFDB), -1, sizeof(SystemDataFactoy_t0F295EDA05C547BBA48910AB76B4238A905BCFDB_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5570;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5570 = { sizeof(SystemEventManager_t7316E370F40FC805D47CEFFCBE61D389BB43726D), -1, sizeof(SystemEventManager_t7316E370F40FC805D47CEFFCBE61D389BB43726D_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5571;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5571 = { sizeof(U3CLoadDataU3Ed__24_t6EFA0692B3238C766DC5A6517BD833394891B861), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5572;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5572 = { sizeof(U3CLoadingLevelU3Ed__26_t476BA2DF47915057102D79DD6CB80352000EEF6D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5573;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5573 = { sizeof(U3CResetMangerU3Ed__28_t1FE305717A3F1D2336122D8640D24E3B87FCBF73), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5574;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5574 = { sizeof(U3CSaveCloundDataU3Ed__31_tE45FED7C47BFA6B90878786427B102C1547FEF46)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5575;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5575 = { sizeof(SystemGameManager_tEBED10FE7ED1E6F2223FCAD4CB6FFFD68712293D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5576;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5576 = { sizeof(SystmConfigurationManager_tC1C970C67458E76BA7D6A62A62A7B7DD818C1B89), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5577;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5577 = { sizeof(U3CTargetOpenU3Ed__17_t665AA827813FAA7F050E4C681F457C33196B2399), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5578;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5578 = { sizeof(UiManager_tD893C8D36318160D6F63738C3B33FE3C22D9688F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5579;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5579 = { sizeof(GuestNeedFoodData_t9129C6F34A34F0999113B706D151DDD20C290EDF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5580;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5580 = { sizeof(GuestParentInfo_t89532795C9D4D6B5B3F4BB06EA6F6732125B48F9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5581;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5581 = { sizeof(LevelData_tE6EAE053C277D3E4ED073363F0BF24BDBADFC4D5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5582;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5582 = { sizeof(LevelGuestData_t02E2B626BF5CBDA7A4186527CF21191B18AF1F76), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5583;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5583 = { sizeof(GuestTable_t773854D36ADB091AC6769C229D86C334DCE32D7D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5584;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5584 = { sizeof(NewBehaviourScript_t89363A736D121ACCCE70DC36A66775D2F1D42CF7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5585;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5585 = { sizeof(PlateItem_tF7BA2EA5B617D317A76FED2604C037ECD6399943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5586;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5586 = { sizeof(RestaurantInfo_t5A071066135E99AEC87FFC53FA192968999F8A19), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5587;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5587 = { sizeof(HomeMapUiManager_tED39D923B951D28CA26D7E8BBB2F0251C8A3E9F5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5588;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5588 = { sizeof(U3CLoadConfigurU3Ed__6_tBBA8EA0059B963EF7054B688242391A5E7BC77ED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5589;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5589 = { sizeof(StartHomeManager_tB9F9914830AFB48632C8B356435C243F2865B490), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5590;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5590 = { sizeof(U3CInitDataU3Ed__8_tA803BE6DD6574338ED9428EBFAEE0877E2FFB391), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5591;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5591 = { sizeof(UiMapPlane_t14D8BC95B1C4C0407390DC6A79DD55B3315B61A3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5592;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5592 = { sizeof(U3CSaveCloundDataU3Ed__9_t82051DDCDC50EC3A559ABD9C5B2FBDDD0156DCB6)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5593;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5593 = { sizeof(ComparePlayerDataPlanel_t2A3BBF964761B874A51BB23F98AFD96D7C7D48B6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5594;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5594 = { sizeof(DamondPlane_tBE7F8A5FAAC8C3AFDD5C480FC51C5C229D9DE93B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5595;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5595 = { sizeof(DataPlanel_t63B93F225509A11432BB74BCC752CB1F1846EE66), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5596;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5596 = { sizeof(GameHomePlanel_t8A036D21BEF99C8AE3853851AB94592D7821F08C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5597;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5597 = { sizeof(HeadItem_t312518C0502789BA1EB4486CDB4B1DE9D091BCE7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5598;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5598 = { sizeof(HeadPlanel_t30E55A397C5AC7D53D584544BB98F6BA41F6AC96), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5599;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5599 = { sizeof(HomePlanel_tC54E997184482AEDEE52C73099DB871A611E87A2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5600;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5600 = { sizeof(LevelInfoPlane_tB71B01496603F22E8E44451B5A1EC9ACDABBFAC7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5601;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5601 = { sizeof(LevelSelect_tBC6DA72BFDF1D19B21377EFF8C8BA66197DB356F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5602;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5602 = { sizeof(LoadinPlanle_tF6494D52114639C6DB4E80385577677753F498C0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5603;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5603 = { sizeof(lockPlanel_tA610DCB63E2BDA659C0462950078BC088F970148), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5604;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5604 = { sizeof(U3CCionScoreU3Ed__4_t9A1CB285821A6BBB62B51FB7D304471B81EBD6CB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5605;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5605 = { sizeof(OverPlanel_t3BD8295CC1BDBF33D0900B2F4059249914D6F9A0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5606;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5606 = { sizeof(U3CShowItemPropU3Ed__15_tC74846BAC048DF4CDE72610F91EF6200C2D92B2D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5607;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5607 = { sizeof(U3CShowItemGemU3Ed__17_tCE23953C6F6F7EBD2703E88D404B88BD9FADA89E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5608;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5608 = { sizeof(ShoppPlanel_t07BFA47DF40CE73299500CC5B2D0334F2F1353BF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5609;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5609 = { sizeof(U3CCloseTargetU3Ed__4_t361763AA681089C44C1EE487F32C71C4B88A6104), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5610;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5610 = { sizeof(TargetPlanle_t6A8EBC369B27AE5A05189F09204256FCF600578E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5611;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5611 = { sizeof(TaskItem_tEB8E8BD8BCE9F38E36B33A0A4E863E3A74EF50BB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5612;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5612 = { sizeof(U3CCreaAchieveTaksU3Ed__6_t09C47A859EEA6C654555154BE7B672B4E52180AC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5613;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5613 = { sizeof(TaskPlanel_t885BA63C37C30C820EC675FCCE61B39C24BE96B6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5614;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5614 = { sizeof(UiLoadIngSlider_t46E44ECE6F645B402AD3E8ED6CFA4C54FDA9D3AD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5615;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5615 = { sizeof(UiWoPropAbility_t64D4B3E5A3130C108CC67FE84351386EE0E3AE3F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5616;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5616 = { sizeof(UIText_t28F680F7B1090866318E4896D50C299BD1FE7971), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5617;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5617 = { sizeof(BuildinFileManifest_tC1B882410A653ED35F30219FCA0359884F2935C1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5618;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5618 = { sizeof(StreamingAssetsDefine_tCBF2C51B50E4332F48F0455040BC89B0B32E1108), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5619;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5619 = { sizeof(GameQueryServices_tC8FF0FEB9480CFE5F968BA6B8FF938A4F0ECB842), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5620;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5620 = { sizeof(StreamingAssetsHelper_tEBE3CCE7BF20D9F8E7B22F20F6DCFBB686158685), -1, sizeof(StreamingAssetsHelper_tEBE3CCE7BF20D9F8E7B22F20F6DCFBB686158685_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5621;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5621 = { sizeof(U3CInintCubleU3Ed__6_t4C543808971F450CE5717919F31BADDC73D90E22), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5622;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5622 = { sizeof(YooResoursceManagur_t41268F368AF5E66702DC6C69966EBCF528661E8B), -1, sizeof(YooResoursceManagur_t41268F368AF5E66702DC6C69966EBCF528661E8B_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5623;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5623 = { sizeof(RemoteServices_t1C3C2F5B3D4D6F99D184D692B3EBAB379E94C935), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5624;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5624 = { sizeof(U3CModuleU3E_t625F4EE40CDF76E63929DA3A0864C5AEF068DCF1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5625;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5625 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5626;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5626 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5627;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5627 = { sizeof(JsonData_t3C51D89A19D30A47A74617107D861959322307F1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5628;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5628 = { sizeof(OrderedDictionaryEnumerator_t557577F1E3C3DC7E5A3BFA6B12F2E0F7921D9C18), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5629;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5629 = { sizeof(JsonException_tA6D9726FBCE77AF0E9B652ABE7C01E4F1E55FA76), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5630;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5630 = { sizeof(PropertyMetadata_t39488BB7A97631357ACA1E4409FA30D92DC4564D)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5631;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5631 = { sizeof(ArrayMetadata_tACCCBD1B8815EDDC21759548BA3FD120086DAA7D)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5632;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5632 = { sizeof(ObjectMetadata_t02B03E2889142EBF02504FC09D146A753E9A1661)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5633;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5633 = { sizeof(ExporterFunc_t0E0FD6EE483CD6380339D2D7AC616079B1D5ED8A), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5634;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5634 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5635;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5635 = { sizeof(ImporterFunc_t3D41CCB293D2761DC6862831F8BC45D580EB6AA5), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5636;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5636 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5637;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5637 = { sizeof(WrapperFactory_tB4894CC7D3A41BE3FF6BA079B4490DF5FC5C7BDD), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5638;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5638 = { sizeof(U3CU3Ec_t908A12620392AFC75CA4D67D039F5FD576166F2D), -1, sizeof(U3CU3Ec_t908A12620392AFC75CA4D67D039F5FD576166F2D_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5639;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5639 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5640;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5640 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5641;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5641 = { sizeof(JsonMapper_t1D8418D75E295325E817EEBABCF549B289BDD24A), -1, sizeof(JsonMapper_t1D8418D75E295325E817EEBABCF549B289BDD24A_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5642;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5642 = { sizeof(JsonMockWrapper_t1307E083A859AFEC66C78C143D33726957B3E8DB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5643;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5643 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5644;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5644 = { sizeof(JsonReader_t848C81F0C12BBE7E0135F28350F6635928FAAABD), -1, sizeof(JsonReader_t848C81F0C12BBE7E0135F28350F6635928FAAABD_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5645;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5645 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5646;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5646 = { sizeof(WriterContext_t5E6D8EFF41F41F382870C9F0452344B25B6E666F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5647;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5647 = { sizeof(JsonWriter_t303427DE92107A0D92969763C95C5C20DB2D6F8F), -1, sizeof(JsonWriter_t303427DE92107A0D92969763C95C5C20DB2D6F8F_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5648;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5648 = { sizeof(FsmContext_t039D467FB708DBF74426E9076FDA22760B0D30D1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5649;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5649 = { sizeof(StateHandler_t96934EB5CE1BA6A1DE19972E93F1CA6D31CD3603), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5650;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5650 = { sizeof(Lexer_t8EA7E771DBFBCCC71E85B400AF902FC3F482EBC9), -1, sizeof(Lexer_t8EA7E771DBFBCCC71E85B400AF902FC3F482EBC9_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5651;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5651 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5652;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5652 = { sizeof(U3CU3Ec__DisplayClass2_0_t6F9528147EBA1501BA7DC4A28B00FEDC10940082), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5653;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5653 = { sizeof(U3CU3Ec_t3AAEE6BBEC4C519EEB27C34FC45DFB4EB1584CE2), -1, sizeof(U3CU3Ec_t3AAEE6BBEC4C519EEB27C34FC45DFB4EB1584CE2_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5654;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5654 = { sizeof(UnityTypeBindings_t8B29FCFD0144021BCAE930AB4077026676F186BA), -1, sizeof(UnityTypeBindings_t8B29FCFD0144021BCAE930AB4077026676F186BA_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5655;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5655 = { sizeof(Extensions_tFC09F69054DCDDA5BCB44DE0C1773B9DD74D1C1D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5656;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5656 = { sizeof(JsonIgnore_t3FF65951D3F8D995E69E2E396F84F51967463103), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5657;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5657 = { sizeof(__StaticArrayInitTypeSizeU3D12_t2075943EB4AAA12F6AE68BBCD8EFE67B74570C3B)+ sizeof(RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t2075943EB4AAA12F6AE68BBCD8EFE67B74570C3B), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5658;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5658 = { sizeof(__StaticArrayInitTypeSizeU3D112_t3634DC8000B09BC9B0CCFCCA5B7FF92AE5FB2971)+ sizeof(RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D112_t3634DC8000B09BC9B0CCFCCA5B7FF92AE5FB2971), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5659;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5659 = { sizeof(U3CPrivateImplementationDetailsU3E_t9B0B2A8DD3C1F36C25925A26EB3ED0CB0DF8189E), -1, sizeof(U3CPrivateImplementationDetailsU3E_t9B0B2A8DD3C1F36C25925A26EB3ED0CB0DF8189E_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5660;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5660 = { sizeof(U3CModuleU3E_t853A105E2E1595E463CC860AFEE0FB13A177A12C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5661;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5661 = { sizeof(U3CModuleU3E_t9BF3BFAB7D61D65692779A81BB8D587D68A78414), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5662;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5662 = { sizeof(CheckFrame_tC52E353B564D9F0D14E116565C13913C8BD05EBF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5663;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5663 = { sizeof(HideLoadingPage_tEF54F8AA9B26E57CF291BB457A0ABCBC91BC182D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5664;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5664 = { sizeof(ProfValue_t5AE6537628F2F452404571B15FF3E9EB015608BD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5665;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5665 = { sizeof(WXProfileStatsScript_t45573420CB5800F0D44EF926BAC238BAC9805CC2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5666;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5666 = { sizeof(TouchData_tBFC18681FF77A826E4D5823C44B9F68E1A823A18), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5667;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5667 = { sizeof(U3CU3Ec_tB5CF89FA9196447842235712836756FE02414173), -1, sizeof(U3CU3Ec_tB5CF89FA9196447842235712836756FE02414173_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5668;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5668 = { sizeof(WXTouchInputOverride_t537346190BF9A183BE089711BE35EB7E4ADE5A8D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5669;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5669 = { sizeof(WX_t60FFD778F96FE09EE88AE242EA3EFB9AC865BA05), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5670;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5670 = { sizeof(U3CU3Ec__DisplayClass74_0_tD5EC8BD28644403EAA9A867BF36E91EE2B8D01D9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5671;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5671 = { sizeof(WXBase_t6D33401602504B1D3E788C43C9B9DFFCD0B3DD23), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5672;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5672 = { sizeof(U3CModuleU3E_tF062866229C4952B8051AD32AB6E9D931142CC95), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5673;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5673 = { sizeof(U3CU3Ec__DisplayClass0_0_t59F50543B6FEFF5A3083DDBD0014F90AF0F88A43), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5674;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5674 = { sizeof(U3CU3Ec__DisplayClass1_0_tEAB34591A3D9422D47089AF3756769B92EA68A23), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5675;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5675 = { sizeof(U3CU3Ec__DisplayClass2_0_t983A17B7EA42860742F49575E50045E560CC39B9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5676;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5676 = { sizeof(DOTweenModuleAudio_tCF19013F0C086697BB9D373975D877E26BD2CE09), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5677;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5677 = { sizeof(U3CU3Ec__DisplayClass0_0_t7A8E83B59527B3FEAE3B44C39ED05E6008A297C3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5678;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5678 = { sizeof(U3CU3Ec__DisplayClass1_0_t3A78B607DFF930B7D43BCC15854BA73BFD4287A8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5679;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5679 = { sizeof(U3CU3Ec__DisplayClass2_0_t8C1AE406019D344EE0395C065C3EC1A3154FEB7A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5680;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5680 = { sizeof(U3CU3Ec__DisplayClass3_0_tD6D6E53B37AFAC161D55DEB2BCFA4F353A7932B9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5681;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5681 = { sizeof(U3CU3Ec__DisplayClass4_0_t9E669FCCA49892C3EB54A986B376D01225F5C32F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5682;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5682 = { sizeof(U3CU3Ec__DisplayClass5_0_tCE2250763F6C0A955A4B7F95159E527AECDCCDCC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5683;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5683 = { sizeof(U3CU3Ec__DisplayClass6_0_tF676EF1983CD3B91B739A19ABEEAB2A5B12A30CA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5684;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5684 = { sizeof(U3CU3Ec__DisplayClass7_0_t838318FF7AD89F850DE26411D3961E68C2B8507A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5685;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5685 = { sizeof(U3CU3Ec__DisplayClass8_0_t2C10088D8C53CEFD5E5012FBFC4B65019A1989EF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5686;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5686 = { sizeof(U3CU3Ec__DisplayClass9_0_t249DDBCB23E8308D09D0B03AB7A0B2CCB4F854E3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5687;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5687 = { sizeof(U3CU3Ec__DisplayClass10_0_tB8A336F0E7DC9883C0E326A2DFE75AB01145E4BF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5688;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5688 = { sizeof(DOTweenModulePhysics_t288D427B27A061E96FAD8BB322D3640CA9EC1C14), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5689;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5689 = { sizeof(U3CU3Ec__DisplayClass0_0_tE3BC3F80330AED5085BA4E033B920916AE2EE75C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5690;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5690 = { sizeof(U3CU3Ec__DisplayClass1_0_t1193F41F38E742B3C5DC7DB01E9714978DDCBD01), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5691;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5691 = { sizeof(U3CU3Ec__DisplayClass2_0_t449E13E54A99AFAE4984651C67700DEC993B98ED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5692;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5692 = { sizeof(U3CU3Ec__DisplayClass3_0_t892F716A8A1CDF8AFF37E5CA93703B60ADC4B9F3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5693;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5693 = { sizeof(U3CU3Ec__DisplayClass4_0_t5ED7ACF8AF501EC38AC580536C22E8222A74CD9A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5694;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5694 = { sizeof(U3CU3Ec__DisplayClass5_0_t5952AFF296950978CAEC258447E9176598BA17EB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5695;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5695 = { sizeof(U3CU3Ec__DisplayClass6_0_tF7A18B8A6B9E46A956B38BDD1913A5AC7CCCD0E6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5696;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5696 = { sizeof(U3CU3Ec__DisplayClass7_0_tAD72DF3E41D581C965CFD52335B1FFAA457EAF96), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5697;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5697 = { sizeof(U3CU3Ec__DisplayClass8_0_t9C697E36D5D0AF152BCACC17C1D020869C34C0B8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5698;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5698 = { sizeof(DOTweenModulePhysics2D_t9F08692BD09A9DC4646782951FD80D8A529905F4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5699;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5699 = { sizeof(U3CU3Ec__DisplayClass0_0_tCF08126DC44C5C3B34BACA4E22626734E523AA4E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5700;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5700 = { sizeof(U3CU3Ec__DisplayClass1_0_tDB440B05588765695FD264B656234EFBF64155DA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5701;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5701 = { sizeof(U3CU3Ec__DisplayClass3_0_tC6A3C24ECE56869CB1D6AB3A4FE842EF0C4297E1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5702;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5702 = { sizeof(DOTweenModuleSprite_tF147E9559811CF3DC449BEEBFF0EF43A5DB9BCD7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5703;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5703 = { sizeof(Utils_t3635A7E5B7FAC073A571339320CB18B71E25ABD8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5704;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5704 = { sizeof(U3CU3Ec__DisplayClass0_0_tB18B0BBCEBB20AFFAEF4A5DC6E774BB71965CB5D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5705;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5705 = { sizeof(U3CU3Ec__DisplayClass1_0_tD8156063570C9F9EDAFB40925044950112A26793), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5706;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5706 = { sizeof(U3CU3Ec__DisplayClass2_0_t51A1D3D91AFC969F0DAF516721DDD2B73F01ED0D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5707;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5707 = { sizeof(U3CU3Ec__DisplayClass3_0_t2C69FBF013ADB32163CC4F67A146C846E181C18B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5708;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5708 = { sizeof(U3CU3Ec__DisplayClass4_0_t8053081EDEC54E0DFD42D923A1A91BC995ACC579), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5709;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5709 = { sizeof(U3CU3Ec__DisplayClass5_0_t97A2D1591F4437C9A3355B842244B21EEDFC7D3C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5710;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5710 = { sizeof(U3CU3Ec__DisplayClass7_0_t6825EE9800F145C3DC6520DEBF5CCD211D160E49), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5711;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5711 = { sizeof(U3CU3Ec__DisplayClass8_0_t20FEDD5A66AA07E40BC3977804178A67426F6D3D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5712;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5712 = { sizeof(U3CU3Ec__DisplayClass9_0_tE85C8192601740DA12B6A252D29726E9A7F06E5C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5713;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5713 = { sizeof(U3CU3Ec__DisplayClass10_0_tCC949A20CDD228AC8F8B3A8CD5753ADA411636B3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5714;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5714 = { sizeof(U3CU3Ec__DisplayClass11_0_t65D5FA900D6220B15D4F61001E4C0761C72A6796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5715;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5715 = { sizeof(U3CU3Ec__DisplayClass12_0_tCD751A3A64E63AA8CAAFAF76B7A47FB79314B30B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5716;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5716 = { sizeof(U3CU3Ec__DisplayClass13_0_tA7908A005DAFADE5E96F672B1A0AC21105DC4A84), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5717;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5717 = { sizeof(U3CU3Ec__DisplayClass14_0_t14BD385BD495A01BD6C66D2D92360407933E64E9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5718;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5718 = { sizeof(U3CU3Ec__DisplayClass15_0_tD42752F57E0F92A701F49E8A350FDCE98CD5B0F8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5719;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5719 = { sizeof(U3CU3Ec__DisplayClass16_0_t19F6A4F2A285615B2EFC11553DF0D96149166E7A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5720;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5720 = { sizeof(U3CU3Ec__DisplayClass17_0_t284A2013A9CF39A877F17C22914B8003908BAF2A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5721;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5721 = { sizeof(U3CU3Ec__DisplayClass18_0_t8041ED4DE7248F565E86D82594DDD9BD74602121), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5722;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5722 = { sizeof(U3CU3Ec__DisplayClass19_0_tE0338A49FF6F541CD74258CC0E361949F17279F8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5723;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5723 = { sizeof(U3CU3Ec__DisplayClass20_0_t984E360099B0FDF4B884478FC49D03EC7CC5FC62), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5724;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5724 = { sizeof(U3CU3Ec__DisplayClass21_0_t103F15D006C0D1B8C5D8BF3FB799D36CD246036A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5725;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5725 = { sizeof(U3CU3Ec__DisplayClass22_0_t5C323042506D23A90B58A769162DBBF0BC76B035), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5726;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5726 = { sizeof(U3CU3Ec__DisplayClass23_0_t3DFCA163008250F066061F9E3ED061A2BCFA666F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5727;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5727 = { sizeof(U3CU3Ec__DisplayClass24_0_t04F97A805A71B33C86A4F3A2F05635361548FD88), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5728;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5728 = { sizeof(U3CU3Ec__DisplayClass25_0_t4DA808F4CFC3EA9D61AC01DF1B84F0601D03DABB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5729;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5729 = { sizeof(U3CU3Ec__DisplayClass26_0_t124EF2B4B70F54D478CDCCFB5BCD1975BB607407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5730;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5730 = { sizeof(U3CU3Ec__DisplayClass27_0_t5DF692DEA21E1F075A801B6B88FD7309CD5F86B7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5731;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5731 = { sizeof(U3CU3Ec__DisplayClass28_0_tD0958576900019D44F20769282634622310A6335), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5732;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5732 = { sizeof(U3CU3Ec__DisplayClass29_0_tAF3D1F414662AB44D54A1823AC81F9067EB039C5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5733;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5733 = { sizeof(U3CU3Ec__DisplayClass30_0_tEC2889D7CADE796A4FFB321716EC8FE47DEEBC46), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5734;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5734 = { sizeof(U3CU3Ec__DisplayClass31_0_tA83E5335930C3B73B36584C0C49AEF4D60F994FA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5735;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5735 = { sizeof(U3CU3Ec__DisplayClass32_0_tC0A04FC69DFA3D09F95256B3260E19CDF93CFFAB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5736;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5736 = { sizeof(U3CU3Ec__DisplayClass33_0_t5E307A3D92BFAC7CC6F0D5285761865A1041D7A8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5737;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5737 = { sizeof(U3CU3Ec__DisplayClass34_0_t18D864D495A67B8E3723FF168949A00B63EE2EC5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5738;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5738 = { sizeof(U3CU3Ec__DisplayClass35_0_tB39D8ABA66B038B793C44B42BF240EA543CB3427), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5739;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5739 = { sizeof(U3CU3Ec__DisplayClass36_0_tAE460456A155A97552A76CF67AF3D3BE866F30C8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5740;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5740 = { sizeof(U3CU3Ec__DisplayClass37_0_tEB346CC740290347A0EAF5C4DBE0B8B55EC9B6D0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5741;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5741 = { sizeof(U3CU3Ec__DisplayClass38_0_t267FF01517518B123F4EB11811FAF5BEDB2CB83D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5742;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5742 = { sizeof(U3CU3Ec__DisplayClass39_0_t38D78B606D82F31C19D959AF12B5C4F1D4AF65C2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5743;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5743 = { sizeof(U3CU3Ec__DisplayClass40_0_t0E7A83E8D1C93359BF9EB9E6A9AF3AA18311CD24), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5744;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5744 = { sizeof(U3CU3Ec__DisplayClass41_0_t7022E6DD68301100D1A7392EF5170CF153EDAD93), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5745;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5745 = { sizeof(DOTweenModuleUI_t4988700FDB42C34ACA64236BD53E96D3049D86FE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5746;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5746 = { sizeof(U3CU3Ec__DisplayClass8_0_t841A32590F7E9B5CE337F1E7576F44ACC997C081), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5747;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5747 = { sizeof(U3CU3Ec__DisplayClass9_0_t67C62AE5CAF58BCB265D7E092190857BFBCCEC86), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5748;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5748 = { sizeof(U3CAsyncWaitForCompletionU3Ed__10_tC84049D47EAD23B14384BDEF646D532785ECBF0E)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5749;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5749 = { sizeof(U3CAsyncWaitForRewindU3Ed__11_tC8D7C20224797881A037D09DA8079ECCC3E518FE)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5750;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5750 = { sizeof(U3CAsyncWaitForKillU3Ed__12_t6EA9E2438625431E39D01AB7EEBB4501D6B5E54E)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5751;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5751 = { sizeof(U3CAsyncWaitForElapsedLoopsU3Ed__13_tC7B431C2393096ACD1A6BA0EAFEA84EE62DAF825)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5752;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5752 = { sizeof(U3CAsyncWaitForPositionU3Ed__14_tA6006769EC53BBEBA0665ECA79096B606FDA8A4A)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5753;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5753 = { sizeof(U3CAsyncWaitForStartU3Ed__15_tB4B1CE199FE822B67BCF87301159986D9D50961B)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5754;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5754 = { sizeof(DOTweenModuleUnityVersion_t028EE91988A2A48FC2EEACA97E9BA03468B748F6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5755;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5755 = { sizeof(WaitForCompletion_tC84400E0FA4E28B95AA56DF28973D5FFDA16AFA0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5756;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5756 = { sizeof(WaitForRewind_t2ABB006386A81D361C36B476044786442726743D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5757;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5757 = { sizeof(WaitForKill_t532BDFE32D7C3892E01BF80054F95A9A5C1C24DE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5758;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5758 = { sizeof(WaitForElapsedLoops_t24C0691B408798B4BE5CCC92DC8B4D40430717BC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5759;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5759 = { sizeof(WaitForPosition_t302B4F4C6FC89426E08DDC65543F45785B20B84B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5760;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5760 = { sizeof(WaitForStart_t4448F8E46F59EE599CA8DCEF52FC706221093F30), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5761;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5761 = { sizeof(DOTweenCYInstruction_tCB7C6C1E61447CF998FF1A85EDB5949ED08C303B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5762;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5762 = { sizeof(Physics_t835AE7329FBFA61BA3A2271950FD3F7F6A18770A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5763;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5763 = { sizeof(DOTweenModuleUtils_t5554865584F951A4A4E5DD282E6EBC60F5CEC6E9), -1, sizeof(DOTweenModuleUtils_t5554865584F951A4A4E5DD282E6EBC60F5CEC6E9_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5764;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5764 = { sizeof(U3CModuleU3E_tCFDAF3CE34E8117DEABC58BB3EBDB7B80EA66F5A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5765;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5765 = { sizeof(BaselineFunction_t13AFADEF52F63320B2159C237635948AEB801679), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5766;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5766 = { sizeof(Logger_t092B1218ED93DD47180692D5761559B2054234A0), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5767;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5767 = { sizeof(MeasureFunction_t60EBED1328F5328D4FA7E26335967E59E73B4D09), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5768;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5768 = { sizeof(MeasureOutput_t6C4FCF151309F81DF23561CF3FF1777445FBD84E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5769;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5769 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5770;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5770 = { sizeof(YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345), -1, sizeof(YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5771;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5771 = { sizeof(YogaConstants_tE52AB48288567AEF285EDE0C8884AFD803AD9D3C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5772;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5772 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5773;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5773 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5774;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5774 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5775;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5775 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5776;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5776 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5777;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5777 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5778;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5778 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5779;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5779 = { sizeof(Native_t97ADC11284398663A27E9214C13A84F868A25614), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5780;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5780 = { sizeof(YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5781;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5781 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5782;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5782 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5783;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5783 = { sizeof(YogaSize_tA276812CB1E90E7AA2028A9474EA6EA46B3B38EA)+ sizeof(RuntimeObject), sizeof(YogaSize_tA276812CB1E90E7AA2028A9474EA6EA46B3B38EA), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5784;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5784 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5785;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5785 = { sizeof(YogaValue_t9066126971BFC18D9B4A8AB11435557F19598F8C)+ sizeof(RuntimeObject), sizeof(YogaValue_t9066126971BFC18D9B4A8AB11435557F19598F8C), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5786;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5786 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5787;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5787 = { sizeof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5788;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5788 = { sizeof(TextVertex_tF56662BA585F7DD34D71971F1AA1D2E767946CF3)+ sizeof(RuntimeObject), sizeof(TextVertex_tF56662BA585F7DD34D71971F1AA1D2E767946CF3), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5789;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5789 = { sizeof(TextNative_t463AA48470CE96DB270F55A6F73EF2D90401C00C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5790;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5790 = { sizeof(UIElementsRuntimeUtilityNative_t9DE2C23158D553BB693212D0D8AEAE8594E75938), -1, sizeof(UIElementsRuntimeUtilityNative_t9DE2C23158D553BB693212D0D8AEAE8594E75938_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5791;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5791 = { sizeof(GfxUpdateBufferRange_tC47258BCB472B0727B4FCE21A2A53506644C1A97)+ sizeof(RuntimeObject), sizeof(GfxUpdateBufferRange_tC47258BCB472B0727B4FCE21A2A53506644C1A97), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5792;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5792 = { sizeof(DrawBufferRange_t684F255F5C954760B12F6689F84E78811040C7A4)+ sizeof(RuntimeObject), sizeof(DrawBufferRange_t684F255F5C954760B12F6689F84E78811040C7A4), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5793;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5793 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5794;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5794 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5795;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5795 = { sizeof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD), -1, sizeof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5796;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5796 = { sizeof(U3CModuleU3E_t3B74AF9E7E84B3C57D4687184E31363228069DF2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5797;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5797 = { sizeof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756)+ sizeof(RuntimeObject), sizeof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5798;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5798 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5799;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5799 = { sizeof(GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D)+ sizeof(RuntimeObject), sizeof(GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D), sizeof(GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5800;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5800 = { sizeof(GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A)+ sizeof(RuntimeObject), sizeof(GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5801;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5801 = { sizeof(Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F), sizeof(Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5802;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5802 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5803;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5803 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5804;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5804 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5805;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5805 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5806;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5806 = { sizeof(FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172)+ sizeof(RuntimeObject), sizeof(FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5807;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5807 = { sizeof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A), -1, sizeof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5808;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5808 = { sizeof(FontEngineUtilities_t08D8707F6F929B42407961E303FD339A793E5BBB)+ sizeof(RuntimeObject), sizeof(FontEngineUtilities_t08D8707F6F929B42407961E303FD339A793E5BBB), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5809;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5809 = { sizeof(GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C)+ sizeof(RuntimeObject), sizeof(GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5810;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5810 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5811;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5811 = { sizeof(GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E)+ sizeof(RuntimeObject), sizeof(GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5812;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5812 = { sizeof(GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7)+ sizeof(RuntimeObject), sizeof(GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5813;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5813 = { sizeof(GlyphPairAdjustmentRecord_t6E4295094D349DBF22BC59116FBC8F22EA55420E)+ sizeof(RuntimeObject), sizeof(GlyphPairAdjustmentRecord_t6E4295094D349DBF22BC59116FBC8F22EA55420E), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5814;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5814 = { sizeof(U3CModuleU3E_t42F165DEA2597BD5AB2C914FCF80349ECF878162), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5815;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5815 = { sizeof(WebRequestUtils_t23F1FB533DBFDA3BE5624D901D535B4C6EFAD443), -1, sizeof(WebRequestUtils_t23F1FB533DBFDA3BE5624D901D535B4C6EFAD443_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5816;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5816 = { sizeof(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045), -1, sizeof(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5817;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5817 = { sizeof(WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B), -1, sizeof(WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5818;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5818 = { sizeof(UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5819;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5819 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5820;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5820 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5821;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5821 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5822;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5822 = { sizeof(UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5823;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5823 = { sizeof(CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804), sizeof(CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5824;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5824 = { sizeof(DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB), sizeof(DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5825;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5825 = { sizeof(DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974), sizeof(DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5826;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5826 = { sizeof(DownloadHandlerScript_t42FD7363F738391BB1EA2552FF18F9FA7C0EE38B), sizeof(DownloadHandlerScript_t42FD7363F738391BB1EA2552FF18F9FA7C0EE38B_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5827;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5827 = { sizeof(DownloadHandlerFile_tD1342A7B8173C9ECC7B3BB9E1A7631D7AEFBD902), sizeof(DownloadHandlerFile_tD1342A7B8173C9ECC7B3BB9E1A7631D7AEFBD902_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5828;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5828 = { sizeof(UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6), sizeof(UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5829;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5829 = { sizeof(U3CModuleU3E_t8B36B9B16FF72CF5A0EBA03D2FA162E77C86534C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5830;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5830 = { sizeof(SharedBetweenAnimatorsAttribute_t44FFD5D3B5AEBB394182D66E2198FA398087449C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5831;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5831 = { sizeof(StateMachineBehaviour_t59C5685227B06222F5AF7027E2DA530AB99AFDF7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5832;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5832 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5833;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5833 = { sizeof(AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5834;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5834 = { sizeof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5835;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5835 = { sizeof(AnimationClip_t00BD2F131D308A4AD2C6B0BF66644FC25FECE712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5836;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5836 = { sizeof(AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03)+ sizeof(RuntimeObject), sizeof(AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5837;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5837 = { sizeof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2)+ sizeof(RuntimeObject), sizeof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5838;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5838 = { sizeof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD)+ sizeof(RuntimeObject), sizeof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5839;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5839 = { sizeof(Animator_t8A52E42AE54F76681838FE9E632683EF3952E883), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5840;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5840 = { sizeof(OnOverrideControllerDirtyCallback_tDC67F7661A27502AD804BDE0B696955AFD4A44D5), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5841;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5841 = { sizeof(AnimatorOverrideController_tF78BD58B30BB0D767E7A96F8428EA66F2DFD5493), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5842;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5842 = { sizeof(SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126)+ sizeof(RuntimeObject), sizeof(SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5843;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5843 = { sizeof(HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E)+ sizeof(RuntimeObject), sizeof(HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5844;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5844 = { sizeof(HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8)+ sizeof(RuntimeObject), sizeof(HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5845;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5845 = { sizeof(Motion_tBCD49FBF5608AD21FC03B63C8182FABCEF2707AC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5846;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5846 = { sizeof(RuntimeAnimatorController_t6F7C753402B42EC23C163099CF935C5E0D7A7254), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5847;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5847 = { sizeof(DiscreteEvaluationAttribute_tF23FCB5AB01B394BF5BD84623364A965C90F8BB9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5848;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5848 = { sizeof(NotKeyableAttribute_tDDB6B25B26F649E3CED893EE1E63B6DE66844483), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5849;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5849 = { sizeof(AnimationClipPlayable_t54CEA0DD315B1674C2BD49E681005C4271D73969)+ sizeof(RuntimeObject), sizeof(AnimationClipPlayable_t54CEA0DD315B1674C2BD49E681005C4271D73969), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5850;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5850 = { sizeof(AnimationHumanStream_t31E8EAD3F7C2C29CAE7B4EFB87AA84ECC6DCC6EC)+ sizeof(RuntimeObject), sizeof(AnimationHumanStream_t31E8EAD3F7C2C29CAE7B4EFB87AA84ECC6DCC6EC), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5851;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5851 = { sizeof(AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D)+ sizeof(RuntimeObject), sizeof(AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D), sizeof(AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5852;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5852 = { sizeof(AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0)+ sizeof(RuntimeObject), sizeof(AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0), sizeof(AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5853;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5853 = { sizeof(AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18)+ sizeof(RuntimeObject), sizeof(AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18), sizeof(AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5854;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5854 = { sizeof(AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4)+ sizeof(RuntimeObject), sizeof(AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4), sizeof(AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5855;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5855 = { sizeof(AnimationPlayableOutput_t753AC95DC826789BC537D18449E93114777DDF4E)+ sizeof(RuntimeObject), sizeof(AnimationPlayableOutput_t753AC95DC826789BC537D18449E93114777DDF4E), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5856;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5856 = { sizeof(AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C)+ sizeof(RuntimeObject), sizeof(AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C), sizeof(AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5857;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5857 = { sizeof(AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD)+ sizeof(RuntimeObject), sizeof(AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD), sizeof(AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5858;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5858 = { sizeof(AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127)+ sizeof(RuntimeObject), sizeof(AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127), sizeof(AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5859;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5859 = { sizeof(AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A)+ sizeof(RuntimeObject), sizeof(AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5860;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5860 = { sizeof(AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A)+ sizeof(RuntimeObject), sizeof(AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A), sizeof(AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5861;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5861 = { sizeof(U3CModuleU3E_t88546BFE60862623CAD588FB9164795ED5BF96FF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5862;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5862 = { sizeof(DotfuscatorAttribute_t90A6A71AB7691DF0BFCD2AC365104BF68425B9E7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5863;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5863 = { sizeof(UnityFontABTool_t29C54DB12CF17A7849B7DA9EA1D87475ADA05FFA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5864;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5864 = { sizeof(a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C)+ sizeof(RuntimeObject), sizeof(a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5865;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5865 = { sizeof(a_tC1CB14265A54FD08AC6F722A13BAD61141D148AF)+ sizeof(RuntimeObject), sizeof(a_tC1CB14265A54FD08AC6F722A13BAD61141D148AF), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5866;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5866 = { sizeof(b_tFB66B901E947622DFAF3E315886F1089C51D6893)+ sizeof(RuntimeObject), sizeof(b_tFB66B901E947622DFAF3E315886F1089C51D6893), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5867;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5867 = { sizeof(c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D)+ sizeof(RuntimeObject), sizeof(c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5868;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5868 = { sizeof(d_t2D0D74CB19250389E57D70E733F007725D5AA1F2)+ sizeof(RuntimeObject), sizeof(d_t2D0D74CB19250389E57D70E733F007725D5AA1F2), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5869;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5869 = { sizeof(e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024)+ sizeof(RuntimeObject), sizeof(e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5870;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5870 = { sizeof(b_t972FAFF471676E1BF6780CEF716F7509668D82DA), -1, sizeof(b_t972FAFF471676E1BF6780CEF716F7509668D82DA_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5871;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5871 = { sizeof(a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6)+ sizeof(RuntimeObject), sizeof(a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5872;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5872 = { sizeof(b_t377CCBB7B97AADE524489D5766C28A547BBAB31B)+ sizeof(RuntimeObject), sizeof(b_t377CCBB7B97AADE524489D5766C28A547BBAB31B_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5873;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5873 = { sizeof(c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0)+ sizeof(RuntimeObject), sizeof(c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5874;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5874 = { sizeof(d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2)+ sizeof(RuntimeObject), sizeof(d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5875;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5875 = { sizeof(e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777)+ sizeof(RuntimeObject), sizeof(e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5876;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5876 = { sizeof(c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5877;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5877 = { sizeof(a_tC905F9A47B8F42EC7D8702A0FDD3F296EF74457B)+ sizeof(RuntimeObject), sizeof(a_tC905F9A47B8F42EC7D8702A0FDD3F296EF74457B), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5878;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5878 = { sizeof(b_tF461D840050343BC580EE7A263DE9FE625E47B84)+ sizeof(RuntimeObject), sizeof(b_tF461D840050343BC580EE7A263DE9FE625E47B84), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5879;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5879 = { sizeof(c_t7A9F326BDE1D1BFC4A56A02D80580068ABFC2798)+ sizeof(RuntimeObject), sizeof(c_t7A9F326BDE1D1BFC4A56A02D80580068ABFC2798), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5880;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5880 = { sizeof(d_t7410D4303AF1B382C8A19061C6DA00D2360AB7AC), -1, sizeof(d_t7410D4303AF1B382C8A19061C6DA00D2360AB7AC_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5881;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5881 = { sizeof(U3CModuleU3E_t7A1E3DF1BFD27FA828B031F2A96909F13C3F170B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5882;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5882 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5883;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5883 = { sizeof(CanvasGroup_t048C1461B14628CFAEBE6E7353093ADB04EBC094), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5884;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5884 = { sizeof(CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5885;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5885 = { sizeof(RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244), -1, sizeof(RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5886;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5886 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5887;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5887 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5888;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5888 = { sizeof(WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5889;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5889 = { sizeof(Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26), -1, sizeof(Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5890;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5890 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5891;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5891 = { sizeof(UISystemProfilerApi_t891AC4E16D3C12EAFD2748AE04F7A070F632396A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5892;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5892 = { sizeof(U3CModuleU3E_tCFCF033B61CFCC76C69180CF9A7B07EED67725EA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5893;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5893 = { sizeof(ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5894;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5894 = { sizeof(Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5895;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5895 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5896;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5896 = { sizeof(Physics_t1244C2983AEAFA149425AFFC3DF53BC91C18ED56), -1, sizeof(Physics_t1244C2983AEAFA149425AFFC3DF53BC91C18ED56_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5897;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5897 = { sizeof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960)+ sizeof(RuntimeObject), sizeof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5898;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5898 = { sizeof(RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5)+ sizeof(RuntimeObject), sizeof(RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5899;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5899 = { sizeof(Rigidbody_t268697F5A994213ED97393309870968BC1C7393C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5900;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5900 = { sizeof(Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5901;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5901 = { sizeof(CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5902;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5902 = { sizeof(MeshCollider_tB525E4DDE383252364ED0BDD32CF2B53914EE455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5903;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5903 = { sizeof(CapsuleCollider_t3A1671C74F0836ABEF5D01A7470B5B2BE290A808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5904;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5904 = { sizeof(BoxCollider_tFA5D239388334D6DE0B8FFDAD6825C5B03786E23), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5905;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5905 = { sizeof(SphereCollider_tBA111C542CE97F6873DE742757213D6265C7D275), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5906;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5906 = { sizeof(ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9)+ sizeof(RuntimeObject), sizeof(ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5907;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5907 = { sizeof(PhysicsScene_t55222DD37072E8560EE054A07C0E3FE391D9D9DE)+ sizeof(RuntimeObject), sizeof(PhysicsScene_t55222DD37072E8560EE054A07C0E3FE391D9D9DE), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5908;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5908 = { sizeof(U3CModuleU3E_t806C4A82D63BA5BEE007D75772441609D967BADA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5909;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5909 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5910;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5910 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5911;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5911 = { sizeof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5912;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5912 = { sizeof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5913;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5913 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5914;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5914 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5915;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5915 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5916;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5916 = { sizeof(UICharInfo_t24C2EA0F2F3A938100C271891D9DEB015ABA5FBD)+ sizeof(RuntimeObject), sizeof(UICharInfo_t24C2EA0F2F3A938100C271891D9DEB015ABA5FBD), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5917;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5917 = { sizeof(UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC)+ sizeof(RuntimeObject), sizeof(UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5918;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5918 = { sizeof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207)+ sizeof(RuntimeObject), sizeof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207), sizeof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5919;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5919 = { sizeof(FontTextureRebuildCallback_t76D5E172DF8AA57E67763D453AAC40F0961D09B1), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5920;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5920 = { sizeof(Font_tC95270EA3198038970422D78B74A7F2E218A96B6), -1, sizeof(Font_tC95270EA3198038970422D78B74A7F2E218A96B6_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5921;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5921 = { sizeof(U3CModuleU3E_t941B0EB06FD57B79F043CCA70C8AA4C0B3FB68E7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5922;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5922 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5923;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5923 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5924;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5924 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5925;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5925 = { sizeof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417)+ sizeof(RuntimeObject), sizeof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5926;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5926 = { sizeof(CameraRaycastHelper_tEF8B5EE50B6F5141652EAAF44A77E8B3621FE455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5927;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5927 = { sizeof(Input_t47D83E2A50E6AF7F8A47AA06FBEF9EBE6BBC22BB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5928;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5928 = { sizeof(HitInfo_t34AF939575E1C059D581AB7ED8F039BCFFC70314)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5929;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5929 = { sizeof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39), -1, sizeof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5930;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5930 = { sizeof(U3CModuleU3E_t0643977EA9107777E6F2E30DC5F5326A467F5F6B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5931;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5931 = { sizeof(PhysicsScene2D_t550D023B9E77BE6844564BB4F9FA291EEA10FDC9)+ sizeof(RuntimeObject), sizeof(PhysicsScene2D_t550D023B9E77BE6844564BB4F9FA291EEA10FDC9), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5932;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5932 = { sizeof(Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D), -1, sizeof(Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5933;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5933 = { sizeof(ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14)+ sizeof(RuntimeObject), sizeof(ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5934;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5934 = { sizeof(Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B), sizeof(Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5935;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5935 = { sizeof(ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801)+ sizeof(RuntimeObject), sizeof(ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5936;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5936 = { sizeof(RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA)+ sizeof(RuntimeObject), sizeof(RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5937;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5937 = { sizeof(Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5938;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5938 = { sizeof(Collider2D_t6A17BA7734600EF3F26588E9ED903617D5B8EB52), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5939;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5939 = { sizeof(Joint2D_tFA088656425446CDA98555EC8A0E5FE25945F843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5940;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5940 = { sizeof(U3CModuleU3E_t416C1B54F702B9F0B5C7C848BFDFA85A9E90F443), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5941;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5941 = { sizeof(AssetFileNameExtensionAttribute_tEA86B663DC42BB5C4F9A2A081CD7D28845D9D056), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5942;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5942 = { sizeof(ThreadAndSerializationSafeAttribute_t819C12E8106F42E7493B11DDA93C36F6FB864357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5943;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5943 = { sizeof(WritableAttribute_t7D85DADDFD6751C94E2E9594E562AD281A3B6E7B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5944;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5944 = { sizeof(UnityEngineModuleAssembly_tB6587DA5BA2569921894019758C4D69095012710), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5945;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5945 = { sizeof(NativeClassAttribute_t774C48B9F745C9B0FD2FA82F9B42D4A18E162FA7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5946;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5946 = { sizeof(UnityString_tEB81DAFE75C642A9472D9FEDA7C2EC19A7B672B6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5947;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5947 = { sizeof(VisibleToOtherModulesAttribute_tE7803AC6A0462A18B7EEF17C4A1036DEE993B489), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5948;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5948 = { sizeof(NativeConditionalAttribute_tB722B3ED350E82853F8CEFF672A6CDC4B6B362CA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5949;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5949 = { sizeof(NativeHeaderAttribute_t35DDAA41C31EEE4C94D2586F33D3EB26C0EA6F51), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5950;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5950 = { sizeof(NativeNameAttribute_t222751782B5418807DFE2A88CA0B24CA691B8621), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5951;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5951 = { sizeof(NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5952;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5952 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5953;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5953 = { sizeof(NativePropertyAttribute_tAF7FB03BF7FFE9E8AB0E75B0F842FC0AA22AE607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5954;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5954 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5955;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5955 = { sizeof(NativeAsStructAttribute_t48549F0E2D38CC0251B7BF2780E434EA141DF2D8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5956;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5956 = { sizeof(NativeTypeAttribute_tB60F1675F1F20B6CB1B871FDDD067D672F75B8D1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5957;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5957 = { sizeof(NotNullAttribute_t2E29B7802E8ED55CEA04EC4A6C254C6B60272DF7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5958;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5958 = { sizeof(UnmarshalledAttribute_t3D645C3393EF99EED2893026413D4F5B489CD13B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5959;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5959 = { sizeof(FreeFunctionAttribute_t1200571BEDF64167E58F976FB7374AEA5D9BCBB6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5960;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5960 = { sizeof(ThreadSafeAttribute_t2535A209D57BDA2FF398C4CA766059277FC349FE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5961;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5961 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5962;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5962 = { sizeof(StaticAccessorAttribute_tDE194716AED7A414D473DC570B2E0035A5CE130A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5963;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5963 = { sizeof(NativeThrowsAttribute_t211CE8D047A8D45676C9ED399D5AA3B4A2C3E625), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5964;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5964 = { sizeof(IgnoreAttribute_tAB3F6C4808BA16CD585D60A6353B3E0599DFCE4D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5965;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5965 = { sizeof(PreventReadOnlyInstanceModificationAttribute_t7FBCFCBA855C80F9E87486C8A6B4DDBA47B78415), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5966;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5966 = { sizeof(UsedByNativeCodeAttribute_t3FE9A7CDCC6A3A4122D8BF44F1D0A37BB38894C1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5967;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5967 = { sizeof(RequiredByNativeCodeAttribute_t86B11F2BA12BB463CE3258E64E16B43484014FCA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5968;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5968 = { sizeof(U3CModuleU3E_tAE2FD82E48B7893673000630524705C468010525), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5969;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5969 = { sizeof(AssetBundle_tB38418819A49060CD738CB21541649340F082943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5970;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5970 = { sizeof(AssetBundleCreateRequest_t73B8714B9459A01540E091C3770A408E67188CF6), sizeof(AssetBundleCreateRequest_t73B8714B9459A01540E091C3770A408E67188CF6_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5971;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5971 = { sizeof(AssetBundleRecompressOperation_tFDA1FB5AE1E072FC6CAC1CF0064C13D77F87CDDE), sizeof(AssetBundleRecompressOperation_tFDA1FB5AE1E072FC6CAC1CF0064C13D77F87CDDE_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5972;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5972 = { sizeof(AssetBundleRequest_tED9F5504E75ED1BCFF8DA9B51F5C7356617E6621), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5973;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5973 = { sizeof(U3CModuleU3E_t462BCCFB9B78348533823E0754F65F52A5348F89), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5974;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5974 = { sizeof(AudioConfigurationChangeHandler_tE071B0CBA3B3A77D3E41F5FCB65B4017885B3177), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5975;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5975 = { sizeof(AudioSettings_t66C4BCA1E463B061E2EC9063FB882ACED20D47BD), -1, sizeof(AudioSettings_t66C4BCA1E463B061E2EC9063FB882ACED20D47BD_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5976;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5976 = { sizeof(PCMReaderCallback_t3396D9613664F0AFF65FB91018FD0F901CC16F1E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5977;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5977 = { sizeof(PCMSetPositionCallback_t8D7135A2FB40647CAEC93F5254AD59E18DEB6072), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5978;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5978 = { sizeof(AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5979;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5979 = { sizeof(AudioBehaviour_t2DC0BEF7B020C952F3D2DA5AAAC88501C7EEB941), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5980;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5980 = { sizeof(AudioListener_t1D629CE9BC079C8ECDE8F822616E8A8E319EAE35), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5981;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5981 = { sizeof(AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5982;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5982 = { sizeof(SampleFramesHandler_tFE84FF9BBCEFB880D46227188F375BEF680AAA30), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5983;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5983 = { sizeof(AudioSampleProvider_t602353124A2F6F2AEC38E56C3C21932344F712E2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5984;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5984 = { sizeof(AudioClipPlayable_tD4B758E68CAE03CB0CD31F90C8A3E603B97143A0)+ sizeof(RuntimeObject), sizeof(AudioClipPlayable_tD4B758E68CAE03CB0CD31F90C8A3E603B97143A0), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5985;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5985 = { sizeof(AudioMixer_tE2E8D79241711CDF9AB428C7FB96A35D80E40B04), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5986;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5986 = { sizeof(AudioMixerPlayable_t6AADDF0C53DF1B4C17969EC24B3B4E4975F3A56C)+ sizeof(RuntimeObject), sizeof(AudioMixerPlayable_t6AADDF0C53DF1B4C17969EC24B3B4E4975F3A56C), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5987;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5987 = { sizeof(AudioPlayableOutput_tC3DFF8095F429D90129A367EAB98A24F6D6ADF20)+ sizeof(RuntimeObject), sizeof(AudioPlayableOutput_tC3DFF8095F429D90129A367EAB98A24F6D6ADF20), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5988;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5988 = { sizeof(U3CModuleU3E_tD4D8152B1CC10B76FF3BD3BF122F926B6BF0D3EE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5989;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5989 = { sizeof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D)+ sizeof(RuntimeObject), sizeof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5990;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5990 = { sizeof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0)+ sizeof(RuntimeObject), sizeof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5991;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5991 = { sizeof(ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5992;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5992 = { sizeof(ParticleSystemRenderer_t576C271A363A738A6C576D4C6AEFB3B5B23E46C4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5993;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5993 = { sizeof(U3CModuleU3E_t6B4A7D64487421A1C7A9ACB5578F8A35510E2A0C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5994;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5994 = { sizeof(UpdatedEventHandler_tB0D5A5BA322FE093894992C29DCF51E7E12579C4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5995;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5995 = { sizeof(RemoteSettings_t9DFFC747AB3E7A39DF4527F245B529A407427250), -1, sizeof(RemoteSettings_t9DFFC747AB3E7A39DF4527F245B529A407427250_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5996;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5996 = { sizeof(RemoteConfigSettings_tC979947EE51355162B3241B9F80D95A8FD25FE52), sizeof(RemoteConfigSettings_tC979947EE51355162B3241B9F80D95A8FD25FE52_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5997;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5997 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5998;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5998 = { sizeof(RemoteConfigSettingsHelper_t29B2673892F8181388B45FFEEE354B3773629588), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5999;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5999 = { sizeof(ContinuousEvent_t71122F6F65BF7EA8490EA664A55D5C03790CB6CF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6000;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6000 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6001;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6001 = { sizeof(SessionStateChanged_t1180FB66E702B635CAD9316DC661D931277B2A0C), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6002;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6002 = { sizeof(IdentityTokenChanged_tE8CB0DAB5F6E640A847803F582E6CB6237742395), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6003;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6003 = { sizeof(AnalyticsSessionInfo_tDE8F7A9E13EF9723E2D975F76E916753DA61AD76), -1, sizeof(AnalyticsSessionInfo_tDE8F7A9E13EF9723E2D975F76E916753DA61AD76_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6004;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6004 = { sizeof(U3CModuleU3E_t2F9091E403B25A5364AE8A6B2C249E31D405E3F4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6005;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6005 = { sizeof(JsonUtility_t731013D97E03B7EDAE6186D6D6826A53B85F7197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6006;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6006 = { sizeof(U3CModuleU3E_tA3EED36B70F256012F5BB9BC014BD75FF7BF2C66), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6007;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6007 = { sizeof(DownloadHandlerAssetBundle_tCD9D8BA067912469251677D16DFCADD13CAD510C), sizeof(DownloadHandlerAssetBundle_tCD9D8BA067912469251677D16DFCADD13CAD510C_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6008;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6008 = { sizeof(U3CModuleU3E_t5E8190EE43F4DF5D80E8A6651A0469A8FD445F94), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6009;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6009 = { sizeof(Tilemap_t18C4166D0AC702D5BFC0C411FA73C4B61D9D1751), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6010;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6010 = { sizeof(TilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6011;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6011 = { sizeof(U3CModuleU3E_t2C7BF608494A5C8FB8C8C4D318FB27BCF6CE322A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6012;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6012 = { sizeof(GridLayout_tAD661B1E1E57C16BE21C8C13432EA04FE1F0418B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6013;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6013 = { sizeof(U3CModuleU3E_t4BC86AB59C63F2CC6EB51BE560C15CFCAE821BC7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6014;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6014 = { sizeof(SpriteShapeRenderer_tE998BB73CF661079736CCC23617E597AB230A4AC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6015;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6015 = { sizeof(U3CModuleU3E_t90149EF90407715CC46EB5A9704669888393F1DE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6016;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6016 = { sizeof(ASN1_t33549D58797C9C33AA83F13AD184EAA00C584A6F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6017;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6017 = { sizeof(ASN1Convert_tDA6D2B7710D7868F3D559D5BE7F2C7816BB50AB6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6018;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6018 = { sizeof(BitConverterLE_tB6EF365ED05024FCC12DA3939B10FDEBDB29E1BD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6019;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6019 = { sizeof(CryptoConvert_t676AC22DA6332E9936696ECC97197AB7B1BC7252), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6020;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6020 = { sizeof(U3CModuleU3E_t4791F64F4B6411D4D033A002CAD365D597AA2451), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6021;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6021 = { sizeof(XmlReader_t4C709DEF5F01606ECB60B638F1BD6F6E0A9116FD), -1, sizeof(XmlReader_t4C709DEF5F01606ECB60B638F1BD6F6E0A9116FD_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6022;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6022 = { sizeof(XmlNode_t3180B9B3D5C36CD58F5327D9F13458E3B3F030AF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6023;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6023 = { sizeof(U3CModuleU3E_tDA285F13E9413BF3B79A99D6E310BE9AF3444EEB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6024;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6024 = { sizeof(ConfigurationElement_tAE3EE71C256825472831FFBB7F491275DFAF089E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6025;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6025 = { sizeof(ConfigurationSection_t0BC609F0151B160A4FAB8226679B62AF22539C3E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6026;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6026 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6027;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6027 = { sizeof(ConfigurationPropertyCollection_t1DEB95D3283BB11A46B862E9D13710ED698B6C93), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6028;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6028 = { sizeof(ConfigurationElementCollection_t56E8398661A85A59616301BADF13026FB1492606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6029;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6029 = { sizeof(ConfigurationCollectionAttribute_t1D7DBAAB4908B6B8F26EA1C66106A67BDE949558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6030;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6030 = { sizeof(ConfigurationSectionGroup_tE7948C2D31B193F4BA8828947ED3094B952C7863), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6031;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6031 = { sizeof(IgnoreSection_t43A7C33C0083D18639AA3CC3D75DD93FCF1C5D97), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6032;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6032 = { sizeof(ThrowStub_t9161280E38728A40D9B1A975AEE62E89C379E400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6033;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6033 = { 0, sizeof(Il2CppIActivationFactory*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6034;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6034 = { sizeof(Il2CppComObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6035;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6035 = { sizeof(__Il2CppComDelegate_tD0DD2BBA6AC8F151D32B6DFD02F6BDA339F8DC4D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6036;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6036 = { sizeof(Il2CppFullySharedGenericAny), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6037;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6037 = { sizeof(Il2CppFullySharedGenericStruct)+ sizeof(RuntimeObject), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
