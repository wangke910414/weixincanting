﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>



// System.Action`1<System.String>
struct Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>
struct Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D;
// System.Collections.Generic.Dictionary`2<Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType,Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType>
struct Dictionary_2_t5C32AF17A5801FB3109E5B0E622BA8402A04E08E;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tAE94C8F24AD5B94D4EE85CA9FC59E3409D41CAF7;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Action`1<System.String>>
struct KeyCollection_tDC9905DAB7B3D843994B8AF143B97B5C1D5B6936;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Action`1<System.String>>
struct ValueCollection_t1BFE2D9246539713D76621C5D038C86BA2DA3F76;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Action`1<System.String>>[]
struct EntryU5BU5D_t17B4B52BA0C980E519D97C6C5B12E911F4167F02;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// WeChatWASM.FrameDataOptions
struct FrameDataOptions_t66ED48EBAED3F8B42B6966BA4C7A5F6CD210CBB5;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// WeChatWASM.VideoDecoderStartOption
struct VideoDecoderStartOption_tC19674BC1FCCBCA5BD4CE3C6203D6952424C460C;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// WeChatWASM.WXVideoDecoder
struct WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* JsonMapper_t1D8418D75E295325E817EEBABCF549B289BDD24A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WXSDKManagerHandler_t0D5D9346DBFB9CC39B975C251D35AA6D1682B725_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_mCA46B88F063E9105EDE4ADCA61BD5620782617C7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_mD435FAAC8BD5406C6DBEC96534F6FFF8793EB06E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m1DBE8BD6A92ED74AFCC4FB67F1D075C92A6CC1A2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_mAC12398F029B0CC08037CAC73D4E875F4E9ADD6D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* JsonMapper_ToObject_TisFrameDataOptions_t66ED48EBAED3F8B42B6966BA4C7A5F6CD210CBB5_mCDB54EF60F9623C73A7CD2931BEFC005D0032799_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_Value_m0DF3B826A745419D26A168689AEB4BE6F8698ECA_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>
struct Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t17B4B52BA0C980E519D97C6C5B12E911F4167F02* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_tDC9905DAB7B3D843994B8AF143B97B5C1D5B6936* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_t1BFE2D9246539713D76621C5D038C86BA2DA3F76* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// A.C
struct C_t768917471FC69A04F4DA6940D42848A0AC779B07  : public RuntimeObject
{
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// WeChatWASM.WXVideoDecoder
struct WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5  : public RuntimeObject
{
	// System.String WeChatWASM.WXVideoDecoder::cm
	String_t* ___cm_0;
};

// System.Nullable`1<System.Boolean>
struct Nullable_1_t78F453FADB4A9F50F267A4E349019C34410D1A01 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	bool ___value_1;
};

// System.Nullable`1<System.Double>
struct Nullable_1_t6E154519A812D040E3016229CD7638843A2CC165 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	double ___value_1;
};

// System.Nullable`1<System.Int32>
struct Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	int32_t ___value_1;
};

// System.Nullable`1<Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericStructType>
typedef Il2CppFullySharedGenericStruct Nullable_1_t71C4EA4E848DBD7A4A97704069FB951159A3A339;

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;
};

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17 
{
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;
};

// System.Double
struct Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F 
{
	// System.Double System.Double::m_value
	double ___m_value_0;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

// System.UInt32
struct UInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B 
{
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// WeChatWASM.FrameDataOptions
struct FrameDataOptions_t66ED48EBAED3F8B42B6966BA4C7A5F6CD210CBB5  : public RuntimeObject
{
	// System.Byte[] WeChatWASM.FrameDataOptions::data
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___data_0;
	// System.Nullable`1<System.Int32> WeChatWASM.FrameDataOptions::arrayBufferLength
	Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 ___arrayBufferLength_1;
	// System.Double WeChatWASM.FrameDataOptions::height
	double ___height_2;
	// System.Double WeChatWASM.FrameDataOptions::pkDts
	double ___pkDts_3;
	// System.Double WeChatWASM.FrameDataOptions::pkPts
	double ___pkPts_4;
	// System.Double WeChatWASM.FrameDataOptions::width
	double ___width_5;
};

// WeChatWASM.VideoDecoderStartOption
struct VideoDecoderStartOption_tC19674BC1FCCBCA5BD4CE3C6203D6952424C460C  : public RuntimeObject
{
	// System.String WeChatWASM.VideoDecoderStartOption::source
	String_t* ___source_0;
	// System.Nullable`1<System.Boolean> WeChatWASM.VideoDecoderStartOption::abortAudio
	Nullable_1_t78F453FADB4A9F50F267A4E349019C34410D1A01 ___abortAudio_1;
	// System.Nullable`1<System.Boolean> WeChatWASM.VideoDecoderStartOption::abortVideo
	Nullable_1_t78F453FADB4A9F50F267A4E349019C34410D1A01 ___abortVideo_2;
	// System.Nullable`1<System.Double> WeChatWASM.VideoDecoderStartOption::mode
	Nullable_1_t6E154519A812D040E3016229CD7638843A2CC165 ___mode_3;
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// System.Action`1<System.String>
struct Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A  : public MulticastDelegate_t
{
};

// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>

// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>

// A.C

// A.C

// System.String
struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.String

// WeChatWASM.WXVideoDecoder
struct WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_StaticFields
{
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>> WeChatWASM.WXVideoDecoder::OnActionList
	Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* ___OnActionList_1;
};

// WeChatWASM.WXVideoDecoder

// System.Nullable`1<System.Int32>

// System.Nullable`1<System.Int32>

// System.Nullable`1<Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericStructType>

// System.Nullable`1<Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericStructType>

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Boolean

// System.Byte

// System.Byte

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17_StaticFields
{
	// System.Byte[] System.Char::s_categoryForLatin1
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___s_categoryForLatin1_3;
};

// System.Char

// System.Double

// System.Double

// System.Int32

// System.Int32

// System.UInt32

// System.UInt32

// System.Void

// System.Void

// System.Delegate

// System.Delegate

// WeChatWASM.FrameDataOptions

// WeChatWASM.FrameDataOptions

// WeChatWASM.VideoDecoderStartOption

// WeChatWASM.VideoDecoderStartOption

// System.Action`1<System.String>

// System.Action`1<System.String>
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031  : public RuntimeArray
{
	ALIGN_FIELD (8) uint8_t m_Items[1];

	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// T LitJson.JsonMapper::ToObject<Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JsonMapper_ToObject_TisIl2CppFullySharedGenericAny_m63EC0711A340085DD65C2F6D27635BBE5D13058F_gshared (String_t* ___0_json, Il2CppFullySharedGenericAny* il2cppRetVal, const RuntimeMethod* method) ;
// T System.Nullable`1<Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericStructType>::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Nullable_1_get_Value_mA083C4D9192050DC38513BDD9D364C5C68A3A675_gshared (Nullable_1_t71C4EA4E848DBD7A4A97704069FB951159A3A339* __this, Il2CppFullySharedGenericStruct* il2cppRetVal, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType,Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType>::ContainsKey(TKey)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_mA268E9B914DCE838DD0CD9D879BAAEECD0C677AA_gshared (Dictionary_2_t5C32AF17A5801FB3109E5B0E622BA8402A04E08E* __this, /*Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType*/Il2CppFullySharedGenericAny ___0_key, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType,Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType>::set_Item(TKey,TValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_m4C6841170DD11AED683D2D71919F362A4CFF4A80_gshared (Dictionary_2_t5C32AF17A5801FB3109E5B0E622BA8402A04E08E* __this, /*Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType*/Il2CppFullySharedGenericAny ___0_key, /*Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType*/Il2CppFullySharedGenericAny ___1_value, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType,Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m7745B6ED71E47C95E1BFCE647C4F026A404C668F_gshared (Dictionary_2_t5C32AF17A5801FB3109E5B0E622BA8402A04E08E* __this, const RuntimeMethod* method) ;
// TValue System.Collections.Generic.Dictionary`2<Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType,Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType>::get_Item(TKey)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_get_Item_m2E96908E9716367701CD737FA54C884EB2A8C3EA_gshared (Dictionary_2_t5C32AF17A5801FB3109E5B0E622BA8402A04E08E* __this, /*Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType*/Il2CppFullySharedGenericAny ___0_key, Il2CppFullySharedGenericAny* il2cppRetVal, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType,Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType>::Add(TKey,TValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m54D479280472DEA042DB3933AF547E666B017333_gshared (Dictionary_2_t5C32AF17A5801FB3109E5B0E622BA8402A04E08E* __this, /*Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType*/Il2CppFullySharedGenericAny ___0_key, /*Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType*/Il2CppFullySharedGenericAny ___1_value, const RuntimeMethod* method) ;

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.String WeChatWASM.WXVideoDecoder::A(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* WXVideoDecoder_A_mA9CC35C18D80D968667C233937A041E3C13BBAF5 (String_t* ___0_p, const RuntimeMethod* method) ;
// T LitJson.JsonMapper::ToObject<WeChatWASM.FrameDataOptions>(System.String)
inline FrameDataOptions_t66ED48EBAED3F8B42B6966BA4C7A5F6CD210CBB5* JsonMapper_ToObject_TisFrameDataOptions_t66ED48EBAED3F8B42B6966BA4C7A5F6CD210CBB5_mCDB54EF60F9623C73A7CD2931BEFC005D0032799 (String_t* ___0_json, const RuntimeMethod* method)
{
	FrameDataOptions_t66ED48EBAED3F8B42B6966BA4C7A5F6CD210CBB5* il2cppRetVal;
	((  void (*) (String_t*, Il2CppFullySharedGenericAny*, const RuntimeMethod*))JsonMapper_ToObject_TisIl2CppFullySharedGenericAny_m63EC0711A340085DD65C2F6D27635BBE5D13058F_gshared)(___0_json, (Il2CppFullySharedGenericAny*)&il2cppRetVal, method);
	return il2cppRetVal;
}
// T System.Nullable`1<System.Int32>::get_Value()
inline int32_t Nullable_1_get_Value_m0DF3B826A745419D26A168689AEB4BE6F8698ECA (Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28* __this, const RuntimeMethod* method)
{
	int32_t il2cppRetVal;
	((  void (*) (Nullable_1_t71C4EA4E848DBD7A4A97704069FB951159A3A339*, Il2CppFullySharedGenericStruct*, const RuntimeMethod*))Nullable_1_get_Value_mA083C4D9192050DC38513BDD9D364C5C68A3A675_gshared)((Nullable_1_t71C4EA4E848DBD7A4A97704069FB951159A3A339*)__this, (Il2CppFullySharedGenericStruct*)&il2cppRetVal, method);
	return il2cppRetVal;
}
// System.String WeChatWASM.WXSDKManagerHandler::WXSetArrayBuffer(System.Byte[],System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* WXSDKManagerHandler_WXSetArrayBuffer_mDAEA932726FFD397D3993F0F71E3CAC05F28B533 (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_data, String_t* ___1_callbackId, const RuntimeMethod* method) ;
// System.Void WeChatWASM.WXVideoDecoder::a(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder_a_m95AF245275836B82683E2C7D6D2CD97A71FD2B44 (String_t* ___0_p, const RuntimeMethod* method) ;
// System.Void WeChatWASM.WXVideoDecoder::A(System.String,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder_A_m2DA4107B1994C26A198BD9B38951AC80AAA62808 (String_t* ___0_p, double ___1_p, const RuntimeMethod* method) ;
// System.String LitJson.JsonMapper::ToJson(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* JsonMapper_ToJson_m47E09DCA4D412AB6D98769A2A5544C6D65155811 (RuntimeObject* ___0_obj, const RuntimeMethod* method) ;
// System.Void WeChatWASM.WXVideoDecoder::A(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder_A_mC04899C7AC10F17A0CBEB5F2774A2FC61A66DAC0 (String_t* ___0_p, String_t* ___1_p, const RuntimeMethod* method) ;
// System.Void WeChatWASM.WXVideoDecoder::B(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder_B_mDAF92408E9FB19FDF04D6A6DDCC6BFC005FE2369 (String_t* ___0_p, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m9E3155FB84015C823606188F53B47CB44C444991 (String_t* ___0_str0, String_t* ___1_str1, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>::ContainsKey(TKey)
inline bool Dictionary_2_ContainsKey_mD435FAAC8BD5406C6DBEC96534F6FFF8793EB06E (Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* __this, String_t* ___0_key, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t5C32AF17A5801FB3109E5B0E622BA8402A04E08E*, /*Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType*/Il2CppFullySharedGenericAny, const RuntimeMethod*))Dictionary_2_ContainsKey_mA268E9B914DCE838DD0CD9D879BAAEECD0C677AA_gshared)((Dictionary_2_t5C32AF17A5801FB3109E5B0E622BA8402A04E08E*)__this, (Il2CppFullySharedGenericAny)___0_key, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>::set_Item(TKey,TValue)
inline void Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189 (Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* __this, String_t* ___0_key, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___1_value, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t5C32AF17A5801FB3109E5B0E622BA8402A04E08E*, /*Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType*/Il2CppFullySharedGenericAny, /*Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType*/Il2CppFullySharedGenericAny, const RuntimeMethod*))Dictionary_2_set_Item_m4C6841170DD11AED683D2D71919F362A4CFF4A80_gshared)((Dictionary_2_t5C32AF17A5801FB3109E5B0E622BA8402A04E08E*)__this, (Il2CppFullySharedGenericAny)___0_key, (Il2CppFullySharedGenericAny)___1_value, method);
}
// System.Void WeChatWASM.WXVideoDecoder::a(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder_a_mE20C1E0C243196F9B19E333498D6F41DBB6D6C18 (String_t* ___0_p, String_t* ___1_p, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>::.ctor()
inline void Dictionary_2__ctor_m1DBE8BD6A92ED74AFCC4FB67F1D075C92A6CC1A2 (Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t5C32AF17A5801FB3109E5B0E622BA8402A04E08E*, const RuntimeMethod*))Dictionary_2__ctor_m7745B6ED71E47C95E1BFCE647C4F026A404C668F_gshared)((Dictionary_2_t5C32AF17A5801FB3109E5B0E622BA8402A04E08E*)__this, method);
}
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>::get_Item(TKey)
inline Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* Dictionary_2_get_Item_mAC12398F029B0CC08037CAC73D4E875F4E9ADD6D (Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* __this, String_t* ___0_key, const RuntimeMethod* method)
{
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* il2cppRetVal;
	((  void (*) (Dictionary_2_t5C32AF17A5801FB3109E5B0E622BA8402A04E08E*, /*Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType*/Il2CppFullySharedGenericAny, Il2CppFullySharedGenericAny*, const RuntimeMethod*))Dictionary_2_get_Item_m2E96908E9716367701CD737FA54C884EB2A8C3EA_gshared)((Dictionary_2_t5C32AF17A5801FB3109E5B0E622BA8402A04E08E*)__this, (Il2CppFullySharedGenericAny)___0_key, (Il2CppFullySharedGenericAny*)&il2cppRetVal, method);
	return il2cppRetVal;
}
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t* Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00 (Delegate_t* ___0_a, Delegate_t* ___1_b, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>::Add(TKey,TValue)
inline void Dictionary_2_Add_mCA46B88F063E9105EDE4ADCA61BD5620782617C7 (Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* __this, String_t* ___0_key, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___1_value, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t5C32AF17A5801FB3109E5B0E622BA8402A04E08E*, /*Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType*/Il2CppFullySharedGenericAny, /*Unity.IL2CPP.Metadata.__Il2CppFullySharedGenericType*/Il2CppFullySharedGenericAny, const RuntimeMethod*))Dictionary_2_Add_m54D479280472DEA042DB3933AF547E666B017333_gshared)((Dictionary_2_t5C32AF17A5801FB3109E5B0E622BA8402A04E08E*)__this, (Il2CppFullySharedGenericAny)___0_key, (Il2CppFullySharedGenericAny)___1_value, method);
}
// System.Void WeChatWASM.WXVideoDecoder::B(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder_B_m4DA41D996448E6F70AE79329FBD4665E2DCA6CCE (String_t* ___0_p, String_t* ___1_p, const RuntimeMethod* method) ;
// System.Char System.String::get_Chars(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3 (String_t* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C char* DEFAULT_CALL WX_VideoDecoderGetFrameData(char*);
IL2CPP_EXTERN_C void DEFAULT_CALL WX_VideoDecoderRemove(char*);
IL2CPP_EXTERN_C void DEFAULT_CALL WX_VideoDecoderSeek(char*, double);
IL2CPP_EXTERN_C void DEFAULT_CALL WX_VideoDecoderStart(char*, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL WX_VideoDecoderStop(char*);
IL2CPP_EXTERN_C void DEFAULT_CALL WX_VideoDecoderOff(char*, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL WX_VideoDecoderOn(char*, char*);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WeChatWASM.WXVideoDecoder::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder__ctor_m9B3EA162CB13055C52CC62AD568416D9C05D756B (WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5* __this, String_t* ___0_id, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		String_t* L_0 = ___0_id;
		__this->___cm_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___cm_0), (void*)L_0);
		return;
	}
}
// System.String WeChatWASM.WXVideoDecoder::A(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* WXVideoDecoder_A_mA9CC35C18D80D968667C233937A041E3C13BBAF5 (String_t* ___0_p, const RuntimeMethod* method) 
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___0_p' to native representation
	char* ____0_p_marshaled = NULL;
	____0_p_marshaled = il2cpp_codegen_marshal_string(___0_p);

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(WX_VideoDecoderGetFrameData)(____0_p_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	// Marshaling cleanup of parameter '___0_p' native representation
	il2cpp_codegen_marshal_free(____0_p_marshaled);
	____0_p_marshaled = NULL;

	return _returnValue_unmarshaled;
}
// WeChatWASM.FrameDataOptions WeChatWASM.WXVideoDecoder::GetFrameData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FrameDataOptions_t66ED48EBAED3F8B42B6966BA4C7A5F6CD210CBB5* WXVideoDecoder_GetFrameData_m4D31A329AF7156F772E41D0DBC91FE3C49DB5F66 (WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JsonMapper_ToObject_TisFrameDataOptions_t66ED48EBAED3F8B42B6966BA4C7A5F6CD210CBB5_mCDB54EF60F9623C73A7CD2931BEFC005D0032799_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JsonMapper_t1D8418D75E295325E817EEBABCF549B289BDD24A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1_get_Value_m0DF3B826A745419D26A168689AEB4BE6F8698ECA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WXSDKManagerHandler_t0D5D9346DBFB9CC39B975C251D35AA6D1682B725_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	FrameDataOptions_t66ED48EBAED3F8B42B6966BA4C7A5F6CD210CBB5* V_0 = NULL;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_1 = NULL;
	FrameDataOptions_t66ED48EBAED3F8B42B6966BA4C7A5F6CD210CBB5* V_2 = NULL;
	{
		String_t* L_0 = __this->___cm_0;
		String_t* L_1;
		L_1 = WXVideoDecoder_A_mA9CC35C18D80D968667C233937A041E3C13BBAF5(L_0, NULL);
		il2cpp_codegen_runtime_class_init_inline(JsonMapper_t1D8418D75E295325E817EEBABCF549B289BDD24A_il2cpp_TypeInfo_var);
		FrameDataOptions_t66ED48EBAED3F8B42B6966BA4C7A5F6CD210CBB5* L_2;
		L_2 = JsonMapper_ToObject_TisFrameDataOptions_t66ED48EBAED3F8B42B6966BA4C7A5F6CD210CBB5_mCDB54EF60F9623C73A7CD2931BEFC005D0032799(L_1, JsonMapper_ToObject_TisFrameDataOptions_t66ED48EBAED3F8B42B6966BA4C7A5F6CD210CBB5_mCDB54EF60F9623C73A7CD2931BEFC005D0032799_RuntimeMethod_var);
		V_0 = L_2;
		FrameDataOptions_t66ED48EBAED3F8B42B6966BA4C7A5F6CD210CBB5* L_3 = V_0;
		Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28* L_4 = (&L_3->___arrayBufferLength_1);
		int32_t L_5;
		L_5 = Nullable_1_get_Value_m0DF3B826A745419D26A168689AEB4BE6F8698ECA(L_4, Nullable_1_get_Value_m0DF3B826A745419D26A168689AEB4BE6F8698ECA_RuntimeMethod_var);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_6 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)SZArrayNew(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var, (uint32_t)L_5);
		V_1 = L_6;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_7 = V_1;
		String_t* L_8 = __this->___cm_0;
		il2cpp_codegen_runtime_class_init_inline(WXSDKManagerHandler_t0D5D9346DBFB9CC39B975C251D35AA6D1682B725_il2cpp_TypeInfo_var);
		String_t* L_9;
		L_9 = WXSDKManagerHandler_WXSetArrayBuffer_mDAEA932726FFD397D3993F0F71E3CAC05F28B533(L_7, L_8, NULL);
		FrameDataOptions_t66ED48EBAED3F8B42B6966BA4C7A5F6CD210CBB5* L_10 = V_0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_11 = V_1;
		L_10->___data_0 = L_11;
		Il2CppCodeGenWriteBarrier((void**)(&L_10->___data_0), (void*)L_11);
		FrameDataOptions_t66ED48EBAED3F8B42B6966BA4C7A5F6CD210CBB5* L_12 = V_0;
		V_2 = L_12;
		goto IL_003b;
	}

IL_003b:
	{
		FrameDataOptions_t66ED48EBAED3F8B42B6966BA4C7A5F6CD210CBB5* L_13 = V_2;
		return L_13;
	}
}
// System.Void WeChatWASM.WXVideoDecoder::a(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder_a_m95AF245275836B82683E2C7D6D2CD97A71FD2B44 (String_t* ___0_p, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___0_p' to native representation
	char* ____0_p_marshaled = NULL;
	____0_p_marshaled = il2cpp_codegen_marshal_string(___0_p);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(WX_VideoDecoderRemove)(____0_p_marshaled);

	// Marshaling cleanup of parameter '___0_p' native representation
	il2cpp_codegen_marshal_free(____0_p_marshaled);
	____0_p_marshaled = NULL;

}
// System.Void WeChatWASM.WXVideoDecoder::Remove()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder_Remove_mE27FB0457AB06C1CD733E5E7A29232DE3ECE8EEB (WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___cm_0;
		WXVideoDecoder_a_m95AF245275836B82683E2C7D6D2CD97A71FD2B44(L_0, NULL);
		return;
	}
}
// System.Void WeChatWASM.WXVideoDecoder::A(System.String,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder_A_m2DA4107B1994C26A198BD9B38951AC80AAA62808 (String_t* ___0_p, double ___1_p, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, double);

	// Marshaling of parameter '___0_p' to native representation
	char* ____0_p_marshaled = NULL;
	____0_p_marshaled = il2cpp_codegen_marshal_string(___0_p);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(WX_VideoDecoderSeek)(____0_p_marshaled, ___1_p);

	// Marshaling cleanup of parameter '___0_p' native representation
	il2cpp_codegen_marshal_free(____0_p_marshaled);
	____0_p_marshaled = NULL;

}
// System.Void WeChatWASM.WXVideoDecoder::Seek(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder_Seek_mEF6233784B3A783267630182D11591B645DCBE82 (WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5* __this, double ___0_position, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___cm_0;
		double L_1 = ___0_position;
		WXVideoDecoder_A_m2DA4107B1994C26A198BD9B38951AC80AAA62808(L_0, L_1, NULL);
		return;
	}
}
// System.Void WeChatWASM.WXVideoDecoder::A(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder_A_mC04899C7AC10F17A0CBEB5F2774A2FC61A66DAC0 (String_t* ___0_p, String_t* ___1_p, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___0_p' to native representation
	char* ____0_p_marshaled = NULL;
	____0_p_marshaled = il2cpp_codegen_marshal_string(___0_p);

	// Marshaling of parameter '___1_p' to native representation
	char* ____1_p_marshaled = NULL;
	____1_p_marshaled = il2cpp_codegen_marshal_string(___1_p);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(WX_VideoDecoderStart)(____0_p_marshaled, ____1_p_marshaled);

	// Marshaling cleanup of parameter '___0_p' native representation
	il2cpp_codegen_marshal_free(____0_p_marshaled);
	____0_p_marshaled = NULL;

	// Marshaling cleanup of parameter '___1_p' native representation
	il2cpp_codegen_marshal_free(____1_p_marshaled);
	____1_p_marshaled = NULL;

}
// System.Void WeChatWASM.WXVideoDecoder::Start(WeChatWASM.VideoDecoderStartOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder_Start_m693C475622713400C4A39ADE19CF7B0CDED6BA43 (WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5* __this, VideoDecoderStartOption_tC19674BC1FCCBCA5BD4CE3C6203D6952424C460C* ___0_option, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JsonMapper_t1D8418D75E295325E817EEBABCF549B289BDD24A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->___cm_0;
		VideoDecoderStartOption_tC19674BC1FCCBCA5BD4CE3C6203D6952424C460C* L_1 = ___0_option;
		il2cpp_codegen_runtime_class_init_inline(JsonMapper_t1D8418D75E295325E817EEBABCF549B289BDD24A_il2cpp_TypeInfo_var);
		String_t* L_2;
		L_2 = JsonMapper_ToJson_m47E09DCA4D412AB6D98769A2A5544C6D65155811(L_1, NULL);
		WXVideoDecoder_A_mC04899C7AC10F17A0CBEB5F2774A2FC61A66DAC0(L_0, L_2, NULL);
		return;
	}
}
// System.Void WeChatWASM.WXVideoDecoder::B(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder_B_mDAF92408E9FB19FDF04D6A6DDCC6BFC005FE2369 (String_t* ___0_p, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___0_p' to native representation
	char* ____0_p_marshaled = NULL;
	____0_p_marshaled = il2cpp_codegen_marshal_string(___0_p);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(WX_VideoDecoderStop)(____0_p_marshaled);

	// Marshaling cleanup of parameter '___0_p' native representation
	il2cpp_codegen_marshal_free(____0_p_marshaled);
	____0_p_marshaled = NULL;

}
// System.Void WeChatWASM.WXVideoDecoder::Stop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder_Stop_m941A5CFCD440F160C21750077CAE0E279F08F28B (WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___cm_0;
		WXVideoDecoder_B_mDAF92408E9FB19FDF04D6A6DDCC6BFC005FE2369(L_0, NULL);
		return;
	}
}
// System.Void WeChatWASM.WXVideoDecoder::a(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder_a_mE20C1E0C243196F9B19E333498D6F41DBB6D6C18 (String_t* ___0_p, String_t* ___1_p, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___0_p' to native representation
	char* ____0_p_marshaled = NULL;
	____0_p_marshaled = il2cpp_codegen_marshal_string(___0_p);

	// Marshaling of parameter '___1_p' to native representation
	char* ____1_p_marshaled = NULL;
	____1_p_marshaled = il2cpp_codegen_marshal_string(___1_p);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(WX_VideoDecoderOff)(____0_p_marshaled, ____1_p_marshaled);

	// Marshaling cleanup of parameter '___0_p' native representation
	il2cpp_codegen_marshal_free(____0_p_marshaled);
	____0_p_marshaled = NULL;

	// Marshaling cleanup of parameter '___1_p' native representation
	il2cpp_codegen_marshal_free(____1_p_marshaled);
	____1_p_marshaled = NULL;

}
// System.Void WeChatWASM.WXVideoDecoder::Off(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder_Off_m18D01C3C71270757C56EFF8FC9F60F86F1C7FFC3 (WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5* __this, String_t* ___0_eventName, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_mD435FAAC8BD5406C6DBEC96534F6FFF8793EB06E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_0 = ((WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_StaticFields*)il2cpp_codegen_static_fields_for(WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_il2cpp_TypeInfo_var))->___OnActionList_1;
		String_t* L_1 = __this->___cm_0;
		String_t* L_2 = ___0_eventName;
		String_t* L_3;
		L_3 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(L_1, L_2, NULL);
		bool L_4;
		L_4 = Dictionary_2_ContainsKey_mD435FAAC8BD5406C6DBEC96534F6FFF8793EB06E(L_0, L_3, Dictionary_2_ContainsKey_mD435FAAC8BD5406C6DBEC96534F6FFF8793EB06E_RuntimeMethod_var);
		V_0 = L_4;
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_0042;
		}
	}
	{
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_6 = ((WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_StaticFields*)il2cpp_codegen_static_fields_for(WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_il2cpp_TypeInfo_var))->___OnActionList_1;
		String_t* L_7 = __this->___cm_0;
		String_t* L_8 = ___0_eventName;
		String_t* L_9;
		L_9 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(L_7, L_8, NULL);
		Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189(L_6, L_9, (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)NULL, Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189_RuntimeMethod_var);
		String_t* L_10 = __this->___cm_0;
		String_t* L_11 = ___0_eventName;
		WXVideoDecoder_a_mE20C1E0C243196F9B19E333498D6F41DBB6D6C18(L_10, L_11, NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void WeChatWASM.WXVideoDecoder::B(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder_B_m4DA41D996448E6F70AE79329FBD4665E2DCA6CCE (String_t* ___0_p, String_t* ___1_p, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___0_p' to native representation
	char* ____0_p_marshaled = NULL;
	____0_p_marshaled = il2cpp_codegen_marshal_string(___0_p);

	// Marshaling of parameter '___1_p' to native representation
	char* ____1_p_marshaled = NULL;
	____1_p_marshaled = il2cpp_codegen_marshal_string(___1_p);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(WX_VideoDecoderOn)(____0_p_marshaled, ____1_p_marshaled);

	// Marshaling cleanup of parameter '___0_p' native representation
	il2cpp_codegen_marshal_free(____0_p_marshaled);
	____0_p_marshaled = NULL;

	// Marshaling cleanup of parameter '___1_p' native representation
	il2cpp_codegen_marshal_free(____1_p_marshaled);
	____1_p_marshaled = NULL;

}
// System.Void WeChatWASM.WXVideoDecoder::On(System.String,System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WXVideoDecoder_On_mF04B5B803CAAECE7E5F2DDF0E0E667A6D7678B77 (WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5* __this, String_t* ___0_eventName, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___1_callback, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_mCA46B88F063E9105EDE4ADCA61BD5620782617C7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_mD435FAAC8BD5406C6DBEC96534F6FFF8793EB06E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m1DBE8BD6A92ED74AFCC4FB67F1D075C92A6CC1A2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mAC12398F029B0CC08037CAC73D4E875F4E9ADD6D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* V_2 = NULL;
	String_t* V_3 = NULL;
	{
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_0 = ((WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_StaticFields*)il2cpp_codegen_static_fields_for(WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_il2cpp_TypeInfo_var))->___OnActionList_1;
		V_0 = (bool)((((RuntimeObject*)(Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D*)L_0) == ((RuntimeObject*)(RuntimeObject*)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_2 = (Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D*)il2cpp_codegen_object_new(Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1DBE8BD6A92ED74AFCC4FB67F1D075C92A6CC1A2(L_2, Dictionary_2__ctor_m1DBE8BD6A92ED74AFCC4FB67F1D075C92A6CC1A2_RuntimeMethod_var);
		((WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_StaticFields*)il2cpp_codegen_static_fields_for(WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_il2cpp_TypeInfo_var))->___OnActionList_1 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&((WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_StaticFields*)il2cpp_codegen_static_fields_for(WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_il2cpp_TypeInfo_var))->___OnActionList_1), (void*)L_2);
	}

IL_0019:
	{
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_3 = ((WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_StaticFields*)il2cpp_codegen_static_fields_for(WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_il2cpp_TypeInfo_var))->___OnActionList_1;
		String_t* L_4 = __this->___cm_0;
		String_t* L_5 = ___0_eventName;
		String_t* L_6;
		L_6 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(L_4, L_5, NULL);
		bool L_7;
		L_7 = Dictionary_2_ContainsKey_mD435FAAC8BD5406C6DBEC96534F6FFF8793EB06E(L_3, L_6, Dictionary_2_ContainsKey_mD435FAAC8BD5406C6DBEC96534F6FFF8793EB06E_RuntimeMethod_var);
		V_1 = L_7;
		bool L_8 = V_1;
		if (!L_8)
		{
			goto IL_0064;
		}
	}
	{
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_9 = ((WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_StaticFields*)il2cpp_codegen_static_fields_for(WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_il2cpp_TypeInfo_var))->___OnActionList_1;
		V_2 = L_9;
		String_t* L_10 = __this->___cm_0;
		String_t* L_11 = ___0_eventName;
		String_t* L_12;
		L_12 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(L_10, L_11, NULL);
		V_3 = L_12;
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_13 = V_2;
		String_t* L_14 = V_3;
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_15 = V_2;
		String_t* L_16 = V_3;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_17;
		L_17 = Dictionary_2_get_Item_mAC12398F029B0CC08037CAC73D4E875F4E9ADD6D(L_15, L_16, Dictionary_2_get_Item_mAC12398F029B0CC08037CAC73D4E875F4E9ADD6D_RuntimeMethod_var);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_18 = ___1_callback;
		Delegate_t* L_19;
		L_19 = Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00(L_17, L_18, NULL);
		Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189(L_13, L_14, ((Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)Castclass((RuntimeObject*)L_19, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var)), Dictionary_2_set_Item_m712233828B73716AD094E3EDD334AE3F214A6189_RuntimeMethod_var);
		goto IL_008b;
	}

IL_0064:
	{
		Dictionary_2_t599EFBA58C4F1673138C703D60976BB1FAACE83D* L_20 = ((WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_StaticFields*)il2cpp_codegen_static_fields_for(WXVideoDecoder_t876B0AE77C0649A528A4A9C3C022865E65A512B5_il2cpp_TypeInfo_var))->___OnActionList_1;
		String_t* L_21 = __this->___cm_0;
		String_t* L_22 = ___0_eventName;
		String_t* L_23;
		L_23 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(L_21, L_22, NULL);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_24 = ___1_callback;
		Dictionary_2_Add_mCA46B88F063E9105EDE4ADCA61BD5620782617C7(L_20, L_23, L_24, Dictionary_2_Add_mCA46B88F063E9105EDE4ADCA61BD5620782617C7_RuntimeMethod_var);
		String_t* L_25 = __this->___cm_0;
		String_t* L_26 = ___0_eventName;
		WXVideoDecoder_B_m4DA41D996448E6F70AE79329FBD4665E2DCA6CCE(L_25, L_26, NULL);
	}

IL_008b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.UInt32 A.C::A(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t C_A_m7B3D910143258596FE37FF40986F10BAC7A56BA8 (String_t* ___0_p, const RuntimeMethod* method) 
{
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___0_p;
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		V_0 = ((int32_t)-2128831035);
		V_1 = 0;
		goto IL_0021;
	}

IL_000d:
	{
		String_t* L_1 = ___0_p;
		int32_t L_2 = V_1;
		Il2CppChar L_3;
		L_3 = String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3(L_1, L_2, NULL);
		uint32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_multiply(((int32_t)((int32_t)L_3^(int32_t)L_4)), ((int32_t)16777619)));
		int32_t L_5 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_5, 1));
	}

IL_0021:
	{
		int32_t L_6 = V_1;
		String_t* L_7 = ___0_p;
		int32_t L_8;
		L_8 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_7, NULL);
		if ((((int32_t)L_6) >= ((int32_t)L_8)))
		{
			goto IL_002c;
		}
	}
	{
		goto IL_000d;
	}

IL_002c:
	{
		uint32_t L_9 = V_0;
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____stringLength_4;
		return L_0;
	}
}
