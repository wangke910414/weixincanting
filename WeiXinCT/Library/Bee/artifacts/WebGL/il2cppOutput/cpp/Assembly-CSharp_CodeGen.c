﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CloudFunction::Init()
extern void CloudFunction_Init_mD21C18614CBCBBAA720953C47C4158EAF8A42586 (void);
// 0x00000002 System.Void CloudFunction::CloundSave()
extern void CloudFunction_CloundSave_m40031A6D61020EBD0ED810159CCC1B04410779CB (void);
// 0x00000003 System.Void CloudFunction::CallCloundFunction(System.String)
extern void CloudFunction_CallCloundFunction_mBCAC23D670258D0E63F2A57AECFA750883B3F1C1 (void);
// 0x00000004 System.Void CloudFunction::.ctor()
extern void CloudFunction__ctor_m1B01BDBA24B845059113F1964BCA848DEF104C8E (void);
// 0x00000005 System.Void CloudFunction/<>c::.cctor()
extern void U3CU3Ec__cctor_m9F637F56559CE29C2DB0CCB3557965AE73B77290 (void);
// 0x00000006 System.Void CloudFunction/<>c::.ctor()
extern void U3CU3Ec__ctor_mD2336D1090E2DCDCF60002800F87CDD36F0791BA (void);
// 0x00000007 System.Void CloudFunction/<>c::<CallCloundFunction>b__3_0(WeChatWASM.WXCloudCallFunctionResponse)
extern void U3CU3Ec_U3CCallCloundFunctionU3Eb__3_0_m0D4CD09E26921C34D5E2CB3F4AF54CBA64D8F80A (void);
// 0x00000008 System.Void CloudFunction/<>c::<CallCloundFunction>b__3_1(WeChatWASM.WXCloudCallFunctionResponse)
extern void U3CU3Ec_U3CCallCloundFunctionU3Eb__3_1_m56DC8BE8E81C80062002D03B6B82E23AFABEEB67 (void);
// 0x00000009 System.Void CloudFunction/<>c::<CallCloundFunction>b__3_2(WeChatWASM.WXCloudCallFunctionResponse)
extern void U3CU3Ec_U3CCallCloundFunctionU3Eb__3_2_m2BB384AB7724178CC31A241180D9F4989A90217F (void);
// 0x0000000A System.Void CloundSavePlayerDaata::.ctor()
extern void CloundSavePlayerDaata__ctor_mE560C359DD486E8E5FCDC99AF6951460A74A15F7 (void);
// 0x0000000B System.Void CloudSaveSystem::Awake()
extern void CloudSaveSystem_Awake_m8E28FC73A68ADB384F88679FE8C421DC4D4EC940 (void);
// 0x0000000C System.Void CloudSaveSystem::Start()
extern void CloudSaveSystem_Start_m134AFFF68B01C20893EDE764551B956435B6995C (void);
// 0x0000000D System.Threading.Tasks.Task CloudSaveSystem::ListAllKeys()
extern void CloudSaveSystem_ListAllKeys_m967DDD542D57D1C2B65799DAF1C3F771943322DD (void);
// 0x0000000E System.Threading.Tasks.Task CloudSaveSystem::ForceSaveSingleData(System.String,System.String)
extern void CloudSaveSystem_ForceSaveSingleData_m5FC2776F9485FC6BF1C5FBF1F176363E00A54BF3 (void);
// 0x0000000F System.Threading.Tasks.Task CloudSaveSystem::ForceSaveObjectData(System.String,T)
// 0x00000010 System.Void CloudSaveSystem::.ctor()
extern void CloudSaveSystem__ctor_m3677611609401D74983F5FAC0F6793F2D83E2057 (void);
// 0x00000011 System.Void CloudSaveSystem/<Awake>d__1::MoveNext()
extern void U3CAwakeU3Ed__1_MoveNext_m2073F4CF637E0510E977A773753398EC37A950AB (void);
// 0x00000012 System.Void CloudSaveSystem/<Awake>d__1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAwakeU3Ed__1_SetStateMachine_mEA3B34C59FBB01ACB78902CEFB0AD8C46349BF60 (void);
// 0x00000013 System.Void CloudSaveSystem/<ListAllKeys>d__3::MoveNext()
extern void U3CListAllKeysU3Ed__3_MoveNext_mF06AF0AE486327A68DADF1C848857A23656D0C64 (void);
// 0x00000014 System.Void CloudSaveSystem/<ListAllKeys>d__3::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CListAllKeysU3Ed__3_SetStateMachine_m235973254ED93FF7B9B529DC584277B39342A146 (void);
// 0x00000015 System.Void CloudSaveSystem/<ForceSaveSingleData>d__4::MoveNext()
extern void U3CForceSaveSingleDataU3Ed__4_MoveNext_mA214175827119FE72D7D121464A09F4E6107DBC8 (void);
// 0x00000016 System.Void CloudSaveSystem/<ForceSaveSingleData>d__4::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CForceSaveSingleDataU3Ed__4_SetStateMachine_m9CACD98A7A027DCAD32BD77D301143845CB04033 (void);
// 0x00000017 System.Void CloudSaveSystem/<ForceSaveObjectData>d__5`1::MoveNext()
// 0x00000018 System.Void CloudSaveSystem/<ForceSaveObjectData>d__5`1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x00000019 System.Void FireEffect::Start()
extern void FireEffect_Start_m3967721274CBB6830BFD981B3F15FC4787A3BB6B (void);
// 0x0000001A System.Collections.IEnumerator FireEffect::CreateFireEffect()
extern void FireEffect_CreateFireEffect_m90B44CA491715A06CAC2A6029C3054850BF26539 (void);
// 0x0000001B System.Collections.IEnumerator FireEffect::BehindEffcet()
extern void FireEffect_BehindEffcet_mD4702C1DC033B0E6A39E331C0ABCB999BA7A39EC (void);
// 0x0000001C System.Void FireEffect::ClickContuine()
extern void FireEffect_ClickContuine_mC23FC3BC3EF8287D217E7ED65EAFB4662C00B545 (void);
// 0x0000001D System.Void FireEffect::.ctor()
extern void FireEffect__ctor_m3BADA4F79A8A2E8F1D4DA055E6F6B932C7D80670 (void);
// 0x0000001E System.Void FireEffect::<BehindEffcet>b__13_0()
extern void FireEffect_U3CBehindEffcetU3Eb__13_0_mD99FA4FE93434493F0DFC2D1535A74EC274E510C (void);
// 0x0000001F System.Void FireEffect/<CreateFireEffect>d__12::.ctor(System.Int32)
extern void U3CCreateFireEffectU3Ed__12__ctor_m7A9C136207A996639D2B5006E439740FDD322B8A (void);
// 0x00000020 System.Void FireEffect/<CreateFireEffect>d__12::System.IDisposable.Dispose()
extern void U3CCreateFireEffectU3Ed__12_System_IDisposable_Dispose_mD2C057419285C41C0B1CB4F7726A818713AFC014 (void);
// 0x00000021 System.Boolean FireEffect/<CreateFireEffect>d__12::MoveNext()
extern void U3CCreateFireEffectU3Ed__12_MoveNext_mD4921A758E77F20970DE391581D9E3E22CEB6F12 (void);
// 0x00000022 System.Object FireEffect/<CreateFireEffect>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateFireEffectU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5909438A77C952A795E7429DEEAB85D45C4354D6 (void);
// 0x00000023 System.Void FireEffect/<CreateFireEffect>d__12::System.Collections.IEnumerator.Reset()
extern void U3CCreateFireEffectU3Ed__12_System_Collections_IEnumerator_Reset_m3AFFAB201913F4DBE4887497924BC3EE9628A8C6 (void);
// 0x00000024 System.Object FireEffect/<CreateFireEffect>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CCreateFireEffectU3Ed__12_System_Collections_IEnumerator_get_Current_m50DFE4897336348174F0EC30DB06BA246ADC6350 (void);
// 0x00000025 System.Void FireEffect/<BehindEffcet>d__13::.ctor(System.Int32)
extern void U3CBehindEffcetU3Ed__13__ctor_m28208CE8B37AE3BD3DEA92A9CA7C2550CD1B0A26 (void);
// 0x00000026 System.Void FireEffect/<BehindEffcet>d__13::System.IDisposable.Dispose()
extern void U3CBehindEffcetU3Ed__13_System_IDisposable_Dispose_m5D5ECE97714B45352E7FE0799D7D22D8FBF4FC24 (void);
// 0x00000027 System.Boolean FireEffect/<BehindEffcet>d__13::MoveNext()
extern void U3CBehindEffcetU3Ed__13_MoveNext_mFC472CFD18E9A9E0CF539B7CE2C979D8337ADBF9 (void);
// 0x00000028 System.Object FireEffect/<BehindEffcet>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBehindEffcetU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2415F6F12EDB41117580352211AFD403458630F3 (void);
// 0x00000029 System.Void FireEffect/<BehindEffcet>d__13::System.Collections.IEnumerator.Reset()
extern void U3CBehindEffcetU3Ed__13_System_Collections_IEnumerator_Reset_m4852B9E34F04E6A242FADC7DC6683B8486227923 (void);
// 0x0000002A System.Object FireEffect/<BehindEffcet>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CBehindEffcetU3Ed__13_System_Collections_IEnumerator_get_Current_m8E7ECFAD48F2F16F3430F65D369A450C578B6F50 (void);
// 0x0000002B System.Void CookedFoodOpreat::ResetCookedFood()
extern void CookedFoodOpreat_ResetCookedFood_m7A2A4637889FD40142350B4D94116BADC721BE94 (void);
// 0x0000002C System.Void CookedFoodOpreat::LoadingCookedMinFood(MainFoodCompnentAssets,MainFoodItem)
extern void CookedFoodOpreat_LoadingCookedMinFood_mF19349348882279F244E785E8109559024FCB0A4 (void);
// 0x0000002D System.Void CookedFoodOpreat::AddFoodCooker(FoodProperty)
extern void CookedFoodOpreat_AddFoodCooker_m192DAC18546ADFE94092E5EED3087F1A59035253 (void);
// 0x0000002E System.Void CookedFoodOpreat::SubmitFoodCompelet(System.Int32)
extern void CookedFoodOpreat_SubmitFoodCompelet_m83EF4764CC6D3BA5B43F8C855E99B708C97AA36A (void);
// 0x0000002F System.Void CookedFoodOpreat::.ctor()
extern void CookedFoodOpreat__ctor_mFB4D745758078EB79BD8877BD37BA1A93B937DE1 (void);
// 0x00000030 System.String DrinkOpreat::get_DrinkName()
extern void DrinkOpreat_get_DrinkName_m0E0A0727D20225132FFAD728389D4CA0700CAACA (void);
// 0x00000031 System.Void DrinkOpreat::set_DrinkName(System.String)
extern void DrinkOpreat_set_DrinkName_mB5D06D6E9047959678D718576C2DACFDDDF81D5A (void);
// 0x00000032 System.String DrinkOpreat::get_DrinkID()
extern void DrinkOpreat_get_DrinkID_mD9ED8F928C2D3BAA540CD9292A2BD7B138E63691 (void);
// 0x00000033 System.Void DrinkOpreat::set_DrinkID(System.String)
extern void DrinkOpreat_set_DrinkID_m8AA6F510EA7343922485F10AF162FD9C2F80BCFB (void);
// 0x00000034 System.Int32 DrinkOpreat::get_DrinkPrice()
extern void DrinkOpreat_get_DrinkPrice_m6B42867611F83B868B044BFF997A2E6E594BEB63 (void);
// 0x00000035 System.Void DrinkOpreat::set_DrinkPrice(System.Int32)
extern void DrinkOpreat_set_DrinkPrice_m492D2382FFAF2808318394C357C161591D9E79DA (void);
// 0x00000036 System.Void DrinkOpreat::Start()
extern void DrinkOpreat_Start_mFD2AD5F55BE49144A498F8F95E95EA48535A5EF7 (void);
// 0x00000037 System.Void DrinkOpreat::Configuer(SystemGameManager)
extern void DrinkOpreat_Configuer_m5BA4D423B60F1702541380425CE74FAA643B3060 (void);
// 0x00000038 System.Void DrinkOpreat::Update()
extern void DrinkOpreat_Update_mD929B3C122EACD14BF07A1345BE24D0B7A69976B (void);
// 0x00000039 System.Void DrinkOpreat::OnClick()
extern void DrinkOpreat_OnClick_mD21923C42890E1F8091563FC60DE6F5FFB2DBDFF (void);
// 0x0000003A System.Collections.IEnumerator DrinkOpreat::SartAddDrink()
extern void DrinkOpreat_SartAddDrink_m0CE606B1D4EEA452D7B7FD5E7CB1A7F3E51B5EA7 (void);
// 0x0000003B System.Void DrinkOpreat::SubmitDrink()
extern void DrinkOpreat_SubmitDrink_mD3135D7CA2D3714043784A4F953321186F247D1E (void);
// 0x0000003C System.Void DrinkOpreat::.ctor()
extern void DrinkOpreat__ctor_mC6BE7AFAEC3E0DF028483B2D5C3FC6C25FEFFF78 (void);
// 0x0000003D System.Void DrinkOpreat/<SartAddDrink>d__24::.ctor(System.Int32)
extern void U3CSartAddDrinkU3Ed__24__ctor_mB49863CFEB9407A8AE4B6E16C0133B0598B12A34 (void);
// 0x0000003E System.Void DrinkOpreat/<SartAddDrink>d__24::System.IDisposable.Dispose()
extern void U3CSartAddDrinkU3Ed__24_System_IDisposable_Dispose_m593F1B67BD1FB1502C8588DE468C9F4B71292CA0 (void);
// 0x0000003F System.Boolean DrinkOpreat/<SartAddDrink>d__24::MoveNext()
extern void U3CSartAddDrinkU3Ed__24_MoveNext_m15C8B27026EFD4BFA4C787F81ADB998091AA19AC (void);
// 0x00000040 System.Object DrinkOpreat/<SartAddDrink>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSartAddDrinkU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m23444EF529856D487D201B9669EE056E56029845 (void);
// 0x00000041 System.Void DrinkOpreat/<SartAddDrink>d__24::System.Collections.IEnumerator.Reset()
extern void U3CSartAddDrinkU3Ed__24_System_Collections_IEnumerator_Reset_m1D5FD5E7335F1AF14C1B928E28B7C2991ADE18B4 (void);
// 0x00000042 System.Object DrinkOpreat/<SartAddDrink>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CSartAddDrinkU3Ed__24_System_Collections_IEnumerator_get_Current_mD876C0587ADA58667A54D19932239E91163F1B36 (void);
// 0x00000043 System.String FoodProperty::get_FoodNmae()
extern void FoodProperty_get_FoodNmae_m182BC9812ED2D09F54AB0DC4FEA8CB6AAAE34136 (void);
// 0x00000044 System.Void FoodProperty::set_FoodNmae(System.String)
extern void FoodProperty_set_FoodNmae_m86476A1859800FF4C697FDABA44AC713AFCA7643 (void);
// 0x00000045 System.Void FoodProperty::RestFood()
extern void FoodProperty_RestFood_m6D96FCE46A07EF56A515EF2B4AF2A5EB2B58B018 (void);
// 0x00000046 System.Void FoodProperty::AddOtherFood(System.String)
extern void FoodProperty_AddOtherFood_m900419690EF58C567B884BE51D60ED752E552A6B (void);
// 0x00000047 System.Void FoodProperty::.ctor()
extern void FoodProperty__ctor_m783FCBE3F3295D1ABE6A1E350411E2519557E12F (void);
// 0x00000048 System.Void GreensFood::ClickFood()
extern void GreensFood_ClickFood_mB2343BEA39EBE4173C4964F7D30A70559D69E11F (void);
// 0x00000049 System.Void GreensFood::.ctor()
extern void GreensFood__ctor_m803C9C98FB08E674C37C00634165BB11633E3785 (void);
// 0x0000004A System.Void MakinFoodOpreat::LoadingMakinMinFood(MainFoodCompnentAssets)
extern void MakinFoodOpreat_LoadingMakinMinFood_m011C9148FF9D02379C8CC651CA8FB73725932F45 (void);
// 0x0000004B System.Void MakinFoodOpreat::ResetMakinFood()
extern void MakinFoodOpreat_ResetMakinFood_m91B16C938DA909495660E819B19AD01373A5C306 (void);
// 0x0000004C System.Void MakinFoodOpreat::AddFoodMaking()
extern void MakinFoodOpreat_AddFoodMaking_m9AE6FC571022AD0BE5063B75036889DA90862BAE (void);
// 0x0000004D System.Void MakinFoodOpreat::AddOtherFood(System.String,System.String,System.Int32,System.Single)
extern void MakinFoodOpreat_AddOtherFood_m9A951432C5B1A5206E01B9BF0F3744040E9441F1 (void);
// 0x0000004E System.Void MakinFoodOpreat::.ctor()
extern void MakinFoodOpreat__ctor_m58F9EA11BF9794D748D6EA7FE2C9236411FC31DE (void);
// 0x0000004F System.Void OtherFood::.ctor()
extern void OtherFood__ctor_mD3C47369BED186C27FB2EC9E287C6864656DE86C (void);
// 0x00000050 System.Void otherFoodTypeTwo::ClickFood()
extern void otherFoodTypeTwo_ClickFood_mD53CA9599B6535ADBCD666CCB726422D2005F42C (void);
// 0x00000051 System.Void otherFoodTypeTwo::.ctor()
extern void otherFoodTypeTwo__ctor_m2BE978CB8A4F6C244241E71D4F33E1212A9098DA (void);
// 0x00000052 System.String RawFoodOpreat::get_FoodName()
extern void RawFoodOpreat_get_FoodName_m3B67A96DCFD5C95F5BAC785655ECFE5D192AA7BD (void);
// 0x00000053 System.Void RawFoodOpreat::set_FoodName(System.String)
extern void RawFoodOpreat_set_FoodName_mCE22154F334E2CC875CCBD0403BDC56043FDCA53 (void);
// 0x00000054 System.Void RawFoodOpreat::Start()
extern void RawFoodOpreat_Start_m38E3A35979EEEAA1506AA3E392E1C8F67AA03865 (void);
// 0x00000055 System.Void RawFoodOpreat::EvetClick()
extern void RawFoodOpreat_EvetClick_mB033958C51CB3854F9CAE8D54C5D9E2D51228E37 (void);
// 0x00000056 System.Void RawFoodOpreat::.ctor()
extern void RawFoodOpreat__ctor_m91CD052D141D58FC616811708AD3B17337C4B7C1 (void);
// 0x00000057 System.Void SteakFood::ClilkFood()
extern void SteakFood_ClilkFood_mFEE6A1709EE2F82B2DE5D8470284D271DD0980C7 (void);
// 0x00000058 System.Void SteakFood::OnEnable()
extern void SteakFood_OnEnable_m4F7CDBB71668757D93F89DA1CA00ADF9680C3994 (void);
// 0x00000059 System.Void SteakFood::OnDisable()
extern void SteakFood_OnDisable_mD7A0CC010CF11A5A28EFDA8DD61216094F27AC31 (void);
// 0x0000005A System.Void SteakFood::RestFood()
extern void SteakFood_RestFood_mAE75259F10B2F7BAAEA0217D912A159F11830381 (void);
// 0x0000005B System.Void SteakFood::Update()
extern void SteakFood_Update_m6835B2B7163F53C5F3B2369442E1E3998F941E50 (void);
// 0x0000005C System.Void SteakFood::AddOtherFood(System.String)
extern void SteakFood_AddOtherFood_mA547CC6E19152557F8134570521A4ACA88801A0F (void);
// 0x0000005D System.Void SteakFood::.ctor()
extern void SteakFood__ctor_mB4D756CE8FC57921E01E07F49EB6869415A274B5 (void);
// 0x0000005E System.Void FoodIDGenerate::.ctor()
extern void FoodIDGenerate__ctor_mC6F4E4E3000529C8A89697E8C21A7A56BA5DA991 (void);
// 0x0000005F System.String AESUtils::Encrypt(System.String)
extern void AESUtils_Encrypt_mABBA2B4C72B4B346A3F83E76B1CEF80A77A87ED9 (void);
// 0x00000060 System.String AESUtils::Decrypt(System.String)
extern void AESUtils_Decrypt_mC6739A384E4EEC38B514F9B564C91576B79731B8 (void);
// 0x00000061 System.Void AESUtils::.ctor()
extern void AESUtils__ctor_m5A1E5E0CCEB4DF7800C7B50D610BA078487CDF6E (void);
// 0x00000062 System.Void CloundSaveGameData::.ctor()
extern void CloundSaveGameData__ctor_m0D3E18E0F16D1E6EC8FC955F1A7795CA921D23B0 (void);
// 0x00000063 System.Void MainFoodCompnentAssets::.ctor()
extern void MainFoodCompnentAssets__ctor_mBBD5FB2F1D59D46C72214E89FEA52981595D646E (void);
// 0x00000064 System.Void OtheFoodCompnentAssets::.ctor()
extern void OtheFoodCompnentAssets__ctor_m7AE04539E4CA3CB4AC3CB731410C62BFCE95A388 (void);
// 0x00000065 System.Void OtherFoodItem::.ctor()
extern void OtherFoodItem__ctor_mAC4B24BAED3F66015D6B8B93E51F3A60C0C0C642 (void);
// 0x00000066 System.Void MainFoodItem::.ctor()
extern void MainFoodItem__ctor_m559FCDA441B25F96ECFC09259677FB0099B6E9AC (void);
// 0x00000067 System.Void FoodItem::.ctor()
extern void FoodItem__ctor_m68AC1D167A2EDB705DF322708CE06C6C6B3EA3CB (void);
// 0x00000068 System.Void FoodMenuAsset::.ctor()
extern void FoodMenuAsset__ctor_mB3F23F6A3BE2E018E39FF69C0BE2214427F94BBF (void);
// 0x00000069 System.Void FoodMenu::.ctor()
extern void FoodMenu__ctor_mB7636CEF989E5F91B52C380D84AB2848325BA965 (void);
// 0x0000006A System.Void GameTaskData::.ctor()
extern void GameTaskData__ctor_m48DC71356C1DB8FE33F2D80CE36980CBAF445128 (void);
// 0x0000006B System.Void FoodMenuConfigur::.ctor()
extern void FoodMenuConfigur__ctor_mB7AE8A536E8FBFE885F1AF13CC6FFC333AC5F4B2 (void);
// 0x0000006C System.Void GuestConfigur::.ctor()
extern void GuestConfigur__ctor_m0B7DEAAC5B02E946FA120D86BF17BC6B89FC670F (void);
// 0x0000006D UnityEngine.GameObject GuestData::get_GuestPrefab()
extern void GuestData_get_GuestPrefab_mCA685EEF6F411E9ECC6C7D7A1C8A6D4CE8087E54 (void);
// 0x0000006E System.Void GuestData::.ctor()
extern void GuestData__ctor_mF8BA2E8B9C409B7AFABD6249BB565A8C93408FD7 (void);
// 0x0000006F System.Void HeadAssetConfigur::.ctor()
extern void HeadAssetConfigur__ctor_m1C93507C8F780973B13A052B92C6373DF0C5BA67 (void);
// 0x00000070 System.Void HeadAssetData::.ctor()
extern void HeadAssetData__ctor_mC014C6F69E196C8DB977007F194A85BFCABFDE8C (void);
// 0x00000071 System.Void MainFoodConfigur::.ctor()
extern void MainFoodConfigur__ctor_mA49777F4E5C2654AABAF6B55E6CAF740006A0134 (void);
// 0x00000072 System.Void OtherFoodConfigur::.ctor()
extern void OtherFoodConfigur__ctor_m47B77365B68DD47C1A9978629512BC62B3AE2A42 (void);
// 0x00000073 System.Void RawFoodAssetsConfigur::.ctor()
extern void RawFoodAssetsConfigur__ctor_m1E58C03DEB02A606C55EC84B7A9637D4159CC174 (void);
// 0x00000074 System.Void RawFoodData::.ctor()
extern void RawFoodData__ctor_m4F23C97A3A516149EE5B65451B31F5344D1E2EAF (void);
// 0x00000075 System.Void RestaurantAssetConfiger::.ctor()
extern void RestaurantAssetConfiger__ctor_m5175DB4789A6303B8F415A5C975E49E973792293 (void);
// 0x00000076 System.Void RestaurantData::.ctor()
extern void RestaurantData__ctor_mED325B149A80736088D56CC1590B0FA655CCA658 (void);
// 0x00000077 System.Void DrinkComponent::.ctor()
extern void DrinkComponent__ctor_m2A9532E35E4BAFA83EE206D5C59D4E598A380160 (void);
// 0x00000078 System.Void ShoppItemConfigur::.ctor()
extern void ShoppItemConfigur__ctor_m738999308310E63FFBF83E1FF1B41941CB615A08 (void);
// 0x00000079 System.Void ShoppItemPropData::.ctor()
extern void ShoppItemPropData__ctor_m0EBDA70EA01D09EDE157E88576FEBFBC99BF6F3A (void);
// 0x0000007A System.Void ShoppItemGemData::.ctor()
extern void ShoppItemGemData__ctor_m7A5126FEB8F5372E18CE6670F95EC742F20E554F (void);
// 0x0000007B System.Void ControllManager::Configuer(SystemGameManager)
extern void ControllManager_Configuer_m11135D88D5491A63C4F48EE1E6DE7ED8C5994927 (void);
// 0x0000007C System.Void ControllManager::Init()
extern void ControllManager_Init_m039C52A8BE0333E83889DAC285443193D9180C89 (void);
// 0x0000007D System.Void ControllManager::Update()
extern void ControllManager_Update_mCD36E094028BACB97D11A58719771321726B1502 (void);
// 0x0000007E System.Void ControllManager::.ctor()
extern void ControllManager__ctor_m08C4F2F5B06F57F7D02333F00C0A6DA8F08B131C (void);
// 0x0000007F UnityEngine.GameObject EventParamProperties::get_DataObject()
extern void EventParamProperties_get_DataObject_m46996693C9EE878EBF72DD8A145BFC6DF24B01D9 (void);
// 0x00000080 System.Void EventParamProperties::set_DataObject(UnityEngine.GameObject)
extern void EventParamProperties_set_DataObject_m0F3D20933C311955E267969DAE080D1262AC832A (void);
// 0x00000081 System.Void EventParamProperties::.ctor()
extern void EventParamProperties__ctor_m9ED223FB1D814D584E1A2D4700F3C22EBD09399C (void);
// 0x00000082 System.Void EventDeliverCookedFood::.ctor()
extern void EventDeliverCookedFood__ctor_m75E0739E1C82EE33A3622EB4B698982AE9DE287F (void);
// 0x00000083 System.Void EventAddOtherFood::.ctor()
extern void EventAddOtherFood__ctor_mF817ADE9A4A81CF000E6BE61B554DCC7708ADE70 (void);
// 0x00000084 System.Void EventAddDrink::.ctor()
extern void EventAddDrink__ctor_mD8B651EF87E332CC8BD6BD0147002423B976F911 (void);
// 0x00000085 System.Int32 Guest::get_NeedFoddCount()
extern void Guest_get_NeedFoddCount_m84B0E0252F57B1A3379F1805105D9B66DA5228BA (void);
// 0x00000086 System.Void Guest::set_NeedFoddCount(System.Int32)
extern void Guest_set_NeedFoddCount_m6233BB0D398FD092192E828FD619E5435F55E0E5 (void);
// 0x00000087 System.Void Guest::Start()
extern void Guest_Start_m8C27C4E65A1BE5DD2F897E9C3D7858A846E4B9EA (void);
// 0x00000088 System.Void Guest::GuestRest()
extern void Guest_GuestRest_mB85644BDE0DF93620C9CD20C6F24427524E01FAF (void);
// 0x00000089 System.Void Guest::SetBalckTrager(UnityEngine.Transform)
extern void Guest_SetBalckTrager_m7C5BA9FF7A2D8EB492BB6D6BAFCEBF60A0D60FBE (void);
// 0x0000008A System.Void Guest::Configur(GuestManager)
extern void Guest_Configur_m713029DFF58A2F5DE257568BAEED1563952E3850 (void);
// 0x0000008B System.Void Guest::GuestInfo(LevelGuestData)
extern void Guest_GuestInfo_mAB0E5D9B7DEC660BF9F5519BEDEFE52DB5F0112B (void);
// 0x0000008C System.Void Guest::CreateNeedList(FoodMenuAsset)
extern void Guest_CreateNeedList_m3EF4068CD1B282CC711B6B5DF6067FA708BAF048 (void);
// 0x0000008D System.Void Guest::SetFoodMenu(FoodMenu)
extern void Guest_SetFoodMenu_mC1CD07D417A2AF670576D379898755B5B96650EC (void);
// 0x0000008E System.Boolean Guest::ComparisonNeedFoodNum(System.Single)
extern void Guest_ComparisonNeedFoodNum_m5F0D558C3E72B797F37BE903970BA165F2F2E4B4 (void);
// 0x0000008F System.Void Guest::StartGuest(UnityEngine.Transform,System.Int32)
extern void Guest_StartGuest_m85EACBC5A8914EA1CD45BF66A480B2D8E250419E (void);
// 0x00000090 System.Collections.IEnumerator Guest::GuestCone(UnityEngine.Transform)
extern void Guest_GuestCone_m2F1E48BABBF77E3DC8437D59F1CCF5592DFD3EF4 (void);
// 0x00000091 System.Void Guest::GuestWaitStart()
extern void Guest_GuestWaitStart_m3C4AB816BCA8DCD5CDC868C6DA811F3E849EBA8B (void);
// 0x00000092 System.Void Guest::Update()
extern void Guest_Update_m8E5F04FBBD9104522EFE5B94054E54BE79760C34 (void);
// 0x00000093 System.Void Guest::CompeletNeedFood()
extern void Guest_CompeletNeedFood_m68448E83859DF9C3090C5D431CAD2A49AC96F9BC (void);
// 0x00000094 System.Void Guest::GuestWalk()
extern void Guest_GuestWalk_mEA5D82EBDE12A263FDFB81E2809C5AB2DFDBCE78 (void);
// 0x00000095 System.Void Guest::.ctor()
extern void Guest__ctor_m5B8E0311ED23C7CDC58A261772C23E718F862CD0 (void);
// 0x00000096 System.Void Guest::<GuestWaitStart>b__34_0()
extern void Guest_U3CGuestWaitStartU3Eb__34_0_mFA79CA8683A6BF9372FEA6984449F56F9CDC5697 (void);
// 0x00000097 System.Void Guest::<GuestWalk>b__37_0()
extern void Guest_U3CGuestWalkU3Eb__37_0_m5E7FD21B493760AFA1613A3FBB33CEF332C3B304 (void);
// 0x00000098 System.Void Guest/<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_mAAB140FDFF7F5613D3B095C5F7C0BED8686BBA26 (void);
// 0x00000099 System.Void Guest/<>c__DisplayClass31_0::<ComparisonNeedFoodNum>b__0()
extern void U3CU3Ec__DisplayClass31_0_U3CComparisonNeedFoodNumU3Eb__0_mCE42ACA4BBB03154B45255C4AB6635D285EEABD3 (void);
// 0x0000009A System.Void Guest/<GuestCone>d__33::.ctor(System.Int32)
extern void U3CGuestConeU3Ed__33__ctor_m22091E15388CBFD4276608933F24B4CECF28333C (void);
// 0x0000009B System.Void Guest/<GuestCone>d__33::System.IDisposable.Dispose()
extern void U3CGuestConeU3Ed__33_System_IDisposable_Dispose_m504191E7270F5B7BB011E7B1FE3349028E59F9CE (void);
// 0x0000009C System.Boolean Guest/<GuestCone>d__33::MoveNext()
extern void U3CGuestConeU3Ed__33_MoveNext_m573C14231875314856FEE2C2B6C4C34033CB7F23 (void);
// 0x0000009D System.Object Guest/<GuestCone>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGuestConeU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m03706D255746C1B8E78735EC8FE86751E64CF6DC (void);
// 0x0000009E System.Void Guest/<GuestCone>d__33::System.Collections.IEnumerator.Reset()
extern void U3CGuestConeU3Ed__33_System_Collections_IEnumerator_Reset_m6EB7CFD7A25750D3838767516B1923831FBFD25B (void);
// 0x0000009F System.Object Guest/<GuestCone>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CGuestConeU3Ed__33_System_Collections_IEnumerator_get_Current_m5FBCA8C78D33C14A94294946057F93A3AC0EB650 (void);
// 0x000000A0 System.Void GuestManager::Configuer(SystemGameManager)
extern void GuestManager_Configuer_m1E9DA203833945624D4DE65C6097CEFDCAE2F120 (void);
// 0x000000A1 System.Void GuestManager::Init()
extern void GuestManager_Init_m45E7FF5936ABD0664DFA4B3E69A2452415AD79AC (void);
// 0x000000A2 System.Void GuestManager::OnDisable()
extern void GuestManager_OnDisable_m2EDCF3E2F76F60B2B7BBA2B899194F2AA3D130EF (void);
// 0x000000A3 System.Collections.IEnumerator GuestManager::ResetManager()
extern void GuestManager_ResetManager_mE99D83B998017EDADC2439153B6D801747148781 (void);
// 0x000000A4 System.Int32 GuestManager::GetCurrLevelGuestCount()
extern void GuestManager_GetCurrLevelGuestCount_m3C1856131C809902D8693E56463782123ED91979 (void);
// 0x000000A5 System.Void GuestManager::RunGuestManager()
extern void GuestManager_RunGuestManager_mFA5181EFEC0A93BE8661A166999765959AD3EF26 (void);
// 0x000000A6 System.Void GuestManager::GuestEnd()
extern void GuestManager_GuestEnd_m68F73E70E0A57C5B55B1597EC31C2F837FF5920A (void);
// 0x000000A7 System.Void GuestManager::SpawnGuest()
extern void GuestManager_SpawnGuest_mA2F3C6296B38A376F1B0AF6AF14A42844E499B45 (void);
// 0x000000A8 System.Void GuestManager::SetGuestMenu(System.Int32,FoodMenu)
extern void GuestManager_SetGuestMenu_mC73DD1B65089D6D810C7142C91ECD730195C05C6 (void);
// 0x000000A9 System.Boolean GuestManager::foodMenuLock(FoodMenu)
extern void GuestManager_foodMenuLock_m54A1DB892E5FA332D714A9DDC1548A7655CD96CE (void);
// 0x000000AA System.Void GuestManager::OneceMoreStateGuest(System.Int32)
extern void GuestManager_OneceMoreStateGuest_m6AAA9E603FF93C4D6CC5C7B15E2BBC23B8B19D1E (void);
// 0x000000AB System.Void GuestManager::StateGuestCome()
extern void GuestManager_StateGuestCome_m11B99B303676065836203EF0DE7BDBA3A1A2AAE4 (void);
// 0x000000AC System.Collections.IEnumerator GuestManager::StateGuestComeOne()
extern void GuestManager_StateGuestComeOne_m2819DA0993B30BFCC5FD99482398168B67782421 (void);
// 0x000000AD System.Boolean GuestManager::CompeletTarget()
extern void GuestManager_CompeletTarget_m4E36C1A74EC5B3CA3F5F480794A187C9643142A5 (void);
// 0x000000AE System.Void GuestManager::AddTollGuserData(System.String,GuestTable)
extern void GuestManager_AddTollGuserData_m291B71D90F6426B008E2581AC56463147D3A449D (void);
// 0x000000AF System.Void GuestManager::CallGuestReceiveFood(System.String,EventParamProperties)
extern void GuestManager_CallGuestReceiveFood_mB1F1BFC2821E2248E533078A2B35E45956D9FA5F (void);
// 0x000000B0 System.Void GuestManager::GiveGuestFood(System.String,System.Int32,System.Single,System.Single)
extern void GuestManager_GiveGuestFood_m7A813A21D5DAA852755257C7A0F23A994C64D469 (void);
// 0x000000B1 System.Void GuestManager::CallAddDrink(System.String,EventParamProperties)
extern void GuestManager_CallAddDrink_m9C7D121CC175248326023C886C05479FE471AC7A (void);
// 0x000000B2 System.Void GuestManager::.ctor()
extern void GuestManager__ctor_m737B5B7C4F0D12A119EF07DD7CC74E07597DE844 (void);
// 0x000000B3 System.Void GuestManager/<ResetManager>d__11::.ctor(System.Int32)
extern void U3CResetManagerU3Ed__11__ctor_m25797BFC57E47A30920DEBC8B61AEE25EE03712A (void);
// 0x000000B4 System.Void GuestManager/<ResetManager>d__11::System.IDisposable.Dispose()
extern void U3CResetManagerU3Ed__11_System_IDisposable_Dispose_mFEB35C550D0DD1EA2EAA7A17F3B2B077B5C509AD (void);
// 0x000000B5 System.Boolean GuestManager/<ResetManager>d__11::MoveNext()
extern void U3CResetManagerU3Ed__11_MoveNext_m4EAEF24C56807873A73CAD49E61A438EEF18C3A3 (void);
// 0x000000B6 System.Object GuestManager/<ResetManager>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResetManagerU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60C2FD3279EC280632304C5003C316F5BC339224 (void);
// 0x000000B7 System.Void GuestManager/<ResetManager>d__11::System.Collections.IEnumerator.Reset()
extern void U3CResetManagerU3Ed__11_System_Collections_IEnumerator_Reset_mDD64A7EA4908862A56386DE9F92354A6CEEFACCD (void);
// 0x000000B8 System.Object GuestManager/<ResetManager>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CResetManagerU3Ed__11_System_Collections_IEnumerator_get_Current_m5AB29A93F44AF9AC5F3CF35088B4B195D7932E6C (void);
// 0x000000B9 System.Void GuestManager/<StateGuestComeOne>d__20::.ctor(System.Int32)
extern void U3CStateGuestComeOneU3Ed__20__ctor_m10A5B4D963F3C464B86F48756B1EA76D24F2875F (void);
// 0x000000BA System.Void GuestManager/<StateGuestComeOne>d__20::System.IDisposable.Dispose()
extern void U3CStateGuestComeOneU3Ed__20_System_IDisposable_Dispose_m5CA8C6C298E0C9EE9D43569C253E9A548EE9AA7F (void);
// 0x000000BB System.Boolean GuestManager/<StateGuestComeOne>d__20::MoveNext()
extern void U3CStateGuestComeOneU3Ed__20_MoveNext_mDED9ABCAACCDCBF98563AB0295640A464046760B (void);
// 0x000000BC System.Object GuestManager/<StateGuestComeOne>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStateGuestComeOneU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m740C2CB747EE772625C674A6685C383C23160AE6 (void);
// 0x000000BD System.Void GuestManager/<StateGuestComeOne>d__20::System.Collections.IEnumerator.Reset()
extern void U3CStateGuestComeOneU3Ed__20_System_Collections_IEnumerator_Reset_m671B8D0358C7315D168F7EFB54114904C977E1DA (void);
// 0x000000BE System.Object GuestManager/<StateGuestComeOne>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CStateGuestComeOneU3Ed__20_System_Collections_IEnumerator_get_Current_m0855980882734C8C1D980A1025A405A44E98E2A7 (void);
// 0x000000BF System.Void GuideInfo::ClickEvent()
extern void GuideInfo_ClickEvent_mC3B688B4BDE99DB434D12EC21235F0050BD848B2 (void);
// 0x000000C0 System.Void GuideInfo::.ctor()
extern void GuideInfo__ctor_m08A0194DE6C0C723ECC3BF6631E3BEF66E9815CF (void);
// 0x000000C1 System.Void SystemGuideEvent::Awake()
extern void SystemGuideEvent_Awake_mAF45D7ADB7485C5D3BB3FE41068AA9A11689720E (void);
// 0x000000C2 System.Void SystemGuideEvent::Start()
extern void SystemGuideEvent_Start_m33957F6C7D30CBC9DEA8811E3217112681A0F920 (void);
// 0x000000C3 System.Void SystemGuideEvent::AddOnceGame()
extern void SystemGuideEvent_AddOnceGame_m8445858E364BE743F7319E7E2719081A394B09CC (void);
// 0x000000C4 System.Void SystemGuideEvent::ShowGuide(System.String,System.Action)
extern void SystemGuideEvent_ShowGuide_mE8D393F0001B43056B42A98B409261EC0EA0625F (void);
// 0x000000C5 System.Void SystemGuideEvent::.ctor()
extern void SystemGuideEvent__ctor_m6334B6456DD6F4CB42712E255F0C91A19B28E602 (void);
// 0x000000C6 System.Int32 LevelManager::get_CurrLevel()
extern void LevelManager_get_CurrLevel_mA248FF9400D0E4DC8D0FD0F6B7423A9486F1EA40 (void);
// 0x000000C7 System.Void LevelManager::set_CurrLevel(System.Int32)
extern void LevelManager_set_CurrLevel_mBC4CC002D968B932972BD8F401B028AC0A4AEC54 (void);
// 0x000000C8 RestaurantInfo LevelManager::get_CurrRestaurantInfo()
extern void LevelManager_get_CurrRestaurantInfo_mF963844AF5EC0F36BAD3FB13CC3D20970DE29D62 (void);
// 0x000000C9 System.Void LevelManager::Configur(SystemGameManager)
extern void LevelManager_Configur_mFDA771DFE4BBA34ECADB4A337DC43B69200E70FF (void);
// 0x000000CA LevelData LevelManager::GetCurrLevelData()
extern void LevelManager_GetCurrLevelData_m7EBB842E6F572EEAE054F5F539207733D59F25B6 (void);
// 0x000000CB System.Collections.IEnumerator LevelManager::RestManager()
extern void LevelManager_RestManager_mFD17CE9472415142AF83CD9062947944EF5482AB (void);
// 0x000000CC System.Void LevelManager::SpawnRestaurant()
extern void LevelManager_SpawnRestaurant_m9589BFB79634C95224553B326EDBF3C320DCFC8E (void);
// 0x000000CD System.Collections.IEnumerator LevelManager::SpawnRestauranYoo()
extern void LevelManager_SpawnRestauranYoo_m803475F9106881A6CCEED8979A2798B2A12B7E21 (void);
// 0x000000CE System.Void LevelManager::.ctor()
extern void LevelManager__ctor_m97F35AC08C296B73BD7D85FFB593A7BEA61B3F92 (void);
// 0x000000CF System.Void LevelManager/<RestManager>d__12::.ctor(System.Int32)
extern void U3CRestManagerU3Ed__12__ctor_m1107F88F0DEE0EC07E51CF67F15FBE2DA28FA88B (void);
// 0x000000D0 System.Void LevelManager/<RestManager>d__12::System.IDisposable.Dispose()
extern void U3CRestManagerU3Ed__12_System_IDisposable_Dispose_mDE66F2B3784773AB60500935B81F60DE841DE9CE (void);
// 0x000000D1 System.Boolean LevelManager/<RestManager>d__12::MoveNext()
extern void U3CRestManagerU3Ed__12_MoveNext_mA8488ECFCE8852E25181A8FF432C0DE680F1650E (void);
// 0x000000D2 System.Object LevelManager/<RestManager>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRestManagerU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB4C668E196003D1292524F03B6AF0011E50668A1 (void);
// 0x000000D3 System.Void LevelManager/<RestManager>d__12::System.Collections.IEnumerator.Reset()
extern void U3CRestManagerU3Ed__12_System_Collections_IEnumerator_Reset_mEA8129A9EBAD039B0B0E0885ACFC3A547AA3BEE3 (void);
// 0x000000D4 System.Object LevelManager/<RestManager>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CRestManagerU3Ed__12_System_Collections_IEnumerator_get_Current_m7F2B45FCEA29A5162BB5958FB852CD722DFA778A (void);
// 0x000000D5 System.Void LevelManager/<SpawnRestauranYoo>d__14::.ctor(System.Int32)
extern void U3CSpawnRestauranYooU3Ed__14__ctor_m2B2DC5A7B856D7E256594B3F3F23F70530168C99 (void);
// 0x000000D6 System.Void LevelManager/<SpawnRestauranYoo>d__14::System.IDisposable.Dispose()
extern void U3CSpawnRestauranYooU3Ed__14_System_IDisposable_Dispose_m0400A35DABA2DF91023CD4E460010D11403D9BA6 (void);
// 0x000000D7 System.Boolean LevelManager/<SpawnRestauranYoo>d__14::MoveNext()
extern void U3CSpawnRestauranYooU3Ed__14_MoveNext_mF440571B9199BF2EA8809E30503022144DC406D1 (void);
// 0x000000D8 System.Void LevelManager/<SpawnRestauranYoo>d__14::<>m__Finally1()
extern void U3CSpawnRestauranYooU3Ed__14_U3CU3Em__Finally1_mFF9A547AC3DAD345D43FAB1B2AB975A7D70FC370 (void);
// 0x000000D9 System.Object LevelManager/<SpawnRestauranYoo>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnRestauranYooU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31DC238ABAB71D32CE9CE8F5E4E943E9B0257224 (void);
// 0x000000DA System.Void LevelManager/<SpawnRestauranYoo>d__14::System.Collections.IEnumerator.Reset()
extern void U3CSpawnRestauranYooU3Ed__14_System_Collections_IEnumerator_Reset_mA0FC535E9C3B9815E0ECD14C0F23DF011515FF94 (void);
// 0x000000DB System.Object LevelManager/<SpawnRestauranYoo>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnRestauranYooU3Ed__14_System_Collections_IEnumerator_get_Current_m05C067FF9D6A438BBFD563194CE26798FDFD9D09 (void);
// 0x000000DC System.Collections.Generic.List`1<LevelData> LoadConfigursManager::get_LevelDataList()
extern void LoadConfigursManager_get_LevelDataList_m8F75A8550E70B1397BF65EE153BDCD43B5129940 (void);
// 0x000000DD System.Collections.Generic.Dictionary`2<System.String,OtherFoodItem> LoadConfigursManager::get_OetherFoodDictionary()
extern void LoadConfigursManager_get_OetherFoodDictionary_m36B7FC9F0D6DC32F3E4AC2DEE549AE373483FDA5 (void);
// 0x000000DE System.Collections.Generic.Dictionary`2<System.String,GuestTable> LoadConfigursManager::get_LevelGuestDataDrictionary()
extern void LoadConfigursManager_get_LevelGuestDataDrictionary_mDEFE35D5A36BB7CDD161E4073C872BA7E1701A7E (void);
// 0x000000DF System.Collections.Generic.Dictionary`2<System.String,FoodMenu> LoadConfigursManager::get_FoodMendDicrionary()
extern void LoadConfigursManager_get_FoodMendDicrionary_mAE13857BC3759CFD1E7D5320F0EB83423A804FA4 (void);
// 0x000000E0 System.Collections.Generic.Dictionary`2<System.String,FoodItem> LoadConfigursManager::get_FoodItemDictionary()
extern void LoadConfigursManager_get_FoodItemDictionary_mBB5535E4217C463EEDD30CF00B0F545961FACECA (void);
// 0x000000E1 System.Collections.Generic.Dictionary`2<System.String,MainFoodItem> LoadConfigursManager::get_MainFoodItemDictionary()
extern void LoadConfigursManager_get_MainFoodItemDictionary_m05C6DBBA287F6F0E66240F5F4C853076BCF4B97F (void);
// 0x000000E2 System.Collections.Generic.List`1<GameTaskData> LoadConfigursManager::get_TaskList()
extern void LoadConfigursManager_get_TaskList_m4347F42FCE07A8DD37998C2B97C0D7A183D907D3 (void);
// 0x000000E3 System.Boolean LoadConfigursManager::CreateLoadData()
extern void LoadConfigursManager_CreateLoadData_mC05CB48283C754DC8D477F9F9886E90FF5319CFE (void);
// 0x000000E4 System.Collections.IEnumerator LoadConfigursManager::Configuer(SystmConfigurationManager)
extern void LoadConfigursManager_Configuer_m6B0B924FF4B416FB9833E39B05546FC5F412A673 (void);
// 0x000000E5 System.Collections.IEnumerator LoadConfigursManager::LoadGameData()
extern void LoadConfigursManager_LoadGameData_mCC0C37AEAB2789459128CECB16A7B05389D828D2 (void);
// 0x000000E6 System.Collections.IEnumerator LoadConfigursManager::LoadLevel()
extern void LoadConfigursManager_LoadLevel_m3397FFF0C54833CC9FF4D442DFACA23624ABADF6 (void);
// 0x000000E7 System.Collections.IEnumerator LoadConfigursManager::LoadLlvelGuset()
extern void LoadConfigursManager_LoadLlvelGuset_mA09446075E27282615E56CC050A811491DE6445B (void);
// 0x000000E8 System.Collections.IEnumerator LoadConfigursManager::LoadFoodMenu()
extern void LoadConfigursManager_LoadFoodMenu_m247B655731C858C6C86F9A6C1AE469F9355A2424 (void);
// 0x000000E9 System.Collections.IEnumerator LoadConfigursManager::LoadFoodItem()
extern void LoadConfigursManager_LoadFoodItem_m02E050D58D7FF5E9E6859F628E519A7D6C799268 (void);
// 0x000000EA System.Collections.IEnumerator LoadConfigursManager::LoadMainFoodItem()
extern void LoadConfigursManager_LoadMainFoodItem_m275260C59226326E1083F630D988780B51294647 (void);
// 0x000000EB System.Collections.IEnumerator LoadConfigursManager::LoadOtherFoodItem()
extern void LoadConfigursManager_LoadOtherFoodItem_m266311F77A2B35DB3420548C85487B1D54D0E3EF (void);
// 0x000000EC System.Collections.IEnumerator LoadConfigursManager::LoadTaskData()
extern void LoadConfigursManager_LoadTaskData_m642212D10E509B10AEDB5ED423CACDB8D9A061A3 (void);
// 0x000000ED System.Void LoadConfigursManager::.ctor()
extern void LoadConfigursManager__ctor_m70E7AD404DF3703056D75354E072129D8A3C5BB5 (void);
// 0x000000EE System.Void LoadConfigursManager/<Configuer>d__25::.ctor(System.Int32)
extern void U3CConfiguerU3Ed__25__ctor_m9BF13BFE8A290FD12FCDB0F85D8D655105421D1F (void);
// 0x000000EF System.Void LoadConfigursManager/<Configuer>d__25::System.IDisposable.Dispose()
extern void U3CConfiguerU3Ed__25_System_IDisposable_Dispose_mC23AE4FF0E01CA7B41C3F560B9F45AD066F7165F (void);
// 0x000000F0 System.Boolean LoadConfigursManager/<Configuer>d__25::MoveNext()
extern void U3CConfiguerU3Ed__25_MoveNext_m9C2EC606E1BB3A7F4C1968626657B70F283CE0F3 (void);
// 0x000000F1 System.Object LoadConfigursManager/<Configuer>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CConfiguerU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m38808BF86499BC4F1DE18F706EC4809A7AAA3AC6 (void);
// 0x000000F2 System.Void LoadConfigursManager/<Configuer>d__25::System.Collections.IEnumerator.Reset()
extern void U3CConfiguerU3Ed__25_System_Collections_IEnumerator_Reset_m5D04295C3369EAD257995C8C972FBAF86EBEA5CE (void);
// 0x000000F3 System.Object LoadConfigursManager/<Configuer>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CConfiguerU3Ed__25_System_Collections_IEnumerator_get_Current_m3C60F36356282E59C8777AEA4FEB49495C355F9B (void);
// 0x000000F4 System.Void LoadConfigursManager/<LoadGameData>d__26::.ctor(System.Int32)
extern void U3CLoadGameDataU3Ed__26__ctor_m69920F5A82686334E47741F404C358F90B223D46 (void);
// 0x000000F5 System.Void LoadConfigursManager/<LoadGameData>d__26::System.IDisposable.Dispose()
extern void U3CLoadGameDataU3Ed__26_System_IDisposable_Dispose_m37EF2A08DA94685C88623BEB3CAE99A31E06056B (void);
// 0x000000F6 System.Boolean LoadConfigursManager/<LoadGameData>d__26::MoveNext()
extern void U3CLoadGameDataU3Ed__26_MoveNext_m7BAAE6692FAA56B6A9425CC5B2A7879F1140C4AF (void);
// 0x000000F7 System.Object LoadConfigursManager/<LoadGameData>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadGameDataU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1B136C554167950EAD4FBA400C0357C8E8656E92 (void);
// 0x000000F8 System.Void LoadConfigursManager/<LoadGameData>d__26::System.Collections.IEnumerator.Reset()
extern void U3CLoadGameDataU3Ed__26_System_Collections_IEnumerator_Reset_m22EE74F1CC14356DFDD1DE6B6C3CE932D8AA0027 (void);
// 0x000000F9 System.Object LoadConfigursManager/<LoadGameData>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CLoadGameDataU3Ed__26_System_Collections_IEnumerator_get_Current_m2512E0381B54CE83797A6AFF266905CD224F4BF1 (void);
// 0x000000FA System.Void LoadConfigursManager/<LoadLevel>d__27::.ctor(System.Int32)
extern void U3CLoadLevelU3Ed__27__ctor_m211C9203A1D4E624153DD184197EA5119AA94182 (void);
// 0x000000FB System.Void LoadConfigursManager/<LoadLevel>d__27::System.IDisposable.Dispose()
extern void U3CLoadLevelU3Ed__27_System_IDisposable_Dispose_m27D084B3024499C26AF51D62993110F35A24308B (void);
// 0x000000FC System.Boolean LoadConfigursManager/<LoadLevel>d__27::MoveNext()
extern void U3CLoadLevelU3Ed__27_MoveNext_m2CF8DB3BEE32F498AA22FCF0A8EE775EC0DBAC6F (void);
// 0x000000FD System.Object LoadConfigursManager/<LoadLevel>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadLevelU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA09FDD773DF28A1AC1E580F8A232852B17A9508D (void);
// 0x000000FE System.Void LoadConfigursManager/<LoadLevel>d__27::System.Collections.IEnumerator.Reset()
extern void U3CLoadLevelU3Ed__27_System_Collections_IEnumerator_Reset_m735476FC354847C8EE4164C47EB473011E05C829 (void);
// 0x000000FF System.Object LoadConfigursManager/<LoadLevel>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CLoadLevelU3Ed__27_System_Collections_IEnumerator_get_Current_mF83ABA21793A0900C74E2AF918EB96768E0E5D09 (void);
// 0x00000100 System.Void LoadConfigursManager/<LoadLlvelGuset>d__28::.ctor(System.Int32)
extern void U3CLoadLlvelGusetU3Ed__28__ctor_m661B826FF14E724C97015F108927A0B31EF7D351 (void);
// 0x00000101 System.Void LoadConfigursManager/<LoadLlvelGuset>d__28::System.IDisposable.Dispose()
extern void U3CLoadLlvelGusetU3Ed__28_System_IDisposable_Dispose_mDBB6CFFC80EDDAAC5A09121011E7BB6DA856287A (void);
// 0x00000102 System.Boolean LoadConfigursManager/<LoadLlvelGuset>d__28::MoveNext()
extern void U3CLoadLlvelGusetU3Ed__28_MoveNext_mD94147E62EFE6753EFCCE2FC35F51FE6C66BBFB4 (void);
// 0x00000103 System.Object LoadConfigursManager/<LoadLlvelGuset>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadLlvelGusetU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3584D7F85E906FFAC71149BEAFA0F5220F13B37F (void);
// 0x00000104 System.Void LoadConfigursManager/<LoadLlvelGuset>d__28::System.Collections.IEnumerator.Reset()
extern void U3CLoadLlvelGusetU3Ed__28_System_Collections_IEnumerator_Reset_m516AD0BB878982920CD09E951BC520747876F5E2 (void);
// 0x00000105 System.Object LoadConfigursManager/<LoadLlvelGuset>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CLoadLlvelGusetU3Ed__28_System_Collections_IEnumerator_get_Current_m718F57AE8B59A2B09FD5E4730A6CEEF30BED7F15 (void);
// 0x00000106 System.Void LoadConfigursManager/<LoadFoodMenu>d__29::.ctor(System.Int32)
extern void U3CLoadFoodMenuU3Ed__29__ctor_m869B922E41435332C272B5F6C8ED29FE60D489AF (void);
// 0x00000107 System.Void LoadConfigursManager/<LoadFoodMenu>d__29::System.IDisposable.Dispose()
extern void U3CLoadFoodMenuU3Ed__29_System_IDisposable_Dispose_m4AA5BAAF16FE4D06E27592E83F41D64D357727DE (void);
// 0x00000108 System.Boolean LoadConfigursManager/<LoadFoodMenu>d__29::MoveNext()
extern void U3CLoadFoodMenuU3Ed__29_MoveNext_m6B972CDD976CE893E490760FBBF7796E9AC50F72 (void);
// 0x00000109 System.Object LoadConfigursManager/<LoadFoodMenu>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadFoodMenuU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8BEAED93B1409EC8D2D9722993E6C84CAB8FA3ED (void);
// 0x0000010A System.Void LoadConfigursManager/<LoadFoodMenu>d__29::System.Collections.IEnumerator.Reset()
extern void U3CLoadFoodMenuU3Ed__29_System_Collections_IEnumerator_Reset_m28B2A4107B6863ECB0FB63CF77F669938A64134C (void);
// 0x0000010B System.Object LoadConfigursManager/<LoadFoodMenu>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CLoadFoodMenuU3Ed__29_System_Collections_IEnumerator_get_Current_mECC1CF0109EB7720779D199107B675DA6A2F9DC8 (void);
// 0x0000010C System.Void LoadConfigursManager/<LoadFoodItem>d__30::.ctor(System.Int32)
extern void U3CLoadFoodItemU3Ed__30__ctor_mB6C6F8744DABF0ABEAB18143ACEC1CFA9D6AF9C9 (void);
// 0x0000010D System.Void LoadConfigursManager/<LoadFoodItem>d__30::System.IDisposable.Dispose()
extern void U3CLoadFoodItemU3Ed__30_System_IDisposable_Dispose_m3C544C066A806052C10EC384161A4C29613F1100 (void);
// 0x0000010E System.Boolean LoadConfigursManager/<LoadFoodItem>d__30::MoveNext()
extern void U3CLoadFoodItemU3Ed__30_MoveNext_m8ADE63C3F3284B1CBB0800741EB78B5C07897A06 (void);
// 0x0000010F System.Object LoadConfigursManager/<LoadFoodItem>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadFoodItemU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DE1903E3664282E99E6770B40BFE011B10E7648 (void);
// 0x00000110 System.Void LoadConfigursManager/<LoadFoodItem>d__30::System.Collections.IEnumerator.Reset()
extern void U3CLoadFoodItemU3Ed__30_System_Collections_IEnumerator_Reset_m6A30FFF929371D2F8172E7A4FB3DEFFF944FBCD7 (void);
// 0x00000111 System.Object LoadConfigursManager/<LoadFoodItem>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CLoadFoodItemU3Ed__30_System_Collections_IEnumerator_get_Current_m372594A8052EA16CC04E1B3E8AC1B6B31C7868BC (void);
// 0x00000112 System.Void LoadConfigursManager/<LoadMainFoodItem>d__31::.ctor(System.Int32)
extern void U3CLoadMainFoodItemU3Ed__31__ctor_m278CC63ECC1F6D8CDEA5DE73B10CA8C7AE931AA6 (void);
// 0x00000113 System.Void LoadConfigursManager/<LoadMainFoodItem>d__31::System.IDisposable.Dispose()
extern void U3CLoadMainFoodItemU3Ed__31_System_IDisposable_Dispose_m449BD9A0BE940725D5144310D4FFB412583DF7DA (void);
// 0x00000114 System.Boolean LoadConfigursManager/<LoadMainFoodItem>d__31::MoveNext()
extern void U3CLoadMainFoodItemU3Ed__31_MoveNext_mEFFDA34F57E03AD56ED542BABDDAA3208B60779B (void);
// 0x00000115 System.Object LoadConfigursManager/<LoadMainFoodItem>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadMainFoodItemU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC28457AB84A079958A94D124E77AD99490251096 (void);
// 0x00000116 System.Void LoadConfigursManager/<LoadMainFoodItem>d__31::System.Collections.IEnumerator.Reset()
extern void U3CLoadMainFoodItemU3Ed__31_System_Collections_IEnumerator_Reset_m8DF21827E6A731A1487C27098E805BC2CA7AAC05 (void);
// 0x00000117 System.Object LoadConfigursManager/<LoadMainFoodItem>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CLoadMainFoodItemU3Ed__31_System_Collections_IEnumerator_get_Current_m75EFBF9A78E565190328770B25559C31A6445AE3 (void);
// 0x00000118 System.Void LoadConfigursManager/<LoadOtherFoodItem>d__32::.ctor(System.Int32)
extern void U3CLoadOtherFoodItemU3Ed__32__ctor_m3ACA5448A5F88ADF63D79EF57D03315DE832034E (void);
// 0x00000119 System.Void LoadConfigursManager/<LoadOtherFoodItem>d__32::System.IDisposable.Dispose()
extern void U3CLoadOtherFoodItemU3Ed__32_System_IDisposable_Dispose_m7BE9EC73D03B07252DBE706932DDDC173EE9BB0D (void);
// 0x0000011A System.Boolean LoadConfigursManager/<LoadOtherFoodItem>d__32::MoveNext()
extern void U3CLoadOtherFoodItemU3Ed__32_MoveNext_m2C0E761B4F7B634ACFC9A150E3C7B26176EB11FD (void);
// 0x0000011B System.Object LoadConfigursManager/<LoadOtherFoodItem>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadOtherFoodItemU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE9AF60BAED187D79F60B1A36EBAEDC23224E8C68 (void);
// 0x0000011C System.Void LoadConfigursManager/<LoadOtherFoodItem>d__32::System.Collections.IEnumerator.Reset()
extern void U3CLoadOtherFoodItemU3Ed__32_System_Collections_IEnumerator_Reset_m337C715E2D4773E5E3166F32203038A3E016B646 (void);
// 0x0000011D System.Object LoadConfigursManager/<LoadOtherFoodItem>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CLoadOtherFoodItemU3Ed__32_System_Collections_IEnumerator_get_Current_m448071658DBC519EEA6C1C46EEEFBDC7AC240CCC (void);
// 0x0000011E System.Void LoadConfigursManager/<LoadTaskData>d__33::.ctor(System.Int32)
extern void U3CLoadTaskDataU3Ed__33__ctor_m6940EC9635F83D1A412B6C5EDAB50AFF4D71F059 (void);
// 0x0000011F System.Void LoadConfigursManager/<LoadTaskData>d__33::System.IDisposable.Dispose()
extern void U3CLoadTaskDataU3Ed__33_System_IDisposable_Dispose_m27FB299F26E52188C8B0F7D0D1DA433CCC4122A0 (void);
// 0x00000120 System.Boolean LoadConfigursManager/<LoadTaskData>d__33::MoveNext()
extern void U3CLoadTaskDataU3Ed__33_MoveNext_mFE6D830FC90936F1A74CCE23673122CDFE6F6A72 (void);
// 0x00000121 System.Object LoadConfigursManager/<LoadTaskData>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadTaskDataU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54A685D362F3F13655316F16FFC2C92A2B171708 (void);
// 0x00000122 System.Void LoadConfigursManager/<LoadTaskData>d__33::System.Collections.IEnumerator.Reset()
extern void U3CLoadTaskDataU3Ed__33_System_Collections_IEnumerator_Reset_mEF06AE983088FB860B42600459EDA5EF1FCDDD78 (void);
// 0x00000123 System.Object LoadConfigursManager/<LoadTaskData>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CLoadTaskDataU3Ed__33_System_Collections_IEnumerator_get_Current_mD6107444418461B83F9EBB1F9E36CD7712A6932C (void);
// 0x00000124 System.Void MapManager::Configur(SystemGameManager)
extern void MapManager_Configur_m9064C96483E3067D44A003C233CB311765F4E5E0 (void);
// 0x00000125 System.Void MapManager::Init()
extern void MapManager_Init_mDEAFF88C04EF2378FD0C6EDF230ADC619538EEE5 (void);
// 0x00000126 System.Void MapManager::MoveMap()
extern void MapManager_MoveMap_m24BAC81AC951C12C29D831D443E133988C7306DF (void);
// 0x00000127 System.Void MapManager::.ctor()
extern void MapManager__ctor_m622D319D4B07043DDB690FB88FD531C149C63A7F (void);
// 0x00000128 System.Collections.Generic.Dictionary`2<System.String,MainFoodCompnentAssets> OperationManage::get_CurrMainFooResdDictionary()
extern void OperationManage_get_CurrMainFooResdDictionary_m4F55677A7179EC9B9FDD248445ECE686155D20D4 (void);
// 0x00000129 System.Void OperationManage::Configuer(SystemGameManager)
extern void OperationManage_Configuer_mAB091D6CF5433BFA445E6099CF71499393A774BC (void);
// 0x0000012A System.Void OperationManage::Init()
extern void OperationManage_Init_m67D156AE8CB4361BA8C5AFB1459BABCD31F57788 (void);
// 0x0000012B System.Void OperationManage::OnDisable()
extern void OperationManage_OnDisable_m94142E27019232D69C1AF2BE8A770EF7688129F8 (void);
// 0x0000012C System.Collections.IEnumerator OperationManage::ResetManager()
extern void OperationManage_ResetManager_mA3090BABFD001F709C472ABD8E2EB2C0C04952B1 (void);
// 0x0000012D System.Void OperationManage::LoadingFood()
extern void OperationManage_LoadingFood_m20692AC5B14356B5D11BEDA29815659A926D0A8C (void);
// 0x0000012E System.Void OperationManage::CallMakingFood(System.String,EventParamProperties)
extern void OperationManage_CallMakingFood_m0D7EDA5A5981F13E96C3E1A9DE27F57A6BF001C9 (void);
// 0x0000012F System.Void OperationManage::CallAddOtherFood(System.String,EventParamProperties)
extern void OperationManage_CallAddOtherFood_m7A34B4BB3EFD9B1592784D378915887E6DA0D762 (void);
// 0x00000130 System.Void OperationManage::CallCoodeFood(System.String,EventParamProperties)
extern void OperationManage_CallCoodeFood_m5FA51442F293EC6164D83ACF1C3450D930328A0D (void);
// 0x00000131 System.Void OperationManage::CallSubmitCompeletFood(System.String,EventParamProperties)
extern void OperationManage_CallSubmitCompeletFood_mD6AE251BAF1F28A2FEFC9C6E02AC9C1AAF833D34 (void);
// 0x00000132 System.Void OperationManage::.ctor()
extern void OperationManage__ctor_mE70FE15C790E75B857B3104503E313376624AB02 (void);
// 0x00000133 System.Void OperationManage/<ResetManager>d__14::.ctor(System.Int32)
extern void U3CResetManagerU3Ed__14__ctor_m667DDBDE193598F0FA20A8B46A27CF6BFBE409A1 (void);
// 0x00000134 System.Void OperationManage/<ResetManager>d__14::System.IDisposable.Dispose()
extern void U3CResetManagerU3Ed__14_System_IDisposable_Dispose_m3E97DBE382CC8F43F7DE6F7FAF11A807078FA3FE (void);
// 0x00000135 System.Boolean OperationManage/<ResetManager>d__14::MoveNext()
extern void U3CResetManagerU3Ed__14_MoveNext_m2A042EDF6AA8C585A9DC2AAA56909243AFAAA0BC (void);
// 0x00000136 System.Object OperationManage/<ResetManager>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResetManagerU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9FDB8AD2EC9E49D54CBB3E58C2F648FB514112CB (void);
// 0x00000137 System.Void OperationManage/<ResetManager>d__14::System.Collections.IEnumerator.Reset()
extern void U3CResetManagerU3Ed__14_System_Collections_IEnumerator_Reset_m4307E06D8893C70F12C370B46BA545A81507FD04 (void);
// 0x00000138 System.Object OperationManage/<ResetManager>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CResetManagerU3Ed__14_System_Collections_IEnumerator_get_Current_m69388E442045B69F5FE587860F75B9E1598964C2 (void);
// 0x00000139 System.Void PlayerDataParameter::.ctor()
extern void PlayerDataParameter__ctor_m5DF18427D5F2CD076A02E1B945B96B2F6F1C8066 (void);
// 0x0000013A PlayerData PlayerData::get_Inastance()
extern void PlayerData_get_Inastance_m3832A091380C1B3B6DD43C1BA59B488E305B2199 (void);
// 0x0000013B System.Void PlayerData::Create(UnityEngine.MonoBehaviour)
extern void PlayerData_Create_m5E3DDCC42DBEC53E52B32ECB8D70F97B1D6A24E3 (void);
// 0x0000013C System.Void PlayerData::Create()
extern void PlayerData_Create_m1D5ADD9C6648466D8524AC1EAE9264EC3D2BE02B (void);
// 0x0000013D System.Collections.IEnumerator PlayerData::Read()
extern void PlayerData_Read_m39AE6A510549330E8D762319702DF7EEF15C3D12 (void);
// 0x0000013E System.Void PlayerData::NewSave()
extern void PlayerData_NewSave_m895A3A53BA086830E6228EB6CEE9C310AA181EF0 (void);
// 0x0000013F System.Void PlayerData::Save()
extern void PlayerData_Save_m7ACDE3046A5377BD21EB86D7A5B2D35036663CE4 (void);
// 0x00000140 System.Void PlayerData::ClearSave()
extern void PlayerData_ClearSave_m097943EAE9AD065D37871712D3BEE689A5CB149A (void);
// 0x00000141 System.Void PlayerData::.ctor()
extern void PlayerData__ctor_mDD79CC7712EAF325215F97832BFE3403DF47DFA8 (void);
// 0x00000142 System.Void PlayerData::.cctor()
extern void PlayerData__cctor_mAA4A511FFB992534007E014521B7BC752BA46776 (void);
// 0x00000143 System.Void PlayerData/<Read>d__9::.ctor(System.Int32)
extern void U3CReadU3Ed__9__ctor_m82130BBB229ADF75F01F85196B6AD408BA7C5FC8 (void);
// 0x00000144 System.Void PlayerData/<Read>d__9::System.IDisposable.Dispose()
extern void U3CReadU3Ed__9_System_IDisposable_Dispose_mB3B9737706374FF9000C326636044E6458660CEE (void);
// 0x00000145 System.Boolean PlayerData/<Read>d__9::MoveNext()
extern void U3CReadU3Ed__9_MoveNext_m4E566CD1B19E7D6265E782BD6DFF61F8B402CA12 (void);
// 0x00000146 System.Object PlayerData/<Read>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReadU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC6B0780885F02C5CF7E125ECCA231C7FC7696AE9 (void);
// 0x00000147 System.Void PlayerData/<Read>d__9::System.Collections.IEnumerator.Reset()
extern void U3CReadU3Ed__9_System_Collections_IEnumerator_Reset_m08792EB7D144ED8F19C45A285841C8D6A2AC747C (void);
// 0x00000148 System.Object PlayerData/<Read>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CReadU3Ed__9_System_Collections_IEnumerator_get_Current_m1291C058D013C746335A37DE95EF8F0A5C2B9A0F (void);
// 0x00000149 System.String ResourceProfile::get_ConfigurName()
extern void ResourceProfile_get_ConfigurName_m32791B0D98CF227EDCE782BC6A31C03412A05D70 (void);
// 0x0000014A System.Void ResourceProfile::set_ConfigurName(System.String)
extern void ResourceProfile_set_ConfigurName_m8E7B8382885915E24D6BB0B76885619FC22FB642 (void);
// 0x0000014B System.Void ResourceProfile::.ctor()
extern void ResourceProfile__ctor_m3A44D85C9891C5A90A6368CA4D09E3489601E877 (void);
// 0x0000014C System.Collections.Generic.Dictionary`2<System.String,ResourceProfile> SystemDataFactoy::get_DictionaryConfiguirs()
extern void SystemDataFactoy_get_DictionaryConfiguirs_m4FF49213FF91CF4F401239161D8558B233216655 (void);
// 0x0000014D System.Void SystemDataFactoy::set_DictionaryConfiguirs(System.Collections.Generic.Dictionary`2<System.String,ResourceProfile>)
extern void SystemDataFactoy_set_DictionaryConfiguirs_mED3C4063D8E68B7C3B987524B99B50E4A7E4D6B3 (void);
// 0x0000014E System.Collections.IEnumerator SystemDataFactoy::Configur(SystmConfigurationManager)
extern void SystemDataFactoy_Configur_m5401629C61EE9A8E8A6DB5B54A7C1335D15BD3F4 (void);
// 0x0000014F System.Boolean SystemDataFactoy::Create()
extern void SystemDataFactoy_Create_m5A43EA6DD0713BB94A05EA0D380FDEB23801C1CC (void);
// 0x00000150 System.Collections.IEnumerator SystemDataFactoy::LoadConfigurs()
extern void SystemDataFactoy_LoadConfigurs_m51E7617588511C9E3D39FE78A70520AA4C35EB49 (void);
// 0x00000151 ResourceProfile SystemDataFactoy::GetResourceConfigur(System.String)
extern void SystemDataFactoy_GetResourceConfigur_m3BDAEFF051302F458A6DE060A988457C74105F72 (void);
// 0x00000152 System.Void SystemDataFactoy::LoadAssetBundle()
extern void SystemDataFactoy_LoadAssetBundle_m2FCD6432E2BAEFE6B1F3B1E8362541C5B3A92A98 (void);
// 0x00000153 UnityEngine.GameObject SystemDataFactoy::GetResource(System.String)
extern void SystemDataFactoy_GetResource_m4B4E71D947BAD62BC92EA6FDA37CA7E475B31A5B (void);
// 0x00000154 System.Void SystemDataFactoy::.ctor()
extern void SystemDataFactoy__ctor_mBAE5A426D420654433699B204CAB15F405942F28 (void);
// 0x00000155 System.Void SystemDataFactoy/<Configur>d__8::.ctor(System.Int32)
extern void U3CConfigurU3Ed__8__ctor_m38CCCAE906F3D6FB49B74D61BDA4D57F4ABC813E (void);
// 0x00000156 System.Void SystemDataFactoy/<Configur>d__8::System.IDisposable.Dispose()
extern void U3CConfigurU3Ed__8_System_IDisposable_Dispose_m37DAA65B41B5603BD5BDC9DE18121846F084EC7B (void);
// 0x00000157 System.Boolean SystemDataFactoy/<Configur>d__8::MoveNext()
extern void U3CConfigurU3Ed__8_MoveNext_m450C9CE0A5C3EDBC85933A8A3E23A4E17F9E63B4 (void);
// 0x00000158 System.Object SystemDataFactoy/<Configur>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CConfigurU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2987EC12DF72E7096AE31E42BCCB44E25643937B (void);
// 0x00000159 System.Void SystemDataFactoy/<Configur>d__8::System.Collections.IEnumerator.Reset()
extern void U3CConfigurU3Ed__8_System_Collections_IEnumerator_Reset_m7CD56B0FFD67A29A2519E91560D80320986A3482 (void);
// 0x0000015A System.Object SystemDataFactoy/<Configur>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CConfigurU3Ed__8_System_Collections_IEnumerator_get_Current_mAB2C8F18593C1DB338D13F1923D0DCB4A695FA56 (void);
// 0x0000015B System.Void SystemDataFactoy/<LoadConfigurs>d__10::.ctor(System.Int32)
extern void U3CLoadConfigursU3Ed__10__ctor_m0D57A0BEA29A3E666F2783FEE55E3E132BE452E6 (void);
// 0x0000015C System.Void SystemDataFactoy/<LoadConfigurs>d__10::System.IDisposable.Dispose()
extern void U3CLoadConfigursU3Ed__10_System_IDisposable_Dispose_m032ECB9A2A5D2FFC941308B698EC8A836BE1D983 (void);
// 0x0000015D System.Boolean SystemDataFactoy/<LoadConfigurs>d__10::MoveNext()
extern void U3CLoadConfigursU3Ed__10_MoveNext_m5F289B085DC0243D5B79010380B43B5D497F27C2 (void);
// 0x0000015E System.Object SystemDataFactoy/<LoadConfigurs>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadConfigursU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7B88339CC1EDBF6D06446A07F02269335401AE3C (void);
// 0x0000015F System.Void SystemDataFactoy/<LoadConfigurs>d__10::System.Collections.IEnumerator.Reset()
extern void U3CLoadConfigursU3Ed__10_System_Collections_IEnumerator_Reset_m6A23C2FB65642F1C4E43D43E91B874ACCF8FB305 (void);
// 0x00000160 System.Object SystemDataFactoy/<LoadConfigurs>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CLoadConfigursU3Ed__10_System_Collections_IEnumerator_get_Current_mD29614105EB118354D9060283FDFA11AE381130E (void);
// 0x00000161 System.Void SystemEventManager::ConFigur(SystemGameManager)
extern void SystemEventManager_ConFigur_m49461E5B9A478FECDD8EFF5582361C55B3CA0B67 (void);
// 0x00000162 System.Void SystemEventManager::StartListening(System.String,System.Action`2<System.String,EventParamProperties>)
extern void SystemEventManager_StartListening_m1E32F458A59BA1B6EB2E256B65366ABC0656C6F8 (void);
// 0x00000163 System.Void SystemEventManager::TriggerEvent(System.String,EventParamProperties)
extern void SystemEventManager_TriggerEvent_mE2DA3AFF6564F3F1A402F4651C5E5824A3234ABA (void);
// 0x00000164 System.Void SystemEventManager::StopListening(System.String,System.Action`2<System.String,EventParamProperties>)
extern void SystemEventManager_StopListening_m4736B4BB2DF246CD89F71A647AF91361D6AAE8E7 (void);
// 0x00000165 System.Void SystemEventManager::Clear()
extern void SystemEventManager_Clear_mE32E0E65DBA977075F9C3540A4491BE382956A4C (void);
// 0x00000166 System.Void SystemEventManager::.ctor()
extern void SystemEventManager__ctor_mCE0D000D52444F00BC192B9E4DBFE73F8098681D (void);
// 0x00000167 System.Void SystemEventManager::.cctor()
extern void SystemEventManager__cctor_m8DA21258B2E3B1195C32F731EAE120264186FB59 (void);
// 0x00000168 SystmConfigurationManager SystemGameManager::get_SystmConfigurationManager()
extern void SystemGameManager_get_SystmConfigurationManager_mC47CA842BAE52DC50C75B260B2ECD39A7B9870AA (void);
// 0x00000169 GameStat SystemGameManager::get_GameStat()
extern void SystemGameManager_get_GameStat_m89042CF99C4F0B9A7D1033B8D306D68EF602F0E3 (void);
// 0x0000016A System.Void SystemGameManager::set_GameStat(GameStat)
extern void SystemGameManager_set_GameStat_m4B147D14295E6F67A8ACDDA22C22CE32D4F60738 (void);
// 0x0000016B LevelManager SystemGameManager::get_LevelManager()
extern void SystemGameManager_get_LevelManager_mB1068C3EA73977B6ECC366AFDA961029D556C130 (void);
// 0x0000016C System.Void SystemGameManager::set_LevelManager(LevelManager)
extern void SystemGameManager_set_LevelManager_m5A5E2477EEA1A083E2C30F6BF4A9CB05867C5988 (void);
// 0x0000016D OperationManage SystemGameManager::get_OperationManage()
extern void SystemGameManager_get_OperationManage_m7E665DA3CE84912EF66AB9273038C8E58751672D (void);
// 0x0000016E GuestManager SystemGameManager::get_GuestManager()
extern void SystemGameManager_get_GuestManager_m9E79AAEF230120F2569565E15A295374D5B8FE5A (void);
// 0x0000016F GuestManager SystemGameManager::get_mGuestManager()
extern void SystemGameManager_get_mGuestManager_m586B96928C82E13CFEDBCF256D768DD5FEF9ADDC (void);
// 0x00000170 UiManager SystemGameManager::get_UiManager()
extern void SystemGameManager_get_UiManager_m076C0BBE25B122C7F97CC21318158E592614CCAE (void);
// 0x00000171 System.Void SystemGameManager::Start()
extern void SystemGameManager_Start_mE6276384BA173315A4BBFF6ACF9B7102CCF6A0B8 (void);
// 0x00000172 System.Void SystemGameManager::Init()
extern void SystemGameManager_Init_m5588BE6A59893A4436B72D49DD4EC3B321A998B2 (void);
// 0x00000173 System.Collections.IEnumerator SystemGameManager::LoadData()
extern void SystemGameManager_LoadData_mC922EAA6D5AE721742E8DD09DFC3E6E696C5B9FF (void);
// 0x00000174 System.Void SystemGameManager::ChangeLevel()
extern void SystemGameManager_ChangeLevel_m6D71D266B700878A61704EFC9B08BAF6AA99A0CB (void);
// 0x00000175 System.Collections.IEnumerator SystemGameManager::LoadingLevel()
extern void SystemGameManager_LoadingLevel_m403F426EDC866C182CB4A83CFDA741C49C92BFEC (void);
// 0x00000176 System.Void SystemGameManager::ContinueGame()
extern void SystemGameManager_ContinueGame_mF14B8471A21308687E73BB480EF84F7FE0B8AB45 (void);
// 0x00000177 System.Collections.IEnumerator SystemGameManager::ResetManger()
extern void SystemGameManager_ResetManger_mA0485083939DD8BAFC3C8EBDF2075CDCD641D334 (void);
// 0x00000178 System.Void SystemGameManager::SetGameRun()
extern void SystemGameManager_SetGameRun_m16C863B4BD2F660848E2FA9D1DFAE578BB9297CB (void);
// 0x00000179 System.Void SystemGameManager::GameEnd()
extern void SystemGameManager_GameEnd_m77EB26138C4B487748157C4CC8E9DA5D98CA8181 (void);
// 0x0000017A System.Void SystemGameManager::SaveCloundData()
extern void SystemGameManager_SaveCloundData_mB2B10C9204B03B871A95D755CA60AF5D911671B4 (void);
// 0x0000017B System.Void SystemGameManager::.ctor()
extern void SystemGameManager__ctor_mBAC18CD30C927FEA57C2793189AB448BE795B021 (void);
// 0x0000017C System.Void SystemGameManager/<LoadData>d__24::.ctor(System.Int32)
extern void U3CLoadDataU3Ed__24__ctor_m02AADB872B54F60465E6AE01FEE687660984C2AC (void);
// 0x0000017D System.Void SystemGameManager/<LoadData>d__24::System.IDisposable.Dispose()
extern void U3CLoadDataU3Ed__24_System_IDisposable_Dispose_m0275841DBC368AE0F3979D73507246B7FF95909A (void);
// 0x0000017E System.Boolean SystemGameManager/<LoadData>d__24::MoveNext()
extern void U3CLoadDataU3Ed__24_MoveNext_m8F1986F1A293A99DF4DEFEAAB730CE4068EB2F59 (void);
// 0x0000017F System.Object SystemGameManager/<LoadData>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadDataU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5122489B9203191F986A039F470B7D2534C869BE (void);
// 0x00000180 System.Void SystemGameManager/<LoadData>d__24::System.Collections.IEnumerator.Reset()
extern void U3CLoadDataU3Ed__24_System_Collections_IEnumerator_Reset_mDEA95A48CACC4E225236D00515D342E4EE02B057 (void);
// 0x00000181 System.Object SystemGameManager/<LoadData>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CLoadDataU3Ed__24_System_Collections_IEnumerator_get_Current_m1B478C1DF046ACC9003F19B5385804920A4A4592 (void);
// 0x00000182 System.Void SystemGameManager/<LoadingLevel>d__26::.ctor(System.Int32)
extern void U3CLoadingLevelU3Ed__26__ctor_mD8D4A8C530558282D7E6EFDD58F14439930C5FA4 (void);
// 0x00000183 System.Void SystemGameManager/<LoadingLevel>d__26::System.IDisposable.Dispose()
extern void U3CLoadingLevelU3Ed__26_System_IDisposable_Dispose_m6227C8D76A85C6534E78EF1EF195880CCA3D20F8 (void);
// 0x00000184 System.Boolean SystemGameManager/<LoadingLevel>d__26::MoveNext()
extern void U3CLoadingLevelU3Ed__26_MoveNext_m2AB868E4BB0FCC80EE468FC5343F6DB66F003B8E (void);
// 0x00000185 System.Object SystemGameManager/<LoadingLevel>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadingLevelU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFFCFA7D514691029125BE192CDB7FC37408EEB56 (void);
// 0x00000186 System.Void SystemGameManager/<LoadingLevel>d__26::System.Collections.IEnumerator.Reset()
extern void U3CLoadingLevelU3Ed__26_System_Collections_IEnumerator_Reset_m64F1414C1F3D7DA54C8CD51D79C2AD2D41B6680C (void);
// 0x00000187 System.Object SystemGameManager/<LoadingLevel>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CLoadingLevelU3Ed__26_System_Collections_IEnumerator_get_Current_mB355AC6D536105E08EC6DDC011417515C96E1989 (void);
// 0x00000188 System.Void SystemGameManager/<ResetManger>d__28::.ctor(System.Int32)
extern void U3CResetMangerU3Ed__28__ctor_m29A72BDF8E9BCEE28F95D9D25D33FEFAC3B3D042 (void);
// 0x00000189 System.Void SystemGameManager/<ResetManger>d__28::System.IDisposable.Dispose()
extern void U3CResetMangerU3Ed__28_System_IDisposable_Dispose_m4D120DE3C11918A203A5E33F5FFA5C4A68175004 (void);
// 0x0000018A System.Boolean SystemGameManager/<ResetManger>d__28::MoveNext()
extern void U3CResetMangerU3Ed__28_MoveNext_m78A70C4FD792A74C85275CE71070289334BBAA21 (void);
// 0x0000018B System.Object SystemGameManager/<ResetManger>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResetMangerU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m68D919578583A286FEE91418F5CC0C2AC1034EA9 (void);
// 0x0000018C System.Void SystemGameManager/<ResetManger>d__28::System.Collections.IEnumerator.Reset()
extern void U3CResetMangerU3Ed__28_System_Collections_IEnumerator_Reset_mC8133562628A3871A4206AA831CA347AFEDB4B1E (void);
// 0x0000018D System.Object SystemGameManager/<ResetManger>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CResetMangerU3Ed__28_System_Collections_IEnumerator_get_Current_m0F6E19620C39FA151FE0BC645B7F060E4900A752 (void);
// 0x0000018E System.Void SystemGameManager/<SaveCloundData>d__31::MoveNext()
extern void U3CSaveCloundDataU3Ed__31_MoveNext_m5CD46301F4D3D42310CBB8F6551B53F5D9DDFD6F (void);
// 0x0000018F System.Void SystemGameManager/<SaveCloundData>d__31::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSaveCloundDataU3Ed__31_SetStateMachine_m4CEC5F75738743AAFFE9AECB9893C38431C69113 (void);
// 0x00000190 System.Void SystmConfigurationManager::.ctor()
extern void SystmConfigurationManager__ctor_m2883D12F8778CC0E7D86EB3A8EAD51006E160EB9 (void);
// 0x00000191 System.Void UiManager::Configuer(SystemGameManager)
extern void UiManager_Configuer_mF7BF29AC3CB92D52E8F5B7078A051B5048D6A4BE (void);
// 0x00000192 System.Void UiManager::ClickStatrGame()
extern void UiManager_ClickStatrGame_m3020F30326E42E4B784579263B6683FDCC40F91E (void);
// 0x00000193 System.Void UiManager::CallUiRunGame()
extern void UiManager_CallUiRunGame_mD5C8FEAC5047C49A083503BAC68EF4B31CEA9667 (void);
// 0x00000194 System.Void UiManager::ChangeLevelEffice()
extern void UiManager_ChangeLevelEffice_m146DE0946B2BA534335778523D00FE78A90CEDF6 (void);
// 0x00000195 System.Void UiManager::LoadingPlanle()
extern void UiManager_LoadingPlanle_mA6F7F9856020FC8D111C5123414E4BB66B1865BA (void);
// 0x00000196 System.Void UiManager::LoadingContinue()
extern void UiManager_LoadingContinue_m33599E1334E4AB0B1AB27A807D62492B49D7DED8 (void);
// 0x00000197 System.Collections.IEnumerator UiManager::TargetOpen()
extern void UiManager_TargetOpen_m72DB0A7C49FE9945D9B52F0E598634BBF5506DEF (void);
// 0x00000198 System.Void UiManager::GuetNeedComelet()
extern void UiManager_GuetNeedComelet_m77013E97306C6F006ADF64B6F8208982B478B7D6 (void);
// 0x00000199 System.Void UiManager::.ctor()
extern void UiManager__ctor_mF1223D076D6716E82EAF760F5337E462084CD2F0 (void);
// 0x0000019A System.Void UiManager/<TargetOpen>d__17::.ctor(System.Int32)
extern void U3CTargetOpenU3Ed__17__ctor_mED0B1D3A961CE72313A26E2468EF5453D584D63D (void);
// 0x0000019B System.Void UiManager/<TargetOpen>d__17::System.IDisposable.Dispose()
extern void U3CTargetOpenU3Ed__17_System_IDisposable_Dispose_m845B89A44CBA3DE5BCB167330D2479D512334B4C (void);
// 0x0000019C System.Boolean UiManager/<TargetOpen>d__17::MoveNext()
extern void U3CTargetOpenU3Ed__17_MoveNext_m4D0EF85F8B1B369F88D93E2D180263436775E2BA (void);
// 0x0000019D System.Object UiManager/<TargetOpen>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTargetOpenU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m11C299473D12F7FF9F424571A34D9677BEC6528A (void);
// 0x0000019E System.Void UiManager/<TargetOpen>d__17::System.Collections.IEnumerator.Reset()
extern void U3CTargetOpenU3Ed__17_System_Collections_IEnumerator_Reset_mC544395FF7F68A1570F6E08575CA6E9F7183A531 (void);
// 0x0000019F System.Object UiManager/<TargetOpen>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CTargetOpenU3Ed__17_System_Collections_IEnumerator_get_Current_mF01FC11BA102021DF78210470156B34103ED149F (void);
// 0x000001A0 System.Void GuestNeedFoodData::.ctor()
extern void GuestNeedFoodData__ctor_m7D325A917721D2C1228BCA7D44325910A49F83B1 (void);
// 0x000001A1 System.Void GuestParentInfo::Start()
extern void GuestParentInfo_Start_m0DE6AA1DD59C4B1444EF7CDE316B6320754A4C03 (void);
// 0x000001A2 System.Boolean GuestParentInfo::GetHaveGuest(System.Int32)
extern void GuestParentInfo_GetHaveGuest_m91774CFC75B0D9BD412A59E390B762C17BB2B838 (void);
// 0x000001A3 System.Void GuestParentInfo::SetHaveGuest(System.Int32,System.Boolean)
extern void GuestParentInfo_SetHaveGuest_m0999A1646A5DF3DE50B70717E30AADBDE9591BAB (void);
// 0x000001A4 System.Void GuestParentInfo::ResetGuestParentInfo()
extern void GuestParentInfo_ResetGuestParentInfo_m1BE3C6186245CD730D16FA3237ED629C1EDD5DD1 (void);
// 0x000001A5 System.Void GuestParentInfo::.ctor()
extern void GuestParentInfo__ctor_m3BA78BF6B64E641BBCB62081257C56ADC6F17185 (void);
// 0x000001A6 System.Void LevelData::.ctor()
extern void LevelData__ctor_m923000759B849E6F472A21ECEDD12006D85EDEDD (void);
// 0x000001A7 System.Void LevelGuestData::.ctor()
extern void LevelGuestData__ctor_mFA81EBD5E1EF1325DF1AF750D5E961E404E3F75D (void);
// 0x000001A8 System.Void GuestTable::.ctor()
extern void GuestTable__ctor_m38A07898CF7A2293AAE9A7C438F0BFABA4CDA67D (void);
// 0x000001A9 System.Void NewBehaviourScript::Start()
extern void NewBehaviourScript_Start_mDEAA416D3A6676A79851DDF6B62D676E53372A87 (void);
// 0x000001AA System.Void NewBehaviourScript::Update()
extern void NewBehaviourScript_Update_m68CAC9D7524B9FA1DFCA3F99BA694CD73147209B (void);
// 0x000001AB System.Void NewBehaviourScript::.ctor()
extern void NewBehaviourScript__ctor_mD2E080DE77BCDB61B6D2EC8AD996FAE611B97F3C (void);
// 0x000001AC System.String[] PlateItem::get_OtherFoodName()
extern void PlateItem_get_OtherFoodName_mA6F28FF8B3073550E34C45AFF4E567FE84AC505D (void);
// 0x000001AD FoodProperty PlateItem::get_FoodItem()
extern void PlateItem_get_FoodItem_m40777DA2EF749D25E16E034799F17DD6DBDB1876 (void);
// 0x000001AE System.Void PlateItem::set_FoodItem(FoodProperty)
extern void PlateItem_set_FoodItem_m3AEBC34F9C96466BD5768F9C322BEA3BD876F2F3 (void);
// 0x000001AF System.Void PlateItem::set_MainFoodPrice(System.Single)
extern void PlateItem_set_MainFoodPrice_m3D7B4FF25832B03B5E3B93096B3DFEDF67EFB06A (void);
// 0x000001B0 System.Void PlateItem::Start()
extern void PlateItem_Start_mE05F47ED78D3C6313944CE83C176A988A4B9B1E2 (void);
// 0x000001B1 System.Void PlateItem::SetFoodPrice(System.Single)
extern void PlateItem_SetFoodPrice_m5D43AFF3DD75BACAA10E703969FC555AE2441CD9 (void);
// 0x000001B2 System.String PlateItem::GetPlateFoodID()
extern void PlateItem_GetPlateFoodID_m49A5AA657A7238BDE1AABDA38AD8F283EF49DAE3 (void);
// 0x000001B3 System.Single PlateItem::GetPlateFoddIDNumber()
extern void PlateItem_GetPlateFoddIDNumber_m2E65D6B782D91B0E01B9FE9D282FC2BEB2F6408E (void);
// 0x000001B4 System.Void PlateItem::ClearPlate()
extern void PlateItem_ClearPlate_m774B4E6E407A7B1E3C21FE9078C7DE49D1D227D4 (void);
// 0x000001B5 System.Boolean PlateItem::AddOtherFood(System.String,System.Int32)
extern void PlateItem_AddOtherFood_m4CCBA5C83CD1FD9224C2D12C09A6CC4CCF8870B2 (void);
// 0x000001B6 System.Void PlateItem::DeliverFood()
extern void PlateItem_DeliverFood_m0FA434F79143D8BA275ACBD7646BD131DB6943D7 (void);
// 0x000001B7 System.Void PlateItem::.ctor()
extern void PlateItem__ctor_mA42614FF360CF5824A83894C22991E6CE29BD72D (void);
// 0x000001B8 DrinkOpreat RestaurantInfo::get_DrinkOpreat()
extern void RestaurantInfo_get_DrinkOpreat_mA7BCF7E18EAB337A5E00246F17ABF0D0D03C6A3A (void);
// 0x000001B9 System.Void RestaurantInfo::Configuer(SystemGameManager)
extern void RestaurantInfo_Configuer_m35E0F6A03125C3A359A736FD789B0CC0378101C4 (void);
// 0x000001BA System.Void RestaurantInfo::.ctor()
extern void RestaurantInfo__ctor_m657BDB9E6BC5E35A5D4E4D5D02390E909439046B (void);
// 0x000001BB HomePlanel HomeMapUiManager::get_HomePlanel()
extern void HomeMapUiManager_get_HomePlanel_m0750B2B76C47AA5FDA9CBC99BAC1BD6772593D76 (void);
// 0x000001BC DamondPlane HomeMapUiManager::get_DamonPlane()
extern void HomeMapUiManager_get_DamonPlane_mFF43751CE8C4D49E570D36289650A15B58C79F4D (void);
// 0x000001BD HeadPlanel HomeMapUiManager::get_HeadPlanel()
extern void HomeMapUiManager_get_HeadPlanel_m44F6AC860FF688CAF4F028C1733B200AAA2C584A (void);
// 0x000001BE ShoppPlanel HomeMapUiManager::get_ShoppPlanel()
extern void HomeMapUiManager_get_ShoppPlanel_m332AF2D623D43B176CD3B3E6C120971AECFBC058 (void);
// 0x000001BF TaskPlanel HomeMapUiManager::get_TaskPlanel()
extern void HomeMapUiManager_get_TaskPlanel_mA27E4CB52BD0F9E8C5A597726431752DF86D2429 (void);
// 0x000001C0 DataPlanel HomeMapUiManager::get_DataPlanel()
extern void HomeMapUiManager_get_DataPlanel_mA7ABA6369996BEC80191FCC39CC737839EC0889C (void);
// 0x000001C1 UiMapPlane HomeMapUiManager::get_MapHome()
extern void HomeMapUiManager_get_MapHome_m1CB2A792AFEE02CBC95C1333D8BA530F78B2AB63 (void);
// 0x000001C2 ComparePlayerDataPlanel HomeMapUiManager::get_ComparePlayerDataPlanel()
extern void HomeMapUiManager_get_ComparePlayerDataPlanel_mF3E3CACA8F5248206F85ED4C064F64D888018DA3 (void);
// 0x000001C3 System.Void HomeMapUiManager::Configur(StartHomeManager)
extern void HomeMapUiManager_Configur_mBB120FBB4D65D7AB49595123100601FE279DE4BC (void);
// 0x000001C4 System.Void HomeMapUiManager::OpenLevelInfoPlane()
extern void HomeMapUiManager_OpenLevelInfoPlane_m6FEC86A7DF55612795CA96B3F437DB23C5F32D27 (void);
// 0x000001C5 System.Void HomeMapUiManager::CloseLevelInfoPlane()
extern void HomeMapUiManager_CloseLevelInfoPlane_mE668DAD4ADC2C6B550B4C032C0C7641AAFE7D217 (void);
// 0x000001C6 System.Void HomeMapUiManager::OpenLoackPlane()
extern void HomeMapUiManager_OpenLoackPlane_m795299FD88C8EEA255678090A113456C70B04704 (void);
// 0x000001C7 System.Void HomeMapUiManager::.ctor()
extern void HomeMapUiManager__ctor_mAAB28BF2CF1BDF372433452739F2EB90EBBB0497 (void);
// 0x000001C8 System.Void StartHomeManager::Awake()
extern void StartHomeManager_Awake_mF4BAB324091D29A81DB26D75490B02FBC475A0C2 (void);
// 0x000001C9 System.Void StartHomeManager::Start()
extern void StartHomeManager_Start_m0FFC27D888A8A667DAB08D991A7AF385434D5CB8 (void);
// 0x000001CA System.Collections.IEnumerator StartHomeManager::LoadConfigur()
extern void StartHomeManager_LoadConfigur_mA16AF5BBF073B100EF6CE7418B46AB584EDE4B1B (void);
// 0x000001CB System.Void StartHomeManager::Update()
extern void StartHomeManager_Update_m57051F4666E1728774C9036D7822471025F3332E (void);
// 0x000001CC System.Void StartHomeManager::.ctor()
extern void StartHomeManager__ctor_m8ABF4F2ED83A4AA1DD815B0918A3810A8012E135 (void);
// 0x000001CD System.Void StartHomeManager/<LoadConfigur>d__6::.ctor(System.Int32)
extern void U3CLoadConfigurU3Ed__6__ctor_m6B578AE1CB8435E7F81B9AD301D7AA2F06909A70 (void);
// 0x000001CE System.Void StartHomeManager/<LoadConfigur>d__6::System.IDisposable.Dispose()
extern void U3CLoadConfigurU3Ed__6_System_IDisposable_Dispose_mF7A940081B711D3DAFE59CEB06AD1483BF8C81FB (void);
// 0x000001CF System.Boolean StartHomeManager/<LoadConfigur>d__6::MoveNext()
extern void U3CLoadConfigurU3Ed__6_MoveNext_mB0B2E5E9ED2302427EA02F00BD9A15E310A2FD41 (void);
// 0x000001D0 System.Object StartHomeManager/<LoadConfigur>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadConfigurU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE660A9CA9D6786739A3A9A126D5FED1D6249760A (void);
// 0x000001D1 System.Void StartHomeManager/<LoadConfigur>d__6::System.Collections.IEnumerator.Reset()
extern void U3CLoadConfigurU3Ed__6_System_Collections_IEnumerator_Reset_m60E3A693D3CDD12BF3EAF39A330E993D6D7F7880 (void);
// 0x000001D2 System.Object StartHomeManager/<LoadConfigur>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CLoadConfigurU3Ed__6_System_Collections_IEnumerator_get_Current_mFFB428DEBC1380397859BB1ED740A450C71E4CBD (void);
// 0x000001D3 System.Void UiMapPlane::Init(HomeMapUiManager)
extern void UiMapPlane_Init_mB8385523C840783CA14BC2597172B16E9509B255 (void);
// 0x000001D4 System.Collections.IEnumerator UiMapPlane::InitData()
extern void UiMapPlane_InitData_mE57C7EB1FD2820C459CE2124BEF525EE2338FD2C (void);
// 0x000001D5 System.Void UiMapPlane::UpdataShowData()
extern void UiMapPlane_UpdataShowData_m18DCE3F61517BE776FD882E7D2A6409A048682B4 (void);
// 0x000001D6 System.Void UiMapPlane::ClickLevel(System.Int32)
extern void UiMapPlane_ClickLevel_m397C87CF748425BFBA83B821A56D3E472826F6F3 (void);
// 0x000001D7 System.Void UiMapPlane::ClickLock()
extern void UiMapPlane_ClickLock_mBCA13AF529F36B561D643D7A0FA4DC77076D2F71 (void);
// 0x000001D8 System.Void UiMapPlane::GuideClick()
extern void UiMapPlane_GuideClick_mD649FF28B2416F71500A3CC0F31649D9D1CCEA6D (void);
// 0x000001D9 System.Void UiMapPlane::CallCloseLevelInfoDlg()
extern void UiMapPlane_CallCloseLevelInfoDlg_m518F4F5752F2BABB11435599F641A7EFB1F62854 (void);
// 0x000001DA System.Void UiMapPlane::.ctor()
extern void UiMapPlane__ctor_m831F4D13538649DF1D420B4938D6FBE45AE13DEC (void);
// 0x000001DB System.Void UiMapPlane/<InitData>d__8::.ctor(System.Int32)
extern void U3CInitDataU3Ed__8__ctor_m4F4589C160781079654D48B05987E7ECCADD4089 (void);
// 0x000001DC System.Void UiMapPlane/<InitData>d__8::System.IDisposable.Dispose()
extern void U3CInitDataU3Ed__8_System_IDisposable_Dispose_m98B33BF7FB0B7080D036D3D1DDAC681E23154A8E (void);
// 0x000001DD System.Boolean UiMapPlane/<InitData>d__8::MoveNext()
extern void U3CInitDataU3Ed__8_MoveNext_m77752F32A951C03E78CA9DBE2A5B954E83680202 (void);
// 0x000001DE System.Object UiMapPlane/<InitData>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitDataU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m74C02834BF39C792446726C0B904EF78BFFF1D59 (void);
// 0x000001DF System.Void UiMapPlane/<InitData>d__8::System.Collections.IEnumerator.Reset()
extern void U3CInitDataU3Ed__8_System_Collections_IEnumerator_Reset_m178945C942CF77F638FA54C05FE9A2B458428E09 (void);
// 0x000001E0 System.Object UiMapPlane/<InitData>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CInitDataU3Ed__8_System_Collections_IEnumerator_get_Current_mB94218612EB5FF22597A0D35ED5CA062D8C15DC2 (void);
// 0x000001E1 System.Void ComparePlayerDataPlanel::Confgur(HomeMapUiManager)
extern void ComparePlayerDataPlanel_Confgur_m9E61BD3600F9433415D4A25B162714B31DC86FF9 (void);
// 0x000001E2 System.Void ComparePlayerDataPlanel::SetShowDaata(CloundSavePlayerDaata)
extern void ComparePlayerDataPlanel_SetShowDaata_m1290B7F6B571BA81306670D7EBFF2883E4EB0F88 (void);
// 0x000001E3 System.Void ComparePlayerDataPlanel::OnClose()
extern void ComparePlayerDataPlanel_OnClose_m183C409F33C410F36184984FFD1D0CF097908655 (void);
// 0x000001E4 System.Void ComparePlayerDataPlanel::OnClickLocadData()
extern void ComparePlayerDataPlanel_OnClickLocadData_m13227A5716C5FFB4CAB194C68ED782979F1EE89C (void);
// 0x000001E5 System.Void ComparePlayerDataPlanel::OnClickCloundData()
extern void ComparePlayerDataPlanel_OnClickCloundData_m46DF3ECFBDBC0192810D5260CEAD315B0D1B5453 (void);
// 0x000001E6 System.Void ComparePlayerDataPlanel::SaveCloundData()
extern void ComparePlayerDataPlanel_SaveCloundData_m456C7309F0A7275F30F20448CE61772C44A6D785 (void);
// 0x000001E7 System.Void ComparePlayerDataPlanel::.ctor()
extern void ComparePlayerDataPlanel__ctor_m48E172E133E168FEDAA0358160867B79AC4122DB (void);
// 0x000001E8 System.Void ComparePlayerDataPlanel/<SaveCloundData>d__9::MoveNext()
extern void U3CSaveCloundDataU3Ed__9_MoveNext_mACD985869A31304DC796854120D03455D811A674 (void);
// 0x000001E9 System.Void ComparePlayerDataPlanel/<SaveCloundData>d__9::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSaveCloundDataU3Ed__9_SetStateMachine_m06D2A0150AC5B1EEBA00854CEFA5A7B0CB4240DC (void);
// 0x000001EA System.Void DamondPlane::Confgur(HomeMapUiManager)
extern void DamondPlane_Confgur_m6E0044FC1D816145A036943156FE10DB0A731D29 (void);
// 0x000001EB System.Void DamondPlane::OnClickClose()
extern void DamondPlane_OnClickClose_m5A8B482A0F67F196544B98D84847E3181BCB1DFB (void);
// 0x000001EC System.Void DamondPlane::.ctor()
extern void DamondPlane__ctor_mF43248ED94782CA062CBC33F6C404D470BAE77E4 (void);
// 0x000001ED System.Void DataPlanel::Confgur(HomeMapUiManager)
extern void DataPlanel_Confgur_mA4201119D5B890BDBFD88EBB8C0B7F52EF74ACB8 (void);
// 0x000001EE System.Void DataPlanel::UpdateShowData()
extern void DataPlanel_UpdateShowData_mC7EE42CD6682184D00F4AF2382F96B0DA31C84E3 (void);
// 0x000001EF System.Void DataPlanel::.ctor()
extern void DataPlanel__ctor_mFC620E95F967E54730BF0203D249C635F4B426A1 (void);
// 0x000001F0 System.Void GameHomePlanel::Start()
extern void GameHomePlanel_Start_mF16E53D69797D33444746A83286837F307DA69A7 (void);
// 0x000001F1 System.Void GameHomePlanel::OpenUiWo()
extern void GameHomePlanel_OpenUiWo_m623CDF13352A1092D3A05D3E37BF5F06DFD17714 (void);
// 0x000001F2 System.Void GameHomePlanel::CloseUiWo()
extern void GameHomePlanel_CloseUiWo_m23D8B67F778AF91D91F507919DB02D981C9D3D27 (void);
// 0x000001F3 System.Void GameHomePlanel::SetTraget()
extern void GameHomePlanel_SetTraget_m45B4650F06FAE261EF1C903C82A5E67B3BA631E2 (void);
// 0x000001F4 System.Void GameHomePlanel::.ctor()
extern void GameHomePlanel__ctor_mF85290FF7F7DBA547B203AE122A9B9F91D5B8E72 (void);
// 0x000001F5 System.Void HeadItem::InteHeadItem(HeadPlanel,HeadAssetData)
extern void HeadItem_InteHeadItem_mE8D9B76C86B2D6AA17796B05C95964E136E3ACB2 (void);
// 0x000001F6 System.Void HeadItem::OnClick()
extern void HeadItem_OnClick_mE55DC2E32572600606E2AF8B28112C0E64CF3B2E (void);
// 0x000001F7 System.Void HeadItem::.ctor()
extern void HeadItem__ctor_m64B1F1094F0E8ED92C5E8EEA6632EA2D54433325 (void);
// 0x000001F8 System.Void HeadPlanel::OnClickClose()
extern void HeadPlanel_OnClickClose_m8D3ECD9E88FE1B6A3B2AE340C7BDC1042793D077 (void);
// 0x000001F9 HeadAssetData HeadPlanel::get_CurrHeadData()
extern void HeadPlanel_get_CurrHeadData_mC2748CC0D8D6AC00C9ED68D9108F36CD19D6EB4C (void);
// 0x000001FA System.Void HeadPlanel::set_CurrHeadData(HeadAssetData)
extern void HeadPlanel_set_CurrHeadData_m2B8AA255D097D31A482A270D598A66A1FF7BEB50 (void);
// 0x000001FB System.Void HeadPlanel::Confgur(HomeMapUiManager)
extern void HeadPlanel_Confgur_m723C5570D77B8CAE3E76D4ED4834E056ABE3F32D (void);
// 0x000001FC System.Void HeadPlanel::Start()
extern void HeadPlanel_Start_mD4528F1FC9A686418C6543FEAA30CA60ADEF0231 (void);
// 0x000001FD System.Void HeadPlanel::SpeapeHead()
extern void HeadPlanel_SpeapeHead_m0F0BED4A26780DAFB37AA7509BACC25A2592FE25 (void);
// 0x000001FE System.Void HeadPlanel::CallSelectHead(HeadAssetData)
extern void HeadPlanel_CallSelectHead_m0E433E5C43BFEC5CB8FD972DF585F09CE1AE217F (void);
// 0x000001FF System.Void HeadPlanel::OnClickSave()
extern void HeadPlanel_OnClickSave_mA5EE650B2B245A2BE562F31B228450F5144CB098 (void);
// 0x00000200 System.Void HeadPlanel::.ctor()
extern void HeadPlanel__ctor_mE10C8F72E6D7773E5DFA306029AF2A04B4B22EC3 (void);
// 0x00000201 System.Void HomePlanel::Confgur(HomeMapUiManager)
extern void HomePlanel_Confgur_mBAA429069ED4A5CC990D250B3A61B3E8C04AC7C0 (void);
// 0x00000202 System.Void HomePlanel::Init()
extern void HomePlanel_Init_m8B8CA35CDB15181FCF03E54CE0209F9611B6FBC2 (void);
// 0x00000203 System.Void HomePlanel::OnClickDanmondPlanel()
extern void HomePlanel_OnClickDanmondPlanel_m330C0AE2B7CD4360E64F74016119E537D1417DDB (void);
// 0x00000204 System.Void HomePlanel::OnClickHeadPlanel()
extern void HomePlanel_OnClickHeadPlanel_m14872D80E1BCECA85BBBDA45E8B9CD8AE7F1E015 (void);
// 0x00000205 System.Void HomePlanel::OnClickShoppPlanel()
extern void HomePlanel_OnClickShoppPlanel_m009150F205B37CE09B01954AB9403D4A00709B0B (void);
// 0x00000206 System.Void HomePlanel::OnClickTaskPlanel()
extern void HomePlanel_OnClickTaskPlanel_m466E58DB13A71A658BDFC1CB31E40DD89F0A76AC (void);
// 0x00000207 System.Void HomePlanel::SetHeadImage(HeadAssetData)
extern void HomePlanel_SetHeadImage_mC731464C4FFCBF9B7E7AE853B68B5E5559F84AA5 (void);
// 0x00000208 System.Void HomePlanel::.ctor()
extern void HomePlanel__ctor_mE723F4468513DB38738F426C31BD958469B3624A (void);
// 0x00000209 System.Void LevelInfoPlane::Init(HomeMapUiManager)
extern void LevelInfoPlane_Init_mE5D317CE4F5080028CE5CCC115E1A5AC8BAFF9CF (void);
// 0x0000020A System.Void LevelInfoPlane::IntDlg()
extern void LevelInfoPlane_IntDlg_mCB889BB23E0B0CB722BCD37CCC9D5F9C39B6E4F8 (void);
// 0x0000020B System.Void LevelInfoPlane::OpenUiWo()
extern void LevelInfoPlane_OpenUiWo_m8D275657E92D6991F60EBAE22F0F80220384A321 (void);
// 0x0000020C System.Void LevelInfoPlane::CloseUiWo()
extern void LevelInfoPlane_CloseUiWo_m99D6D7CB4D0AE6416AA3D7CF16CBE4C99102C84F (void);
// 0x0000020D System.Void LevelInfoPlane::OpenInit()
extern void LevelInfoPlane_OpenInit_m9919CC9C1C0917C067F4314F833F51B2A8213F5D (void);
// 0x0000020E System.Void LevelInfoPlane::ClickStartGame()
extern void LevelInfoPlane_ClickStartGame_m03FFD0305504CCCF66070FF14258A0477828CB20 (void);
// 0x0000020F System.Void LevelInfoPlane::ClickExist()
extern void LevelInfoPlane_ClickExist_m51E2C80F4B92F1748D53A160FBB66294DF56D6CC (void);
// 0x00000210 System.Void LevelInfoPlane::InitTipDlgOne(System.Int32)
extern void LevelInfoPlane_InitTipDlgOne_mACF91FE2A8F2CE6044C0BFE79088EF609A0AD253 (void);
// 0x00000211 System.Void LevelInfoPlane::OnClilkUpGread(System.Int32)
extern void LevelInfoPlane_OnClilkUpGread_m60688541F69DBCAF5DE8CF6B9606ED114534D547 (void);
// 0x00000212 System.Void LevelInfoPlane::SetShowLevelText(System.Int32)
extern void LevelInfoPlane_SetShowLevelText_m3FA6558891A3340E88FAD457B30CD258EA2459D5 (void);
// 0x00000213 System.Void LevelInfoPlane::.ctor()
extern void LevelInfoPlane__ctor_m58BC9F097EA795757FE834BE265986036900A6EF (void);
// 0x00000214 System.Int32 LevelSelect::get_SeLectLevel()
extern void LevelSelect_get_SeLectLevel_mA2A185047C3A0EB8C9DD698F7009DA58811DD7D3 (void);
// 0x00000215 System.Void LevelSelect::set_SeLectLevel(System.Int32)
extern void LevelSelect_set_SeLectLevel_m42E004163654537BE57EC9E211582CF7ADFC8136 (void);
// 0x00000216 LevelInfoPlane LevelSelect::get_LevelPlanel()
extern void LevelSelect_get_LevelPlanel_m8B993CBCC45CB2DA6CDCC75F71E0BF8E9FFE57CA (void);
// 0x00000217 System.Void LevelSelect::set_LevelPlanel(LevelInfoPlane)
extern void LevelSelect_set_LevelPlanel_m32F398E9D946DE09682F79A12D3ECDDB15A087B2 (void);
// 0x00000218 System.Void LevelSelect::OnCklic()
extern void LevelSelect_OnCklic_m39AE82A08DE01C9A21D57067FD16EFDA1ADF7DFA (void);
// 0x00000219 System.Void LevelSelect::.ctor()
extern void LevelSelect__ctor_m0D5B63D5D849E01B4536B330A09209A7DE744D4F (void);
// 0x0000021A System.Void LoadinPlanle::Start()
extern void LoadinPlanle_Start_mE3F5AA67E1E540A8C93E8B0EC73CE9CCDC50702C (void);
// 0x0000021B System.Void LoadinPlanle::OnEnable()
extern void LoadinPlanle_OnEnable_m6C32616F1A602C600867A7201A8B320EC1B19ED9 (void);
// 0x0000021C System.Void LoadinPlanle::OnDisable()
extern void LoadinPlanle_OnDisable_mBBA51CCB69E4420F81B92F3A2335D47328E70DDF (void);
// 0x0000021D System.Void LoadinPlanle::OpenUiWo()
extern void LoadinPlanle_OpenUiWo_m273B7E60778F6A750FA81B457EC26BDCDFA9E4D5 (void);
// 0x0000021E System.Void LoadinPlanle::CloseUiWo()
extern void LoadinPlanle_CloseUiWo_m1590049A6EFD9A44814BAF3574DC9D0B2B514525 (void);
// 0x0000021F System.Void LoadinPlanle::Update()
extern void LoadinPlanle_Update_m62E1AF1DD77D86DE8266A31F5ABFA34FDF120425 (void);
// 0x00000220 System.Void LoadinPlanle::.ctor()
extern void LoadinPlanle__ctor_mAC765036BE7D0A875EA48096340B6B3757A747C2 (void);
// 0x00000221 System.Void lockPlanel::ClickExist()
extern void lockPlanel_ClickExist_m178557665F7775CD5D97417A5EB52063C5E75F50 (void);
// 0x00000222 System.Void lockPlanel::.ctor()
extern void lockPlanel__ctor_m63CF43E8DF22400A9E4A113248428CC04D1FC386 (void);
// 0x00000223 System.Void OverPlanel::UiConfigur(UiManager)
extern void OverPlanel_UiConfigur_mEB971B3A22CB62EB1DFFC0201858A6211320D8B5 (void);
// 0x00000224 System.Void OverPlanel::OpenUiWo()
extern void OverPlanel_OpenUiWo_mC75E528DEBA36CE98CFEF57EE31D8D5A84FA19B5 (void);
// 0x00000225 System.Collections.IEnumerator OverPlanel::CionScore()
extern void OverPlanel_CionScore_mC43A7058B64A75452B4B7D8A27B86F1756CD98C7 (void);
// 0x00000226 System.Void OverPlanel::CloseUiWo()
extern void OverPlanel_CloseUiWo_m1B1C17F17F5E85EF82A4124040386BC5E235ECBD (void);
// 0x00000227 System.Void OverPlanel::OnClickContinue()
extern void OverPlanel_OnClickContinue_mFEC5779EF998788DB66645E607B3EF3D8B508B80 (void);
// 0x00000228 System.Void OverPlanel::.ctor()
extern void OverPlanel__ctor_mCA387785AA5FCB06BCFDB419D5760E3D1F224353 (void);
// 0x00000229 System.Void OverPlanel/<CionScore>d__4::.ctor(System.Int32)
extern void U3CCionScoreU3Ed__4__ctor_m80379E91AF4A1F7C9DD02BF943464652219FDFE2 (void);
// 0x0000022A System.Void OverPlanel/<CionScore>d__4::System.IDisposable.Dispose()
extern void U3CCionScoreU3Ed__4_System_IDisposable_Dispose_mACA7CF4D22831DD7486A058947FF13E53C7D3F24 (void);
// 0x0000022B System.Boolean OverPlanel/<CionScore>d__4::MoveNext()
extern void U3CCionScoreU3Ed__4_MoveNext_m237BB619023BA47D7340EA92C1477325C3ED5389 (void);
// 0x0000022C System.Object OverPlanel/<CionScore>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCionScoreU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD904D52B0C1B44DD5E37BD306820C5ADD7E5187 (void);
// 0x0000022D System.Void OverPlanel/<CionScore>d__4::System.Collections.IEnumerator.Reset()
extern void U3CCionScoreU3Ed__4_System_Collections_IEnumerator_Reset_mF7513A9877E648A3600BF8A9D13226C1D2462EA2 (void);
// 0x0000022E System.Object OverPlanel/<CionScore>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CCionScoreU3Ed__4_System_Collections_IEnumerator_get_Current_mB51AD0111929402688106ED3799322FC60A77962 (void);
// 0x0000022F System.Void ShoppPlanel::InitWo()
extern void ShoppPlanel_InitWo_mAB5C9F16E68C3F5EBA1990B066DF2A14EAB86DBD (void);
// 0x00000230 System.Void ShoppPlanel::OnClickShowBut(System.Int32)
extern void ShoppPlanel_OnClickShowBut_mD3B2B6682970B9EAF6DF70995E05FCE5EFB8D35D (void);
// 0x00000231 System.Void ShoppPlanel::InteShop()
extern void ShoppPlanel_InteShop_m5F2BB04777C40BE52B5B0EB236536FD4BB829993 (void);
// 0x00000232 System.Void ShoppPlanel::OpenUiWo()
extern void ShoppPlanel_OpenUiWo_mF1BAC82D48C1A2EEBF230783EA2889880F071374 (void);
// 0x00000233 System.Void ShoppPlanel::OnClickClose()
extern void ShoppPlanel_OnClickClose_mB5B4F5B9D00A9B7D8AC65896BE8C59D407264BA1 (void);
// 0x00000234 System.Void ShoppPlanel::OpenEnd()
extern void ShoppPlanel_OpenEnd_m4483104F0683B7486C1A055144003E8310763307 (void);
// 0x00000235 System.Void ShoppPlanel::CreateShopItem()
extern void ShoppPlanel_CreateShopItem_mE7C889B7379EF9B493C2A7B52FF08701E644E217 (void);
// 0x00000236 System.Void ShoppPlanel::RestItemProp()
extern void ShoppPlanel_RestItemProp_mEE4141AFE96F3E051EF1500E41DA132662616C8B (void);
// 0x00000237 System.Collections.IEnumerator ShoppPlanel::ShowItemProp()
extern void ShoppPlanel_ShowItemProp_m9597457A674D7A81C7368067E9A7FE75B47DD92F (void);
// 0x00000238 System.Void ShoppPlanel::RestGemItem()
extern void ShoppPlanel_RestGemItem_m151EBBA9310B0DDC19598CEAEA79CD60B5641760 (void);
// 0x00000239 System.Collections.IEnumerator ShoppPlanel::ShowItemGem()
extern void ShoppPlanel_ShowItemGem_m2081330CBB337A386F76CDE366C780C74B2B9552 (void);
// 0x0000023A System.Void ShoppPlanel::.ctor()
extern void ShoppPlanel__ctor_mDBF79CA44B1B047189683245466C172D62B917D1 (void);
// 0x0000023B System.Void ShoppPlanel/<ShowItemProp>d__15::.ctor(System.Int32)
extern void U3CShowItemPropU3Ed__15__ctor_mD7DCCE94780DB0BB6A5B25352D17721F436505AC (void);
// 0x0000023C System.Void ShoppPlanel/<ShowItemProp>d__15::System.IDisposable.Dispose()
extern void U3CShowItemPropU3Ed__15_System_IDisposable_Dispose_m1FACC6ECF1DC29083D20B21BC79511B825296BC0 (void);
// 0x0000023D System.Boolean ShoppPlanel/<ShowItemProp>d__15::MoveNext()
extern void U3CShowItemPropU3Ed__15_MoveNext_mB10CB3D693F8A43204317CADC940F3548201F411 (void);
// 0x0000023E System.Object ShoppPlanel/<ShowItemProp>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowItemPropU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC2EC1B7C197E146A0638F96DF752605E4132C37 (void);
// 0x0000023F System.Void ShoppPlanel/<ShowItemProp>d__15::System.Collections.IEnumerator.Reset()
extern void U3CShowItemPropU3Ed__15_System_Collections_IEnumerator_Reset_m7F8A09787537263A9A48C9ADDEDC5E192ABCAA94 (void);
// 0x00000240 System.Object ShoppPlanel/<ShowItemProp>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CShowItemPropU3Ed__15_System_Collections_IEnumerator_get_Current_m0167222C7C6FF0DE5C400F7CEEF968677F0A2F3D (void);
// 0x00000241 System.Void ShoppPlanel/<ShowItemGem>d__17::.ctor(System.Int32)
extern void U3CShowItemGemU3Ed__17__ctor_m426C867B155BCA30C02F9BAFC6B76164657D28B9 (void);
// 0x00000242 System.Void ShoppPlanel/<ShowItemGem>d__17::System.IDisposable.Dispose()
extern void U3CShowItemGemU3Ed__17_System_IDisposable_Dispose_mF92EAC0E0F3E7FAFBA216CC8E0ABF44A892F0943 (void);
// 0x00000243 System.Boolean ShoppPlanel/<ShowItemGem>d__17::MoveNext()
extern void U3CShowItemGemU3Ed__17_MoveNext_m583A0806C428067B786C408AE6475DBF1414CE5C (void);
// 0x00000244 System.Object ShoppPlanel/<ShowItemGem>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowItemGemU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC5F55885EF4674C0B057225F61D7EEC67EEB4507 (void);
// 0x00000245 System.Void ShoppPlanel/<ShowItemGem>d__17::System.Collections.IEnumerator.Reset()
extern void U3CShowItemGemU3Ed__17_System_Collections_IEnumerator_Reset_m137A415D3C1012DD01B47D70F8AEA763E29B5EAD (void);
// 0x00000246 System.Object ShoppPlanel/<ShowItemGem>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CShowItemGemU3Ed__17_System_Collections_IEnumerator_get_Current_m81AF1EA687AB5CB6AF8349AD18270FAE52D4DB9D (void);
// 0x00000247 System.Void TargetPlanle::OpenUiWo()
extern void TargetPlanle_OpenUiWo_m2D953FC97557AA6C08F24000709E6A9EFB8D0CEC (void);
// 0x00000248 System.Collections.IEnumerator TargetPlanle::CloseTarget()
extern void TargetPlanle_CloseTarget_mB5E276228CEE597029BCE6FF70E53B34AD6E332D (void);
// 0x00000249 System.Void TargetPlanle::CloseUiWo()
extern void TargetPlanle_CloseUiWo_m25B260D268DED319814CBED09AC6A1700EBEE021 (void);
// 0x0000024A System.Void TargetPlanle::OnEnable()
extern void TargetPlanle_OnEnable_m7A418B817C1907234007812CB00802971D79D2D1 (void);
// 0x0000024B System.Void TargetPlanle::OnDisable()
extern void TargetPlanle_OnDisable_mDDF3E498711FD1B139D38BDABDDA051D66A7E022 (void);
// 0x0000024C System.Void TargetPlanle::SetTragetValue(System.Single)
extern void TargetPlanle_SetTragetValue_m98794229AD586B79D0884321623139AA121DDD5D (void);
// 0x0000024D System.Void TargetPlanle::.ctor()
extern void TargetPlanle__ctor_mBDAEFDAE1F3EF58318C47747DD75F29D929DF7A2 (void);
// 0x0000024E System.Void TargetPlanle::<OpenUiWo>b__3_0()
extern void TargetPlanle_U3COpenUiWoU3Eb__3_0_mD735279E1ADCB34D62665343502201AE5A631748 (void);
// 0x0000024F System.Void TargetPlanle::<CloseTarget>b__4_0()
extern void TargetPlanle_U3CCloseTargetU3Eb__4_0_m2C133333A1AEA778813D5547B3D57B8E03F47E57 (void);
// 0x00000250 System.Void TargetPlanle/<CloseTarget>d__4::.ctor(System.Int32)
extern void U3CCloseTargetU3Ed__4__ctor_mF80142F891816CD667601530C206979866D49789 (void);
// 0x00000251 System.Void TargetPlanle/<CloseTarget>d__4::System.IDisposable.Dispose()
extern void U3CCloseTargetU3Ed__4_System_IDisposable_Dispose_mE3FBD3C178D7045B5F0CB50B09A60D6D3868B6BD (void);
// 0x00000252 System.Boolean TargetPlanle/<CloseTarget>d__4::MoveNext()
extern void U3CCloseTargetU3Ed__4_MoveNext_mFD061711DD38BCC8D1F4672A0629A4425560F914 (void);
// 0x00000253 System.Object TargetPlanle/<CloseTarget>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCloseTargetU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BAF61E67BE70012731868F9795D8B9A0E33DA00 (void);
// 0x00000254 System.Void TargetPlanle/<CloseTarget>d__4::System.Collections.IEnumerator.Reset()
extern void U3CCloseTargetU3Ed__4_System_Collections_IEnumerator_Reset_mE448CD797AB70D0EF76047A43109947A52D998C8 (void);
// 0x00000255 System.Object TargetPlanle/<CloseTarget>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CCloseTargetU3Ed__4_System_Collections_IEnumerator_get_Current_m0502E9D81E981F4CC5CA22CBBA75110B34B0DD18 (void);
// 0x00000256 System.Void TaskItem::.ctor()
extern void TaskItem__ctor_m56CDBC9FF5F2FECD1D9F05238639BAE0F70D999D (void);
// 0x00000257 System.Void TaskPlanel::InitWo()
extern void TaskPlanel_InitWo_m186951AF56B975DB392D7107066CF299C43B79AB (void);
// 0x00000258 System.Void TaskPlanel::OnClickClose()
extern void TaskPlanel_OnClickClose_m917FB168C8F5E42E17FC026183458610BD0CE218 (void);
// 0x00000259 System.Collections.IEnumerator TaskPlanel::CreaAchieveTaks()
extern void TaskPlanel_CreaAchieveTaks_m7D20628DE8CE750379B828A158100DB19F83EA8F (void);
// 0x0000025A System.Void TaskPlanel::OnClickBtn(System.Int32)
extern void TaskPlanel_OnClickBtn_mAC133A0302D8168ED42074471BC9E90D2D20CDB8 (void);
// 0x0000025B System.Void TaskPlanel::ResetTask()
extern void TaskPlanel_ResetTask_m4BC95B96E24733C69F8E8FB0A238AE58537E0101 (void);
// 0x0000025C System.Void TaskPlanel::.ctor()
extern void TaskPlanel__ctor_m986B5603FEE7E3ECF9672412E1B1DA7BBDED3A9C (void);
// 0x0000025D System.Void TaskPlanel/<CreaAchieveTaks>d__6::.ctor(System.Int32)
extern void U3CCreaAchieveTaksU3Ed__6__ctor_m2E48CA8D75C667CCD99565680623CA11964E0734 (void);
// 0x0000025E System.Void TaskPlanel/<CreaAchieveTaks>d__6::System.IDisposable.Dispose()
extern void U3CCreaAchieveTaksU3Ed__6_System_IDisposable_Dispose_mEF03D66F151B3978DC7120463198BD66DDA87091 (void);
// 0x0000025F System.Boolean TaskPlanel/<CreaAchieveTaks>d__6::MoveNext()
extern void U3CCreaAchieveTaksU3Ed__6_MoveNext_mDE62DF17EC939BD334BADBFB03F0458578C5D5D1 (void);
// 0x00000260 System.Object TaskPlanel/<CreaAchieveTaks>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreaAchieveTaksU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m47F8A9CDD5AAB320EC6F4717D86DACAF215B28E9 (void);
// 0x00000261 System.Void TaskPlanel/<CreaAchieveTaks>d__6::System.Collections.IEnumerator.Reset()
extern void U3CCreaAchieveTaksU3Ed__6_System_Collections_IEnumerator_Reset_m32B360F2FD948ACA57B23CF106B014E7FA81FAD7 (void);
// 0x00000262 System.Object TaskPlanel/<CreaAchieveTaks>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CCreaAchieveTaksU3Ed__6_System_Collections_IEnumerator_get_Current_m5F89F63697A111978448F91ED5B46E47FF20AE52 (void);
// 0x00000263 System.Void UiLoadIngSlider::.ctor()
extern void UiLoadIngSlider__ctor_m7DBB5BA9AAF7D84B15E03DB5783FDBAF2304AD44 (void);
// 0x00000264 UnityEngine.CanvasGroup UiWoPropAbility::get_TargetGroup()
extern void UiWoPropAbility_get_TargetGroup_m2CFF9DC8FE3A8834A8B4E64EE34F438733ED6683 (void);
// 0x00000265 System.Void UiWoPropAbility::set_TargetGroup(UnityEngine.CanvasGroup)
extern void UiWoPropAbility_set_TargetGroup_mC0C6B9C0FBB9BECCEF1BB43B727DF42A5796470E (void);
// 0x00000266 System.Single UiWoPropAbility::get_ChangeTime()
extern void UiWoPropAbility_get_ChangeTime_m32CC18DF71D53B4D86BA8CA371EDCF68DE34196D (void);
// 0x00000267 System.Void UiWoPropAbility::set_ChangeTime(System.Single)
extern void UiWoPropAbility_set_ChangeTime_m3646EFB665CAEFB8A2DC3AE4E20322B1D879C04B (void);
// 0x00000268 SystemGameManager UiWoPropAbility::get_SystemGameManager()
extern void UiWoPropAbility_get_SystemGameManager_m5B947ABA36EEBEE9930007A80CDC82F4E9B4CBCE (void);
// 0x00000269 System.Void UiWoPropAbility::set_SystemGameManager(SystemGameManager)
extern void UiWoPropAbility_set_SystemGameManager_m1FA14FFA50972355FDF9935A6DB533D5D66EEAEF (void);
// 0x0000026A System.Void UiWoPropAbility::Configur(SystemGameManager)
extern void UiWoPropAbility_Configur_mD2A1263D8AA35EBDA50BE2E81F965CC71491461B (void);
// 0x0000026B System.Void UiWoPropAbility::Start()
extern void UiWoPropAbility_Start_m452A76E7AA8125956FB2F4609C807BD3BF142424 (void);
// 0x0000026C System.Void UiWoPropAbility::OpenUiWo()
extern void UiWoPropAbility_OpenUiWo_m44E48CAE27D852D72C0621858314F749F14526BB (void);
// 0x0000026D System.Void UiWoPropAbility::CloseUiWo()
extern void UiWoPropAbility_CloseUiWo_m0F18DA106F9973577EC0327FC77496FF03F5A72F (void);
// 0x0000026E System.Void UiWoPropAbility::SetUiEffect(System.Boolean)
extern void UiWoPropAbility_SetUiEffect_m59B53448F3412F0942B98B214F5294F583FD14DC (void);
// 0x0000026F System.Void UiWoPropAbility::InitWo()
extern void UiWoPropAbility_InitWo_m1828FC238DC42F435B42E65112CB9A6A39C320D3 (void);
// 0x00000270 System.Void UiWoPropAbility::OpenEnd()
extern void UiWoPropAbility_OpenEnd_mE3FA9799500DD7E52F9C63FB6E22CECC331A343B (void);
// 0x00000271 System.Void UiWoPropAbility::.ctor()
extern void UiWoPropAbility__ctor_m19DAA4ECB7C46DB867AB99C53B205B73409EC6FB (void);
// 0x00000272 System.Void UiWoPropAbility::<SetUiEffect>b__17_0()
extern void UiWoPropAbility_U3CSetUiEffectU3Eb__17_0_mCF74BD82B3F35F7EDB222ED0B54A3B52E7505EE8 (void);
// 0x00000273 System.Void UiWoPropAbility::<SetUiEffect>b__17_1()
extern void UiWoPropAbility_U3CSetUiEffectU3Eb__17_1_mF5217FFE8B514D61FF7C236E0E6342CAA0810D43 (void);
// 0x00000274 System.Void UIText::Start()
extern void UIText_Start_m938B8C6E726152E3D5F8AFE68198AAB2D33E4B3D (void);
// 0x00000275 System.Void UIText::clickOpen()
extern void UIText_clickOpen_mA281DFD1E42C87EA6E1635629BC285C1E989C7A1 (void);
// 0x00000276 System.Void UIText::.ctor()
extern void UIText__ctor_mD8921B9D29FA52E93E49473F0E232C21223B5667 (void);
// 0x00000277 System.Void BuildinFileManifest::.ctor()
extern void BuildinFileManifest__ctor_mBF7538337B56D8286696B71A381EAA4F71D2C37F (void);
// 0x00000278 System.Void StreamingAssetsDefine::.ctor()
extern void StreamingAssetsDefine__ctor_m24F6603DC5D25F321EA1E8E0CE58118D7513F764 (void);
// 0x00000279 System.Boolean GameQueryServices::QueryStreamingAssets(System.String,System.String)
extern void GameQueryServices_QueryStreamingAssets_m9A5CACD2BCA2B44B6AA7BD134D672339C1B4FC12 (void);
// 0x0000027A System.Void GameQueryServices::.ctor()
extern void GameQueryServices__ctor_m075C10A435857E061FE78F8BD293A6CF55EC105C (void);
// 0x0000027B System.Void StreamingAssetsHelper::Init()
extern void StreamingAssetsHelper_Init_m5FE865BDAEBFFA2604FFFAA940A1A637BC3FDD23 (void);
// 0x0000027C System.Boolean StreamingAssetsHelper::FileExists(System.String,System.String)
extern void StreamingAssetsHelper_FileExists_m5580EDFD4FA0EAA5D08E6B3486B5BF8136860B57 (void);
// 0x0000027D System.Void StreamingAssetsHelper::.ctor()
extern void StreamingAssetsHelper__ctor_m48EF042D675EBAA22A67CA435AA84396E03D8282 (void);
// 0x0000027E System.Void StreamingAssetsHelper::.cctor()
extern void StreamingAssetsHelper__cctor_mBEFB5C410D84FF5E45D9C429A5C86803771BD080 (void);
// 0x0000027F YooResoursceManagur YooResoursceManagur::get_Instace()
extern void YooResoursceManagur_get_Instace_mF273E0E72BD4555D79D941F284AD4120F5BD083A (void);
// 0x00000280 System.Void YooResoursceManagur::InitYoo(StartHomeManager)
extern void YooResoursceManagur_InitYoo_mFA78C2CCF4910B0C21A83EFD1EF8A2BD1275FDD9 (void);
// 0x00000281 System.Collections.IEnumerator YooResoursceManagur::InintCuble()
extern void YooResoursceManagur_InintCuble_m50675D154C18CC361B127C4541E015DEEDDEAED5 (void);
// 0x00000282 System.Void YooResoursceManagur::.ctor()
extern void YooResoursceManagur__ctor_mF5BE416EDB5E508149540A53DC4168A9F1DC3384 (void);
// 0x00000283 System.Void YooResoursceManagur/<InintCuble>d__6::.ctor(System.Int32)
extern void U3CInintCubleU3Ed__6__ctor_m8B92F5BD83B46A28A147FC2BDE1B3E5095884628 (void);
// 0x00000284 System.Void YooResoursceManagur/<InintCuble>d__6::System.IDisposable.Dispose()
extern void U3CInintCubleU3Ed__6_System_IDisposable_Dispose_m7948C9CA8843B5EE76713C0903D64F769445F17D (void);
// 0x00000285 System.Boolean YooResoursceManagur/<InintCuble>d__6::MoveNext()
extern void U3CInintCubleU3Ed__6_MoveNext_mC91222FAA0F8AAEEF9ADFA8BC77BD9A09FF5FC43 (void);
// 0x00000286 System.Object YooResoursceManagur/<InintCuble>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInintCubleU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB723C0B18AB92A430D1494C005E23D1EF68BE8BD (void);
// 0x00000287 System.Void YooResoursceManagur/<InintCuble>d__6::System.Collections.IEnumerator.Reset()
extern void U3CInintCubleU3Ed__6_System_Collections_IEnumerator_Reset_mE1F5F072B88223134146CAC582CFFD746A8F0267 (void);
// 0x00000288 System.Object YooResoursceManagur/<InintCuble>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CInintCubleU3Ed__6_System_Collections_IEnumerator_get_Current_m32B7F3F71FECA267377BB8BB803DF4E28AF82372 (void);
// 0x00000289 System.Void RemoteServices::.ctor(System.String,System.String)
extern void RemoteServices__ctor_m9C9D2E5D7B003C687EF7B42526DC595B9E5A27A5 (void);
// 0x0000028A System.String RemoteServices::YooAsset.IRemoteServices.GetRemoteMainURL(System.String)
extern void RemoteServices_YooAsset_IRemoteServices_GetRemoteMainURL_m80A98B59950800895374F5940DA72C4F3881FAEC (void);
// 0x0000028B System.String RemoteServices::YooAsset.IRemoteServices.GetRemoteFallbackURL(System.String)
extern void RemoteServices_YooAsset_IRemoteServices_GetRemoteFallbackURL_m956EE47AEE6B58E35FC33EB3B9834887AFA39588 (void);
static Il2CppMethodPointer s_methodPointers[651] = 
{
	CloudFunction_Init_mD21C18614CBCBBAA720953C47C4158EAF8A42586,
	CloudFunction_CloundSave_m40031A6D61020EBD0ED810159CCC1B04410779CB,
	CloudFunction_CallCloundFunction_mBCAC23D670258D0E63F2A57AECFA750883B3F1C1,
	CloudFunction__ctor_m1B01BDBA24B845059113F1964BCA848DEF104C8E,
	U3CU3Ec__cctor_m9F637F56559CE29C2DB0CCB3557965AE73B77290,
	U3CU3Ec__ctor_mD2336D1090E2DCDCF60002800F87CDD36F0791BA,
	U3CU3Ec_U3CCallCloundFunctionU3Eb__3_0_m0D4CD09E26921C34D5E2CB3F4AF54CBA64D8F80A,
	U3CU3Ec_U3CCallCloundFunctionU3Eb__3_1_m56DC8BE8E81C80062002D03B6B82E23AFABEEB67,
	U3CU3Ec_U3CCallCloundFunctionU3Eb__3_2_m2BB384AB7724178CC31A241180D9F4989A90217F,
	CloundSavePlayerDaata__ctor_mE560C359DD486E8E5FCDC99AF6951460A74A15F7,
	CloudSaveSystem_Awake_m8E28FC73A68ADB384F88679FE8C421DC4D4EC940,
	CloudSaveSystem_Start_m134AFFF68B01C20893EDE764551B956435B6995C,
	CloudSaveSystem_ListAllKeys_m967DDD542D57D1C2B65799DAF1C3F771943322DD,
	CloudSaveSystem_ForceSaveSingleData_m5FC2776F9485FC6BF1C5FBF1F176363E00A54BF3,
	NULL,
	CloudSaveSystem__ctor_m3677611609401D74983F5FAC0F6793F2D83E2057,
	U3CAwakeU3Ed__1_MoveNext_m2073F4CF637E0510E977A773753398EC37A950AB,
	U3CAwakeU3Ed__1_SetStateMachine_mEA3B34C59FBB01ACB78902CEFB0AD8C46349BF60,
	U3CListAllKeysU3Ed__3_MoveNext_mF06AF0AE486327A68DADF1C848857A23656D0C64,
	U3CListAllKeysU3Ed__3_SetStateMachine_m235973254ED93FF7B9B529DC584277B39342A146,
	U3CForceSaveSingleDataU3Ed__4_MoveNext_mA214175827119FE72D7D121464A09F4E6107DBC8,
	U3CForceSaveSingleDataU3Ed__4_SetStateMachine_m9CACD98A7A027DCAD32BD77D301143845CB04033,
	NULL,
	NULL,
	FireEffect_Start_m3967721274CBB6830BFD981B3F15FC4787A3BB6B,
	FireEffect_CreateFireEffect_m90B44CA491715A06CAC2A6029C3054850BF26539,
	FireEffect_BehindEffcet_mD4702C1DC033B0E6A39E331C0ABCB999BA7A39EC,
	FireEffect_ClickContuine_mC23FC3BC3EF8287D217E7ED65EAFB4662C00B545,
	FireEffect__ctor_m3BADA4F79A8A2E8F1D4DA055E6F6B932C7D80670,
	FireEffect_U3CBehindEffcetU3Eb__13_0_mD99FA4FE93434493F0DFC2D1535A74EC274E510C,
	U3CCreateFireEffectU3Ed__12__ctor_m7A9C136207A996639D2B5006E439740FDD322B8A,
	U3CCreateFireEffectU3Ed__12_System_IDisposable_Dispose_mD2C057419285C41C0B1CB4F7726A818713AFC014,
	U3CCreateFireEffectU3Ed__12_MoveNext_mD4921A758E77F20970DE391581D9E3E22CEB6F12,
	U3CCreateFireEffectU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5909438A77C952A795E7429DEEAB85D45C4354D6,
	U3CCreateFireEffectU3Ed__12_System_Collections_IEnumerator_Reset_m3AFFAB201913F4DBE4887497924BC3EE9628A8C6,
	U3CCreateFireEffectU3Ed__12_System_Collections_IEnumerator_get_Current_m50DFE4897336348174F0EC30DB06BA246ADC6350,
	U3CBehindEffcetU3Ed__13__ctor_m28208CE8B37AE3BD3DEA92A9CA7C2550CD1B0A26,
	U3CBehindEffcetU3Ed__13_System_IDisposable_Dispose_m5D5ECE97714B45352E7FE0799D7D22D8FBF4FC24,
	U3CBehindEffcetU3Ed__13_MoveNext_mFC472CFD18E9A9E0CF539B7CE2C979D8337ADBF9,
	U3CBehindEffcetU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2415F6F12EDB41117580352211AFD403458630F3,
	U3CBehindEffcetU3Ed__13_System_Collections_IEnumerator_Reset_m4852B9E34F04E6A242FADC7DC6683B8486227923,
	U3CBehindEffcetU3Ed__13_System_Collections_IEnumerator_get_Current_m8E7ECFAD48F2F16F3430F65D369A450C578B6F50,
	CookedFoodOpreat_ResetCookedFood_m7A2A4637889FD40142350B4D94116BADC721BE94,
	CookedFoodOpreat_LoadingCookedMinFood_mF19349348882279F244E785E8109559024FCB0A4,
	CookedFoodOpreat_AddFoodCooker_m192DAC18546ADFE94092E5EED3087F1A59035253,
	CookedFoodOpreat_SubmitFoodCompelet_m83EF4764CC6D3BA5B43F8C855E99B708C97AA36A,
	CookedFoodOpreat__ctor_mFB4D745758078EB79BD8877BD37BA1A93B937DE1,
	DrinkOpreat_get_DrinkName_m0E0A0727D20225132FFAD728389D4CA0700CAACA,
	DrinkOpreat_set_DrinkName_mB5D06D6E9047959678D718576C2DACFDDDF81D5A,
	DrinkOpreat_get_DrinkID_mD9ED8F928C2D3BAA540CD9292A2BD7B138E63691,
	DrinkOpreat_set_DrinkID_m8AA6F510EA7343922485F10AF162FD9C2F80BCFB,
	DrinkOpreat_get_DrinkPrice_m6B42867611F83B868B044BFF997A2E6E594BEB63,
	DrinkOpreat_set_DrinkPrice_m492D2382FFAF2808318394C357C161591D9E79DA,
	DrinkOpreat_Start_mFD2AD5F55BE49144A498F8F95E95EA48535A5EF7,
	DrinkOpreat_Configuer_m5BA4D423B60F1702541380425CE74FAA643B3060,
	DrinkOpreat_Update_mD929B3C122EACD14BF07A1345BE24D0B7A69976B,
	DrinkOpreat_OnClick_mD21923C42890E1F8091563FC60DE6F5FFB2DBDFF,
	DrinkOpreat_SartAddDrink_m0CE606B1D4EEA452D7B7FD5E7CB1A7F3E51B5EA7,
	DrinkOpreat_SubmitDrink_mD3135D7CA2D3714043784A4F953321186F247D1E,
	DrinkOpreat__ctor_mC6BE7AFAEC3E0DF028483B2D5C3FC6C25FEFFF78,
	U3CSartAddDrinkU3Ed__24__ctor_mB49863CFEB9407A8AE4B6E16C0133B0598B12A34,
	U3CSartAddDrinkU3Ed__24_System_IDisposable_Dispose_m593F1B67BD1FB1502C8588DE468C9F4B71292CA0,
	U3CSartAddDrinkU3Ed__24_MoveNext_m15C8B27026EFD4BFA4C787F81ADB998091AA19AC,
	U3CSartAddDrinkU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m23444EF529856D487D201B9669EE056E56029845,
	U3CSartAddDrinkU3Ed__24_System_Collections_IEnumerator_Reset_m1D5FD5E7335F1AF14C1B928E28B7C2991ADE18B4,
	U3CSartAddDrinkU3Ed__24_System_Collections_IEnumerator_get_Current_mD876C0587ADA58667A54D19932239E91163F1B36,
	FoodProperty_get_FoodNmae_m182BC9812ED2D09F54AB0DC4FEA8CB6AAAE34136,
	FoodProperty_set_FoodNmae_m86476A1859800FF4C697FDABA44AC713AFCA7643,
	FoodProperty_RestFood_m6D96FCE46A07EF56A515EF2B4AF2A5EB2B58B018,
	FoodProperty_AddOtherFood_m900419690EF58C567B884BE51D60ED752E552A6B,
	FoodProperty__ctor_m783FCBE3F3295D1ABE6A1E350411E2519557E12F,
	GreensFood_ClickFood_mB2343BEA39EBE4173C4964F7D30A70559D69E11F,
	GreensFood__ctor_m803C9C98FB08E674C37C00634165BB11633E3785,
	MakinFoodOpreat_LoadingMakinMinFood_m011C9148FF9D02379C8CC651CA8FB73725932F45,
	MakinFoodOpreat_ResetMakinFood_m91B16C938DA909495660E819B19AD01373A5C306,
	MakinFoodOpreat_AddFoodMaking_m9AE6FC571022AD0BE5063B75036889DA90862BAE,
	MakinFoodOpreat_AddOtherFood_m9A951432C5B1A5206E01B9BF0F3744040E9441F1,
	MakinFoodOpreat__ctor_m58F9EA11BF9794D748D6EA7FE2C9236411FC31DE,
	OtherFood__ctor_mD3C47369BED186C27FB2EC9E287C6864656DE86C,
	otherFoodTypeTwo_ClickFood_mD53CA9599B6535ADBCD666CCB726422D2005F42C,
	otherFoodTypeTwo__ctor_m2BE978CB8A4F6C244241E71D4F33E1212A9098DA,
	RawFoodOpreat_get_FoodName_m3B67A96DCFD5C95F5BAC785655ECFE5D192AA7BD,
	RawFoodOpreat_set_FoodName_mCE22154F334E2CC875CCBD0403BDC56043FDCA53,
	RawFoodOpreat_Start_m38E3A35979EEEAA1506AA3E392E1C8F67AA03865,
	RawFoodOpreat_EvetClick_mB033958C51CB3854F9CAE8D54C5D9E2D51228E37,
	RawFoodOpreat__ctor_m91CD052D141D58FC616811708AD3B17337C4B7C1,
	SteakFood_ClilkFood_mFEE6A1709EE2F82B2DE5D8470284D271DD0980C7,
	SteakFood_OnEnable_m4F7CDBB71668757D93F89DA1CA00ADF9680C3994,
	SteakFood_OnDisable_mD7A0CC010CF11A5A28EFDA8DD61216094F27AC31,
	SteakFood_RestFood_mAE75259F10B2F7BAAEA0217D912A159F11830381,
	SteakFood_Update_m6835B2B7163F53C5F3B2369442E1E3998F941E50,
	SteakFood_AddOtherFood_mA547CC6E19152557F8134570521A4ACA88801A0F,
	SteakFood__ctor_mB4D756CE8FC57921E01E07F49EB6869415A274B5,
	FoodIDGenerate__ctor_mC6F4E4E3000529C8A89697E8C21A7A56BA5DA991,
	AESUtils_Encrypt_mABBA2B4C72B4B346A3F83E76B1CEF80A77A87ED9,
	AESUtils_Decrypt_mC6739A384E4EEC38B514F9B564C91576B79731B8,
	AESUtils__ctor_m5A1E5E0CCEB4DF7800C7B50D610BA078487CDF6E,
	CloundSaveGameData__ctor_m0D3E18E0F16D1E6EC8FC955F1A7795CA921D23B0,
	MainFoodCompnentAssets__ctor_mBBD5FB2F1D59D46C72214E89FEA52981595D646E,
	OtheFoodCompnentAssets__ctor_m7AE04539E4CA3CB4AC3CB731410C62BFCE95A388,
	OtherFoodItem__ctor_mAC4B24BAED3F66015D6B8B93E51F3A60C0C0C642,
	MainFoodItem__ctor_m559FCDA441B25F96ECFC09259677FB0099B6E9AC,
	FoodItem__ctor_m68AC1D167A2EDB705DF322708CE06C6C6B3EA3CB,
	FoodMenuAsset__ctor_mB3F23F6A3BE2E018E39FF69C0BE2214427F94BBF,
	FoodMenu__ctor_mB7636CEF989E5F91B52C380D84AB2848325BA965,
	GameTaskData__ctor_m48DC71356C1DB8FE33F2D80CE36980CBAF445128,
	FoodMenuConfigur__ctor_mB7AE8A536E8FBFE885F1AF13CC6FFC333AC5F4B2,
	GuestConfigur__ctor_m0B7DEAAC5B02E946FA120D86BF17BC6B89FC670F,
	GuestData_get_GuestPrefab_mCA685EEF6F411E9ECC6C7D7A1C8A6D4CE8087E54,
	GuestData__ctor_mF8BA2E8B9C409B7AFABD6249BB565A8C93408FD7,
	HeadAssetConfigur__ctor_m1C93507C8F780973B13A052B92C6373DF0C5BA67,
	HeadAssetData__ctor_mC014C6F69E196C8DB977007F194A85BFCABFDE8C,
	MainFoodConfigur__ctor_mA49777F4E5C2654AABAF6B55E6CAF740006A0134,
	OtherFoodConfigur__ctor_m47B77365B68DD47C1A9978629512BC62B3AE2A42,
	RawFoodAssetsConfigur__ctor_m1E58C03DEB02A606C55EC84B7A9637D4159CC174,
	RawFoodData__ctor_m4F23C97A3A516149EE5B65451B31F5344D1E2EAF,
	RestaurantAssetConfiger__ctor_m5175DB4789A6303B8F415A5C975E49E973792293,
	RestaurantData__ctor_mED325B149A80736088D56CC1590B0FA655CCA658,
	DrinkComponent__ctor_m2A9532E35E4BAFA83EE206D5C59D4E598A380160,
	ShoppItemConfigur__ctor_m738999308310E63FFBF83E1FF1B41941CB615A08,
	ShoppItemPropData__ctor_m0EBDA70EA01D09EDE157E88576FEBFBC99BF6F3A,
	ShoppItemGemData__ctor_m7A5126FEB8F5372E18CE6670F95EC742F20E554F,
	ControllManager_Configuer_m11135D88D5491A63C4F48EE1E6DE7ED8C5994927,
	ControllManager_Init_m039C52A8BE0333E83889DAC285443193D9180C89,
	ControllManager_Update_mCD36E094028BACB97D11A58719771321726B1502,
	ControllManager__ctor_m08C4F2F5B06F57F7D02333F00C0A6DA8F08B131C,
	EventParamProperties_get_DataObject_m46996693C9EE878EBF72DD8A145BFC6DF24B01D9,
	EventParamProperties_set_DataObject_m0F3D20933C311955E267969DAE080D1262AC832A,
	EventParamProperties__ctor_m9ED223FB1D814D584E1A2D4700F3C22EBD09399C,
	EventDeliverCookedFood__ctor_m75E0739E1C82EE33A3622EB4B698982AE9DE287F,
	EventAddOtherFood__ctor_mF817ADE9A4A81CF000E6BE61B554DCC7708ADE70,
	EventAddDrink__ctor_mD8B651EF87E332CC8BD6BD0147002423B976F911,
	Guest_get_NeedFoddCount_m84B0E0252F57B1A3379F1805105D9B66DA5228BA,
	Guest_set_NeedFoddCount_m6233BB0D398FD092192E828FD619E5435F55E0E5,
	Guest_Start_m8C27C4E65A1BE5DD2F897E9C3D7858A846E4B9EA,
	Guest_GuestRest_mB85644BDE0DF93620C9CD20C6F24427524E01FAF,
	Guest_SetBalckTrager_m7C5BA9FF7A2D8EB492BB6D6BAFCEBF60A0D60FBE,
	Guest_Configur_m713029DFF58A2F5DE257568BAEED1563952E3850,
	Guest_GuestInfo_mAB0E5D9B7DEC660BF9F5519BEDEFE52DB5F0112B,
	Guest_CreateNeedList_m3EF4068CD1B282CC711B6B5DF6067FA708BAF048,
	Guest_SetFoodMenu_mC1CD07D417A2AF670576D379898755B5B96650EC,
	Guest_ComparisonNeedFoodNum_m5F0D558C3E72B797F37BE903970BA165F2F2E4B4,
	Guest_StartGuest_m85EACBC5A8914EA1CD45BF66A480B2D8E250419E,
	Guest_GuestCone_m2F1E48BABBF77E3DC8437D59F1CCF5592DFD3EF4,
	Guest_GuestWaitStart_m3C4AB816BCA8DCD5CDC868C6DA811F3E849EBA8B,
	Guest_Update_m8E5F04FBBD9104522EFE5B94054E54BE79760C34,
	Guest_CompeletNeedFood_m68448E83859DF9C3090C5D431CAD2A49AC96F9BC,
	Guest_GuestWalk_mEA5D82EBDE12A263FDFB81E2809C5AB2DFDBCE78,
	Guest__ctor_m5B8E0311ED23C7CDC58A261772C23E718F862CD0,
	Guest_U3CGuestWaitStartU3Eb__34_0_mFA79CA8683A6BF9372FEA6984449F56F9CDC5697,
	Guest_U3CGuestWalkU3Eb__37_0_m5E7FD21B493760AFA1613A3FBB33CEF332C3B304,
	U3CU3Ec__DisplayClass31_0__ctor_mAAB140FDFF7F5613D3B095C5F7C0BED8686BBA26,
	U3CU3Ec__DisplayClass31_0_U3CComparisonNeedFoodNumU3Eb__0_mCE42ACA4BBB03154B45255C4AB6635D285EEABD3,
	U3CGuestConeU3Ed__33__ctor_m22091E15388CBFD4276608933F24B4CECF28333C,
	U3CGuestConeU3Ed__33_System_IDisposable_Dispose_m504191E7270F5B7BB011E7B1FE3349028E59F9CE,
	U3CGuestConeU3Ed__33_MoveNext_m573C14231875314856FEE2C2B6C4C34033CB7F23,
	U3CGuestConeU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m03706D255746C1B8E78735EC8FE86751E64CF6DC,
	U3CGuestConeU3Ed__33_System_Collections_IEnumerator_Reset_m6EB7CFD7A25750D3838767516B1923831FBFD25B,
	U3CGuestConeU3Ed__33_System_Collections_IEnumerator_get_Current_m5FBCA8C78D33C14A94294946057F93A3AC0EB650,
	GuestManager_Configuer_m1E9DA203833945624D4DE65C6097CEFDCAE2F120,
	GuestManager_Init_m45E7FF5936ABD0664DFA4B3E69A2452415AD79AC,
	GuestManager_OnDisable_m2EDCF3E2F76F60B2B7BBA2B899194F2AA3D130EF,
	GuestManager_ResetManager_mE99D83B998017EDADC2439153B6D801747148781,
	GuestManager_GetCurrLevelGuestCount_m3C1856131C809902D8693E56463782123ED91979,
	GuestManager_RunGuestManager_mFA5181EFEC0A93BE8661A166999765959AD3EF26,
	GuestManager_GuestEnd_m68F73E70E0A57C5B55B1597EC31C2F837FF5920A,
	GuestManager_SpawnGuest_mA2F3C6296B38A376F1B0AF6AF14A42844E499B45,
	GuestManager_SetGuestMenu_mC73DD1B65089D6D810C7142C91ECD730195C05C6,
	GuestManager_foodMenuLock_m54A1DB892E5FA332D714A9DDC1548A7655CD96CE,
	GuestManager_OneceMoreStateGuest_m6AAA9E603FF93C4D6CC5C7B15E2BBC23B8B19D1E,
	GuestManager_StateGuestCome_m11B99B303676065836203EF0DE7BDBA3A1A2AAE4,
	GuestManager_StateGuestComeOne_m2819DA0993B30BFCC5FD99482398168B67782421,
	GuestManager_CompeletTarget_m4E36C1A74EC5B3CA3F5F480794A187C9643142A5,
	GuestManager_AddTollGuserData_m291B71D90F6426B008E2581AC56463147D3A449D,
	GuestManager_CallGuestReceiveFood_mB1F1BFC2821E2248E533078A2B35E45956D9FA5F,
	GuestManager_GiveGuestFood_m7A813A21D5DAA852755257C7A0F23A994C64D469,
	GuestManager_CallAddDrink_m9C7D121CC175248326023C886C05479FE471AC7A,
	GuestManager__ctor_m737B5B7C4F0D12A119EF07DD7CC74E07597DE844,
	U3CResetManagerU3Ed__11__ctor_m25797BFC57E47A30920DEBC8B61AEE25EE03712A,
	U3CResetManagerU3Ed__11_System_IDisposable_Dispose_mFEB35C550D0DD1EA2EAA7A17F3B2B077B5C509AD,
	U3CResetManagerU3Ed__11_MoveNext_m4EAEF24C56807873A73CAD49E61A438EEF18C3A3,
	U3CResetManagerU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60C2FD3279EC280632304C5003C316F5BC339224,
	U3CResetManagerU3Ed__11_System_Collections_IEnumerator_Reset_mDD64A7EA4908862A56386DE9F92354A6CEEFACCD,
	U3CResetManagerU3Ed__11_System_Collections_IEnumerator_get_Current_m5AB29A93F44AF9AC5F3CF35088B4B195D7932E6C,
	U3CStateGuestComeOneU3Ed__20__ctor_m10A5B4D963F3C464B86F48756B1EA76D24F2875F,
	U3CStateGuestComeOneU3Ed__20_System_IDisposable_Dispose_m5CA8C6C298E0C9EE9D43569C253E9A548EE9AA7F,
	U3CStateGuestComeOneU3Ed__20_MoveNext_mDED9ABCAACCDCBF98563AB0295640A464046760B,
	U3CStateGuestComeOneU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m740C2CB747EE772625C674A6685C383C23160AE6,
	U3CStateGuestComeOneU3Ed__20_System_Collections_IEnumerator_Reset_m671B8D0358C7315D168F7EFB54114904C977E1DA,
	U3CStateGuestComeOneU3Ed__20_System_Collections_IEnumerator_get_Current_m0855980882734C8C1D980A1025A405A44E98E2A7,
	GuideInfo_ClickEvent_mC3B688B4BDE99DB434D12EC21235F0050BD848B2,
	GuideInfo__ctor_m08A0194DE6C0C723ECC3BF6631E3BEF66E9815CF,
	SystemGuideEvent_Awake_mAF45D7ADB7485C5D3BB3FE41068AA9A11689720E,
	SystemGuideEvent_Start_m33957F6C7D30CBC9DEA8811E3217112681A0F920,
	SystemGuideEvent_AddOnceGame_m8445858E364BE743F7319E7E2719081A394B09CC,
	SystemGuideEvent_ShowGuide_mE8D393F0001B43056B42A98B409261EC0EA0625F,
	SystemGuideEvent__ctor_m6334B6456DD6F4CB42712E255F0C91A19B28E602,
	LevelManager_get_CurrLevel_mA248FF9400D0E4DC8D0FD0F6B7423A9486F1EA40,
	LevelManager_set_CurrLevel_mBC4CC002D968B932972BD8F401B028AC0A4AEC54,
	LevelManager_get_CurrRestaurantInfo_mF963844AF5EC0F36BAD3FB13CC3D20970DE29D62,
	LevelManager_Configur_mFDA771DFE4BBA34ECADB4A337DC43B69200E70FF,
	LevelManager_GetCurrLevelData_m7EBB842E6F572EEAE054F5F539207733D59F25B6,
	LevelManager_RestManager_mFD17CE9472415142AF83CD9062947944EF5482AB,
	LevelManager_SpawnRestaurant_m9589BFB79634C95224553B326EDBF3C320DCFC8E,
	LevelManager_SpawnRestauranYoo_m803475F9106881A6CCEED8979A2798B2A12B7E21,
	LevelManager__ctor_m97F35AC08C296B73BD7D85FFB593A7BEA61B3F92,
	U3CRestManagerU3Ed__12__ctor_m1107F88F0DEE0EC07E51CF67F15FBE2DA28FA88B,
	U3CRestManagerU3Ed__12_System_IDisposable_Dispose_mDE66F2B3784773AB60500935B81F60DE841DE9CE,
	U3CRestManagerU3Ed__12_MoveNext_mA8488ECFCE8852E25181A8FF432C0DE680F1650E,
	U3CRestManagerU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB4C668E196003D1292524F03B6AF0011E50668A1,
	U3CRestManagerU3Ed__12_System_Collections_IEnumerator_Reset_mEA8129A9EBAD039B0B0E0885ACFC3A547AA3BEE3,
	U3CRestManagerU3Ed__12_System_Collections_IEnumerator_get_Current_m7F2B45FCEA29A5162BB5958FB852CD722DFA778A,
	U3CSpawnRestauranYooU3Ed__14__ctor_m2B2DC5A7B856D7E256594B3F3F23F70530168C99,
	U3CSpawnRestauranYooU3Ed__14_System_IDisposable_Dispose_m0400A35DABA2DF91023CD4E460010D11403D9BA6,
	U3CSpawnRestauranYooU3Ed__14_MoveNext_mF440571B9199BF2EA8809E30503022144DC406D1,
	U3CSpawnRestauranYooU3Ed__14_U3CU3Em__Finally1_mFF9A547AC3DAD345D43FAB1B2AB975A7D70FC370,
	U3CSpawnRestauranYooU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31DC238ABAB71D32CE9CE8F5E4E943E9B0257224,
	U3CSpawnRestauranYooU3Ed__14_System_Collections_IEnumerator_Reset_mA0FC535E9C3B9815E0ECD14C0F23DF011515FF94,
	U3CSpawnRestauranYooU3Ed__14_System_Collections_IEnumerator_get_Current_m05C067FF9D6A438BBFD563194CE26798FDFD9D09,
	LoadConfigursManager_get_LevelDataList_m8F75A8550E70B1397BF65EE153BDCD43B5129940,
	LoadConfigursManager_get_OetherFoodDictionary_m36B7FC9F0D6DC32F3E4AC2DEE549AE373483FDA5,
	LoadConfigursManager_get_LevelGuestDataDrictionary_mDEFE35D5A36BB7CDD161E4073C872BA7E1701A7E,
	LoadConfigursManager_get_FoodMendDicrionary_mAE13857BC3759CFD1E7D5320F0EB83423A804FA4,
	LoadConfigursManager_get_FoodItemDictionary_mBB5535E4217C463EEDD30CF00B0F545961FACECA,
	LoadConfigursManager_get_MainFoodItemDictionary_m05C6DBBA287F6F0E66240F5F4C853076BCF4B97F,
	LoadConfigursManager_get_TaskList_m4347F42FCE07A8DD37998C2B97C0D7A183D907D3,
	LoadConfigursManager_CreateLoadData_mC05CB48283C754DC8D477F9F9886E90FF5319CFE,
	LoadConfigursManager_Configuer_m6B0B924FF4B416FB9833E39B05546FC5F412A673,
	LoadConfigursManager_LoadGameData_mCC0C37AEAB2789459128CECB16A7B05389D828D2,
	LoadConfigursManager_LoadLevel_m3397FFF0C54833CC9FF4D442DFACA23624ABADF6,
	LoadConfigursManager_LoadLlvelGuset_mA09446075E27282615E56CC050A811491DE6445B,
	LoadConfigursManager_LoadFoodMenu_m247B655731C858C6C86F9A6C1AE469F9355A2424,
	LoadConfigursManager_LoadFoodItem_m02E050D58D7FF5E9E6859F628E519A7D6C799268,
	LoadConfigursManager_LoadMainFoodItem_m275260C59226326E1083F630D988780B51294647,
	LoadConfigursManager_LoadOtherFoodItem_m266311F77A2B35DB3420548C85487B1D54D0E3EF,
	LoadConfigursManager_LoadTaskData_m642212D10E509B10AEDB5ED423CACDB8D9A061A3,
	LoadConfigursManager__ctor_m70E7AD404DF3703056D75354E072129D8A3C5BB5,
	U3CConfiguerU3Ed__25__ctor_m9BF13BFE8A290FD12FCDB0F85D8D655105421D1F,
	U3CConfiguerU3Ed__25_System_IDisposable_Dispose_mC23AE4FF0E01CA7B41C3F560B9F45AD066F7165F,
	U3CConfiguerU3Ed__25_MoveNext_m9C2EC606E1BB3A7F4C1968626657B70F283CE0F3,
	U3CConfiguerU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m38808BF86499BC4F1DE18F706EC4809A7AAA3AC6,
	U3CConfiguerU3Ed__25_System_Collections_IEnumerator_Reset_m5D04295C3369EAD257995C8C972FBAF86EBEA5CE,
	U3CConfiguerU3Ed__25_System_Collections_IEnumerator_get_Current_m3C60F36356282E59C8777AEA4FEB49495C355F9B,
	U3CLoadGameDataU3Ed__26__ctor_m69920F5A82686334E47741F404C358F90B223D46,
	U3CLoadGameDataU3Ed__26_System_IDisposable_Dispose_m37EF2A08DA94685C88623BEB3CAE99A31E06056B,
	U3CLoadGameDataU3Ed__26_MoveNext_m7BAAE6692FAA56B6A9425CC5B2A7879F1140C4AF,
	U3CLoadGameDataU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1B136C554167950EAD4FBA400C0357C8E8656E92,
	U3CLoadGameDataU3Ed__26_System_Collections_IEnumerator_Reset_m22EE74F1CC14356DFDD1DE6B6C3CE932D8AA0027,
	U3CLoadGameDataU3Ed__26_System_Collections_IEnumerator_get_Current_m2512E0381B54CE83797A6AFF266905CD224F4BF1,
	U3CLoadLevelU3Ed__27__ctor_m211C9203A1D4E624153DD184197EA5119AA94182,
	U3CLoadLevelU3Ed__27_System_IDisposable_Dispose_m27D084B3024499C26AF51D62993110F35A24308B,
	U3CLoadLevelU3Ed__27_MoveNext_m2CF8DB3BEE32F498AA22FCF0A8EE775EC0DBAC6F,
	U3CLoadLevelU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA09FDD773DF28A1AC1E580F8A232852B17A9508D,
	U3CLoadLevelU3Ed__27_System_Collections_IEnumerator_Reset_m735476FC354847C8EE4164C47EB473011E05C829,
	U3CLoadLevelU3Ed__27_System_Collections_IEnumerator_get_Current_mF83ABA21793A0900C74E2AF918EB96768E0E5D09,
	U3CLoadLlvelGusetU3Ed__28__ctor_m661B826FF14E724C97015F108927A0B31EF7D351,
	U3CLoadLlvelGusetU3Ed__28_System_IDisposable_Dispose_mDBB6CFFC80EDDAAC5A09121011E7BB6DA856287A,
	U3CLoadLlvelGusetU3Ed__28_MoveNext_mD94147E62EFE6753EFCCE2FC35F51FE6C66BBFB4,
	U3CLoadLlvelGusetU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3584D7F85E906FFAC71149BEAFA0F5220F13B37F,
	U3CLoadLlvelGusetU3Ed__28_System_Collections_IEnumerator_Reset_m516AD0BB878982920CD09E951BC520747876F5E2,
	U3CLoadLlvelGusetU3Ed__28_System_Collections_IEnumerator_get_Current_m718F57AE8B59A2B09FD5E4730A6CEEF30BED7F15,
	U3CLoadFoodMenuU3Ed__29__ctor_m869B922E41435332C272B5F6C8ED29FE60D489AF,
	U3CLoadFoodMenuU3Ed__29_System_IDisposable_Dispose_m4AA5BAAF16FE4D06E27592E83F41D64D357727DE,
	U3CLoadFoodMenuU3Ed__29_MoveNext_m6B972CDD976CE893E490760FBBF7796E9AC50F72,
	U3CLoadFoodMenuU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8BEAED93B1409EC8D2D9722993E6C84CAB8FA3ED,
	U3CLoadFoodMenuU3Ed__29_System_Collections_IEnumerator_Reset_m28B2A4107B6863ECB0FB63CF77F669938A64134C,
	U3CLoadFoodMenuU3Ed__29_System_Collections_IEnumerator_get_Current_mECC1CF0109EB7720779D199107B675DA6A2F9DC8,
	U3CLoadFoodItemU3Ed__30__ctor_mB6C6F8744DABF0ABEAB18143ACEC1CFA9D6AF9C9,
	U3CLoadFoodItemU3Ed__30_System_IDisposable_Dispose_m3C544C066A806052C10EC384161A4C29613F1100,
	U3CLoadFoodItemU3Ed__30_MoveNext_m8ADE63C3F3284B1CBB0800741EB78B5C07897A06,
	U3CLoadFoodItemU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DE1903E3664282E99E6770B40BFE011B10E7648,
	U3CLoadFoodItemU3Ed__30_System_Collections_IEnumerator_Reset_m6A30FFF929371D2F8172E7A4FB3DEFFF944FBCD7,
	U3CLoadFoodItemU3Ed__30_System_Collections_IEnumerator_get_Current_m372594A8052EA16CC04E1B3E8AC1B6B31C7868BC,
	U3CLoadMainFoodItemU3Ed__31__ctor_m278CC63ECC1F6D8CDEA5DE73B10CA8C7AE931AA6,
	U3CLoadMainFoodItemU3Ed__31_System_IDisposable_Dispose_m449BD9A0BE940725D5144310D4FFB412583DF7DA,
	U3CLoadMainFoodItemU3Ed__31_MoveNext_mEFFDA34F57E03AD56ED542BABDDAA3208B60779B,
	U3CLoadMainFoodItemU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC28457AB84A079958A94D124E77AD99490251096,
	U3CLoadMainFoodItemU3Ed__31_System_Collections_IEnumerator_Reset_m8DF21827E6A731A1487C27098E805BC2CA7AAC05,
	U3CLoadMainFoodItemU3Ed__31_System_Collections_IEnumerator_get_Current_m75EFBF9A78E565190328770B25559C31A6445AE3,
	U3CLoadOtherFoodItemU3Ed__32__ctor_m3ACA5448A5F88ADF63D79EF57D03315DE832034E,
	U3CLoadOtherFoodItemU3Ed__32_System_IDisposable_Dispose_m7BE9EC73D03B07252DBE706932DDDC173EE9BB0D,
	U3CLoadOtherFoodItemU3Ed__32_MoveNext_m2C0E761B4F7B634ACFC9A150E3C7B26176EB11FD,
	U3CLoadOtherFoodItemU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE9AF60BAED187D79F60B1A36EBAEDC23224E8C68,
	U3CLoadOtherFoodItemU3Ed__32_System_Collections_IEnumerator_Reset_m337C715E2D4773E5E3166F32203038A3E016B646,
	U3CLoadOtherFoodItemU3Ed__32_System_Collections_IEnumerator_get_Current_m448071658DBC519EEA6C1C46EEEFBDC7AC240CCC,
	U3CLoadTaskDataU3Ed__33__ctor_m6940EC9635F83D1A412B6C5EDAB50AFF4D71F059,
	U3CLoadTaskDataU3Ed__33_System_IDisposable_Dispose_m27FB299F26E52188C8B0F7D0D1DA433CCC4122A0,
	U3CLoadTaskDataU3Ed__33_MoveNext_mFE6D830FC90936F1A74CCE23673122CDFE6F6A72,
	U3CLoadTaskDataU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54A685D362F3F13655316F16FFC2C92A2B171708,
	U3CLoadTaskDataU3Ed__33_System_Collections_IEnumerator_Reset_mEF06AE983088FB860B42600459EDA5EF1FCDDD78,
	U3CLoadTaskDataU3Ed__33_System_Collections_IEnumerator_get_Current_mD6107444418461B83F9EBB1F9E36CD7712A6932C,
	MapManager_Configur_m9064C96483E3067D44A003C233CB311765F4E5E0,
	MapManager_Init_mDEAFF88C04EF2378FD0C6EDF230ADC619538EEE5,
	MapManager_MoveMap_m24BAC81AC951C12C29D831D443E133988C7306DF,
	MapManager__ctor_m622D319D4B07043DDB690FB88FD531C149C63A7F,
	OperationManage_get_CurrMainFooResdDictionary_m4F55677A7179EC9B9FDD248445ECE686155D20D4,
	OperationManage_Configuer_mAB091D6CF5433BFA445E6099CF71499393A774BC,
	OperationManage_Init_m67D156AE8CB4361BA8C5AFB1459BABCD31F57788,
	OperationManage_OnDisable_m94142E27019232D69C1AF2BE8A770EF7688129F8,
	OperationManage_ResetManager_mA3090BABFD001F709C472ABD8E2EB2C0C04952B1,
	OperationManage_LoadingFood_m20692AC5B14356B5D11BEDA29815659A926D0A8C,
	OperationManage_CallMakingFood_m0D7EDA5A5981F13E96C3E1A9DE27F57A6BF001C9,
	OperationManage_CallAddOtherFood_m7A34B4BB3EFD9B1592784D378915887E6DA0D762,
	OperationManage_CallCoodeFood_m5FA51442F293EC6164D83ACF1C3450D930328A0D,
	OperationManage_CallSubmitCompeletFood_mD6AE251BAF1F28A2FEFC9C6E02AC9C1AAF833D34,
	OperationManage__ctor_mE70FE15C790E75B857B3104503E313376624AB02,
	U3CResetManagerU3Ed__14__ctor_m667DDBDE193598F0FA20A8B46A27CF6BFBE409A1,
	U3CResetManagerU3Ed__14_System_IDisposable_Dispose_m3E97DBE382CC8F43F7DE6F7FAF11A807078FA3FE,
	U3CResetManagerU3Ed__14_MoveNext_m2A042EDF6AA8C585A9DC2AAA56909243AFAAA0BC,
	U3CResetManagerU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9FDB8AD2EC9E49D54CBB3E58C2F648FB514112CB,
	U3CResetManagerU3Ed__14_System_Collections_IEnumerator_Reset_m4307E06D8893C70F12C370B46BA545A81507FD04,
	U3CResetManagerU3Ed__14_System_Collections_IEnumerator_get_Current_m69388E442045B69F5FE587860F75B9E1598964C2,
	PlayerDataParameter__ctor_m5DF18427D5F2CD076A02E1B945B96B2F6F1C8066,
	PlayerData_get_Inastance_m3832A091380C1B3B6DD43C1BA59B488E305B2199,
	PlayerData_Create_m5E3DDCC42DBEC53E52B32ECB8D70F97B1D6A24E3,
	PlayerData_Create_m1D5ADD9C6648466D8524AC1EAE9264EC3D2BE02B,
	PlayerData_Read_m39AE6A510549330E8D762319702DF7EEF15C3D12,
	PlayerData_NewSave_m895A3A53BA086830E6228EB6CEE9C310AA181EF0,
	PlayerData_Save_m7ACDE3046A5377BD21EB86D7A5B2D35036663CE4,
	PlayerData_ClearSave_m097943EAE9AD065D37871712D3BEE689A5CB149A,
	PlayerData__ctor_mDD79CC7712EAF325215F97832BFE3403DF47DFA8,
	PlayerData__cctor_mAA4A511FFB992534007E014521B7BC752BA46776,
	U3CReadU3Ed__9__ctor_m82130BBB229ADF75F01F85196B6AD408BA7C5FC8,
	U3CReadU3Ed__9_System_IDisposable_Dispose_mB3B9737706374FF9000C326636044E6458660CEE,
	U3CReadU3Ed__9_MoveNext_m4E566CD1B19E7D6265E782BD6DFF61F8B402CA12,
	U3CReadU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC6B0780885F02C5CF7E125ECCA231C7FC7696AE9,
	U3CReadU3Ed__9_System_Collections_IEnumerator_Reset_m08792EB7D144ED8F19C45A285841C8D6A2AC747C,
	U3CReadU3Ed__9_System_Collections_IEnumerator_get_Current_m1291C058D013C746335A37DE95EF8F0A5C2B9A0F,
	ResourceProfile_get_ConfigurName_m32791B0D98CF227EDCE782BC6A31C03412A05D70,
	ResourceProfile_set_ConfigurName_m8E7B8382885915E24D6BB0B76885619FC22FB642,
	ResourceProfile__ctor_m3A44D85C9891C5A90A6368CA4D09E3489601E877,
	SystemDataFactoy_get_DictionaryConfiguirs_m4FF49213FF91CF4F401239161D8558B233216655,
	SystemDataFactoy_set_DictionaryConfiguirs_mED3C4063D8E68B7C3B987524B99B50E4A7E4D6B3,
	SystemDataFactoy_Configur_m5401629C61EE9A8E8A6DB5B54A7C1335D15BD3F4,
	SystemDataFactoy_Create_m5A43EA6DD0713BB94A05EA0D380FDEB23801C1CC,
	SystemDataFactoy_LoadConfigurs_m51E7617588511C9E3D39FE78A70520AA4C35EB49,
	SystemDataFactoy_GetResourceConfigur_m3BDAEFF051302F458A6DE060A988457C74105F72,
	SystemDataFactoy_LoadAssetBundle_m2FCD6432E2BAEFE6B1F3B1E8362541C5B3A92A98,
	SystemDataFactoy_GetResource_m4B4E71D947BAD62BC92EA6FDA37CA7E475B31A5B,
	SystemDataFactoy__ctor_mBAE5A426D420654433699B204CAB15F405942F28,
	U3CConfigurU3Ed__8__ctor_m38CCCAE906F3D6FB49B74D61BDA4D57F4ABC813E,
	U3CConfigurU3Ed__8_System_IDisposable_Dispose_m37DAA65B41B5603BD5BDC9DE18121846F084EC7B,
	U3CConfigurU3Ed__8_MoveNext_m450C9CE0A5C3EDBC85933A8A3E23A4E17F9E63B4,
	U3CConfigurU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2987EC12DF72E7096AE31E42BCCB44E25643937B,
	U3CConfigurU3Ed__8_System_Collections_IEnumerator_Reset_m7CD56B0FFD67A29A2519E91560D80320986A3482,
	U3CConfigurU3Ed__8_System_Collections_IEnumerator_get_Current_mAB2C8F18593C1DB338D13F1923D0DCB4A695FA56,
	U3CLoadConfigursU3Ed__10__ctor_m0D57A0BEA29A3E666F2783FEE55E3E132BE452E6,
	U3CLoadConfigursU3Ed__10_System_IDisposable_Dispose_m032ECB9A2A5D2FFC941308B698EC8A836BE1D983,
	U3CLoadConfigursU3Ed__10_MoveNext_m5F289B085DC0243D5B79010380B43B5D497F27C2,
	U3CLoadConfigursU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7B88339CC1EDBF6D06446A07F02269335401AE3C,
	U3CLoadConfigursU3Ed__10_System_Collections_IEnumerator_Reset_m6A23C2FB65642F1C4E43D43E91B874ACCF8FB305,
	U3CLoadConfigursU3Ed__10_System_Collections_IEnumerator_get_Current_mD29614105EB118354D9060283FDFA11AE381130E,
	SystemEventManager_ConFigur_m49461E5B9A478FECDD8EFF5582361C55B3CA0B67,
	SystemEventManager_StartListening_m1E32F458A59BA1B6EB2E256B65366ABC0656C6F8,
	SystemEventManager_TriggerEvent_mE2DA3AFF6564F3F1A402F4651C5E5824A3234ABA,
	SystemEventManager_StopListening_m4736B4BB2DF246CD89F71A647AF91361D6AAE8E7,
	SystemEventManager_Clear_mE32E0E65DBA977075F9C3540A4491BE382956A4C,
	SystemEventManager__ctor_mCE0D000D52444F00BC192B9E4DBFE73F8098681D,
	SystemEventManager__cctor_m8DA21258B2E3B1195C32F731EAE120264186FB59,
	SystemGameManager_get_SystmConfigurationManager_mC47CA842BAE52DC50C75B260B2ECD39A7B9870AA,
	SystemGameManager_get_GameStat_m89042CF99C4F0B9A7D1033B8D306D68EF602F0E3,
	SystemGameManager_set_GameStat_m4B147D14295E6F67A8ACDDA22C22CE32D4F60738,
	SystemGameManager_get_LevelManager_mB1068C3EA73977B6ECC366AFDA961029D556C130,
	SystemGameManager_set_LevelManager_m5A5E2477EEA1A083E2C30F6BF4A9CB05867C5988,
	SystemGameManager_get_OperationManage_m7E665DA3CE84912EF66AB9273038C8E58751672D,
	SystemGameManager_get_GuestManager_m9E79AAEF230120F2569565E15A295374D5B8FE5A,
	SystemGameManager_get_mGuestManager_m586B96928C82E13CFEDBCF256D768DD5FEF9ADDC,
	SystemGameManager_get_UiManager_m076C0BBE25B122C7F97CC21318158E592614CCAE,
	SystemGameManager_Start_mE6276384BA173315A4BBFF6ACF9B7102CCF6A0B8,
	SystemGameManager_Init_m5588BE6A59893A4436B72D49DD4EC3B321A998B2,
	SystemGameManager_LoadData_mC922EAA6D5AE721742E8DD09DFC3E6E696C5B9FF,
	SystemGameManager_ChangeLevel_m6D71D266B700878A61704EFC9B08BAF6AA99A0CB,
	SystemGameManager_LoadingLevel_m403F426EDC866C182CB4A83CFDA741C49C92BFEC,
	SystemGameManager_ContinueGame_mF14B8471A21308687E73BB480EF84F7FE0B8AB45,
	SystemGameManager_ResetManger_mA0485083939DD8BAFC3C8EBDF2075CDCD641D334,
	SystemGameManager_SetGameRun_m16C863B4BD2F660848E2FA9D1DFAE578BB9297CB,
	SystemGameManager_GameEnd_m77EB26138C4B487748157C4CC8E9DA5D98CA8181,
	SystemGameManager_SaveCloundData_mB2B10C9204B03B871A95D755CA60AF5D911671B4,
	SystemGameManager__ctor_mBAC18CD30C927FEA57C2793189AB448BE795B021,
	U3CLoadDataU3Ed__24__ctor_m02AADB872B54F60465E6AE01FEE687660984C2AC,
	U3CLoadDataU3Ed__24_System_IDisposable_Dispose_m0275841DBC368AE0F3979D73507246B7FF95909A,
	U3CLoadDataU3Ed__24_MoveNext_m8F1986F1A293A99DF4DEFEAAB730CE4068EB2F59,
	U3CLoadDataU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5122489B9203191F986A039F470B7D2534C869BE,
	U3CLoadDataU3Ed__24_System_Collections_IEnumerator_Reset_mDEA95A48CACC4E225236D00515D342E4EE02B057,
	U3CLoadDataU3Ed__24_System_Collections_IEnumerator_get_Current_m1B478C1DF046ACC9003F19B5385804920A4A4592,
	U3CLoadingLevelU3Ed__26__ctor_mD8D4A8C530558282D7E6EFDD58F14439930C5FA4,
	U3CLoadingLevelU3Ed__26_System_IDisposable_Dispose_m6227C8D76A85C6534E78EF1EF195880CCA3D20F8,
	U3CLoadingLevelU3Ed__26_MoveNext_m2AB868E4BB0FCC80EE468FC5343F6DB66F003B8E,
	U3CLoadingLevelU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFFCFA7D514691029125BE192CDB7FC37408EEB56,
	U3CLoadingLevelU3Ed__26_System_Collections_IEnumerator_Reset_m64F1414C1F3D7DA54C8CD51D79C2AD2D41B6680C,
	U3CLoadingLevelU3Ed__26_System_Collections_IEnumerator_get_Current_mB355AC6D536105E08EC6DDC011417515C96E1989,
	U3CResetMangerU3Ed__28__ctor_m29A72BDF8E9BCEE28F95D9D25D33FEFAC3B3D042,
	U3CResetMangerU3Ed__28_System_IDisposable_Dispose_m4D120DE3C11918A203A5E33F5FFA5C4A68175004,
	U3CResetMangerU3Ed__28_MoveNext_m78A70C4FD792A74C85275CE71070289334BBAA21,
	U3CResetMangerU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m68D919578583A286FEE91418F5CC0C2AC1034EA9,
	U3CResetMangerU3Ed__28_System_Collections_IEnumerator_Reset_mC8133562628A3871A4206AA831CA347AFEDB4B1E,
	U3CResetMangerU3Ed__28_System_Collections_IEnumerator_get_Current_m0F6E19620C39FA151FE0BC645B7F060E4900A752,
	U3CSaveCloundDataU3Ed__31_MoveNext_m5CD46301F4D3D42310CBB8F6551B53F5D9DDFD6F,
	U3CSaveCloundDataU3Ed__31_SetStateMachine_m4CEC5F75738743AAFFE9AECB9893C38431C69113,
	SystmConfigurationManager__ctor_m2883D12F8778CC0E7D86EB3A8EAD51006E160EB9,
	UiManager_Configuer_mF7BF29AC3CB92D52E8F5B7078A051B5048D6A4BE,
	UiManager_ClickStatrGame_m3020F30326E42E4B784579263B6683FDCC40F91E,
	UiManager_CallUiRunGame_mD5C8FEAC5047C49A083503BAC68EF4B31CEA9667,
	UiManager_ChangeLevelEffice_m146DE0946B2BA534335778523D00FE78A90CEDF6,
	UiManager_LoadingPlanle_mA6F7F9856020FC8D111C5123414E4BB66B1865BA,
	UiManager_LoadingContinue_m33599E1334E4AB0B1AB27A807D62492B49D7DED8,
	UiManager_TargetOpen_m72DB0A7C49FE9945D9B52F0E598634BBF5506DEF,
	UiManager_GuetNeedComelet_m77013E97306C6F006ADF64B6F8208982B478B7D6,
	UiManager__ctor_mF1223D076D6716E82EAF760F5337E462084CD2F0,
	U3CTargetOpenU3Ed__17__ctor_mED0B1D3A961CE72313A26E2468EF5453D584D63D,
	U3CTargetOpenU3Ed__17_System_IDisposable_Dispose_m845B89A44CBA3DE5BCB167330D2479D512334B4C,
	U3CTargetOpenU3Ed__17_MoveNext_m4D0EF85F8B1B369F88D93E2D180263436775E2BA,
	U3CTargetOpenU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m11C299473D12F7FF9F424571A34D9677BEC6528A,
	U3CTargetOpenU3Ed__17_System_Collections_IEnumerator_Reset_mC544395FF7F68A1570F6E08575CA6E9F7183A531,
	U3CTargetOpenU3Ed__17_System_Collections_IEnumerator_get_Current_mF01FC11BA102021DF78210470156B34103ED149F,
	GuestNeedFoodData__ctor_m7D325A917721D2C1228BCA7D44325910A49F83B1,
	GuestParentInfo_Start_m0DE6AA1DD59C4B1444EF7CDE316B6320754A4C03,
	GuestParentInfo_GetHaveGuest_m91774CFC75B0D9BD412A59E390B762C17BB2B838,
	GuestParentInfo_SetHaveGuest_m0999A1646A5DF3DE50B70717E30AADBDE9591BAB,
	GuestParentInfo_ResetGuestParentInfo_m1BE3C6186245CD730D16FA3237ED629C1EDD5DD1,
	GuestParentInfo__ctor_m3BA78BF6B64E641BBCB62081257C56ADC6F17185,
	LevelData__ctor_m923000759B849E6F472A21ECEDD12006D85EDEDD,
	LevelGuestData__ctor_mFA81EBD5E1EF1325DF1AF750D5E961E404E3F75D,
	GuestTable__ctor_m38A07898CF7A2293AAE9A7C438F0BFABA4CDA67D,
	NewBehaviourScript_Start_mDEAA416D3A6676A79851DDF6B62D676E53372A87,
	NewBehaviourScript_Update_m68CAC9D7524B9FA1DFCA3F99BA694CD73147209B,
	NewBehaviourScript__ctor_mD2E080DE77BCDB61B6D2EC8AD996FAE611B97F3C,
	PlateItem_get_OtherFoodName_mA6F28FF8B3073550E34C45AFF4E567FE84AC505D,
	PlateItem_get_FoodItem_m40777DA2EF749D25E16E034799F17DD6DBDB1876,
	PlateItem_set_FoodItem_m3AEBC34F9C96466BD5768F9C322BEA3BD876F2F3,
	PlateItem_set_MainFoodPrice_m3D7B4FF25832B03B5E3B93096B3DFEDF67EFB06A,
	PlateItem_Start_mE05F47ED78D3C6313944CE83C176A988A4B9B1E2,
	PlateItem_SetFoodPrice_m5D43AFF3DD75BACAA10E703969FC555AE2441CD9,
	PlateItem_GetPlateFoodID_m49A5AA657A7238BDE1AABDA38AD8F283EF49DAE3,
	PlateItem_GetPlateFoddIDNumber_m2E65D6B782D91B0E01B9FE9D282FC2BEB2F6408E,
	PlateItem_ClearPlate_m774B4E6E407A7B1E3C21FE9078C7DE49D1D227D4,
	PlateItem_AddOtherFood_m4CCBA5C83CD1FD9224C2D12C09A6CC4CCF8870B2,
	PlateItem_DeliverFood_m0FA434F79143D8BA275ACBD7646BD131DB6943D7,
	PlateItem__ctor_mA42614FF360CF5824A83894C22991E6CE29BD72D,
	RestaurantInfo_get_DrinkOpreat_mA7BCF7E18EAB337A5E00246F17ABF0D0D03C6A3A,
	RestaurantInfo_Configuer_m35E0F6A03125C3A359A736FD789B0CC0378101C4,
	RestaurantInfo__ctor_m657BDB9E6BC5E35A5D4E4D5D02390E909439046B,
	HomeMapUiManager_get_HomePlanel_m0750B2B76C47AA5FDA9CBC99BAC1BD6772593D76,
	HomeMapUiManager_get_DamonPlane_mFF43751CE8C4D49E570D36289650A15B58C79F4D,
	HomeMapUiManager_get_HeadPlanel_m44F6AC860FF688CAF4F028C1733B200AAA2C584A,
	HomeMapUiManager_get_ShoppPlanel_m332AF2D623D43B176CD3B3E6C120971AECFBC058,
	HomeMapUiManager_get_TaskPlanel_mA27E4CB52BD0F9E8C5A597726431752DF86D2429,
	HomeMapUiManager_get_DataPlanel_mA7ABA6369996BEC80191FCC39CC737839EC0889C,
	HomeMapUiManager_get_MapHome_m1CB2A792AFEE02CBC95C1333D8BA530F78B2AB63,
	HomeMapUiManager_get_ComparePlayerDataPlanel_mF3E3CACA8F5248206F85ED4C064F64D888018DA3,
	HomeMapUiManager_Configur_mBB120FBB4D65D7AB49595123100601FE279DE4BC,
	HomeMapUiManager_OpenLevelInfoPlane_m6FEC86A7DF55612795CA96B3F437DB23C5F32D27,
	HomeMapUiManager_CloseLevelInfoPlane_mE668DAD4ADC2C6B550B4C032C0C7641AAFE7D217,
	HomeMapUiManager_OpenLoackPlane_m795299FD88C8EEA255678090A113456C70B04704,
	HomeMapUiManager__ctor_mAAB28BF2CF1BDF372433452739F2EB90EBBB0497,
	StartHomeManager_Awake_mF4BAB324091D29A81DB26D75490B02FBC475A0C2,
	StartHomeManager_Start_m0FFC27D888A8A667DAB08D991A7AF385434D5CB8,
	StartHomeManager_LoadConfigur_mA16AF5BBF073B100EF6CE7418B46AB584EDE4B1B,
	StartHomeManager_Update_m57051F4666E1728774C9036D7822471025F3332E,
	StartHomeManager__ctor_m8ABF4F2ED83A4AA1DD815B0918A3810A8012E135,
	U3CLoadConfigurU3Ed__6__ctor_m6B578AE1CB8435E7F81B9AD301D7AA2F06909A70,
	U3CLoadConfigurU3Ed__6_System_IDisposable_Dispose_mF7A940081B711D3DAFE59CEB06AD1483BF8C81FB,
	U3CLoadConfigurU3Ed__6_MoveNext_mB0B2E5E9ED2302427EA02F00BD9A15E310A2FD41,
	U3CLoadConfigurU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE660A9CA9D6786739A3A9A126D5FED1D6249760A,
	U3CLoadConfigurU3Ed__6_System_Collections_IEnumerator_Reset_m60E3A693D3CDD12BF3EAF39A330E993D6D7F7880,
	U3CLoadConfigurU3Ed__6_System_Collections_IEnumerator_get_Current_mFFB428DEBC1380397859BB1ED740A450C71E4CBD,
	UiMapPlane_Init_mB8385523C840783CA14BC2597172B16E9509B255,
	UiMapPlane_InitData_mE57C7EB1FD2820C459CE2124BEF525EE2338FD2C,
	UiMapPlane_UpdataShowData_m18DCE3F61517BE776FD882E7D2A6409A048682B4,
	UiMapPlane_ClickLevel_m397C87CF748425BFBA83B821A56D3E472826F6F3,
	UiMapPlane_ClickLock_mBCA13AF529F36B561D643D7A0FA4DC77076D2F71,
	UiMapPlane_GuideClick_mD649FF28B2416F71500A3CC0F31649D9D1CCEA6D,
	UiMapPlane_CallCloseLevelInfoDlg_m518F4F5752F2BABB11435599F641A7EFB1F62854,
	UiMapPlane__ctor_m831F4D13538649DF1D420B4938D6FBE45AE13DEC,
	U3CInitDataU3Ed__8__ctor_m4F4589C160781079654D48B05987E7ECCADD4089,
	U3CInitDataU3Ed__8_System_IDisposable_Dispose_m98B33BF7FB0B7080D036D3D1DDAC681E23154A8E,
	U3CInitDataU3Ed__8_MoveNext_m77752F32A951C03E78CA9DBE2A5B954E83680202,
	U3CInitDataU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m74C02834BF39C792446726C0B904EF78BFFF1D59,
	U3CInitDataU3Ed__8_System_Collections_IEnumerator_Reset_m178945C942CF77F638FA54C05FE9A2B458428E09,
	U3CInitDataU3Ed__8_System_Collections_IEnumerator_get_Current_mB94218612EB5FF22597A0D35ED5CA062D8C15DC2,
	ComparePlayerDataPlanel_Confgur_m9E61BD3600F9433415D4A25B162714B31DC86FF9,
	ComparePlayerDataPlanel_SetShowDaata_m1290B7F6B571BA81306670D7EBFF2883E4EB0F88,
	ComparePlayerDataPlanel_OnClose_m183C409F33C410F36184984FFD1D0CF097908655,
	ComparePlayerDataPlanel_OnClickLocadData_m13227A5716C5FFB4CAB194C68ED782979F1EE89C,
	ComparePlayerDataPlanel_OnClickCloundData_m46DF3ECFBDBC0192810D5260CEAD315B0D1B5453,
	ComparePlayerDataPlanel_SaveCloundData_m456C7309F0A7275F30F20448CE61772C44A6D785,
	ComparePlayerDataPlanel__ctor_m48E172E133E168FEDAA0358160867B79AC4122DB,
	U3CSaveCloundDataU3Ed__9_MoveNext_mACD985869A31304DC796854120D03455D811A674,
	U3CSaveCloundDataU3Ed__9_SetStateMachine_m06D2A0150AC5B1EEBA00854CEFA5A7B0CB4240DC,
	DamondPlane_Confgur_m6E0044FC1D816145A036943156FE10DB0A731D29,
	DamondPlane_OnClickClose_m5A8B482A0F67F196544B98D84847E3181BCB1DFB,
	DamondPlane__ctor_mF43248ED94782CA062CBC33F6C404D470BAE77E4,
	DataPlanel_Confgur_mA4201119D5B890BDBFD88EBB8C0B7F52EF74ACB8,
	DataPlanel_UpdateShowData_mC7EE42CD6682184D00F4AF2382F96B0DA31C84E3,
	DataPlanel__ctor_mFC620E95F967E54730BF0203D249C635F4B426A1,
	GameHomePlanel_Start_mF16E53D69797D33444746A83286837F307DA69A7,
	GameHomePlanel_OpenUiWo_m623CDF13352A1092D3A05D3E37BF5F06DFD17714,
	GameHomePlanel_CloseUiWo_m23D8B67F778AF91D91F507919DB02D981C9D3D27,
	GameHomePlanel_SetTraget_m45B4650F06FAE261EF1C903C82A5E67B3BA631E2,
	GameHomePlanel__ctor_mF85290FF7F7DBA547B203AE122A9B9F91D5B8E72,
	HeadItem_InteHeadItem_mE8D9B76C86B2D6AA17796B05C95964E136E3ACB2,
	HeadItem_OnClick_mE55DC2E32572600606E2AF8B28112C0E64CF3B2E,
	HeadItem__ctor_m64B1F1094F0E8ED92C5E8EEA6632EA2D54433325,
	HeadPlanel_OnClickClose_m8D3ECD9E88FE1B6A3B2AE340C7BDC1042793D077,
	HeadPlanel_get_CurrHeadData_mC2748CC0D8D6AC00C9ED68D9108F36CD19D6EB4C,
	HeadPlanel_set_CurrHeadData_m2B8AA255D097D31A482A270D598A66A1FF7BEB50,
	HeadPlanel_Confgur_m723C5570D77B8CAE3E76D4ED4834E056ABE3F32D,
	HeadPlanel_Start_mD4528F1FC9A686418C6543FEAA30CA60ADEF0231,
	HeadPlanel_SpeapeHead_m0F0BED4A26780DAFB37AA7509BACC25A2592FE25,
	HeadPlanel_CallSelectHead_m0E433E5C43BFEC5CB8FD972DF585F09CE1AE217F,
	HeadPlanel_OnClickSave_mA5EE650B2B245A2BE562F31B228450F5144CB098,
	HeadPlanel__ctor_mE10C8F72E6D7773E5DFA306029AF2A04B4B22EC3,
	HomePlanel_Confgur_mBAA429069ED4A5CC990D250B3A61B3E8C04AC7C0,
	HomePlanel_Init_m8B8CA35CDB15181FCF03E54CE0209F9611B6FBC2,
	HomePlanel_OnClickDanmondPlanel_m330C0AE2B7CD4360E64F74016119E537D1417DDB,
	HomePlanel_OnClickHeadPlanel_m14872D80E1BCECA85BBBDA45E8B9CD8AE7F1E015,
	HomePlanel_OnClickShoppPlanel_m009150F205B37CE09B01954AB9403D4A00709B0B,
	HomePlanel_OnClickTaskPlanel_m466E58DB13A71A658BDFC1CB31E40DD89F0A76AC,
	HomePlanel_SetHeadImage_mC731464C4FFCBF9B7E7AE853B68B5E5559F84AA5,
	HomePlanel__ctor_mE723F4468513DB38738F426C31BD958469B3624A,
	LevelInfoPlane_Init_mE5D317CE4F5080028CE5CCC115E1A5AC8BAFF9CF,
	LevelInfoPlane_IntDlg_mCB889BB23E0B0CB722BCD37CCC9D5F9C39B6E4F8,
	LevelInfoPlane_OpenUiWo_m8D275657E92D6991F60EBAE22F0F80220384A321,
	LevelInfoPlane_CloseUiWo_m99D6D7CB4D0AE6416AA3D7CF16CBE4C99102C84F,
	LevelInfoPlane_OpenInit_m9919CC9C1C0917C067F4314F833F51B2A8213F5D,
	LevelInfoPlane_ClickStartGame_m03FFD0305504CCCF66070FF14258A0477828CB20,
	LevelInfoPlane_ClickExist_m51E2C80F4B92F1748D53A160FBB66294DF56D6CC,
	LevelInfoPlane_InitTipDlgOne_mACF91FE2A8F2CE6044C0BFE79088EF609A0AD253,
	LevelInfoPlane_OnClilkUpGread_m60688541F69DBCAF5DE8CF6B9606ED114534D547,
	LevelInfoPlane_SetShowLevelText_m3FA6558891A3340E88FAD457B30CD258EA2459D5,
	LevelInfoPlane__ctor_m58BC9F097EA795757FE834BE265986036900A6EF,
	LevelSelect_get_SeLectLevel_mA2A185047C3A0EB8C9DD698F7009DA58811DD7D3,
	LevelSelect_set_SeLectLevel_m42E004163654537BE57EC9E211582CF7ADFC8136,
	LevelSelect_get_LevelPlanel_m8B993CBCC45CB2DA6CDCC75F71E0BF8E9FFE57CA,
	LevelSelect_set_LevelPlanel_m32F398E9D946DE09682F79A12D3ECDDB15A087B2,
	LevelSelect_OnCklic_m39AE82A08DE01C9A21D57067FD16EFDA1ADF7DFA,
	LevelSelect__ctor_m0D5B63D5D849E01B4536B330A09209A7DE744D4F,
	LoadinPlanle_Start_mE3F5AA67E1E540A8C93E8B0EC73CE9CCDC50702C,
	LoadinPlanle_OnEnable_m6C32616F1A602C600867A7201A8B320EC1B19ED9,
	LoadinPlanle_OnDisable_mBBA51CCB69E4420F81B92F3A2335D47328E70DDF,
	LoadinPlanle_OpenUiWo_m273B7E60778F6A750FA81B457EC26BDCDFA9E4D5,
	LoadinPlanle_CloseUiWo_m1590049A6EFD9A44814BAF3574DC9D0B2B514525,
	LoadinPlanle_Update_m62E1AF1DD77D86DE8266A31F5ABFA34FDF120425,
	LoadinPlanle__ctor_mAC765036BE7D0A875EA48096340B6B3757A747C2,
	lockPlanel_ClickExist_m178557665F7775CD5D97417A5EB52063C5E75F50,
	lockPlanel__ctor_m63CF43E8DF22400A9E4A113248428CC04D1FC386,
	OverPlanel_UiConfigur_mEB971B3A22CB62EB1DFFC0201858A6211320D8B5,
	OverPlanel_OpenUiWo_mC75E528DEBA36CE98CFEF57EE31D8D5A84FA19B5,
	OverPlanel_CionScore_mC43A7058B64A75452B4B7D8A27B86F1756CD98C7,
	OverPlanel_CloseUiWo_m1B1C17F17F5E85EF82A4124040386BC5E235ECBD,
	OverPlanel_OnClickContinue_mFEC5779EF998788DB66645E607B3EF3D8B508B80,
	OverPlanel__ctor_mCA387785AA5FCB06BCFDB419D5760E3D1F224353,
	U3CCionScoreU3Ed__4__ctor_m80379E91AF4A1F7C9DD02BF943464652219FDFE2,
	U3CCionScoreU3Ed__4_System_IDisposable_Dispose_mACA7CF4D22831DD7486A058947FF13E53C7D3F24,
	U3CCionScoreU3Ed__4_MoveNext_m237BB619023BA47D7340EA92C1477325C3ED5389,
	U3CCionScoreU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD904D52B0C1B44DD5E37BD306820C5ADD7E5187,
	U3CCionScoreU3Ed__4_System_Collections_IEnumerator_Reset_mF7513A9877E648A3600BF8A9D13226C1D2462EA2,
	U3CCionScoreU3Ed__4_System_Collections_IEnumerator_get_Current_mB51AD0111929402688106ED3799322FC60A77962,
	ShoppPlanel_InitWo_mAB5C9F16E68C3F5EBA1990B066DF2A14EAB86DBD,
	ShoppPlanel_OnClickShowBut_mD3B2B6682970B9EAF6DF70995E05FCE5EFB8D35D,
	ShoppPlanel_InteShop_m5F2BB04777C40BE52B5B0EB236536FD4BB829993,
	ShoppPlanel_OpenUiWo_mF1BAC82D48C1A2EEBF230783EA2889880F071374,
	ShoppPlanel_OnClickClose_mB5B4F5B9D00A9B7D8AC65896BE8C59D407264BA1,
	ShoppPlanel_OpenEnd_m4483104F0683B7486C1A055144003E8310763307,
	ShoppPlanel_CreateShopItem_mE7C889B7379EF9B493C2A7B52FF08701E644E217,
	ShoppPlanel_RestItemProp_mEE4141AFE96F3E051EF1500E41DA132662616C8B,
	ShoppPlanel_ShowItemProp_m9597457A674D7A81C7368067E9A7FE75B47DD92F,
	ShoppPlanel_RestGemItem_m151EBBA9310B0DDC19598CEAEA79CD60B5641760,
	ShoppPlanel_ShowItemGem_m2081330CBB337A386F76CDE366C780C74B2B9552,
	ShoppPlanel__ctor_mDBF79CA44B1B047189683245466C172D62B917D1,
	U3CShowItemPropU3Ed__15__ctor_mD7DCCE94780DB0BB6A5B25352D17721F436505AC,
	U3CShowItemPropU3Ed__15_System_IDisposable_Dispose_m1FACC6ECF1DC29083D20B21BC79511B825296BC0,
	U3CShowItemPropU3Ed__15_MoveNext_mB10CB3D693F8A43204317CADC940F3548201F411,
	U3CShowItemPropU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC2EC1B7C197E146A0638F96DF752605E4132C37,
	U3CShowItemPropU3Ed__15_System_Collections_IEnumerator_Reset_m7F8A09787537263A9A48C9ADDEDC5E192ABCAA94,
	U3CShowItemPropU3Ed__15_System_Collections_IEnumerator_get_Current_m0167222C7C6FF0DE5C400F7CEEF968677F0A2F3D,
	U3CShowItemGemU3Ed__17__ctor_m426C867B155BCA30C02F9BAFC6B76164657D28B9,
	U3CShowItemGemU3Ed__17_System_IDisposable_Dispose_mF92EAC0E0F3E7FAFBA216CC8E0ABF44A892F0943,
	U3CShowItemGemU3Ed__17_MoveNext_m583A0806C428067B786C408AE6475DBF1414CE5C,
	U3CShowItemGemU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC5F55885EF4674C0B057225F61D7EEC67EEB4507,
	U3CShowItemGemU3Ed__17_System_Collections_IEnumerator_Reset_m137A415D3C1012DD01B47D70F8AEA763E29B5EAD,
	U3CShowItemGemU3Ed__17_System_Collections_IEnumerator_get_Current_m81AF1EA687AB5CB6AF8349AD18270FAE52D4DB9D,
	TargetPlanle_OpenUiWo_m2D953FC97557AA6C08F24000709E6A9EFB8D0CEC,
	TargetPlanle_CloseTarget_mB5E276228CEE597029BCE6FF70E53B34AD6E332D,
	TargetPlanle_CloseUiWo_m25B260D268DED319814CBED09AC6A1700EBEE021,
	TargetPlanle_OnEnable_m7A418B817C1907234007812CB00802971D79D2D1,
	TargetPlanle_OnDisable_mDDF3E498711FD1B139D38BDABDDA051D66A7E022,
	TargetPlanle_SetTragetValue_m98794229AD586B79D0884321623139AA121DDD5D,
	TargetPlanle__ctor_mBDAEFDAE1F3EF58318C47747DD75F29D929DF7A2,
	TargetPlanle_U3COpenUiWoU3Eb__3_0_mD735279E1ADCB34D62665343502201AE5A631748,
	TargetPlanle_U3CCloseTargetU3Eb__4_0_m2C133333A1AEA778813D5547B3D57B8E03F47E57,
	U3CCloseTargetU3Ed__4__ctor_mF80142F891816CD667601530C206979866D49789,
	U3CCloseTargetU3Ed__4_System_IDisposable_Dispose_mE3FBD3C178D7045B5F0CB50B09A60D6D3868B6BD,
	U3CCloseTargetU3Ed__4_MoveNext_mFD061711DD38BCC8D1F4672A0629A4425560F914,
	U3CCloseTargetU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BAF61E67BE70012731868F9795D8B9A0E33DA00,
	U3CCloseTargetU3Ed__4_System_Collections_IEnumerator_Reset_mE448CD797AB70D0EF76047A43109947A52D998C8,
	U3CCloseTargetU3Ed__4_System_Collections_IEnumerator_get_Current_m0502E9D81E981F4CC5CA22CBBA75110B34B0DD18,
	TaskItem__ctor_m56CDBC9FF5F2FECD1D9F05238639BAE0F70D999D,
	TaskPlanel_InitWo_m186951AF56B975DB392D7107066CF299C43B79AB,
	TaskPlanel_OnClickClose_m917FB168C8F5E42E17FC026183458610BD0CE218,
	TaskPlanel_CreaAchieveTaks_m7D20628DE8CE750379B828A158100DB19F83EA8F,
	TaskPlanel_OnClickBtn_mAC133A0302D8168ED42074471BC9E90D2D20CDB8,
	TaskPlanel_ResetTask_m4BC95B96E24733C69F8E8FB0A238AE58537E0101,
	TaskPlanel__ctor_m986B5603FEE7E3ECF9672412E1B1DA7BBDED3A9C,
	U3CCreaAchieveTaksU3Ed__6__ctor_m2E48CA8D75C667CCD99565680623CA11964E0734,
	U3CCreaAchieveTaksU3Ed__6_System_IDisposable_Dispose_mEF03D66F151B3978DC7120463198BD66DDA87091,
	U3CCreaAchieveTaksU3Ed__6_MoveNext_mDE62DF17EC939BD334BADBFB03F0458578C5D5D1,
	U3CCreaAchieveTaksU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m47F8A9CDD5AAB320EC6F4717D86DACAF215B28E9,
	U3CCreaAchieveTaksU3Ed__6_System_Collections_IEnumerator_Reset_m32B360F2FD948ACA57B23CF106B014E7FA81FAD7,
	U3CCreaAchieveTaksU3Ed__6_System_Collections_IEnumerator_get_Current_m5F89F63697A111978448F91ED5B46E47FF20AE52,
	UiLoadIngSlider__ctor_m7DBB5BA9AAF7D84B15E03DB5783FDBAF2304AD44,
	UiWoPropAbility_get_TargetGroup_m2CFF9DC8FE3A8834A8B4E64EE34F438733ED6683,
	UiWoPropAbility_set_TargetGroup_mC0C6B9C0FBB9BECCEF1BB43B727DF42A5796470E,
	UiWoPropAbility_get_ChangeTime_m32CC18DF71D53B4D86BA8CA371EDCF68DE34196D,
	UiWoPropAbility_set_ChangeTime_m3646EFB665CAEFB8A2DC3AE4E20322B1D879C04B,
	UiWoPropAbility_get_SystemGameManager_m5B947ABA36EEBEE9930007A80CDC82F4E9B4CBCE,
	UiWoPropAbility_set_SystemGameManager_m1FA14FFA50972355FDF9935A6DB533D5D66EEAEF,
	UiWoPropAbility_Configur_mD2A1263D8AA35EBDA50BE2E81F965CC71491461B,
	UiWoPropAbility_Start_m452A76E7AA8125956FB2F4609C807BD3BF142424,
	UiWoPropAbility_OpenUiWo_m44E48CAE27D852D72C0621858314F749F14526BB,
	UiWoPropAbility_CloseUiWo_m0F18DA106F9973577EC0327FC77496FF03F5A72F,
	UiWoPropAbility_SetUiEffect_m59B53448F3412F0942B98B214F5294F583FD14DC,
	UiWoPropAbility_InitWo_m1828FC238DC42F435B42E65112CB9A6A39C320D3,
	UiWoPropAbility_OpenEnd_mE3FA9799500DD7E52F9C63FB6E22CECC331A343B,
	UiWoPropAbility__ctor_m19DAA4ECB7C46DB867AB99C53B205B73409EC6FB,
	UiWoPropAbility_U3CSetUiEffectU3Eb__17_0_mCF74BD82B3F35F7EDB222ED0B54A3B52E7505EE8,
	UiWoPropAbility_U3CSetUiEffectU3Eb__17_1_mF5217FFE8B514D61FF7C236E0E6342CAA0810D43,
	UIText_Start_m938B8C6E726152E3D5F8AFE68198AAB2D33E4B3D,
	UIText_clickOpen_mA281DFD1E42C87EA6E1635629BC285C1E989C7A1,
	UIText__ctor_mD8921B9D29FA52E93E49473F0E232C21223B5667,
	BuildinFileManifest__ctor_mBF7538337B56D8286696B71A381EAA4F71D2C37F,
	StreamingAssetsDefine__ctor_m24F6603DC5D25F321EA1E8E0CE58118D7513F764,
	GameQueryServices_QueryStreamingAssets_m9A5CACD2BCA2B44B6AA7BD134D672339C1B4FC12,
	GameQueryServices__ctor_m075C10A435857E061FE78F8BD293A6CF55EC105C,
	StreamingAssetsHelper_Init_m5FE865BDAEBFFA2604FFFAA940A1A637BC3FDD23,
	StreamingAssetsHelper_FileExists_m5580EDFD4FA0EAA5D08E6B3486B5BF8136860B57,
	StreamingAssetsHelper__ctor_m48EF042D675EBAA22A67CA435AA84396E03D8282,
	StreamingAssetsHelper__cctor_mBEFB5C410D84FF5E45D9C429A5C86803771BD080,
	YooResoursceManagur_get_Instace_mF273E0E72BD4555D79D941F284AD4120F5BD083A,
	YooResoursceManagur_InitYoo_mFA78C2CCF4910B0C21A83EFD1EF8A2BD1275FDD9,
	YooResoursceManagur_InintCuble_m50675D154C18CC361B127C4541E015DEEDDEAED5,
	YooResoursceManagur__ctor_mF5BE416EDB5E508149540A53DC4168A9F1DC3384,
	U3CInintCubleU3Ed__6__ctor_m8B92F5BD83B46A28A147FC2BDE1B3E5095884628,
	U3CInintCubleU3Ed__6_System_IDisposable_Dispose_m7948C9CA8843B5EE76713C0903D64F769445F17D,
	U3CInintCubleU3Ed__6_MoveNext_mC91222FAA0F8AAEEF9ADFA8BC77BD9A09FF5FC43,
	U3CInintCubleU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB723C0B18AB92A430D1494C005E23D1EF68BE8BD,
	U3CInintCubleU3Ed__6_System_Collections_IEnumerator_Reset_mE1F5F072B88223134146CAC582CFFD746A8F0267,
	U3CInintCubleU3Ed__6_System_Collections_IEnumerator_get_Current_m32B7F3F71FECA267377BB8BB803DF4E28AF82372,
	RemoteServices__ctor_m9C9D2E5D7B003C687EF7B42526DC595B9E5A27A5,
	RemoteServices_YooAsset_IRemoteServices_GetRemoteMainURL_m80A98B59950800895374F5940DA72C4F3881FAEC,
	RemoteServices_YooAsset_IRemoteServices_GetRemoteFallbackURL_m956EE47AEE6B58E35FC33EB3B9834887AFA39588,
};
extern void U3CAwakeU3Ed__1_MoveNext_m2073F4CF637E0510E977A773753398EC37A950AB_AdjustorThunk (void);
extern void U3CAwakeU3Ed__1_SetStateMachine_mEA3B34C59FBB01ACB78902CEFB0AD8C46349BF60_AdjustorThunk (void);
extern void U3CListAllKeysU3Ed__3_MoveNext_mF06AF0AE486327A68DADF1C848857A23656D0C64_AdjustorThunk (void);
extern void U3CListAllKeysU3Ed__3_SetStateMachine_m235973254ED93FF7B9B529DC584277B39342A146_AdjustorThunk (void);
extern void U3CForceSaveSingleDataU3Ed__4_MoveNext_mA214175827119FE72D7D121464A09F4E6107DBC8_AdjustorThunk (void);
extern void U3CForceSaveSingleDataU3Ed__4_SetStateMachine_m9CACD98A7A027DCAD32BD77D301143845CB04033_AdjustorThunk (void);
extern void U3CSaveCloundDataU3Ed__31_MoveNext_m5CD46301F4D3D42310CBB8F6551B53F5D9DDFD6F_AdjustorThunk (void);
extern void U3CSaveCloundDataU3Ed__31_SetStateMachine_m4CEC5F75738743AAFFE9AECB9893C38431C69113_AdjustorThunk (void);
extern void U3CSaveCloundDataU3Ed__9_MoveNext_mACD985869A31304DC796854120D03455D811A674_AdjustorThunk (void);
extern void U3CSaveCloundDataU3Ed__9_SetStateMachine_m06D2A0150AC5B1EEBA00854CEFA5A7B0CB4240DC_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[10] = 
{
	{ 0x06000011, U3CAwakeU3Ed__1_MoveNext_m2073F4CF637E0510E977A773753398EC37A950AB_AdjustorThunk },
	{ 0x06000012, U3CAwakeU3Ed__1_SetStateMachine_mEA3B34C59FBB01ACB78902CEFB0AD8C46349BF60_AdjustorThunk },
	{ 0x06000013, U3CListAllKeysU3Ed__3_MoveNext_mF06AF0AE486327A68DADF1C848857A23656D0C64_AdjustorThunk },
	{ 0x06000014, U3CListAllKeysU3Ed__3_SetStateMachine_m235973254ED93FF7B9B529DC584277B39342A146_AdjustorThunk },
	{ 0x06000015, U3CForceSaveSingleDataU3Ed__4_MoveNext_mA214175827119FE72D7D121464A09F4E6107DBC8_AdjustorThunk },
	{ 0x06000016, U3CForceSaveSingleDataU3Ed__4_SetStateMachine_m9CACD98A7A027DCAD32BD77D301143845CB04033_AdjustorThunk },
	{ 0x0600018E, U3CSaveCloundDataU3Ed__31_MoveNext_m5CD46301F4D3D42310CBB8F6551B53F5D9DDFD6F_AdjustorThunk },
	{ 0x0600018F, U3CSaveCloundDataU3Ed__31_SetStateMachine_m4CEC5F75738743AAFFE9AECB9893C38431C69113_AdjustorThunk },
	{ 0x060001E8, U3CSaveCloundDataU3Ed__9_MoveNext_mACD985869A31304DC796854120D03455D811A674_AdjustorThunk },
	{ 0x060001E9, U3CSaveCloundDataU3Ed__9_SetStateMachine_m06D2A0150AC5B1EEBA00854CEFA5A7B0CB4240DC_AdjustorThunk },
};
static const int32_t s_InvokerIndices[651] = 
{
	3572,
	1766,
	1576,
	1766,
	3572,
	1766,
	1576,
	1576,
	1576,
	1766,
	1766,
	1766,
	1708,
	979,
	0,
	1766,
	1766,
	1576,
	1766,
	1576,
	1766,
	1576,
	0,
	0,
	1766,
	1708,
	1708,
	1766,
	1766,
	1766,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1766,
	1158,
	1576,
	1571,
	1766,
	1708,
	1576,
	1708,
	1576,
	1698,
	1571,
	1766,
	1576,
	1766,
	1766,
	1708,
	1766,
	1766,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1708,
	1576,
	1766,
	1576,
	1766,
	1766,
	1766,
	1576,
	1766,
	1766,
	465,
	1766,
	1766,
	1766,
	1766,
	1708,
	1576,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1576,
	1766,
	1766,
	3341,
	3341,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1708,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1576,
	1766,
	1766,
	1766,
	1708,
	1576,
	1766,
	1766,
	1766,
	1766,
	1698,
	1571,
	1766,
	1766,
	1576,
	1576,
	1576,
	1576,
	1576,
	1332,
	1154,
	1457,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1576,
	1766,
	1766,
	1708,
	1698,
	1766,
	1766,
	1766,
	1127,
	1314,
	1571,
	1766,
	1708,
	1668,
	1158,
	1158,
	447,
	1158,
	1766,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1766,
	1766,
	1766,
	1766,
	1766,
	1158,
	1766,
	1698,
	1571,
	1708,
	1576,
	1708,
	1708,
	1766,
	1708,
	1766,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1571,
	1766,
	1668,
	1766,
	1708,
	1766,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	3522,
	1457,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1766,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1576,
	1766,
	1766,
	1766,
	1708,
	1576,
	1766,
	1766,
	1708,
	1766,
	1158,
	1158,
	1158,
	1158,
	1766,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1766,
	3543,
	3487,
	3572,
	1708,
	1766,
	1766,
	3572,
	1766,
	3572,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1708,
	1576,
	1766,
	1708,
	1576,
	1457,
	3522,
	1708,
	1457,
	1766,
	1457,
	1766,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1576,
	3131,
	3131,
	3131,
	3572,
	1766,
	3572,
	1708,
	1698,
	1571,
	1708,
	1576,
	1708,
	1708,
	1708,
	1708,
	1766,
	1766,
	1708,
	1766,
	1708,
	1766,
	1708,
	1766,
	1766,
	1766,
	1766,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1766,
	1576,
	1766,
	1576,
	1766,
	1766,
	1766,
	1766,
	1766,
	1708,
	1766,
	1766,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1766,
	1766,
	1304,
	1121,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1708,
	1708,
	1576,
	1593,
	1766,
	1593,
	1708,
	1731,
	1766,
	823,
	1766,
	1766,
	1708,
	1576,
	1766,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1576,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1708,
	1766,
	1766,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1576,
	1708,
	1766,
	1571,
	1766,
	1766,
	1766,
	1766,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1576,
	1576,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1576,
	1576,
	1766,
	1766,
	1576,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1158,
	1766,
	1766,
	1766,
	1708,
	1576,
	1576,
	1766,
	1766,
	1576,
	1766,
	1766,
	1576,
	1766,
	1766,
	1766,
	1766,
	1766,
	1576,
	1766,
	1576,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1571,
	1571,
	1571,
	1766,
	1698,
	1571,
	1708,
	1576,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1576,
	1766,
	1708,
	1766,
	1766,
	1766,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1766,
	1571,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1708,
	1766,
	1708,
	1766,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1766,
	1708,
	1766,
	1766,
	1766,
	1593,
	1766,
	1766,
	1766,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1766,
	1766,
	1766,
	1708,
	1571,
	1766,
	1766,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1766,
	1708,
	1576,
	1731,
	1593,
	1708,
	1576,
	1576,
	1766,
	1766,
	1766,
	1543,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	824,
	1766,
	3572,
	2817,
	1766,
	3572,
	3543,
	3487,
	1708,
	1766,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1158,
	1457,
	1457,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x02000009, { 3, 1 } },
	{ 0x0600000F, { 0, 3 } },
};
extern const uint32_t g_rgctx_U3CForceSaveObjectDataU3Ed__5_1_tEE76853785E076167E299D600941467EDCFDA7CE;
extern const uint32_t g_rgctx_AsyncTaskMethodBuilder_Start_TisU3CForceSaveObjectDataU3Ed__5_1_tEE76853785E076167E299D600941467EDCFDA7CE_mDBFE6172FBD16B6CE5CEB915458E90E1DAB9748F;
extern const uint32_t g_rgctx_U3CForceSaveObjectDataU3Ed__5_1U26_tCA7C898DD9B50BCB08CD595BA11128C853962ACA;
extern const uint32_t g_rgctx_U3CForceSaveObjectDataU3Ed__5_1_t2C50E18DBC4F2301AC384296D698438773FB5D10;
static const Il2CppRGCTXDefinition s_rgctxValues[4] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CForceSaveObjectDataU3Ed__5_1_tEE76853785E076167E299D600941467EDCFDA7CE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_AsyncTaskMethodBuilder_Start_TisU3CForceSaveObjectDataU3Ed__5_1_tEE76853785E076167E299D600941467EDCFDA7CE_mDBFE6172FBD16B6CE5CEB915458E90E1DAB9748F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CForceSaveObjectDataU3Ed__5_1U26_tCA7C898DD9B50BCB08CD595BA11128C853962ACA },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CForceSaveObjectDataU3Ed__5_1_t2C50E18DBC4F2301AC384296D698438773FB5D10 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	651,
	s_methodPointers,
	10,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	4,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
