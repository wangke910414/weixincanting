﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>


struct VirtualActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct VirtualActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtualFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtualFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct VirtualFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};

// System.Collections.Generic.Dictionary`2<System.Int32,System.Text.Encoding>
struct Dictionary_2_t87EDE08B2E48F793A22DE50D6B3CC2E7EBB2DB54;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t4C228DE57804012969575431CFF12D57C875552D;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// System.Type[]
struct TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB;
// b/c[]
struct cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E;
// b/d[]
struct dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066;
// c/a[]
struct aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2;
// c/c[]
struct cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF;
// System.Attribute
struct Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA;
// System.IO.BinaryReader
struct BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158;
// System.IO.BinaryWriter
struct BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E;
// System.Reflection.Binder
struct Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t52460FA30AE37F4F26ACB81055E58002262F19F2;
// System.Text.Decoder
struct Decoder_tE16E789E38B25DD304004FC630EA8B21000ECBBC;
// System.Text.DecoderFallback
struct DecoderFallback_t7324102215E4ED41EC065C02EB501CB0BC23CD90;
// DotfuscatorAttribute
struct DotfuscatorAttribute_t90A6A71AB7691DF0BFCD2AC365104BF68425B9E7;
// System.Text.Encoder
struct Encoder_tAF9067231A76315584BDF4CD27990E2F485A78FA;
// System.Text.EncoderFallback
struct EncoderFallback_tD2C40CE114AA9D8E1F7196608B2D088548015293;
// System.Text.Encoding
struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095;
// System.Reflection.MemberFilter
struct MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553;
// System.IO.MemoryStream
struct MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t0D5CB5685D9BFA5BF95CEC6E7395490F933E8DB2;
// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE;
// System.String
struct String_t;
// System.Type
struct Type_t;
// Unity.FontABTool.UnityFontABTool
struct UnityFontABTool_t29C54DB12CF17A7849B7DA9EA1D87475ADA05FFA;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// b
struct b_t972FAFF471676E1BF6780CEF716F7509668D82DA;
// c
struct c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t0821BF49EE38596C7734E86E1A6A39D769BE2C05;
// c/c
struct c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0;

IL2CPP_EXTERN_C RuntimeClass* BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* b_t972FAFF471676E1BF6780CEF716F7509668D82DA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeField* d_t7410D4303AF1B382C8A19061C6DA00D2360AB7AC____a_0_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* d_t7410D4303AF1B382C8A19061C6DA00D2360AB7AC____b_1_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* d_t7410D4303AF1B382C8A19061C6DA00D2360AB7AC____c_2_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* d_t7410D4303AF1B382C8A19061C6DA00D2360AB7AC____d_3_FieldInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral1937633CE6C19B9FEB3C6F2F3EA9DA5E3D90EE46;
IL2CPP_EXTERN_C String_t* _stringLiteral570DC3936806A5949979D01A37BBAA35EC50DD5F;
IL2CPP_EXTERN_C String_t* _stringLiteral6254C1E893723324192461A9456E4F1C1F5CA6D5;
IL2CPP_EXTERN_C const RuntimeMethod* Type_GetType_m71A077E0B5DA3BD1DC0AB9AE387056CFCF56F93F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityFontABTool_a_m34550A50BEBBEB488C37E50155EBC33CE9C63819_RuntimeMethod_var;
struct a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6;
struct c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0;;
struct c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_com;
struct c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_com;;
struct c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_pinvoke;
struct c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_pinvoke;;

struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E;
struct dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066;
struct aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2;
struct cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t88546BFE60862623CAD588FB9164795ED5BF96FF 
{
};

// System.Attribute
struct Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA  : public RuntimeObject
{
};

// System.IO.BinaryReader
struct BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158  : public RuntimeObject
{
	// System.IO.Stream System.IO.BinaryReader::m_stream
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___m_stream_0;
	// System.Byte[] System.IO.BinaryReader::m_buffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___m_buffer_1;
	// System.Text.Decoder System.IO.BinaryReader::m_decoder
	Decoder_tE16E789E38B25DD304004FC630EA8B21000ECBBC* ___m_decoder_2;
	// System.Byte[] System.IO.BinaryReader::m_charBytes
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___m_charBytes_3;
	// System.Char[] System.IO.BinaryReader::m_singleChar
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___m_singleChar_4;
	// System.Char[] System.IO.BinaryReader::m_charBuffer
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___m_charBuffer_5;
	// System.Int32 System.IO.BinaryReader::m_maxCharsSize
	int32_t ___m_maxCharsSize_6;
	// System.Boolean System.IO.BinaryReader::m_2BytesPerChar
	bool ___m_2BytesPerChar_7;
	// System.Boolean System.IO.BinaryReader::m_isMemoryStream
	bool ___m_isMemoryStream_8;
	// System.Boolean System.IO.BinaryReader::m_leaveOpen
	bool ___m_leaveOpen_9;
};

// System.IO.BinaryWriter
struct BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E  : public RuntimeObject
{
	// System.IO.Stream System.IO.BinaryWriter::OutStream
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___OutStream_1;
	// System.Byte[] System.IO.BinaryWriter::_buffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____buffer_2;
	// System.Text.Encoding System.IO.BinaryWriter::_encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ____encoding_3;
	// System.Text.Encoder System.IO.BinaryWriter::_encoder
	Encoder_tAF9067231A76315584BDF4CD27990E2F485A78FA* ____encoder_4;
	// System.Boolean System.IO.BinaryWriter::_leaveOpen
	bool ____leaveOpen_5;
	// System.Byte[] System.IO.BinaryWriter::_largeByteBuffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____largeByteBuffer_6;
	// System.Int32 System.IO.BinaryWriter::_maxChars
	int32_t ____maxChars_7;
};

// System.Text.Encoding
struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095  : public RuntimeObject
{
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t52460FA30AE37F4F26ACB81055E58002262F19F2* ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_tD2C40CE114AA9D8E1F7196608B2D088548015293* ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t7324102215E4ED41EC065C02EB501CB0BC23CD90* ___decoderFallback_14;
};

// System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE  : public RuntimeObject
{
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject* ____identity_0;
};
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

// Unity.FontABTool.UnityFontABTool
struct UnityFontABTool_t29C54DB12CF17A7849B7DA9EA1D87475ADA05FFA  : public RuntimeObject
{
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;
};

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17 
{
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;
};

// DotfuscatorAttribute
struct DotfuscatorAttribute_t90A6A71AB7691DF0BFCD2AC365104BF68425B9E7  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String DotfuscatorAttribute::a
	String_t* ___a_0;
	// System.Int32 DotfuscatorAttribute::c
	int32_t ___c_1;
};

// System.Int16
struct Int16_tB8EF286A9C33492FA6E6D6E67320BE93E794A175 
{
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.Int64
struct Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3 
{
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

// System.Reflection.MethodBase
struct MethodBase_t  : public MemberInfo_t
{
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE  : public MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE
{
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t0821BF49EE38596C7734E86E1A6A39D769BE2C05* ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t0D5CB5685D9BFA5BF95CEC6E7395490F933E8DB2* ____asyncActiveSemaphore_3;
};

// System.UInt16
struct UInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455 
{
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_0;
};

// System.UInt32
struct UInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B 
{
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;
};

// System.UInt64
struct UInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF 
{
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// a
struct a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C 
{
	// System.UInt32 a::a
	uint32_t ___a_0;
	// System.UInt32 a::b
	uint32_t ___b_1;
	// System.UInt32 a::c
	uint32_t ___c_2;
	// System.UInt32 a::d
	uint32_t ___d_3;
};

// b/a
struct a_tC1CB14265A54FD08AC6F722A13BAD61141D148AF 
{
	// System.UInt32 b/a::a
	uint32_t ___a_0;
	// System.UInt32 b/a::b
	uint32_t ___b_1;
	// System.UInt32 b/a::c
	uint32_t ___c_2;
	// System.UInt32 b/a::d
	uint32_t ___d_3;
	// System.Byte b/a::e
	uint8_t ___e_4;
};

// b/b
struct b_tFB66B901E947622DFAF3E315886F1089C51D6893 
{
	// System.UInt64 b/b::a
	uint64_t ___a_0;
	// System.UInt32 b/b::b
	uint32_t ___b_1;
	// System.UInt32 b/b::c
	uint32_t ___c_2;
	// System.UInt64 b/b::d
	uint64_t ___d_3;
	// System.UInt64 b/b::e
	uint64_t ___e_4;
	// System.UInt64 b/b::f
	uint64_t ___f_5;
	// System.Byte b/b::g
	uint8_t ___g_6;
};

// b/d
struct d_t2D0D74CB19250389E57D70E733F007725D5AA1F2 
{
	// System.UInt64 b/d::a
	uint64_t ___a_0;
	// System.UInt32 b/d::b
	uint32_t ___b_1;
	// System.UInt32 b/d::c
	uint32_t ___c_2;
};

// b/e
struct e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024 
{
	// System.String b/e::a
	String_t* ___a_0;
	// System.Single b/e::b
	float ___b_1;
	// System.Single b/e::c
	float ___c_2;
	// System.Byte[] b/e::d
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___d_3;
	// System.Single b/e::e
	float ___e_4;
	// System.Single b/e::f
	float ___f_5;
};
// Native definition for P/Invoke marshalling of b/e
struct e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024_marshaled_pinvoke
{
	char* ___a_0;
	float ___b_1;
	float ___c_2;
	Il2CppSafeArray/*NONE*/* ___d_3;
	float ___e_4;
	float ___f_5;
};
// Native definition for COM marshalling of b/e
struct e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024_marshaled_com
{
	Il2CppChar* ___a_0;
	float ___b_1;
	float ___c_2;
	Il2CppSafeArray/*NONE*/* ___d_3;
	float ___e_4;
	float ___f_5;
};

// c/a
struct a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6 
{
	// System.UInt32 c/a::a
	uint32_t ___a_0;
	// System.UInt32 c/a::b
	uint32_t ___b_1;
	// System.UInt16 c/a::c
	uint16_t ___c_2;
};

// c/c
struct c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0 
{
	// System.UInt64 c/c::a
	uint64_t ___a_0;
	// System.UInt64 c/c::b
	uint64_t ___b_1;
	// System.UInt32 c/c::c
	uint32_t ___c_2;
	// System.String c/c::d
	String_t* ___d_3;
};
// Native definition for P/Invoke marshalling of c/c
struct c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_pinvoke
{
	uint64_t ___a_0;
	uint64_t ___b_1;
	uint32_t ___c_2;
	char* ___d_3;
};
// Native definition for COM marshalling of c/c
struct c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_com
{
	uint64_t ___a_0;
	uint64_t ___b_1;
	uint32_t ___c_2;
	Il2CppChar* ___d_3;
};

// c/d
struct d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2 
{
	// System.UInt32 c/d::a
	uint32_t ___a_0;
	// c/c[] c/d::b
	cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF* ___b_1;
};
// Native definition for P/Invoke marshalling of c/d
struct d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2_marshaled_pinvoke
{
	uint32_t ___a_0;
	c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_pinvoke* ___b_1;
};
// Native definition for COM marshalling of c/d
struct d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2_marshaled_com
{
	uint32_t ___a_0;
	c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_com* ___b_1;
};

// c/e
struct e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777 
{
	// System.String c/e::a
	String_t* ___a_0;
	// System.UInt32 c/e::b
	uint32_t ___b_1;
	// System.String c/e::c
	String_t* ___c_2;
	// System.String c/e::d
	String_t* ___d_3;
	// System.UInt64 c/e::e
	uint64_t ___e_4;
	// System.UInt32 c/e::f
	uint32_t ___f_5;
	// System.UInt32 c/e::g
	uint32_t ___g_6;
	// System.UInt32 c/e::h
	uint32_t ___h_7;
};
// Native definition for P/Invoke marshalling of c/e
struct e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777_marshaled_pinvoke
{
	char* ___a_0;
	uint32_t ___b_1;
	char* ___c_2;
	char* ___d_3;
	uint64_t ___e_4;
	uint32_t ___f_5;
	uint32_t ___g_6;
	uint32_t ___h_7;
};
// Native definition for COM marshalling of c/e
struct e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777_marshaled_com
{
	Il2CppChar* ___a_0;
	uint32_t ___b_1;
	Il2CppChar* ___c_2;
	Il2CppChar* ___d_3;
	uint64_t ___e_4;
	uint32_t ___f_5;
	uint32_t ___g_6;
	uint32_t ___h_7;
};

// d/a
struct a_tC905F9A47B8F42EC7D8702A0FDD3F296EF74457B 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t a_tC905F9A47B8F42EC7D8702A0FDD3F296EF74457B__padding[46];
	};
};

// d/b
struct b_tF461D840050343BC580EE7A263DE9FE625E47B84 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t b_tF461D840050343BC580EE7A263DE9FE625E47B84__padding[72];
	};
};

// d/c
struct c_t7A9F326BDE1D1BFC4A56A02D80580068ABFC2798 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t c_t7A9F326BDE1D1BFC4A56A02D80580068ABFC2798__padding[7950];
	};
};

// System.IO.MemoryStream
struct MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2  : public Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE
{
	// System.Byte[] System.IO.MemoryStream::_buffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____buffer_4;
	// System.Int32 System.IO.MemoryStream::_origin
	int32_t ____origin_5;
	// System.Int32 System.IO.MemoryStream::_position
	int32_t ____position_6;
	// System.Int32 System.IO.MemoryStream::_length
	int32_t ____length_7;
	// System.Int32 System.IO.MemoryStream::_capacity
	int32_t ____capacity_8;
	// System.Boolean System.IO.MemoryStream::_expandable
	bool ____expandable_9;
	// System.Boolean System.IO.MemoryStream::_writable
	bool ____writable_10;
	// System.Boolean System.IO.MemoryStream::_exposable
	bool ____exposable_11;
	// System.Boolean System.IO.MemoryStream::_isOpen
	bool ____isOpen_12;
	// System.Threading.Tasks.Task`1<System.Int32> System.IO.MemoryStream::_lastReadTask
	Task_1_t4C228DE57804012969575431CFF12D57C875552D* ____lastReadTask_13;
};

// System.Reflection.MethodInfo
struct MethodInfo_t  : public MethodBase_t
{
};

// System.RuntimeFieldHandle
struct RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 
{
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;
};

// System.RuntimeTypeHandle
struct RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B 
{
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;
};

// b
struct b_t972FAFF471676E1BF6780CEF716F7509668D82DA  : public RuntimeObject
{
	// System.Int64 b::d
	int64_t ___d_3;
	// System.IO.BinaryReader b::e
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___e_4;
	// System.IO.BinaryWriter b::f
	BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* ___f_5;
	// b/b b::g
	b_tFB66B901E947622DFAF3E315886F1089C51D6893 ___g_6;
	// b/c[] b::h
	cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E* ___h_7;
	// b/d[] b::i
	dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* ___i_8;
	// b/e b::j
	e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024 ___j_9;
	// System.Byte[] b::k
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___k_10;
};

// d
struct d_t7410D4303AF1B382C8A19061C6DA00D2360AB7AC  : public RuntimeObject
{
};

// b/c
struct c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D 
{
	// System.Int32 b/c::a
	int32_t ___a_0;
	// System.Boolean b/c::b
	bool ___b_1;
	// System.Int16 b/c::c
	int16_t ___c_2;
	// a b/c::d
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C ___d_3;
	// a b/c::e
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C ___e_4;
	// System.UInt32 b/c::f
	uint32_t ___f_5;
	// System.UInt32 b/c::g
	uint32_t ___g_6;
};
// Native definition for P/Invoke marshalling of b/c
struct c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D_marshaled_pinvoke
{
	int32_t ___a_0;
	int32_t ___b_1;
	int16_t ___c_2;
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C ___d_3;
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C ___e_4;
	uint32_t ___f_5;
	uint32_t ___g_6;
};
// Native definition for COM marshalling of b/c
struct c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D_marshaled_com
{
	int32_t ___a_0;
	int32_t ___b_1;
	int16_t ___c_2;
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C ___d_3;
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C ___e_4;
	uint32_t ___f_5;
	uint32_t ___g_6;
};

// c/b
struct b_t377CCBB7B97AADE524489D5766C28A547BBAB31B 
{
	// a c/b::a
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C ___a_0;
	// System.UInt32 c/b::b
	uint32_t ___b_1;
	// c/a[] c/b::c
	aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2* ___c_2;
};
// Native definition for P/Invoke marshalling of c/b
struct b_t377CCBB7B97AADE524489D5766C28A547BBAB31B_marshaled_pinvoke
{
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C ___a_0;
	uint32_t ___b_1;
	a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6* ___c_2;
};
// Native definition for COM marshalling of c/b
struct b_t377CCBB7B97AADE524489D5766C28A547BBAB31B_marshaled_com
{
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C ___a_0;
	uint32_t ___b_1;
	a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6* ___c_2;
};

// System.Type
struct Type_t  : public MemberInfo_t
{
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ____impl_8;
};

// c
struct c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57  : public RuntimeObject
{
	// c/e c::b
	e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777 ___b_1;
	// c/b c::c
	b_t377CCBB7B97AADE524489D5766C28A547BBAB31B ___c_2;
	// c/d c::d
	d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2 ___d_3;
	// System.IO.BinaryWriter c::e
	BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* ___e_4;
};

// <Module>

// <Module>

// System.Attribute

// System.Attribute

// System.IO.BinaryReader

// System.IO.BinaryReader

// System.IO.BinaryWriter
struct BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E_StaticFields
{
	// System.IO.BinaryWriter System.IO.BinaryWriter::Null
	BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* ___Null_0;
};

// System.IO.BinaryWriter

// System.Text.Encoding
struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095_StaticFields
{
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___latin1Encoding_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Text.Encoding> modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Dictionary_2_t87EDE08B2E48F793A22DE50D6B3CC2E7EBB2DB54* ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject* ___s_InternalSyncObject_15;
};

// System.Text.Encoding

// System.String
struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.String

// Unity.FontABTool.UnityFontABTool

// Unity.FontABTool.UnityFontABTool

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Boolean

// System.Byte

// System.Byte

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17_StaticFields
{
	// System.Byte[] System.Char::s_categoryForLatin1
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___s_categoryForLatin1_3;
};

// System.Char

// DotfuscatorAttribute

// DotfuscatorAttribute

// System.Int16

// System.Int16

// System.Int32

// System.Int32

// System.Int64

// System.Int64

// System.Single

// System.Single

// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE_StaticFields
{
	// System.IO.Stream System.IO.Stream::Null
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___Null_1;
};

// System.IO.Stream

// System.UInt16

// System.UInt16

// System.UInt32

// System.UInt32

// System.UInt64

// System.UInt64

// System.Void

// System.Void

// a

// a

// b/a

// b/a

// b/b

// b/b

// b/d

// b/d

// b/e

// b/e

// c/a

// c/a

// c/c

// c/c

// c/d

// c/d

// c/e

// c/e

// d/a

// d/a

// d/b

// d/b

// d/c

// d/c

// System.IO.MemoryStream

// System.IO.MemoryStream

// System.Reflection.MethodInfo

// System.Reflection.MethodInfo

// System.RuntimeFieldHandle

// System.RuntimeFieldHandle

// b
struct b_t972FAFF471676E1BF6780CEF716F7509668D82DA_StaticFields
{
	// System.Byte[] b::l
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___l_11;
};

// b

// d
struct d_t7410D4303AF1B382C8A19061C6DA00D2360AB7AC_StaticFields
{
	// System.Int64 d::a
	int64_t ___a_0;
	// d/c d::b
	c_t7A9F326BDE1D1BFC4A56A02D80580068ABFC2798 ___b_1;
	// d/b d::c
	b_tF461D840050343BC580EE7A263DE9FE625E47B84 ___c_2;
	// d/a d::d
	a_tC905F9A47B8F42EC7D8702A0FDD3F296EF74457B ___d_3;
};

// d

// b/c

// b/c

// c/b

// c/b

// System.Type
struct Type_t_StaticFields
{
	// System.Reflection.Binder modreq(System.Runtime.CompilerServices.IsVolatile) System.Type::s_defaultBinder
	Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235* ___s_defaultBinder_0;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_1;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB* ___EmptyTypes_2;
	// System.Object System.Type::Missing
	RuntimeObject* ___Missing_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterNameIgnoreCase_6;
};

// System.Type

// c

// c
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031  : public RuntimeArray
{
	ALIGN_FIELD (8) uint8_t m_Items[1];

	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// b/c[]
struct cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E  : public RuntimeArray
{
	ALIGN_FIELD (8) c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D m_Items[1];

	inline c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D value)
	{
		m_Items[index] = value;
	}
};
// b/d[]
struct dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066  : public RuntimeArray
{
	ALIGN_FIELD (8) d_t2D0D74CB19250389E57D70E733F007725D5AA1F2 m_Items[1];

	inline d_t2D0D74CB19250389E57D70E733F007725D5AA1F2 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline d_t2D0D74CB19250389E57D70E733F007725D5AA1F2* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, d_t2D0D74CB19250389E57D70E733F007725D5AA1F2 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline d_t2D0D74CB19250389E57D70E733F007725D5AA1F2 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline d_t2D0D74CB19250389E57D70E733F007725D5AA1F2* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, d_t2D0D74CB19250389E57D70E733F007725D5AA1F2 value)
	{
		m_Items[index] = value;
	}
};
// c/c[]
struct cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF  : public RuntimeArray
{
	ALIGN_FIELD (8) c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0 m_Items[1];

	inline c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___d_3), (void*)NULL);
	}
	inline c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0 value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___d_3), (void*)NULL);
	}
};
// c/a[]
struct aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2  : public RuntimeArray
{
	ALIGN_FIELD (8) a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6 m_Items[1];

	inline a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6 value)
	{
		m_Items[index] = value;
	}
};

IL2CPP_EXTERN_C void c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshal_pinvoke(const c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0& unmarshaled, c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_pinvoke& marshaled);
IL2CPP_EXTERN_C void c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshal_pinvoke_back(const c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_pinvoke& marshaled, c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0& unmarshaled);
IL2CPP_EXTERN_C void c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshal_pinvoke_cleanup(c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_pinvoke& marshaled);
IL2CPP_EXTERN_C void c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshal_com(const c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0& unmarshaled, c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_com& marshaled);
IL2CPP_EXTERN_C void c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshal_com_back(const c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_com& marshaled, c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0& unmarshaled);
IL2CPP_EXTERN_C void c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshal_com_cleanup(c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_com& marshaled);


// System.Void System.Attribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attribute__ctor_m79ED1BF1EE36D1E417BA89A0D9F91F8AAD8D19E2 (Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA* __this, const RuntimeMethod* method) ;
// System.Byte[] Unity.FontABTool.UnityFontABTool::a(System.Byte[],System.String,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* UnityFontABTool_a_m34550A50BEBBEB488C37E50155EBC33CE9C63819 (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_A_0, String_t* ___1_A_1, float ___2_A_2, float ___3_A_3, float ___4_A_4, float ___5_A_5, const RuntimeMethod* method) ;
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Application_get_platform_m59EF7D6155D18891B24767F83F388160B1FF2138 (const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2 (RuntimeObject* ___0_message, const RuntimeMethod* method) ;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeHelpers_InitializeArray_m751372AA3F24FBF6DA9B9D687CBFA2DE436CAB9B (RuntimeArray* ___0_array, RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 ___1_fldHandle, const RuntimeMethod* method) ;
// System.Text.Encoding System.Text.Encoding::get_ASCII()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* Encoding_get_ASCII_mCC61B512D320FD4E2E71CC0DFDF8DDF3CD215C65 (const RuntimeMethod* method) ;
// System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Reflection.BindingFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MethodInfo_t* Type_GetMethod_m9E66B5053F150537A74C490C1DA5174A7875189D (Type_t* __this, String_t* ___0_name, int32_t ___1_bindingAttr, const RuntimeMethod* method) ;
// System.Void b::.ctor(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void b__ctor_m5D218ADFB9B12CAEBCFB3E050637FA0BEF33705D (b_t972FAFF471676E1BF6780CEF716F7509668D82DA* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_A_0, const RuntimeMethod* method) ;
// System.Void b::b()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void b_b_m3EEA39E861DB95D43365715F1CC72391418DB524 (b_t972FAFF471676E1BF6780CEF716F7509668D82DA* __this, const RuntimeMethod* method) ;
// System.Byte[] b::a(System.String,System.Byte[],System.Single,System.Single,System.Single,System.Single,System.Reflection.MethodInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* b_a_m83904CAC9196C33E326CF37A434BCF9F4AA1C646 (b_t972FAFF471676E1BF6780CEF716F7509668D82DA* __this, String_t* ___0_A_0, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___1_A_1, float ___2_A_2, float ___3_A_3, float ___4_A_4, float ___5_A_5, MethodInfo_t* ___6_A_6, const RuntimeMethod* method) ;
// System.Void b::a()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void b_a_m0EB9556527D166F142FCB52A806DF275DDE1E6A4 (b_t972FAFF471676E1BF6780CEF716F7509668D82DA* __this, const RuntimeMethod* method) ;
// System.Void c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void c__ctor_mCE86689EC3BFA79682A9DAD7E22DF7B0CE36DD2D (c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57* __this, const RuntimeMethod* method) ;
// System.Byte[] c::a(System.Byte[],System.String,System.String,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* c_a_m1BBA40310965FA9C86772427E8AD85FE919E422F (c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_A_0, String_t* ___1_A_1, String_t* ___2_A_2, uint32_t ___3_A_3, const RuntimeMethod* method) ;
// System.Void c::a()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void c_a_m68DACE5CC586ED73D8A47E1CAF221A5F3BA10E78 (c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57* __this, const RuntimeMethod* method) ;
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void System.IO.MemoryStream::.ctor(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MemoryStream__ctor_m662CA0D5A0004A2E3B475FE8DCD687B654870AA2 (MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_buffer, const RuntimeMethod* method) ;
// System.Void System.IO.BinaryReader::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BinaryReader__ctor_m898732FE0DBEDD480B24F6DE45A9AC696E44CC0F (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_input, const RuntimeMethod* method) ;
// System.Void System.IO.MemoryStream::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MemoryStream__ctor_m8F3BAE0B48E65BAA13C52FB020E502B3EA22CA6B (MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* __this, const RuntimeMethod* method) ;
// System.Void System.IO.BinaryWriter::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BinaryWriter__ctor_mF2F1235E378C3EC493A8C816597BCEB4205A9CA0 (BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_output, const RuntimeMethod* method) ;
// System.Void b::a(System.UInt32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void b_a_m0B288ECDA5A92480B615876EA2B09ECDAFC27335 (uint32_t* ___0_A_0, const RuntimeMethod* method) ;
// System.Void b::a(System.UInt64&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void b_a_mA29C97334753DB15E7A7F615EBCDBD17B8882970 (uint64_t* ___0_A_0, const RuntimeMethod* method) ;
// System.Void b::b(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void b_b_m26D9779F99EB6F88BC55D13609AD116C70C234F9 (b_t972FAFF471676E1BF6780CEF716F7509668D82DA* __this, int64_t ___0_A_0, const RuntimeMethod* method) ;
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) ;
// System.Char System.String::get_Chars(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3 (String_t* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// System.Void b::a(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void b_a_m4763D12C4B10DCBB806877202AB15E4A3B7F1DE6 (b_t972FAFF471676E1BF6780CEF716F7509668D82DA* __this, int64_t ___0_A_0, const RuntimeMethod* method) ;
// System.UInt64 c::a(System.UInt64,System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t c_a_m4EBA04D0095E840BB3DB1EA942C3BA9104450E3E (uint64_t ___0_A_0, uint64_t ___1_A_1, const RuntimeMethod* method) ;
// System.Void c::a(System.UInt32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void c_a_mDFCC496EEE1479D0DFCE8DBE3D26D8BD8E19988A (uint32_t* ___0_A_0, const RuntimeMethod* method) ;
// System.Void c::a(System.UInt16&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void c_a_m091F763E46A6CD5B09755C0CBCA803F85F1C3A79 (uint16_t* ___0_A_0, const RuntimeMethod* method) ;
// System.Void c::a(System.UInt64&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void c_a_mC8410CC3B2FB053767D958B48C6BB152133F10B6 (uint64_t* ___0_A_0, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DotfuscatorAttribute::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DotfuscatorAttribute__ctor_m6C48D0E19C41E11223F9C5464EB27BCD660425EC (DotfuscatorAttribute_t90A6A71AB7691DF0BFCD2AC365104BF68425B9E7* __this, String_t* ___0_a, int32_t ___1_c, const RuntimeMethod* method) 
{
	{
		Attribute__ctor_m79ED1BF1EE36D1E417BA89A0D9F91F8AAD8D19E2(__this, NULL);
		String_t* L_0 = ___0_a;
		__this->___a_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___a_0), (void*)L_0);
		int32_t L_1 = ___1_c;
		__this->___c_1 = L_1;
		return;
	}
}
// System.String DotfuscatorAttribute::a()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* DotfuscatorAttribute_a_mA3B52A263303F2491FF01DAEE9AC1A842CA8E123 (DotfuscatorAttribute_t90A6A71AB7691DF0BFCD2AC365104BF68425B9E7* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___a_0;
		return L_0;
	}
}
// System.Int32 DotfuscatorAttribute::c()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DotfuscatorAttribute_c_m576B5AF32A0617E2C3B23B5384A3675B9794909D (DotfuscatorAttribute_t90A6A71AB7691DF0BFCD2AC365104BF68425B9E7* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___c_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Byte[] Unity.FontABTool.UnityFontABTool::PacKFontAB(System.Byte[],System.String,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* UnityFontABTool_PacKFontAB_mEA2AD10F9017F1D5A401BEA9A6DA86CA639BADDC (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_fontData, String_t* ___1_fontName, float ___2_lineSpace, float ___3_fontSize, float ___4_ascent, float ___5_desent, const RuntimeMethod* method) 
{
	{
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = ___0_fontData;
		String_t* L_1 = ___1_fontName;
		float L_2 = ___2_lineSpace;
		float L_3 = ___3_fontSize;
		float L_4 = ___4_ascent;
		float L_5 = ___5_desent;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_6;
		L_6 = UnityFontABTool_a_m34550A50BEBBEB488C37E50155EBC33CE9C63819(L_0, L_1, L_2, L_3, L_4, L_5, NULL);
		return L_6;
	}
}
// System.Byte[] Unity.FontABTool.UnityFontABTool::a(System.Byte[],System.String,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* UnityFontABTool_a_m34550A50BEBBEB488C37E50155EBC33CE9C63819 (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_A_0, String_t* ___1_A_1, float ___2_A_2, float ___3_A_3, float ___4_A_4, float ___5_A_5, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_GetType_m71A077E0B5DA3BD1DC0AB9AE387056CFCF56F93F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityFontABTool_a_m34550A50BEBBEB488C37E50155EBC33CE9C63819_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral570DC3936806A5949979D01A37BBAA35EC50DD5F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&b_t972FAFF471676E1BF6780CEF716F7509668D82DA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&d_t7410D4303AF1B382C8A19061C6DA00D2360AB7AC____a_0_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&d_t7410D4303AF1B382C8A19061C6DA00D2360AB7AC____b_1_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&d_t7410D4303AF1B382C8A19061C6DA00D2360AB7AC____c_2_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&d_t7410D4303AF1B382C8A19061C6DA00D2360AB7AC____d_3_FieldInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_0 = NULL;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_1 = NULL;
	Type_t* V_2 = NULL;
	MethodInfo_t* V_3 = NULL;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_4 = NULL;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_5 = NULL;
	uint32_t V_6 = 0;
	b_t972FAFF471676E1BF6780CEF716F7509668D82DA* V_7 = NULL;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_8 = NULL;
	c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57* V_9 = NULL;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_10 = NULL;
	{
		int32_t L_0;
		L_0 = Application_get_platform_m59EF7D6155D18891B24767F83F388160B1FF2138(NULL);
		if ((((int32_t)L_0) == ((int32_t)((int32_t)17))))
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_1;
		L_1 = Application_get_platform_m59EF7D6155D18891B24767F83F388160B1FF2138(NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_2;
		L_2 = Application_get_platform_m59EF7D6155D18891B24767F83F388160B1FF2138(NULL);
		if ((((int32_t)L_2) == ((int32_t)7)))
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3;
		L_3 = Application_get_platform_m59EF7D6155D18891B24767F83F388160B1FF2138(NULL);
		if ((((int32_t)L_3) == ((int32_t)((int32_t)16))))
		{
			goto IL_002d;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(_stringLiteral570DC3936806A5949979D01A37BBAA35EC50DD5F, NULL);
		return (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)NULL;
	}

IL_002d:
	{
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_4 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)SZArrayNew(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var, (uint32_t)((int32_t)72));
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_5 = L_4;
		RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 L_6 = { reinterpret_cast<intptr_t> (d_t7410D4303AF1B382C8A19061C6DA00D2360AB7AC____c_2_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m751372AA3F24FBF6DA9B9D687CBFA2DE436CAB9B((RuntimeArray*)L_5, L_6, NULL);
		V_0 = L_5;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_7 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)SZArrayNew(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var, (uint32_t)8);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_8 = L_7;
		RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 L_9 = { reinterpret_cast<intptr_t> (d_t7410D4303AF1B382C8A19061C6DA00D2360AB7AC____a_0_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m751372AA3F24FBF6DA9B9D687CBFA2DE436CAB9B((RuntimeArray*)L_8, L_9, NULL);
		V_1 = L_8;
		Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_10;
		L_10 = Encoding_get_ASCII_mCC61B512D320FD4E2E71CC0DFDF8DDF3CD215C65(NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_11 = V_0;
		String_t* L_12;
		L_12 = VirtualFuncInvoker1< String_t*, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* >::Invoke(33 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_10, L_11);
		il2cpp_codegen_runtime_class_init_inline(Type_t_il2cpp_TypeInfo_var);
		Type_t* L_13;
		L_13 = il2cpp_codegen_get_type(L_12, Type_GetType_m71A077E0B5DA3BD1DC0AB9AE387056CFCF56F93F_RuntimeMethod_var, UnityFontABTool_a_m34550A50BEBBEB488C37E50155EBC33CE9C63819_RuntimeMethod_var);
		V_2 = L_13;
		Type_t* L_14 = V_2;
		Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_15;
		L_15 = Encoding_get_ASCII_mCC61B512D320FD4E2E71CC0DFDF8DDF3CD215C65(NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_16 = V_1;
		String_t* L_17;
		L_17 = VirtualFuncInvoker1< String_t*, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* >::Invoke(33 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_15, L_16);
		MethodInfo_t* L_18;
		L_18 = Type_GetMethod_m9E66B5053F150537A74C490C1DA5174A7875189D(L_14, L_17, ((int32_t)16), NULL);
		V_3 = L_18;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_19 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)SZArrayNew(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var, (uint32_t)((int32_t)7950));
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_20 = L_19;
		RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 L_21 = { reinterpret_cast<intptr_t> (d_t7410D4303AF1B382C8A19061C6DA00D2360AB7AC____b_1_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m751372AA3F24FBF6DA9B9D687CBFA2DE436CAB9B((RuntimeArray*)L_20, L_21, NULL);
		V_4 = L_20;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_22 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)SZArrayNew(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var, (uint32_t)((int32_t)46));
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_23 = L_22;
		RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 L_24 = { reinterpret_cast<intptr_t> (d_t7410D4303AF1B382C8A19061C6DA00D2360AB7AC____d_3_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m751372AA3F24FBF6DA9B9D687CBFA2DE436CAB9B((RuntimeArray*)L_23, L_24, NULL);
		V_5 = L_23;
		V_6 = 6;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_25 = V_4;
		b_t972FAFF471676E1BF6780CEF716F7509668D82DA* L_26 = (b_t972FAFF471676E1BF6780CEF716F7509668D82DA*)il2cpp_codegen_object_new(b_t972FAFF471676E1BF6780CEF716F7509668D82DA_il2cpp_TypeInfo_var);
		b__ctor_m5D218ADFB9B12CAEBCFB3E050637FA0BEF33705D(L_26, L_25, NULL);
		V_7 = L_26;
		b_t972FAFF471676E1BF6780CEF716F7509668D82DA* L_27 = V_7;
		b_b_m3EEA39E861DB95D43365715F1CC72391418DB524(L_27, NULL);
		b_t972FAFF471676E1BF6780CEF716F7509668D82DA* L_28 = V_7;
		String_t* L_29 = ___1_A_1;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_30 = ___0_A_0;
		float L_31 = ___2_A_2;
		float L_32 = ___3_A_3;
		float L_33 = ___4_A_4;
		float L_34 = ___5_A_5;
		MethodInfo_t* L_35 = V_3;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_36;
		L_36 = b_a_m83904CAC9196C33E326CF37A434BCF9F4AA1C646(L_28, L_29, L_30, L_31, L_32, L_33, L_34, L_35, NULL);
		V_8 = L_36;
		b_t972FAFF471676E1BF6780CEF716F7509668D82DA* L_37 = V_7;
		b_a_m0EB9556527D166F142FCB52A806DF275DDE1E6A4(L_37, NULL);
		c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57* L_38 = (c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57*)il2cpp_codegen_object_new(c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57_il2cpp_TypeInfo_var);
		c__ctor_mCE86689EC3BFA79682A9DAD7E22DF7B0CE36DD2D(L_38, NULL);
		V_9 = L_38;
		c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57* L_39 = V_9;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_40 = V_8;
		Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_41;
		L_41 = Encoding_get_ASCII_mCC61B512D320FD4E2E71CC0DFDF8DDF3CD215C65(NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_42 = V_5;
		String_t* L_43;
		L_43 = VirtualFuncInvoker3< String_t*, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t >::Invoke(34 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_41, L_42, 0, ((int32_t)36));
		Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_44;
		L_44 = Encoding_get_ASCII_mCC61B512D320FD4E2E71CC0DFDF8DDF3CD215C65(NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_45 = V_5;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_46 = V_5;
		String_t* L_47;
		L_47 = VirtualFuncInvoker3< String_t*, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t >::Invoke(34 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_44, L_45, ((int32_t)36), ((int32_t)il2cpp_codegen_subtract(((int32_t)(((RuntimeArray*)L_46)->max_length)), ((int32_t)36))));
		uint32_t L_48 = V_6;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_49;
		L_49 = c_a_m1BBA40310965FA9C86772427E8AD85FE919E422F(L_39, L_40, L_43, L_47, L_48, NULL);
		V_10 = L_49;
		c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57* L_50 = V_9;
		c_a_m68DACE5CC586ED73D8A47E1CAF221A5F3BA10E78(L_50, NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_51 = V_10;
		return L_51;
	}
}
// System.Void Unity.FontABTool.UnityFontABTool::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityFontABTool__ctor_mF821CE89D979CE95D05DF50837598828C7C4C934 (UnityFontABTool_t29C54DB12CF17A7849B7DA9EA1D87475ADA05FFA* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void b::.ctor(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void b__ctor_m5D218ADFB9B12CAEBCFB3E050637FA0BEF33705D (b_t972FAFF471676E1BF6780CEF716F7509668D82DA* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_A_0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = ___0_A_0;
		__this->___k_10 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___k_10), (void*)L_0);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_1 = __this->___k_10;
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_2 = (MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2*)il2cpp_codegen_object_new(MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m662CA0D5A0004A2E3B475FE8DCD687B654870AA2(L_2, L_1, NULL);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_3 = (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158*)il2cpp_codegen_object_new(BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158_il2cpp_TypeInfo_var);
		BinaryReader__ctor_m898732FE0DBEDD480B24F6DE45A9AC696E44CC0F(L_3, L_2, NULL);
		__this->___e_4 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___e_4), (void*)L_3);
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_4 = (MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2*)il2cpp_codegen_object_new(MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m8F3BAE0B48E65BAA13C52FB020E502B3EA22CA6B(L_4, NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_5 = (BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E*)il2cpp_codegen_object_new(BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E_il2cpp_TypeInfo_var);
		BinaryWriter__ctor_mF2F1235E378C3EC493A8C816597BCEB4205A9CA0(L_5, L_4, NULL);
		__this->___f_5 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___f_5), (void*)L_5);
		return;
	}
}
// System.Void b::a(System.UInt32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void b_a_m0B288ECDA5A92480B615876EA2B09ECDAFC27335 (uint32_t* ___0_A_0, const RuntimeMethod* method) 
{
	{
		uint32_t* L_0 = ___0_A_0;
		uint32_t* L_1 = ___0_A_0;
		int32_t L_2 = *((uint32_t*)L_1);
		uint32_t* L_3 = ___0_A_0;
		int32_t L_4 = *((uint32_t*)L_3);
		uint32_t* L_5 = ___0_A_0;
		int32_t L_6 = *((uint32_t*)L_5);
		uint32_t* L_7 = ___0_A_0;
		int32_t L_8 = *((uint32_t*)L_7);
		*((int32_t*)L_0) = (int32_t)((int32_t)(((int32_t)(((int32_t)(((int32_t)((uint32_t)L_2>>((int32_t)24)))|((int32_t)(((int32_t)((uint32_t)L_4>>8))&((int32_t)65280)))))|((int32_t)(((int32_t)(L_6<<8))&((int32_t)16711680)))))|((int32_t)(L_8<<((int32_t)24)))));
		return;
	}
}
// System.Void b::a(System.UInt64&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void b_a_mA29C97334753DB15E7A7F615EBCDBD17B8882970 (uint64_t* ___0_A_0, const RuntimeMethod* method) 
{
	uint64_t V_0 = 0;
	{
		uint64_t* L_0 = ___0_A_0;
		int64_t L_1 = *((int64_t*)L_0);
		V_0 = L_1;
		uint64_t* L_2 = ___0_A_0;
		uint64_t L_3 = V_0;
		uint64_t L_4 = V_0;
		uint64_t L_5 = V_0;
		uint64_t L_6 = V_0;
		uint64_t L_7 = V_0;
		uint64_t L_8 = V_0;
		uint64_t L_9 = V_0;
		uint64_t L_10 = V_0;
		*((int64_t*)L_2) = (int64_t)((int64_t)(((int64_t)(((int64_t)(((int64_t)(((int64_t)(((int64_t)(((int64_t)(((int64_t)(((int64_t)((int32_t)255))&((int64_t)((uint64_t)L_3>>((int32_t)56)))))|((int64_t)(((int64_t)((int32_t)65280))&((int64_t)((uint64_t)L_4>>((int32_t)40)))))))|((int64_t)(((int64_t)((int32_t)16711680))&((int64_t)((uint64_t)L_5>>((int32_t)24)))))))|((int64_t)(((int64_t)(uint64_t)((uint32_t)((int32_t)-16777216)))&((int64_t)((uint64_t)L_6>>8))))))|((int64_t)(((int64_t)1095216660480LL)&((int64_t)((int64_t)L_7<<8))))))|((int64_t)(((int64_t)280375465082880LL)&((int64_t)((int64_t)L_8<<((int32_t)24)))))))|((int64_t)(((int64_t)71776119061217280LL)&((int64_t)((int64_t)L_9<<((int32_t)40)))))))|((int64_t)(((int64_t)-72057594037927936LL)&((int64_t)((int64_t)L_10<<((int32_t)56)))))));
		return;
	}
}
// System.Void b::b()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void b_b_m3EEA39E861DB95D43365715F1CC72391418DB524 (b_t972FAFF471676E1BF6780CEF716F7509668D82DA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&b_t972FAFF471676E1BF6780CEF716F7509668D82DA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	a_tC1CB14265A54FD08AC6F722A13BAD61141D148AF V_0;
	memset((&V_0), 0, sizeof(V_0));
	uint8_t V_1 = 0x0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int64_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B16_0 = 0;
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_0 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_1;
		L_1 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_0);
		int64_t L_2;
		L_2 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_1, ((int64_t)0), 0);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_3 = __this->___e_4;
		uint32_t L_4;
		L_4 = VirtualFuncInvoker0< uint32_t >::Invoke(16 /* System.UInt32 System.IO.BinaryReader::ReadUInt32() */, L_3);
		(&V_0)->___a_0 = L_4;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_5 = __this->___e_4;
		uint32_t L_6;
		L_6 = VirtualFuncInvoker0< uint32_t >::Invoke(16 /* System.UInt32 System.IO.BinaryReader::ReadUInt32() */, L_5);
		(&V_0)->___b_1 = L_6;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_7 = __this->___e_4;
		uint32_t L_8;
		L_8 = VirtualFuncInvoker0< uint32_t >::Invoke(16 /* System.UInt32 System.IO.BinaryReader::ReadUInt32() */, L_7);
		(&V_0)->___c_2 = L_8;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_9 = __this->___e_4;
		uint32_t L_10;
		L_10 = VirtualFuncInvoker0< uint32_t >::Invoke(16 /* System.UInt32 System.IO.BinaryReader::ReadUInt32() */, L_9);
		(&V_0)->___d_3 = L_10;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_11 = __this->___e_4;
		uint8_t L_12;
		L_12 = VirtualFuncInvoker0< uint8_t >::Invoke(10 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_11);
		(&V_0)->___e_4 = L_12;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_13 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_14;
		L_14 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_13);
		int64_t L_15;
		L_15 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_14, ((int64_t)3), 1);
		uint32_t* L_16 = (&(&V_0)->___c_2);
		il2cpp_codegen_runtime_class_init_inline(b_t972FAFF471676E1BF6780CEF716F7509668D82DA_il2cpp_TypeInfo_var);
		b_a_m0B288ECDA5A92480B615876EA2B09ECDAFC27335(L_16, NULL);
		uint32_t* L_17 = (&(&V_0)->___a_0);
		b_a_m0B288ECDA5A92480B615876EA2B09ECDAFC27335(L_17, NULL);
		uint32_t* L_18 = (&(&V_0)->___b_1);
		b_a_m0B288ECDA5A92480B615876EA2B09ECDAFC27335(L_18, NULL);
		uint32_t* L_19 = (&(&V_0)->___d_3);
		b_a_m0B288ECDA5A92480B615876EA2B09ECDAFC27335(L_19, NULL);
		a_tC1CB14265A54FD08AC6F722A13BAD61141D148AF L_20 = V_0;
		uint32_t L_21 = L_20.___c_2;
		if ((!(((uint32_t)L_21) < ((uint32_t)((int32_t)22)))))
		{
			goto IL_0119;
		}
	}
	{
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_22 = (&__this->___g_6);
		a_tC1CB14265A54FD08AC6F722A13BAD61141D148AF L_23 = V_0;
		uint32_t L_24 = L_23.___c_2;
		L_22->___b_1 = L_24;
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_25 = (&__this->___g_6);
		a_tC1CB14265A54FD08AC6F722A13BAD61141D148AF L_26 = V_0;
		uint32_t L_27 = L_26.___a_0;
		L_25->___d_3 = ((int64_t)(uint64_t)L_27);
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_28 = (&__this->___g_6);
		a_tC1CB14265A54FD08AC6F722A13BAD61141D148AF L_29 = V_0;
		uint32_t L_30 = L_29.___b_1;
		L_28->___e_4 = ((int64_t)(uint64_t)L_30);
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_31 = (&__this->___g_6);
		a_tC1CB14265A54FD08AC6F722A13BAD61141D148AF L_32 = V_0;
		uint32_t L_33 = L_32.___d_3;
		L_31->___f_5 = ((int64_t)(uint64_t)L_33);
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_34 = (&__this->___g_6);
		a_tC1CB14265A54FD08AC6F722A13BAD61141D148AF L_35 = V_0;
		uint8_t L_36 = L_35.___e_4;
		L_34->___g_6 = L_36;
		goto IL_021b;
	}

IL_0119:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_37 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_38;
		L_38 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_37);
		int64_t L_39;
		L_39 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_38, ((int64_t)0), 0);
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_40 = (&__this->___g_6);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_41 = __this->___e_4;
		uint64_t L_42;
		L_42 = VirtualFuncInvoker0< uint64_t >::Invoke(18 /* System.UInt64 System.IO.BinaryReader::ReadUInt64() */, L_41);
		L_40->___a_0 = L_42;
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_43 = (&__this->___g_6);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_44 = __this->___e_4;
		uint32_t L_45;
		L_45 = VirtualFuncInvoker0< uint32_t >::Invoke(16 /* System.UInt32 System.IO.BinaryReader::ReadUInt32() */, L_44);
		L_43->___b_1 = L_45;
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_46 = (&__this->___g_6);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_47 = __this->___e_4;
		uint32_t L_48;
		L_48 = VirtualFuncInvoker0< uint32_t >::Invoke(16 /* System.UInt32 System.IO.BinaryReader::ReadUInt32() */, L_47);
		L_46->___c_2 = L_48;
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_49 = (&__this->___g_6);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_50 = __this->___e_4;
		uint64_t L_51;
		L_51 = VirtualFuncInvoker0< uint64_t >::Invoke(18 /* System.UInt64 System.IO.BinaryReader::ReadUInt64() */, L_50);
		L_49->___d_3 = L_51;
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_52 = (&__this->___g_6);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_53 = __this->___e_4;
		uint64_t L_54;
		L_54 = VirtualFuncInvoker0< uint64_t >::Invoke(18 /* System.UInt64 System.IO.BinaryReader::ReadUInt64() */, L_53);
		L_52->___e_4 = L_54;
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_55 = (&__this->___g_6);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_56 = __this->___e_4;
		uint64_t L_57;
		L_57 = VirtualFuncInvoker0< uint64_t >::Invoke(18 /* System.UInt64 System.IO.BinaryReader::ReadUInt64() */, L_56);
		L_55->___f_5 = L_57;
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_58 = (&__this->___g_6);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_59 = __this->___e_4;
		uint8_t L_60;
		L_60 = VirtualFuncInvoker0< uint8_t >::Invoke(10 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_59);
		L_58->___g_6 = L_60;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_61 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_62;
		L_62 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_61);
		int64_t L_63;
		L_63 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_62, ((int64_t)7), 1);
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_64 = (&__this->___g_6);
		uint32_t* L_65 = (&L_64->___b_1);
		il2cpp_codegen_runtime_class_init_inline(b_t972FAFF471676E1BF6780CEF716F7509668D82DA_il2cpp_TypeInfo_var);
		b_a_m0B288ECDA5A92480B615876EA2B09ECDAFC27335(L_65, NULL);
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_66 = (&__this->___g_6);
		uint64_t* L_67 = (&L_66->___d_3);
		b_a_mA29C97334753DB15E7A7F615EBCDBD17B8882970(L_67, NULL);
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_68 = (&__this->___g_6);
		uint64_t* L_69 = (&L_68->___e_4);
		b_a_mA29C97334753DB15E7A7F615EBCDBD17B8882970(L_69, NULL);
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_70 = (&__this->___g_6);
		uint64_t* L_71 = (&L_70->___f_5);
		b_a_mA29C97334753DB15E7A7F615EBCDBD17B8882970(L_71, NULL);
	}

IL_021b:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_72 = __this->___e_4;
		uint8_t L_73;
		L_73 = VirtualFuncInvoker0< uint8_t >::Invoke(10 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_72);
		V_1 = L_73;
		goto IL_0235;
	}

IL_0229:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_74 = __this->___e_4;
		uint8_t L_75;
		L_75 = VirtualFuncInvoker0< uint8_t >::Invoke(10 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_74);
		V_1 = L_75;
	}

IL_0235:
	{
		uint8_t L_76 = V_1;
		if (L_76)
		{
			goto IL_0229;
		}
	}
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_77 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_78;
		L_78 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_77);
		int64_t L_79;
		L_79 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_78, ((int64_t)5), 1);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_80 = __this->___e_4;
		int32_t L_81;
		L_81 = VirtualFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_80);
		V_2 = L_81;
		int32_t L_82 = V_2;
		cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E* L_83 = (cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E*)(cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E*)SZArrayNew(cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E_il2cpp_TypeInfo_var, (uint32_t)L_82);
		__this->___h_7 = L_83;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___h_7), (void*)L_83);
		V_4 = 0;
		goto IL_0408;
	}

IL_026c:
	{
		cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E* L_84 = __this->___h_7;
		int32_t L_85 = V_4;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_86 = __this->___e_4;
		int32_t L_87;
		L_87 = VirtualFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_86);
		((L_84)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_85)))->___a_0 = L_87;
		cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E* L_88 = __this->___h_7;
		int32_t L_89 = V_4;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_90 = __this->___e_4;
		bool L_91;
		L_91 = VirtualFuncInvoker0< bool >::Invoke(9 /* System.Boolean System.IO.BinaryReader::ReadBoolean() */, L_90);
		((L_88)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_89)))->___b_1 = L_91;
		cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E* L_92 = __this->___h_7;
		int32_t L_93 = V_4;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_94 = __this->___e_4;
		int16_t L_95;
		L_95 = VirtualFuncInvoker0< int16_t >::Invoke(13 /* System.Int16 System.IO.BinaryReader::ReadInt16() */, L_94);
		((L_92)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_93)))->___c_2 = L_95;
		cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E* L_96 = __this->___h_7;
		int32_t L_97 = V_4;
		int32_t L_98 = ((L_96)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_97)))->___a_0;
		if ((((int32_t)L_98) == ((int32_t)(-1))))
		{
			goto IL_02f0;
		}
	}
	{
		cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E* L_99 = __this->___h_7;
		int32_t L_100 = V_4;
		int32_t L_101 = ((L_99)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_100)))->___a_0;
		G_B10_0 = ((((int32_t)L_101) == ((int32_t)((int32_t)114)))? 1 : 0);
		goto IL_02f1;
	}

IL_02f0:
	{
		G_B10_0 = 1;
	}

IL_02f1:
	{
		cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E* L_102 = __this->___h_7;
		int32_t L_103 = V_4;
		int16_t L_104 = ((L_102)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_103)))->___c_2;
		if (!((int32_t)(G_B10_0|((((int32_t)((((int32_t)L_104) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0))))
		{
			goto IL_0323;
		}
	}
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_105 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_106;
		L_106 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_105);
		int64_t L_107;
		L_107 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_106, ((int64_t)((int32_t)32)), 1);
		goto IL_0338;
	}

IL_0323:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_108 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_109;
		L_109 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_108);
		int64_t L_110;
		L_110 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_109, ((int64_t)((int32_t)16)), 1);
	}

IL_0338:
	{
		cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E* L_111 = __this->___h_7;
		int32_t L_112 = V_4;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_113 = __this->___e_4;
		uint32_t L_114;
		L_114 = VirtualFuncInvoker0< uint32_t >::Invoke(16 /* System.UInt32 System.IO.BinaryReader::ReadUInt32() */, L_113);
		((L_111)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_112)))->___f_5 = L_114;
		cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E* L_115 = __this->___h_7;
		int32_t L_116 = V_4;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_117 = __this->___e_4;
		uint32_t L_118;
		L_118 = VirtualFuncInvoker0< uint32_t >::Invoke(16 /* System.UInt32 System.IO.BinaryReader::ReadUInt32() */, L_117);
		((L_115)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_116)))->___g_6 = L_118;
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_119 = (&__this->___g_6);
		uint32_t L_120 = L_119->___b_1;
		if ((!(((uint32_t)L_120) > ((uint32_t)((int32_t)17)))))
		{
			goto IL_0385;
		}
	}
	{
		G_B16_0 = ((int32_t)32);
		goto IL_0387;
	}

IL_0385:
	{
		G_B16_0 = ((int32_t)24);
	}

IL_0387:
	{
		V_5 = G_B16_0;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_121 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_122;
		L_122 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_121);
		cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E* L_123 = __this->___h_7;
		int32_t L_124 = V_4;
		uint32_t L_125 = ((L_123)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_124)))->___f_5;
		int32_t L_126 = V_5;
		cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E* L_127 = __this->___h_7;
		int32_t L_128 = V_4;
		uint32_t L_129 = ((L_127)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_128)))->___g_6;
		int64_t L_130;
		L_130 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_122, ((int64_t)il2cpp_codegen_add(((int64_t)il2cpp_codegen_multiply(((int64_t)(uint64_t)L_125), ((int64_t)L_126))), ((int64_t)(uint64_t)L_129))), 1);
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_131 = (&__this->___g_6);
		uint32_t L_132 = L_131->___b_1;
		if ((!(((uint32_t)L_132) >= ((uint32_t)((int32_t)21)))))
		{
			goto IL_0402;
		}
	}
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_133 = __this->___e_4;
		int32_t L_134;
		L_134 = VirtualFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_133);
		V_6 = L_134;
		int32_t L_135 = V_6;
		if ((((int32_t)L_135) <= ((int32_t)0)))
		{
			goto IL_0402;
		}
	}
	{
		int32_t L_136 = V_6;
		V_7 = ((int64_t)((int32_t)il2cpp_codegen_multiply(4, L_136)));
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_137 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_138;
		L_138 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_137);
		int64_t L_139 = V_7;
		int64_t L_140;
		L_140 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_138, L_139, 1);
	}

IL_0402:
	{
		int32_t L_141 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_141, 1));
	}

IL_0408:
	{
		int32_t L_142 = V_4;
		int32_t L_143 = V_2;
		if ((((int32_t)L_142) < ((int32_t)L_143)))
		{
			goto IL_026c;
		}
	}
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_144 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_145;
		L_145 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_144);
		int64_t L_146;
		L_146 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_145);
		__this->___d_3 = L_146;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_147 = __this->___e_4;
		int32_t L_148;
		L_148 = VirtualFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_147);
		V_3 = L_148;
		int32_t L_149 = V_3;
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_150 = (dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066*)(dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066*)SZArrayNew(dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066_il2cpp_TypeInfo_var, (uint32_t)L_149);
		__this->___i_8 = L_150;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___i_8), (void*)L_150);
		V_8 = 0;
		goto IL_0515;
	}

IL_0446:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_151 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_152;
		L_152 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_151);
		int64_t L_153;
		L_153 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_152);
		b_b_m26D9779F99EB6F88BC55D13609AD116C70C234F9(__this, L_153, NULL);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_154 = __this->___e_4;
		int64_t L_155;
		L_155 = VirtualFuncInvoker0< int64_t >::Invoke(17 /* System.Int64 System.IO.BinaryReader::ReadInt64() */, L_154);
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_156 = (&__this->___g_6);
		uint32_t L_157 = L_156->___b_1;
		if ((!(((uint32_t)L_157) < ((uint32_t)((int32_t)22)))))
		{
			goto IL_0497;
		}
	}
	{
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_158 = __this->___i_8;
		int32_t L_159 = V_8;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_160 = __this->___e_4;
		uint32_t L_161;
		L_161 = VirtualFuncInvoker0< uint32_t >::Invoke(16 /* System.UInt32 System.IO.BinaryReader::ReadUInt32() */, L_160);
		((L_158)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_159)))->___a_0 = ((int64_t)(uint64_t)L_161);
		goto IL_04b4;
	}

IL_0497:
	{
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_162 = __this->___i_8;
		int32_t L_163 = V_8;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_164 = __this->___e_4;
		uint64_t L_165;
		L_165 = VirtualFuncInvoker0< uint64_t >::Invoke(18 /* System.UInt64 System.IO.BinaryReader::ReadUInt64() */, L_164);
		((L_162)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_163)))->___a_0 = L_165;
	}

IL_04b4:
	{
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_166 = __this->___i_8;
		int32_t L_167 = V_8;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_168 = __this->___e_4;
		uint32_t L_169;
		L_169 = VirtualFuncInvoker0< uint32_t >::Invoke(16 /* System.UInt32 System.IO.BinaryReader::ReadUInt32() */, L_168);
		((L_166)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_167)))->___b_1 = L_169;
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_170 = __this->___i_8;
		int32_t L_171 = V_8;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_172 = __this->___e_4;
		uint32_t L_173;
		L_173 = VirtualFuncInvoker0< uint32_t >::Invoke(16 /* System.UInt32 System.IO.BinaryReader::ReadUInt32() */, L_172);
		((L_170)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_171)))->___c_2 = L_173;
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_174 = __this->___i_8;
		int32_t L_175 = V_8;
		uint64_t* L_176 = (&((L_174)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_175)))->___a_0);
		uint64_t* L_177 = L_176;
		int64_t L_178 = *((int64_t*)L_177);
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_179 = (&__this->___g_6);
		uint64_t L_180 = L_179->___f_5;
		*((int64_t*)L_177) = (int64_t)((int64_t)il2cpp_codegen_add(L_178, (int64_t)L_180));
		int32_t L_181 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add(L_181, 1));
	}

IL_0515:
	{
		int32_t L_182 = V_8;
		int32_t L_183 = V_3;
		if ((((int32_t)L_182) < ((int32_t)L_183)))
		{
			goto IL_0446;
		}
	}
	{
		return;
	}
}
// System.Void b::b(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void b_b_m26D9779F99EB6F88BC55D13609AD116C70C234F9 (b_t972FAFF471676E1BF6780CEF716F7509668D82DA* __this, int64_t ___0_A_0, const RuntimeMethod* method) 
{
	int64_t V_0 = 0;
	{
		int64_t L_0 = ___0_A_0;
		int64_t L_1 = ___0_A_0;
		V_0 = ((int64_t)il2cpp_codegen_subtract(((int64_t)il2cpp_codegen_multiply(((int64_t)(((int64_t)il2cpp_codegen_add(L_0, ((int64_t)3)))/((int64_t)4))), ((int64_t)4))), L_1));
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_2 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_3;
		L_3 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_2);
		int64_t L_4 = V_0;
		int64_t L_5;
		L_5 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_3, L_4, 1);
		return;
	}
}
// System.Void b::a(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void b_a_m4763D12C4B10DCBB806877202AB15E4A3B7F1DE6 (b_t972FAFF471676E1BF6780CEF716F7509668D82DA* __this, int64_t ___0_A_0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&b_t972FAFF471676E1BF6780CEF716F7509668D82DA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int64_t V_0 = 0;
	{
		int64_t L_0 = ___0_A_0;
		int64_t L_1 = ___0_A_0;
		V_0 = ((int64_t)il2cpp_codegen_subtract(((int64_t)il2cpp_codegen_multiply(((int64_t)(((int64_t)il2cpp_codegen_add(L_0, ((int64_t)3)))/((int64_t)4))), ((int64_t)4))), L_1));
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_2 = __this->___f_5;
		il2cpp_codegen_runtime_class_init_inline(b_t972FAFF471676E1BF6780CEF716F7509668D82DA_il2cpp_TypeInfo_var);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = ((b_t972FAFF471676E1BF6780CEF716F7509668D82DA_StaticFields*)il2cpp_codegen_static_fields_for(b_t972FAFF471676E1BF6780CEF716F7509668D82DA_il2cpp_TypeInfo_var))->___l_11;
		int64_t L_4 = V_0;
		VirtualActionInvoker3< ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t >::Invoke(13 /* System.Void System.IO.BinaryWriter::Write(System.Byte[],System.Int32,System.Int32) */, L_2, L_3, 0, ((int32_t)L_4));
		return;
	}
}
// System.Byte[] b::a(System.String,System.Byte[],System.Single,System.Single,System.Single,System.Single,System.Reflection.MethodInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* b_a_m83904CAC9196C33E326CF37A434BCF9F4AA1C646 (b_t972FAFF471676E1BF6780CEF716F7509668D82DA* __this, String_t* ___0_A_0, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___1_A_1, float ___2_A_2, float ___3_A_3, float ___4_A_4, float ___5_A_5, MethodInfo_t* ___6_A_6, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&b_t972FAFF471676E1BF6780CEF716F7509668D82DA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int64_t V_0 = 0;
	uint64_t V_1 = 0;
	uint64_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	uint32_t V_7 = 0;
	int32_t V_8 = 0;
	uint64_t V_9 = 0;
	{
		V_0 = ((int64_t)0);
		V_1 = ((int64_t)(std::numeric_limits<int64_t>::max)());
		V_3 = 0;
		goto IL_03a6;
	}

IL_0014:
	{
		cU5BU5D_t26D4CE3F57334C6FFF203596C58265DE0F59F77E* L_0 = __this->___h_7;
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_1 = __this->___i_8;
		int32_t L_2 = V_3;
		uint32_t L_3 = ((L_1)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_2)))->___c_2;
		int32_t L_4 = ((L_0)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_3)))->___a_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)128)))))
		{
			goto IL_03a2;
		}
	}
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_5 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_6;
		L_6 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_5);
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_7 = __this->___i_8;
		int32_t L_8 = V_3;
		uint64_t L_9 = ((L_7)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_8)))->___a_0;
		int64_t L_10;
		L_10 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_6, L_9, 0);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_11 = __this->___f_5;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_12 = __this->___k_10;
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_13 = __this->___i_8;
		int32_t L_14 = V_3;
		uint64_t L_15 = ((L_13)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_14)))->___a_0;
		VirtualActionInvoker3< ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t >::Invoke(13 /* System.Void System.IO.BinaryWriter::Write(System.Byte[],System.Int32,System.Int32) */, L_11, L_12, 0, ((int32_t)L_15));
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_16 = __this->___i_8;
		int32_t L_17 = V_3;
		uint64_t L_18 = ((L_16)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_17)))->___a_0;
		V_1 = L_18;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_19 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_20;
		L_20 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_19);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_21 = __this->___e_4;
		int32_t L_22;
		L_22 = VirtualFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_21);
		int64_t L_23;
		L_23 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_20, ((int64_t)L_22), 1);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_24 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_25;
		L_25 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_24);
		int64_t L_26;
		L_26 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_25);
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_27 = __this->___i_8;
		int32_t L_28 = V_3;
		uint64_t L_29 = ((L_27)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_28)))->___a_0;
		b_b_m26D9779F99EB6F88BC55D13609AD116C70C234F9(__this, ((int64_t)il2cpp_codegen_subtract(L_26, (int64_t)L_29)), NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_30 = __this->___f_5;
		String_t* L_31 = ___0_A_0;
		int32_t L_32;
		L_32 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_31, NULL);
		VirtualActionInvoker1< int32_t >::Invoke(19 /* System.Void System.IO.BinaryWriter::Write(System.Int32) */, L_30, L_32);
		V_6 = 0;
		goto IL_010e;
	}

IL_00f4:
	{
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_33 = __this->___f_5;
		String_t* L_34 = ___0_A_0;
		int32_t L_35 = V_6;
		Il2CppChar L_36;
		L_36 = String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3(L_34, L_35, NULL);
		VirtualActionInvoker1< uint8_t >::Invoke(11 /* System.Void System.IO.BinaryWriter::Write(System.Byte) */, L_33, (uint8_t)((int32_t)(uint8_t)L_36));
		int32_t L_37 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add(L_37, 1));
	}

IL_010e:
	{
		int32_t L_38 = V_6;
		String_t* L_39 = ___0_A_0;
		int32_t L_40;
		L_40 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_39, NULL);
		if ((((int32_t)L_38) < ((int32_t)L_40)))
		{
			goto IL_00f4;
		}
	}
	{
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_41 = __this->___f_5;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_42;
		L_42 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(7 /* System.IO.Stream System.IO.BinaryWriter::get_BaseStream() */, L_41);
		int64_t L_43;
		L_43 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_42);
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_44 = __this->___i_8;
		int32_t L_45 = V_3;
		uint64_t L_46 = ((L_44)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_45)))->___a_0;
		b_a_m4763D12C4B10DCBB806877202AB15E4A3B7F1DE6(__this, ((int64_t)il2cpp_codegen_subtract(L_43, (int64_t)L_46)), NULL);
		e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024* L_47 = (&__this->___j_9);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_48 = __this->___e_4;
		float L_49;
		L_49 = VirtualFuncInvoker0< float >::Invoke(19 /* System.Single System.IO.BinaryReader::ReadSingle() */, L_48);
		L_47->___b_1 = L_49;
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_50 = __this->___f_5;
		float L_51 = ___2_A_2;
		VirtualActionInvoker1< float >::Invoke(23 /* System.Void System.IO.BinaryWriter::Write(System.Single) */, L_50, L_51);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_52 = __this->___f_5;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_53 = __this->___k_10;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_54 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_55;
		L_55 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_54);
		int64_t L_56;
		L_56 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_55);
		VirtualActionInvoker3< ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t >::Invoke(13 /* System.Void System.IO.BinaryWriter::Write(System.Byte[],System.Int32,System.Int32) */, L_52, L_53, ((int32_t)L_56), ((int32_t)12));
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_57 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_58;
		L_58 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_57);
		int64_t L_59;
		L_59 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_58, ((int64_t)((int32_t)12)), 1);
		e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024* L_60 = (&__this->___j_9);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_61 = __this->___e_4;
		float L_62;
		L_62 = VirtualFuncInvoker0< float >::Invoke(19 /* System.Single System.IO.BinaryReader::ReadSingle() */, L_61);
		L_60->___c_2 = L_62;
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_63 = __this->___f_5;
		float L_64 = ___3_A_3;
		VirtualActionInvoker1< float >::Invoke(23 /* System.Void System.IO.BinaryWriter::Write(System.Single) */, L_63, L_64);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_65 = __this->___f_5;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_66 = __this->___k_10;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_67 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_68;
		L_68 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_67);
		int64_t L_69;
		L_69 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_68);
		VirtualActionInvoker3< ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t >::Invoke(13 /* System.Void System.IO.BinaryWriter::Write(System.Byte[],System.Int32,System.Int32) */, L_65, L_66, ((int32_t)L_69), ((int32_t)12));
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_70 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_71;
		L_71 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_70);
		int64_t L_72;
		L_72 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_71, ((int64_t)((int32_t)12)), 1);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_73 = __this->___f_5;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_74 = __this->___k_10;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_75 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_76;
		L_76 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_75);
		int64_t L_77;
		L_77 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_76);
		VirtualActionInvoker3< ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t >::Invoke(13 /* System.Void System.IO.BinaryWriter::Write(System.Byte[],System.Int32,System.Int32) */, L_73, L_74, ((int32_t)L_77), ((int32_t)20));
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_78 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_79;
		L_79 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_78);
		int64_t L_80;
		L_80 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_79, ((int64_t)((int32_t)20)), 1);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_81 = __this->___f_5;
		VirtualActionInvoker1< int32_t >::Invoke(19 /* System.Void System.IO.BinaryWriter::Write(System.Int32) */, L_81, 0);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_82 = __this->___e_4;
		int32_t L_83;
		L_83 = VirtualFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_82);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_84 = __this->___f_5;
		VirtualActionInvoker1< int32_t >::Invoke(19 /* System.Void System.IO.BinaryWriter::Write(System.Int32) */, L_84, 0);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_85 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_86;
		L_86 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_85);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_87 = __this->___e_4;
		int32_t L_88;
		L_88 = VirtualFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_87);
		int64_t L_89;
		L_89 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_86, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_88, 8))), 1);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_90 = __this->___f_5;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_91 = __this->___k_10;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_92 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_93;
		L_93 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_92);
		int64_t L_94;
		L_94 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_93);
		VirtualActionInvoker3< ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t >::Invoke(13 /* System.Void System.IO.BinaryWriter::Write(System.Byte[],System.Int32,System.Int32) */, L_90, L_91, ((int32_t)L_94), 4);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_95 = __this->___e_4;
		float L_96;
		L_96 = VirtualFuncInvoker0< float >::Invoke(19 /* System.Single System.IO.BinaryReader::ReadSingle() */, L_95);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_97 = __this->___f_5;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_98 = ___1_A_1;
		VirtualActionInvoker1< int32_t >::Invoke(19 /* System.Void System.IO.BinaryWriter::Write(System.Int32) */, L_97, ((int32_t)(((RuntimeArray*)L_98)->max_length)));
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_99 = __this->___f_5;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_100 = ___1_A_1;
		VirtualActionInvoker1< ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* >::Invoke(12 /* System.Void System.IO.BinaryWriter::Write(System.Byte[]) */, L_99, L_100);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_101 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_102;
		L_102 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_101);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_103 = __this->___e_4;
		int32_t L_104;
		L_104 = VirtualFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_103);
		int64_t L_105;
		L_105 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_102, ((int64_t)L_104), 1);
		e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024* L_106 = (&__this->___j_9);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_107 = __this->___e_4;
		float L_108;
		L_108 = VirtualFuncInvoker0< float >::Invoke(19 /* System.Single System.IO.BinaryReader::ReadSingle() */, L_107);
		L_106->___e_4 = L_108;
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_109 = __this->___f_5;
		float L_110 = ___4_A_4;
		VirtualActionInvoker1< float >::Invoke(23 /* System.Void System.IO.BinaryWriter::Write(System.Single) */, L_109, L_110);
		e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024* L_111 = (&__this->___j_9);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_112 = __this->___e_4;
		float L_113;
		L_113 = VirtualFuncInvoker0< float >::Invoke(19 /* System.Single System.IO.BinaryReader::ReadSingle() */, L_112);
		L_111->___f_5 = L_113;
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_114 = __this->___f_5;
		float L_115 = ___5_A_5;
		VirtualActionInvoker1< float >::Invoke(23 /* System.Void System.IO.BinaryWriter::Write(System.Single) */, L_114, L_115);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_116 = __this->___f_5;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_117;
		L_117 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(7 /* System.IO.Stream System.IO.BinaryWriter::get_BaseStream() */, L_116);
		int64_t L_118;
		L_118 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_117);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_119 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_120;
		L_120 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_119);
		int64_t L_121;
		L_121 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_120);
		V_0 = ((int64_t)il2cpp_codegen_subtract(L_118, L_121));
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_122 = __this->___i_8;
		int32_t L_123 = V_3;
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_124 = __this->___i_8;
		int32_t L_125 = V_3;
		uint32_t L_126 = ((L_124)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_125)))->___b_1;
		int64_t L_127 = V_0;
		((L_122)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_123)))->___b_1 = ((int32_t)(uint32_t)((int64_t)il2cpp_codegen_add(((int64_t)(uint64_t)L_126), L_127)));
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_128 = (&__this->___g_6);
		uint64_t L_129 = L_128->___e_4;
		V_4 = ((int32_t)L_129);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_130 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_131;
		L_131 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_130);
		int64_t L_132;
		L_132 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_131);
		V_5 = ((int32_t)L_132);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_133 = __this->___f_5;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_134 = __this->___k_10;
		int32_t L_135 = V_5;
		int32_t L_136 = V_4;
		int32_t L_137 = V_5;
		VirtualActionInvoker3< ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t >::Invoke(13 /* System.Void System.IO.BinaryWriter::Write(System.Byte[],System.Int32,System.Int32) */, L_133, L_134, L_135, ((int32_t)il2cpp_codegen_subtract(L_136, L_137)));
	}

IL_03a2:
	{
		int32_t L_138 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_138, 1));
	}

IL_03a6:
	{
		int32_t L_139 = V_3;
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_140 = __this->___i_8;
		if ((((int32_t)L_139) < ((int32_t)((int32_t)(((RuntimeArray*)L_140)->max_length)))))
		{
			goto IL_0014;
		}
	}
	{
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_141 = __this->___f_5;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_142;
		L_142 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(7 /* System.IO.Stream System.IO.BinaryWriter::get_BaseStream() */, L_141);
		int64_t L_143;
		L_143 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_142);
		V_2 = L_143;
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_144 = (&__this->___g_6);
		uint32_t L_145 = L_144->___b_1;
		if ((!(((uint32_t)L_145) < ((uint32_t)((int32_t)22)))))
		{
			goto IL_03fc;
		}
	}
	{
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_146 = __this->___f_5;
		int64_t L_147;
		L_147 = VirtualFuncInvoker2< int64_t, int32_t, int32_t >::Invoke(9 /* System.Int64 System.IO.BinaryWriter::Seek(System.Int32,System.IO.SeekOrigin) */, L_146, 4, 0);
		uint64_t L_148 = V_2;
		V_7 = ((int32_t)(uint32_t)L_148);
		il2cpp_codegen_runtime_class_init_inline(b_t972FAFF471676E1BF6780CEF716F7509668D82DA_il2cpp_TypeInfo_var);
		b_a_m0B288ECDA5A92480B615876EA2B09ECDAFC27335((&V_7), NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_149 = __this->___f_5;
		uint32_t L_150 = V_7;
		VirtualActionInvoker1< uint32_t >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.UInt32) */, L_149, L_150);
		goto IL_041e;
	}

IL_03fc:
	{
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_151 = __this->___f_5;
		int64_t L_152;
		L_152 = VirtualFuncInvoker2< int64_t, int32_t, int32_t >::Invoke(9 /* System.Int64 System.IO.BinaryWriter::Seek(System.Int32,System.IO.SeekOrigin) */, L_151, ((int32_t)24), 0);
		il2cpp_codegen_runtime_class_init_inline(b_t972FAFF471676E1BF6780CEF716F7509668D82DA_il2cpp_TypeInfo_var);
		b_a_mA29C97334753DB15E7A7F615EBCDBD17B8882970((&V_2), NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_153 = __this->___f_5;
		uint64_t L_154 = V_2;
		VirtualActionInvoker1< uint64_t >::Invoke(22 /* System.Void System.IO.BinaryWriter::Write(System.UInt64) */, L_153, L_154);
	}

IL_041e:
	{
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_155 = __this->___f_5;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_156;
		L_156 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(7 /* System.IO.Stream System.IO.BinaryWriter::get_BaseStream() */, L_155);
		int64_t L_157 = __this->___d_3;
		int64_t L_158;
		L_158 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_156, ((int64_t)il2cpp_codegen_add(L_157, ((int64_t)4))), 0);
		V_8 = 0;
		goto IL_0529;
	}

IL_0441:
	{
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_159 = __this->___i_8;
		int32_t L_160 = V_8;
		uint64_t L_161 = ((L_159)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_160)))->___a_0;
		uint64_t L_162 = V_1;
		if ((!(((uint64_t)L_161) > ((uint64_t)L_162))))
		{
			goto IL_047c;
		}
	}
	{
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_163 = __this->___i_8;
		int32_t L_164 = V_8;
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_165 = __this->___i_8;
		int32_t L_166 = V_8;
		uint64_t L_167 = ((L_165)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_166)))->___a_0;
		int64_t L_168 = V_0;
		((L_163)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_164)))->___a_0 = ((int64_t)il2cpp_codegen_add((int64_t)L_167, L_168));
	}

IL_047c:
	{
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_169 = __this->___f_5;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_170;
		L_170 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(7 /* System.IO.Stream System.IO.BinaryWriter::get_BaseStream() */, L_169);
		int64_t L_171;
		L_171 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_170);
		b_a_m4763D12C4B10DCBB806877202AB15E4A3B7F1DE6(__this, L_171, NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_172 = __this->___f_5;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_173;
		L_173 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(7 /* System.IO.Stream System.IO.BinaryWriter::get_BaseStream() */, L_172);
		int64_t L_174;
		L_174 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_173, ((int64_t)8), 1);
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_175 = __this->___i_8;
		int32_t L_176 = V_8;
		uint64_t L_177 = ((L_175)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_176)))->___a_0;
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_178 = (&__this->___g_6);
		uint64_t L_179 = L_178->___f_5;
		V_9 = ((int64_t)il2cpp_codegen_subtract((int64_t)L_177, (int64_t)L_179));
		b_tFB66B901E947622DFAF3E315886F1089C51D6893* L_180 = (&__this->___g_6);
		uint32_t L_181 = L_180->___b_1;
		if ((!(((uint32_t)L_181) < ((uint32_t)((int32_t)22)))))
		{
			goto IL_04e5;
		}
	}
	{
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_182 = __this->___f_5;
		uint64_t L_183 = V_9;
		VirtualActionInvoker1< uint32_t >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.UInt32) */, L_182, ((int32_t)(uint32_t)L_183));
		goto IL_04f2;
	}

IL_04e5:
	{
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_184 = __this->___f_5;
		uint64_t L_185 = V_9;
		VirtualActionInvoker1< uint64_t >::Invoke(22 /* System.Void System.IO.BinaryWriter::Write(System.UInt64) */, L_184, L_185);
	}

IL_04f2:
	{
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_186 = __this->___f_5;
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_187 = __this->___i_8;
		int32_t L_188 = V_8;
		uint32_t L_189 = ((L_187)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_188)))->___b_1;
		VirtualActionInvoker1< uint32_t >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.UInt32) */, L_186, L_189);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_190 = __this->___f_5;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_191;
		L_191 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(7 /* System.IO.Stream System.IO.BinaryWriter::get_BaseStream() */, L_190);
		int64_t L_192;
		L_192 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_191, ((int64_t)4), 1);
		int32_t L_193 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add(L_193, 1));
	}

IL_0529:
	{
		int32_t L_194 = V_8;
		dU5BU5D_t886B56BD55BEADFD10654DA21C63F18DF1F86066* L_195 = __this->___i_8;
		if ((((int32_t)L_194) < ((int32_t)((int32_t)(((RuntimeArray*)L_195)->max_length)))))
		{
			goto IL_0441;
		}
	}
	{
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_196 = __this->___f_5;
		VirtualActionInvoker0::Invoke(8 /* System.Void System.IO.BinaryWriter::Flush() */, L_196);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_197 = __this->___f_5;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_198;
		L_198 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(7 /* System.IO.Stream System.IO.BinaryWriter::get_BaseStream() */, L_197);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_199;
		L_199 = VirtualFuncInvoker0< ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* >::Invoke(30 /* System.Byte[] System.IO.MemoryStream::ToArray() */, ((MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2*)CastclassClass((RuntimeObject*)L_198, MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2_il2cpp_TypeInfo_var)));
		return L_199;
	}
}
// System.Void b::a()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void b_a_m0EB9556527D166F142FCB52A806DF275DDE1E6A4 (b_t972FAFF471676E1BF6780CEF716F7509668D82DA* __this, const RuntimeMethod* method) 
{
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_0 = __this->___e_4;
		VirtualActionInvoker0::Invoke(6 /* System.Void System.IO.BinaryReader::Close() */, L_0);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_1 = __this->___f_5;
		VirtualActionInvoker0::Invoke(5 /* System.Void System.IO.BinaryWriter::Close() */, L_1);
		return;
	}
}
// System.Void b::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void b__cctor_mF0E9901289C18E88DCAC4C9B4D543B9520884441 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&b_t972FAFF471676E1BF6780CEF716F7509668D82DA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)SZArrayNew(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var, (uint32_t)4);
		((b_t972FAFF471676E1BF6780CEF716F7509668D82DA_StaticFields*)il2cpp_codegen_static_fields_for(b_t972FAFF471676E1BF6780CEF716F7509668D82DA_il2cpp_TypeInfo_var))->___l_11 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((b_t972FAFF471676E1BF6780CEF716F7509668D82DA_StaticFields*)il2cpp_codegen_static_fields_for(b_t972FAFF471676E1BF6780CEF716F7509668D82DA_il2cpp_TypeInfo_var))->___l_11), (void*)L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: b/c
IL2CPP_EXTERN_C void c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D_marshal_pinvoke(const c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D& unmarshaled, c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D_marshaled_pinvoke& marshaled)
{
	marshaled.___a_0 = unmarshaled.___a_0;
	marshaled.___b_1 = static_cast<int32_t>(unmarshaled.___b_1);
	marshaled.___c_2 = unmarshaled.___c_2;
	marshaled.___d_3 = unmarshaled.___d_3;
	marshaled.___e_4 = unmarshaled.___e_4;
	marshaled.___f_5 = unmarshaled.___f_5;
	marshaled.___g_6 = unmarshaled.___g_6;
}
IL2CPP_EXTERN_C void c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D_marshal_pinvoke_back(const c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D_marshaled_pinvoke& marshaled, c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D& unmarshaled)
{
	int32_t unmarshaleda_temp_0 = 0;
	unmarshaleda_temp_0 = marshaled.___a_0;
	unmarshaled.___a_0 = unmarshaleda_temp_0;
	bool unmarshaledb_temp_1 = false;
	unmarshaledb_temp_1 = static_cast<bool>(marshaled.___b_1);
	unmarshaled.___b_1 = unmarshaledb_temp_1;
	int16_t unmarshaledc_temp_2 = 0;
	unmarshaledc_temp_2 = marshaled.___c_2;
	unmarshaled.___c_2 = unmarshaledc_temp_2;
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C unmarshaledd_temp_3;
	memset((&unmarshaledd_temp_3), 0, sizeof(unmarshaledd_temp_3));
	unmarshaledd_temp_3 = marshaled.___d_3;
	unmarshaled.___d_3 = unmarshaledd_temp_3;
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C unmarshalede_temp_4;
	memset((&unmarshalede_temp_4), 0, sizeof(unmarshalede_temp_4));
	unmarshalede_temp_4 = marshaled.___e_4;
	unmarshaled.___e_4 = unmarshalede_temp_4;
	uint32_t unmarshaledf_temp_5 = 0;
	unmarshaledf_temp_5 = marshaled.___f_5;
	unmarshaled.___f_5 = unmarshaledf_temp_5;
	uint32_t unmarshaledg_temp_6 = 0;
	unmarshaledg_temp_6 = marshaled.___g_6;
	unmarshaled.___g_6 = unmarshaledg_temp_6;
}
// Conversion method for clean up from marshalling of: b/c
IL2CPP_EXTERN_C void c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D_marshal_pinvoke_cleanup(c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: b/c
IL2CPP_EXTERN_C void c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D_marshal_com(const c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D& unmarshaled, c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D_marshaled_com& marshaled)
{
	marshaled.___a_0 = unmarshaled.___a_0;
	marshaled.___b_1 = static_cast<int32_t>(unmarshaled.___b_1);
	marshaled.___c_2 = unmarshaled.___c_2;
	marshaled.___d_3 = unmarshaled.___d_3;
	marshaled.___e_4 = unmarshaled.___e_4;
	marshaled.___f_5 = unmarshaled.___f_5;
	marshaled.___g_6 = unmarshaled.___g_6;
}
IL2CPP_EXTERN_C void c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D_marshal_com_back(const c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D_marshaled_com& marshaled, c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D& unmarshaled)
{
	int32_t unmarshaleda_temp_0 = 0;
	unmarshaleda_temp_0 = marshaled.___a_0;
	unmarshaled.___a_0 = unmarshaleda_temp_0;
	bool unmarshaledb_temp_1 = false;
	unmarshaledb_temp_1 = static_cast<bool>(marshaled.___b_1);
	unmarshaled.___b_1 = unmarshaledb_temp_1;
	int16_t unmarshaledc_temp_2 = 0;
	unmarshaledc_temp_2 = marshaled.___c_2;
	unmarshaled.___c_2 = unmarshaledc_temp_2;
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C unmarshaledd_temp_3;
	memset((&unmarshaledd_temp_3), 0, sizeof(unmarshaledd_temp_3));
	unmarshaledd_temp_3 = marshaled.___d_3;
	unmarshaled.___d_3 = unmarshaledd_temp_3;
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C unmarshalede_temp_4;
	memset((&unmarshalede_temp_4), 0, sizeof(unmarshalede_temp_4));
	unmarshalede_temp_4 = marshaled.___e_4;
	unmarshaled.___e_4 = unmarshalede_temp_4;
	uint32_t unmarshaledf_temp_5 = 0;
	unmarshaledf_temp_5 = marshaled.___f_5;
	unmarshaled.___f_5 = unmarshaledf_temp_5;
	uint32_t unmarshaledg_temp_6 = 0;
	unmarshaledg_temp_6 = marshaled.___g_6;
	unmarshaled.___g_6 = unmarshaledg_temp_6;
}
// Conversion method for clean up from marshalling of: b/c
IL2CPP_EXTERN_C void c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D_marshal_com_cleanup(c_tB612DD27F43FBA96FB060C6E8739F6634DA61D4D_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: b/e
IL2CPP_EXTERN_C void e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024_marshal_pinvoke(const e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024& unmarshaled, e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024_marshaled_pinvoke& marshaled)
{
	marshaled.___a_0 = il2cpp_codegen_marshal_string(unmarshaled.___a_0);
	marshaled.___b_1 = unmarshaled.___b_1;
	marshaled.___c_2 = unmarshaled.___c_2;
	marshaled.___d_3 = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.___d_3);
	marshaled.___e_4 = unmarshaled.___e_4;
	marshaled.___f_5 = unmarshaled.___f_5;
}
IL2CPP_EXTERN_C void e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024_marshal_pinvoke_back(const e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024_marshaled_pinvoke& marshaled, e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.___a_0 = il2cpp_codegen_marshal_string_result(marshaled.___a_0);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___a_0), (void*)il2cpp_codegen_marshal_string_result(marshaled.___a_0));
	float unmarshaledb_temp_1 = 0.0f;
	unmarshaledb_temp_1 = marshaled.___b_1;
	unmarshaled.___b_1 = unmarshaledb_temp_1;
	float unmarshaledc_temp_2 = 0.0f;
	unmarshaledc_temp_2 = marshaled.___c_2;
	unmarshaled.___c_2 = unmarshaledc_temp_2;
	unmarshaled.___d_3 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___d_3);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___d_3), (void*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___d_3));
	float unmarshalede_temp_4 = 0.0f;
	unmarshalede_temp_4 = marshaled.___e_4;
	unmarshaled.___e_4 = unmarshalede_temp_4;
	float unmarshaledf_temp_5 = 0.0f;
	unmarshaledf_temp_5 = marshaled.___f_5;
	unmarshaled.___f_5 = unmarshaledf_temp_5;
}
// Conversion method for clean up from marshalling of: b/e
IL2CPP_EXTERN_C void e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024_marshal_pinvoke_cleanup(e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___a_0);
	marshaled.___a_0 = NULL;
	il2cpp_codegen_com_destroy_safe_array(marshaled.___d_3);
	marshaled.___d_3 = NULL;
}
// Conversion methods for marshalling of: b/e
IL2CPP_EXTERN_C void e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024_marshal_com(const e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024& unmarshaled, e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024_marshaled_com& marshaled)
{
	marshaled.___a_0 = il2cpp_codegen_marshal_bstring(unmarshaled.___a_0);
	marshaled.___b_1 = unmarshaled.___b_1;
	marshaled.___c_2 = unmarshaled.___c_2;
	marshaled.___d_3 = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.___d_3);
	marshaled.___e_4 = unmarshaled.___e_4;
	marshaled.___f_5 = unmarshaled.___f_5;
}
IL2CPP_EXTERN_C void e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024_marshal_com_back(const e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024_marshaled_com& marshaled, e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.___a_0 = il2cpp_codegen_marshal_bstring_result(marshaled.___a_0);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___a_0), (void*)il2cpp_codegen_marshal_bstring_result(marshaled.___a_0));
	float unmarshaledb_temp_1 = 0.0f;
	unmarshaledb_temp_1 = marshaled.___b_1;
	unmarshaled.___b_1 = unmarshaledb_temp_1;
	float unmarshaledc_temp_2 = 0.0f;
	unmarshaledc_temp_2 = marshaled.___c_2;
	unmarshaled.___c_2 = unmarshaledc_temp_2;
	unmarshaled.___d_3 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___d_3);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___d_3), (void*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___d_3));
	float unmarshalede_temp_4 = 0.0f;
	unmarshalede_temp_4 = marshaled.___e_4;
	unmarshaled.___e_4 = unmarshalede_temp_4;
	float unmarshaledf_temp_5 = 0.0f;
	unmarshaledf_temp_5 = marshaled.___f_5;
	unmarshaled.___f_5 = unmarshaledf_temp_5;
}
// Conversion method for clean up from marshalling of: b/e
IL2CPP_EXTERN_C void e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024_marshal_com_cleanup(e_t6E9BFF36EA2E22351F735E15F79BD7BA80D0E024_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___a_0);
	marshaled.___a_0 = NULL;
	il2cpp_codegen_com_destroy_safe_array(marshaled.___d_3);
	marshaled.___d_3 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.UInt64 c::a(System.UInt64,System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t c_a_m4EBA04D0095E840BB3DB1EA942C3BA9104450E3E (uint64_t ___0_A_0, uint64_t ___1_A_1, const RuntimeMethod* method) 
{
	{
		uint64_t L_0 = ___0_A_0;
		uint64_t L_1 = ___1_A_1;
		uint64_t L_2 = ___1_A_1;
		return ((int64_t)(((int64_t)il2cpp_codegen_add((int64_t)L_0, ((int64_t)il2cpp_codegen_subtract((int64_t)L_1, ((int64_t)1)))))&((~((int64_t)il2cpp_codegen_subtract((int64_t)L_2, ((int64_t)1)))))));
	}
}
// System.Void c::a(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void c_a_mCE5ABC97A65DF6C7C6651DC4FFA7BACC4D82DD85 (c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57* __this, int64_t ___0_A_0, const RuntimeMethod* method) 
{
	int64_t V_0 = 0;
	{
		int64_t L_0 = ___0_A_0;
		uint64_t L_1;
		L_1 = c_a_m4EBA04D0095E840BB3DB1EA942C3BA9104450E3E(L_0, ((int64_t)((int32_t)16)), NULL);
		int64_t L_2 = ___0_A_0;
		V_0 = ((int64_t)il2cpp_codegen_subtract((int64_t)L_1, L_2));
		goto IL_001f;
	}

IL_000e:
	{
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_3 = __this->___e_4;
		VirtualActionInvoker1< Il2CppChar >::Invoke(14 /* System.Void System.IO.BinaryWriter::Write(System.Char) */, L_3, 0);
		int64_t L_4 = V_0;
		V_0 = ((int64_t)il2cpp_codegen_subtract(L_4, ((int64_t)1)));
	}

IL_001f:
	{
		int64_t L_5 = V_0;
		if ((((int64_t)L_5) > ((int64_t)((int64_t)0))))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.Void c::a(System.UInt16&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void c_a_m091F763E46A6CD5B09755C0CBCA803F85F1C3A79 (uint16_t* ___0_A_0, const RuntimeMethod* method) 
{
	{
		uint16_t* L_0 = ___0_A_0;
		uint16_t* L_1 = ___0_A_0;
		int32_t L_2 = *((uint16_t*)L_1);
		uint16_t* L_3 = ___0_A_0;
		int32_t L_4 = *((uint16_t*)L_3);
		*((int16_t*)L_0) = (int16_t)((int32_t)(uint16_t)((int32_t)(((int32_t)(L_2>>8))|((int32_t)(L_4<<8)))));
		return;
	}
}
// System.Void c::a(System.UInt32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void c_a_mDFCC496EEE1479D0DFCE8DBE3D26D8BD8E19988A (uint32_t* ___0_A_0, const RuntimeMethod* method) 
{
	{
		uint32_t* L_0 = ___0_A_0;
		uint32_t* L_1 = ___0_A_0;
		int32_t L_2 = *((uint32_t*)L_1);
		uint32_t* L_3 = ___0_A_0;
		int32_t L_4 = *((uint32_t*)L_3);
		uint32_t* L_5 = ___0_A_0;
		int32_t L_6 = *((uint32_t*)L_5);
		uint32_t* L_7 = ___0_A_0;
		int32_t L_8 = *((uint32_t*)L_7);
		*((int32_t*)L_0) = (int32_t)((int32_t)(((int32_t)(((int32_t)(((int32_t)((uint32_t)L_2>>((int32_t)24)))|((int32_t)(((int32_t)((uint32_t)L_4>>8))&((int32_t)65280)))))|((int32_t)(((int32_t)(L_6<<8))&((int32_t)16711680)))))|((int32_t)(L_8<<((int32_t)24)))));
		return;
	}
}
// System.Void c::a(System.UInt64&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void c_a_mC8410CC3B2FB053767D958B48C6BB152133F10B6 (uint64_t* ___0_A_0, const RuntimeMethod* method) 
{
	uint64_t V_0 = 0;
	{
		uint64_t* L_0 = ___0_A_0;
		int64_t L_1 = *((int64_t*)L_0);
		V_0 = L_1;
		uint64_t* L_2 = ___0_A_0;
		uint64_t L_3 = V_0;
		uint64_t L_4 = V_0;
		uint64_t L_5 = V_0;
		uint64_t L_6 = V_0;
		uint64_t L_7 = V_0;
		uint64_t L_8 = V_0;
		uint64_t L_9 = V_0;
		uint64_t L_10 = V_0;
		*((int64_t*)L_2) = (int64_t)((int64_t)(((int64_t)(((int64_t)(((int64_t)(((int64_t)(((int64_t)(((int64_t)(((int64_t)(((int64_t)((int32_t)255))&((int64_t)((uint64_t)L_3>>((int32_t)56)))))|((int64_t)(((int64_t)((int32_t)65280))&((int64_t)((uint64_t)L_4>>((int32_t)40)))))))|((int64_t)(((int64_t)((int32_t)16711680))&((int64_t)((uint64_t)L_5>>((int32_t)24)))))))|((int64_t)(((int64_t)(uint64_t)((uint32_t)((int32_t)-16777216)))&((int64_t)((uint64_t)L_6>>8))))))|((int64_t)(((int64_t)1095216660480LL)&((int64_t)((int64_t)L_7<<8))))))|((int64_t)(((int64_t)280375465082880LL)&((int64_t)((int64_t)L_8<<((int32_t)24)))))))|((int64_t)(((int64_t)71776119061217280LL)&((int64_t)((int64_t)L_9<<((int32_t)40)))))))|((int64_t)(((int64_t)-72057594037927936LL)&((int64_t)((int64_t)L_10<<((int32_t)56)))))));
		return;
	}
}
// System.Byte[] c::a(System.Byte[],System.String,System.String,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* c_a_m1BBA40310965FA9C86772427E8AD85FE919E422F (c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_A_0, String_t* ___1_A_1, String_t* ___2_A_2, uint32_t ___3_A_3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1937633CE6C19B9FEB3C6F2F3EA9DA5E3D90EE46);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6254C1E893723324192461A9456E4F1C1F5CA6D5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	Il2CppChar V_3 = 0x0;
	Il2CppChar V_4 = 0x0;
	Il2CppChar V_5 = 0x0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	Il2CppChar V_8 = 0x0;
	{
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_0 = (&__this->___b_1);
		L_0->___a_0 = _stringLiteral1937633CE6C19B9FEB3C6F2F3EA9DA5E3D90EE46;
		Il2CppCodeGenWriteBarrier((void**)(&L_0->___a_0), (void*)_stringLiteral1937633CE6C19B9FEB3C6F2F3EA9DA5E3D90EE46);
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_1 = (&__this->___b_1);
		uint32_t L_2 = ___3_A_3;
		L_1->___b_1 = L_2;
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_3 = (&__this->___b_1);
		L_3->___c_2 = _stringLiteral6254C1E893723324192461A9456E4F1C1F5CA6D5;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___c_2), (void*)_stringLiteral6254C1E893723324192461A9456E4F1C1F5CA6D5);
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_4 = (&__this->___b_1);
		String_t* L_5 = ___2_A_2;
		L_4->___d_3 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&L_4->___d_3), (void*)L_5);
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_6 = (&__this->___b_1);
		String_t* L_7 = ___1_A_1;
		int32_t L_8;
		L_8 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_7, NULL);
		L_6->___g_6 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_add(((int32_t)54), L_8)), 1));
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_9 = (&__this->___b_1);
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_10 = (&__this->___b_1);
		uint32_t L_11 = L_10->___g_6;
		L_9->___f_5 = L_11;
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_12 = (&__this->___b_1);
		L_12->___e_4 = ((int64_t)0);
		d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2* L_13 = (&__this->___d_3);
		L_13->___a_0 = 1;
		d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2* L_14 = (&__this->___d_3);
		cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF* L_15 = (cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF*)(cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF*)SZArrayNew(cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF_il2cpp_TypeInfo_var, (uint32_t)1);
		L_14->___b_1 = L_15;
		Il2CppCodeGenWriteBarrier((void**)(&L_14->___b_1), (void*)L_15);
		d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2* L_16 = (&__this->___d_3);
		cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF* L_17 = L_16->___b_1;
		((L_17)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(0)))->___a_0 = ((int64_t)0);
		d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2* L_18 = (&__this->___d_3);
		cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF* L_19 = L_18->___b_1;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_20 = ___0_A_0;
		((L_19)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(0)))->___b_1 = ((int64_t)((int32_t)(((RuntimeArray*)L_20)->max_length)));
		d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2* L_21 = (&__this->___d_3);
		cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF* L_22 = L_21->___b_1;
		((L_22)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(0)))->___c_2 = 4;
		d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2* L_23 = (&__this->___d_3);
		cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF* L_24 = L_23->___b_1;
		String_t* L_25 = ___1_A_1;
		((L_24)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(0)))->___d_3 = L_25;
		Il2CppCodeGenWriteBarrier((void**)(&((L_24)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(0)))->___d_3), (void*)L_25);
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_26 = (&__this->___c_2);
		a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C* L_27 = (&L_26->___a_0);
		il2cpp_codegen_initobj(L_27, sizeof(a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C));
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_28 = (&__this->___c_2);
		L_28->___b_1 = 1;
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_29 = (&__this->___c_2);
		aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2* L_30 = (aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2*)(aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2*)SZArrayNew(aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2_il2cpp_TypeInfo_var, (uint32_t)1);
		L_29->___c_2 = L_30;
		Il2CppCodeGenWriteBarrier((void**)(&L_29->___c_2), (void*)L_30);
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_31 = (&__this->___c_2);
		aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2* L_32 = L_31->___c_2;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_33 = ___0_A_0;
		((L_32)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(0)))->___b_1 = ((int32_t)(((RuntimeArray*)L_33)->max_length));
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_34 = (&__this->___c_2);
		aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2* L_35 = L_34->___c_2;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_36 = ___0_A_0;
		((L_35)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(0)))->___a_0 = ((int32_t)(((RuntimeArray*)L_36)->max_length));
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_37 = (&__this->___c_2);
		aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2* L_38 = L_37->___c_2;
		((L_38)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(0)))->___c_2 = (uint16_t)((int32_t)64);
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_39 = (&__this->___b_1);
		L_39->___h_7 = ((int32_t)64);
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_40 = (MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2*)il2cpp_codegen_object_new(MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m8F3BAE0B48E65BAA13C52FB020E502B3EA22CA6B(L_40, NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_41 = (BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E*)il2cpp_codegen_object_new(BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E_il2cpp_TypeInfo_var);
		BinaryWriter__ctor_mF2F1235E378C3EC493A8C816597BCEB4205A9CA0(L_41, L_40, NULL);
		__this->___e_4 = L_41;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___e_4), (void*)L_41);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_42 = __this->___e_4;
		int64_t L_43;
		L_43 = VirtualFuncInvoker2< int64_t, int32_t, int32_t >::Invoke(9 /* System.Int64 System.IO.BinaryWriter::Seek(System.Int32,System.IO.SeekOrigin) */, L_42, 0, 0);
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_44 = (&__this->___b_1);
		String_t* L_45 = L_44->___a_0;
		V_1 = L_45;
		V_2 = 0;
		goto IL_01ba;
	}

IL_01a2:
	{
		String_t* L_46 = V_1;
		int32_t L_47 = V_2;
		Il2CppChar L_48;
		L_48 = String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3(L_46, L_47, NULL);
		V_3 = L_48;
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_49 = __this->___e_4;
		Il2CppChar L_50 = V_3;
		VirtualActionInvoker1< Il2CppChar >::Invoke(14 /* System.Void System.IO.BinaryWriter::Write(System.Char) */, L_49, L_50);
		int32_t L_51 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_51, 1));
	}

IL_01ba:
	{
		int32_t L_52 = V_2;
		String_t* L_53 = V_1;
		int32_t L_54;
		L_54 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_53, NULL);
		if ((((int32_t)L_52) < ((int32_t)L_54)))
		{
			goto IL_01a2;
		}
	}
	{
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_55 = __this->___e_4;
		VirtualActionInvoker1< Il2CppChar >::Invoke(14 /* System.Void System.IO.BinaryWriter::Write(System.Char) */, L_55, 0);
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_56 = (&__this->___b_1);
		uint32_t* L_57 = (&L_56->___b_1);
		c_a_mDFCC496EEE1479D0DFCE8DBE3D26D8BD8E19988A(L_57, NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_58 = __this->___e_4;
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_59 = (&__this->___b_1);
		uint32_t L_60 = L_59->___b_1;
		VirtualActionInvoker1< uint32_t >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.UInt32) */, L_58, L_60);
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_61 = (&__this->___b_1);
		String_t* L_62 = L_61->___c_2;
		V_1 = L_62;
		V_2 = 0;
		goto IL_021f;
	}

IL_0205:
	{
		String_t* L_63 = V_1;
		int32_t L_64 = V_2;
		Il2CppChar L_65;
		L_65 = String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3(L_63, L_64, NULL);
		V_4 = L_65;
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_66 = __this->___e_4;
		Il2CppChar L_67 = V_4;
		VirtualActionInvoker1< Il2CppChar >::Invoke(14 /* System.Void System.IO.BinaryWriter::Write(System.Char) */, L_66, L_67);
		int32_t L_68 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_68, 1));
	}

IL_021f:
	{
		int32_t L_69 = V_2;
		String_t* L_70 = V_1;
		int32_t L_71;
		L_71 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_70, NULL);
		if ((((int32_t)L_69) < ((int32_t)L_71)))
		{
			goto IL_0205;
		}
	}
	{
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_72 = __this->___e_4;
		VirtualActionInvoker1< Il2CppChar >::Invoke(14 /* System.Void System.IO.BinaryWriter::Write(System.Char) */, L_72, 0);
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_73 = (&__this->___b_1);
		String_t* L_74 = L_73->___d_3;
		V_1 = L_74;
		V_2 = 0;
		goto IL_025e;
	}

IL_0244:
	{
		String_t* L_75 = V_1;
		int32_t L_76 = V_2;
		Il2CppChar L_77;
		L_77 = String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3(L_75, L_76, NULL);
		V_5 = L_77;
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_78 = __this->___e_4;
		Il2CppChar L_79 = V_5;
		VirtualActionInvoker1< Il2CppChar >::Invoke(14 /* System.Void System.IO.BinaryWriter::Write(System.Char) */, L_78, L_79);
		int32_t L_80 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_80, 1));
	}

IL_025e:
	{
		int32_t L_81 = V_2;
		String_t* L_82 = V_1;
		int32_t L_83;
		L_83 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_82, NULL);
		if ((((int32_t)L_81) < ((int32_t)L_83)))
		{
			goto IL_0244;
		}
	}
	{
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_84 = __this->___e_4;
		VirtualActionInvoker1< Il2CppChar >::Invoke(14 /* System.Void System.IO.BinaryWriter::Write(System.Char) */, L_84, 0);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_85 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_86;
		L_86 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(7 /* System.IO.Stream System.IO.BinaryWriter::get_BaseStream() */, L_85);
		int64_t L_87;
		L_87 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_86);
		V_0 = ((int32_t)L_87);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_88 = __this->___e_4;
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_89 = (&__this->___b_1);
		uint64_t L_90 = L_89->___e_4;
		VirtualActionInvoker1< uint64_t >::Invoke(22 /* System.Void System.IO.BinaryWriter::Write(System.UInt64) */, L_88, L_90);
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_91 = (&__this->___b_1);
		uint32_t* L_92 = (&L_91->___f_5);
		c_a_mDFCC496EEE1479D0DFCE8DBE3D26D8BD8E19988A(L_92, NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_93 = __this->___e_4;
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_94 = (&__this->___b_1);
		uint32_t L_95 = L_94->___f_5;
		VirtualActionInvoker1< uint32_t >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.UInt32) */, L_93, L_95);
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_96 = (&__this->___b_1);
		uint32_t* L_97 = (&L_96->___g_6);
		c_a_mDFCC496EEE1479D0DFCE8DBE3D26D8BD8E19988A(L_97, NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_98 = __this->___e_4;
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_99 = (&__this->___b_1);
		uint32_t L_100 = L_99->___g_6;
		VirtualActionInvoker1< uint32_t >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.UInt32) */, L_98, L_100);
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_101 = (&__this->___b_1);
		uint32_t* L_102 = (&L_101->___h_7);
		c_a_mDFCC496EEE1479D0DFCE8DBE3D26D8BD8E19988A(L_102, NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_103 = __this->___e_4;
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_104 = (&__this->___b_1);
		uint32_t L_105 = L_104->___h_7;
		VirtualActionInvoker1< uint32_t >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.UInt32) */, L_103, L_105);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_106 = __this->___e_4;
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_107 = (&__this->___c_2);
		a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C* L_108 = (&L_107->___a_0);
		uint32_t L_109 = L_108->___a_0;
		VirtualActionInvoker1< uint32_t >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.UInt32) */, L_106, L_109);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_110 = __this->___e_4;
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_111 = (&__this->___c_2);
		a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C* L_112 = (&L_111->___a_0);
		uint32_t L_113 = L_112->___b_1;
		VirtualActionInvoker1< uint32_t >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.UInt32) */, L_110, L_113);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_114 = __this->___e_4;
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_115 = (&__this->___c_2);
		a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C* L_116 = (&L_115->___a_0);
		uint32_t L_117 = L_116->___c_2;
		VirtualActionInvoker1< uint32_t >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.UInt32) */, L_114, L_117);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_118 = __this->___e_4;
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_119 = (&__this->___c_2);
		a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C* L_120 = (&L_119->___a_0);
		uint32_t L_121 = L_120->___d_3;
		VirtualActionInvoker1< uint32_t >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.UInt32) */, L_118, L_121);
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_122 = (&__this->___c_2);
		uint32_t* L_123 = (&L_122->___b_1);
		c_a_mDFCC496EEE1479D0DFCE8DBE3D26D8BD8E19988A(L_123, NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_124 = __this->___e_4;
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_125 = (&__this->___c_2);
		uint32_t L_126 = L_125->___b_1;
		VirtualActionInvoker1< uint32_t >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.UInt32) */, L_124, L_126);
		V_6 = 0;
		goto IL_0467;
	}

IL_03a7:
	{
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_127 = (&__this->___c_2);
		aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2* L_128 = L_127->___c_2;
		int32_t L_129 = V_6;
		uint32_t* L_130 = (&((L_128)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_129)))->___a_0);
		c_a_mDFCC496EEE1479D0DFCE8DBE3D26D8BD8E19988A(L_130, NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_131 = __this->___e_4;
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_132 = (&__this->___c_2);
		aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2* L_133 = L_132->___c_2;
		int32_t L_134 = V_6;
		uint32_t L_135 = ((L_133)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_134)))->___a_0;
		VirtualActionInvoker1< uint32_t >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.UInt32) */, L_131, L_135);
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_136 = (&__this->___c_2);
		aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2* L_137 = L_136->___c_2;
		int32_t L_138 = V_6;
		uint32_t* L_139 = (&((L_137)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_138)))->___b_1);
		c_a_mDFCC496EEE1479D0DFCE8DBE3D26D8BD8E19988A(L_139, NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_140 = __this->___e_4;
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_141 = (&__this->___c_2);
		aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2* L_142 = L_141->___c_2;
		int32_t L_143 = V_6;
		uint32_t L_144 = ((L_142)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_143)))->___b_1;
		VirtualActionInvoker1< uint32_t >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.UInt32) */, L_140, L_144);
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_145 = (&__this->___c_2);
		aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2* L_146 = L_145->___c_2;
		int32_t L_147 = V_6;
		uint16_t* L_148 = (&((L_146)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_147)))->___c_2);
		c_a_m091F763E46A6CD5B09755C0CBCA803F85F1C3A79(L_148, NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_149 = __this->___e_4;
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_150 = (&__this->___c_2);
		aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2* L_151 = L_150->___c_2;
		int32_t L_152 = V_6;
		uint16_t L_153 = ((L_151)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_152)))->___c_2;
		VirtualActionInvoker1< uint16_t >::Invoke(18 /* System.Void System.IO.BinaryWriter::Write(System.UInt16) */, L_149, L_153);
		int32_t L_154 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add(L_154, 1));
	}

IL_0467:
	{
		int32_t L_155 = V_6;
		b_t377CCBB7B97AADE524489D5766C28A547BBAB31B* L_156 = (&__this->___c_2);
		aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2* L_157 = L_156->___c_2;
		if ((((int32_t)L_155) < ((int32_t)((int32_t)(((RuntimeArray*)L_157)->max_length)))))
		{
			goto IL_03a7;
		}
	}
	{
		d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2* L_158 = (&__this->___d_3);
		uint32_t* L_159 = (&L_158->___a_0);
		c_a_mDFCC496EEE1479D0DFCE8DBE3D26D8BD8E19988A(L_159, NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_160 = __this->___e_4;
		d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2* L_161 = (&__this->___d_3);
		uint32_t L_162 = L_161->___a_0;
		VirtualActionInvoker1< uint32_t >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.UInt32) */, L_160, L_162);
		V_7 = 0;
		goto IL_05b4;
	}

IL_04a9:
	{
		d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2* L_163 = (&__this->___d_3);
		cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF* L_164 = L_163->___b_1;
		int32_t L_165 = V_7;
		uint64_t* L_166 = (&((L_164)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_165)))->___a_0);
		c_a_mC8410CC3B2FB053767D958B48C6BB152133F10B6(L_166, NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_167 = __this->___e_4;
		d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2* L_168 = (&__this->___d_3);
		cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF* L_169 = L_168->___b_1;
		int32_t L_170 = V_7;
		uint64_t L_171 = ((L_169)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_170)))->___a_0;
		VirtualActionInvoker1< uint64_t >::Invoke(22 /* System.Void System.IO.BinaryWriter::Write(System.UInt64) */, L_167, L_171);
		d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2* L_172 = (&__this->___d_3);
		cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF* L_173 = L_172->___b_1;
		int32_t L_174 = V_7;
		uint64_t* L_175 = (&((L_173)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_174)))->___b_1);
		c_a_mC8410CC3B2FB053767D958B48C6BB152133F10B6(L_175, NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_176 = __this->___e_4;
		d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2* L_177 = (&__this->___d_3);
		cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF* L_178 = L_177->___b_1;
		int32_t L_179 = V_7;
		uint64_t L_180 = ((L_178)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_179)))->___b_1;
		VirtualActionInvoker1< uint64_t >::Invoke(22 /* System.Void System.IO.BinaryWriter::Write(System.UInt64) */, L_176, L_180);
		d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2* L_181 = (&__this->___d_3);
		cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF* L_182 = L_181->___b_1;
		int32_t L_183 = V_7;
		uint32_t* L_184 = (&((L_182)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_183)))->___c_2);
		c_a_mDFCC496EEE1479D0DFCE8DBE3D26D8BD8E19988A(L_184, NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_185 = __this->___e_4;
		d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2* L_186 = (&__this->___d_3);
		cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF* L_187 = L_186->___b_1;
		int32_t L_188 = V_7;
		uint32_t L_189 = ((L_187)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_188)))->___c_2;
		VirtualActionInvoker1< uint32_t >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.UInt32) */, L_185, L_189);
		d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2* L_190 = (&__this->___d_3);
		cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF* L_191 = L_190->___b_1;
		int32_t L_192 = V_7;
		String_t* L_193 = ((L_191)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_192)))->___d_3;
		V_1 = L_193;
		V_2 = 0;
		goto IL_0599;
	}

IL_057f:
	{
		String_t* L_194 = V_1;
		int32_t L_195 = V_2;
		Il2CppChar L_196;
		L_196 = String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3(L_194, L_195, NULL);
		V_8 = L_196;
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_197 = __this->___e_4;
		Il2CppChar L_198 = V_8;
		VirtualActionInvoker1< Il2CppChar >::Invoke(14 /* System.Void System.IO.BinaryWriter::Write(System.Char) */, L_197, L_198);
		int32_t L_199 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_199, 1));
	}

IL_0599:
	{
		int32_t L_200 = V_2;
		String_t* L_201 = V_1;
		int32_t L_202;
		L_202 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_201, NULL);
		if ((((int32_t)L_200) < ((int32_t)L_202)))
		{
			goto IL_057f;
		}
	}
	{
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_203 = __this->___e_4;
		VirtualActionInvoker1< Il2CppChar >::Invoke(14 /* System.Void System.IO.BinaryWriter::Write(System.Char) */, L_203, 0);
		int32_t L_204 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add(L_204, 1));
	}

IL_05b4:
	{
		int32_t L_205 = V_7;
		d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2* L_206 = (&__this->___d_3);
		cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF* L_207 = L_206->___b_1;
		if ((((int32_t)L_205) < ((int32_t)((int32_t)(((RuntimeArray*)L_207)->max_length)))))
		{
			goto IL_04a9;
		}
	}
	{
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_208 = __this->___e_4;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_209 = ___0_A_0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_210 = ___0_A_0;
		VirtualActionInvoker3< ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t >::Invoke(13 /* System.Void System.IO.BinaryWriter::Write(System.Byte[],System.Int32,System.Int32) */, L_208, L_209, 0, ((int32_t)(((RuntimeArray*)L_210)->max_length)));
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_211 = (&__this->___b_1);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_212 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_213;
		L_213 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(7 /* System.IO.Stream System.IO.BinaryWriter::get_BaseStream() */, L_212);
		int64_t L_214;
		L_214 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_213);
		L_211->___e_4 = L_214;
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_215 = __this->___e_4;
		int32_t L_216 = V_0;
		int64_t L_217;
		L_217 = VirtualFuncInvoker2< int64_t, int32_t, int32_t >::Invoke(9 /* System.Int64 System.IO.BinaryWriter::Seek(System.Int32,System.IO.SeekOrigin) */, L_215, L_216, 0);
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_218 = (&__this->___b_1);
		uint64_t* L_219 = (&L_218->___e_4);
		c_a_mC8410CC3B2FB053767D958B48C6BB152133F10B6(L_219, NULL);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_220 = __this->___e_4;
		e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777* L_221 = (&__this->___b_1);
		uint64_t L_222 = L_221->___e_4;
		VirtualActionInvoker1< uint64_t >::Invoke(22 /* System.Void System.IO.BinaryWriter::Write(System.UInt64) */, L_220, L_222);
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_223 = __this->___e_4;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_224;
		L_224 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(7 /* System.IO.Stream System.IO.BinaryWriter::get_BaseStream() */, L_223);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_225;
		L_225 = VirtualFuncInvoker0< ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* >::Invoke(30 /* System.Byte[] System.IO.MemoryStream::ToArray() */, ((MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2*)CastclassClass((RuntimeObject*)L_224, MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2_il2cpp_TypeInfo_var)));
		return L_225;
	}
}
// System.Void c::a()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void c_a_m68DACE5CC586ED73D8A47E1CAF221A5F3BA10E78 (c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57* __this, const RuntimeMethod* method) 
{
	{
		BinaryWriter_tFB94D67EDFA3F6A34744A163BDABE287FDF2ED1E* L_0 = __this->___e_4;
		VirtualActionInvoker0::Invoke(5 /* System.Void System.IO.BinaryWriter::Close() */, L_0);
		return;
	}
}
// System.Void c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void c__ctor_mCE86689EC3BFA79682A9DAD7E22DF7B0CE36DD2D (c_t7F65E177D884F69F349293FB2D51B2D5CC9E5C57* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: c/b
IL2CPP_EXTERN_C void b_t377CCBB7B97AADE524489D5766C28A547BBAB31B_marshal_pinvoke(const b_t377CCBB7B97AADE524489D5766C28A547BBAB31B& unmarshaled, b_t377CCBB7B97AADE524489D5766C28A547BBAB31B_marshaled_pinvoke& marshaled)
{
	marshaled.___a_0 = unmarshaled.___a_0;
	marshaled.___b_1 = unmarshaled.___b_1;
	if (unmarshaled.___c_2 != NULL)
	{
		il2cpp_array_size_t _unmarshaledc_Length = (unmarshaled.___c_2)->max_length;
		marshaled.___c_2 = il2cpp_codegen_marshal_allocate_array<a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6>(_unmarshaledc_Length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(_unmarshaledc_Length); i++)
		{
			(marshaled.___c_2)[i] = (unmarshaled.___c_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		marshaled.___c_2 = NULL;
	}
}
IL2CPP_EXTERN_C void b_t377CCBB7B97AADE524489D5766C28A547BBAB31B_marshal_pinvoke_back(const b_t377CCBB7B97AADE524489D5766C28A547BBAB31B_marshaled_pinvoke& marshaled, b_t377CCBB7B97AADE524489D5766C28A547BBAB31B& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C unmarshaleda_temp_0;
	memset((&unmarshaleda_temp_0), 0, sizeof(unmarshaleda_temp_0));
	unmarshaleda_temp_0 = marshaled.___a_0;
	unmarshaled.___a_0 = unmarshaleda_temp_0;
	uint32_t unmarshaledb_temp_1 = 0;
	unmarshaledb_temp_1 = marshaled.___b_1;
	unmarshaled.___b_1 = unmarshaledb_temp_1;
	if (marshaled.___c_2 != NULL)
	{
		if (unmarshaled.___c_2 == NULL)
		{
			unmarshaled.___c_2 = reinterpret_cast<aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2*>((aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2*)SZArrayNew(aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2_il2cpp_TypeInfo_var, 1));
			Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___c_2), (void*)reinterpret_cast<aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2*>((aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2*)SZArrayNew(aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2_il2cpp_TypeInfo_var, 1)));
		}
		il2cpp_array_size_t _arrayLength = (unmarshaled.___c_2)->max_length;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(_arrayLength); i++)
		{
			(unmarshaled.___c_2)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (marshaled.___c_2)[i]);
		}
	}
}
// Conversion method for clean up from marshalling of: c/b
IL2CPP_EXTERN_C void b_t377CCBB7B97AADE524489D5766C28A547BBAB31B_marshal_pinvoke_cleanup(b_t377CCBB7B97AADE524489D5766C28A547BBAB31B_marshaled_pinvoke& marshaled)
{
	if (marshaled.___c_2 != NULL)
	{
		il2cpp_codegen_marshal_free(marshaled.___c_2);
		marshaled.___c_2 = NULL;
	}
}
// Conversion methods for marshalling of: c/b
IL2CPP_EXTERN_C void b_t377CCBB7B97AADE524489D5766C28A547BBAB31B_marshal_com(const b_t377CCBB7B97AADE524489D5766C28A547BBAB31B& unmarshaled, b_t377CCBB7B97AADE524489D5766C28A547BBAB31B_marshaled_com& marshaled)
{
	marshaled.___a_0 = unmarshaled.___a_0;
	marshaled.___b_1 = unmarshaled.___b_1;
	if (unmarshaled.___c_2 != NULL)
	{
		il2cpp_array_size_t _unmarshaledc_Length = (unmarshaled.___c_2)->max_length;
		marshaled.___c_2 = il2cpp_codegen_marshal_allocate_array<a_t00E06D5713FD42C79E5600C3AEDDF7EF416521C6>(_unmarshaledc_Length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(_unmarshaledc_Length); i++)
		{
			(marshaled.___c_2)[i] = (unmarshaled.___c_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
		}
	}
	else
	{
		marshaled.___c_2 = NULL;
	}
}
IL2CPP_EXTERN_C void b_t377CCBB7B97AADE524489D5766C28A547BBAB31B_marshal_com_back(const b_t377CCBB7B97AADE524489D5766C28A547BBAB31B_marshaled_com& marshaled, b_t377CCBB7B97AADE524489D5766C28A547BBAB31B& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	a_tBEC5E8B2EF7FDD45D1BC2DB41C020C362C9A1A8C unmarshaleda_temp_0;
	memset((&unmarshaleda_temp_0), 0, sizeof(unmarshaleda_temp_0));
	unmarshaleda_temp_0 = marshaled.___a_0;
	unmarshaled.___a_0 = unmarshaleda_temp_0;
	uint32_t unmarshaledb_temp_1 = 0;
	unmarshaledb_temp_1 = marshaled.___b_1;
	unmarshaled.___b_1 = unmarshaledb_temp_1;
	if (marshaled.___c_2 != NULL)
	{
		if (unmarshaled.___c_2 == NULL)
		{
			unmarshaled.___c_2 = reinterpret_cast<aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2*>((aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2*)SZArrayNew(aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2_il2cpp_TypeInfo_var, 1));
			Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___c_2), (void*)reinterpret_cast<aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2*>((aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2*)SZArrayNew(aU5BU5D_tFF2FF19488F0C10FCD0BAE6A94CA1E8DE56FA5B2_il2cpp_TypeInfo_var, 1)));
		}
		il2cpp_array_size_t _arrayLength = (unmarshaled.___c_2)->max_length;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(_arrayLength); i++)
		{
			(unmarshaled.___c_2)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), (marshaled.___c_2)[i]);
		}
	}
}
// Conversion method for clean up from marshalling of: c/b
IL2CPP_EXTERN_C void b_t377CCBB7B97AADE524489D5766C28A547BBAB31B_marshal_com_cleanup(b_t377CCBB7B97AADE524489D5766C28A547BBAB31B_marshaled_com& marshaled)
{
	if (marshaled.___c_2 != NULL)
	{
		il2cpp_codegen_marshal_free(marshaled.___c_2);
		marshaled.___c_2 = NULL;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: c/c
IL2CPP_EXTERN_C void c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshal_pinvoke(const c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0& unmarshaled, c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_pinvoke& marshaled)
{
	marshaled.___a_0 = unmarshaled.___a_0;
	marshaled.___b_1 = unmarshaled.___b_1;
	marshaled.___c_2 = unmarshaled.___c_2;
	marshaled.___d_3 = il2cpp_codegen_marshal_string(unmarshaled.___d_3);
}
IL2CPP_EXTERN_C void c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshal_pinvoke_back(const c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_pinvoke& marshaled, c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0& unmarshaled)
{
	uint64_t unmarshaleda_temp_0 = 0;
	unmarshaleda_temp_0 = marshaled.___a_0;
	unmarshaled.___a_0 = unmarshaleda_temp_0;
	uint64_t unmarshaledb_temp_1 = 0;
	unmarshaledb_temp_1 = marshaled.___b_1;
	unmarshaled.___b_1 = unmarshaledb_temp_1;
	uint32_t unmarshaledc_temp_2 = 0;
	unmarshaledc_temp_2 = marshaled.___c_2;
	unmarshaled.___c_2 = unmarshaledc_temp_2;
	unmarshaled.___d_3 = il2cpp_codegen_marshal_string_result(marshaled.___d_3);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___d_3), (void*)il2cpp_codegen_marshal_string_result(marshaled.___d_3));
}
// Conversion method for clean up from marshalling of: c/c
IL2CPP_EXTERN_C void c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshal_pinvoke_cleanup(c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___d_3);
	marshaled.___d_3 = NULL;
}
// Conversion methods for marshalling of: c/c
IL2CPP_EXTERN_C void c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshal_com(const c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0& unmarshaled, c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_com& marshaled)
{
	marshaled.___a_0 = unmarshaled.___a_0;
	marshaled.___b_1 = unmarshaled.___b_1;
	marshaled.___c_2 = unmarshaled.___c_2;
	marshaled.___d_3 = il2cpp_codegen_marshal_bstring(unmarshaled.___d_3);
}
IL2CPP_EXTERN_C void c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshal_com_back(const c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_com& marshaled, c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0& unmarshaled)
{
	uint64_t unmarshaleda_temp_0 = 0;
	unmarshaleda_temp_0 = marshaled.___a_0;
	unmarshaled.___a_0 = unmarshaleda_temp_0;
	uint64_t unmarshaledb_temp_1 = 0;
	unmarshaledb_temp_1 = marshaled.___b_1;
	unmarshaled.___b_1 = unmarshaledb_temp_1;
	uint32_t unmarshaledc_temp_2 = 0;
	unmarshaledc_temp_2 = marshaled.___c_2;
	unmarshaled.___c_2 = unmarshaledc_temp_2;
	unmarshaled.___d_3 = il2cpp_codegen_marshal_bstring_result(marshaled.___d_3);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___d_3), (void*)il2cpp_codegen_marshal_bstring_result(marshaled.___d_3));
}
// Conversion method for clean up from marshalling of: c/c
IL2CPP_EXTERN_C void c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshal_com_cleanup(c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___d_3);
	marshaled.___d_3 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif


// Conversion methods for marshalling of: c/d
IL2CPP_EXTERN_C void d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2_marshal_pinvoke(const d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2& unmarshaled, d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2_marshaled_pinvoke& marshaled)
{
	marshaled.___a_0 = unmarshaled.___a_0;
	if (unmarshaled.___b_1 != NULL)
	{
		il2cpp_array_size_t _unmarshaledb_Length = (unmarshaled.___b_1)->max_length;
		marshaled.___b_1 = il2cpp_codegen_marshal_allocate_array<c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_pinvoke>(_unmarshaledb_Length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(_unmarshaledb_Length); i++)
		{
			c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshal_pinvoke((unmarshaled.___b_1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)), (marshaled.___b_1)[i]);
		}
	}
	else
	{
		marshaled.___b_1 = NULL;
	}
}
IL2CPP_EXTERN_C void d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2_marshal_pinvoke_back(const d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2_marshaled_pinvoke& marshaled, d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t unmarshaleda_temp_0 = 0;
	unmarshaleda_temp_0 = marshaled.___a_0;
	unmarshaled.___a_0 = unmarshaleda_temp_0;
	if (marshaled.___b_1 != NULL)
	{
		if (unmarshaled.___b_1 == NULL)
		{
			unmarshaled.___b_1 = reinterpret_cast<cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF*>((cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF*)SZArrayNew(cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF_il2cpp_TypeInfo_var, 1));
			Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___b_1), (void*)reinterpret_cast<cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF*>((cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF*)SZArrayNew(cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF_il2cpp_TypeInfo_var, 1)));
		}
		il2cpp_array_size_t _arrayLength = (unmarshaled.___b_1)->max_length;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(_arrayLength); i++)
		{
			c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0 _marshaled____b_1_i__unmarshaled;
			memset((&_marshaled____b_1_i__unmarshaled), 0, sizeof(_marshaled____b_1_i__unmarshaled));
			c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshal_pinvoke_back((marshaled.___b_1)[i], _marshaled____b_1_i__unmarshaled);
			(unmarshaled.___b_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _marshaled____b_1_i__unmarshaled);
		}
	}
}
// Conversion method for clean up from marshalling of: c/d
IL2CPP_EXTERN_C void d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2_marshal_pinvoke_cleanup(d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2_marshaled_pinvoke& marshaled)
{
	if (marshaled.___b_1 != NULL)
	{
		const il2cpp_array_size_t marshaled____b_1_CleanupLoopCount = 1;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(marshaled____b_1_CleanupLoopCount); i++)
		{
			c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshal_pinvoke_cleanup((marshaled.___b_1)[i]);
		}
		il2cpp_codegen_marshal_free(marshaled.___b_1);
		marshaled.___b_1 = NULL;
	}
}


// Conversion methods for marshalling of: c/d
IL2CPP_EXTERN_C void d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2_marshal_com(const d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2& unmarshaled, d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2_marshaled_com& marshaled)
{
	marshaled.___a_0 = unmarshaled.___a_0;
	if (unmarshaled.___b_1 != NULL)
	{
		il2cpp_array_size_t _unmarshaledb_Length = (unmarshaled.___b_1)->max_length;
		marshaled.___b_1 = il2cpp_codegen_marshal_allocate_array<c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshaled_com>(_unmarshaledb_Length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(_unmarshaledb_Length); i++)
		{
			c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshal_com((unmarshaled.___b_1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)), (marshaled.___b_1)[i]);
		}
	}
	else
	{
		marshaled.___b_1 = NULL;
	}
}
IL2CPP_EXTERN_C void d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2_marshal_com_back(const d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2_marshaled_com& marshaled, d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t unmarshaleda_temp_0 = 0;
	unmarshaleda_temp_0 = marshaled.___a_0;
	unmarshaled.___a_0 = unmarshaleda_temp_0;
	if (marshaled.___b_1 != NULL)
	{
		if (unmarshaled.___b_1 == NULL)
		{
			unmarshaled.___b_1 = reinterpret_cast<cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF*>((cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF*)SZArrayNew(cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF_il2cpp_TypeInfo_var, 1));
			Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___b_1), (void*)reinterpret_cast<cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF*>((cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF*)SZArrayNew(cU5BU5D_tCE1829F56B17247471E9231A707729ACB211D4AF_il2cpp_TypeInfo_var, 1)));
		}
		il2cpp_array_size_t _arrayLength = (unmarshaled.___b_1)->max_length;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(_arrayLength); i++)
		{
			c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0 _marshaled____b_1_i__unmarshaled;
			memset((&_marshaled____b_1_i__unmarshaled), 0, sizeof(_marshaled____b_1_i__unmarshaled));
			c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshal_com_back((marshaled.___b_1)[i], _marshaled____b_1_i__unmarshaled);
			(unmarshaled.___b_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), _marshaled____b_1_i__unmarshaled);
		}
	}
}
// Conversion method for clean up from marshalling of: c/d
IL2CPP_EXTERN_C void d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2_marshal_com_cleanup(d_t91252C73747E35BD6BF8F1B82C3D14A7C49A1FA2_marshaled_com& marshaled)
{
	if (marshaled.___b_1 != NULL)
	{
		const il2cpp_array_size_t marshaled____b_1_CleanupLoopCount = 1;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(marshaled____b_1_CleanupLoopCount); i++)
		{
			c_t6D7AF158A10F936C66079BD1BF16CA9074FDDBF0_marshal_com_cleanup((marshaled.___b_1)[i]);
		}
		il2cpp_codegen_marshal_free(marshaled.___b_1);
		marshaled.___b_1 = NULL;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: c/e
IL2CPP_EXTERN_C void e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777_marshal_pinvoke(const e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777& unmarshaled, e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777_marshaled_pinvoke& marshaled)
{
	marshaled.___a_0 = il2cpp_codegen_marshal_string(unmarshaled.___a_0);
	marshaled.___b_1 = unmarshaled.___b_1;
	marshaled.___c_2 = il2cpp_codegen_marshal_string(unmarshaled.___c_2);
	marshaled.___d_3 = il2cpp_codegen_marshal_string(unmarshaled.___d_3);
	marshaled.___e_4 = unmarshaled.___e_4;
	marshaled.___f_5 = unmarshaled.___f_5;
	marshaled.___g_6 = unmarshaled.___g_6;
	marshaled.___h_7 = unmarshaled.___h_7;
}
IL2CPP_EXTERN_C void e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777_marshal_pinvoke_back(const e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777_marshaled_pinvoke& marshaled, e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777& unmarshaled)
{
	unmarshaled.___a_0 = il2cpp_codegen_marshal_string_result(marshaled.___a_0);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___a_0), (void*)il2cpp_codegen_marshal_string_result(marshaled.___a_0));
	uint32_t unmarshaledb_temp_1 = 0;
	unmarshaledb_temp_1 = marshaled.___b_1;
	unmarshaled.___b_1 = unmarshaledb_temp_1;
	unmarshaled.___c_2 = il2cpp_codegen_marshal_string_result(marshaled.___c_2);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___c_2), (void*)il2cpp_codegen_marshal_string_result(marshaled.___c_2));
	unmarshaled.___d_3 = il2cpp_codegen_marshal_string_result(marshaled.___d_3);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___d_3), (void*)il2cpp_codegen_marshal_string_result(marshaled.___d_3));
	uint64_t unmarshalede_temp_4 = 0;
	unmarshalede_temp_4 = marshaled.___e_4;
	unmarshaled.___e_4 = unmarshalede_temp_4;
	uint32_t unmarshaledf_temp_5 = 0;
	unmarshaledf_temp_5 = marshaled.___f_5;
	unmarshaled.___f_5 = unmarshaledf_temp_5;
	uint32_t unmarshaledg_temp_6 = 0;
	unmarshaledg_temp_6 = marshaled.___g_6;
	unmarshaled.___g_6 = unmarshaledg_temp_6;
	uint32_t unmarshaledh_temp_7 = 0;
	unmarshaledh_temp_7 = marshaled.___h_7;
	unmarshaled.___h_7 = unmarshaledh_temp_7;
}
// Conversion method for clean up from marshalling of: c/e
IL2CPP_EXTERN_C void e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777_marshal_pinvoke_cleanup(e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___a_0);
	marshaled.___a_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___c_2);
	marshaled.___c_2 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___d_3);
	marshaled.___d_3 = NULL;
}
// Conversion methods for marshalling of: c/e
IL2CPP_EXTERN_C void e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777_marshal_com(const e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777& unmarshaled, e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777_marshaled_com& marshaled)
{
	marshaled.___a_0 = il2cpp_codegen_marshal_bstring(unmarshaled.___a_0);
	marshaled.___b_1 = unmarshaled.___b_1;
	marshaled.___c_2 = il2cpp_codegen_marshal_bstring(unmarshaled.___c_2);
	marshaled.___d_3 = il2cpp_codegen_marshal_bstring(unmarshaled.___d_3);
	marshaled.___e_4 = unmarshaled.___e_4;
	marshaled.___f_5 = unmarshaled.___f_5;
	marshaled.___g_6 = unmarshaled.___g_6;
	marshaled.___h_7 = unmarshaled.___h_7;
}
IL2CPP_EXTERN_C void e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777_marshal_com_back(const e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777_marshaled_com& marshaled, e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777& unmarshaled)
{
	unmarshaled.___a_0 = il2cpp_codegen_marshal_bstring_result(marshaled.___a_0);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___a_0), (void*)il2cpp_codegen_marshal_bstring_result(marshaled.___a_0));
	uint32_t unmarshaledb_temp_1 = 0;
	unmarshaledb_temp_1 = marshaled.___b_1;
	unmarshaled.___b_1 = unmarshaledb_temp_1;
	unmarshaled.___c_2 = il2cpp_codegen_marshal_bstring_result(marshaled.___c_2);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___c_2), (void*)il2cpp_codegen_marshal_bstring_result(marshaled.___c_2));
	unmarshaled.___d_3 = il2cpp_codegen_marshal_bstring_result(marshaled.___d_3);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___d_3), (void*)il2cpp_codegen_marshal_bstring_result(marshaled.___d_3));
	uint64_t unmarshalede_temp_4 = 0;
	unmarshalede_temp_4 = marshaled.___e_4;
	unmarshaled.___e_4 = unmarshalede_temp_4;
	uint32_t unmarshaledf_temp_5 = 0;
	unmarshaledf_temp_5 = marshaled.___f_5;
	unmarshaled.___f_5 = unmarshaledf_temp_5;
	uint32_t unmarshaledg_temp_6 = 0;
	unmarshaledg_temp_6 = marshaled.___g_6;
	unmarshaled.___g_6 = unmarshaledg_temp_6;
	uint32_t unmarshaledh_temp_7 = 0;
	unmarshaledh_temp_7 = marshaled.___h_7;
	unmarshaled.___h_7 = unmarshaledh_temp_7;
}
// Conversion method for clean up from marshalling of: c/e
IL2CPP_EXTERN_C void e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777_marshal_com_cleanup(e_t46A5A7038DBE7E47EBDBC72ADDF791921AAC9777_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___a_0);
	marshaled.___a_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___c_2);
	marshaled.___c_2 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___d_3);
	marshaled.___d_3 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____stringLength_4;
		return L_0;
	}
}
