﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void DotfuscatorAttribute::.ctor(System.String,System.Int32)
extern void DotfuscatorAttribute__ctor_m6C48D0E19C41E11223F9C5464EB27BCD660425EC (void);
// 0x00000002 System.String DotfuscatorAttribute::a()
extern void DotfuscatorAttribute_a_mA3B52A263303F2491FF01DAEE9AC1A842CA8E123 (void);
// 0x00000003 System.Int32 DotfuscatorAttribute::c()
extern void DotfuscatorAttribute_c_m576B5AF32A0617E2C3B23B5384A3675B9794909D (void);
// 0x00000004 System.Byte[] Unity.FontABTool.UnityFontABTool::PacKFontAB(System.Byte[],System.String,System.Single,System.Single,System.Single,System.Single)
extern void UnityFontABTool_PacKFontAB_mEA2AD10F9017F1D5A401BEA9A6DA86CA639BADDC (void);
// 0x00000005 System.Byte[] Unity.FontABTool.UnityFontABTool::a(System.Byte[],System.String,System.Single,System.Single,System.Single,System.Single)
extern void UnityFontABTool_a_m34550A50BEBBEB488C37E50155EBC33CE9C63819 (void);
// 0x00000006 System.Void Unity.FontABTool.UnityFontABTool::.ctor()
extern void UnityFontABTool__ctor_mF821CE89D979CE95D05DF50837598828C7C4C934 (void);
// 0x00000007 System.Void b::.ctor(System.Byte[])
extern void b__ctor_m5D218ADFB9B12CAEBCFB3E050637FA0BEF33705D (void);
// 0x00000008 System.Void b::a(System.UInt32&)
extern void b_a_m0B288ECDA5A92480B615876EA2B09ECDAFC27335 (void);
// 0x00000009 System.Void b::a(System.UInt64&)
extern void b_a_mA29C97334753DB15E7A7F615EBCDBD17B8882970 (void);
// 0x0000000A System.Void b::b()
extern void b_b_m3EEA39E861DB95D43365715F1CC72391418DB524 (void);
// 0x0000000B System.Void b::b(System.Int64)
extern void b_b_m26D9779F99EB6F88BC55D13609AD116C70C234F9 (void);
// 0x0000000C System.Void b::a(System.Int64)
extern void b_a_m4763D12C4B10DCBB806877202AB15E4A3B7F1DE6 (void);
// 0x0000000D System.Byte[] b::a(System.String,System.Byte[],System.Single,System.Single,System.Single,System.Single,System.Reflection.MethodInfo)
extern void b_a_m83904CAC9196C33E326CF37A434BCF9F4AA1C646 (void);
// 0x0000000E System.Void b::a()
extern void b_a_m0EB9556527D166F142FCB52A806DF275DDE1E6A4 (void);
// 0x0000000F System.Void b::.cctor()
extern void b__cctor_mF0E9901289C18E88DCAC4C9B4D543B9520884441 (void);
// 0x00000010 System.UInt64 c::a(System.UInt64,System.UInt64)
extern void c_a_m4EBA04D0095E840BB3DB1EA942C3BA9104450E3E (void);
// 0x00000011 System.Void c::a(System.Int64)
extern void c_a_mCE5ABC97A65DF6C7C6651DC4FFA7BACC4D82DD85 (void);
// 0x00000012 System.Void c::a(System.UInt16&)
extern void c_a_m091F763E46A6CD5B09755C0CBCA803F85F1C3A79 (void);
// 0x00000013 System.Void c::a(System.UInt32&)
extern void c_a_mDFCC496EEE1479D0DFCE8DBE3D26D8BD8E19988A (void);
// 0x00000014 System.Void c::a(System.UInt64&)
extern void c_a_mC8410CC3B2FB053767D958B48C6BB152133F10B6 (void);
// 0x00000015 System.Byte[] c::a(System.Byte[],System.String,System.String,System.UInt32)
extern void c_a_m1BBA40310965FA9C86772427E8AD85FE919E422F (void);
// 0x00000016 System.Void c::a()
extern void c_a_m68DACE5CC586ED73D8A47E1CAF221A5F3BA10E78 (void);
// 0x00000017 System.Void c::.ctor()
extern void c__ctor_mCE86689EC3BFA79682A9DAD7E22DF7B0CE36DD2D (void);
static Il2CppMethodPointer s_methodPointers[23] = 
{
	DotfuscatorAttribute__ctor_m6C48D0E19C41E11223F9C5464EB27BCD660425EC,
	DotfuscatorAttribute_a_mA3B52A263303F2491FF01DAEE9AC1A842CA8E123,
	DotfuscatorAttribute_c_m576B5AF32A0617E2C3B23B5384A3675B9794909D,
	UnityFontABTool_PacKFontAB_mEA2AD10F9017F1D5A401BEA9A6DA86CA639BADDC,
	UnityFontABTool_a_m34550A50BEBBEB488C37E50155EBC33CE9C63819,
	UnityFontABTool__ctor_mF821CE89D979CE95D05DF50837598828C7C4C934,
	b__ctor_m5D218ADFB9B12CAEBCFB3E050637FA0BEF33705D,
	b_a_m0B288ECDA5A92480B615876EA2B09ECDAFC27335,
	b_a_mA29C97334753DB15E7A7F615EBCDBD17B8882970,
	b_b_m3EEA39E861DB95D43365715F1CC72391418DB524,
	b_b_m26D9779F99EB6F88BC55D13609AD116C70C234F9,
	b_a_m4763D12C4B10DCBB806877202AB15E4A3B7F1DE6,
	b_a_m83904CAC9196C33E326CF37A434BCF9F4AA1C646,
	b_a_m0EB9556527D166F142FCB52A806DF275DDE1E6A4,
	b__cctor_mF0E9901289C18E88DCAC4C9B4D543B9520884441,
	c_a_m4EBA04D0095E840BB3DB1EA942C3BA9104450E3E,
	c_a_mCE5ABC97A65DF6C7C6651DC4FFA7BACC4D82DD85,
	c_a_m091F763E46A6CD5B09755C0CBCA803F85F1C3A79,
	c_a_mDFCC496EEE1479D0DFCE8DBE3D26D8BD8E19988A,
	c_a_mC8410CC3B2FB053767D958B48C6BB152133F10B6,
	c_a_m1BBA40310965FA9C86772427E8AD85FE919E422F,
	c_a_m68DACE5CC586ED73D8A47E1CAF221A5F3BA10E78,
	c__ctor_mCE86689EC3BFA79682A9DAD7E22DF7B0CE36DD2D,
};
static const int32_t s_InvokerIndices[23] = 
{
	1154,
	1708,
	1698,
	1936,
	1936,
	1766,
	1576,
	3477,
	3477,
	1766,
	1572,
	1572,
	101,
	1766,
	3572,
	3063,
	1572,
	3477,
	3477,
	3477,
	369,
	1766,
	1766,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Unity_FontABTool_CodeGenModule;
const Il2CppCodeGenModule g_Unity_FontABTool_CodeGenModule = 
{
	"Unity.FontABTool.dll",
	23,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
