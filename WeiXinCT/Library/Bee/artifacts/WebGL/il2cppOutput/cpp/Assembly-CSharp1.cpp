﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>



// RemoteServices
struct RemoteServices_t1C3C2F5B3D4D6F99D184D692B3EBAB379E94C935;
// System.String
struct String_t;

IL2CPP_EXTERN_C String_t* _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteServices
struct RemoteServices_t1C3C2F5B3D4D6F99D184D692B3EBAB379E94C935  : public RuntimeObject
{
	// System.String RemoteServices::_defaultHostServer
	String_t* ____defaultHostServer_0;
	// System.String RemoteServices::_fallbackHostServer
	String_t* ____fallbackHostServer_1;
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// RemoteServices

// RemoteServices

// System.String
struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.String

// System.Void

// System.Void
#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B (String_t* ___0_str0, String_t* ___1_str1, String_t* ___2_str2, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RemoteServices::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RemoteServices__ctor_m9C9D2E5D7B003C687EF7B42526DC595B9E5A27A5 (RemoteServices_t1C3C2F5B3D4D6F99D184D692B3EBAB379E94C935* __this, String_t* ___0_defaultHostServer, String_t* ___1_fallbackHostServer, const RuntimeMethod* method) 
{
	{
		// public RemoteServices(string defaultHostServer, string fallbackHostServer)
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		// _defaultHostServer = defaultHostServer;
		String_t* L_0 = ___0_defaultHostServer;
		__this->____defaultHostServer_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____defaultHostServer_0), (void*)L_0);
		// _fallbackHostServer = fallbackHostServer;
		String_t* L_1 = ___1_fallbackHostServer;
		__this->____fallbackHostServer_1 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____fallbackHostServer_1), (void*)L_1);
		// }
		return;
	}
}
// System.String RemoteServices::YooAsset.IRemoteServices.GetRemoteMainURL(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* RemoteServices_YooAsset_IRemoteServices_GetRemoteMainURL_m80A98B59950800895374F5940DA72C4F3881FAEC (RemoteServices_t1C3C2F5B3D4D6F99D184D692B3EBAB379E94C935* __this, String_t* ___0_fileName, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return $"{_defaultHostServer}/{fileName}";
		String_t* L_0 = __this->____defaultHostServer_0;
		String_t* L_1 = ___0_fileName;
		String_t* L_2;
		L_2 = String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B(L_0, _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1, L_1, NULL);
		return L_2;
	}
}
// System.String RemoteServices::YooAsset.IRemoteServices.GetRemoteFallbackURL(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* RemoteServices_YooAsset_IRemoteServices_GetRemoteFallbackURL_m956EE47AEE6B58E35FC33EB3B9834887AFA39588 (RemoteServices_t1C3C2F5B3D4D6F99D184D692B3EBAB379E94C935* __this, String_t* ___0_fileName, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return $"{_fallbackHostServer}/{fileName}";
		String_t* L_0 = __this->____fallbackHostServer_1;
		String_t* L_1 = ___0_fileName;
		String_t* L_2;
		L_2 = String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B(L_0, _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1, L_1, NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
