﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* WXAssetBundleRequest_Callback_mBDBC695795945916CE988FA73D548FBFD70B9CCB_RuntimeMethod_var;
extern const RuntimeMethod* WXSDKManagerHandler_DelegateOnCloseEvent_m51F987A089DD23E07359E77B998275CA8ADA64F1_RuntimeMethod_var;
extern const RuntimeMethod* WXSDKManagerHandler_DelegateOnErrorEvent_m84321402DEA4A472CEF6ECB9A0608A7A0ADB3A6A_RuntimeMethod_var;
extern const RuntimeMethod* WXSDKManagerHandler_DelegateOnMessageEvent_mB4CC33FEFEA70BE79535B3D1CA87F4FEEEF1B418_RuntimeMethod_var;



// 0x00000001 System.Void A.A::.ctor()
extern void A__ctor_mD2C8A206ADA7E9B311035F305B4765101C19AE5A (void);
// 0x00000002 System.Void A.a::.ctor(System.Byte)
extern void a__ctor_m9CBB7D8CC082AB77F1A7BACFADD193AD858C5E44 (void);
// 0x00000003 System.Void A.a::.ctor(System.Byte[])
extern void a__ctor_mD502D73051687A347BD946B842E5764424A9C7FA (void);
// 0x00000004 System.Void PlayerPrefs::SetInt(System.String,System.Int32)
extern void PlayerPrefs_SetInt_m0B50B3EFF113A2E6A01E18CB839AC32D42CE6081 (void);
// 0x00000005 System.Int32 PlayerPrefs::GetInt(System.String,System.Int32)
extern void PlayerPrefs_GetInt_mFDDE681FB8681AF55487A406939A63879A1971BD (void);
// 0x00000006 System.Void PlayerPrefs::SetString(System.String,System.String)
extern void PlayerPrefs_SetString_m240AFEEBAB31ACA324135E1231FB2A60C8099AC7 (void);
// 0x00000007 System.String PlayerPrefs::GetString(System.String,System.String)
extern void PlayerPrefs_GetString_m9A8A6866EE5AD9B71A0070397F6031471AD0AAC2 (void);
// 0x00000008 System.Void PlayerPrefs::SetFloat(System.String,System.Single)
extern void PlayerPrefs_SetFloat_mAB94151B0C3595B0F5553E0725E61C722DDCEC52 (void);
// 0x00000009 System.Single PlayerPrefs::GetFloat(System.String,System.Single)
extern void PlayerPrefs_GetFloat_mF0318A2480AEDCFD4A390145FD140CC2CB96D859 (void);
// 0x0000000A System.Void PlayerPrefs::DeleteAll()
extern void PlayerPrefs_DeleteAll_m9C8A6B970FC696EFAFDEA765A5EE84D6C2F92E9A (void);
// 0x0000000B System.Void PlayerPrefs::DeleteKey(System.String)
extern void PlayerPrefs_DeleteKey_mEA684FE4DADA7F05D3938512644842889EB69175 (void);
// 0x0000000C System.Boolean PlayerPrefs::HasKey(System.String)
extern void PlayerPrefs_HasKey_mDB54F9599829D4B307FEDC34275A4C01ACAE7B03 (void);
// 0x0000000D System.Void PlayerPrefs::Save()
extern void PlayerPrefs_Save_m5E9E78CA1C53E90DC6CA592F66DD043394CA6E65 (void);
// 0x0000000E System.Void WeChatWASM.Cloud::A(System.String,System.String,System.String,System.String,System.String,System.String)
extern void Cloud_A_m15C3DC69C6BB05B0B6B4D6C94A5972D75457160D (void);
// 0x0000000F System.Void WeChatWASM.Cloud::A(System.String)
extern void Cloud_A_mE4C7ABF027A5F997756BA5CB4AD2A27364D4CC97 (void);
// 0x00000010 System.String WeChatWASM.Cloud::a(System.String)
extern void Cloud_a_mAD988736B81CE3DB98EAAF53AFE1D2BFF48D6707 (void);
// 0x00000011 System.Void WeChatWASM.Cloud::Init(WeChatWASM.CallFunctionInitParam)
extern void Cloud_Init_mEA14E763F7BF8CE496A2CAA405A8873AF655B9E4 (void);
// 0x00000012 System.Void WeChatWASM.Cloud::CallFunction(WeChatWASM.CallFunctionParam)
extern void Cloud_CallFunction_mBAD01AD8443563D6D48F8B5B8A63B53E23C8B200 (void);
// 0x00000013 System.String WeChatWASM.Cloud::CloudID(System.String)
extern void Cloud_CloudID_mDA96C9642E8D509C84F8A9CBD053838D625FEAEB (void);
// 0x00000014 System.Void WeChatWASM.Cloud::.ctor()
extern void Cloud__ctor_mE7AEF87A4D4265D6879B364F02180F9AA1743DD5 (void);
// 0x00000015 System.Void WeChatWASM.WXAdBaseStyle::.ctor(System.String,WeChatWASM.Style)
extern void WXAdBaseStyle__ctor_m2E315FD29901FB14612A426ED3161CA5B8BCC7D1 (void);
// 0x00000016 System.Int32 WeChatWASM.WXAdBaseStyle::get_left()
extern void WXAdBaseStyle_get_left_m3A0415B9A751CDA668346C65786AE34FF83DB1A3 (void);
// 0x00000017 System.Void WeChatWASM.WXAdBaseStyle::set_left(System.Int32)
extern void WXAdBaseStyle_set_left_m17E0D81D2AE0B3964A2719BC7B5B35220286A36B (void);
// 0x00000018 System.Int32 WeChatWASM.WXAdBaseStyle::get_top()
extern void WXAdBaseStyle_get_top_m46A709DC852BFAA08A930182AE375582979AD645 (void);
// 0x00000019 System.Void WeChatWASM.WXAdBaseStyle::set_top(System.Int32)
extern void WXAdBaseStyle_set_top_m088BF2B9990B1DF22BCF82EF85D34708F6F6DC68 (void);
// 0x0000001A System.Int32 WeChatWASM.WXAdBaseStyle::get_width()
extern void WXAdBaseStyle_get_width_m7763E75B0F6AEAA43CC5CA0F77F13AEA727640F9 (void);
// 0x0000001B System.Void WeChatWASM.WXAdBaseStyle::set_width(System.Int32)
extern void WXAdBaseStyle_set_width_mB10CA97D4E5451D3DDE1FE70DF8658593D47DAA1 (void);
// 0x0000001C System.Int32 WeChatWASM.WXAdBaseStyle::get_height()
extern void WXAdBaseStyle_get_height_mC533A17CE0262C1539F57C283B8699C2AF0CD43C (void);
// 0x0000001D System.Void WeChatWASM.WXAdBaseStyle::set_height(System.Int32)
extern void WXAdBaseStyle_set_height_m3C1FFBB10CDDE46B8F80A81E420896C188682310 (void);
// 0x0000001E System.Int32 WeChatWASM.WXAdBaseStyle::get_realHeight()
extern void WXAdBaseStyle_get_realHeight_mC6A4D7FA1058B6F5FAC9D3A994DBE319F8F20A33 (void);
// 0x0000001F System.Int32 WeChatWASM.WXAdBaseStyle::get_realWidth()
extern void WXAdBaseStyle_get_realWidth_m9C8FCBECDEFCF0D4199999AA8951AD9618355DA7 (void);
// 0x00000020 System.Void WeChatWASM.WXAdCustomStyle::.ctor(System.String,WeChatWASM.CustomStyle)
extern void WXAdCustomStyle__ctor_m356128F9CF764392EC032C7637049E9363E2F2E2 (void);
// 0x00000021 System.Int32 WeChatWASM.WXAdCustomStyle::get_left()
extern void WXAdCustomStyle_get_left_m5B478BE3F137A7172C2DBC78EAFDBF480F343EC1 (void);
// 0x00000022 System.Void WeChatWASM.WXAdCustomStyle::set_left(System.Int32)
extern void WXAdCustomStyle_set_left_m120900208F4C25304170AFB2B3C15768CEB39A3E (void);
// 0x00000023 System.Int32 WeChatWASM.WXAdCustomStyle::get_top()
extern void WXAdCustomStyle_get_top_mB439372EA50882CDABD5230250F49C23FDF31724 (void);
// 0x00000024 System.Void WeChatWASM.WXAdCustomStyle::set_top(System.Int32)
extern void WXAdCustomStyle_set_top_m77C4C1F0102BD83D1A8D9766154909A68816A9ED (void);
// 0x00000025 System.String WeChatWASM.WXAssetBundleRequest::get_url()
extern void WXAssetBundleRequest_get_url_m4E62A30BDFA2B3F4BAEE9BC349BA1837AAA306F9 (void);
// 0x00000026 System.Void WeChatWASM.WXAssetBundleRequest::Dispose()
extern void WXAssetBundleRequest_Dispose_mB4B23267F91B9DAED647A0FAC9E177061C3EF4A6 (void);
// 0x00000027 UnityEngine.AssetBundle WeChatWASM.WXAssetBundleRequest::get_assetBundle()
extern void WXAssetBundleRequest_get_assetBundle_mADF08C636E0460A882567267E8E42E940FC11069 (void);
// 0x00000028 System.Void WeChatWASM.WXAssetBundleRequest::.ctor(System.String,System.UInt32,System.UInt64)
extern void WXAssetBundleRequest__ctor_m3B42CB55647F948A3BF8DA23690AD87CC104BB31 (void);
// 0x00000029 System.Void WeChatWASM.WXAssetBundleRequest::Callback(System.IntPtr,System.UInt32,System.IntPtr)
extern void WXAssetBundleRequest_Callback_mBDBC695795945916CE988FA73D548FBFD70B9CCB (void);
// 0x0000002A System.Void WeChatWASM.WXAssetBundleRequest::A(System.String,System.String,WeChatWASM.WXAssetBundleRequest/WXAssetBundleCallBack)
extern void WXAssetBundleRequest_A_mA24EAF7B3D9C88B75FA829092E0D513223E8DF8C (void);
// 0x0000002B System.Object WeChatWASM.WXAssetBundleRequest::get_Current()
extern void WXAssetBundleRequest_get_Current_mD9E608C41CA90039DE2B97F90ECD14D1B21FE34A (void);
// 0x0000002C System.Boolean WeChatWASM.WXAssetBundleRequest::MoveNext()
extern void WXAssetBundleRequest_MoveNext_m7DFC38CFFDECD8AD6D7F113918434637B1B0FD41 (void);
// 0x0000002D System.Void WeChatWASM.WXAssetBundleRequest::Reset()
extern void WXAssetBundleRequest_Reset_m49A3E7EE68558BEC80F12FC8469F7CFE84C567D3 (void);
// 0x0000002E System.Void WeChatWASM.WXAssetBundleRequest::.cctor()
extern void WXAssetBundleRequest__cctor_mB5C3FB517A545F506CB1B86E14153BEA69B25160 (void);
// 0x0000002F System.Void WeChatWASM.WXAssetBundleRequest/WXAssetBundleCallBack::.ctor(System.Object,System.IntPtr)
extern void WXAssetBundleCallBack__ctor_m27421744C2E47B438185B4C8EA73D554FA4692AF (void);
// 0x00000030 System.Void WeChatWASM.WXAssetBundleRequest/WXAssetBundleCallBack::Invoke(System.IntPtr,System.UInt32,System.IntPtr)
extern void WXAssetBundleCallBack_Invoke_mD5A374766A36A59D5AB27EE0185194E1211ECC88 (void);
// 0x00000031 System.IAsyncResult WeChatWASM.WXAssetBundleRequest/WXAssetBundleCallBack::BeginInvoke(System.IntPtr,System.UInt32,System.IntPtr,System.AsyncCallback,System.Object)
extern void WXAssetBundleCallBack_BeginInvoke_m2EB36FDACC854B2742272609225DDA9CAC2B6AA6 (void);
// 0x00000032 System.Void WeChatWASM.WXAssetBundleRequest/WXAssetBundleCallBack::EndInvoke(System.IAsyncResult)
extern void WXAssetBundleCallBack_EndInvoke_m7AE166BB184BDA3F87ECE3713E35B1228340382D (void);
// 0x00000033 System.Void WeChatWASM.WXAssetBundle::.ctor(UnityEngine.AssetBundle,System.String)
extern void WXAssetBundle__ctor_m4C32474A1117F69306F9BA2C70F4C7372494908B (void);
// 0x00000034 System.String WeChatWASM.WXAssetBundle::get_name()
extern void WXAssetBundle_get_name_mD43C1CD9CDA01C71B2B110C3250192C6989AF9F7 (void);
// 0x00000035 System.Int32 WeChatWASM.WXAssetBundle::GetInstanceID()
extern void WXAssetBundle_GetInstanceID_mB02B0E502911E51451A4C9C4983AD38781C05ED5 (void);
// 0x00000036 System.Int32 WeChatWASM.WXAssetBundle::GetHashCode()
extern void WXAssetBundle_GetHashCode_mEA5A33D95A8BF7F821D87F8FB25EF5E3B90F823E (void);
// 0x00000037 System.String WeChatWASM.WXAssetBundle::ToString()
extern void WXAssetBundle_ToString_mD438A2D06A9950C09D534516D6ACB9D7F5232DA2 (void);
// 0x00000038 System.Void WeChatWASM.WXAssetBundle::A(System.Int32,System.Int32)
extern void WXAssetBundle_A_m4D3050923B6F9E263E3CFEE8C0075799D311F1CF (void);
// 0x00000039 System.String WeChatWASM.WXAssetBundle::A()
extern void WXAssetBundle_A_mCB6917BB48B1ACCD583644B97CE5A4C036CA00B5 (void);
// 0x0000003A System.Void WeChatWASM.WXAssetBundle::UnloadbyPath(System.String)
extern void WXAssetBundle_UnloadbyPath_mA8BF25943B32E7E11CE57BFF13FFC5FACC607905 (void);
// 0x0000003B System.Void WeChatWASM.WXAssetBundle::UnCleanbyPath(System.String)
extern void WXAssetBundle_UnCleanbyPath_mFEB78394B5A840A3275AE09266AFC76E1B06FC13 (void);
// 0x0000003C System.Boolean WeChatWASM.WXAssetBundle::CheckWXFSReady()
extern void WXAssetBundle_CheckWXFSReady_m84B9C318CDF0C2C36E5196DD7CB6D26F4D28031D (void);
// 0x0000003D System.Boolean WeChatWASM.WXAssetBundle::get_isStreamedSceneAssetBundle()
extern void WXAssetBundle_get_isStreamedSceneAssetBundle_mDAA16E8C842AEC7A858967EB3E924AA446D72217 (void);
// 0x0000003E WeChatWASM.WXAssetBundleRequest WeChatWASM.WXAssetBundle::LoadFromFileAsync(System.String)
extern void WXAssetBundle_LoadFromFileAsync_mB95FB0D4248FC3E9099FB2555BDAC35601519590 (void);
// 0x0000003F WeChatWASM.WXAssetBundleRequest WeChatWASM.WXAssetBundle::LoadFromFileAsync(System.String,System.UInt32)
extern void WXAssetBundle_LoadFromFileAsync_m09DD7B4FD66E5B76BCFF59E4C51388F3D55318B1 (void);
// 0x00000040 WeChatWASM.WXAssetBundleRequest WeChatWASM.WXAssetBundle::LoadFromFileAsync(System.String,System.UInt32,System.UInt64)
extern void WXAssetBundle_LoadFromFileAsync_mDEEBC0F87D86FD9E5B6FEB3ED820D2408038FF93 (void);
// 0x00000041 UnityEngine.AssetBundle WeChatWASM.WXAssetBundle::LoadFromFile(System.String)
extern void WXAssetBundle_LoadFromFile_m82E25FDE1ECFBB8E110307C474556406BFE1EC9B (void);
// 0x00000042 UnityEngine.AssetBundle WeChatWASM.WXAssetBundle::LoadFromFile(System.String,System.UInt32)
extern void WXAssetBundle_LoadFromFile_m3BB9915F9BCA23F55EED463F6D1C6C6EDFBC2E48 (void);
// 0x00000043 UnityEngine.AssetBundle WeChatWASM.WXAssetBundle::LoadFromFile(System.String,System.UInt32,System.UInt64)
extern void WXAssetBundle_LoadFromFile_mB7EB45F2F35A06DD46316A08DCC1DD61B87263AF (void);
// 0x00000044 UnityEngine.AssetBundleRequest WeChatWASM.WXAssetBundle::LoadAssetAsync(System.String)
extern void WXAssetBundle_LoadAssetAsync_m63819FE0F06BAA3F949BE93CB883BF6393FF7215 (void);
// 0x00000045 UnityEngine.AssetBundleRequest WeChatWASM.WXAssetBundle::LoadAssetAsync(System.String)
// 0x00000046 UnityEngine.AssetBundleRequest WeChatWASM.WXAssetBundle::LoadAssetAsync(System.String,System.Type)
extern void WXAssetBundle_LoadAssetAsync_m7EA02C544C6F69DC1FC77554B720EE26BE051E1A (void);
// 0x00000047 UnityEngine.Object WeChatWASM.WXAssetBundle::LoadAsset(System.String)
extern void WXAssetBundle_LoadAsset_mBF5CCED5B4E9CCABC3D0FD6A533FA95E7C6C69D7 (void);
// 0x00000048 T WeChatWASM.WXAssetBundle::LoadAsset(System.String)
// 0x00000049 UnityEngine.Object WeChatWASM.WXAssetBundle::LoadAsset(System.String,System.Type)
extern void WXAssetBundle_LoadAsset_mD816C47ABEE468D17BA15C39C9E5F91BCC48B746 (void);
// 0x0000004A A[] WeChatWASM.WXAssetBundle::A(UnityEngine.Object[])
// 0x0000004B UnityEngine.Object[] WeChatWASM.WXAssetBundle::LoadAllAssets()
extern void WXAssetBundle_LoadAllAssets_mFAE50BA36101BC5F8227109753D92BC718B54064 (void);
// 0x0000004C T[] WeChatWASM.WXAssetBundle::LoadAllAssets()
// 0x0000004D UnityEngine.Object[] WeChatWASM.WXAssetBundle::LoadAllAssets(System.Type)
extern void WXAssetBundle_LoadAllAssets_mA82F86D685321E6DA9EBA80CC43F351C6471C7BA (void);
// 0x0000004E UnityEngine.AssetBundleRequest WeChatWASM.WXAssetBundle::LoadAllAssetsAsync()
extern void WXAssetBundle_LoadAllAssetsAsync_m6B827DF80E4BAADEA2462D15081B9DECDC930546 (void);
// 0x0000004F UnityEngine.AssetBundleRequest WeChatWASM.WXAssetBundle::LoadAllAssetsAsync()
// 0x00000050 UnityEngine.AssetBundleRequest WeChatWASM.WXAssetBundle::LoadAllAssetsAsync(System.Type)
extern void WXAssetBundle_LoadAllAssetsAsync_m04A8819A9F19C3BFBD1BFC1EF7C7BF0E9E70C33D (void);
// 0x00000051 System.String[] WeChatWASM.WXAssetBundle::AllAssetNames()
extern void WXAssetBundle_AllAssetNames_mE8E0834C89C63B6440F45949084D50FD4C9FC3A2 (void);
// 0x00000052 System.String[] WeChatWASM.WXAssetBundle::GetAllAssetNames()
extern void WXAssetBundle_GetAllAssetNames_mD846A328EC234B265AC14B67FB17CA1DD0565AB8 (void);
// 0x00000053 System.Void WeChatWASM.WXAssetBundle::UnloadAllAssetBundles(System.Boolean)
extern void WXAssetBundle_UnloadAllAssetBundles_mE892F374E9F6AD063684B42233096DAD9AB57966 (void);
// 0x00000054 System.Void WeChatWASM.WXAssetBundle::Unload(System.Boolean)
extern void WXAssetBundle_Unload_m731E0E297A7EC36E554CB3761AB2BB725B3100EE (void);
// 0x00000055 UnityEngine.AssetBundle WeChatWASM.WXAssetBundle::op_Explicit(WeChatWASM.WXAssetBundle)
extern void WXAssetBundle_op_Explicit_m01AFABD53FF72BA584146C1E521A60836D4E8A99 (void);
// 0x00000056 UnityEngine.Networking.UnityWebRequest WeChatWASM.WXAssetBundle::GetAssetBundle(System.String)
extern void WXAssetBundle_GetAssetBundle_m1D5BEBBCC8FC2D0CAF9E1D006D10FF92A7B9935D (void);
// 0x00000057 UnityEngine.Networking.UnityWebRequest WeChatWASM.WXAssetBundle::GetAssetBundle(System.String,System.UInt32)
extern void WXAssetBundle_GetAssetBundle_m5D4A6BF61B6CD7308AFEDE49CF2F8F9AC1F56E12 (void);
// 0x00000058 System.Void WeChatWASM.WXAssetBundle::.cctor()
extern void WXAssetBundle__cctor_m2DD6AD773A56BF1534985D2219FC2888DD8933BF (void);
// 0x00000059 System.Void WeChatWASM.DownloadHandlerWXAssetBundle::.ctor(System.String,System.UInt32)
extern void DownloadHandlerWXAssetBundle__ctor_m5E66ED397902696AE691E6461E80C7AFA4BA69F5 (void);
// 0x0000005A UnityEngine.AssetBundle WeChatWASM.DownloadHandlerWXAssetBundle::get_assetBundle()
extern void DownloadHandlerWXAssetBundle_get_assetBundle_mCCE7DB97A8B0446F1DA7A22A4A7E07AE3AFC02DE (void);
// 0x0000005B System.Byte[] WeChatWASM.DownloadHandlerWXAssetBundle::GetData()
extern void DownloadHandlerWXAssetBundle_GetData_m7747202EB8E11E678AA4EC1EDBDF973FDC13FA0F (void);
// 0x0000005C System.Boolean WeChatWASM.DownloadHandlerWXAssetBundle::ReceiveData(System.Byte[],System.Int32)
extern void DownloadHandlerWXAssetBundle_ReceiveData_mA50ACAFF74F39600B0E721A41D0BD35A5EDDE89D (void);
// 0x0000005D System.Void WeChatWASM.DownloadHandlerWXAssetBundle::CompleteContent()
extern void DownloadHandlerWXAssetBundle_CompleteContent_mC2F43B6054A619D5972DA550FA2E8BC89339B67F (void);
// 0x0000005E System.Void WeChatWASM.AssetBundleExtensions::WXUnload(UnityEngine.AssetBundle,System.Boolean)
extern void AssetBundleExtensions_WXUnload_m9B92DA1C58848A9C896F1DC4640E96DFCA28AC80 (void);
// 0x0000005F System.Void WeChatWASM.WXBannerAd::.ctor(System.String,WeChatWASM.Style)
extern void WXBannerAd__ctor_m26192C8F799505C1FE297698A6C5D8A6F5095F2C (void);
// 0x00000060 System.Void WeChatWASM.WXBannerAd::OnResizeCallback(WeChatWASM.WXADResizeResponse)
extern void WXBannerAd_OnResizeCallback_m0282E67C9CE203C2B3E5B513F03157567B9BC425 (void);
// 0x00000061 System.Void WeChatWASM.WXBannerAd::OnResize(System.Action`1<WeChatWASM.WXADResizeResponse>)
extern void WXBannerAd_OnResize_mC3E9C0926D723BD2A329E88CCC6B2D16CC1FF4AE (void);
// 0x00000062 System.Void WeChatWASM.WXBannerAd::OffResize(System.Action`1<WeChatWASM.WXADResizeResponse>)
extern void WXBannerAd_OffResize_mD7D8015CC17CE9D045C5DEB5BD21A745E973D5E2 (void);
// 0x00000063 System.Void WeChatWASM.WXBannerAd::Hide()
extern void WXBannerAd_Hide_m86DB266D122347EA56C55AB64E864F2D2302EC2F (void);
// 0x00000064 System.Void WeChatWASM.WXBaseAd::.ctor(System.String)
extern void WXBaseAd__ctor_mA7DA04EBEA505832C6B4B9014A81F3308B3F18DD (void);
// 0x00000065 System.Void WeChatWASM.WXBaseAd::Show(System.Action`1<WeChatWASM.WXTextResponse>,System.Action`1<WeChatWASM.WXTextResponse>)
extern void WXBaseAd_Show_m54A0DCD94CE9F5388D21DAA2E698F8BFD545AA10 (void);
// 0x00000066 System.Void WeChatWASM.WXBaseAd::Show(System.String,System.String,System.Action`1<WeChatWASM.WXTextResponse>,System.Action`1<WeChatWASM.WXTextResponse>)
extern void WXBaseAd_Show_m5475FE3B14B8CFA5F3E6A84EB4E2F77BFAFFEDEE (void);
// 0x00000067 System.Void WeChatWASM.WXBaseAd::OnError(System.Action`1<WeChatWASM.WXADErrorResponse>)
extern void WXBaseAd_OnError_m80C97C76469809C444DBCA3F0B5E85B8AC59AC0A (void);
// 0x00000068 System.Void WeChatWASM.WXBaseAd::OnLoad(System.Action`1<WeChatWASM.WXADLoadResponse>)
extern void WXBaseAd_OnLoad_mDA34BEFB24F0277D90FB941DBCCAB3D9B839A992 (void);
// 0x00000069 System.Void WeChatWASM.WXBaseAd::OffError(System.Action`1<WeChatWASM.WXADErrorResponse>)
extern void WXBaseAd_OffError_mEF73FA4DC1D66D3B8663D6BEDE6829C3DF3CD18A (void);
// 0x0000006A System.Void WeChatWASM.WXBaseAd::OffLoad(System.Action`1<WeChatWASM.WXADLoadResponse>)
extern void WXBaseAd_OffLoad_m005EB0306B4A5CC18D400EC3FBE57277CDD0843A (void);
// 0x0000006B System.Void WeChatWASM.WXBaseAd::Destroy()
extern void WXBaseAd_Destroy_mB760C762BA8047A0020031C7C1B7FADD2E8943D7 (void);
// 0x0000006C System.Void WeChatWASM.WXBaseAd::.cctor()
extern void WXBaseAd__cctor_m5FDAD0BC043F67D607904A55B804E7C9EADB38B8 (void);
// 0x0000006D System.Int32 A.B::A()
extern void B_A_m4E48AC3983FA372CE7D69C3EA6159255CA7A84B7 (void);
// 0x0000006E System.Boolean A.B::a()
extern void B_a_mD8F715546AF0EDDCABD940D8D83D0612AE92C1B6 (void);
// 0x0000006F System.String A.B::A(System.Action`1<A>)
// 0x00000070 System.Void A.B::A(System.String)
// 0x00000071 System.Void A.B::a(System.String)
extern void B_a_m78B1DF252D6681DB0C644F349D73AC2F8292AA79 (void);
// 0x00000072 System.Collections.IEnumerator A.B::B()
extern void B_B_mC080A903D8C65107D7F549C4BD44DB5AA4D81C9F (void);
// 0x00000073 System.Void A.B::.ctor()
extern void B__ctor_m90270E9130B5DE1DC0800DF973E914791E9A31F7 (void);
// 0x00000074 System.Void A.B::.cctor()
extern void B__cctor_m7D14A65DC7DCFBC95987B568E48DF94DBD09C8D2 (void);
// 0x00000075 System.Void A.B/A::.ctor(System.Int32)
extern void A__ctor_mEE08D8FA98577D81412E2B5C71A9634BC649B4B2 (void);
// 0x00000076 System.Void A.B/A::A()
extern void A_A_m1AF45509D6E215553BA3874E43C0E13FC51FEB9E (void);
// 0x00000077 System.Boolean A.B/A::MoveNext()
extern void A_MoveNext_m11AEDB6E5E5E34D6973686CC6969F75CE4C46128 (void);
// 0x00000078 System.Object A.B/A::a()
extern void A_a_m2EC799866EFA7D3BF8BCC61ADB49017C2911A31B (void);
// 0x00000079 System.Void A.B/A::B()
extern void A_B_m566C3B09D01C265876E46A3ED449EE65B6A979B7 (void);
// 0x0000007A System.Object A.B/A::b()
extern void A_b_m7FFBE02A63389544712CCAB91EA27F2E5C82CFB2 (void);
// 0x0000007B System.Void WeChatWASM.WXCamera::.ctor(System.String)
extern void WXCamera__ctor_mDCCB7F5132E8CE92208AB4C319473070002177C4 (void);
// 0x0000007C System.Void WeChatWASM.WXCamera::A(System.String)
extern void WXCamera_A_m1EC08EA924045A3C28B08EC3D7D2A2C5D19897D4 (void);
// 0x0000007D System.Void WeChatWASM.WXCamera::CloseFrameChange()
extern void WXCamera_CloseFrameChange_m50B0403D2679AEE500A0908665A38B1A619A8C4D (void);
// 0x0000007E System.Void WeChatWASM.WXCamera::a(System.String)
extern void WXCamera_a_m60467455B02E94B55E26CCC50E414DDEA5EED941 (void);
// 0x0000007F System.Void WeChatWASM.WXCamera::Destroy()
extern void WXCamera_Destroy_m927B8C44BE95F61BFE74654DD0F31B0EA682283E (void);
// 0x00000080 System.Void WeChatWASM.WXCamera::B(System.String)
extern void WXCamera_B_m3447119896005D4492824EC7613FE556FA10E1D5 (void);
// 0x00000081 System.Void WeChatWASM.WXCamera::ListenFrameChange()
extern void WXCamera_ListenFrameChange_m5557F7FD3EC66FC2FF38A6F77DF6AB14F3DC6113 (void);
// 0x00000082 System.Void WeChatWASM.WXCamera::b(System.String)
extern void WXCamera_b_m8B7EEB2A97D83DAFAA4F58F22D86A4C8756F9338 (void);
// 0x00000083 System.Void WeChatWASM.WXCamera::OnAuthCancel(System.Action)
extern void WXCamera_OnAuthCancel_m43D12CECE05BADB25FC11676B8B85B16CE036032 (void);
// 0x00000084 System.Void WeChatWASM.WXCamera::C(System.String)
extern void WXCamera_C_mAA02704063649E7F8F97530D23EB5E4C7CB05FCF (void);
// 0x00000085 System.Void WeChatWASM.WXCamera::OnCameraFrame(System.Action`1<WeChatWASM.OnCameraFrameCallbackResult>)
extern void WXCamera_OnCameraFrame_m1DC6072627D3AADFEAA5121E9804FDF93BC5BE71 (void);
// 0x00000086 System.Void WeChatWASM.WXCamera::c(System.String)
extern void WXCamera_c_m8C280BFFC0E4BD2AB095A00257F575DC070AAECA (void);
// 0x00000087 System.Void WeChatWASM.WXCamera::OnStop(System.Action)
extern void WXCamera_OnStop_m717B27357B02D6A12AD9B62E5EF785507780DDEC (void);
// 0x00000088 System.Void WeChatWASM.WXCamera::.cctor()
extern void WXCamera__cctor_m202CFEC690005B3D222A58EEEBCBDC088A2EF876 (void);
// 0x00000089 System.Void WeChatWASM.OnCameraFrameCallbackResult::.ctor()
extern void OnCameraFrameCallbackResult__ctor_m531F9A3635EB4F26B097B49F3A58B2FCE4DD1FD8 (void);
// 0x0000008A System.Void WeChatWASM.CreateCameraOption::.ctor()
extern void CreateCameraOption__ctor_mEA1A1C5053981B1025C0136381F8DDC36CE8F494 (void);
// 0x0000008B System.String WeChatWASM.WXCanvas::A(System.String)
extern void WXCanvas_A_mFB624D25BFDC45981BA7289C77C3EA42C1BCEF07 (void);
// 0x0000008C System.Void WeChatWASM.WXCanvas::A(System.String,System.String,System.String,System.String)
extern void WXCanvas_A_m9A04F26A4FBE4955F7318B15A3C160EE1F21602B (void);
// 0x0000008D System.String WeChatWASM.WXCanvas::ToTempFilePathSync(WeChatWASM.WXToTempFilePathSyncParam)
extern void WXCanvas_ToTempFilePathSync_mAD404D144E9CD5B4037FB5A3D8B3422A43C7A0E4 (void);
// 0x0000008E System.Void WeChatWASM.WXCanvas::ToTempFilePath(WeChatWASM.WXToTempFilePathParam)
extern void WXCanvas_ToTempFilePath_m1C4ADF2874CE867DE973C157F45709F1A14B6FE3 (void);
// 0x0000008F System.Void WeChatWASM.WXCanvas::.ctor()
extern void WXCanvas__ctor_mE4CA0446DE34F81E7053076107F1D80C2E4BF6A1 (void);
// 0x00000090 System.Void WeChatWASM.WXChat::.ctor(System.String)
extern void WXChat__ctor_mB593785D368580C90A767BFAF2FAD63D9C97458E (void);
// 0x00000091 System.Void WeChatWASM.WXChat::A(System.String)
extern void WXChat_A_m541D7A61A09CFDE50D7A4FCC4838B454570EA859 (void);
// 0x00000092 System.Void WeChatWASM.WXChat::On(System.String,System.Action`1<WeChatWASM.WXChatOnCallbackRes>)
extern void WXChat_On_mCDE8DA1B2A61E6A1EE32FD797462C5C8430E24D1 (void);
// 0x00000093 System.Void WeChatWASM.WXChat::a(System.String)
extern void WXChat_a_mFB785E2B8BC643BC1FC037E27D19F503F66622D6 (void);
// 0x00000094 System.Void WeChatWASM.WXChat::Off(System.String)
extern void WXChat_Off_mBF3B58BB717E89BAC7B8FB3D0568805DACFCC305 (void);
// 0x00000095 System.Void WeChatWASM.WXChat::A()
extern void WXChat_A_m21A96FCEBCE2531C18E93767F2445F1B5F5C4B6D (void);
// 0x00000096 System.Void WeChatWASM.WXChat::Hide()
extern void WXChat_Hide_mD8DE7632BB06796FBE5BC7C90B6E617344FFBFB8 (void);
// 0x00000097 System.Void WeChatWASM.WXChat::B(System.String)
extern void WXChat_B_m9E4A8412A423EEE342D0502145CCE42D2B64B400 (void);
// 0x00000098 System.Void WeChatWASM.WXChat::Show(WeChatWASM.WXChatOptions)
extern void WXChat_Show_mE103FCC09D799C3D12E84805430FD56421B6FB20 (void);
// 0x00000099 System.Void WeChatWASM.WXChat::a()
extern void WXChat_a_mF3F78B2089665FE9D725B1BA189F83145F13BA7E (void);
// 0x0000009A System.Void WeChatWASM.WXChat::Close()
extern void WXChat_Close_m05C3B3FF04414FED3F076A13409D17ED56C07BF7 (void);
// 0x0000009B System.Void WeChatWASM.WXChat::b(System.String)
extern void WXChat_b_m445ACF77948DF4E8589D6BB39F2226734F2A9FA8 (void);
// 0x0000009C System.Void WeChatWASM.WXChat::Open(System.String)
extern void WXChat_Open_mCB2FC0EBA00D29130F27C4681D8873883315D290 (void);
// 0x0000009D System.Void WeChatWASM.WXChat::C(System.String)
extern void WXChat_C_m4708D44630DB29E7E22B95D887FF1A708439FD3A (void);
// 0x0000009E System.Void WeChatWASM.WXChat::SetTabs(System.String[])
extern void WXChat_SetTabs_mA9F4E6F676352F2D44499DDDE4BE7D8225BF1B1D (void);
// 0x0000009F System.Void WeChatWASM.WXChat::c(System.String)
extern void WXChat_c_m67649AF18889B9651B1CE2C2B29862AC8779DE0F (void);
// 0x000000A0 System.Void WeChatWASM.WXChat::SetChatSignature(System.String)
extern void WXChat_SetChatSignature_m9ED11FE1D262C607C895C2196E305D81DE47E5DF (void);
// 0x000000A1 System.Void WeChatWASM.WXChat::.cctor()
extern void WXChat__cctor_m68B8DF5DA672671950C5C66B5B104BA4E43AF1ED (void);
// 0x000000A2 System.Void WeChatWASM.WXChatOptions::.ctor()
extern void WXChatOptions__ctor_mFD514551ADDCCFA66A20FB63D299E663440AEB19 (void);
// 0x000000A3 System.Void WeChatWASM.WXChatOnCallback::.ctor()
extern void WXChatOnCallback__ctor_m58E0233E08E9F11C6259044B8B578CC85A52CE56 (void);
// 0x000000A4 System.Void WeChatWASM.WXChatOnCallbackRes::.ctor()
extern void WXChatOnCallbackRes__ctor_m2DA037F42ECB27DDF2D0C0757808DB4FB9771FAA (void);
// 0x000000A5 System.Void WeChatWASM.WXCustomAd::.ctor(System.String,WeChatWASM.CustomStyle)
extern void WXCustomAd__ctor_mEF651094144B2069883ABD834EB5081A5277641C (void);
// 0x000000A6 System.Void WeChatWASM.WXCustomAd::OnCloseCallback()
extern void WXCustomAd_OnCloseCallback_mE08F86A9C80ECB053F110E16C4D155A73DD16A76 (void);
// 0x000000A7 System.Void WeChatWASM.WXCustomAd::OnHideCallback()
extern void WXCustomAd_OnHideCallback_mD70D24D0FB1D359401DE19DD89C0A54F4AC24CB3 (void);
// 0x000000A8 System.Void WeChatWASM.WXCustomAd::OnClose(System.Action)
extern void WXCustomAd_OnClose_mE1F055A8C3D066FBB81A1E9E07BBBFB9B7464B65 (void);
// 0x000000A9 System.Void WeChatWASM.WXCustomAd::OnHide(System.Action)
extern void WXCustomAd_OnHide_mA54BF4C5AE29D84CC31BF25F0C3F96C7E7077685 (void);
// 0x000000AA System.Void WeChatWASM.WXCustomAd::OffClose(System.Action)
extern void WXCustomAd_OffClose_m80EBCCBA47614FBBFC9EA02C3EF0A341C37B7726 (void);
// 0x000000AB System.Void WeChatWASM.WXCustomAd::OffHide(System.Action)
extern void WXCustomAd_OffHide_m8DDC52C3EC926435B91567A7AB4499E5D841283E (void);
// 0x000000AC System.Void WeChatWASM.WXCustomAd::Hide(System.Action`1<WeChatWASM.WXTextResponse>,System.Action`1<WeChatWASM.WXTextResponse>)
extern void WXCustomAd_Hide_m33FB4A77D72D5697651B58C399E57DB82AE72947 (void);
// 0x000000AD System.Void WeChatWASM.WXDownloadTask::.ctor(System.String,WeChatWASM.DownloadFileOption)
extern void WXDownloadTask__ctor_mC9E93342B400BD6F243A44E32ECF3A9A9132A07D (void);
// 0x000000AE System.Void WeChatWASM.WXDownloadTask::A(System.String)
extern void WXDownloadTask_A_mC0240A67512BFE623B3DABA2B3D8FE72495DC92F (void);
// 0x000000AF System.Void WeChatWASM.WXDownloadTask::Abort()
extern void WXDownloadTask_Abort_mFCC67001644E11A792B698A9142CF00056DA6F7C (void);
// 0x000000B0 System.Void WeChatWASM.WXDownloadTask::a(System.String)
extern void WXDownloadTask_a_m6395D092D973C731EAE7DF4C7BA22DD0A9F6AC9A (void);
// 0x000000B1 System.Void WeChatWASM.WXDownloadTask::OffHeadersReceived()
extern void WXDownloadTask_OffHeadersReceived_mC99131BFD4D005AFB4247D40649A55A383845F0E (void);
// 0x000000B2 System.Void WeChatWASM.WXDownloadTask::B(System.String)
extern void WXDownloadTask_B_m8777CF4698E44F919608E83BAD27FE2FA6FABD38 (void);
// 0x000000B3 System.Void WeChatWASM.WXDownloadTask::OffProgressUpdate()
extern void WXDownloadTask_OffProgressUpdate_mDE6A3C6F0BC73B96C50731DF7787CB42ECA3AA98 (void);
// 0x000000B4 System.Void WeChatWASM.WXDownloadTask::b(System.String)
extern void WXDownloadTask_b_m38F6B847779C75F383FE693E35BCE8AA00261AE0 (void);
// 0x000000B5 System.Void WeChatWASM.WXDownloadTask::OnHeadersReceived(System.Action`1<WeChatWASM.OnHeadersReceivedListenerResult>)
extern void WXDownloadTask_OnHeadersReceived_m21103E990EFD1D6ACA4A4449163BF1A142825640 (void);
// 0x000000B6 System.Void WeChatWASM.WXDownloadTask::C(System.String)
extern void WXDownloadTask_C_m61B6B8B972F73F24902D6AB1632D15266B9A4931 (void);
// 0x000000B7 System.Void WeChatWASM.WXDownloadTask::OnProgressUpdate(System.Action`1<WeChatWASM.DownloadTaskOnProgressUpdateListenerResult>)
extern void WXDownloadTask_OnProgressUpdate_mA73674328205CED0D3C0BF3800F3EC37E495EF93 (void);
// 0x000000B8 System.String WeChatWASM.WXEnv::A()
extern void WXEnv_A_m6C619AF6E1179D2C0870A0BBEE948CFE0B07CE98 (void);
// 0x000000B9 System.String WeChatWASM.WXEnv::get_USER_DATA_PATH()
extern void WXEnv_get_USER_DATA_PATH_m18B5E1D35E0F96A2EC3556DAB5F522038FD08443 (void);
// 0x000000BA System.Void WeChatWASM.WXEnv::.ctor()
extern void WXEnv__ctor_m3B0DF58986C02376ED27D7E0C49D649582EF0BB8 (void);
// 0x000000BB System.Void WeChatWASM.WXFeedbackButton::A(System.String,System.String,System.String)
extern void WXFeedbackButton_A_m470E791DEBD2EBFF89933D72B101661B8BFF270F (void);
// 0x000000BC System.Void WeChatWASM.WXFeedbackButton::.ctor(System.String,WeChatWASM.CreateOpenSettingButtonOption)
extern void WXFeedbackButton__ctor_m6F76DDC96017B2F976FC99EEA1849B657D10E092 (void);
// 0x000000BD System.String WeChatWASM.WXFeedbackButton::get_type()
extern void WXFeedbackButton_get_type_m2C8EE6E785C1D63687FE7E07AE37C58EFF25DB52 (void);
// 0x000000BE System.Void WeChatWASM.WXFeedbackButton::set_type(System.String)
extern void WXFeedbackButton_set_type_m688D37933F0FE53598A8343CC46FD2DF1F7CC9E4 (void);
// 0x000000BF System.String WeChatWASM.WXFeedbackButton::get_image()
extern void WXFeedbackButton_get_image_mE8EC6D907A35221CAD032EC532BEE302A89BE083 (void);
// 0x000000C0 System.Void WeChatWASM.WXFeedbackButton::set_image(System.String)
extern void WXFeedbackButton_set_image_m5D809B752618507195E2CB3A1CB48BB8198B8F96 (void);
// 0x000000C1 System.String WeChatWASM.WXFeedbackButton::get_text()
extern void WXFeedbackButton_get_text_m9B6726EFF17A0B98E3CC0F7F3BEB56C6ACB7D910 (void);
// 0x000000C2 System.Void WeChatWASM.WXFeedbackButton::set_text(System.String)
extern void WXFeedbackButton_set_text_m2894CE5D5374140EF9F7F4146EAF0F125C206349 (void);
// 0x000000C3 System.Void WeChatWASM.WXFeedbackButton::A(System.String)
extern void WXFeedbackButton_A_mD47A44EE8986DB7513B3D92ED88C2D89A560AF7C (void);
// 0x000000C4 System.Void WeChatWASM.WXFeedbackButton::Destroy()
extern void WXFeedbackButton_Destroy_m85F9447436E5B7D3756DD8EDC1E2D6671755F344 (void);
// 0x000000C5 System.Void WeChatWASM.WXFeedbackButton::a(System.String)
extern void WXFeedbackButton_a_mB997CE6759025C001CCFD0BC6BA6AAEE800E3791 (void);
// 0x000000C6 System.Void WeChatWASM.WXFeedbackButton::Hide()
extern void WXFeedbackButton_Hide_m7B65A458F08962EBFF1AF4B64A0ECC02632BED4E (void);
// 0x000000C7 System.Void WeChatWASM.WXFeedbackButton::B(System.String)
extern void WXFeedbackButton_B_m7F1A5585FDA65975CADA990FCE65308790A302D8 (void);
// 0x000000C8 System.Void WeChatWASM.WXFeedbackButton::OffTap()
extern void WXFeedbackButton_OffTap_mED501D90E97F29756DEF1E91BC223F701E9E63E5 (void);
// 0x000000C9 System.Void WeChatWASM.WXFeedbackButton::b(System.String)
extern void WXFeedbackButton_b_mA7C62EE79AB602762A759A52F7BC3A7D053B9AD4 (void);
// 0x000000CA System.Void WeChatWASM.WXFeedbackButton::OnTap(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WXFeedbackButton_OnTap_mCFA129959DAF12BC5FE311D605227808ED4B6ADA (void);
// 0x000000CB System.Void WeChatWASM.WXFeedbackButton::C(System.String)
extern void WXFeedbackButton_C_mF39DB0D6E6A0A6586F8F7A32FB85DAEBC88F211F (void);
// 0x000000CC System.Void WeChatWASM.WXFeedbackButton::Show()
extern void WXFeedbackButton_Show_m07E29180A22A14F981F744B491F2E92F0D53E713 (void);
// 0x000000CD System.Void WeChatWASM.CleanFileCacheParams::.ctor()
extern void CleanFileCacheParams__ctor_m51F3466967FECDC0CFE4AC4DD93F37A6D29ECB40 (void);
// 0x000000CE System.Void WeChatWASM.FileCacheCommonParams::.ctor()
extern void FileCacheCommonParams__ctor_mC0D549BA966F1EFF3D1AB3DADD43CA25886AB866 (void);
// 0x000000CF System.String WeChatWASM.WXFileCacheCleanTask::A(System.Int32)
extern void WXFileCacheCleanTask_A_mCF8E704667CBEF2B4879E5B8E85E7B9901FD5D16 (void);
// 0x000000D0 System.String WeChatWASM.WXFileCacheCleanTask::A()
extern void WXFileCacheCleanTask_A_mEB188F1CFEE3AA616277DACA1CBC842402FC7794 (void);
// 0x000000D1 System.String WeChatWASM.WXFileCacheCleanTask::A(System.String)
extern void WXFileCacheCleanTask_A_m2B8BD0F84313DD1E26DB16360BDE9BED4A580C02 (void);
// 0x000000D2 System.Void WeChatWASM.WXFileCacheCleanTask::.ctor(System.Int32,System.Action`1<WeChatWASM.ReleaseResult>)
extern void WXFileCacheCleanTask__ctor_mB5E5C26A45DE7879B61B0A87E7749B405EBFB8DF (void);
// 0x000000D3 System.Void WeChatWASM.WXFileCacheCleanTask::.ctor(System.Boolean,System.Action`1<System.Boolean>)
extern void WXFileCacheCleanTask__ctor_m095F207622C856CAFD5C220AFCB8604198A81EFB (void);
// 0x000000D4 System.Void WeChatWASM.WXFileCacheCleanTask::.ctor(System.String,System.Action`1<System.Boolean>)
extern void WXFileCacheCleanTask__ctor_mE5EF300B9BAA6B90C26B70745BCE7348025D5417 (void);
// 0x000000D5 System.Void WeChatWASM.WXFileCacheCleanTask::.cctor()
extern void WXFileCacheCleanTask__cctor_m291ED3D8F0FF3D2B2B32869DF6D4F159548AB866 (void);
// 0x000000D6 System.String WeChatWASM.WXFileSystemManager::A(System.String,System.String,System.String)
extern void WXFileSystemManager_A_m9591CF0640BB23FA5B7CD34A3909239884DFF38C (void);
// 0x000000D7 System.String WeChatWASM.WXFileSystemManager::A(System.String,System.Byte[],System.Int32,System.String)
extern void WXFileSystemManager_A_mD4C9355A46B27497E015C1F409DA8A10891031DB (void);
// 0x000000D8 System.String WeChatWASM.WXFileSystemManager::A(System.String,System.Byte[],System.Int32,System.String,System.String,System.String,System.String)
extern void WXFileSystemManager_A_m999770071533D1833D444FFD49DA141EACF82E04 (void);
// 0x000000D9 System.String WeChatWASM.WXFileSystemManager::A(System.String,System.String,System.String,System.String,System.String,System.String)
extern void WXFileSystemManager_A_m02F8E2FC9A1614283C8CD2ECB4F6525B9046BB6C (void);
// 0x000000DA System.String WeChatWASM.WXFileSystemManager::a(System.String,System.Byte[],System.Int32,System.String,System.String,System.String,System.String)
extern void WXFileSystemManager_a_m4C6AFFB9E3820DF7517C3902662B8E786ADE29D6 (void);
// 0x000000DB System.String WeChatWASM.WXFileSystemManager::a(System.String,System.String,System.String,System.String,System.String,System.String)
extern void WXFileSystemManager_a_m2492927C33C42D8CE4C239DC2CADCBD7ACDFD029 (void);
// 0x000000DC System.String WeChatWASM.WXFileSystemManager::A(System.String,System.String)
extern void WXFileSystemManager_A_m669638DDB81E9216115E2B11B473A08D27218EB0 (void);
// 0x000000DD System.String WeChatWASM.WXFileSystemManager::A(System.String)
extern void WXFileSystemManager_A_m238899490D3A4191351DA49AB88A48B15F51D6D1 (void);
// 0x000000DE System.String WeChatWASM.WXFileSystemManager::a(System.String)
extern void WXFileSystemManager_a_mB1E5F7FEAA465CC34FF8A80F584AD06C6978E993 (void);
// 0x000000DF System.Void WeChatWASM.WXFileSystemManager::A(System.String,System.String,System.String,System.String)
extern void WXFileSystemManager_A_mF5AB7CABF9465110093D000295804D5EBA68A7B2 (void);
// 0x000000E0 System.String WeChatWASM.WXFileSystemManager::a(System.String,System.String)
extern void WXFileSystemManager_a_m24601D51E6D57106CA565DCB7F5009291DC11565 (void);
// 0x000000E1 System.Void WeChatWASM.WXFileSystemManager::A(System.String,System.String,System.String,System.String,System.String)
extern void WXFileSystemManager_A_m52AF92D52BACBA92DA08005E3EEFE6E9F17AB620 (void);
// 0x000000E2 System.String WeChatWASM.WXFileSystemManager::B(System.String)
extern void WXFileSystemManager_B_m795D6B31E9447445F125751E17CD9F79964CBB1D (void);
// 0x000000E3 System.Void WeChatWASM.WXFileSystemManager::a(System.String,System.String,System.String,System.String)
extern void WXFileSystemManager_a_m46238F4249F24C604AAE04D01006EF269592661F (void);
// 0x000000E4 System.Void WeChatWASM.WXFileSystemManager::A(System.String,System.Boolean,System.String,System.String,System.String)
extern void WXFileSystemManager_A_mD895BD5A0F8A233B04F64A24234E863D01551536 (void);
// 0x000000E5 System.String WeChatWASM.WXFileSystemManager::A(System.String,System.Boolean)
extern void WXFileSystemManager_A_m576A5C367EC479E942748C0265F3688EEB8E3398 (void);
// 0x000000E6 System.Void WeChatWASM.WXFileSystemManager::a(System.String,System.Boolean,System.String,System.String,System.String)
extern void WXFileSystemManager_a_m91A176FC803EE96A090FFA50975D54609637EDA3 (void);
// 0x000000E7 System.String WeChatWASM.WXFileSystemManager::a(System.String,System.Boolean)
extern void WXFileSystemManager_a_m5A44CD08AE030B9B7F2AB6C22B8CC1C5DC2F80E0 (void);
// 0x000000E8 System.String WeChatWASM.WXFileSystemManager::WriteFileSync(System.String,System.String,System.String)
extern void WXFileSystemManager_WriteFileSync_m1ED2CE83AA1A85249B185CD274A83D821A33A036 (void);
// 0x000000E9 System.String WeChatWASM.WXFileSystemManager::WriteFileSync(System.String,System.Byte[],System.String)
extern void WXFileSystemManager_WriteFileSync_mB5C1BE9E45E9400BE306A2104A289DD6CEFA93EF (void);
// 0x000000EA System.Void WeChatWASM.WXFileSystemManager::WriteFile(WeChatWASM.WriteFileParam)
extern void WXFileSystemManager_WriteFile_m0199A22E0EF1F48F95DC7921F9C24791A630CCE3 (void);
// 0x000000EB System.Void WeChatWASM.WXFileSystemManager::WriteFile(WeChatWASM.WriteFileStringParam)
extern void WXFileSystemManager_WriteFile_m6E42D71AB0FB03328B33E7F68380CB3FBFEF85B4 (void);
// 0x000000EC System.Void WeChatWASM.WXFileSystemManager::AppendFile(WeChatWASM.WriteFileParam)
extern void WXFileSystemManager_AppendFile_mE1E48F55CB23FA84F8EA61B222FF0750F518AF25 (void);
// 0x000000ED System.Void WeChatWASM.WXFileSystemManager::AppendFile(WeChatWASM.WriteFileStringParam)
extern void WXFileSystemManager_AppendFile_mF93528F4D4BE9AD4A8304B61B46ED1EE4C07023E (void);
// 0x000000EE System.Void WeChatWASM.WXFileSystemManager::ReadFile(WeChatWASM.ReadFileParam)
extern void WXFileSystemManager_ReadFile_m23F1697D4060A8156D7150FC46FBE7BC832B9CFF (void);
// 0x000000EF System.Byte[] WeChatWASM.WXFileSystemManager::ReadFileSync(System.String,System.Nullable`1<System.Int64>,System.Nullable`1<System.Int64>)
extern void WXFileSystemManager_ReadFileSync_mB3DDEB867A560DFCE019564B38CA186AFB58F79A (void);
// 0x000000F0 System.String WeChatWASM.WXFileSystemManager::ReadFileSync(System.String,System.String,System.Nullable`1<System.Int64>,System.Nullable`1<System.Int64>)
extern void WXFileSystemManager_ReadFileSync_mD4FCD980CF6893FF76882907FD9303376F33FBB8 (void);
// 0x000000F1 System.String WeChatWASM.WXFileSystemManager::AccessSync(System.String)
extern void WXFileSystemManager_AccessSync_mCB50A2CE49CADBF428A6202906B364B08A1AC5A3 (void);
// 0x000000F2 System.Void WeChatWASM.WXFileSystemManager::Access(WeChatWASM.AccessParam)
extern void WXFileSystemManager_Access_mB800F2F9AD2CEDAD4CD121649DBD4A8B18EB19BE (void);
// 0x000000F3 System.String WeChatWASM.WXFileSystemManager::CopyFileSync(System.String,System.String)
extern void WXFileSystemManager_CopyFileSync_m160587D2FCDCDDE3B7AE3D65652EEDF61E3C578C (void);
// 0x000000F4 System.Void WeChatWASM.WXFileSystemManager::CopyFile(WeChatWASM.CopyFileParam)
extern void WXFileSystemManager_CopyFile_mED468262DE57DC709778958E733720290F899232 (void);
// 0x000000F5 System.String WeChatWASM.WXFileSystemManager::UnlinkSync(System.String)
extern void WXFileSystemManager_UnlinkSync_m59BAD70900F4DBFA96424E739C505478A58BB710 (void);
// 0x000000F6 System.Void WeChatWASM.WXFileSystemManager::Unlink(WeChatWASM.UnlinkParam)
extern void WXFileSystemManager_Unlink_m39F93FA799985CD09FF65860887ED95887AF0FDA (void);
// 0x000000F7 System.Void WeChatWASM.WXFileSystemManager::Mkdir(WeChatWASM.MkdirParam)
extern void WXFileSystemManager_Mkdir_mE306EC05F3BFF3E3DC7784B20947505C525E7F8E (void);
// 0x000000F8 System.String WeChatWASM.WXFileSystemManager::MkdirSync(System.String,System.Boolean)
extern void WXFileSystemManager_MkdirSync_mB96CE1EC8EC68AC3AE477AF6626E4CA97133D5FE (void);
// 0x000000F9 System.Void WeChatWASM.WXFileSystemManager::Rmdir(WeChatWASM.RmdirParam)
extern void WXFileSystemManager_Rmdir_m8C4F013E104E681A9A19B6BFD84BAAF16C34B816 (void);
// 0x000000FA System.String WeChatWASM.WXFileSystemManager::RmdirSync(System.String,System.Boolean)
extern void WXFileSystemManager_RmdirSync_m29FED2EF31B0C7DCF6507924D6D4D7FF75592DE4 (void);
// 0x000000FB System.Void WeChatWASM.WXFileSystemManager::B(System.String,System.String)
extern void WXFileSystemManager_B_m58C37951CCCD48EB4005CB8A1CD35833F52B553B (void);
// 0x000000FC System.Void WeChatWASM.WXFileSystemManager::Stat(WeChatWASM.WXStatOption)
extern void WXFileSystemManager_Stat_m356909F9D17DF6C5F41B2BF67C34BF74E40CDB9F (void);
// 0x000000FD System.Void WeChatWASM.WXFileSystemManager::HandleStatCallback(System.String)
extern void WXFileSystemManager_HandleStatCallback_m1A40863EF3B7E61480496868FC8771A7D10A92A7 (void);
// 0x000000FE System.Void WeChatWASM.WXFileSystemManager::HandleMethodCallback(System.String)
extern void WXFileSystemManager_HandleMethodCallback_m2D4A220BD67AA94EA647B3FF9779D447E0F71481 (void);
// 0x000000FF System.Void WeChatWASM.WXFileSystemManager::BaseMethodCallback(System.String,System.String,System.String)
// 0x00000100 System.String WeChatWASM.WXFileSystemManager::A(A,System.String)
// 0x00000101 System.String WeChatWASM.WXFileSystemManager::a(A,System.String)
// 0x00000102 System.String WeChatWASM.WXFileSystemManager::A(A,System.String)
// 0x00000103 System.Void WeChatWASM.WXFileSystemManager::a(System.String,System.String,System.String)
extern void WXFileSystemManager_a_mE16A47B8A91018AD48798ADE1710B23D1BB2F70E (void);
// 0x00000104 System.Void WeChatWASM.WXFileSystemManager::a(System.String,System.Byte[],System.Int32,System.String)
extern void WXFileSystemManager_a_mE80347C5299EDD1B802079681909F91B1E6E57E3 (void);
// 0x00000105 System.Void WeChatWASM.WXFileSystemManager::AppendFileSync(System.String,System.String,System.String)
extern void WXFileSystemManager_AppendFileSync_mF9BE7B4C1F1670772D7F7B709FE4F440047CF3DA (void);
// 0x00000106 System.Void WeChatWASM.WXFileSystemManager::AppendFileSync(System.String,System.Byte[],System.String)
extern void WXFileSystemManager_AppendFileSync_m8C0AEABFB5487723081ED342662FB8766440CD1C (void);
// 0x00000107 System.String WeChatWASM.WXFileSystemManager::b(System.String)
extern void WXFileSystemManager_b_mA3D8A33F264808E719AFEE7B24779D12FB45435D (void);
// 0x00000108 System.String[] WeChatWASM.WXFileSystemManager::ReaddirSync(System.String)
extern void WXFileSystemManager_ReaddirSync_mDB746288392660B6556E95411965B124B65A4493 (void);
// 0x00000109 System.Int32 WeChatWASM.WXFileSystemManager::b(System.String,System.String)
extern void WXFileSystemManager_b_m407C8A7450A817C359B476852DA5BC9BA78324D2 (void);
// 0x0000010A System.Byte[] WeChatWASM.WXFileSystemManager::ReadCompressedFileSync(WeChatWASM.ReadCompressedFileSyncOption)
extern void WXFileSystemManager_ReadCompressedFileSync_m8E96317F6C923AA5F8208686DDBD1A27F66DEAFD (void);
// 0x0000010B System.Void WeChatWASM.WXFileSystemManager::C(System.String,System.String)
extern void WXFileSystemManager_C_mAE790E31A6FDC2815EBCB307BD3AE9741DCF9DD0 (void);
// 0x0000010C System.Void WeChatWASM.WXFileSystemManager::Close(WeChatWASM.FileSystemManagerCloseOption)
extern void WXFileSystemManager_Close_m4515A7DE1525A0B83B99BBE36844FC5072F3610B (void);
// 0x0000010D System.Void WeChatWASM.WXFileSystemManager::c(System.String,System.String)
extern void WXFileSystemManager_c_mAABBB8B6B18085BCD7C6B126911400EAEBD4F2B2 (void);
// 0x0000010E System.Void WeChatWASM.WXFileSystemManager::Fstat(WeChatWASM.FstatOption)
extern void WXFileSystemManager_Fstat_mB3823854328D85C55925DB4674430ECD93DC72E1 (void);
// 0x0000010F System.Void WeChatWASM.WXFileSystemManager::D(System.String,System.String)
extern void WXFileSystemManager_D_m3798A6F865295520A168299F1076903E1D1BC6A0 (void);
// 0x00000110 System.Void WeChatWASM.WXFileSystemManager::Ftruncate(WeChatWASM.FtruncateOption)
extern void WXFileSystemManager_Ftruncate_m4742419E5902E2E198CF72E17365BCC491018583 (void);
// 0x00000111 System.Void WeChatWASM.WXFileSystemManager::d(System.String,System.String)
extern void WXFileSystemManager_d_mB3BB64747CDA2E7879C2048C20DD9DEB68F8D1DC (void);
// 0x00000112 System.Void WeChatWASM.WXFileSystemManager::GetFileInfo(WeChatWASM.GetFileInfoOption)
extern void WXFileSystemManager_GetFileInfo_m24DDD955DD40999C58F2985F9A1A0C9C08B4F640 (void);
// 0x00000113 System.Void WeChatWASM.WXFileSystemManager::E(System.String,System.String)
extern void WXFileSystemManager_E_mD3D67F8236D060F8E839AF85E9AE699149C61837 (void);
// 0x00000114 System.Void WeChatWASM.WXFileSystemManager::GetSavedFileList(WeChatWASM.GetSavedFileListOption)
extern void WXFileSystemManager_GetSavedFileList_m3508124AC4BE31D205016F5FA37225A5129AD67F (void);
// 0x00000115 System.Void WeChatWASM.WXFileSystemManager::e(System.String,System.String)
extern void WXFileSystemManager_e_mB9BCC070B5C3E4C48871E4750FA0EA6F839B6EAF (void);
// 0x00000116 System.Void WeChatWASM.WXFileSystemManager::Open(WeChatWASM.OpenOption)
extern void WXFileSystemManager_Open_m39341599AABCD154625D7D2C7EF706AA2487EFE3 (void);
// 0x00000117 System.String WeChatWASM.WXFileSystemManager::B(System.String,System.Byte[],System.Int32,System.String)
extern void WXFileSystemManager_B_m3C748E2196D4BC3B3BC1A8C2E0D9F92081527E73 (void);
// 0x00000118 System.Void WeChatWASM.WXFileSystemManager::Read(WeChatWASM.ReadOption)
extern void WXFileSystemManager_Read_mA7AADAA6C97D86F712CA5F65E1E78CD9E1C57076 (void);
// 0x00000119 System.Void WeChatWASM.WXFileSystemManager::F(System.String,System.String)
extern void WXFileSystemManager_F_m122F335BC1BAD184F6006AFC668C85AB77D52668 (void);
// 0x0000011A System.Void WeChatWASM.WXFileSystemManager::f(System.String,System.String)
extern void WXFileSystemManager_f_m1D56E54B4E8B67B8824B8C62E6F71DA915BBB041 (void);
// 0x0000011B System.Void WeChatWASM.WXFileSystemManager::ReadZipEntry(WeChatWASM.ReadZipEntryOption)
extern void WXFileSystemManager_ReadZipEntry_m3547EEE019FA6576902E9A6B38EE1241B6DE54F1 (void);
// 0x0000011C System.Void WeChatWASM.WXFileSystemManager::ReadZipEntry(WeChatWASM.ReadZipEntryOptionString)
extern void WXFileSystemManager_ReadZipEntry_mE5CBBC94E5E830911C7F1594492065EA45B4B547 (void);
// 0x0000011D System.Void WeChatWASM.WXFileSystemManager::G(System.String,System.String)
extern void WXFileSystemManager_G_mE7FE0F9DF21D4F8EC3BC03C3C00E751B5C768BDF (void);
// 0x0000011E System.Void WeChatWASM.WXFileSystemManager::Readdir(WeChatWASM.ReaddirOption)
extern void WXFileSystemManager_Readdir_mA1DA3316E612C04B76CC5746C1C99FEE7EF394A6 (void);
// 0x0000011F System.Void WeChatWASM.WXFileSystemManager::g(System.String,System.String)
extern void WXFileSystemManager_g_mD92353E047EFDAD0EB4D7F4B56ECAB082AB49400 (void);
// 0x00000120 System.Void WeChatWASM.WXFileSystemManager::RemoveSavedFile(WeChatWASM.RemoveSavedFileOption)
extern void WXFileSystemManager_RemoveSavedFile_m59CA1073BA1E5E3A09027098F3823E294BFED4DA (void);
// 0x00000121 System.Void WeChatWASM.WXFileSystemManager::H(System.String,System.String)
extern void WXFileSystemManager_H_mE3A526D46A7849D798F953D2309EFBBDA4746A6A (void);
// 0x00000122 System.Void WeChatWASM.WXFileSystemManager::Rename(WeChatWASM.RenameOption)
extern void WXFileSystemManager_Rename_m6F6BF8E3A85C87D917C7C7F148936C49AF99A017 (void);
// 0x00000123 System.Void WeChatWASM.WXFileSystemManager::h(System.String,System.String)
extern void WXFileSystemManager_h_m0C7C93779C150F5BEC80F70CDA32A7F378B9B8AA (void);
// 0x00000124 System.Void WeChatWASM.WXFileSystemManager::RenameSync(System.String,System.String)
extern void WXFileSystemManager_RenameSync_mF6915E3047B1E0FFE93793E0D559B998A2C23BFB (void);
// 0x00000125 System.Void WeChatWASM.WXFileSystemManager::I(System.String,System.String)
extern void WXFileSystemManager_I_mD540CD62417582A572ACD9F81989C965C4C114E2 (void);
// 0x00000126 System.Void WeChatWASM.WXFileSystemManager::SaveFile(WeChatWASM.SaveFileOption)
extern void WXFileSystemManager_SaveFile_m1F65C1779F75684793D258EC2080534D923A2E03 (void);
// 0x00000127 System.Void WeChatWASM.WXFileSystemManager::i(System.String,System.String)
extern void WXFileSystemManager_i_m6B894F73354118C79E6BF77357353040F4EEE1C4 (void);
// 0x00000128 System.Void WeChatWASM.WXFileSystemManager::Truncate(WeChatWASM.TruncateOption)
extern void WXFileSystemManager_Truncate_m0094F4E62367C91F9375F901D8ABBE4C3CB45B49 (void);
// 0x00000129 System.Void WeChatWASM.WXFileSystemManager::J(System.String,System.String)
extern void WXFileSystemManager_J_m22BE7DFA63B1619CF2CF09C64C73F2F45A8904A2 (void);
// 0x0000012A System.Void WeChatWASM.WXFileSystemManager::Unzip(WeChatWASM.UnzipOption)
extern void WXFileSystemManager_Unzip_mD92281762629E86C75D8CC3515723FE5E4301B40 (void);
// 0x0000012B System.String WeChatWASM.WXFileSystemManager::b(System.String,System.Byte[],System.Int32,System.String)
extern void WXFileSystemManager_b_mD6F441A2C4FFE3A6F3402139F512EBF948410DE4 (void);
// 0x0000012C System.String WeChatWASM.WXFileSystemManager::j(System.String,System.String)
extern void WXFileSystemManager_j_m43511A5EA6D2A95E7FED1A79D3A80702BCC0AC2B (void);
// 0x0000012D System.Void WeChatWASM.WXFileSystemManager::Write(WeChatWASM.WriteOption)
extern void WXFileSystemManager_Write_mEA4CC1C1FE1593571716D07F0C302DAAC7EBC67C (void);
// 0x0000012E System.Void WeChatWASM.WXFileSystemManager::Write(WeChatWASM.WriteStringOption)
extern void WXFileSystemManager_Write_m780D682F9AC588F9217D7298402E23B0BCAB4BE4 (void);
// 0x0000012F System.String WeChatWASM.WXFileSystemManager::K(System.String,System.String)
extern void WXFileSystemManager_K_mB5688D3679B109B05E8C89DE1F17192E9DE2C0EF (void);
// 0x00000130 WeChatWASM.ReadResult WeChatWASM.WXFileSystemManager::ReadSync(WeChatWASM.ReadSyncOption)
extern void WXFileSystemManager_ReadSync_mD0F927949871100CBDC6C43B53DCFA9E5D438CCE (void);
// 0x00000131 System.String WeChatWASM.WXFileSystemManager::C(System.String)
extern void WXFileSystemManager_C_mE3909D077B1FF3F31BC6B14B62F556DBB660BF48 (void);
// 0x00000132 WeChatWASM.Stats WeChatWASM.WXFileSystemManager::FstatSync(WeChatWASM.FstatSyncOption)
extern void WXFileSystemManager_FstatSync_m159078767B657D9677285A6D36AADDD808DC7B86 (void);
// 0x00000133 System.String WeChatWASM.WXFileSystemManager::B(System.String,System.Boolean)
extern void WXFileSystemManager_B_mEED45C2CDE33F11FF8D41B4D10B57B2FC166C6C5 (void);
// 0x00000134 WeChatWASM.WXStat[] WeChatWASM.WXFileSystemManager::StatSync(System.String)
extern void WXFileSystemManager_StatSync_mEC9B6F13AF070FEE3EAEEB6E0A613E2ED06EC1B2 (void);
// 0x00000135 System.String WeChatWASM.WXFileSystemManager::A(System.String,System.Byte[],System.Int32)
extern void WXFileSystemManager_A_m54C632EA68E82BBA8165E595008A5FE3F16B5E08 (void);
// 0x00000136 System.String WeChatWASM.WXFileSystemManager::c(System.String)
extern void WXFileSystemManager_c_mF9819E9B7EFDC37CCC0190E52A10DC70413E43D2 (void);
// 0x00000137 WeChatWASM.WriteResult WeChatWASM.WXFileSystemManager::WriteSync(WeChatWASM.WriteSyncOption)
extern void WXFileSystemManager_WriteSync_m0C4CB9A33EE790C46CBDFA29548CF179479FDCBE (void);
// 0x00000138 WeChatWASM.WriteResult WeChatWASM.WXFileSystemManager::WriteSync(WeChatWASM.WriteSyncStringOption)
extern void WXFileSystemManager_WriteSync_m70710D645CF53DD161778544925119CB11DFB3D8 (void);
// 0x00000139 System.String WeChatWASM.WXFileSystemManager::D(System.String)
extern void WXFileSystemManager_D_m4EF0BB8E5CFE37059E0FA41D21C70AC0FC0AD288 (void);
// 0x0000013A System.String WeChatWASM.WXFileSystemManager::OpenSync(WeChatWASM.OpenSyncOption)
extern void WXFileSystemManager_OpenSync_m62490F9238C6FCEE27FACE5D746FBC54F5B503B9 (void);
// 0x0000013B System.String WeChatWASM.WXFileSystemManager::k(System.String,System.String)
extern void WXFileSystemManager_k_mD4795145F7058A5A9A5A125624619F0790E2B094 (void);
// 0x0000013C System.String WeChatWASM.WXFileSystemManager::SaveFileSync(System.String,System.String)
extern void WXFileSystemManager_SaveFileSync_m3DDBA037D321CB6E5687EE117C60B491088BBC6D (void);
// 0x0000013D System.Void WeChatWASM.WXFileSystemManager::d(System.String)
extern void WXFileSystemManager_d_mB7E15196A216EE5D5E9E41E8F1CB7A2C957B7AEC (void);
// 0x0000013E System.Void WeChatWASM.WXFileSystemManager::CloseSync(WeChatWASM.CloseSyncOption)
extern void WXFileSystemManager_CloseSync_m0EE64EE5C7A6DF234DE32DB48FDCBF9D2C7D1EFD (void);
// 0x0000013F System.Void WeChatWASM.WXFileSystemManager::E(System.String)
extern void WXFileSystemManager_E_mEA94915A7EF1C3DA345081D9D08F1FCE4467C03A (void);
// 0x00000140 System.Void WeChatWASM.WXFileSystemManager::FtruncateSync(WeChatWASM.FtruncateSyncOption)
extern void WXFileSystemManager_FtruncateSync_m4768ECC26A015732F432A6F2BEDE345A77955EE4 (void);
// 0x00000141 System.Void WeChatWASM.WXFileSystemManager::e(System.String)
extern void WXFileSystemManager_e_m788381E49EF9C18FD0B416D2B3D26ADAD55A3FA8 (void);
// 0x00000142 System.Void WeChatWASM.WXFileSystemManager::TruncateSync(WeChatWASM.TruncateSyncOption)
extern void WXFileSystemManager_TruncateSync_m1AAA461C6C3D6171BA8BC2A16D8EE8022EE0E789 (void);
// 0x00000143 System.Void WeChatWASM.WXFileSystemManager::.ctor()
extern void WXFileSystemManager__ctor_m94A82C30476311581C5A4BA7F257797E00D632D6 (void);
// 0x00000144 System.Void WeChatWASM.WXFileSystemManager::.cctor()
extern void WXFileSystemManager__cctor_m1DEB0EB91E402CE2B02048EBEB3E1018B72CA86D (void);
// 0x00000145 System.Void WeChatWASM.WXFont::A(System.String,System.String)
extern void WXFont_A_m1F5D52B763EF78F2C67ADF9CFDA60FDA186836BD (void);
// 0x00000146 System.String WeChatWASM.WXFont::A(System.Byte[],System.String)
extern void WXFont_A_mAF36F5DD5B8C1574F93CF6EA1CCD7C7C04291520 (void);
// 0x00000147 System.Void WeChatWASM.WXFont::GetFontData(WeChatWASM.GetFontParam)
extern void WXFont_GetFontData_m56CCBE553C89A5D733A6A3060B490A0D8E69C5B5 (void);
// 0x00000148 System.Void WeChatWASM.WXFont::HandleGetFontRawDataCallback(System.String)
extern void WXFont_HandleGetFontRawDataCallback_m4D9B3E45E91C79CBA97B0B17DA901E8C5B4D6FEC (void);
// 0x00000149 System.Void WeChatWASM.WXFont::.ctor()
extern void WXFont__ctor_m82B8DBD396F0ABBC2372480E5F1C86E67EB778E8 (void);
// 0x0000014A System.Void WeChatWASM.WXFont::.cctor()
extern void WXFont__cctor_mA0D5069F657F02BD9B02CD6AC1246D2746909CF4 (void);
// 0x0000014B System.Void WeChatWASM.WXGameClubButton::A(System.String)
extern void WXGameClubButton_A_m5A57A97D393C8ED275915FA99D85F73D74C7EB61 (void);
// 0x0000014C System.Void WeChatWASM.WXGameClubButton::a(System.String)
extern void WXGameClubButton_a_mC1E810136460FF2B607158056EA3A6947A9EE7F1 (void);
// 0x0000014D System.Void WeChatWASM.WXGameClubButton::A(System.String,System.String,System.String)
extern void WXGameClubButton_A_m48039D2AEBE467735E97B403585FC89B9EFB84AE (void);
// 0x0000014E System.Void WeChatWASM.WXGameClubButton::B(System.String)
extern void WXGameClubButton_B_mF24CBC7C524E674364CB327DDB960CB82EF106A3 (void);
// 0x0000014F System.Void WeChatWASM.WXGameClubButton::A(System.String,System.String)
extern void WXGameClubButton_A_mFFC6CD9E346436132F7FA3E6FD098CD6158162C4 (void);
// 0x00000150 System.Void WeChatWASM.WXGameClubButton::a(System.String,System.String)
extern void WXGameClubButton_a_m3FEE242352533A22D6984A2A614E25E762F08F3E (void);
// 0x00000151 System.Void WeChatWASM.WXGameClubButton::.ctor(System.String,WeChatWASM.WXCreateGameClubButtonParam)
extern void WXGameClubButton__ctor_mD659E9CA2652F112E1D36F148E315E9B2BA76BCC (void);
// 0x00000152 WeChatWASM.GameClubButtonIcon WeChatWASM.WXGameClubButton::get_icon()
extern void WXGameClubButton_get_icon_mF6C3FBECD087174C58467D4C734B02D877910AFA (void);
// 0x00000153 System.Void WeChatWASM.WXGameClubButton::set_icon(WeChatWASM.GameClubButtonIcon)
extern void WXGameClubButton_set_icon_m729220A2C4D1F1C0A10595BD7EAEAC9EB962308F (void);
// 0x00000154 WeChatWASM.GameClubButtonType WeChatWASM.WXGameClubButton::get_type()
extern void WXGameClubButton_get_type_m566EC00E3DA1EF6C99D7C247EE1203F117458B3E (void);
// 0x00000155 System.Void WeChatWASM.WXGameClubButton::set_type(WeChatWASM.GameClubButtonType)
extern void WXGameClubButton_set_type_m5870F9FF019DAC720B407C501D6C78382C12FFD2 (void);
// 0x00000156 System.String WeChatWASM.WXGameClubButton::get_text()
extern void WXGameClubButton_get_text_m746C64B9989FE55DF6FD51F0D29F90AB587E20DD (void);
// 0x00000157 System.Void WeChatWASM.WXGameClubButton::set_text(System.String)
extern void WXGameClubButton_set_text_m7CA027BC607F93C364FDD1E5FE8D22E09B198E0D (void);
// 0x00000158 System.String WeChatWASM.WXGameClubButton::get_image()
extern void WXGameClubButton_get_image_m382C406AEFE343D788BD1FDE9EF33F406DAC81DE (void);
// 0x00000159 System.Void WeChatWASM.WXGameClubButton::set_image(System.String)
extern void WXGameClubButton_set_image_mD28FFDA8AF853D5A02A02A20C82A5360D7A0983D (void);
// 0x0000015A WeChatWASM.WXGameClubButtonStyle WeChatWASM.WXGameClubButton::get_styleObj()
extern void WXGameClubButton_get_styleObj_m6AC492C9265C06A84AE5B1CD4CCAD3260B15ACA5 (void);
// 0x0000015B System.Void WeChatWASM.WXGameClubButton::set_styleObj(WeChatWASM.WXGameClubButtonStyle)
extern void WXGameClubButton_set_styleObj_m917DA390978633A116AE39DDD874B2BD70ABBB9C (void);
// 0x0000015C System.Void WeChatWASM.WXGameClubButton::Destroy()
extern void WXGameClubButton_Destroy_m8F0C1A8319082917A0D34B18FA633EEA936619FD (void);
// 0x0000015D System.Void WeChatWASM.WXGameClubButton::Hide()
extern void WXGameClubButton_Hide_m40BC14A215814F65460CB0F890D5449247EC49FA (void);
// 0x0000015E System.Void WeChatWASM.WXGameClubButton::Show()
extern void WXGameClubButton_Show_mA7823782B538124FFD4FDB105FC2E50B64C50457 (void);
// 0x0000015F System.Void WeChatWASM.WXGameClubButton::OnTap(System.Action)
extern void WXGameClubButton_OnTap_m2460D0FB36AEA25289D2E1CBE23FCA3015B497FB (void);
// 0x00000160 System.Void WeChatWASM.WXGameClubButton::OffTap(System.Action)
extern void WXGameClubButton_OffTap_mC6E5001DF80D1C8D9DA42372A9D50A662ADCDCB5 (void);
// 0x00000161 System.Void WeChatWASM.WXGameClubButton::_HandleCallBack(System.String)
extern void WXGameClubButton__HandleCallBack_mB3EFA6D1F86EFB44E90A291141655A7CA9133E00 (void);
// 0x00000162 System.Void WeChatWASM.WXGameClubButton::.cctor()
extern void WXGameClubButton__cctor_mB5D7431C1D63514AA35DA3E35544469DB090C1DE (void);
// 0x00000163 System.Void WeChatWASM.WXGameClubButtonStyle::.ctor(System.String,WeChatWASM.GameClubButtonStyle)
extern void WXGameClubButtonStyle__ctor_m4790FB4735C6CE2404B87B654CF8857F809FFC54 (void);
// 0x00000164 WeChatWASM.GameClubButtonStyle WeChatWASM.WXGameClubButtonStyle::get_styleObj()
extern void WXGameClubButtonStyle_get_styleObj_m704E0779BF681C476C6BB42C043290DE65533EEF (void);
// 0x00000165 System.Int32 WeChatWASM.WXGameClubButtonStyle::get_left()
extern void WXGameClubButtonStyle_get_left_m827925CF7788EC76D6FF9C39549DF6BCA55D9D69 (void);
// 0x00000166 System.Void WeChatWASM.WXGameClubButtonStyle::set_left(System.Int32)
extern void WXGameClubButtonStyle_set_left_m69ED0DE3800078467FBD72884DC68B7827112677 (void);
// 0x00000167 System.Int32 WeChatWASM.WXGameClubButtonStyle::get_top()
extern void WXGameClubButtonStyle_get_top_m368D52FCC4770BFC2C8B75AAC6F79EB7DA672559 (void);
// 0x00000168 System.Void WeChatWASM.WXGameClubButtonStyle::set_top(System.Int32)
extern void WXGameClubButtonStyle_set_top_m236D035C2D906CA5E1285059A0EFEC1B09D8F126 (void);
// 0x00000169 System.Int32 WeChatWASM.WXGameClubButtonStyle::get_width()
extern void WXGameClubButtonStyle_get_width_mCFCFBF1AD944B3F9C61B46B5B80394DAB3C96422 (void);
// 0x0000016A System.Void WeChatWASM.WXGameClubButtonStyle::set_width(System.Int32)
extern void WXGameClubButtonStyle_set_width_m4FFEAA42E8070DA1B00E0869667B5255A08C36C9 (void);
// 0x0000016B System.Int32 WeChatWASM.WXGameClubButtonStyle::get_height()
extern void WXGameClubButtonStyle_get_height_m35790BDFE1496718E0DAA809FE8A3072959818F1 (void);
// 0x0000016C System.Void WeChatWASM.WXGameClubButtonStyle::set_height(System.Int32)
extern void WXGameClubButtonStyle_set_height_m259A4C2846383163F5BB7A6F57E682190EBAEEBF (void);
// 0x0000016D System.Int32 WeChatWASM.WXGameClubButtonStyle::get_borderWidth()
extern void WXGameClubButtonStyle_get_borderWidth_m39417608DCE1E49B11115ADDB05ED2E6DB03BF45 (void);
// 0x0000016E System.Void WeChatWASM.WXGameClubButtonStyle::set_borderWidth(System.Int32)
extern void WXGameClubButtonStyle_set_borderWidth_m866CE0EF215EDAC5964B5B83138564A4E1A04C6C (void);
// 0x0000016F System.Int32 WeChatWASM.WXGameClubButtonStyle::get_borderRadius()
extern void WXGameClubButtonStyle_get_borderRadius_mF5628DFE626D2DEF6ADDD5BF58EC583AEC0CA23C (void);
// 0x00000170 System.Void WeChatWASM.WXGameClubButtonStyle::set_borderRadius(System.Int32)
extern void WXGameClubButtonStyle_set_borderRadius_m2FA5F3FAEE972404D5E8F207572D58A7A9FA7E83 (void);
// 0x00000171 System.Int32 WeChatWASM.WXGameClubButtonStyle::get_fontSize()
extern void WXGameClubButtonStyle_get_fontSize_mAC2E3E872CB0234EE2B8F939808929820090D6E5 (void);
// 0x00000172 System.Void WeChatWASM.WXGameClubButtonStyle::set_fontSize(System.Int32)
extern void WXGameClubButtonStyle_set_fontSize_m8DB9902778BDC30B9316B29B166EC3D49A04045E (void);
// 0x00000173 System.Int32 WeChatWASM.WXGameClubButtonStyle::get_lineHeight()
extern void WXGameClubButtonStyle_get_lineHeight_mD8A3A6E687F9ADDF9D3F4B457616A0C73E57D514 (void);
// 0x00000174 System.Void WeChatWASM.WXGameClubButtonStyle::set_lineHeight(System.Int32)
extern void WXGameClubButtonStyle_set_lineHeight_mB3CB0DE75261FE8330F181AE7E22DCED58348C97 (void);
// 0x00000175 System.String WeChatWASM.WXGameClubButtonStyle::get_backgroundColor()
extern void WXGameClubButtonStyle_get_backgroundColor_m1469F35AE99CB82879D375A8DCB11C6729B9FC42 (void);
// 0x00000176 System.Void WeChatWASM.WXGameClubButtonStyle::set_backgroundColor(System.String)
extern void WXGameClubButtonStyle_set_backgroundColor_mB74E48D6DEE050BD15068A8F67160F9567F6AB64 (void);
// 0x00000177 System.String WeChatWASM.WXGameClubButtonStyle::get_borderColor()
extern void WXGameClubButtonStyle_get_borderColor_mE55C97A556A9324F7250DFE9D61898C0ACD26987 (void);
// 0x00000178 System.Void WeChatWASM.WXGameClubButtonStyle::set_borderColor(System.String)
extern void WXGameClubButtonStyle_set_borderColor_mF5484BEAE1170E57EF5E0B92E703DED116EB5965 (void);
// 0x00000179 System.String WeChatWASM.WXGameClubButtonStyle::get_color()
extern void WXGameClubButtonStyle_get_color_m0046F90546434A9AABF70E678D10213BB7C036CE (void);
// 0x0000017A System.Void WeChatWASM.WXGameClubButtonStyle::set_color(System.String)
extern void WXGameClubButtonStyle_set_color_mA58EA7718894A388FD76D61CD820E9DCA2EE01F8 (void);
// 0x0000017B WeChatWASM.GameClubButtonTextAlign WeChatWASM.WXGameClubButtonStyle::get_textAlign()
extern void WXGameClubButtonStyle_get_textAlign_mE04980A202CB88D4FDACC30CF0779070A7C6F035 (void);
// 0x0000017C System.Void WeChatWASM.WXGameClubButtonStyle::set_textAlign(WeChatWASM.GameClubButtonTextAlign)
extern void WXGameClubButtonStyle_set_textAlign_mD23A829BBD7BD91CC793DCAAE2DB0A8E7270156A (void);
// 0x0000017D System.Void WeChatWASM.WXGameRecorder::.ctor(System.String)
extern void WXGameRecorder__ctor_m90871441927A2E4F46A22E6613F7A65B7BAE4ADA (void);
// 0x0000017E System.Void WeChatWASM.WXGameRecorder::A(System.String,System.String)
extern void WXGameRecorder_A_mFC3D174D348FDDC4EEE0CEAAA0A8FC217721B62C (void);
// 0x0000017F System.Void WeChatWASM.WXGameRecorder::Off(System.String)
extern void WXGameRecorder_Off_m6369B9CB87C96495730C58FEE1741ED415F73B60 (void);
// 0x00000180 System.Void WeChatWASM.WXGameRecorder::a(System.String,System.String)
extern void WXGameRecorder_a_mCFCFDD39FEF0FDF936EEA8461CE153FED2947015 (void);
// 0x00000181 System.Void WeChatWASM.WXGameRecorder::On(System.String,System.Action`1<WeChatWASM.GameRecorderCallbackRes>)
extern void WXGameRecorder_On_m63B6C7D8EE9413CA7190A56F6F2C136C9F35CA7F (void);
// 0x00000182 System.Void WeChatWASM.WXGameRecorder::B(System.String,System.String)
extern void WXGameRecorder_B_m738D9CB3315042437BD822C1797C1BD824B9710F (void);
// 0x00000183 System.Void WeChatWASM.WXGameRecorder::Start(WeChatWASM.GameRecorderStartOption)
extern void WXGameRecorder_Start_mD02684EA6B127CC9842513C9F075BCC041FA1AE6 (void);
// 0x00000184 System.Void WeChatWASM.WXGameRecorder::A(System.String)
extern void WXGameRecorder_A_mD496CA6F66B7984798DA9C8B08DB326A29C03A20 (void);
// 0x00000185 System.Void WeChatWASM.WXGameRecorder::Abort()
extern void WXGameRecorder_Abort_m1AFD203D3A25DF068AFFFA614EC0AAA921D85873 (void);
// 0x00000186 System.Void WeChatWASM.WXGameRecorder::a(System.String)
extern void WXGameRecorder_a_m100F6832D3651AD164E09310F432054C37DEDAEB (void);
// 0x00000187 System.Void WeChatWASM.WXGameRecorder::Pause()
extern void WXGameRecorder_Pause_mAA0E0E179CAFDAC80802B8129287E36364BAE444 (void);
// 0x00000188 System.Void WeChatWASM.WXGameRecorder::B(System.String)
extern void WXGameRecorder_B_mFB2E046325D240E31F07531843D8395B183DF38D (void);
// 0x00000189 System.Void WeChatWASM.WXGameRecorder::Resume()
extern void WXGameRecorder_Resume_mD129EC3C8D428AA325EE73B02397CE5AE7B0EB6F (void);
// 0x0000018A System.Void WeChatWASM.WXGameRecorder::b(System.String)
extern void WXGameRecorder_b_m9B125031725E35EA860F2A8AE4D864A70B0CCCCD (void);
// 0x0000018B System.Void WeChatWASM.WXGameRecorder::Stop()
extern void WXGameRecorder_Stop_m6808D91390204103E8D75F3C6343E294D0F1F49E (void);
// 0x0000018C System.Void WeChatWASM.WXGameRecorder::.cctor()
extern void WXGameRecorder__cctor_m948DBB1E62A125F3D0D98E62A7C587F70AF8B658 (void);
// 0x0000018D System.Void WeChatWASM.GameRecorderStartOption::.ctor()
extern void GameRecorderStartOption__ctor_mA8562A983B2ED945ABCEF6B3B65B6E558BD5FC63 (void);
// 0x0000018E System.Void WeChatWASM.GameRecorderCallbackRes::.ctor()
extern void GameRecorderCallbackRes__ctor_mC7815B29882B730FAAE71BDE60D66DE3FC797AD3 (void);
// 0x0000018F System.Void WeChatWASM.GameRecorderCallback::.ctor()
extern void GameRecorderCallback__ctor_m439B9D9C5E973EB2DCE7986DE9CDB36835650551 (void);
// 0x00000190 System.Void WeChatWASM.WXInnerAudioContext::A(System.String,System.String,System.Boolean)
extern void WXInnerAudioContext_A_mCC5B431411E276B510837178C80F326D839984E9 (void);
// 0x00000191 System.Void WeChatWASM.WXInnerAudioContext::A(System.String,System.String,System.String)
extern void WXInnerAudioContext_A_mEB069167335A738F73F2C4B467991D38D8A38478 (void);
// 0x00000192 System.Void WeChatWASM.WXInnerAudioContext::A(System.String,System.String,System.Single)
extern void WXInnerAudioContext_A_mCAD05B4619CE64EEFB8ACA428F60B1DA0B1003A8 (void);
// 0x00000193 System.Single WeChatWASM.WXInnerAudioContext::A(System.String,System.String)
extern void WXInnerAudioContext_A_m938C42E78994F516264591AF523D6DF289CC5D15 (void);
// 0x00000194 System.Boolean WeChatWASM.WXInnerAudioContext::a(System.String,System.String)
extern void WXInnerAudioContext_a_mDDAC7D845115FBC15215B4C6D6D1871CEBE89751 (void);
// 0x00000195 System.Void WeChatWASM.WXInnerAudioContext::A(System.String)
extern void WXInnerAudioContext_A_m6BCB6588D8BC453E79738447FA62A0881CCE6369 (void);
// 0x00000196 System.Void WeChatWASM.WXInnerAudioContext::a(System.String)
extern void WXInnerAudioContext_a_m6FACFE33007F5F5653602798CF0EA27AE804E3D6 (void);
// 0x00000197 System.Void WeChatWASM.WXInnerAudioContext::B(System.String)
extern void WXInnerAudioContext_B_m18FFFBE837A5487AECAE60344CA71F9CDFF36BFD (void);
// 0x00000198 System.Void WeChatWASM.WXInnerAudioContext::A(System.String,System.Single)
extern void WXInnerAudioContext_A_m5B30A5CEC4CABF77FC919FF661FAFAF76E1E4B24 (void);
// 0x00000199 System.Void WeChatWASM.WXInnerAudioContext::b(System.String)
extern void WXInnerAudioContext_b_m2DC0CFBC4781D8E66EC8D3E327DD964D05BFB77D (void);
// 0x0000019A System.Void WeChatWASM.WXInnerAudioContext::B(System.String,System.String)
extern void WXInnerAudioContext_B_m1611EBCD43FAA2D7FE78AFCBB4EED6755FAC9BB5 (void);
// 0x0000019B System.Void WeChatWASM.WXInnerAudioContext::b(System.String,System.String)
extern void WXInnerAudioContext_b_mAFFFE659D4180427794D4F9DF44BA13B0E0768B7 (void);
// 0x0000019C System.Void WeChatWASM.WXInnerAudioContext::.ctor(System.String,WeChatWASM.InnerAudioContextParam)
extern void WXInnerAudioContext__ctor_m4A99E5C7EFF72080EF521E4E6F60C806D7C82F68 (void);
// 0x0000019D System.Void WeChatWASM.WXInnerAudioContext::_HandleCallBack(System.String,System.String)
extern void WXInnerAudioContext__HandleCallBack_m9EB8C55D4B05BA1A90A307B1D9460BB0AC523C6B (void);
// 0x0000019E System.Boolean WeChatWASM.WXInnerAudioContext::get_autoplay()
extern void WXInnerAudioContext_get_autoplay_m8332FDA54B7BE6793591188CD3029728A3550913 (void);
// 0x0000019F System.Void WeChatWASM.WXInnerAudioContext::set_autoplay(System.Boolean)
extern void WXInnerAudioContext_set_autoplay_mD4AEF513346974F6530102BEB472B10882822741 (void);
// 0x000001A0 System.String WeChatWASM.WXInnerAudioContext::get_src()
extern void WXInnerAudioContext_get_src_m5B6E9F0D1673329441885F3D239805C19ACD572D (void);
// 0x000001A1 System.Void WeChatWASM.WXInnerAudioContext::set_src(System.String)
extern void WXInnerAudioContext_set_src_m138A42B19CA284CE3C16719C47886891E2DE082A (void);
// 0x000001A2 System.Boolean WeChatWASM.WXInnerAudioContext::get_needDownload()
extern void WXInnerAudioContext_get_needDownload_mE19CF42194E74C89324D73505D3D080F6CC8014D (void);
// 0x000001A3 System.Void WeChatWASM.WXInnerAudioContext::set_needDownload(System.Boolean)
extern void WXInnerAudioContext_set_needDownload_m0E945D03451F0CDEC85DF845C0C219BC9BF61455 (void);
// 0x000001A4 System.Single WeChatWASM.WXInnerAudioContext::get_startTime()
extern void WXInnerAudioContext_get_startTime_mD02A7AE8C38EDDBA3FA8CE2AF98D78D7F852D5F8 (void);
// 0x000001A5 System.Void WeChatWASM.WXInnerAudioContext::set_startTime(System.Single)
extern void WXInnerAudioContext_set_startTime_m77AC475473D33E954C1ACF23737761FAEB1B1869 (void);
// 0x000001A6 System.Boolean WeChatWASM.WXInnerAudioContext::get_loop()
extern void WXInnerAudioContext_get_loop_m9D8BFBCC1BD95ABA1104C6B5251CA7DC42EDFA95 (void);
// 0x000001A7 System.Void WeChatWASM.WXInnerAudioContext::set_loop(System.Boolean)
extern void WXInnerAudioContext_set_loop_mB027B701BE5456C906C8C5F2D46EC6C0E0D7FC6B (void);
// 0x000001A8 System.Single WeChatWASM.WXInnerAudioContext::get_volume()
extern void WXInnerAudioContext_get_volume_m2FEA9487EA81C98054EA759EADEDB5B6996A7A6B (void);
// 0x000001A9 System.Void WeChatWASM.WXInnerAudioContext::set_volume(System.Single)
extern void WXInnerAudioContext_set_volume_mE5BFEF51CB435F315777D9CD8B60182A244850EE (void);
// 0x000001AA System.Boolean WeChatWASM.WXInnerAudioContext::get_mute()
extern void WXInnerAudioContext_get_mute_mE5B30C2DF9B6D47BC8D430F5FE2BFCA6DFFC8B80 (void);
// 0x000001AB System.Void WeChatWASM.WXInnerAudioContext::set_mute(System.Boolean)
extern void WXInnerAudioContext_set_mute_mF4B7D5DB8ADF06DEBE26325D2D3FDF944E1C92A9 (void);
// 0x000001AC System.Single WeChatWASM.WXInnerAudioContext::get_playbackRate()
extern void WXInnerAudioContext_get_playbackRate_m631D4FF786155D9D72AB94472E635084C38BFB9D (void);
// 0x000001AD System.Void WeChatWASM.WXInnerAudioContext::set_playbackRate(System.Single)
extern void WXInnerAudioContext_set_playbackRate_mA7ECAFB591918EC57894F2C23D98A77B77D87545 (void);
// 0x000001AE System.Single WeChatWASM.WXInnerAudioContext::get_duration()
extern void WXInnerAudioContext_get_duration_mA0B9D13FF35AFE1B34EE9C7DE9AF79AF3C0D8E77 (void);
// 0x000001AF System.Single WeChatWASM.WXInnerAudioContext::get_currentTime()
extern void WXInnerAudioContext_get_currentTime_m481335124A00A206A6CC88DE0481496BFA910275 (void);
// 0x000001B0 System.Single WeChatWASM.WXInnerAudioContext::get_buffered()
extern void WXInnerAudioContext_get_buffered_m9756B29D5D40282F5B50A4B982868B22E4A2D334 (void);
// 0x000001B1 System.Boolean WeChatWASM.WXInnerAudioContext::get_paused()
extern void WXInnerAudioContext_get_paused_m9D5DC2C3D4251BD482DE60EE7406C9A4F0A80D26 (void);
// 0x000001B2 System.Boolean WeChatWASM.WXInnerAudioContext::get_isPlaying()
extern void WXInnerAudioContext_get_isPlaying_mCE239157D2C5AB5FB7F8692319815BCD9149DFCC (void);
// 0x000001B3 System.Void WeChatWASM.WXInnerAudioContext::Play()
extern void WXInnerAudioContext_Play_m3BCCF9354C6BF190B74362779A6971CE2AC4BA9D (void);
// 0x000001B4 System.Void WeChatWASM.WXInnerAudioContext::Pause()
extern void WXInnerAudioContext_Pause_mB0AE7BF853F3BC1FC3FACC0822ABE440C10A49A2 (void);
// 0x000001B5 System.Void WeChatWASM.WXInnerAudioContext::Stop()
extern void WXInnerAudioContext_Stop_m71A150E8A7516986455B48A57B1504A50877B915 (void);
// 0x000001B6 System.Void WeChatWASM.WXInnerAudioContext::Seek(System.Single)
extern void WXInnerAudioContext_Seek_m00CD843404ACEF377C309A4B73E4D89581ADF3A1 (void);
// 0x000001B7 System.Void WeChatWASM.WXInnerAudioContext::Destroy()
extern void WXInnerAudioContext_Destroy_m466B97E027311156364EC197ECB11A496B517191 (void);
// 0x000001B8 System.Void WeChatWASM.WXInnerAudioContext::OnCanplay(System.Action)
extern void WXInnerAudioContext_OnCanplay_m85085F48B9239820B8F3000B71B3E53A6AE37F53 (void);
// 0x000001B9 System.Void WeChatWASM.WXInnerAudioContext::OffCanplay(System.Action)
extern void WXInnerAudioContext_OffCanplay_mBFC05E025E2CD32A4E9ED8420A81A262FF2F69BC (void);
// 0x000001BA System.Void WeChatWASM.WXInnerAudioContext::OnPlay(System.Action)
extern void WXInnerAudioContext_OnPlay_m83AF0CEB631684568633A119E0631C0D7FC94921 (void);
// 0x000001BB System.Void WeChatWASM.WXInnerAudioContext::OffPlay(System.Action)
extern void WXInnerAudioContext_OffPlay_mD56E599FC3AA486C2AC8AAD45CB9A0F569326722 (void);
// 0x000001BC System.Void WeChatWASM.WXInnerAudioContext::OnPause(System.Action)
extern void WXInnerAudioContext_OnPause_m7728168767B5D206BC9C28AB9131DA58EA018DEB (void);
// 0x000001BD System.Void WeChatWASM.WXInnerAudioContext::OffPause(System.Action)
extern void WXInnerAudioContext_OffPause_m120DC1F361C6453EA09DE6A9C84620B8709082ED (void);
// 0x000001BE System.Void WeChatWASM.WXInnerAudioContext::OnStop(System.Action)
extern void WXInnerAudioContext_OnStop_m0072ED8FE1F391B29BD442AD027E4983AD56F8BD (void);
// 0x000001BF System.Void WeChatWASM.WXInnerAudioContext::OffStop(System.Action)
extern void WXInnerAudioContext_OffStop_mD44706E698436D6A5231EF036FEA158F68BF5822 (void);
// 0x000001C0 System.Void WeChatWASM.WXInnerAudioContext::OnEnded(System.Action)
extern void WXInnerAudioContext_OnEnded_m3BDB9FEAFD6F68D5B1D8D08D8E9336480C7816D0 (void);
// 0x000001C1 System.Void WeChatWASM.WXInnerAudioContext::OffEnded(System.Action)
extern void WXInnerAudioContext_OffEnded_m705FBD4AFD1DD35140F4DD9FADB7FCD177910FE0 (void);
// 0x000001C2 System.Void WeChatWASM.WXInnerAudioContext::OnTimeUpdate(System.Action)
extern void WXInnerAudioContext_OnTimeUpdate_m2B82FBA36D410405B78730F0CBA70593375AB865 (void);
// 0x000001C3 System.Void WeChatWASM.WXInnerAudioContext::OffTimeUpdate(System.Action)
extern void WXInnerAudioContext_OffTimeUpdate_m102ED12B97B82CFE1F6818308245272BB8F0D43F (void);
// 0x000001C4 System.Void WeChatWASM.WXInnerAudioContext::OnError(System.Action`1<WeChatWASM.InnerAudioContextOnErrorListenerResult>)
extern void WXInnerAudioContext_OnError_m1F8DC3ED09CD83E593A73E44C5B1C1712C4E0146 (void);
// 0x000001C5 System.Void WeChatWASM.WXInnerAudioContext::OffError(System.Action`1<WeChatWASM.InnerAudioContextOnErrorListenerResult>)
extern void WXInnerAudioContext_OffError_mE3F15607D35895E71E2D91BAD944DABB76D65AC4 (void);
// 0x000001C6 System.Void WeChatWASM.WXInnerAudioContext::OnWaiting(System.Action)
extern void WXInnerAudioContext_OnWaiting_m726AFC1D84E4E8C4FB20F9A70C39E989DED46835 (void);
// 0x000001C7 System.Void WeChatWASM.WXInnerAudioContext::OffWaiting(System.Action)
extern void WXInnerAudioContext_OffWaiting_m0DCCC0CA31EFDE6621F7B8901D07AD7E60D2E1BE (void);
// 0x000001C8 System.Void WeChatWASM.WXInnerAudioContext::OnSeeking(System.Action)
extern void WXInnerAudioContext_OnSeeking_m6B8BBD160D9DA20B67DF5D1CF174C45741ECFD24 (void);
// 0x000001C9 System.Void WeChatWASM.WXInnerAudioContext::OffSeeking(System.Action)
extern void WXInnerAudioContext_OffSeeking_m2925CB97F293B46D300BB0743DBF44F06DEA4894 (void);
// 0x000001CA System.Void WeChatWASM.WXInnerAudioContext::OnSeeked(System.Action)
extern void WXInnerAudioContext_OnSeeked_mACECD202640A041B47964F3C6AC4239D837F5AE3 (void);
// 0x000001CB System.Void WeChatWASM.WXInnerAudioContext::OffSeeked(System.Action)
extern void WXInnerAudioContext_OffSeeked_m5C7C0C868D11E7454E329E360082F190C9DCB7F1 (void);
// 0x000001CC System.Void WeChatWASM.WXInnerAudioContext::.cctor()
extern void WXInnerAudioContext__cctor_m3CDDA8D30CC230D41273DCD3E86992ABF647AC88 (void);
// 0x000001CD System.Void WeChatWASM.WXInnerAudioContext::A()
extern void WXInnerAudioContext_A_m39FCECA9FBCA63BC0F425E00B3E8459A7E4A0EDD (void);
// 0x000001CE System.Void WeChatWASM.WXInnerAudioContext::a()
extern void WXInnerAudioContext_a_m12EBA13620EB6541D25C395EA91A9ACD05FC584D (void);
// 0x000001CF System.Void WeChatWASM.WXInnerAudioContext::B()
extern void WXInnerAudioContext_B_m342407CF975D166E6759369BE76E4C65FAD7CE4F (void);
// 0x000001D0 System.Void WeChatWASM.WXInnerAudioContext::b()
extern void WXInnerAudioContext_b_mE7250FEE3520CED4AF8FA227DEB22E7B06BAE346 (void);
// 0x000001D1 System.Void WeChatWASM.WXInterstitialAd::.ctor(System.String)
extern void WXInterstitialAd__ctor_m7A41CC58975CEB7AC4D472AED96A604D4EB5B6CC (void);
// 0x000001D2 System.Void WeChatWASM.WXInterstitialAd::Load(System.Action`1<WeChatWASM.WXTextResponse>,System.Action`1<WeChatWASM.WXADErrorResponse>)
extern void WXInterstitialAd_Load_m2DDA990F4B4DB640A4D5FDB2AD6B7A2239DD6D13 (void);
// 0x000001D3 System.Void WeChatWASM.WXInterstitialAd::OnClose(System.Action)
extern void WXInterstitialAd_OnClose_m54992787D926352D08026AFBFCB11161FFC32864 (void);
// 0x000001D4 System.Void WeChatWASM.WXInterstitialAd::OffClose(System.Action)
extern void WXInterstitialAd_OffClose_m6D3EB797318C03EB4971478B7EBB1562D9189340 (void);
// 0x000001D5 System.Void WeChatWASM.WXInterstitialAd::OnCloseCallback()
extern void WXInterstitialAd_OnCloseCallback_m2CD8F1D69EED5516646EB54D7CF27E1896498ECE (void);
// 0x000001D6 System.Void WeChatWASM.LaunchProgressParams::.ctor()
extern void LaunchProgressParams__ctor_mCE461380DA3ED0C85B91CC197F7D131783E95EE1 (void);
// 0x000001D7 System.String WeChatWASM.WXLaunchEventListener::A()
extern void WXLaunchEventListener_A_m736C2D6AD9B7D9A779F6EA3C7BEA348BD633E12C (void);
// 0x000001D8 System.Void WeChatWASM.WXLaunchEventListener::.ctor(System.Action`1<WeChatWASM.LaunchEvent>)
extern void WXLaunchEventListener__ctor_m15C3F544AF0D29020BD7B6CD9137B631F55EEFD1 (void);
// 0x000001D9 System.Void WeChatWASM.WXLaunchEventListener::.cctor()
extern void WXLaunchEventListener__cctor_m8B8610F8DA53E01E531AA3B9B29146731764E335 (void);
// 0x000001DA System.Void WeChatWASM.WXLogManager::.ctor(System.String,WeChatWASM.GetLogManagerOption)
extern void WXLogManager__ctor_mEE71805357F9EF8E1C8B7C6B5C50B30CFDC5D0A9 (void);
// 0x000001DB System.Void WeChatWASM.WXLogManager::A(System.String,System.String)
extern void WXLogManager_A_mDC1023CDC59FDF6057CFCC58B79A72C4C6493ABD (void);
// 0x000001DC System.Void WeChatWASM.WXLogManager::Debug(System.String)
extern void WXLogManager_Debug_m822B9817D0CCBDD6B159C37995DDC5A49E2CE806 (void);
// 0x000001DD System.Void WeChatWASM.WXLogManager::a(System.String,System.String)
extern void WXLogManager_a_m82FB7BA03E2B4649FE4863D64392E069256DD484 (void);
// 0x000001DE System.Void WeChatWASM.WXLogManager::Info(System.String)
extern void WXLogManager_Info_m14980D8500F07AC7B727EE6C0C5B4F0A43BF8720 (void);
// 0x000001DF System.Void WeChatWASM.WXLogManager::B(System.String,System.String)
extern void WXLogManager_B_mD9EDFBB4864A56EE193802A0A9BC5143620F80BC (void);
// 0x000001E0 System.Void WeChatWASM.WXLogManager::Log(System.String)
extern void WXLogManager_Log_mB1E9C575650066A69231B55AE6E9E3ECDEE80D53 (void);
// 0x000001E1 System.Void WeChatWASM.WXLogManager::b(System.String,System.String)
extern void WXLogManager_b_m34C911A505B140F4B166EDD583A64B63598F235F (void);
// 0x000001E2 System.Void WeChatWASM.WXLogManager::Warn(System.String)
extern void WXLogManager_Warn_mEA5643B7BCA7F05A59F2364CF11323D106DDC6DF (void);
// 0x000001E3 System.Int32 A.b::A()
extern void b_A_mE69E42D28E8AA3ED263522A1C8BA0FAAAE4FE6E5 (void);
// 0x000001E4 System.String A.b::A(System.Action`1<A>)
// 0x000001E5 System.Void A.b::A(System.String)
// 0x000001E6 System.Void A.b::.ctor()
extern void b__ctor_mDF75E264434929DAC67D18172A353AEB1DC82748 (void);
// 0x000001E7 System.Void A.b::.cctor()
extern void b__cctor_mDC806E41285B3B67E9459B1E8C64B08D417F1BB6 (void);
// 0x000001E8 System.Void WeChatWASM.WXJSTypeCallback::.ctor()
extern void WXJSTypeCallback__ctor_m4F9EBEDF17B404A1F222E47F99DBCD594913CB91 (void);
// 0x000001E9 System.Void WeChatWASM.WXBaseResponse::.ctor()
extern void WXBaseResponse__ctor_mB8AB7AE058AF9F9260DE076BC2AD66598BD94238 (void);
// 0x000001EA System.Void WeChatWASM.WXBaseActionOption`1::InvokeSuccess(System.Object)
// 0x000001EB System.Void WeChatWASM.WXBaseActionOption`1::InvokeFail(System.Object)
// 0x000001EC System.Void WeChatWASM.WXBaseActionOption`1::InvokeComplete(System.Object)
// 0x000001ED System.Void WeChatWASM.WXBaseActionOption`1::.ctor()
// 0x000001EE System.Void WeChatWASM.WXBaseActionOption`2::InvokeSuccess(System.Object)
// 0x000001EF System.Void WeChatWASM.WXBaseActionOption`2::InvokeFail(System.Object)
// 0x000001F0 System.Void WeChatWASM.WXBaseActionOption`2::InvokeComplete(System.Object)
// 0x000001F1 System.Void WeChatWASM.WXBaseActionOption`2::.ctor()
// 0x000001F2 System.Void WeChatWASM.WXBaseActionOption`3::InvokeSuccess(System.Object)
// 0x000001F3 System.Void WeChatWASM.WXBaseActionOption`3::InvokeFail(System.Object)
// 0x000001F4 System.Void WeChatWASM.WXBaseActionOption`3::InvokeComplete(System.Object)
// 0x000001F5 System.Void WeChatWASM.WXBaseActionOption`3::.ctor()
// 0x000001F6 System.Void WeChatWASM.WXInnerAudioResponse::.ctor()
extern void WXInnerAudioResponse__ctor_mC916D9DA9B7A95E2C1E6CDD87A0041C118132021 (void);
// 0x000001F7 System.Void WeChatWASM.WXTextResponse::.ctor()
extern void WXTextResponse__ctor_m1036DFD7BF991500673381CD6D5F240BF631FC69 (void);
// 0x000001F8 System.Void WeChatWASM.WXReadFileResponse::.ctor()
extern void WXReadFileResponse__ctor_mEA60A03F21A4F9D338F0070178CF8DDE52B4EE65 (void);
// 0x000001F9 System.Void WeChatWASM.WXUserInfoResponse::.ctor()
extern void WXUserInfoResponse__ctor_mA2D00CCF6D83CCA55CCAE778DB82655E2FB47E6E (void);
// 0x000001FA System.Void WeChatWASM.WXADErrorResponse::.ctor()
extern void WXADErrorResponse__ctor_m81FD136D05174A2F26DC22A5DF954EBD23D22E25 (void);
// 0x000001FB System.Void WeChatWASM.WXADLoadResponse::.ctor()
extern void WXADLoadResponse__ctor_m3244D5C9CFF64177FDDC7FCC6055E60D295CFF3F (void);
// 0x000001FC System.Void WeChatWASM.WXADResizeResponse::.ctor()
extern void WXADResizeResponse__ctor_mC242C27353088D156D74E1E19B71944B7E5E2BC1 (void);
// 0x000001FD System.Void WeChatWASM.WXRewardedVideoAdOnCloseResponse::.ctor()
extern void WXRewardedVideoAdOnCloseResponse__ctor_m907F3CA103E84A15FB797E389F4DB97C1CA08C27 (void);
// 0x000001FE System.Void WeChatWASM.RequestAdReportShareBehaviorParam::.ctor()
extern void RequestAdReportShareBehaviorParam__ctor_mB2DE81A093D95438FD320AFA634F710CF940E072 (void);
// 0x000001FF System.Void WeChatWASM.WXRewardedVideoAdReportShareBehaviorResponse::.ctor()
extern void WXRewardedVideoAdReportShareBehaviorResponse__ctor_m285FEE50F3CBA28F91226411C6640FC9A4634F3F (void);
// 0x00000200 System.Void WeChatWASM.WXCloudCallFunctionResponse::.ctor()
extern void WXCloudCallFunctionResponse__ctor_mE9261221AFF83E707977DDB8769D5D7A85BF6A46 (void);
// 0x00000201 System.Void WeChatWASM.WXAccountInfo::.ctor()
extern void WXAccountInfo__ctor_mE383D05E405D6DB46399FDB59A91F8060724C14D (void);
// 0x00000202 System.Void WeChatWASM.TemplateInfo::.ctor()
extern void TemplateInfo__ctor_m87807947743CC3E5F6775C54816CC82348AD731F (void);
// 0x00000203 System.Void WeChatWASM.TemplateInfoItem::.ctor()
extern void TemplateInfoItem__ctor_mBF3F5E00C67FEE76A478023AFDF26CF0EA3B0C15 (void);
// 0x00000204 System.Void WeChatWASM.WXShareAppMessageParam::.ctor()
extern void WXShareAppMessageParam__ctor_mA6A26616D794C4D4F8D776249B8A3DB9C9785BDF (void);
// 0x00000205 System.Void WeChatWASM.WXCreateBannerAdParam::.ctor()
extern void WXCreateBannerAdParam__ctor_m8ACC2D4318C26C5ADD1D083FFB33C43851C2F94F (void);
// 0x00000206 System.Void WeChatWASM.WXCreateRewardedVideoAdParam::.ctor()
extern void WXCreateRewardedVideoAdParam__ctor_m884BBBB040C99488828641DBA5FCF032AE877928 (void);
// 0x00000207 System.Void WeChatWASM.WXCreateInterstitialAdParam::.ctor()
extern void WXCreateInterstitialAdParam__ctor_m7308687FC627D200589146ACD3DADE4A72C129EA (void);
// 0x00000208 System.Void WeChatWASM.WXCreateCustomAdParam::.ctor()
extern void WXCreateCustomAdParam__ctor_m2D09A11DCEA6AD2DF74AF696134110299A04C083 (void);
// 0x00000209 System.Void WeChatWASM.WXToTempFilePathSyncParam::.ctor()
extern void WXToTempFilePathSyncParam__ctor_m59632348F73228E26FF8D357EC7E403B26F5F996 (void);
// 0x0000020A System.Void WeChatWASM.ToTempFilePathParamSuccessCallbackResult::.ctor()
extern void ToTempFilePathParamSuccessCallbackResult__ctor_m0FDEC6B39F433B50EDA34499675DF10BE1DCE532 (void);
// 0x0000020B System.Void WeChatWASM.WXToTempFilePathParam::.ctor()
extern void WXToTempFilePathParam__ctor_m34A61FC0CD9EC7A93690EABD67159A5A4E6AA893 (void);
// 0x0000020C System.Void WeChatWASM.AccessParam::.ctor()
extern void AccessParam__ctor_m76242295CD52A695032894268C9F8EA913FCD608 (void);
// 0x0000020D System.Void WeChatWASM.UnlinkParam::.ctor()
extern void UnlinkParam__ctor_m769BDEC2A023FBD57D4A56087FE16C611C8B7EC9 (void);
// 0x0000020E System.Void WeChatWASM.MkdirParam::.ctor()
extern void MkdirParam__ctor_m33D200AEA7AC3200C14101F034B5F32DB58F0DE7 (void);
// 0x0000020F System.Void WeChatWASM.RmdirParam::.ctor()
extern void RmdirParam__ctor_mB0D606863EC541CCFDAA98416111AF8A8D600D7A (void);
// 0x00000210 System.Void WeChatWASM.CopyFileParam::.ctor()
extern void CopyFileParam__ctor_m08836402B6640631BCC57CAC38C072670B62C925 (void);
// 0x00000211 System.Void WeChatWASM.TouchEvent::.ctor()
extern void TouchEvent__ctor_m395714798AA26AAF8CA3DFDA95E3E72A2B129443 (void);
// 0x00000212 System.Void WeChatWASM.CallFunctionParam::.ctor()
extern void CallFunctionParam__ctor_m843E986A04A12A3E2D72F063FB8902628F131DB9 (void);
// 0x00000213 System.Void WeChatWASM.CallFunctionConf::.ctor()
extern void CallFunctionConf__ctor_mB976A9A9255791C91C9CEE41F0D74C12E4828E4B (void);
// 0x00000214 System.Void WeChatWASM.CallFunctionInitParam::.ctor()
extern void CallFunctionInitParam__ctor_m0E4A9BC5414C2E82B48B67C7CA1F42DE4FE87D6C (void);
// 0x00000215 System.Void WeChatWASM.InnerAudioContextParam::.ctor()
extern void InnerAudioContextParam__ctor_mBEFACA7301B347C0CD2C9AFE5E9DC8F355D562A9 (void);
// 0x00000216 System.Void WeChatWASM.NetworkStatus::.ctor()
extern void NetworkStatus__ctor_m8FC9607BBC798FECE41A588E10D790621AAA6F5F (void);
// 0x00000217 System.Void WeChatWASM.WriteFileParam::.ctor()
extern void WriteFileParam__ctor_mFED492280DC4CBAB23CCBFDFE1AC58D470359C78 (void);
// 0x00000218 System.Void WeChatWASM.WriteFileStringParam::.ctor()
extern void WriteFileStringParam__ctor_mEA15252785A805170C25D0CED61F7A11DF6F6BA8 (void);
// 0x00000219 System.Void WeChatWASM.ReadFileParam::.ctor()
extern void ReadFileParam__ctor_m28A41E35544BF59771E87D323788E4245D2EB9F5 (void);
// 0x0000021A System.Void WeChatWASM.GetFontResponse::.ctor()
extern void GetFontResponse__ctor_mACCCFDF56450517C02102D21FA218FADCFFBBA85 (void);
// 0x0000021B System.Void WeChatWASM.GetFontParam::.ctor()
extern void GetFontParam__ctor_m9FCABA84E92B85B94F09DEFCB0363BD0C315B96E (void);
// 0x0000021C System.Void WeChatWASM.WXGetFontCallback::.ctor()
extern void WXGetFontCallback__ctor_mB6962D1EBF7ABFB63DF4231331D67B909BA6A690 (void);
// 0x0000021D System.Void WeChatWASM.WXStatInfo::.ctor()
extern void WXStatInfo__ctor_mF837F1784107E70E544820175693436F3CED9736 (void);
// 0x0000021E System.Void WeChatWASM.WXStat::.ctor()
extern void WXStat__ctor_mD916D8C443796EA7D4EBF672C62BA78CE3AEF050 (void);
// 0x0000021F System.Void WeChatWASM.WXStatResponse::.ctor()
extern void WXStatResponse__ctor_mBEC924BA3944D44945E457277A6B40DEDBBB2168 (void);
// 0x00000220 System.Void WeChatWASM.WXStatOption::.ctor()
extern void WXStatOption__ctor_m6D3F95DA7DE60B81D0893C349729D050E5EFCAE0 (void);
// 0x00000221 System.Void WeChatWASM.WXVideoCallback::.ctor()
extern void WXVideoCallback__ctor_mDE62A33227486135212BC97B774EE48C89641F5B (void);
// 0x00000222 System.Void WeChatWASM.WXVideoProgress::.ctor()
extern void WXVideoProgress__ctor_m51EE87D28A9F16D66FCE39D6FA3EAAB1161B0BA5 (void);
// 0x00000223 System.Void WeChatWASM.WXVideoTimeUpdate::.ctor()
extern void WXVideoTimeUpdate__ctor_m3A45CA6A70CDD82D4F3FD643E957CAF8A6343B27 (void);
// 0x00000224 System.Void WeChatWASM.WXCreateVideoParam::.ctor()
extern void WXCreateVideoParam__ctor_m07BF803234A336999037AD5DB0509C0EA2D36925 (void);
// 0x00000225 System.Void WeChatWASM.WXCreateGameClubButtonParam::.ctor()
extern void WXCreateGameClubButtonParam__ctor_mDEEEE12CC97D7E64F65A6098D6BDF896C6B8CBAB (void);
// 0x00000226 System.Void WeChatWASM.LaunchEvent::.ctor()
extern void LaunchEvent__ctor_m342E0BE8718F55B9D22FF5640BF9156F676A7D0A (void);
// 0x00000227 System.Void WeChatWASM.InnerAudioContextOnErrorListenerResult::.ctor()
extern void InnerAudioContextOnErrorListenerResult__ctor_m102EBFD7EAD89A9D099CEF0D80FFD4A0C38734FB (void);
// 0x00000228 System.Void WeChatWASM.ReadCompressedFileSyncOption::.ctor()
extern void ReadCompressedFileSyncOption__ctor_m4C814765E4262F0A57ED4A2397F9016DD153EE3C (void);
// 0x00000229 System.Void WeChatWASM.FileError::.ctor()
extern void FileError__ctor_mAB78B16BF6B5622D5C31A0DCF3677970DA5B24C7 (void);
// 0x0000022A System.Void WeChatWASM.FileSystemManagerCloseOption::.ctor()
extern void FileSystemManagerCloseOption__ctor_m64581DCABB2DFAC584639A8F4DF9A968F3CA74CF (void);
// 0x0000022B System.Void WeChatWASM.CopyFileOption::.ctor()
extern void CopyFileOption__ctor_m3493C633D87E73BF713103F60051C578131B49C3 (void);
// 0x0000022C System.Void WeChatWASM.FstatOption::.ctor()
extern void FstatOption__ctor_m42CD789FE4602432E0407CDDB7AF0333AA29FAD2 (void);
// 0x0000022D System.Void WeChatWASM.FstatSuccessCallbackResult::.ctor()
extern void FstatSuccessCallbackResult__ctor_m530BE8D11F21FF61573CE8648DD7E91F1F57C32D (void);
// 0x0000022E System.Void WeChatWASM.Stats::.ctor()
extern void Stats__ctor_mF89A8A8C72FAC2DB3113AB857C023FA509B3734A (void);
// 0x0000022F System.Void WeChatWASM.FtruncateOption::.ctor()
extern void FtruncateOption__ctor_mFD5752391476101618E6A8A7C5A774E54B09A81D (void);
// 0x00000230 System.Void WeChatWASM.GetFileInfoOption::.ctor()
extern void GetFileInfoOption__ctor_mBA2F2BA0FEC60944B1C5FE8D2DA5CFBE45979B7A (void);
// 0x00000231 System.Void WeChatWASM.GetFileInfoSuccessCallbackResult::.ctor()
extern void GetFileInfoSuccessCallbackResult__ctor_m81BDD977813E61EF3A188BED03535B9453860531 (void);
// 0x00000232 System.Void WeChatWASM.GetSavedFileListOption::.ctor()
extern void GetSavedFileListOption__ctor_m23ECDEA91B85ECEC5E14EEF507194E742933AC0F (void);
// 0x00000233 System.Void WeChatWASM.GetSavedFileListSuccessCallbackResult::.ctor()
extern void GetSavedFileListSuccessCallbackResult__ctor_m60117C55524F11C25935FA81336E80344C7A25D5 (void);
// 0x00000234 System.Void WeChatWASM.FileItem::.ctor()
extern void FileItem__ctor_m96843FF2512E65C01F3448D27AC0639CABA52BC7 (void);
// 0x00000235 System.Void WeChatWASM.OpenOption::.ctor()
extern void OpenOption__ctor_m48D92EB39F92FDB95B62B79CF94E2C9284A4027A (void);
// 0x00000236 System.Void WeChatWASM.OpenSuccessCallbackResult::.ctor()
extern void OpenSuccessCallbackResult__ctor_m5E3576E24303653786CDAD8ECBA923A95CBAEE2B (void);
// 0x00000237 System.Void WeChatWASM.ReadOption::.ctor()
extern void ReadOption__ctor_mE24E917999B075AF3D273F08C63E6CB4B37E4BB4 (void);
// 0x00000238 System.Void WeChatWASM.ReadSuccessCallbackResult::.ctor()
extern void ReadSuccessCallbackResult__ctor_mF89E454E4EC8A1A78DBF8385EB679CA78A153330 (void);
// 0x00000239 System.Void WeChatWASM.ReadCompressedFileOption::.ctor()
extern void ReadCompressedFileOption__ctor_m389C3D65C22BF2706E3B42D8951EC65C1E7649BF (void);
// 0x0000023A System.Void WeChatWASM.ReadCompressedFileSuccessCallbackResult::.ctor()
extern void ReadCompressedFileSuccessCallbackResult__ctor_m3992577DC53B6268E772A6891005B06C7A9BD671 (void);
// 0x0000023B System.Void WeChatWASM.ReadZipEntryOptionString::.ctor()
extern void ReadZipEntryOptionString__ctor_m18041060A7831ADDC45E3489CF97A1AC55FDC518 (void);
// 0x0000023C System.Void WeChatWASM.ReadZipEntryOption::.ctor()
extern void ReadZipEntryOption__ctor_m210C8E345BDE3650C95C5092C5339C3CE6CA9240 (void);
// 0x0000023D System.Void WeChatWASM.EntryItem::.ctor()
extern void EntryItem__ctor_mC113894AA53D57A4447D3E1CAAC522D33929E5F9 (void);
// 0x0000023E System.Void WeChatWASM.ReadZipEntrySuccessCallbackResult::.ctor()
extern void ReadZipEntrySuccessCallbackResult__ctor_m5389BAF6B7BBADE026D75C850941645723EB7ED8 (void);
// 0x0000023F System.Void WeChatWASM.EntriesResult::.ctor()
extern void EntriesResult__ctor_m594A56BAEAFE4352BE27AC076146575738E11EAB (void);
// 0x00000240 System.Void WeChatWASM.ZipFileItem::.ctor()
extern void ZipFileItem__ctor_m8B83F0526C80D585C3D5DD482716D3D0BE704E1F (void);
// 0x00000241 System.Void WeChatWASM.ReaddirOption::.ctor()
extern void ReaddirOption__ctor_mF3CA3E163C255943597C9D7EE93E79811F80FD07 (void);
// 0x00000242 System.Void WeChatWASM.ReaddirSuccessCallbackResult::.ctor()
extern void ReaddirSuccessCallbackResult__ctor_mEA44FB742CC98D2A4EEFBF3839CE6F1B00C253BB (void);
// 0x00000243 System.Void WeChatWASM.RemoveSavedFileOption::.ctor()
extern void RemoveSavedFileOption__ctor_m35C94DA5D215BBA47EEF5511811F03D8976F8B0E (void);
// 0x00000244 System.Void WeChatWASM.RenameOption::.ctor()
extern void RenameOption__ctor_m5BF7071C139BF2E22C8924FB12F0558A22574834 (void);
// 0x00000245 System.Void WeChatWASM.SaveFileOption::.ctor()
extern void SaveFileOption__ctor_mC9F2352ACB09AA7B4CE23C8A35544B038457FC52 (void);
// 0x00000246 System.Void WeChatWASM.SaveFileSuccessCallbackResult::.ctor()
extern void SaveFileSuccessCallbackResult__ctor_m040A2B5300A0FA30D88B0B92E8A4ED86D83F6E7A (void);
// 0x00000247 System.Void WeChatWASM.StatOption::.ctor()
extern void StatOption__ctor_m5198A889570995D82A1844C2C04049ED32B6B3A6 (void);
// 0x00000248 System.Void WeChatWASM.StatSuccessCallbackResult::.ctor()
extern void StatSuccessCallbackResult__ctor_m7436DC1A4734B337857C14B56BD8DA939006BC07 (void);
// 0x00000249 System.Void WeChatWASM.TruncateOption::.ctor()
extern void TruncateOption__ctor_mEA917A3FC261F4694F7450A77E94976B6EBB7756 (void);
// 0x0000024A System.Void WeChatWASM.UnlinkOption::.ctor()
extern void UnlinkOption__ctor_m3AAAB604348B7AD769CCB0C2696FFA78966584EF (void);
// 0x0000024B System.Void WeChatWASM.UnzipOption::.ctor()
extern void UnzipOption__ctor_m6331A9D28C6F690C14D5B342ED861FC67BDDE529 (void);
// 0x0000024C System.Void WeChatWASM.WriteOption::.ctor()
extern void WriteOption__ctor_mC5DE86791C6985673740AFB9B2721A2A79105FD0 (void);
// 0x0000024D System.Void WeChatWASM.WriteStringOption::.ctor()
extern void WriteStringOption__ctor_mB808F8807ADE481C6C83623A22B3E13E80F6FA30 (void);
// 0x0000024E System.Void WeChatWASM.WriteSuccessCallbackResult::.ctor()
extern void WriteSuccessCallbackResult__ctor_mEDC2840A4326B8B9688E1FBB70FB13184A1FA6BF (void);
// 0x0000024F System.Void WeChatWASM.ReadSyncOption::.ctor()
extern void ReadSyncOption__ctor_m1EFC2B2E7BCC7996D90DFB987FC562DB971E7BF0 (void);
// 0x00000250 System.Void WeChatWASM.FstatSyncOption::.ctor()
extern void FstatSyncOption__ctor_m83E92CD6E10C6287A0BFEF29E678E44007E33FC6 (void);
// 0x00000251 System.Void WeChatWASM.WriteSyncOption::.ctor()
extern void WriteSyncOption__ctor_mF576F1ED456FC948C771BCB64AFB0454528754CB (void);
// 0x00000252 System.Void WeChatWASM.WriteSyncStringOption::.ctor()
extern void WriteSyncStringOption__ctor_m32691671BE73A7CA2F0D3FEACFA303ADFFFAA7FC (void);
// 0x00000253 System.Void WeChatWASM.OpenSyncOption::.ctor()
extern void OpenSyncOption__ctor_m5454D3C46BB5D2BAA3F99E9A38EEBA6EA4908BDB (void);
// 0x00000254 System.Void WeChatWASM.CloseSyncOption::.ctor()
extern void CloseSyncOption__ctor_m9E150A33950491A337065DA9D86C66A77604AEF0 (void);
// 0x00000255 System.Void WeChatWASM.FtruncateSyncOption::.ctor()
extern void FtruncateSyncOption__ctor_m5BCEFD3C48461FCE819AB129EF5B8E6B805C6CFA (void);
// 0x00000256 System.Void WeChatWASM.TruncateSyncOption::.ctor()
extern void TruncateSyncOption__ctor_m92265FF4D487E27D91164F53D8AD9C74A9F010BE (void);
// 0x00000257 System.Void WeChatWASM.ReadResult::.ctor()
extern void ReadResult__ctor_mAA18D82D81E728DF424054EC47280817A7517CC1 (void);
// 0x00000258 System.Void WeChatWASM.WriteResult::.ctor()
extern void WriteResult__ctor_m9113694DA2F74B5A42CE250AF5C757141B2CC3A0 (void);
// 0x00000259 System.Void WeChatWASM.PrivacyAuthorizeResolveOption::.ctor()
extern void PrivacyAuthorizeResolveOption__ctor_m710F625E439179DE443DFC56C8E1F675B8E815C1 (void);
// 0x0000025A System.Void WeChatWASM.WXJSCallback::.ctor()
extern void WXJSCallback__ctor_m20F3C51480B3DDFAD412C347981811F0FA6F5CF9 (void);
// 0x0000025B System.Void WeChatWASM.AccountInfo::.ctor()
extern void AccountInfo__ctor_m34DC405CA3DBE3C17337079746366DD5E1B56E93 (void);
// 0x0000025C System.Void WeChatWASM.MiniProgram::.ctor()
extern void MiniProgram__ctor_mF540A45D3EFAEBA6040780900238AEEF7480AE07 (void);
// 0x0000025D System.Void WeChatWASM.Plugin::.ctor()
extern void Plugin__ctor_mDF8FCE52CFF719259C5C465F3CCA104EA738A637 (void);
// 0x0000025E System.Void WeChatWASM.AppAuthorizeSetting::.ctor()
extern void AppAuthorizeSetting__ctor_m061052E980553D8A7934D79FB3F9942894CD9889 (void);
// 0x0000025F System.Void WeChatWASM.AppBaseInfo::.ctor()
extern void AppBaseInfo__ctor_mCBA3702C49C9898DF404F3F499A780AF67B7F8CD (void);
// 0x00000260 System.Void WeChatWASM.AppBaseInfoHost::.ctor()
extern void AppBaseInfoHost__ctor_m28EB3EF07E24B780A15654F1C6A442825CC0E338 (void);
// 0x00000261 System.Void WeChatWASM.GetBatteryInfoSyncResult::.ctor()
extern void GetBatteryInfoSyncResult__ctor_mA14E20B0F0A504E543D2EEF62D6D4B4342EFC68B (void);
// 0x00000262 System.Void WeChatWASM.DeviceInfo::.ctor()
extern void DeviceInfo__ctor_m06686DEEAFE1FCE8A36730885B1ABAFCA02F0999 (void);
// 0x00000263 System.Void WeChatWASM.EnterOptionsGame::.ctor()
extern void EnterOptionsGame__ctor_m8C57F0369A56AD99301341E231D01E0CB2812993 (void);
// 0x00000264 System.Void WeChatWASM.EnterOptionsGameReferrerInfo::.ctor()
extern void EnterOptionsGameReferrerInfo__ctor_mFE9DA137D44FB1D58FFA1703B013AA1B77A908DC (void);
// 0x00000265 System.Void WeChatWASM.GameLiveInfo::.ctor()
extern void GameLiveInfo__ctor_m1210B79D56B8D441761B7DC910A2ED7AC4384B15 (void);
// 0x00000266 System.Void WeChatWASM.LaunchOptionsGame::.ctor()
extern void LaunchOptionsGame__ctor_mFE38E642E97D234E6107F29424FAEAB4842A935F (void);
// 0x00000267 System.Void WeChatWASM.ClientRect::.ctor()
extern void ClientRect__ctor_m1B321C0B17498C9C70AC85D8F6E9F813C01C5C49 (void);
// 0x00000268 System.Void WeChatWASM.GetStorageInfoSyncOption::.ctor()
extern void GetStorageInfoSyncOption__ctor_mC3C84543E1D56FA57666EF2212F940E6CFCD8159 (void);
// 0x00000269 System.Void WeChatWASM.SystemInfo::.ctor()
extern void SystemInfo__ctor_mF7B157B9EFC8D4333D2C4A100BB6C6FFE6348175 (void);
// 0x0000026A System.Void WeChatWASM.SystemInfoHost::.ctor()
extern void SystemInfoHost__ctor_m4CD610F0013663642B3C6A4E55E3D5180372E4C2 (void);
// 0x0000026B System.Void WeChatWASM.SafeArea::.ctor()
extern void SafeArea__ctor_m7951E986A04D426DAF0F9EF066AC78D6F3630019 (void);
// 0x0000026C System.Void WeChatWASM.SystemSetting::.ctor()
extern void SystemSetting__ctor_mAC7B93F6473B16D15CAD6D0A48CB8949E1F54D84 (void);
// 0x0000026D System.Void WeChatWASM.WindowInfo::.ctor()
extern void WindowInfo__ctor_mE411EFE07F120F517BFFD24D2101AA06E39B9070 (void);
// 0x0000026E System.Void WeChatWASM.DownloadFileOption::.ctor()
extern void DownloadFileOption__ctor_m41A17E066255DC5362F9B37D1953E59DEC4ED2D7 (void);
// 0x0000026F System.Void WeChatWASM.GeneralCallbackResult::.ctor()
extern void GeneralCallbackResult__ctor_mEA2FBB48B4DFBDF2E9402C3F06350193C5E37BAC (void);
// 0x00000270 System.Void WeChatWASM.DownloadFileSuccessCallbackResult::.ctor()
extern void DownloadFileSuccessCallbackResult__ctor_m953F1CA5B30E3CBA7C35634B55968DE7B2ACA064 (void);
// 0x00000271 System.Void WeChatWASM.RequestProfile::.ctor()
extern void RequestProfile__ctor_m394F0D98DF740C130A4C9B840EDC37C3A246AA73 (void);
// 0x00000272 System.Void WeChatWASM.OnHeadersReceivedListenerResult::.ctor()
extern void OnHeadersReceivedListenerResult__ctor_m047868F7FD44B05D9D12BF34440A35EB5C97D94C (void);
// 0x00000273 System.Void WeChatWASM.DownloadTaskOnProgressUpdateListenerResult::.ctor()
extern void DownloadTaskOnProgressUpdateListenerResult__ctor_m66EFD5FC4AFC2608D96BF44DC89BBCB10F4B406B (void);
// 0x00000274 System.Void WeChatWASM.CreateOpenSettingButtonOption::.ctor()
extern void CreateOpenSettingButtonOption__ctor_m1822443315D3BB224494180FB50DC1120CB06571 (void);
// 0x00000275 System.Void WeChatWASM.OptionStyle::.ctor()
extern void OptionStyle__ctor_mD31F31A5E3AD002A5E4D4DFDE71DE2125D968291 (void);
// 0x00000276 System.Void WeChatWASM.ImageData::.ctor()
extern void ImageData__ctor_m9F1807750EA61FD07C7A9D9AC87B94ED4C693F7C (void);
// 0x00000277 System.Void WeChatWASM.GetLogManagerOption::.ctor()
extern void GetLogManagerOption__ctor_mBFF6AC05216511379C3635CA1BB3B25B7FF318DB (void);
// 0x00000278 System.Void WeChatWASM.Path2D::.ctor()
extern void Path2D__ctor_m9F98E00E8B2914CDC70F51629F336FC58E8B5507 (void);
// 0x00000279 System.Void WeChatWASM.OnCheckForUpdateListenerResult::.ctor()
extern void OnCheckForUpdateListenerResult__ctor_mE7482638765AB2FE99874043C5AEFF1C7308A0D6 (void);
// 0x0000027A System.Void WeChatWASM.VideoDecoderStartOption::.ctor()
extern void VideoDecoderStartOption__ctor_m19DCE8D573379B9EC83EDF857E694CAEF9CF882A (void);
// 0x0000027B System.Void WeChatWASM.SetMessageToFriendQueryOption::.ctor()
extern void SetMessageToFriendQueryOption__ctor_mD9AB373E3C8FD30CAB9C4FA912346A67ADDF7322 (void);
// 0x0000027C System.Void WeChatWASM.GetTextLineHeightOption::.ctor()
extern void GetTextLineHeightOption__ctor_mECFDB7E89E6485F65AD3BD8BF04AFA78D63EE621 (void);
// 0x0000027D System.Void WeChatWASM.AddCardOption::.ctor()
extern void AddCardOption__ctor_m5FBD70B6533E9EDF088A5C90EBCF430CD31401A0 (void);
// 0x0000027E System.Void WeChatWASM.AddCardRequestInfo::.ctor()
extern void AddCardRequestInfo__ctor_m046C6C6CD682D8683022A2203E85F18974981802 (void);
// 0x0000027F System.Void WeChatWASM.AddCardSuccessCallbackResult::.ctor()
extern void AddCardSuccessCallbackResult__ctor_mAE2CEB7F47ABCBF902F5D41929F95BB8DEFCA6ED (void);
// 0x00000280 System.Void WeChatWASM.AddCardResponseInfo::.ctor()
extern void AddCardResponseInfo__ctor_m7E39B466491B1FD92B82D698E7EEE2030EE22A92 (void);
// 0x00000281 System.Void WeChatWASM.AuthPrivateMessageOption::.ctor()
extern void AuthPrivateMessageOption__ctor_mFA75497BD43231F1ADE8485EF92ABCFAAFAD18F7 (void);
// 0x00000282 System.Void WeChatWASM.AuthPrivateMessageSuccessCallbackResult::.ctor()
extern void AuthPrivateMessageSuccessCallbackResult__ctor_m7D4EBA922E619ED3037F1A04DEB7B4E66514875D (void);
// 0x00000283 System.Void WeChatWASM.AuthorizeOption::.ctor()
extern void AuthorizeOption__ctor_m28713D260682257CB9F08640473A7F581569D133 (void);
// 0x00000284 System.Void WeChatWASM.CheckIsAddedToMyMiniProgramOption::.ctor()
extern void CheckIsAddedToMyMiniProgramOption__ctor_m3AE2D03349191AF2CD01D2A4BD2B009F88BEA8CB (void);
// 0x00000285 System.Void WeChatWASM.CheckIsAddedToMyMiniProgramSuccessCallbackResult::.ctor()
extern void CheckIsAddedToMyMiniProgramSuccessCallbackResult__ctor_m90C219205C39ECC464820A3B79E1E3990681A075 (void);
// 0x00000286 System.Void WeChatWASM.CheckSessionOption::.ctor()
extern void CheckSessionOption__ctor_m3C6D1232694258F5BF820DC46255F194A99ADB2A (void);
// 0x00000287 System.Void WeChatWASM.ChooseImageOption::.ctor()
extern void ChooseImageOption__ctor_m812502E3B8C834EFD2F601344D193E83962B5DCD (void);
// 0x00000288 System.Void WeChatWASM.ChooseImageSuccessCallbackResult::.ctor()
extern void ChooseImageSuccessCallbackResult__ctor_m45BD438079712DBC3FB9077F79018913B7A67DF0 (void);
// 0x00000289 System.Void WeChatWASM.ImageFile::.ctor()
extern void ImageFile__ctor_m77BA7A7B96044E2BCD08F9BED3CC2A7F48E5F933 (void);
// 0x0000028A System.Void WeChatWASM.ChooseMediaOption::.ctor()
extern void ChooseMediaOption__ctor_m8003265B7C5059AC194968C38CC2A8F6FE7A4565 (void);
// 0x0000028B System.Void WeChatWASM.ChooseMediaSuccessCallbackResult::.ctor()
extern void ChooseMediaSuccessCallbackResult__ctor_m9ACED599852BDC3D18DB07143ADB91A12BF959C5 (void);
// 0x0000028C System.Void WeChatWASM.MediaFile::.ctor()
extern void MediaFile__ctor_mA3CED9F7A1AC3FC07720AE56E93A09A3D39F5E1E (void);
// 0x0000028D System.Void WeChatWASM.ChooseMessageFileOption::.ctor()
extern void ChooseMessageFileOption__ctor_m3CCD8033D8727292B744DA6F885F32CBA09780F0 (void);
// 0x0000028E System.Void WeChatWASM.ChooseMessageFileSuccessCallbackResult::.ctor()
extern void ChooseMessageFileSuccessCallbackResult__ctor_mAC8830CAB6B4A63D18E77A2BA6BDE46D09FE84DC (void);
// 0x0000028F System.Void WeChatWASM.ChooseFile::.ctor()
extern void ChooseFile__ctor_m8A77A06F8743A4748720C3E02EBA1658476A34B4 (void);
// 0x00000290 System.Void WeChatWASM.CloseBLEConnectionOption::.ctor()
extern void CloseBLEConnectionOption__ctor_mEAC589A317414C8BF32B213D8A3F7EEFE704BAB1 (void);
// 0x00000291 System.Void WeChatWASM.BluetoothError::.ctor()
extern void BluetoothError__ctor_m04663751C344CA799993986FEF6DEEBDF47DE339 (void);
// 0x00000292 System.Void WeChatWASM.CloseBluetoothAdapterOption::.ctor()
extern void CloseBluetoothAdapterOption__ctor_m8EB536D414E3B423B93FE5D096262296FA41BDE7 (void);
// 0x00000293 System.Void WeChatWASM.CreateBLEConnectionOption::.ctor()
extern void CreateBLEConnectionOption__ctor_m08A468FF8EC395EC48A53EBA172ED620C3145713 (void);
// 0x00000294 System.Void WeChatWASM.CreateBLEPeripheralServerOption::.ctor()
extern void CreateBLEPeripheralServerOption__ctor_m155C39E4CA0718B09A587848E10C4B5F6B4B425B (void);
// 0x00000295 System.Void WeChatWASM.CreateBLEPeripheralServerSuccessCallbackResult::.ctor()
extern void CreateBLEPeripheralServerSuccessCallbackResult__ctor_m9B55D1083F65B19A218E72F010AE4F808CFD6A41 (void);
// 0x00000296 System.Void WeChatWASM.BLEPeripheralServer::addService(WeChatWASM.AddServiceOption)
extern void BLEPeripheralServer_addService_m241FE3B03985BBE7ED0A45C40E2865FB138C1D77 (void);
// 0x00000297 System.Void WeChatWASM.BLEPeripheralServer::offCharacteristicReadRequest(System.Action`1<WeChatWASM.OnCharacteristicReadRequestListenerResult>)
extern void BLEPeripheralServer_offCharacteristicReadRequest_m3FED090DC8BE5557367ABDDAC5AB85531E2FF759 (void);
// 0x00000298 System.Void WeChatWASM.BLEPeripheralServer::offCharacteristicSubscribed(System.Action`1<WeChatWASM.OnCharacteristicSubscribedListenerResult>)
extern void BLEPeripheralServer_offCharacteristicSubscribed_mD9425C4D8BD8C9C3EB8F0EF79052F3CEECFA841C (void);
// 0x00000299 System.Void WeChatWASM.BLEPeripheralServer::offCharacteristicUnsubscribed(System.Action`1<WeChatWASM.OnCharacteristicSubscribedListenerResult>)
extern void BLEPeripheralServer_offCharacteristicUnsubscribed_m56BA5DE7B287EC3BF13DD917B12BB46E5A773910 (void);
// 0x0000029A System.Void WeChatWASM.BLEPeripheralServer::offCharacteristicWriteRequest(System.Action`1<WeChatWASM.OnCharacteristicWriteRequestListenerResult>)
extern void BLEPeripheralServer_offCharacteristicWriteRequest_m72DFD242D1A5774AFD1E954657402D7AA73AFF06 (void);
// 0x0000029B System.Void WeChatWASM.BLEPeripheralServer::onCharacteristicReadRequest(System.Action`1<WeChatWASM.OnCharacteristicReadRequestListenerResult>)
extern void BLEPeripheralServer_onCharacteristicReadRequest_m37B1EAB26B22DCE1A2D0390DECA3AECC8F8BA6F2 (void);
// 0x0000029C System.Void WeChatWASM.BLEPeripheralServer::onCharacteristicSubscribed(System.Action`1<WeChatWASM.OnCharacteristicSubscribedListenerResult>)
extern void BLEPeripheralServer_onCharacteristicSubscribed_mE9B9D91B8163A29CFBCCA9FCA07907943D6F03BD (void);
// 0x0000029D System.Void WeChatWASM.BLEPeripheralServer::onCharacteristicUnsubscribed(System.Action`1<WeChatWASM.OnCharacteristicSubscribedListenerResult>)
extern void BLEPeripheralServer_onCharacteristicUnsubscribed_m717045A97D03EB093791E8103DD551119AF7A62B (void);
// 0x0000029E System.Void WeChatWASM.BLEPeripheralServer::onCharacteristicWriteRequest(System.Action`1<WeChatWASM.OnCharacteristicWriteRequestListenerResult>)
extern void BLEPeripheralServer_onCharacteristicWriteRequest_mE320C7EB915650BC0D66094F5477AA622F3FAD29 (void);
// 0x0000029F System.Void WeChatWASM.BLEPeripheralServer::removeService(WeChatWASM.RemoveServiceOption)
extern void BLEPeripheralServer_removeService_m9029DFDC9E18F8CC9EA19080983BC59D3A038DDC (void);
// 0x000002A0 System.Void WeChatWASM.BLEPeripheralServer::startAdvertising(WeChatWASM.StartAdvertisingObject)
extern void BLEPeripheralServer_startAdvertising_m2A2D9251537B41F919F25763441E4C6F9BA80FF8 (void);
// 0x000002A1 System.Void WeChatWASM.BLEPeripheralServer::stopAdvertising(WeChatWASM.StopAdvertisingOption)
extern void BLEPeripheralServer_stopAdvertising_m2C5A79E7EBF1B20041AD0DE407FF0E737A6F115A (void);
// 0x000002A2 System.Void WeChatWASM.BLEPeripheralServer::writeCharacteristicValue(WeChatWASM.WriteCharacteristicValueObject)
extern void BLEPeripheralServer_writeCharacteristicValue_m23F5E4BECC82777B1808F739948C6F88B0F9A2FB (void);
// 0x000002A3 System.Void WeChatWASM.BLEPeripheralServer::.ctor()
extern void BLEPeripheralServer__ctor_mCC0A528D7EE26BC418960F824B8104E65EB14E9D (void);
// 0x000002A4 System.Void WeChatWASM.AddServiceOption::.ctor()
extern void AddServiceOption__ctor_mF2330940768FD6A5E32B4DE5A8F5C966436AC110 (void);
// 0x000002A5 System.Void WeChatWASM.BLEPeripheralService::.ctor()
extern void BLEPeripheralService__ctor_mCDE78F1C99F8387430D2DB46CFB8F6DAF6E9BAD2 (void);
// 0x000002A6 System.Void WeChatWASM.Characteristic::.ctor()
extern void Characteristic__ctor_m0912A51487D22278DD5409F68311AB8C1EB4A3FD (void);
// 0x000002A7 System.Void WeChatWASM.Descriptor::.ctor()
extern void Descriptor__ctor_m457A324908735E037CB7CBBF443B30BB3725CAD2 (void);
// 0x000002A8 System.Void WeChatWASM.DescriptorPermission::.ctor()
extern void DescriptorPermission__ctor_m7F4C0A1B23C0C6EF222647137CCB49627FC134FB (void);
// 0x000002A9 System.Void WeChatWASM.CharacteristicPermission::.ctor()
extern void CharacteristicPermission__ctor_m8AE4ECA84A75CBBAEF09116B84EA14F69AFE9D46 (void);
// 0x000002AA System.Void WeChatWASM.CharacteristicProperties::.ctor()
extern void CharacteristicProperties__ctor_m30A5D3771B649487D2F818528128F4FF73175AF2 (void);
// 0x000002AB System.Void WeChatWASM.OnCharacteristicReadRequestListenerResult::.ctor()
extern void OnCharacteristicReadRequestListenerResult__ctor_m512C9BC5A3A0F9D28F93D1E4977079D2685D4130 (void);
// 0x000002AC System.Void WeChatWASM.OnCharacteristicSubscribedListenerResult::.ctor()
extern void OnCharacteristicSubscribedListenerResult__ctor_mAB9CAF3171E5EA169DBB67C5E6BFAB317F8F5611 (void);
// 0x000002AD System.Void WeChatWASM.OnCharacteristicWriteRequestListenerResult::.ctor()
extern void OnCharacteristicWriteRequestListenerResult__ctor_mB91B3C089743BC6FD14A14C0DC862AB7CC141860 (void);
// 0x000002AE System.Void WeChatWASM.RemoveServiceOption::.ctor()
extern void RemoveServiceOption__ctor_m58A01C682015C93B340BC78BFEB4156CB92314BF (void);
// 0x000002AF System.Void WeChatWASM.StartAdvertisingObject::.ctor()
extern void StartAdvertisingObject__ctor_m7C3C638296D56C063E894577AC839222F3B99B50 (void);
// 0x000002B0 System.Void WeChatWASM.AdvertiseReqObj::.ctor()
extern void AdvertiseReqObj__ctor_m962352AE39FA314C4E63F5919C02F2698A3785E0 (void);
// 0x000002B1 System.Void WeChatWASM.BeaconInfoObj::.ctor()
extern void BeaconInfoObj__ctor_m5F84583189BD303073832055C1261824B3D962E8 (void);
// 0x000002B2 System.Void WeChatWASM.ManufacturerData::.ctor()
extern void ManufacturerData__ctor_m120DF42E5BBB8F81D730A5E492E996573CBE8697 (void);
// 0x000002B3 System.Void WeChatWASM.StopAdvertisingOption::.ctor()
extern void StopAdvertisingOption__ctor_mAA3D98E6482FB3F85174E8CFB3836957003B0A27 (void);
// 0x000002B4 System.Void WeChatWASM.WriteCharacteristicValueObject::.ctor()
extern void WriteCharacteristicValueObject__ctor_mB1A60FF37FD9F73FFC0276A281242F2305EE13B1 (void);
// 0x000002B5 System.Void WeChatWASM.ExitMiniProgramOption::.ctor()
extern void ExitMiniProgramOption__ctor_m8416B198E0006CF255DA5785331A375BECF9A855 (void);
// 0x000002B6 System.Void WeChatWASM.ExitVoIPChatOption::.ctor()
extern void ExitVoIPChatOption__ctor_m9849EED70A9A7B92D7C8E7F2580F4BEB302BA62E (void);
// 0x000002B7 System.Void WeChatWASM.FaceDetectOption::.ctor()
extern void FaceDetectOption__ctor_m9DCB470937A411AE29B1B6DD65C93746CDC8750E (void);
// 0x000002B8 System.Void WeChatWASM.FaceDetectSuccessCallbackResult::.ctor()
extern void FaceDetectSuccessCallbackResult__ctor_mC8147FDD4FF4BED1F3AADB592E9A179B736DD56E (void);
// 0x000002B9 System.Void WeChatWASM.FaceAngel::.ctor()
extern void FaceAngel__ctor_m2D72AE7773D31B5900BAAAE19D37E44BCE0557B6 (void);
// 0x000002BA System.Void WeChatWASM.FaceConf::.ctor()
extern void FaceConf__ctor_m91C6720DF2C3914CE20BD8744D6DBE604A2F577E (void);
// 0x000002BB System.Void WeChatWASM.GetAvailableAudioSourcesOption::.ctor()
extern void GetAvailableAudioSourcesOption__ctor_m2BF98C56312BAD1BF2C66412C59D67017BE9B0DD (void);
// 0x000002BC System.Void WeChatWASM.GetAvailableAudioSourcesSuccessCallbackResult::.ctor()
extern void GetAvailableAudioSourcesSuccessCallbackResult__ctor_m8F3D79140CA1842964F848DAA05FAD043639840E (void);
// 0x000002BD System.Void WeChatWASM.GetBLEDeviceCharacteristicsOption::.ctor()
extern void GetBLEDeviceCharacteristicsOption__ctor_m4DADAF79B4EA68AEB4BFC20D54DDF986BED5AEDC (void);
// 0x000002BE System.Void WeChatWASM.GetBLEDeviceCharacteristicsSuccessCallbackResult::.ctor()
extern void GetBLEDeviceCharacteristicsSuccessCallbackResult__ctor_m4BD2279518CE6EBC411688139C5F45A01F6F11DF (void);
// 0x000002BF System.Void WeChatWASM.BLECharacteristic::.ctor()
extern void BLECharacteristic__ctor_m5E118089BC649E10E8298B519DAB00328B201ACC (void);
// 0x000002C0 System.Void WeChatWASM.BLECharacteristicProperties::.ctor()
extern void BLECharacteristicProperties__ctor_m6444D972D8D9B738E92B9A8B3EEE433A802F8113 (void);
// 0x000002C1 System.Void WeChatWASM.GetBLEDeviceRSSIOption::.ctor()
extern void GetBLEDeviceRSSIOption__ctor_mA0A2FF67922EBF577483B869E21B3134C039476B (void);
// 0x000002C2 System.Void WeChatWASM.GetBLEDeviceRSSISuccessCallbackResult::.ctor()
extern void GetBLEDeviceRSSISuccessCallbackResult__ctor_m22248FA095865F6C8B758B546F2BADE5F67EB597 (void);
// 0x000002C3 System.Void WeChatWASM.GetBLEDeviceServicesOption::.ctor()
extern void GetBLEDeviceServicesOption__ctor_mD060DA095FD48320742FE37F1E03599B5C4BEC3E (void);
// 0x000002C4 System.Void WeChatWASM.GetBLEDeviceServicesSuccessCallbackResult::.ctor()
extern void GetBLEDeviceServicesSuccessCallbackResult__ctor_m7FBECB5DAA23C6779352AAC41DDA546D64E0E13D (void);
// 0x000002C5 System.Void WeChatWASM.BLEService::.ctor()
extern void BLEService__ctor_mD2266968BAD422E31DE0AA8FDED96C1E3E23B020 (void);
// 0x000002C6 System.Void WeChatWASM.GetBLEMTUOption::.ctor()
extern void GetBLEMTUOption__ctor_m0DF6D5E5DD70B8B0BAC00C523F06D00372B23685 (void);
// 0x000002C7 System.Void WeChatWASM.GetBLEMTUSuccessCallbackResult::.ctor()
extern void GetBLEMTUSuccessCallbackResult__ctor_mA56BB447E34DDF74DB3F34F37CCEBDEB63D6F3E9 (void);
// 0x000002C8 System.Void WeChatWASM.GetBatteryInfoOption::.ctor()
extern void GetBatteryInfoOption__ctor_m15ABDE488B7DDE225B85BB3AB428B56A9082A2A0 (void);
// 0x000002C9 System.Void WeChatWASM.GetBatteryInfoSuccessCallbackResult::.ctor()
extern void GetBatteryInfoSuccessCallbackResult__ctor_mAA0B262311BCF3E6CC57058162DB78D606460E7D (void);
// 0x000002CA System.Void WeChatWASM.GetBeaconsOption::.ctor()
extern void GetBeaconsOption__ctor_mEF283F6E2A6D379348200D7D8237C812F5415D6D (void);
// 0x000002CB System.Void WeChatWASM.BeaconError::.ctor()
extern void BeaconError__ctor_mE7ABF680B37AD6BDEC59303092A8AA60F2BDE350 (void);
// 0x000002CC System.Void WeChatWASM.GetBeaconsSuccessCallbackResult::.ctor()
extern void GetBeaconsSuccessCallbackResult__ctor_mA1FC7E34F3BA206D5962C68375F6A9F33CDEB34D (void);
// 0x000002CD System.Void WeChatWASM.BeaconInfo::.ctor()
extern void BeaconInfo__ctor_m8425413CE17B50A27DA8DC65B8F8171230107B9C (void);
// 0x000002CE System.Void WeChatWASM.GetBluetoothAdapterStateOption::.ctor()
extern void GetBluetoothAdapterStateOption__ctor_m32ADA1C463F210332A42DFFBEEEA23E8A3633AF5 (void);
// 0x000002CF System.Void WeChatWASM.GetBluetoothAdapterStateSuccessCallbackResult::.ctor()
extern void GetBluetoothAdapterStateSuccessCallbackResult__ctor_m8CDF4DFBA7EC73C3F718FC8346491BCA55E2F36E (void);
// 0x000002D0 System.Void WeChatWASM.GetBluetoothDevicesOption::.ctor()
extern void GetBluetoothDevicesOption__ctor_m559E2AA8AD6F5B430402B77241E0BE1034B0920B (void);
// 0x000002D1 System.Void WeChatWASM.GetBluetoothDevicesSuccessCallbackResult::.ctor()
extern void GetBluetoothDevicesSuccessCallbackResult__ctor_m3A35FF42553C742204B8DBFDEAED88D0CAB72888 (void);
// 0x000002D2 System.Void WeChatWASM.BlueToothDevice::.ctor()
extern void BlueToothDevice__ctor_m1BEF3347022B8302B6B67296EA065010E156112A (void);
// 0x000002D3 System.Void WeChatWASM.GetChannelsLiveInfoOption::.ctor()
extern void GetChannelsLiveInfoOption__ctor_m46F5B4A6A725A0B1C5ABDF4B7283CB3CA0572813 (void);
// 0x000002D4 System.Void WeChatWASM.GetChannelsLiveInfoSuccessCallbackResult::.ctor()
extern void GetChannelsLiveInfoSuccessCallbackResult__ctor_m369F128F3094EB63D36B382FE4CAEB37731AFDF0 (void);
// 0x000002D5 System.Void WeChatWASM.GetChannelsLiveNoticeInfoOption::.ctor()
extern void GetChannelsLiveNoticeInfoOption__ctor_m7212B338980BC8D1011C3E84E6BEDAA0C97A188E (void);
// 0x000002D6 System.Void WeChatWASM.GetChannelsLiveNoticeInfoSuccessCallbackResult::.ctor()
extern void GetChannelsLiveNoticeInfoSuccessCallbackResult__ctor_m85D1CAF30719E5EED13E28DB2F661C3E19D719C6 (void);
// 0x000002D7 System.Void WeChatWASM.GetClipboardDataOption::.ctor()
extern void GetClipboardDataOption__ctor_m81847558B433460628FC7242DD3CBFED729E9DC0 (void);
// 0x000002D8 System.Void WeChatWASM.GetClipboardDataSuccessCallbackOption::.ctor()
extern void GetClipboardDataSuccessCallbackOption__ctor_m8286E4B09D2BDFEDFD149CA98399E916336F72C9 (void);
// 0x000002D9 System.Void WeChatWASM.GetConnectedBluetoothDevicesOption::.ctor()
extern void GetConnectedBluetoothDevicesOption__ctor_m83F7F03CDCC437590EFF8AD8640796555A8D6E59 (void);
// 0x000002DA System.Void WeChatWASM.GetConnectedBluetoothDevicesSuccessCallbackResult::.ctor()
extern void GetConnectedBluetoothDevicesSuccessCallbackResult__ctor_m8B6D41587C535C8596ECD09EC2DA28FEFE687DBB (void);
// 0x000002DB System.Void WeChatWASM.BluetoothDeviceInfo::.ctor()
extern void BluetoothDeviceInfo__ctor_m6D36E27246FE12E49F7043E5ECCCC207B97C1176 (void);
// 0x000002DC System.Void WeChatWASM.GetExtConfigOption::.ctor()
extern void GetExtConfigOption__ctor_m8D4B75921AAC27D5AB5C93EFFFD0E9D49104F8DB (void);
// 0x000002DD System.Void WeChatWASM.GetExtConfigSuccessCallbackResult::.ctor()
extern void GetExtConfigSuccessCallbackResult__ctor_mF0CCCB65416EBE55EBC55CF89A5419FDF8877683 (void);
// 0x000002DE System.Void WeChatWASM.GetGameClubDataOption::.ctor()
extern void GetGameClubDataOption__ctor_m42373D4CC59E6023804FE0A2BB98DD6DAC93B6D1 (void);
// 0x000002DF System.Void WeChatWASM.DataType::.ctor()
extern void DataType__ctor_m18AE959465EAFA6BA6DBC6EF7BBAA1E59F36DCF6 (void);
// 0x000002E0 System.Void WeChatWASM.GetGameClubDataSuccessCallbackResult::.ctor()
extern void GetGameClubDataSuccessCallbackResult__ctor_mAABFD436C9AC801EAD68AF4610500C0216CF12AE (void);
// 0x000002E1 System.Void WeChatWASM.GetGroupEnterInfoOption::.ctor()
extern void GetGroupEnterInfoOption__ctor_mBC2C990963C5E30EBAB773D87CC5F9A3B9BB4B6E (void);
// 0x000002E2 System.Void WeChatWASM.GetGroupEnterInfoSuccessCallbackResult::.ctor()
extern void GetGroupEnterInfoSuccessCallbackResult__ctor_mBE6C8D556916661C7703FF6C116B271EAADF43B0 (void);
// 0x000002E3 System.Void WeChatWASM.GetInferenceEnvInfoOption::.ctor()
extern void GetInferenceEnvInfoOption__ctor_m07D74505BEEBDCE34F1B069E51B23A77F45D0F64 (void);
// 0x000002E4 System.Void WeChatWASM.GetInferenceEnvInfoSuccessCallbackResult::.ctor()
extern void GetInferenceEnvInfoSuccessCallbackResult__ctor_m025D18CF255D7A00B4CD094B6A97CAA86228C3DE (void);
// 0x000002E5 System.Void WeChatWASM.GetLocalIPAddressOption::.ctor()
extern void GetLocalIPAddressOption__ctor_m8D4D7468684F8D9F8396EF80C009CF021A8CCCFA (void);
// 0x000002E6 System.Void WeChatWASM.GetLocalIPAddressSuccessCallbackResult::.ctor()
extern void GetLocalIPAddressSuccessCallbackResult__ctor_m38A451C761B61E931C549D5A4689013EDBDF26BD (void);
// 0x000002E7 System.Void WeChatWASM.GetLocationOption::.ctor()
extern void GetLocationOption__ctor_mBBDD92ADF5A080941E15F6B7D38B5656DEA6C260 (void);
// 0x000002E8 System.Void WeChatWASM.GetLocationSuccessCallbackResult::.ctor()
extern void GetLocationSuccessCallbackResult__ctor_m45DF4117CB5B3F0332874E37AB9DA39C497C53E5 (void);
// 0x000002E9 System.Void WeChatWASM.GetNetworkTypeOption::.ctor()
extern void GetNetworkTypeOption__ctor_mC8BDC4E6707EECB95744DC555A64BD0C0F2352BB (void);
// 0x000002EA System.Void WeChatWASM.GetNetworkTypeSuccessCallbackResult::.ctor()
extern void GetNetworkTypeSuccessCallbackResult__ctor_m6041480CE5A15EC7CEBC009C8154F111388D8412 (void);
// 0x000002EB System.Void WeChatWASM.GetPrivacySettingOption::.ctor()
extern void GetPrivacySettingOption__ctor_m62BA9DCD2A4662ED7B806791CE3C7D7573CC37E3 (void);
// 0x000002EC System.Void WeChatWASM.GetPrivacySettingSuccessCallbackResult::.ctor()
extern void GetPrivacySettingSuccessCallbackResult__ctor_m51D468D27E2009AEB06088C8CEE605EB78D0F772 (void);
// 0x000002ED System.Void WeChatWASM.GetScreenBrightnessOption::.ctor()
extern void GetScreenBrightnessOption__ctor_m3A96300ECF78BA24787BC6F670B7754827BA660D (void);
// 0x000002EE System.Void WeChatWASM.GetScreenBrightnessSuccessCallbackOption::.ctor()
extern void GetScreenBrightnessSuccessCallbackOption__ctor_m16A0F29281CE34988DBCF872FE54F297239F860C (void);
// 0x000002EF System.Void WeChatWASM.GetSettingOption::.ctor()
extern void GetSettingOption__ctor_mC37D2E28ADECAAE90F1F841EB678D2650FCB414F (void);
// 0x000002F0 System.Void WeChatWASM.GetSettingSuccessCallbackResult::.ctor()
extern void GetSettingSuccessCallbackResult__ctor_mEB31987CAF73BC1E61AC05E1342DC3704F1785C6 (void);
// 0x000002F1 System.Void WeChatWASM.AuthSetting::.ctor()
extern void AuthSetting__ctor_m78098C3670AC6F2BA30023A54C99C2F2974FC753 (void);
// 0x000002F2 System.Void WeChatWASM.SubscriptionsSetting::.ctor()
extern void SubscriptionsSetting__ctor_m8A473707EA25EE2358E38F3A79EA460A26365F62 (void);
// 0x000002F3 System.Void WeChatWASM.GetShareInfoOption::.ctor()
extern void GetShareInfoOption__ctor_m4F1648804AEB3C79D2A4978DC06F09C3DE6DD2A1 (void);
// 0x000002F4 System.Void WeChatWASM.GetStorageInfoOption::.ctor()
extern void GetStorageInfoOption__ctor_mCF136D54733F9D93532D01E18D5A411427D29F7E (void);
// 0x000002F5 System.Void WeChatWASM.GetStorageInfoSuccessCallbackOption::.ctor()
extern void GetStorageInfoSuccessCallbackOption__ctor_m70A64CB40FAA88E5216CB1592ED2D26D7857CE2F (void);
// 0x000002F6 System.Void WeChatWASM.GetSystemInfoOption::.ctor()
extern void GetSystemInfoOption__ctor_mBE651FE489E9CDF55099CB583FFA843D18E3459E (void);
// 0x000002F7 System.Void WeChatWASM.GetSystemInfoAsyncOption::.ctor()
extern void GetSystemInfoAsyncOption__ctor_mC9DA6B5728B3AD5E09F9E633A4F4F15DF37CBBC3 (void);
// 0x000002F8 System.Void WeChatWASM.GetUserInfoOption::.ctor()
extern void GetUserInfoOption__ctor_m6609286616997FEC18A26F4DC1B03ABEBB12C1E5 (void);
// 0x000002F9 System.Void WeChatWASM.GetUserInfoSuccessCallbackResult::.ctor()
extern void GetUserInfoSuccessCallbackResult__ctor_mE2D15A0959801EDE74D0806AC714A03B52B68A76 (void);
// 0x000002FA System.Void WeChatWASM.UserInfo::.ctor()
extern void UserInfo__ctor_m7DE1BEF0EAA4491CCCD869FC71284ABB13E1A5A0 (void);
// 0x000002FB System.Void WeChatWASM.GetUserInteractiveStorageOption::.ctor()
extern void GetUserInteractiveStorageOption__ctor_mD9C0DF59280D39B5D1B5407BFD1675011C69C6EF (void);
// 0x000002FC System.Void WeChatWASM.GetUserInteractiveStorageFailCallbackResult::.ctor()
extern void GetUserInteractiveStorageFailCallbackResult__ctor_mC961AB0174533EEC368E7889D7843BD96CBF5C5A (void);
// 0x000002FD System.Void WeChatWASM.GetUserInteractiveStorageSuccessCallbackResult::.ctor()
extern void GetUserInteractiveStorageSuccessCallbackResult__ctor_mDBB5E26418C7FC461AF674006C283D4922C4EF4F (void);
// 0x000002FE System.Void WeChatWASM.GetWeRunDataOption::.ctor()
extern void GetWeRunDataOption__ctor_m6F80FC3795B741804E509E47DB20032ECAB5EBB3 (void);
// 0x000002FF System.Void WeChatWASM.GetWeRunDataSuccessCallbackResult::.ctor()
extern void GetWeRunDataSuccessCallbackResult__ctor_mFBB8ECA6FAADC239400913F0621491B11D5B17A4 (void);
// 0x00000300 System.Void WeChatWASM.HideKeyboardOption::.ctor()
extern void HideKeyboardOption__ctor_m0EFBABA954A073F6E6922CAE41B962CE25BF89E2 (void);
// 0x00000301 System.Void WeChatWASM.HideLoadingOption::.ctor()
extern void HideLoadingOption__ctor_mE75B52342E332DFBF35FCB691769ACADE5DD83ED (void);
// 0x00000302 System.Void WeChatWASM.HideShareMenuOption::.ctor()
extern void HideShareMenuOption__ctor_m16B9DFB5ACE06AE5B8EBE96346B94E1218624705 (void);
// 0x00000303 System.Void WeChatWASM.HideToastOption::.ctor()
extern void HideToastOption__ctor_mB0BEDAE224B2658A52656F62E33BBA0D6705C29F (void);
// 0x00000304 System.Void WeChatWASM.InitFaceDetectOption::.ctor()
extern void InitFaceDetectOption__ctor_m56A21FBEFA6803D318899DCA49A6A4967F557401 (void);
// 0x00000305 System.Void WeChatWASM.IsBluetoothDevicePairedOption::.ctor()
extern void IsBluetoothDevicePairedOption__ctor_m05A065D4A05BD3091D099A8A6D96F2D20679126C (void);
// 0x00000306 System.Void WeChatWASM.JoinVoIPChatOption::.ctor()
extern void JoinVoIPChatOption__ctor_m3351CBFC23D636DC1872CF6FDAB9947DD4E908D3 (void);
// 0x00000307 System.Void WeChatWASM.JoinVoIPChatError::.ctor()
extern void JoinVoIPChatError__ctor_m57F20CCE55BFABAEDC75020A6FA1BE8E3947031E (void);
// 0x00000308 System.Void WeChatWASM.MuteConfig::.ctor()
extern void MuteConfig__ctor_mC867E103033E007616F8774FBA8CF820D28C7ABF (void);
// 0x00000309 System.Void WeChatWASM.JoinVoIPChatSuccessCallbackResult::.ctor()
extern void JoinVoIPChatSuccessCallbackResult__ctor_mEB95604BFF9E183C35099CAE776C9961EA528728 (void);
// 0x0000030A System.Void WeChatWASM.LoginOption::.ctor()
extern void LoginOption__ctor_m93BDACFA012FBE764615C6CDA1878970834E250B (void);
// 0x0000030B System.Void WeChatWASM.RequestFailCallbackErr::.ctor()
extern void RequestFailCallbackErr__ctor_mCE29E0282B933F1CCBCC60F8563EA5C8E80DD26C (void);
// 0x0000030C System.Void WeChatWASM.LoginSuccessCallbackResult::.ctor()
extern void LoginSuccessCallbackResult__ctor_m4FD99CC272E1182F9292AB46E95A08F29318C664 (void);
// 0x0000030D System.Void WeChatWASM.MakeBluetoothPairOption::.ctor()
extern void MakeBluetoothPairOption__ctor_m27525F85F7047ED168115EB056DC6E49B4E4DABA (void);
// 0x0000030E System.Void WeChatWASM.NavigateToMiniProgramOption::.ctor()
extern void NavigateToMiniProgramOption__ctor_mFF6E5EC639E1CC28623351A9FF3F3ED8BD9C483B (void);
// 0x0000030F System.Void WeChatWASM.NotifyBLECharacteristicValueChangeOption::.ctor()
extern void NotifyBLECharacteristicValueChangeOption__ctor_mAF1C3B7D36FEB0266BA74A9698E6E45E4B71746E (void);
// 0x00000310 System.Void WeChatWASM.OnAccelerometerChangeListenerResult::.ctor()
extern void OnAccelerometerChangeListenerResult__ctor_m1EC942DC32B2D529713A6A8049BB1A387F349854 (void);
// 0x00000311 System.Void WeChatWASM.OnAddToFavoritesListenerResult::.ctor()
extern void OnAddToFavoritesListenerResult__ctor_m36CAE86E7ACE0C1FAA99BE65D594347769074549 (void);
// 0x00000312 System.Void WeChatWASM.OnBLECharacteristicValueChangeListenerResult::.ctor()
extern void OnBLECharacteristicValueChangeListenerResult__ctor_mD816C40BE69327EA08198C6B9609525039D6509D (void);
// 0x00000313 System.Void WeChatWASM.OnBLEConnectionStateChangeListenerResult::.ctor()
extern void OnBLEConnectionStateChangeListenerResult__ctor_m7B1B6EB98B5098602617C71F7DF82A88FC757A04 (void);
// 0x00000314 System.Void WeChatWASM.OnBLEMTUChangeListenerResult::.ctor()
extern void OnBLEMTUChangeListenerResult__ctor_mD5A71F4C73EF09FC5F86554790967252FE3A0B10 (void);
// 0x00000315 System.Void WeChatWASM.OnBLEPeripheralConnectionStateChangedListenerResult::.ctor()
extern void OnBLEPeripheralConnectionStateChangedListenerResult__ctor_mF97D0DD5B0ADF6F943F6C72AED8066546F54F7B2 (void);
// 0x00000316 System.Void WeChatWASM.OnBeaconServiceChangeListenerResult::.ctor()
extern void OnBeaconServiceChangeListenerResult__ctor_m9BCDD0A2321C20130B7974C7ED8345AEB4A3EDF9 (void);
// 0x00000317 System.Void WeChatWASM.OnBeaconUpdateListenerResult::.ctor()
extern void OnBeaconUpdateListenerResult__ctor_m05ED8CA8B07E1DE46CB515D5344CF1267823AADA (void);
// 0x00000318 System.Void WeChatWASM.OnBluetoothAdapterStateChangeListenerResult::.ctor()
extern void OnBluetoothAdapterStateChangeListenerResult__ctor_mDB4BDE68CF8254B8CDB2FCBF53678B32B12603C0 (void);
// 0x00000319 System.Void WeChatWASM.OnBluetoothDeviceFoundListenerResult::.ctor()
extern void OnBluetoothDeviceFoundListenerResult__ctor_mD06CF807A539AE4C7015040F41D159086E10B689 (void);
// 0x0000031A System.Void WeChatWASM.OnCompassChangeListenerResult::.ctor()
extern void OnCompassChangeListenerResult__ctor_m96DF04E8B5140D96063348B44309DD923BA1C870 (void);
// 0x0000031B System.Void WeChatWASM.OnCopyUrlListenerResult::.ctor()
extern void OnCopyUrlListenerResult__ctor_m2C76C0D6B2548CEDCDF3A70318282E6DCDC04B33 (void);
// 0x0000031C System.Void WeChatWASM.OnDeviceMotionChangeListenerResult::.ctor()
extern void OnDeviceMotionChangeListenerResult__ctor_m23B6C7BD79E58593D8629D6971D77604F20A6082 (void);
// 0x0000031D System.Void WeChatWASM.OnDeviceOrientationChangeListenerResult::.ctor()
extern void OnDeviceOrientationChangeListenerResult__ctor_m17BC3F01E9CB80F673AEEEE5BCACBDD0361CB04B (void);
// 0x0000031E System.Void WeChatWASM.WxOnErrorCallbackResult::.ctor()
extern void WxOnErrorCallbackResult__ctor_m80A1177FEDF716E2E8039A4139288219BF433095 (void);
// 0x0000031F System.Void WeChatWASM.OnGyroscopeChangeListenerResult::.ctor()
extern void OnGyroscopeChangeListenerResult__ctor_m4F1BE6EF71091F1DFD3F57AE62BADE392791BBAC (void);
// 0x00000320 System.Void WeChatWASM.OnHandoffListenerResult::.ctor()
extern void OnHandoffListenerResult__ctor_mC541EEFBBA49AA9E01573E501983BF0F0E8E5CF5 (void);
// 0x00000321 System.Void WeChatWASM.OnKeyDownListenerResult::.ctor()
extern void OnKeyDownListenerResult__ctor_mD45F0A390FF3DB2861064522EB7A8ED45D4C8D3D (void);
// 0x00000322 System.Void WeChatWASM.OnKeyboardInputListenerResult::.ctor()
extern void OnKeyboardInputListenerResult__ctor_m197464C7BA8B8C9824F5BBE7A29ABCE88AE6AD55 (void);
// 0x00000323 System.Void WeChatWASM.OnKeyboardHeightChangeListenerResult::.ctor()
extern void OnKeyboardHeightChangeListenerResult__ctor_m9E805308686DBC1CF5610C84080E33A8805DDFD4 (void);
// 0x00000324 System.Void WeChatWASM.OnMemoryWarningListenerResult::.ctor()
extern void OnMemoryWarningListenerResult__ctor_m8169100BD871B3C23F16A2E98A7AAEEB802DAEFE (void);
// 0x00000325 System.Void WeChatWASM.OnMouseDownListenerResult::.ctor()
extern void OnMouseDownListenerResult__ctor_m171D12C1D92B160618CE214650A4426748880AD7 (void);
// 0x00000326 System.Void WeChatWASM.OnMouseMoveListenerResult::.ctor()
extern void OnMouseMoveListenerResult__ctor_m3ED63B6D76FBDF9A16473F391275FB47F26F8BD6 (void);
// 0x00000327 System.Void WeChatWASM.OnNetworkStatusChangeListenerResult::.ctor()
extern void OnNetworkStatusChangeListenerResult__ctor_mC68441949E13C661D6B5711F5CD8D5C2D484F163 (void);
// 0x00000328 System.Void WeChatWASM.OnNetworkWeakChangeListenerResult::.ctor()
extern void OnNetworkWeakChangeListenerResult__ctor_m9120AD1F69EBFEA77BEF2B162F9DEBB3C0F350DA (void);
// 0x00000329 System.Void WeChatWASM.OnShareMessageToFriendListenerResult::.ctor()
extern void OnShareMessageToFriendListenerResult__ctor_m1EFD1C67168A5B2C3A6E44CD5E577E408EA7EBD5 (void);
// 0x0000032A System.Void WeChatWASM.OnShareTimelineListenerResult::.ctor()
extern void OnShareTimelineListenerResult__ctor_m46A20AFAE151261879B58BCCC0FF92638A35D820 (void);
// 0x0000032B System.Void WeChatWASM.OnShowListenerResult::.ctor()
extern void OnShowListenerResult__ctor_m0E84FEA9AD1B58545E37E214A669DA70FE63BC8E (void);
// 0x0000032C System.Void WeChatWASM.ResultReferrerInfo::.ctor()
extern void ResultReferrerInfo__ctor_m1AAB5D628A029F8E6EB71B9444DA88F54ED7431F (void);
// 0x0000032D System.Void WeChatWASM.OnTouchStartListenerResult::.ctor()
extern void OnTouchStartListenerResult__ctor_m8101E5F2EC2E8620266F564EB873BBA1A67C00FA (void);
// 0x0000032E System.Void WeChatWASM.Touch::.ctor()
extern void Touch__ctor_m04431B32D0209BB4210477F1B3ECF59B1582FFE2 (void);
// 0x0000032F System.Void WeChatWASM.OnUnhandledRejectionListenerResult::.ctor()
extern void OnUnhandledRejectionListenerResult__ctor_m7B2D6181B16CDAA64723F20589EBDD2C3D1B9A2F (void);
// 0x00000330 System.Void WeChatWASM.OnVoIPChatInterruptedListenerResult::.ctor()
extern void OnVoIPChatInterruptedListenerResult__ctor_mDF8B833C082038557003CE3A6E466F82C9453E35 (void);
// 0x00000331 System.Void WeChatWASM.OnVoIPChatMembersChangedListenerResult::.ctor()
extern void OnVoIPChatMembersChangedListenerResult__ctor_mB8079D880B85A38081145FE502291627C8E39DCF (void);
// 0x00000332 System.Void WeChatWASM.OnVoIPChatSpeakersChangedListenerResult::.ctor()
extern void OnVoIPChatSpeakersChangedListenerResult__ctor_m4CE382563073A87B45667B3D358CCECEE71643DB (void);
// 0x00000333 System.Void WeChatWASM.OnVoIPChatStateChangedListenerResult::.ctor()
extern void OnVoIPChatStateChangedListenerResult__ctor_m29904DE02343A0C125B231A0962ECABDEA197DAE (void);
// 0x00000334 System.Void WeChatWASM.OnWheelListenerResult::.ctor()
extern void OnWheelListenerResult__ctor_mBE43CC1B60A2C20F26439863B9F57CAAE7340908 (void);
// 0x00000335 System.Void WeChatWASM.OnWindowResizeListenerResult::.ctor()
extern void OnWindowResizeListenerResult__ctor_mD5D2E058D16875C7E51EAE8EEFF618151D928BF9 (void);
// 0x00000336 System.Void WeChatWASM.OpenAppAuthorizeSettingOption::.ctor()
extern void OpenAppAuthorizeSettingOption__ctor_mE2F9BDF9A69E1CD75135AD79EA27FA477F24BC3E (void);
// 0x00000337 System.Void WeChatWASM.OpenBluetoothAdapterOption::.ctor()
extern void OpenBluetoothAdapterOption__ctor_m47E8949C94AC49FCB91743E8A126FD64670F2F59 (void);
// 0x00000338 System.Void WeChatWASM.OpenCardOption::.ctor()
extern void OpenCardOption__ctor_mC212F2189CC22E4CA53BDFB8827F536F9C928E32 (void);
// 0x00000339 System.Void WeChatWASM.OpenCardRequestInfo::.ctor()
extern void OpenCardRequestInfo__ctor_mFF9AB8B60ED86F8CD86C7895ADB6964EA2070580 (void);
// 0x0000033A System.Void WeChatWASM.OpenChannelsActivityOption::.ctor()
extern void OpenChannelsActivityOption__ctor_m8D0A52ED850B966532A43D562C80435C85D328B6 (void);
// 0x0000033B System.Void WeChatWASM.OpenChannelsEventOption::.ctor()
extern void OpenChannelsEventOption__ctor_mEE35B5B6CF4A98152870C911A54568F29B29C759 (void);
// 0x0000033C System.Void WeChatWASM.OpenChannelsLiveOption::.ctor()
extern void OpenChannelsLiveOption__ctor_m9C6B1A43885F38DFFDFD69AF10BD79D85E506DBD (void);
// 0x0000033D System.Void WeChatWASM.OpenChannelsUserProfileOption::.ctor()
extern void OpenChannelsUserProfileOption__ctor_m6AB50402B10E06F38C25F3AB73181586D5390569 (void);
// 0x0000033E System.Void WeChatWASM.OpenCustomerServiceChatOption::.ctor()
extern void OpenCustomerServiceChatOption__ctor_m81408C93D8AF4AF4183152B7DE92FE05BE024974 (void);
// 0x0000033F System.Void WeChatWASM.ExtInfoOption::.ctor()
extern void ExtInfoOption__ctor_mE4A9A404A3D092FE4489234DD15762825A006596 (void);
// 0x00000340 System.Void WeChatWASM.OpenCustomerServiceConversationOption::.ctor()
extern void OpenCustomerServiceConversationOption__ctor_m4B7E65F4D832EC4D796E8CD9088F0D7BCE3FB58A (void);
// 0x00000341 System.Void WeChatWASM.OpenCustomerServiceConversationSuccessCallbackResult::.ctor()
extern void OpenCustomerServiceConversationSuccessCallbackResult__ctor_mB72212CF4775782A83969E8B4CD29DD37C8B89CA (void);
// 0x00000342 System.Void WeChatWASM.OpenPrivacyContractOption::.ctor()
extern void OpenPrivacyContractOption__ctor_m217DB5AB0AEFBDD625CFEFE0979C20CE59A6453A (void);
// 0x00000343 System.Void WeChatWASM.OpenSettingOption::.ctor()
extern void OpenSettingOption__ctor_mA56FD734D8E199086E7E0E410FE4FD42231F95C2 (void);
// 0x00000344 System.Void WeChatWASM.OpenSettingSuccessCallbackResult::.ctor()
extern void OpenSettingSuccessCallbackResult__ctor_m86B6BB1BF27D215372560C58C144F29D457BC1F9 (void);
// 0x00000345 System.Void WeChatWASM.OpenSystemBluetoothSettingOption::.ctor()
extern void OpenSystemBluetoothSettingOption__ctor_m377E1659653C59094F1EC97BF349659F0D6DF717 (void);
// 0x00000346 System.Void WeChatWASM.OperateGameRecorderVideoOption::.ctor()
extern void OperateGameRecorderVideoOption__ctor_m368827A798152668F873371C078E5FDCB0C49E07 (void);
// 0x00000347 System.Void WeChatWASM.PreviewImageOption::.ctor()
extern void PreviewImageOption__ctor_m332324B488FD023C1CE820BFFFE6908642BC41B9 (void);
// 0x00000348 System.Void WeChatWASM.PreviewMediaOption::.ctor()
extern void PreviewMediaOption__ctor_m9DE60A1CDE2BAE446C065BA9CB106A111467C8DA (void);
// 0x00000349 System.Void WeChatWASM.MediaSource::.ctor()
extern void MediaSource__ctor_mDE56F23EDA5D484B00AD7E322FE96729D3294580 (void);
// 0x0000034A System.Void WeChatWASM.ReadBLECharacteristicValueOption::.ctor()
extern void ReadBLECharacteristicValueOption__ctor_m509EFBF44AC14EF7F7DF3223F03C4A9B8B8E9156 (void);
// 0x0000034B System.Void WeChatWASM.RemoveStorageOption::.ctor()
extern void RemoveStorageOption__ctor_mA8F58D1A913C8DD7E3026AF17C6115F00189EE4E (void);
// 0x0000034C System.Void WeChatWASM.RemoveUserCloudStorageOption::.ctor()
extern void RemoveUserCloudStorageOption__ctor_m4AEC9D566801C54A973FF326DF0DA1E110BDFD88 (void);
// 0x0000034D System.Void WeChatWASM.ReportSceneOption::.ctor()
extern void ReportSceneOption__ctor_m8E5ADE63AFB27BA189AD9AE787D91403B355EDFA (void);
// 0x0000034E System.Void WeChatWASM.ReportSceneError::.ctor()
extern void ReportSceneError__ctor_m92CA15D7CCA9A6D059C347FA6AA44717607BA802 (void);
// 0x0000034F System.Void WeChatWASM.ReportSceneFailCallbackErr::.ctor()
extern void ReportSceneFailCallbackErr__ctor_mF9896D6E78BF7E5ADD278D1E1F66486AF33A3ECD (void);
// 0x00000350 System.Void WeChatWASM.ReportSceneSuccessCallbackResult::.ctor()
extern void ReportSceneSuccessCallbackResult__ctor_mA6C9EE4C58CB96A8403A2BBC15A4326788690549 (void);
// 0x00000351 System.Void WeChatWASM.ReportUserBehaviorBranchAnalyticsOption::.ctor()
extern void ReportUserBehaviorBranchAnalyticsOption__ctor_mB633CE445561F87224E174542BC6309B67B60CCE (void);
// 0x00000352 System.Void WeChatWASM.RequestMidasFriendPaymentOption::.ctor()
extern void RequestMidasFriendPaymentOption__ctor_m3CF0B117B388286D5A8C2B54A4350D6617FA504E (void);
// 0x00000353 System.Void WeChatWASM.MidasFriendPaymentError::.ctor()
extern void MidasFriendPaymentError__ctor_m8EFE83862EA6566C4866FF8944D561EF9BB8333B (void);
// 0x00000354 System.Void WeChatWASM.RequestMidasFriendPaymentSuccessCallbackResult::.ctor()
extern void RequestMidasFriendPaymentSuccessCallbackResult__ctor_m0128D58EDF03A3D86AF8BFECF89C6ECD6C79127D (void);
// 0x00000355 System.Void WeChatWASM.RequestMidasPaymentOption::.ctor()
extern void RequestMidasPaymentOption__ctor_mE14E7CD5D96772C97C3AC6FAF318B950FC98DB75 (void);
// 0x00000356 System.Void WeChatWASM.MidasPaymentError::.ctor()
extern void MidasPaymentError__ctor_mB1B7C6B27D9C151C11A621524C61E6902C403349 (void);
// 0x00000357 System.Void WeChatWASM.RequestMidasPaymentFailCallbackErr::.ctor()
extern void RequestMidasPaymentFailCallbackErr__ctor_m262A82B62F9D4D2A04C647F9A3A22E5D1B8BD38D (void);
// 0x00000358 System.Void WeChatWASM.RequestMidasPaymentSuccessCallbackResult::.ctor()
extern void RequestMidasPaymentSuccessCallbackResult__ctor_m968B3E39A7B2FD2455B0C565C48E7C699E37968E (void);
// 0x00000359 System.Void WeChatWASM.RequestSubscribeMessageOption::.ctor()
extern void RequestSubscribeMessageOption__ctor_m90AE2820A24A5AEAEF614C46B732255999B522AE (void);
// 0x0000035A System.Void WeChatWASM.RequestSubscribeMessageFailCallbackResult::.ctor()
extern void RequestSubscribeMessageFailCallbackResult__ctor_m6B12B94335CDCEC9EEDF56632D3F88EF970F6F8F (void);
// 0x0000035B System.Void WeChatWASM.RequestSubscribeMessageSuccessCallbackResult::.ctor()
extern void RequestSubscribeMessageSuccessCallbackResult__ctor_mAC61C9C693EEF202EC4E16DE2DBD4E75DD7E9B6B (void);
// 0x0000035C System.Void WeChatWASM.RequestSubscribeSystemMessageOption::.ctor()
extern void RequestSubscribeSystemMessageOption__ctor_mF4F88D20F5CCEFF380AEDFC3421460214F393E16 (void);
// 0x0000035D System.Void WeChatWASM.RequestSubscribeSystemMessageSuccessCallbackResult::.ctor()
extern void RequestSubscribeSystemMessageSuccessCallbackResult__ctor_mDA6EE15972207E0068E1C066BD4C92AB7426BD62 (void);
// 0x0000035E System.Void WeChatWASM.RequirePrivacyAuthorizeOption::.ctor()
extern void RequirePrivacyAuthorizeOption__ctor_m1CB5BB103872A5B6F024F793B71D5879CE00B2DF (void);
// 0x0000035F System.Void WeChatWASM.ReserveChannelsLiveOption::.ctor()
extern void ReserveChannelsLiveOption__ctor_m6F30CD8F921CDD475E952F2C6AA2BF5471F6DD89 (void);
// 0x00000360 System.Void WeChatWASM.RestartMiniProgramOption::.ctor()
extern void RestartMiniProgramOption__ctor_m5B62CC59FCFDAAD0D6C7E4F1927E46ABF94530AA (void);
// 0x00000361 System.Void WeChatWASM.SaveFileToDiskOption::.ctor()
extern void SaveFileToDiskOption__ctor_m32380FC6255F05C6D69EE0E6968DEFBC6C2AC637 (void);
// 0x00000362 System.Void WeChatWASM.SaveImageToPhotosAlbumOption::.ctor()
extern void SaveImageToPhotosAlbumOption__ctor_m692B8D525AE2F2CAA4D4DB89319F61C0A8A8ED23 (void);
// 0x00000363 System.Void WeChatWASM.ScanCodeOption::.ctor()
extern void ScanCodeOption__ctor_m50F159CDABE935B95CFF562A86F891757CC8B7A2 (void);
// 0x00000364 System.Void WeChatWASM.ScanCodeSuccessCallbackResult::.ctor()
extern void ScanCodeSuccessCallbackResult__ctor_m22D1C13FB7A508157E99D2AE73B45D0A434B4237 (void);
// 0x00000365 System.Void WeChatWASM.SetBLEMTUOption::.ctor()
extern void SetBLEMTUOption__ctor_mEB8A220D6099F4EF0EA2628B8A09FD20C48A6E66 (void);
// 0x00000366 System.Void WeChatWASM.SetBLEMTUFailCallbackResult::.ctor()
extern void SetBLEMTUFailCallbackResult__ctor_m74D99C20515353BA1365B067DAE24E298C2A7161 (void);
// 0x00000367 System.Void WeChatWASM.SetBLEMTUSuccessCallbackResult::.ctor()
extern void SetBLEMTUSuccessCallbackResult__ctor_m36606C47A0E9B2A0A20CC333A28092B235FA14F4 (void);
// 0x00000368 System.Void WeChatWASM.SetClipboardDataOption::.ctor()
extern void SetClipboardDataOption__ctor_m94216C21A1A6DB1E4C67D95881CA56FA92C5C875 (void);
// 0x00000369 System.Void WeChatWASM.SetDeviceOrientationOption::.ctor()
extern void SetDeviceOrientationOption__ctor_mF935408329233B077E1E28E99180EA9AC1D55EB8 (void);
// 0x0000036A System.Void WeChatWASM.SetEnableDebugOption::.ctor()
extern void SetEnableDebugOption__ctor_mBB143C9A78FA1BB325E681BB99ACCE76CEE122B2 (void);
// 0x0000036B System.Void WeChatWASM.SetInnerAudioOption::.ctor()
extern void SetInnerAudioOption__ctor_m1091CEE9D92C6F0601758E66AE8AC49742D3F287 (void);
// 0x0000036C System.Void WeChatWASM.SetKeepScreenOnOption::.ctor()
extern void SetKeepScreenOnOption__ctor_mBC05EC1CEFCFAC959972EB903F5755AF075E6258 (void);
// 0x0000036D System.Void WeChatWASM.SetMenuStyleOption::.ctor()
extern void SetMenuStyleOption__ctor_m0EFC093E8A86A3B72B670C8585A5D6629228ECD2 (void);
// 0x0000036E System.Void WeChatWASM.SetScreenBrightnessOption::.ctor()
extern void SetScreenBrightnessOption__ctor_m385340455DEEFBAD80E9DC82B6ED451DE02152DA (void);
// 0x0000036F System.Void WeChatWASM.SetStatusBarStyleOption::.ctor()
extern void SetStatusBarStyleOption__ctor_m2A7531F5516FD1BD0155EA223036C678325B5C14 (void);
// 0x00000370 System.Void WeChatWASM.SetUserCloudStorageOption::.ctor()
extern void SetUserCloudStorageOption__ctor_m3E9B633FB9C691CFD9A72309DBE387337570A299 (void);
// 0x00000371 System.Void WeChatWASM.KVData::.ctor()
extern void KVData__ctor_m4BB18319F41812D14FF808CB833EEE596D0C7616 (void);
// 0x00000372 System.Void WeChatWASM.ShareAppMessageOption::.ctor()
extern void ShareAppMessageOption__ctor_m9D2D4E0270AA34A0EE5BFDEA3667D1FC8F2067B8 (void);
// 0x00000373 System.Void WeChatWASM.ShowActionSheetOption::.ctor()
extern void ShowActionSheetOption__ctor_m417EDAF0551FD1E9AC9812C181D93AB26748CC41 (void);
// 0x00000374 System.Void WeChatWASM.ShowActionSheetSuccessCallbackResult::.ctor()
extern void ShowActionSheetSuccessCallbackResult__ctor_mC14EE54342212B3C069500091FF304371B7B02DD (void);
// 0x00000375 System.Void WeChatWASM.ShowKeyboardOption::.ctor()
extern void ShowKeyboardOption__ctor_m205E38B38E3C7D7DD5A6DE9BEFC05241F2F80EB1 (void);
// 0x00000376 System.Void WeChatWASM.ShowLoadingOption::.ctor()
extern void ShowLoadingOption__ctor_mC5599336A5B8AB58BD293818CC8E009B5DC25B3B (void);
// 0x00000377 System.Void WeChatWASM.ShowModalOption::.ctor()
extern void ShowModalOption__ctor_m085DDB72C61278E2C91F1ACB8A70D4F3190B001F (void);
// 0x00000378 System.Void WeChatWASM.ShowModalSuccessCallbackResult::.ctor()
extern void ShowModalSuccessCallbackResult__ctor_mFBB3727BD3617016BC8B33D63D37C343FFFC27E8 (void);
// 0x00000379 System.Void WeChatWASM.ShowShareImageMenuOption::.ctor()
extern void ShowShareImageMenuOption__ctor_m675F2AE841562F51BD64BE4540B44FBD99BEAD1C (void);
// 0x0000037A System.Void WeChatWASM.ShowShareMenuOption::.ctor()
extern void ShowShareMenuOption__ctor_m5255C765E1814A055529B01815F7213B24BF32A2 (void);
// 0x0000037B System.Void WeChatWASM.ShowToastOption::.ctor()
extern void ShowToastOption__ctor_mCABB59D742D2891ACC6E25A101606FB84F2E8474 (void);
// 0x0000037C System.Void WeChatWASM.StartAccelerometerOption::.ctor()
extern void StartAccelerometerOption__ctor_m041071735D49E9D9BA203C3F95D9F07C915364DD (void);
// 0x0000037D System.Void WeChatWASM.StartBeaconDiscoveryOption::.ctor()
extern void StartBeaconDiscoveryOption__ctor_m08FBEB25648F3F6DA4AD0253A363F927E6DE96CB (void);
// 0x0000037E System.Void WeChatWASM.StartBluetoothDevicesDiscoveryOption::.ctor()
extern void StartBluetoothDevicesDiscoveryOption__ctor_mD759B1497F133BA779BB3D6E528D6E790D0BE16C (void);
// 0x0000037F System.Void WeChatWASM.StartCompassOption::.ctor()
extern void StartCompassOption__ctor_mEBFB1C53CC35899EA2499330725F175E1A46D9BE (void);
// 0x00000380 System.Void WeChatWASM.StartDeviceMotionListeningOption::.ctor()
extern void StartDeviceMotionListeningOption__ctor_m7619445DC5A6587B67240D3E80BB20FFF12A14E0 (void);
// 0x00000381 System.Void WeChatWASM.StartGyroscopeOption::.ctor()
extern void StartGyroscopeOption__ctor_m6554029ADB18ECA1F7D8F348104866B72C272C1D (void);
// 0x00000382 System.Void WeChatWASM.StopAccelerometerOption::.ctor()
extern void StopAccelerometerOption__ctor_m67D127F65B0015DEF248735F2EDBB39919AF3B4B (void);
// 0x00000383 System.Void WeChatWASM.StopBeaconDiscoveryOption::.ctor()
extern void StopBeaconDiscoveryOption__ctor_m2A988BDD6D68A032518F118D58A984E81DEF7CCB (void);
// 0x00000384 System.Void WeChatWASM.StopBluetoothDevicesDiscoveryOption::.ctor()
extern void StopBluetoothDevicesDiscoveryOption__ctor_m238A2F8ED5EA8A06B8232970D56FE67E7C281C71 (void);
// 0x00000385 System.Void WeChatWASM.StopCompassOption::.ctor()
extern void StopCompassOption__ctor_mF1D345FAEF8C8EC7CEC872AA12908DABA33CF83B (void);
// 0x00000386 System.Void WeChatWASM.StopDeviceMotionListeningOption::.ctor()
extern void StopDeviceMotionListeningOption__ctor_mCE233581F3A2F5284AE265678DB6B4E7DAF03DA9 (void);
// 0x00000387 System.Void WeChatWASM.StopFaceDetectOption::.ctor()
extern void StopFaceDetectOption__ctor_mA554A6F9D9AAB5E5EECA58E0E52E01AB7B4ED3A3 (void);
// 0x00000388 System.Void WeChatWASM.StopGyroscopeOption::.ctor()
extern void StopGyroscopeOption__ctor_mA0655EDA37118FC487BA1E54F182544746548F05 (void);
// 0x00000389 System.Void WeChatWASM.UpdateKeyboardOption::.ctor()
extern void UpdateKeyboardOption__ctor_m4A6A076E1138A931C6D230D6FA9499B7CED54358 (void);
// 0x0000038A System.Void WeChatWASM.UpdateShareMenuOption::.ctor()
extern void UpdateShareMenuOption__ctor_m482E41EE8FD17D0980C2B745E675F860CB2FE270 (void);
// 0x0000038B System.Void WeChatWASM.UpdatableMessageFrontEndTemplateInfo::.ctor()
extern void UpdatableMessageFrontEndTemplateInfo__ctor_m3F4C9AB56D504ACEFE7BF4A1A7D0D0F5F1BE63A6 (void);
// 0x0000038C System.Void WeChatWASM.UpdatableMessageFrontEndParameter::.ctor()
extern void UpdatableMessageFrontEndParameter__ctor_mB7818C309BFD7768D7420422FC0F5B547FB5A1AF (void);
// 0x0000038D System.Void WeChatWASM.UpdateVoIPChatMuteConfigOption::.ctor()
extern void UpdateVoIPChatMuteConfigOption__ctor_m95E7E27B635144023EF289E87791C5869ED074F7 (void);
// 0x0000038E System.Void WeChatWASM.UpdateWeChatAppOption::.ctor()
extern void UpdateWeChatAppOption__ctor_m6608DB8E04275CF71D28C2D229E101D2F09C5FCB (void);
// 0x0000038F System.Void WeChatWASM.VibrateLongOption::.ctor()
extern void VibrateLongOption__ctor_m74D40F6E969C3F59E4DB36CFE5FA552B28CFC7D3 (void);
// 0x00000390 System.Void WeChatWASM.VibrateShortOption::.ctor()
extern void VibrateShortOption__ctor_m19AF74068ACD0917D52AEED7FEA443B07D81AB68 (void);
// 0x00000391 System.Void WeChatWASM.VibrateShortFailCallbackResult::.ctor()
extern void VibrateShortFailCallbackResult__ctor_m7A4AF09BED206290BACCD7CA7B6052CFDEE70C73 (void);
// 0x00000392 System.Void WeChatWASM.WriteBLECharacteristicValueOption::.ctor()
extern void WriteBLECharacteristicValueOption__ctor_m4C0E11AA0B0EA7F20CC7C584B65BEBDF3A5D4E2C (void);
// 0x00000393 System.Void WeChatWASM.StartGameLiveOption::.ctor()
extern void StartGameLiveOption__ctor_m67062514F3F656F7F98413E578AE6FFD96D7B060 (void);
// 0x00000394 System.Void WeChatWASM.CheckGameLiveEnabledOption::.ctor()
extern void CheckGameLiveEnabledOption__ctor_m7B63ED1C3C1241AF77BF16FA448D055CFC7FE237 (void);
// 0x00000395 System.Void WeChatWASM.CheckGameLiveEnabledSuccessCallbackOption::.ctor()
extern void CheckGameLiveEnabledSuccessCallbackOption__ctor_mE15EC51137A0F7D58394A743EDD3A4BEC2EC66C9 (void);
// 0x00000396 System.Void WeChatWASM.OnGameLiveStateChangeCallbackResult::.ctor()
extern void OnGameLiveStateChangeCallbackResult__ctor_m6C7AFBF09893774C4541CBBC91EBDEDB9BE4E084 (void);
// 0x00000397 System.Void WeChatWASM.OnGameLiveStateChangeCallbackResponse::.ctor()
extern void OnGameLiveStateChangeCallbackResponse__ctor_m6566D436C34309D523610AA125CD79EAD2A212DB (void);
// 0x00000398 System.Void WeChatWASM.GameLiveState::.ctor()
extern void GameLiveState__ctor_m463EEB622EDD0A42D6A2FCD66399977C296FE6D6 (void);
// 0x00000399 System.Void WeChatWASM.GetUserCurrentGameliveInfoOption::.ctor()
extern void GetUserCurrentGameliveInfoOption__ctor_m891185F9A4792C74564A81DFAA37A155E99096C9 (void);
// 0x0000039A System.Void WeChatWASM.GetUserCurrentGameliveInfoSuccessCallbackOption::.ctor()
extern void GetUserCurrentGameliveInfoSuccessCallbackOption__ctor_m1CDC665EFAACBC4F8BAA8FFC148F1F304D47B41D (void);
// 0x0000039B System.Void WeChatWASM.GetUserRecentGameLiveInfoOption::.ctor()
extern void GetUserRecentGameLiveInfoOption__ctor_m7D8E3324209097DAE283FB196B7730189ACD8C7C (void);
// 0x0000039C System.Void WeChatWASM.GetUserGameLiveDetailsSuccessCallbackOption::.ctor()
extern void GetUserGameLiveDetailsSuccessCallbackOption__ctor_mDE05BE5CEB5172979A7317706750F49E041D4AA5 (void);
// 0x0000039D System.Void WeChatWASM.GetUserGameLiveDetailsOption::.ctor()
extern void GetUserGameLiveDetailsOption__ctor_mCA43F9E45384BA58885F19E924DD5AD4257C245E (void);
// 0x0000039E System.Void WeChatWASM.OpenChannelsLiveCollectionOption::.ctor()
extern void OpenChannelsLiveCollectionOption__ctor_m416AB518AFE5177242E171E7F2FEE76AA4ADAE23 (void);
// 0x0000039F System.Void WeChatWASM.OpenPageOption::.ctor()
extern void OpenPageOption__ctor_mC449748161D14DA9F3CD537AB990B2181F48035B (void);
// 0x000003A0 System.Void WeChatWASM.RequestMidasPaymentGameItemOption::.ctor()
extern void RequestMidasPaymentGameItemOption__ctor_mE860E58202D5AB273D85E24A939252E2DA72954C (void);
// 0x000003A1 System.Void WeChatWASM.MidasPaymentGameItemError::.ctor()
extern void MidasPaymentGameItemError__ctor_mBBEF23D64DDF03E34FC485CC0E6899DA30AB6433 (void);
// 0x000003A2 System.Void WeChatWASM.RequestSubscribeLiveActivityOption::.ctor()
extern void RequestSubscribeLiveActivityOption__ctor_m43A22B90F9C712F68533410555EA50574918FD28 (void);
// 0x000003A3 System.Void WeChatWASM.RequestSubscribeLiveActivitySuccessCallbackResult::.ctor()
extern void RequestSubscribeLiveActivitySuccessCallbackResult__ctor_m8B9F0F1693B717094477841DDF54CB9869E08A23 (void);
// 0x000003A4 System.Void WeChatWASM.FrameDataOptions::.ctor()
extern void FrameDataOptions__ctor_mE4FDF4B2ED374D0DC3056D1C1DFE3B3E360B7EDB (void);
// 0x000003A5 System.Void WeChatWASM.WXOpenDataContext::PostMessage(System.String)
extern void WXOpenDataContext_PostMessage_m85C3FDCA0C6C814C5416650D59C94325EA2C9EE4 (void);
// 0x000003A6 System.Void WeChatWASM.WXOpenDataContext::.ctor()
extern void WXOpenDataContext__ctor_mA53F9F53F8C03548101FEC3A6AED060695476BD2 (void);
// 0x000003A7 System.Void WeChatWASM.WXRealtimeLogManager::.ctor(System.String)
extern void WXRealtimeLogManager__ctor_mD0568EFD441FEA72D47AA6AEBCD34C211B628F44 (void);
// 0x000003A8 System.Void WeChatWASM.WXRealtimeLogManager::A(System.String,System.String)
extern void WXRealtimeLogManager_A_mCEFDF920469F9C7465ECEE0C259CA107DC11A0A0 (void);
// 0x000003A9 System.Void WeChatWASM.WXRealtimeLogManager::AddFilterMsg(System.String)
extern void WXRealtimeLogManager_AddFilterMsg_m5A92D9791357E586D740FC737E4B1F6CE3EDDDB8 (void);
// 0x000003AA System.Void WeChatWASM.WXRealtimeLogManager::a(System.String,System.String)
extern void WXRealtimeLogManager_a_mCCFFF5E47D99374C3B84F7912B573F025D7CE6EC (void);
// 0x000003AB System.Void WeChatWASM.WXRealtimeLogManager::Error(System.String)
extern void WXRealtimeLogManager_Error_mA94A1468388EE347C59E4A7C622894BBE13E2691 (void);
// 0x000003AC System.Void WeChatWASM.WXRealtimeLogManager::B(System.String,System.String)
extern void WXRealtimeLogManager_B_mBADE1B3EF4CE01ECAACB3A69AEA15DC86F0133F5 (void);
// 0x000003AD System.Void WeChatWASM.WXRealtimeLogManager::Info(System.String)
extern void WXRealtimeLogManager_Info_m86DC3D51ABEAC094ECAE6AA40849E38BBB86B57D (void);
// 0x000003AE System.Void WeChatWASM.WXRealtimeLogManager::b(System.String,System.String)
extern void WXRealtimeLogManager_b_m8B4D60AB29A53E8AE9CCCDC7E37CE8AB08BF13BE (void);
// 0x000003AF System.Void WeChatWASM.WXRealtimeLogManager::SetFilterMsg(System.String)
extern void WXRealtimeLogManager_SetFilterMsg_m12966EBBF5F46BEC374FFED36567699267CF32F1 (void);
// 0x000003B0 System.Void WeChatWASM.WXRealtimeLogManager::C(System.String,System.String)
extern void WXRealtimeLogManager_C_mE73EDD2CF0B6020C8FB6D3C5F41C25E0A61D2384 (void);
// 0x000003B1 System.Void WeChatWASM.WXRealtimeLogManager::Warn(System.String)
extern void WXRealtimeLogManager_Warn_m2306D988C1D3F95564B663D9125CE7154535C09D (void);
// 0x000003B2 System.Void WeChatWASM.WXRecorderManager::.ctor(System.String)
extern void WXRecorderManager__ctor_m6C63BB5EF3B079CBB60C407DC56E74181BAA67BF (void);
// 0x000003B3 System.Void WeChatWASM.WXRecorderManager::A(System.String)
extern void WXRecorderManager_A_m508936A50D9C8CA42DB8D1906ED3C0DEF0A71A9F (void);
// 0x000003B4 System.Void WeChatWASM.WXRecorderManager::OnError(System.Action)
extern void WXRecorderManager_OnError_mC75A9487C26A2588B454A0B1D91D323520CB67FF (void);
// 0x000003B5 System.Void WeChatWASM.WXRecorderManager::a(System.String)
extern void WXRecorderManager_a_m9A29AB914B7E46EB8856957711BD31BE9236DFA8 (void);
// 0x000003B6 System.Void WeChatWASM.WXRecorderManager::OnFrameRecorded(System.Action`1<WeChatWASM.OnFrameRecordedCallbackResult>)
extern void WXRecorderManager_OnFrameRecorded_m8F58CC34FF5994A60EBEE9E311B352D5ABC66FFB (void);
// 0x000003B7 System.Void WeChatWASM.WXRecorderManager::B(System.String)
extern void WXRecorderManager_B_m56A17D9BD1800F9ACADB1D36D9C748AE5C7DEF39 (void);
// 0x000003B8 System.Void WeChatWASM.WXRecorderManager::OnInterruptionBegin(System.Action)
extern void WXRecorderManager_OnInterruptionBegin_m3358E28D71344A3085C11C4B7C79AABDEC9774CE (void);
// 0x000003B9 System.Void WeChatWASM.WXRecorderManager::b(System.String)
extern void WXRecorderManager_b_m9728E23BBB102F6F7010C5B77DDC9E1BAB6438BC (void);
// 0x000003BA System.Void WeChatWASM.WXRecorderManager::OnInterruptionEnd(System.Action)
extern void WXRecorderManager_OnInterruptionEnd_m8EF77072F7D6E406A5B5A3049E9827D7C5AC4096 (void);
// 0x000003BB System.Void WeChatWASM.WXRecorderManager::C(System.String)
extern void WXRecorderManager_C_m06A08E6AA1D40E21F928FF35E4FCF91980BE9708 (void);
// 0x000003BC System.Void WeChatWASM.WXRecorderManager::OnPause(System.Action)
extern void WXRecorderManager_OnPause_m253B24D2479D768CBB82887863F38589F4F3DA2A (void);
// 0x000003BD System.Void WeChatWASM.WXRecorderManager::c(System.String)
extern void WXRecorderManager_c_mDEFEE50E47FDC9F47F161801486E5EC6B76CCE78 (void);
// 0x000003BE System.Void WeChatWASM.WXRecorderManager::OnResume(System.Action)
extern void WXRecorderManager_OnResume_m199F7B6849C988C854DC23E9EBAE4C280F6C5960 (void);
// 0x000003BF System.Void WeChatWASM.WXRecorderManager::D(System.String)
extern void WXRecorderManager_D_m1408F4715458D6E30596FB4CC512E4175ED7417E (void);
// 0x000003C0 System.Void WeChatWASM.WXRecorderManager::OnStart(System.Action)
extern void WXRecorderManager_OnStart_m5DBBEB6D734CD1922E5A34F60C4340203631529B (void);
// 0x000003C1 System.Void WeChatWASM.WXRecorderManager::d(System.String)
extern void WXRecorderManager_d_m2D58D99F5E2E505049A567139A2AED85548EC651 (void);
// 0x000003C2 System.Void WeChatWASM.WXRecorderManager::OnStop(System.Action`1<WeChatWASM.OnStopCallbackResult>)
extern void WXRecorderManager_OnStop_m07FBD1D69CD901D7323D21327B0D190D748018EE (void);
// 0x000003C3 System.Void WeChatWASM.WXRecorderManager::E(System.String)
extern void WXRecorderManager_E_m104395455A4DC4EF808A0A2B429A2E73B60CA882 (void);
// 0x000003C4 System.Void WeChatWASM.WXRecorderManager::Pause()
extern void WXRecorderManager_Pause_mF7CB0B3CE8678872584008D668818FE6FDCEF79F (void);
// 0x000003C5 System.Void WeChatWASM.WXRecorderManager::e(System.String)
extern void WXRecorderManager_e_m312A077955504F16996991B5C8B0A91834867191 (void);
// 0x000003C6 System.Void WeChatWASM.WXRecorderManager::Resume()
extern void WXRecorderManager_Resume_m7C1B863E1D73404F6FF9F5CF4989DCACDC59B9E9 (void);
// 0x000003C7 System.Void WeChatWASM.WXRecorderManager::A(System.String,System.String)
extern void WXRecorderManager_A_mC7EBF70FF01BDCB70F9AF6FB73731FDB03C0397E (void);
// 0x000003C8 System.Void WeChatWASM.WXRecorderManager::Start(WeChatWASM.RecorderManagerStartOption)
extern void WXRecorderManager_Start_m460C9009B65330AE45DAC58F9EE71F51128F82AC (void);
// 0x000003C9 System.Void WeChatWASM.WXRecorderManager::F(System.String)
extern void WXRecorderManager_F_m8EFB62D690DA48100F247ECD17CAC9D90D95F305 (void);
// 0x000003CA System.Void WeChatWASM.WXRecorderManager::Stop()
extern void WXRecorderManager_Stop_m0F70E95BD08A28764D54E8BF1FE25A3F04B00B55 (void);
// 0x000003CB System.Void WeChatWASM.OnFrameRecordedCallbackResult::.ctor()
extern void OnFrameRecordedCallbackResult__ctor_m9FAFAC7DDF34C5688DFC8E717FB22BE9918C75FA (void);
// 0x000003CC System.Void WeChatWASM.OnFrameRecordedBufferCallbackResult::.ctor()
extern void OnFrameRecordedBufferCallbackResult__ctor_m29B24E42A2178866DFBCC3F8669C34EC2640ABEE (void);
// 0x000003CD System.Void WeChatWASM.OnStopCallbackResult::.ctor()
extern void OnStopCallbackResult__ctor_m54AE90E0A07A6B182DAAFEABF47D3601B0BCFE33 (void);
// 0x000003CE System.Void WeChatWASM.RecorderManagerStartOption::.ctor()
extern void RecorderManagerStartOption__ctor_m40B474DEA2F37054EB1D2D423C1CED8AAFD5445E (void);
// 0x000003CF System.String WeChatWASM.WXRewardedVideoAd::A(System.String,System.String)
extern void WXRewardedVideoAd_A_m5ACA2EB7B87E244380E11FFB089D8B27E66F2F78 (void);
// 0x000003D0 System.Void WeChatWASM.WXRewardedVideoAd::.ctor(System.String)
extern void WXRewardedVideoAd__ctor_mBA3881062D98F861C0AA8572913091F5E8AC0E33 (void);
// 0x000003D1 System.Void WeChatWASM.WXRewardedVideoAd::OnCloseCallback(WeChatWASM.WXRewardedVideoAdOnCloseResponse)
extern void WXRewardedVideoAd_OnCloseCallback_m1AF901145BDB7B36719303CCA18CF37494AC3D32 (void);
// 0x000003D2 System.Void WeChatWASM.WXRewardedVideoAd::Load(System.Action`1<WeChatWASM.WXTextResponse>,System.Action`1<WeChatWASM.WXADErrorResponse>)
extern void WXRewardedVideoAd_Load_m44346E7FEB1EDF8D06846F102C5A8D50D570A294 (void);
// 0x000003D3 System.Void WeChatWASM.WXRewardedVideoAd::OnClose(System.Action`1<WeChatWASM.WXRewardedVideoAdOnCloseResponse>)
extern void WXRewardedVideoAd_OnClose_m36E643BE8F5D9AB65E8E1500E4243A943C061CA0 (void);
// 0x000003D4 System.Void WeChatWASM.WXRewardedVideoAd::OffClose(System.Action`1<WeChatWASM.WXRewardedVideoAdOnCloseResponse>)
extern void WXRewardedVideoAd_OffClose_m81118DC866C735F81D70A26A8E5D77FF7AD22AB3 (void);
// 0x000003D5 WeChatWASM.WXRewardedVideoAdReportShareBehaviorResponse WeChatWASM.WXRewardedVideoAd::ReportShareBehavior(WeChatWASM.RequestAdReportShareBehaviorParam)
extern void WXRewardedVideoAd_ReportShareBehavior_mF6BFB3562343F85EC41B23ABF968E67B0F0F7842 (void);
// 0x000003D6 System.String WeChatWASM.WXSDKManagerHandler::GetCallbackId(System.Collections.Generic.Dictionary`2<System.String,T>)
// 0x000003D7 System.Void WeChatWASM.WXSDKManagerHandler::AddCardCallback(System.String)
extern void WXSDKManagerHandler_AddCardCallback_mF81C26ED35A5A9ABEE7F99E14BA4401E0CED4AF7 (void);
// 0x000003D8 System.Void WeChatWASM.WXSDKManagerHandler::A(System.String,System.String)
extern void WXSDKManagerHandler_A_mF78D18580496D91EECA5A81F4E8C7F625A6C5BA2 (void);
// 0x000003D9 System.Void WeChatWASM.WXSDKManagerHandler::AddCard(WeChatWASM.AddCardOption)
extern void WXSDKManagerHandler_AddCard_m36AE74DD5E00C43AEB1B7430FD9FAEE139E86B32 (void);
// 0x000003DA System.Void WeChatWASM.WXSDKManagerHandler::AuthPrivateMessageCallback(System.String)
extern void WXSDKManagerHandler_AuthPrivateMessageCallback_m0F24164768DD7C8D3E47423697FA095C4587ADEB (void);
// 0x000003DB System.Void WeChatWASM.WXSDKManagerHandler::a(System.String,System.String)
extern void WXSDKManagerHandler_a_mB2CD877A88109489BBEC92B3C828988810A78A42 (void);
// 0x000003DC System.Void WeChatWASM.WXSDKManagerHandler::AuthPrivateMessage(WeChatWASM.AuthPrivateMessageOption)
extern void WXSDKManagerHandler_AuthPrivateMessage_m9CCC429D98017D5DD1834461C0E3154EC5940AE1 (void);
// 0x000003DD System.Void WeChatWASM.WXSDKManagerHandler::AuthorizeCallback(System.String)
extern void WXSDKManagerHandler_AuthorizeCallback_m8D9A412DD52B0266DACF5947C12D5EDD608ED60F (void);
// 0x000003DE System.Void WeChatWASM.WXSDKManagerHandler::B(System.String,System.String)
extern void WXSDKManagerHandler_B_mBD775616DD641BB88E4399A2C1DA84D67ED55E1D (void);
// 0x000003DF System.Void WeChatWASM.WXSDKManagerHandler::Authorize(WeChatWASM.AuthorizeOption)
extern void WXSDKManagerHandler_Authorize_m4412649F0D9A306A8B8DDDD2EC9961415ECFA408 (void);
// 0x000003E0 System.Void WeChatWASM.WXSDKManagerHandler::CheckIsAddedToMyMiniProgramCallback(System.String)
extern void WXSDKManagerHandler_CheckIsAddedToMyMiniProgramCallback_m6FE88C5FF2769023A61DDE8589239757F5F1A56F (void);
// 0x000003E1 System.Void WeChatWASM.WXSDKManagerHandler::b(System.String,System.String)
extern void WXSDKManagerHandler_b_mEE617029548D202A73BFEA208ACFD3A2B8ED25A0 (void);
// 0x000003E2 System.Void WeChatWASM.WXSDKManagerHandler::CheckIsAddedToMyMiniProgram(WeChatWASM.CheckIsAddedToMyMiniProgramOption)
extern void WXSDKManagerHandler_CheckIsAddedToMyMiniProgram_mB2852C4469B80C00C2DF306ED88C9A54DD2C579C (void);
// 0x000003E3 System.Void WeChatWASM.WXSDKManagerHandler::CheckSessionCallback(System.String)
extern void WXSDKManagerHandler_CheckSessionCallback_m1BFDF3CE1D0B029027ADF6EBFDF13609F097A76F (void);
// 0x000003E4 System.Void WeChatWASM.WXSDKManagerHandler::C(System.String,System.String)
extern void WXSDKManagerHandler_C_mA49DFF4E5255356C40CE7EE2284AFEFD8FEDEEAE (void);
// 0x000003E5 System.Void WeChatWASM.WXSDKManagerHandler::CheckSession(WeChatWASM.CheckSessionOption)
extern void WXSDKManagerHandler_CheckSession_m9BE75BA73BD576C9C37481EB5D58A2B773CB6763 (void);
// 0x000003E6 System.Void WeChatWASM.WXSDKManagerHandler::ChooseImageCallback(System.String)
extern void WXSDKManagerHandler_ChooseImageCallback_m3A9093329E7286E91E094EE0F6670CBE2076CFF7 (void);
// 0x000003E7 System.Void WeChatWASM.WXSDKManagerHandler::c(System.String,System.String)
extern void WXSDKManagerHandler_c_m22A33D6C009F7034D19A0E1C3BC60E50E18AE0EF (void);
// 0x000003E8 System.Void WeChatWASM.WXSDKManagerHandler::ChooseImage(WeChatWASM.ChooseImageOption)
extern void WXSDKManagerHandler_ChooseImage_m9A1976749AE6D06EB0E05D1DC877F9AE64771CBC (void);
// 0x000003E9 System.Void WeChatWASM.WXSDKManagerHandler::ChooseMediaCallback(System.String)
extern void WXSDKManagerHandler_ChooseMediaCallback_mDE8F36B7A59A7F2ED1BEED10BFE4230BA4EB9170 (void);
// 0x000003EA System.Void WeChatWASM.WXSDKManagerHandler::D(System.String,System.String)
extern void WXSDKManagerHandler_D_mDF26CC2E9D07B155709DF464669701EAF4F9D2A7 (void);
// 0x000003EB System.Void WeChatWASM.WXSDKManagerHandler::ChooseMedia(WeChatWASM.ChooseMediaOption)
extern void WXSDKManagerHandler_ChooseMedia_m7C8E0D8B637975325992160AAE738429872639C8 (void);
// 0x000003EC System.Void WeChatWASM.WXSDKManagerHandler::ChooseMessageFileCallback(System.String)
extern void WXSDKManagerHandler_ChooseMessageFileCallback_m590DF04D7A3276CEEB9F5F1102847A376AC51015 (void);
// 0x000003ED System.Void WeChatWASM.WXSDKManagerHandler::d(System.String,System.String)
extern void WXSDKManagerHandler_d_m3F3043B925F3A333386124806B577623B64B5980 (void);
// 0x000003EE System.Void WeChatWASM.WXSDKManagerHandler::ChooseMessageFile(WeChatWASM.ChooseMessageFileOption)
extern void WXSDKManagerHandler_ChooseMessageFile_mB7E7F98C95B8DDF43CD03B2AAA13B9428201C36F (void);
// 0x000003EF System.Void WeChatWASM.WXSDKManagerHandler::CloseBLEConnectionCallback(System.String)
extern void WXSDKManagerHandler_CloseBLEConnectionCallback_mE9981D10549D04C7AEF342FB14EE54C23DEA3191 (void);
// 0x000003F0 System.Void WeChatWASM.WXSDKManagerHandler::E(System.String,System.String)
extern void WXSDKManagerHandler_E_mE4D151CF693252D176C9A7051DD618D202A120BF (void);
// 0x000003F1 System.Void WeChatWASM.WXSDKManagerHandler::CloseBLEConnection(WeChatWASM.CloseBLEConnectionOption)
extern void WXSDKManagerHandler_CloseBLEConnection_m027893595FB94024CA8230DC76A8F5D9EB9D5677 (void);
// 0x000003F2 System.Void WeChatWASM.WXSDKManagerHandler::CloseBluetoothAdapterCallback(System.String)
extern void WXSDKManagerHandler_CloseBluetoothAdapterCallback_m7C885B1CAAA63329FAD4C255A19981DF6BC005DD (void);
// 0x000003F3 System.Void WeChatWASM.WXSDKManagerHandler::e(System.String,System.String)
extern void WXSDKManagerHandler_e_m4840004541A104FC3B8FF06E6533DCCFEC6AA490 (void);
// 0x000003F4 System.Void WeChatWASM.WXSDKManagerHandler::CloseBluetoothAdapter(WeChatWASM.CloseBluetoothAdapterOption)
extern void WXSDKManagerHandler_CloseBluetoothAdapter_mDE24D06948F1CA476CEE89A7D61153DD25B3ECDB (void);
// 0x000003F5 System.Void WeChatWASM.WXSDKManagerHandler::CreateBLEConnectionCallback(System.String)
extern void WXSDKManagerHandler_CreateBLEConnectionCallback_m5D33DA3DF10FFC17C7EA0E0383A23FF8C0D7D595 (void);
// 0x000003F6 System.Void WeChatWASM.WXSDKManagerHandler::F(System.String,System.String)
extern void WXSDKManagerHandler_F_m32C126B54D2609E20B1160F54170D9CB0CD35C97 (void);
// 0x000003F7 System.Void WeChatWASM.WXSDKManagerHandler::CreateBLEConnection(WeChatWASM.CreateBLEConnectionOption)
extern void WXSDKManagerHandler_CreateBLEConnection_m5BB6D877BE8C77CC9FAD17FF9B84A2A2D5F341FA (void);
// 0x000003F8 System.Void WeChatWASM.WXSDKManagerHandler::CreateBLEPeripheralServerCallback(System.String)
extern void WXSDKManagerHandler_CreateBLEPeripheralServerCallback_m61995D2318A8F504A61F7DB5F3BBE8AD2530F21C (void);
// 0x000003F9 System.Void WeChatWASM.WXSDKManagerHandler::f(System.String,System.String)
extern void WXSDKManagerHandler_f_m41EF543EBA205484EC03A5C45F38B5DC21A9709A (void);
// 0x000003FA System.Void WeChatWASM.WXSDKManagerHandler::CreateBLEPeripheralServer(WeChatWASM.CreateBLEPeripheralServerOption)
extern void WXSDKManagerHandler_CreateBLEPeripheralServer_m1AE25612CBAA74473E9A4AA55D5855DD04DCA677 (void);
// 0x000003FB System.Void WeChatWASM.WXSDKManagerHandler::ExitMiniProgramCallback(System.String)
extern void WXSDKManagerHandler_ExitMiniProgramCallback_m32EF41E6FEE8D7F384BE0590E89864F3165CFB9E (void);
// 0x000003FC System.Void WeChatWASM.WXSDKManagerHandler::G(System.String,System.String)
extern void WXSDKManagerHandler_G_mA93C6C55252C72363B16A5ABA5F9E706BF8A4720 (void);
// 0x000003FD System.Void WeChatWASM.WXSDKManagerHandler::ExitMiniProgram(WeChatWASM.ExitMiniProgramOption)
extern void WXSDKManagerHandler_ExitMiniProgram_m0AD9C912FFB39E9326309300C640ED888B9E0615 (void);
// 0x000003FE System.Void WeChatWASM.WXSDKManagerHandler::ExitVoIPChatCallback(System.String)
extern void WXSDKManagerHandler_ExitVoIPChatCallback_m36CE2131E677F2E3C439E295617BF53A20937C0E (void);
// 0x000003FF System.Void WeChatWASM.WXSDKManagerHandler::g(System.String,System.String)
extern void WXSDKManagerHandler_g_m1930C23894082B41941AF3557437CBD083179079 (void);
// 0x00000400 System.Void WeChatWASM.WXSDKManagerHandler::ExitVoIPChat(WeChatWASM.ExitVoIPChatOption)
extern void WXSDKManagerHandler_ExitVoIPChat_m2DE1A56CC8C27545DB0AD8F0832D1FA78E838270 (void);
// 0x00000401 System.Void WeChatWASM.WXSDKManagerHandler::FaceDetectCallback(System.String)
extern void WXSDKManagerHandler_FaceDetectCallback_m15D6F80FA5C719E53020AC4F32929D61DE774C4E (void);
// 0x00000402 System.Void WeChatWASM.WXSDKManagerHandler::H(System.String,System.String)
extern void WXSDKManagerHandler_H_m6C50414A2465CFC5A2EFC0CE61C60D5549EC0A00 (void);
// 0x00000403 System.Void WeChatWASM.WXSDKManagerHandler::FaceDetect(WeChatWASM.FaceDetectOption)
extern void WXSDKManagerHandler_FaceDetect_mA8B1F45A2E0FF82EEE0ADC4A3F402A02DDA51955 (void);
// 0x00000404 System.Void WeChatWASM.WXSDKManagerHandler::GetAvailableAudioSourcesCallback(System.String)
extern void WXSDKManagerHandler_GetAvailableAudioSourcesCallback_mBE586BD77B1054401B28D25D36C16610C62E1D2F (void);
// 0x00000405 System.Void WeChatWASM.WXSDKManagerHandler::h(System.String,System.String)
extern void WXSDKManagerHandler_h_m6CC463398DAFD841F4262B76458EFCE0A48D60F2 (void);
// 0x00000406 System.Void WeChatWASM.WXSDKManagerHandler::GetAvailableAudioSources(WeChatWASM.GetAvailableAudioSourcesOption)
extern void WXSDKManagerHandler_GetAvailableAudioSources_m083E786C4427C33C122845AD79CB1621D398C069 (void);
// 0x00000407 System.Void WeChatWASM.WXSDKManagerHandler::GetBLEDeviceCharacteristicsCallback(System.String)
extern void WXSDKManagerHandler_GetBLEDeviceCharacteristicsCallback_m90F977DF087811BF85CD97054D51BF8126522AFF (void);
// 0x00000408 System.Void WeChatWASM.WXSDKManagerHandler::I(System.String,System.String)
extern void WXSDKManagerHandler_I_mC99FED494527D6D079DE3A0E425CAC9CAF88C5A6 (void);
// 0x00000409 System.Void WeChatWASM.WXSDKManagerHandler::GetBLEDeviceCharacteristics(WeChatWASM.GetBLEDeviceCharacteristicsOption)
extern void WXSDKManagerHandler_GetBLEDeviceCharacteristics_m340DC9700BB9DB064966533A0FB8E350DE5458F9 (void);
// 0x0000040A System.Void WeChatWASM.WXSDKManagerHandler::GetBLEDeviceRSSICallback(System.String)
extern void WXSDKManagerHandler_GetBLEDeviceRSSICallback_m8C69C4BD89C2D4483F16B2CFFC10B636F2D4927F (void);
// 0x0000040B System.Void WeChatWASM.WXSDKManagerHandler::i(System.String,System.String)
extern void WXSDKManagerHandler_i_m781A1DAA4474EA284F07C8352E0B7C03CE1CE3DF (void);
// 0x0000040C System.Void WeChatWASM.WXSDKManagerHandler::GetBLEDeviceRSSI(WeChatWASM.GetBLEDeviceRSSIOption)
extern void WXSDKManagerHandler_GetBLEDeviceRSSI_m75C2161BDF843C56FE46822EAAE92C116AB6FBAC (void);
// 0x0000040D System.Void WeChatWASM.WXSDKManagerHandler::GetBLEDeviceServicesCallback(System.String)
extern void WXSDKManagerHandler_GetBLEDeviceServicesCallback_mBB991E7925ED181233C2F10DE8E1BB3C8B062388 (void);
// 0x0000040E System.Void WeChatWASM.WXSDKManagerHandler::J(System.String,System.String)
extern void WXSDKManagerHandler_J_m9F9D214021C356B5CC0914894868305A184A4A02 (void);
// 0x0000040F System.Void WeChatWASM.WXSDKManagerHandler::GetBLEDeviceServices(WeChatWASM.GetBLEDeviceServicesOption)
extern void WXSDKManagerHandler_GetBLEDeviceServices_m34B68DC2E47A061A68D4F8E3869DDC8A08882EA7 (void);
// 0x00000410 System.Void WeChatWASM.WXSDKManagerHandler::GetBLEMTUCallback(System.String)
extern void WXSDKManagerHandler_GetBLEMTUCallback_m9FA0AB1460F042CFFB40C820FCC754E9154FDD7D (void);
// 0x00000411 System.Void WeChatWASM.WXSDKManagerHandler::j(System.String,System.String)
extern void WXSDKManagerHandler_j_m62CF5357D9E235EFFE8408E5EB94910A7FC27F35 (void);
// 0x00000412 System.Void WeChatWASM.WXSDKManagerHandler::GetBLEMTU(WeChatWASM.GetBLEMTUOption)
extern void WXSDKManagerHandler_GetBLEMTU_mA29C9B06479DC7A110BE077FB9DDCB8A05470B62 (void);
// 0x00000413 System.Void WeChatWASM.WXSDKManagerHandler::GetBatteryInfoCallback(System.String)
extern void WXSDKManagerHandler_GetBatteryInfoCallback_m70FB9A8B05F6DAD5205274A6FD991B7B1A080887 (void);
// 0x00000414 System.Void WeChatWASM.WXSDKManagerHandler::K(System.String,System.String)
extern void WXSDKManagerHandler_K_m306046EA4FD11161EA266D423EF1F0A2DFF73C31 (void);
// 0x00000415 System.Void WeChatWASM.WXSDKManagerHandler::GetBatteryInfo(WeChatWASM.GetBatteryInfoOption)
extern void WXSDKManagerHandler_GetBatteryInfo_m2E842FB0D7F4F9D5B041FED0EE9D661C1EC48957 (void);
// 0x00000416 System.Void WeChatWASM.WXSDKManagerHandler::GetBeaconsCallback(System.String)
extern void WXSDKManagerHandler_GetBeaconsCallback_m147B06F603CB741E303D09A9FB5C8C291E807AC2 (void);
// 0x00000417 System.Void WeChatWASM.WXSDKManagerHandler::k(System.String,System.String)
extern void WXSDKManagerHandler_k_mAD2D117B222C0F9EFDD778FFDE0CF217CDAD5726 (void);
// 0x00000418 System.Void WeChatWASM.WXSDKManagerHandler::GetBeacons(WeChatWASM.GetBeaconsOption)
extern void WXSDKManagerHandler_GetBeacons_mD98329375BD8A8709BC92036086D3ED4EEF632CE (void);
// 0x00000419 System.Void WeChatWASM.WXSDKManagerHandler::GetBluetoothAdapterStateCallback(System.String)
extern void WXSDKManagerHandler_GetBluetoothAdapterStateCallback_m3F37A4D57F5E42B537731D70478113085E5D423A (void);
// 0x0000041A System.Void WeChatWASM.WXSDKManagerHandler::L(System.String,System.String)
extern void WXSDKManagerHandler_L_mCDD9738B1EB21E2E1B1AE9C44BAE029141D7C537 (void);
// 0x0000041B System.Void WeChatWASM.WXSDKManagerHandler::GetBluetoothAdapterState(WeChatWASM.GetBluetoothAdapterStateOption)
extern void WXSDKManagerHandler_GetBluetoothAdapterState_m9B4683CDDB69F2B078BC2839030AA4C16AFAE0C3 (void);
// 0x0000041C System.Void WeChatWASM.WXSDKManagerHandler::GetBluetoothDevicesCallback(System.String)
extern void WXSDKManagerHandler_GetBluetoothDevicesCallback_m5E57B2367BA75F43A5E0DC3FB6B783306416BD4C (void);
// 0x0000041D System.Void WeChatWASM.WXSDKManagerHandler::l(System.String,System.String)
extern void WXSDKManagerHandler_l_mFD43BFA587FCB2F174502155D968FE59BAAB98FF (void);
// 0x0000041E System.Void WeChatWASM.WXSDKManagerHandler::GetBluetoothDevices(WeChatWASM.GetBluetoothDevicesOption)
extern void WXSDKManagerHandler_GetBluetoothDevices_mE21CF08C3F24C718662A16770B8278F550919802 (void);
// 0x0000041F System.Void WeChatWASM.WXSDKManagerHandler::GetChannelsLiveInfoCallback(System.String)
extern void WXSDKManagerHandler_GetChannelsLiveInfoCallback_m8E6BE4127E7ED953E36EE875DB3AD55B39D2D894 (void);
// 0x00000420 System.Void WeChatWASM.WXSDKManagerHandler::M(System.String,System.String)
extern void WXSDKManagerHandler_M_m73BF600C7B7FD1C8928C76B446EC70CF068F5B93 (void);
// 0x00000421 System.Void WeChatWASM.WXSDKManagerHandler::GetChannelsLiveInfo(WeChatWASM.GetChannelsLiveInfoOption)
extern void WXSDKManagerHandler_GetChannelsLiveInfo_mFEC764C69BFD437C868271C298164C6F12A45652 (void);
// 0x00000422 System.Void WeChatWASM.WXSDKManagerHandler::GetChannelsLiveNoticeInfoCallback(System.String)
extern void WXSDKManagerHandler_GetChannelsLiveNoticeInfoCallback_m5306288E30351C7BCF1E81547FC1576E64FC02F8 (void);
// 0x00000423 System.Void WeChatWASM.WXSDKManagerHandler::m(System.String,System.String)
extern void WXSDKManagerHandler_m_mDBB3E5701656230BB01D4748AA08695CF11F7ADF (void);
// 0x00000424 System.Void WeChatWASM.WXSDKManagerHandler::GetChannelsLiveNoticeInfo(WeChatWASM.GetChannelsLiveNoticeInfoOption)
extern void WXSDKManagerHandler_GetChannelsLiveNoticeInfo_m9BF668AD530C732A3E1EBEE0830C52D7DB2A7EA1 (void);
// 0x00000425 System.Void WeChatWASM.WXSDKManagerHandler::GetClipboardDataCallback(System.String)
extern void WXSDKManagerHandler_GetClipboardDataCallback_mBC05116D756A31E9AEF115EECBD09BC4A0C2A1B4 (void);
// 0x00000426 System.Void WeChatWASM.WXSDKManagerHandler::N(System.String,System.String)
extern void WXSDKManagerHandler_N_mEC253FB6A4FCE362FA77ED8FAE133058A73B39ED (void);
// 0x00000427 System.Void WeChatWASM.WXSDKManagerHandler::GetClipboardData(WeChatWASM.GetClipboardDataOption)
extern void WXSDKManagerHandler_GetClipboardData_m5472556E307D7B073BEA936CA187D2D5607118F2 (void);
// 0x00000428 System.Void WeChatWASM.WXSDKManagerHandler::GetConnectedBluetoothDevicesCallback(System.String)
extern void WXSDKManagerHandler_GetConnectedBluetoothDevicesCallback_m77812E2D48D25F03545ACB1A5E3D14C9E71DDA74 (void);
// 0x00000429 System.Void WeChatWASM.WXSDKManagerHandler::n(System.String,System.String)
extern void WXSDKManagerHandler_n_m26A6A0D5D99DA2B54CCED7D56625F5DFB4EE24F7 (void);
// 0x0000042A System.Void WeChatWASM.WXSDKManagerHandler::GetConnectedBluetoothDevices(WeChatWASM.GetConnectedBluetoothDevicesOption)
extern void WXSDKManagerHandler_GetConnectedBluetoothDevices_m2E5624B35160A28D840BF1FD1ACAF3ADFB9C28CB (void);
// 0x0000042B System.Void WeChatWASM.WXSDKManagerHandler::GetExtConfigCallback(System.String)
extern void WXSDKManagerHandler_GetExtConfigCallback_m3DEDA5B64738176F92261E63FC3B80D6DFB155BA (void);
// 0x0000042C System.Void WeChatWASM.WXSDKManagerHandler::O(System.String,System.String)
extern void WXSDKManagerHandler_O_m0684677CC35C324BC23E063E9272651ED5ED23B3 (void);
// 0x0000042D System.Void WeChatWASM.WXSDKManagerHandler::GetExtConfig(WeChatWASM.GetExtConfigOption)
extern void WXSDKManagerHandler_GetExtConfig_m894BFD20E89ED4BFC625748BC3FAF4C70B804BF5 (void);
// 0x0000042E System.Void WeChatWASM.WXSDKManagerHandler::GetGameClubDataCallback(System.String)
extern void WXSDKManagerHandler_GetGameClubDataCallback_m67B9370BA8D0CE0C0464B795535B8F44152C2CAD (void);
// 0x0000042F System.Void WeChatWASM.WXSDKManagerHandler::o(System.String,System.String)
extern void WXSDKManagerHandler_o_m924E71EB830887E70534AB61C90C96D8A9D57185 (void);
// 0x00000430 System.Void WeChatWASM.WXSDKManagerHandler::GetGameClubData(WeChatWASM.GetGameClubDataOption)
extern void WXSDKManagerHandler_GetGameClubData_m89839BFA486D4C3CB403D2365B0C90133BFD838F (void);
// 0x00000431 System.Void WeChatWASM.WXSDKManagerHandler::GetGroupEnterInfoCallback(System.String)
extern void WXSDKManagerHandler_GetGroupEnterInfoCallback_m8BF3F284CC0C2EDDEB73F775E5288935A8232CF3 (void);
// 0x00000432 System.Void WeChatWASM.WXSDKManagerHandler::P(System.String,System.String)
extern void WXSDKManagerHandler_P_m2B1408E5E71FF2FE1FA08D37E0C29B61A77ECEE2 (void);
// 0x00000433 System.Void WeChatWASM.WXSDKManagerHandler::GetGroupEnterInfo(WeChatWASM.GetGroupEnterInfoOption)
extern void WXSDKManagerHandler_GetGroupEnterInfo_m113BC3AE3E054ADA900B4E069887F003F233678A (void);
// 0x00000434 System.Void WeChatWASM.WXSDKManagerHandler::GetInferenceEnvInfoCallback(System.String)
extern void WXSDKManagerHandler_GetInferenceEnvInfoCallback_m43DE363C84E18C004A3F3B46F2875F5D2EA4E010 (void);
// 0x00000435 System.Void WeChatWASM.WXSDKManagerHandler::p(System.String,System.String)
extern void WXSDKManagerHandler_p_mBFC819AC230F9B7836FD87FE33BB60D6A5E825D7 (void);
// 0x00000436 System.Void WeChatWASM.WXSDKManagerHandler::GetInferenceEnvInfo(WeChatWASM.GetInferenceEnvInfoOption)
extern void WXSDKManagerHandler_GetInferenceEnvInfo_m90F3C805FD6CFCB171C1D69F0D2C2F3F422F4D49 (void);
// 0x00000437 System.Void WeChatWASM.WXSDKManagerHandler::GetLocalIPAddressCallback(System.String)
extern void WXSDKManagerHandler_GetLocalIPAddressCallback_mD54687848AAD0B49B7B0EFB28F2539AAA8A5A46B (void);
// 0x00000438 System.Void WeChatWASM.WXSDKManagerHandler::Q(System.String,System.String)
extern void WXSDKManagerHandler_Q_m0AA1BBBDEAE654B05C43532441E57D2D2959B541 (void);
// 0x00000439 System.Void WeChatWASM.WXSDKManagerHandler::GetLocalIPAddress(WeChatWASM.GetLocalIPAddressOption)
extern void WXSDKManagerHandler_GetLocalIPAddress_mC7CA3C25A2447F9D66FBC9984D5047D1050DC9C7 (void);
// 0x0000043A System.Void WeChatWASM.WXSDKManagerHandler::GetLocationCallback(System.String)
extern void WXSDKManagerHandler_GetLocationCallback_m77761B1CB80F1C154A4A2666297EE68A58402C37 (void);
// 0x0000043B System.Void WeChatWASM.WXSDKManagerHandler::q(System.String,System.String)
extern void WXSDKManagerHandler_q_m2ED81D2D35AE686951841316AC566944C8801063 (void);
// 0x0000043C System.Void WeChatWASM.WXSDKManagerHandler::GetLocation(WeChatWASM.GetLocationOption)
extern void WXSDKManagerHandler_GetLocation_m979940B142AB5C389B375A91D7DA520CFD529125 (void);
// 0x0000043D System.Void WeChatWASM.WXSDKManagerHandler::GetNetworkTypeCallback(System.String)
extern void WXSDKManagerHandler_GetNetworkTypeCallback_mA3E939A65901A25C308CEE63E79DF481969C7F5D (void);
// 0x0000043E System.Void WeChatWASM.WXSDKManagerHandler::R(System.String,System.String)
extern void WXSDKManagerHandler_R_mAFD5C7A9C937CBB6FDD85C8C9DF7D6BB6422E08A (void);
// 0x0000043F System.Void WeChatWASM.WXSDKManagerHandler::GetNetworkType(WeChatWASM.GetNetworkTypeOption)
extern void WXSDKManagerHandler_GetNetworkType_mED4BB255951D163132499F726828F3BB4B893D63 (void);
// 0x00000440 System.Void WeChatWASM.WXSDKManagerHandler::GetPrivacySettingCallback(System.String)
extern void WXSDKManagerHandler_GetPrivacySettingCallback_m41C9CB32696AF2A9C445692711A4F9D64E4AE52C (void);
// 0x00000441 System.Void WeChatWASM.WXSDKManagerHandler::r(System.String,System.String)
extern void WXSDKManagerHandler_r_mD7973A1B23E30898CFC76209EE12DF5C9EAA6E3C (void);
// 0x00000442 System.Void WeChatWASM.WXSDKManagerHandler::GetPrivacySetting(WeChatWASM.GetPrivacySettingOption)
extern void WXSDKManagerHandler_GetPrivacySetting_m3C46E2ADA953BEFABF40D067EFBA9D19839F4235 (void);
// 0x00000443 System.Void WeChatWASM.WXSDKManagerHandler::GetScreenBrightnessCallback(System.String)
extern void WXSDKManagerHandler_GetScreenBrightnessCallback_m8987F1F6C36B4E7FC2CBA95483394EF11385EA07 (void);
// 0x00000444 System.Void WeChatWASM.WXSDKManagerHandler::S(System.String,System.String)
extern void WXSDKManagerHandler_S_m29AF791F83E1625662C0FB2A8B6C3CF2E7F807AF (void);
// 0x00000445 System.Void WeChatWASM.WXSDKManagerHandler::GetScreenBrightness(WeChatWASM.GetScreenBrightnessOption)
extern void WXSDKManagerHandler_GetScreenBrightness_mC95E4F5D70A6CA77F1404366C77E8E221ACAE5DA (void);
// 0x00000446 System.Void WeChatWASM.WXSDKManagerHandler::GetSettingCallback(System.String)
extern void WXSDKManagerHandler_GetSettingCallback_m989F3B898C2948931C332CE7537F41D1B49B26D1 (void);
// 0x00000447 System.Void WeChatWASM.WXSDKManagerHandler::s(System.String,System.String)
extern void WXSDKManagerHandler_s_m6828271ED68351B34003A618AC047F5C58D9423E (void);
// 0x00000448 System.Void WeChatWASM.WXSDKManagerHandler::GetSetting(WeChatWASM.GetSettingOption)
extern void WXSDKManagerHandler_GetSetting_m7B02E3B76717911CBB487F5976C0678C242385DA (void);
// 0x00000449 System.Void WeChatWASM.WXSDKManagerHandler::GetShareInfoCallback(System.String)
extern void WXSDKManagerHandler_GetShareInfoCallback_mC635911F387CA72D397F009A5C6A097AD3377493 (void);
// 0x0000044A System.Void WeChatWASM.WXSDKManagerHandler::T(System.String,System.String)
extern void WXSDKManagerHandler_T_mD8B8F13A2DD5F4EB21D0D4F39AB4BFACF8137E13 (void);
// 0x0000044B System.Void WeChatWASM.WXSDKManagerHandler::GetShareInfo(WeChatWASM.GetShareInfoOption)
extern void WXSDKManagerHandler_GetShareInfo_m33CF859B78EA955F5778D20858BC3F0391CD9A56 (void);
// 0x0000044C System.Void WeChatWASM.WXSDKManagerHandler::GetStorageInfoCallback(System.String)
extern void WXSDKManagerHandler_GetStorageInfoCallback_mB9EEED332AD2AF346361D9E41FC8262C9F6CF540 (void);
// 0x0000044D System.Void WeChatWASM.WXSDKManagerHandler::t(System.String,System.String)
extern void WXSDKManagerHandler_t_mA986484771914B285BB61593326943EBFEF4A76A (void);
// 0x0000044E System.Void WeChatWASM.WXSDKManagerHandler::GetStorageInfo(WeChatWASM.GetStorageInfoOption)
extern void WXSDKManagerHandler_GetStorageInfo_mD7DD5F0522893C29CC35ECB67305B4C8F24E86C5 (void);
// 0x0000044F System.Void WeChatWASM.WXSDKManagerHandler::GetSystemInfoCallback(System.String)
extern void WXSDKManagerHandler_GetSystemInfoCallback_m5AD9B149BDBB1298B9708DF44AB9323849F90011 (void);
// 0x00000450 System.Void WeChatWASM.WXSDKManagerHandler::U(System.String,System.String)
extern void WXSDKManagerHandler_U_m7592DF329C8CCDE134BAAA09EF054C3BAB07DBFD (void);
// 0x00000451 System.Void WeChatWASM.WXSDKManagerHandler::GetSystemInfo(WeChatWASM.GetSystemInfoOption)
extern void WXSDKManagerHandler_GetSystemInfo_m0E9225EEF0D53184FEB52EFFC41F84565725FB31 (void);
// 0x00000452 System.Void WeChatWASM.WXSDKManagerHandler::GetSystemInfoAsyncCallback(System.String)
extern void WXSDKManagerHandler_GetSystemInfoAsyncCallback_m0CA117D3B9B8CFA1D55272A8CEAFA83E722FF486 (void);
// 0x00000453 System.Void WeChatWASM.WXSDKManagerHandler::u(System.String,System.String)
extern void WXSDKManagerHandler_u_mC586A5FF4609FCF490C66BDD6C5467663004DB98 (void);
// 0x00000454 System.Void WeChatWASM.WXSDKManagerHandler::GetSystemInfoAsync(WeChatWASM.GetSystemInfoAsyncOption)
extern void WXSDKManagerHandler_GetSystemInfoAsync_m42341E72B567844235AF0A0C175F6CD9095B1143 (void);
// 0x00000455 System.Void WeChatWASM.WXSDKManagerHandler::GetUserInfoCallback(System.String)
extern void WXSDKManagerHandler_GetUserInfoCallback_m3B67B2E07A09A61F61F1563C37FE255F171BA9CE (void);
// 0x00000456 System.Void WeChatWASM.WXSDKManagerHandler::V(System.String,System.String)
extern void WXSDKManagerHandler_V_m33890476C03B5220212584F65464DF29535FE399 (void);
// 0x00000457 System.Void WeChatWASM.WXSDKManagerHandler::GetUserInfo(WeChatWASM.GetUserInfoOption)
extern void WXSDKManagerHandler_GetUserInfo_m4F937F33D57A112AF3D88686329A216D0187EDD1 (void);
// 0x00000458 System.Void WeChatWASM.WXSDKManagerHandler::GetUserInteractiveStorageCallback(System.String)
extern void WXSDKManagerHandler_GetUserInteractiveStorageCallback_m2F4C47686B11365A618D5DCF6962211AF014CF28 (void);
// 0x00000459 System.Void WeChatWASM.WXSDKManagerHandler::v(System.String,System.String)
extern void WXSDKManagerHandler_v_m2DE00E0A91F2016CF040246AB038AB1319B970F2 (void);
// 0x0000045A System.Void WeChatWASM.WXSDKManagerHandler::GetUserInteractiveStorage(WeChatWASM.GetUserInteractiveStorageOption)
extern void WXSDKManagerHandler_GetUserInteractiveStorage_mED49E8A76A2D58ED3DF76B64AF2B92012EE231E1 (void);
// 0x0000045B System.Void WeChatWASM.WXSDKManagerHandler::GetWeRunDataCallback(System.String)
extern void WXSDKManagerHandler_GetWeRunDataCallback_m1F04715220BBECE408430084B8AEF5A32E9890FA (void);
// 0x0000045C System.Void WeChatWASM.WXSDKManagerHandler::W(System.String,System.String)
extern void WXSDKManagerHandler_W_mC37346BB2E84FC2045A2A20CF92CAE76CAC9658E (void);
// 0x0000045D System.Void WeChatWASM.WXSDKManagerHandler::GetWeRunData(WeChatWASM.GetWeRunDataOption)
extern void WXSDKManagerHandler_GetWeRunData_m021C41FA1A4F7FDF45E0F6DE82EFE530E494813A (void);
// 0x0000045E System.Void WeChatWASM.WXSDKManagerHandler::HideKeyboardCallback(System.String)
extern void WXSDKManagerHandler_HideKeyboardCallback_mD978903D7BC5B2392EDFB71D51571757DA58AA68 (void);
// 0x0000045F System.Void WeChatWASM.WXSDKManagerHandler::w(System.String,System.String)
extern void WXSDKManagerHandler_w_m2009B33323A434E8275A4E746EC8D8A374FD39F9 (void);
// 0x00000460 System.Void WeChatWASM.WXSDKManagerHandler::HideKeyboard(WeChatWASM.HideKeyboardOption)
extern void WXSDKManagerHandler_HideKeyboard_mB753D4B94A0AD39403703D65D2A0BCA5AEA5CF93 (void);
// 0x00000461 System.Void WeChatWASM.WXSDKManagerHandler::HideLoadingCallback(System.String)
extern void WXSDKManagerHandler_HideLoadingCallback_m1240E594B171F5284E93311A0401FB7C767B8192 (void);
// 0x00000462 System.Void WeChatWASM.WXSDKManagerHandler::X(System.String,System.String)
extern void WXSDKManagerHandler_X_mF64D343AB1F98A3F564523B5E4059C1B30498120 (void);
// 0x00000463 System.Void WeChatWASM.WXSDKManagerHandler::HideLoading(WeChatWASM.HideLoadingOption)
extern void WXSDKManagerHandler_HideLoading_m0443D04528C7E20E0A27E6C2E32EAD53A391DB7A (void);
// 0x00000464 System.Void WeChatWASM.WXSDKManagerHandler::HideShareMenuCallback(System.String)
extern void WXSDKManagerHandler_HideShareMenuCallback_mD419B2A84542C299AD8E97573F8927158D867D7E (void);
// 0x00000465 System.Void WeChatWASM.WXSDKManagerHandler::x(System.String,System.String)
extern void WXSDKManagerHandler_x_mC3500B136222DDF2F24121C3DD6B3E087B09E064 (void);
// 0x00000466 System.Void WeChatWASM.WXSDKManagerHandler::HideShareMenu(WeChatWASM.HideShareMenuOption)
extern void WXSDKManagerHandler_HideShareMenu_m3088A1881330A64E7DCC7CE3B35E5EDEFDD1E631 (void);
// 0x00000467 System.Void WeChatWASM.WXSDKManagerHandler::HideToastCallback(System.String)
extern void WXSDKManagerHandler_HideToastCallback_m7DFEEC9A3786D2E10FB87F73022EBDC8B8B338C6 (void);
// 0x00000468 System.Void WeChatWASM.WXSDKManagerHandler::Y(System.String,System.String)
extern void WXSDKManagerHandler_Y_mF4FB5957FF3B03DE7E205A291C5A56A56600A257 (void);
// 0x00000469 System.Void WeChatWASM.WXSDKManagerHandler::HideToast(WeChatWASM.HideToastOption)
extern void WXSDKManagerHandler_HideToast_m0E015FAE5DDFABE390DFC87023A66B3B6B1010E2 (void);
// 0x0000046A System.Void WeChatWASM.WXSDKManagerHandler::InitFaceDetectCallback(System.String)
extern void WXSDKManagerHandler_InitFaceDetectCallback_m42ABBD03790F8B90EB4EA688A597FC8657B5E417 (void);
// 0x0000046B System.Void WeChatWASM.WXSDKManagerHandler::y(System.String,System.String)
extern void WXSDKManagerHandler_y_m196F68DF6BDD2764F32A9779B499FCA1466F5CDE (void);
// 0x0000046C System.Void WeChatWASM.WXSDKManagerHandler::InitFaceDetect(WeChatWASM.InitFaceDetectOption)
extern void WXSDKManagerHandler_InitFaceDetect_mB242C5581EDEFBFFE66A30739451F3B2A02D2E16 (void);
// 0x0000046D System.Void WeChatWASM.WXSDKManagerHandler::IsBluetoothDevicePairedCallback(System.String)
extern void WXSDKManagerHandler_IsBluetoothDevicePairedCallback_m698F406604F33255E111AF6A8769E6ABA8BE5E54 (void);
// 0x0000046E System.Void WeChatWASM.WXSDKManagerHandler::Z(System.String,System.String)
extern void WXSDKManagerHandler_Z_m046742AF0B48E476BB9A141EA03978BAC8FD6AC6 (void);
// 0x0000046F System.Void WeChatWASM.WXSDKManagerHandler::IsBluetoothDevicePaired(WeChatWASM.IsBluetoothDevicePairedOption)
extern void WXSDKManagerHandler_IsBluetoothDevicePaired_m74D6D108BB6849A4147C8D25677B697F970EDA7E (void);
// 0x00000470 System.Void WeChatWASM.WXSDKManagerHandler::JoinVoIPChatCallback(System.String)
extern void WXSDKManagerHandler_JoinVoIPChatCallback_m4EF8E8F33ABA130AE7869478578764446F4E838A (void);
// 0x00000471 System.Void WeChatWASM.WXSDKManagerHandler::z(System.String,System.String)
extern void WXSDKManagerHandler_z_mF414F43E3923642FCE179E839C05A906AEAC88BA (void);
// 0x00000472 System.Void WeChatWASM.WXSDKManagerHandler::JoinVoIPChat(WeChatWASM.JoinVoIPChatOption)
extern void WXSDKManagerHandler_JoinVoIPChat_m4FB725CB3D5EFAFF91009ACA9A8BAD7BE9066CC2 (void);
// 0x00000473 System.Void WeChatWASM.WXSDKManagerHandler::LoginCallback(System.String)
extern void WXSDKManagerHandler_LoginCallback_mD36D507015DF95F83A866CB317183AFFF07DA07C (void);
// 0x00000474 System.Void WeChatWASM.WXSDKManagerHandler::aA(System.String,System.String)
extern void WXSDKManagerHandler_aA_mE0EDBF877556F9D84BBA8FB3FA6EF5977402B3B2 (void);
// 0x00000475 System.Void WeChatWASM.WXSDKManagerHandler::Login(WeChatWASM.LoginOption)
extern void WXSDKManagerHandler_Login_m7C7FAB035B0B17C892EC8D1D6F6086AD78B3B996 (void);
// 0x00000476 System.Void WeChatWASM.WXSDKManagerHandler::MakeBluetoothPairCallback(System.String)
extern void WXSDKManagerHandler_MakeBluetoothPairCallback_m523D0EF99BCD76671A8116453EF57D33B1740D67 (void);
// 0x00000477 System.Void WeChatWASM.WXSDKManagerHandler::aa(System.String,System.String)
extern void WXSDKManagerHandler_aa_mD15A12EECE3757C191D5CFBF5CFF9617D3A423D6 (void);
// 0x00000478 System.Void WeChatWASM.WXSDKManagerHandler::MakeBluetoothPair(WeChatWASM.MakeBluetoothPairOption)
extern void WXSDKManagerHandler_MakeBluetoothPair_mBAF7180401248CD0F463219571854E61E8B617B8 (void);
// 0x00000479 System.Void WeChatWASM.WXSDKManagerHandler::NavigateToMiniProgramCallback(System.String)
extern void WXSDKManagerHandler_NavigateToMiniProgramCallback_m622145989C8838ED3FCC7692EC78CFDD80CCA813 (void);
// 0x0000047A System.Void WeChatWASM.WXSDKManagerHandler::aB(System.String,System.String)
extern void WXSDKManagerHandler_aB_mE8A45E26485788E3822A8AC828B54209E853D82D (void);
// 0x0000047B System.Void WeChatWASM.WXSDKManagerHandler::NavigateToMiniProgram(WeChatWASM.NavigateToMiniProgramOption)
extern void WXSDKManagerHandler_NavigateToMiniProgram_m13C999C5F1A732436025EBDC75129597E7837252 (void);
// 0x0000047C System.Void WeChatWASM.WXSDKManagerHandler::NotifyBLECharacteristicValueChangeCallback(System.String)
extern void WXSDKManagerHandler_NotifyBLECharacteristicValueChangeCallback_mE1AC58B4724801D0324EBCD6766A0D97C80DC440 (void);
// 0x0000047D System.Void WeChatWASM.WXSDKManagerHandler::ab(System.String,System.String)
extern void WXSDKManagerHandler_ab_m74BD3D38A41A1DD35ED6913D3562BDCB0BE6AF5B (void);
// 0x0000047E System.Void WeChatWASM.WXSDKManagerHandler::NotifyBLECharacteristicValueChange(WeChatWASM.NotifyBLECharacteristicValueChangeOption)
extern void WXSDKManagerHandler_NotifyBLECharacteristicValueChange_m03EC6F44E3D791CE3B09060B8C16C9644F65A112 (void);
// 0x0000047F System.Void WeChatWASM.WXSDKManagerHandler::OpenAppAuthorizeSettingCallback(System.String)
extern void WXSDKManagerHandler_OpenAppAuthorizeSettingCallback_m57E9E7A0673E72331B4DEE39FC34BFC201258052 (void);
// 0x00000480 System.Void WeChatWASM.WXSDKManagerHandler::aC(System.String,System.String)
extern void WXSDKManagerHandler_aC_m55CD47754DFA9AE3E3422B82CC7CC00585C45E1E (void);
// 0x00000481 System.Void WeChatWASM.WXSDKManagerHandler::OpenAppAuthorizeSetting(WeChatWASM.OpenAppAuthorizeSettingOption)
extern void WXSDKManagerHandler_OpenAppAuthorizeSetting_mCA5C24FE86AEC1522AE905A574D915C586FB1A2C (void);
// 0x00000482 System.Void WeChatWASM.WXSDKManagerHandler::OpenBluetoothAdapterCallback(System.String)
extern void WXSDKManagerHandler_OpenBluetoothAdapterCallback_m8CDD98BD1B70BD4A184556F31A6409F5E8ED7863 (void);
// 0x00000483 System.Void WeChatWASM.WXSDKManagerHandler::ac(System.String,System.String)
extern void WXSDKManagerHandler_ac_mE54DD3458BDA3DDD9A9BEEF7A154C706DB689FFA (void);
// 0x00000484 System.Void WeChatWASM.WXSDKManagerHandler::OpenBluetoothAdapter(WeChatWASM.OpenBluetoothAdapterOption)
extern void WXSDKManagerHandler_OpenBluetoothAdapter_mCE7AE6F2FED2B94594EC88587FBA6333E7E6E389 (void);
// 0x00000485 System.Void WeChatWASM.WXSDKManagerHandler::OpenCardCallback(System.String)
extern void WXSDKManagerHandler_OpenCardCallback_m724A55CD53AC4DA66B2642EC5176E7AD76AF4E89 (void);
// 0x00000486 System.Void WeChatWASM.WXSDKManagerHandler::aD(System.String,System.String)
extern void WXSDKManagerHandler_aD_m17E7DE497B0E5B16CAD5839E17002CAD80E6BABF (void);
// 0x00000487 System.Void WeChatWASM.WXSDKManagerHandler::OpenCard(WeChatWASM.OpenCardOption)
extern void WXSDKManagerHandler_OpenCard_m879E7A3050D3A272E9C733DCB0B112F416C8DCB3 (void);
// 0x00000488 System.Void WeChatWASM.WXSDKManagerHandler::OpenChannelsActivityCallback(System.String)
extern void WXSDKManagerHandler_OpenChannelsActivityCallback_m25F40676CB710F32EB08795FDC014BD98DB22DBB (void);
// 0x00000489 System.Void WeChatWASM.WXSDKManagerHandler::ad(System.String,System.String)
extern void WXSDKManagerHandler_ad_m2E8B91DC0F5453979F42A8FA979BD7D8F64889EF (void);
// 0x0000048A System.Void WeChatWASM.WXSDKManagerHandler::OpenChannelsActivity(WeChatWASM.OpenChannelsActivityOption)
extern void WXSDKManagerHandler_OpenChannelsActivity_m1F94C917A94F75D5AAA20411770C11AFD0CF2CFC (void);
// 0x0000048B System.Void WeChatWASM.WXSDKManagerHandler::OpenChannelsEventCallback(System.String)
extern void WXSDKManagerHandler_OpenChannelsEventCallback_m0E0FDF81127777780B7DD065B4A6B4F2C660FD49 (void);
// 0x0000048C System.Void WeChatWASM.WXSDKManagerHandler::aE(System.String,System.String)
extern void WXSDKManagerHandler_aE_mB07B9389D1E36D473A9F8FF0056F2B6819CF0135 (void);
// 0x0000048D System.Void WeChatWASM.WXSDKManagerHandler::OpenChannelsEvent(WeChatWASM.OpenChannelsEventOption)
extern void WXSDKManagerHandler_OpenChannelsEvent_m65D68A273540E7DCB760A504B941C56B976E31CD (void);
// 0x0000048E System.Void WeChatWASM.WXSDKManagerHandler::OpenChannelsLiveCallback(System.String)
extern void WXSDKManagerHandler_OpenChannelsLiveCallback_mA69D1C9CB69C0FD4CCCD5D1F3D2F5847944044A5 (void);
// 0x0000048F System.Void WeChatWASM.WXSDKManagerHandler::ae(System.String,System.String)
extern void WXSDKManagerHandler_ae_mB0898F959107D1F49277796467E308622D9E9BCA (void);
// 0x00000490 System.Void WeChatWASM.WXSDKManagerHandler::OpenChannelsLive(WeChatWASM.OpenChannelsLiveOption)
extern void WXSDKManagerHandler_OpenChannelsLive_mC750A4D4FE4D78DEF6395E03131BC4AB5B38AAEF (void);
// 0x00000491 System.Void WeChatWASM.WXSDKManagerHandler::OpenChannelsUserProfileCallback(System.String)
extern void WXSDKManagerHandler_OpenChannelsUserProfileCallback_m1035C8B4BBF4D785FA16F550FA4AFFDD9C039D28 (void);
// 0x00000492 System.Void WeChatWASM.WXSDKManagerHandler::aF(System.String,System.String)
extern void WXSDKManagerHandler_aF_m768DC7A39A2E130351AD39448BDD6ABB29C2EF92 (void);
// 0x00000493 System.Void WeChatWASM.WXSDKManagerHandler::OpenChannelsUserProfile(WeChatWASM.OpenChannelsUserProfileOption)
extern void WXSDKManagerHandler_OpenChannelsUserProfile_m01DE2A9258CC08B83A9B2BAFCA76A56B2A571D81 (void);
// 0x00000494 System.Void WeChatWASM.WXSDKManagerHandler::OpenCustomerServiceChatCallback(System.String)
extern void WXSDKManagerHandler_OpenCustomerServiceChatCallback_m5611CAFF0D03269BBB784ADD5AB4A3D07C0B5D86 (void);
// 0x00000495 System.Void WeChatWASM.WXSDKManagerHandler::af(System.String,System.String)
extern void WXSDKManagerHandler_af_m32B3DDEDB3100A3F527CF7EAB93C3539EAD1EE3D (void);
// 0x00000496 System.Void WeChatWASM.WXSDKManagerHandler::OpenCustomerServiceChat(WeChatWASM.OpenCustomerServiceChatOption)
extern void WXSDKManagerHandler_OpenCustomerServiceChat_m3F563644F36227DA258C50ADA80FD1E491389D3D (void);
// 0x00000497 System.Void WeChatWASM.WXSDKManagerHandler::OpenCustomerServiceConversationCallback(System.String)
extern void WXSDKManagerHandler_OpenCustomerServiceConversationCallback_mA1AD8D758D5C61BAED04F34181D95522533518AC (void);
// 0x00000498 System.Void WeChatWASM.WXSDKManagerHandler::aG(System.String,System.String)
extern void WXSDKManagerHandler_aG_mDA55B5FE41E86E0C5126E489D2B157CE7AD4BA6D (void);
// 0x00000499 System.Void WeChatWASM.WXSDKManagerHandler::OpenCustomerServiceConversation(WeChatWASM.OpenCustomerServiceConversationOption)
extern void WXSDKManagerHandler_OpenCustomerServiceConversation_m46049B1AB5BFAA9F127C2D835CDDA6D4052FE1E9 (void);
// 0x0000049A System.Void WeChatWASM.WXSDKManagerHandler::OpenPrivacyContractCallback(System.String)
extern void WXSDKManagerHandler_OpenPrivacyContractCallback_m2A11D62D2F3E7C4CEF325871122060F37B85EF40 (void);
// 0x0000049B System.Void WeChatWASM.WXSDKManagerHandler::ag(System.String,System.String)
extern void WXSDKManagerHandler_ag_mAF62D8B3DBA738C49D7DC5D7E0E898872CD9692F (void);
// 0x0000049C System.Void WeChatWASM.WXSDKManagerHandler::OpenPrivacyContract(WeChatWASM.OpenPrivacyContractOption)
extern void WXSDKManagerHandler_OpenPrivacyContract_m01C142E6E0544EEEBC3AAEEE3DEC81CBC1D61E78 (void);
// 0x0000049D System.Void WeChatWASM.WXSDKManagerHandler::OpenSettingCallback(System.String)
extern void WXSDKManagerHandler_OpenSettingCallback_mE54A1CD613F0C1252CBB8CEEC7486028F0E4D61A (void);
// 0x0000049E System.Void WeChatWASM.WXSDKManagerHandler::aH(System.String,System.String)
extern void WXSDKManagerHandler_aH_m598E6B661527C370F627E25501E5FFFAA0B3FF20 (void);
// 0x0000049F System.Void WeChatWASM.WXSDKManagerHandler::OpenSetting(WeChatWASM.OpenSettingOption)
extern void WXSDKManagerHandler_OpenSetting_m9AEC5E0A46C02EB99D90FCDF274203473B40A001 (void);
// 0x000004A0 System.Void WeChatWASM.WXSDKManagerHandler::OpenSystemBluetoothSettingCallback(System.String)
extern void WXSDKManagerHandler_OpenSystemBluetoothSettingCallback_m0F9E0B340B947B14FA1FBFF6761C12C06B7460C2 (void);
// 0x000004A1 System.Void WeChatWASM.WXSDKManagerHandler::ah(System.String,System.String)
extern void WXSDKManagerHandler_ah_m830BA29B87F8CDCBF1831E1E8E51D70CBD16BE65 (void);
// 0x000004A2 System.Void WeChatWASM.WXSDKManagerHandler::OpenSystemBluetoothSetting(WeChatWASM.OpenSystemBluetoothSettingOption)
extern void WXSDKManagerHandler_OpenSystemBluetoothSetting_m25775D3702FF2C5991D04CF4693BF57ADE606828 (void);
// 0x000004A3 System.Void WeChatWASM.WXSDKManagerHandler::PreviewImageCallback(System.String)
extern void WXSDKManagerHandler_PreviewImageCallback_m4F31AD81A378601766B808B43723BCE60500B6A1 (void);
// 0x000004A4 System.Void WeChatWASM.WXSDKManagerHandler::aI(System.String,System.String)
extern void WXSDKManagerHandler_aI_m23D54F838523AC77170B4DE64CF49126535E18C1 (void);
// 0x000004A5 System.Void WeChatWASM.WXSDKManagerHandler::PreviewImage(WeChatWASM.PreviewImageOption)
extern void WXSDKManagerHandler_PreviewImage_mAD6331B440447C8E3AE6240457116DBE0E29DF04 (void);
// 0x000004A6 System.Void WeChatWASM.WXSDKManagerHandler::PreviewMediaCallback(System.String)
extern void WXSDKManagerHandler_PreviewMediaCallback_mC9A2BE1FAE50F04F4DA5734893C74D36B6B211D4 (void);
// 0x000004A7 System.Void WeChatWASM.WXSDKManagerHandler::ai(System.String,System.String)
extern void WXSDKManagerHandler_ai_m4A623E10B9C18EB08AC721BFF9735A3D6076391F (void);
// 0x000004A8 System.Void WeChatWASM.WXSDKManagerHandler::PreviewMedia(WeChatWASM.PreviewMediaOption)
extern void WXSDKManagerHandler_PreviewMedia_m41EAB743A1FBA5177ADC5883F3CCA50A98E8AEA1 (void);
// 0x000004A9 System.Void WeChatWASM.WXSDKManagerHandler::ReadBLECharacteristicValueCallback(System.String)
extern void WXSDKManagerHandler_ReadBLECharacteristicValueCallback_mC491B3EF0C7CB91AA52820C4BC6D4EFA9E8DBB16 (void);
// 0x000004AA System.Void WeChatWASM.WXSDKManagerHandler::aJ(System.String,System.String)
extern void WXSDKManagerHandler_aJ_m429364A8D28D50DC6F46CE099E5DEE50E23AE655 (void);
// 0x000004AB System.Void WeChatWASM.WXSDKManagerHandler::ReadBLECharacteristicValue(WeChatWASM.ReadBLECharacteristicValueOption)
extern void WXSDKManagerHandler_ReadBLECharacteristicValue_mF53D36B71F88B956DA8C8E59D722DCB00D38BE97 (void);
// 0x000004AC System.Void WeChatWASM.WXSDKManagerHandler::RemoveStorageCallback(System.String)
extern void WXSDKManagerHandler_RemoveStorageCallback_m8DB4376399791AE842734E0316DAAB159913AA75 (void);
// 0x000004AD System.Void WeChatWASM.WXSDKManagerHandler::aj(System.String,System.String)
extern void WXSDKManagerHandler_aj_m71B6136D8820AEE04421F1A9ABEBB8702B880F51 (void);
// 0x000004AE System.Void WeChatWASM.WXSDKManagerHandler::RemoveStorage(WeChatWASM.RemoveStorageOption)
extern void WXSDKManagerHandler_RemoveStorage_mCF939B7365C1A11C628CDD8A1BB1077A2A23A8D5 (void);
// 0x000004AF System.Void WeChatWASM.WXSDKManagerHandler::RemoveUserCloudStorageCallback(System.String)
extern void WXSDKManagerHandler_RemoveUserCloudStorageCallback_m22BC0AB635F08C25E0E905CB29030739FEBF9269 (void);
// 0x000004B0 System.Void WeChatWASM.WXSDKManagerHandler::aK(System.String,System.String)
extern void WXSDKManagerHandler_aK_mC2F0B76B236793DB0787316E66B055665C134B4C (void);
// 0x000004B1 System.Void WeChatWASM.WXSDKManagerHandler::RemoveUserCloudStorage(WeChatWASM.RemoveUserCloudStorageOption)
extern void WXSDKManagerHandler_RemoveUserCloudStorage_mFA4B1131A13BE834A8C4E000576A78EB709F9D2B (void);
// 0x000004B2 System.Void WeChatWASM.WXSDKManagerHandler::ReportSceneCallback(System.String)
extern void WXSDKManagerHandler_ReportSceneCallback_mA1C3FA5B9582A84FCCDF0D03FD6B784C91033E22 (void);
// 0x000004B3 System.Void WeChatWASM.WXSDKManagerHandler::ak(System.String,System.String)
extern void WXSDKManagerHandler_ak_mAC36648F4DEECEA65D762CACE127B261FB51FB2D (void);
// 0x000004B4 System.Void WeChatWASM.WXSDKManagerHandler::ReportScene(WeChatWASM.ReportSceneOption)
extern void WXSDKManagerHandler_ReportScene_m6B5F743A26CE7CE787169E7E2D883F30C58A1EE3 (void);
// 0x000004B5 System.Void WeChatWASM.WXSDKManagerHandler::RequestMidasFriendPaymentCallback(System.String)
extern void WXSDKManagerHandler_RequestMidasFriendPaymentCallback_m1B9A8A442CE5C86081ADA2358BC36DF3E7B71F6E (void);
// 0x000004B6 System.Void WeChatWASM.WXSDKManagerHandler::aL(System.String,System.String)
extern void WXSDKManagerHandler_aL_m0AE256681ABD8059113DC7976F83399C2C129077 (void);
// 0x000004B7 System.Void WeChatWASM.WXSDKManagerHandler::RequestMidasFriendPayment(WeChatWASM.RequestMidasFriendPaymentOption)
extern void WXSDKManagerHandler_RequestMidasFriendPayment_m548211F94007F7A8A83ECF1C42B3A1EAE66AA8DC (void);
// 0x000004B8 System.Void WeChatWASM.WXSDKManagerHandler::RequestMidasPaymentCallback(System.String)
extern void WXSDKManagerHandler_RequestMidasPaymentCallback_mF1CA3E30E8E5FB9B2806D01F819BB712EEB387DC (void);
// 0x000004B9 System.Void WeChatWASM.WXSDKManagerHandler::al(System.String,System.String)
extern void WXSDKManagerHandler_al_mDCC0C275CF3FB3F917EC7298C8B56EA871655958 (void);
// 0x000004BA System.Void WeChatWASM.WXSDKManagerHandler::RequestMidasPayment(WeChatWASM.RequestMidasPaymentOption)
extern void WXSDKManagerHandler_RequestMidasPayment_m43AED6CC79D8378AF03381D63D4B563C96A0DA89 (void);
// 0x000004BB System.Void WeChatWASM.WXSDKManagerHandler::RequestSubscribeMessageCallback(System.String)
extern void WXSDKManagerHandler_RequestSubscribeMessageCallback_mEADB5B8C1889C3E02B72E4F64052BF3B5FC45FF1 (void);
// 0x000004BC System.Void WeChatWASM.WXSDKManagerHandler::aM(System.String,System.String)
extern void WXSDKManagerHandler_aM_mCC21BCCF7AC7C022B29662C4A6F1F9062EDBEDFD (void);
// 0x000004BD System.Void WeChatWASM.WXSDKManagerHandler::RequestSubscribeMessage(WeChatWASM.RequestSubscribeMessageOption)
extern void WXSDKManagerHandler_RequestSubscribeMessage_m881E791B57DB18DA8A330AFD24482CE5EFBA07B5 (void);
// 0x000004BE System.Void WeChatWASM.WXSDKManagerHandler::RequestSubscribeSystemMessageCallback(System.String)
extern void WXSDKManagerHandler_RequestSubscribeSystemMessageCallback_m16D4CA6E279FC416F183081B934158B93D710B74 (void);
// 0x000004BF System.Void WeChatWASM.WXSDKManagerHandler::am(System.String,System.String)
extern void WXSDKManagerHandler_am_m812664FC348C1867DEC894A0E7559ED31952964D (void);
// 0x000004C0 System.Void WeChatWASM.WXSDKManagerHandler::RequestSubscribeSystemMessage(WeChatWASM.RequestSubscribeSystemMessageOption)
extern void WXSDKManagerHandler_RequestSubscribeSystemMessage_m24A9738F380306EC3C6BACAFC4F3F5591721056E (void);
// 0x000004C1 System.Void WeChatWASM.WXSDKManagerHandler::RequirePrivacyAuthorizeCallback(System.String)
extern void WXSDKManagerHandler_RequirePrivacyAuthorizeCallback_m558DF92ED713BFA2DADF3A9DF5FF1463ADA7E068 (void);
// 0x000004C2 System.Void WeChatWASM.WXSDKManagerHandler::aN(System.String,System.String)
extern void WXSDKManagerHandler_aN_mC6506C54BE34110E429CF06B96666ED0C46BEACC (void);
// 0x000004C3 System.Void WeChatWASM.WXSDKManagerHandler::RequirePrivacyAuthorize(WeChatWASM.RequirePrivacyAuthorizeOption)
extern void WXSDKManagerHandler_RequirePrivacyAuthorize_m4418D8959395CB75ED5901255C4AC5DA0A16C4CE (void);
// 0x000004C4 System.Void WeChatWASM.WXSDKManagerHandler::RestartMiniProgramCallback(System.String)
extern void WXSDKManagerHandler_RestartMiniProgramCallback_mB5A1D925BFE96D960B7DDB7220C38A46436B2D8C (void);
// 0x000004C5 System.Void WeChatWASM.WXSDKManagerHandler::an(System.String,System.String)
extern void WXSDKManagerHandler_an_mB73BC5FD5C5EF057679D6AA82D125CF20D53E034 (void);
// 0x000004C6 System.Void WeChatWASM.WXSDKManagerHandler::RestartMiniProgram(WeChatWASM.RestartMiniProgramOption)
extern void WXSDKManagerHandler_RestartMiniProgram_mD69C210A3AA74A3C6DE5C51DE5D2D2530F8A548D (void);
// 0x000004C7 System.Void WeChatWASM.WXSDKManagerHandler::SaveFileToDiskCallback(System.String)
extern void WXSDKManagerHandler_SaveFileToDiskCallback_mB6415BAEE01704AAC120FA55D96052E4D42685FF (void);
// 0x000004C8 System.Void WeChatWASM.WXSDKManagerHandler::aO(System.String,System.String)
extern void WXSDKManagerHandler_aO_m334D8FA00310F476514F6FFD2E4C0BC9601FA970 (void);
// 0x000004C9 System.Void WeChatWASM.WXSDKManagerHandler::SaveFileToDisk(WeChatWASM.SaveFileToDiskOption)
extern void WXSDKManagerHandler_SaveFileToDisk_mC837E5854D4124B82724D30AFCBE48939CC9A320 (void);
// 0x000004CA System.Void WeChatWASM.WXSDKManagerHandler::SaveImageToPhotosAlbumCallback(System.String)
extern void WXSDKManagerHandler_SaveImageToPhotosAlbumCallback_mA66869782E89E66649E83FB21F2CCD51C501AC2B (void);
// 0x000004CB System.Void WeChatWASM.WXSDKManagerHandler::ao(System.String,System.String)
extern void WXSDKManagerHandler_ao_mBD17D0F8BAA772CC40F179925BC5EEE5C43B9A62 (void);
// 0x000004CC System.Void WeChatWASM.WXSDKManagerHandler::SaveImageToPhotosAlbum(WeChatWASM.SaveImageToPhotosAlbumOption)
extern void WXSDKManagerHandler_SaveImageToPhotosAlbum_m84834C7F52857CFF032AC17A7C29D336B917E64D (void);
// 0x000004CD System.Void WeChatWASM.WXSDKManagerHandler::ScanCodeCallback(System.String)
extern void WXSDKManagerHandler_ScanCodeCallback_m7D0672D687D025E8F9B7830D662BB43EE8D05409 (void);
// 0x000004CE System.Void WeChatWASM.WXSDKManagerHandler::aP(System.String,System.String)
extern void WXSDKManagerHandler_aP_m99B0907EED3A887E71B7BB373589ABD17A60D230 (void);
// 0x000004CF System.Void WeChatWASM.WXSDKManagerHandler::ScanCode(WeChatWASM.ScanCodeOption)
extern void WXSDKManagerHandler_ScanCode_m1D271DFEC44EB0F85FF187F25B936224270DA9BA (void);
// 0x000004D0 System.Void WeChatWASM.WXSDKManagerHandler::SetBLEMTUCallback(System.String)
extern void WXSDKManagerHandler_SetBLEMTUCallback_mD24E3EBC8DDDC485FB571B3278D52B8EF159D582 (void);
// 0x000004D1 System.Void WeChatWASM.WXSDKManagerHandler::ap(System.String,System.String)
extern void WXSDKManagerHandler_ap_mFA750E2372549E8FC9D920857678FCF6DD05528F (void);
// 0x000004D2 System.Void WeChatWASM.WXSDKManagerHandler::SetBLEMTU(WeChatWASM.SetBLEMTUOption)
extern void WXSDKManagerHandler_SetBLEMTU_m4B28677A6607A80566B197080A4A52DAC7458C3C (void);
// 0x000004D3 System.Void WeChatWASM.WXSDKManagerHandler::SetClipboardDataCallback(System.String)
extern void WXSDKManagerHandler_SetClipboardDataCallback_m7A02783DDA08FFB6ED4F49ABD317FD00F6768739 (void);
// 0x000004D4 System.Void WeChatWASM.WXSDKManagerHandler::aQ(System.String,System.String)
extern void WXSDKManagerHandler_aQ_mD958296311DDF89A0D180F577EA2BF40EF0BC7D6 (void);
// 0x000004D5 System.Void WeChatWASM.WXSDKManagerHandler::SetClipboardData(WeChatWASM.SetClipboardDataOption)
extern void WXSDKManagerHandler_SetClipboardData_m0EBA78E48C4C4DEB162799C78E26E5D591B2E58D (void);
// 0x000004D6 System.Void WeChatWASM.WXSDKManagerHandler::SetDeviceOrientationCallback(System.String)
extern void WXSDKManagerHandler_SetDeviceOrientationCallback_m44225ABE9A96A03155F82D034649BD74189653F4 (void);
// 0x000004D7 System.Void WeChatWASM.WXSDKManagerHandler::aq(System.String,System.String)
extern void WXSDKManagerHandler_aq_m1040AF398D2FA3700FB43DC8DE1E6150EAC8A64B (void);
// 0x000004D8 System.Void WeChatWASM.WXSDKManagerHandler::SetDeviceOrientation(WeChatWASM.SetDeviceOrientationOption)
extern void WXSDKManagerHandler_SetDeviceOrientation_mD9047CE0C2C7289F9DAF3BD352F7378E59DBD989 (void);
// 0x000004D9 System.Void WeChatWASM.WXSDKManagerHandler::SetEnableDebugCallback(System.String)
extern void WXSDKManagerHandler_SetEnableDebugCallback_m8A3AF344579609F4A858E937849169E5304730AA (void);
// 0x000004DA System.Void WeChatWASM.WXSDKManagerHandler::aR(System.String,System.String)
extern void WXSDKManagerHandler_aR_m454AE1DE47021AA9BFC524F7D1AA922C7FBF0659 (void);
// 0x000004DB System.Void WeChatWASM.WXSDKManagerHandler::SetEnableDebug(WeChatWASM.SetEnableDebugOption)
extern void WXSDKManagerHandler_SetEnableDebug_m57134913B18BB65387D8770098C84A4541AA2CFF (void);
// 0x000004DC System.Void WeChatWASM.WXSDKManagerHandler::SetInnerAudioOptionCallback(System.String)
extern void WXSDKManagerHandler_SetInnerAudioOptionCallback_mB072F0A5B25C3F2D5C2E954E9BC4638D6FA925CC (void);
// 0x000004DD System.Void WeChatWASM.WXSDKManagerHandler::ar(System.String,System.String)
extern void WXSDKManagerHandler_ar_m830874528A28426E9DDAF04E67F51C2AE0F9F65E (void);
// 0x000004DE System.Void WeChatWASM.WXSDKManagerHandler::SetInnerAudioOption(WeChatWASM.SetInnerAudioOption)
extern void WXSDKManagerHandler_SetInnerAudioOption_m61F05ED6B8053FB98B8D791B86CD2221D8AC8B8B (void);
// 0x000004DF System.Void WeChatWASM.WXSDKManagerHandler::SetKeepScreenOnCallback(System.String)
extern void WXSDKManagerHandler_SetKeepScreenOnCallback_mB85CA0864430FE4933568C062B28ED1B7E19E976 (void);
// 0x000004E0 System.Void WeChatWASM.WXSDKManagerHandler::aS(System.String,System.String)
extern void WXSDKManagerHandler_aS_m044D5999C8FB3990817E39B77735296028114A11 (void);
// 0x000004E1 System.Void WeChatWASM.WXSDKManagerHandler::SetKeepScreenOn(WeChatWASM.SetKeepScreenOnOption)
extern void WXSDKManagerHandler_SetKeepScreenOn_m0EF7CB786DFE67BCCD08889872992E702E1BC564 (void);
// 0x000004E2 System.Void WeChatWASM.WXSDKManagerHandler::SetMenuStyleCallback(System.String)
extern void WXSDKManagerHandler_SetMenuStyleCallback_mE7FBDF1C8098F1ECDCC3C7D3E6D139C728F8219D (void);
// 0x000004E3 System.Void WeChatWASM.WXSDKManagerHandler::as(System.String,System.String)
extern void WXSDKManagerHandler_as_m01B6D46C51318DB4F0863D5CD0DF808DBDDFE727 (void);
// 0x000004E4 System.Void WeChatWASM.WXSDKManagerHandler::SetMenuStyle(WeChatWASM.SetMenuStyleOption)
extern void WXSDKManagerHandler_SetMenuStyle_m23D1761E926A9A31C4A684CB86B7BC030D906BFC (void);
// 0x000004E5 System.Void WeChatWASM.WXSDKManagerHandler::SetScreenBrightnessCallback(System.String)
extern void WXSDKManagerHandler_SetScreenBrightnessCallback_m429829C64D4A30E10A2FA66D888B6E49F02C1EAF (void);
// 0x000004E6 System.Void WeChatWASM.WXSDKManagerHandler::aT(System.String,System.String)
extern void WXSDKManagerHandler_aT_m890F7E9AE6D80B09396C312F2A540C4E2E1300C6 (void);
// 0x000004E7 System.Void WeChatWASM.WXSDKManagerHandler::SetScreenBrightness(WeChatWASM.SetScreenBrightnessOption)
extern void WXSDKManagerHandler_SetScreenBrightness_mA3F70E1E6F2B7F29B91E6C42A658F74415AC26CF (void);
// 0x000004E8 System.Void WeChatWASM.WXSDKManagerHandler::SetStatusBarStyleCallback(System.String)
extern void WXSDKManagerHandler_SetStatusBarStyleCallback_m2882AD70ADC5EC09A37DDCD9DBDDC864C857339E (void);
// 0x000004E9 System.Void WeChatWASM.WXSDKManagerHandler::at(System.String,System.String)
extern void WXSDKManagerHandler_at_m7CECEC27CB76778517AA7F2B3EA79891A0F8F0C2 (void);
// 0x000004EA System.Void WeChatWASM.WXSDKManagerHandler::SetStatusBarStyle(WeChatWASM.SetStatusBarStyleOption)
extern void WXSDKManagerHandler_SetStatusBarStyle_m85AEB483A0B9AA9070A39B7CE987D105FED419A1 (void);
// 0x000004EB System.Void WeChatWASM.WXSDKManagerHandler::SetUserCloudStorageCallback(System.String)
extern void WXSDKManagerHandler_SetUserCloudStorageCallback_mAF70370DB301902F9C6F7562517796FB72045C3B (void);
// 0x000004EC System.Void WeChatWASM.WXSDKManagerHandler::aU(System.String,System.String)
extern void WXSDKManagerHandler_aU_mDFA254A1B2EE639DDEF0A284CA3BED122D1DBCCF (void);
// 0x000004ED System.Void WeChatWASM.WXSDKManagerHandler::SetUserCloudStorage(WeChatWASM.SetUserCloudStorageOption)
extern void WXSDKManagerHandler_SetUserCloudStorage_m551FD6981EE6A203C59EF65CA85C28E6941F0026 (void);
// 0x000004EE System.Void WeChatWASM.WXSDKManagerHandler::ShowActionSheetCallback(System.String)
extern void WXSDKManagerHandler_ShowActionSheetCallback_m41E54935C5D9D8E52781ADA90571A150591D0706 (void);
// 0x000004EF System.Void WeChatWASM.WXSDKManagerHandler::au(System.String,System.String)
extern void WXSDKManagerHandler_au_mE2B972604605771EF3CC324A3BBC736D806EE36D (void);
// 0x000004F0 System.Void WeChatWASM.WXSDKManagerHandler::ShowActionSheet(WeChatWASM.ShowActionSheetOption)
extern void WXSDKManagerHandler_ShowActionSheet_m9CA806108DAA857E2EFCFA165233DFF98FBDC0FC (void);
// 0x000004F1 System.Void WeChatWASM.WXSDKManagerHandler::ShowKeyboardCallback(System.String)
extern void WXSDKManagerHandler_ShowKeyboardCallback_m82B8B6BB776E74B0ACE5EBDCE3CD4C6348194C12 (void);
// 0x000004F2 System.Void WeChatWASM.WXSDKManagerHandler::aV(System.String,System.String)
extern void WXSDKManagerHandler_aV_mEF02629243EB3CDA47B50D30F6E2DD9B98772387 (void);
// 0x000004F3 System.Void WeChatWASM.WXSDKManagerHandler::ShowKeyboard(WeChatWASM.ShowKeyboardOption)
extern void WXSDKManagerHandler_ShowKeyboard_mCA61419A23654C95852AF1171EDDEB375AB0FFA4 (void);
// 0x000004F4 System.Void WeChatWASM.WXSDKManagerHandler::ShowLoadingCallback(System.String)
extern void WXSDKManagerHandler_ShowLoadingCallback_mE0CB3EBE1DF5E89102FF4304E95565D3FB298978 (void);
// 0x000004F5 System.Void WeChatWASM.WXSDKManagerHandler::av(System.String,System.String)
extern void WXSDKManagerHandler_av_m2B33F7E25DE05024D1CF47024AC9ABE2901A7733 (void);
// 0x000004F6 System.Void WeChatWASM.WXSDKManagerHandler::ShowLoading(WeChatWASM.ShowLoadingOption)
extern void WXSDKManagerHandler_ShowLoading_m7D4341653254E2921698A876C8B30AFB065B7DD5 (void);
// 0x000004F7 System.Void WeChatWASM.WXSDKManagerHandler::ShowModalCallback(System.String)
extern void WXSDKManagerHandler_ShowModalCallback_m9805D3009CE775ED8434E49C2F6039E9E6F4C9CC (void);
// 0x000004F8 System.Void WeChatWASM.WXSDKManagerHandler::aW(System.String,System.String)
extern void WXSDKManagerHandler_aW_m939BF4E7867EF04A64428C6F587BBFDE62DF988D (void);
// 0x000004F9 System.Void WeChatWASM.WXSDKManagerHandler::ShowModal(WeChatWASM.ShowModalOption)
extern void WXSDKManagerHandler_ShowModal_m9C8A86296D2A2C4C84B3CB5FDD5E925173EE967D (void);
// 0x000004FA System.Void WeChatWASM.WXSDKManagerHandler::ShowShareImageMenuCallback(System.String)
extern void WXSDKManagerHandler_ShowShareImageMenuCallback_mBE62DFA5605A03228D1D9C0C97EF6F1F54D93F66 (void);
// 0x000004FB System.Void WeChatWASM.WXSDKManagerHandler::aw(System.String,System.String)
extern void WXSDKManagerHandler_aw_mED6C7169B343012E9A7A6AF6F9DFF8CA4D7F5974 (void);
// 0x000004FC System.Void WeChatWASM.WXSDKManagerHandler::ShowShareImageMenu(WeChatWASM.ShowShareImageMenuOption)
extern void WXSDKManagerHandler_ShowShareImageMenu_m3281A663A04DE3943690691F0A05985994555BF1 (void);
// 0x000004FD System.Void WeChatWASM.WXSDKManagerHandler::ShowShareMenuCallback(System.String)
extern void WXSDKManagerHandler_ShowShareMenuCallback_mEBD75CE8D92B6B365B7DFE0CE5C3663324E51A4C (void);
// 0x000004FE System.Void WeChatWASM.WXSDKManagerHandler::aX(System.String,System.String)
extern void WXSDKManagerHandler_aX_mAD2BC71FAAA14AD53511CB49F9E53014FD5F4C84 (void);
// 0x000004FF System.Void WeChatWASM.WXSDKManagerHandler::ShowShareMenu(WeChatWASM.ShowShareMenuOption)
extern void WXSDKManagerHandler_ShowShareMenu_m6B0EF3E27690E87CB12BC19ADF07DB2C6A8BB39B (void);
// 0x00000500 System.Void WeChatWASM.WXSDKManagerHandler::ShowToastCallback(System.String)
extern void WXSDKManagerHandler_ShowToastCallback_mCA1745EC33408A6867AB6C35363FC658C13F4830 (void);
// 0x00000501 System.Void WeChatWASM.WXSDKManagerHandler::ax(System.String,System.String)
extern void WXSDKManagerHandler_ax_mBE540F30025A91A8DE593A792DDBCC87FDB51E99 (void);
// 0x00000502 System.Void WeChatWASM.WXSDKManagerHandler::ShowToast(WeChatWASM.ShowToastOption)
extern void WXSDKManagerHandler_ShowToast_m5D91FE337092A6B80F60DDF4EA091EAD21E42BB6 (void);
// 0x00000503 System.Void WeChatWASM.WXSDKManagerHandler::StartAccelerometerCallback(System.String)
extern void WXSDKManagerHandler_StartAccelerometerCallback_mFA1E10663F9500DECBCB122EC8E4B0F510178C50 (void);
// 0x00000504 System.Void WeChatWASM.WXSDKManagerHandler::aY(System.String,System.String)
extern void WXSDKManagerHandler_aY_m51B1B02DA707678858FF121607DE4E16C8E525C6 (void);
// 0x00000505 System.Void WeChatWASM.WXSDKManagerHandler::StartAccelerometer(WeChatWASM.StartAccelerometerOption)
extern void WXSDKManagerHandler_StartAccelerometer_m6029E87CC481AD76704D4858856C660F6467668F (void);
// 0x00000506 System.Void WeChatWASM.WXSDKManagerHandler::StartBeaconDiscoveryCallback(System.String)
extern void WXSDKManagerHandler_StartBeaconDiscoveryCallback_m2B26461413E656142AE4D43E24E73C6B3308A937 (void);
// 0x00000507 System.Void WeChatWASM.WXSDKManagerHandler::ay(System.String,System.String)
extern void WXSDKManagerHandler_ay_mCEEF66D3D68DE00E93F6408F5688E6BF2732BA3E (void);
// 0x00000508 System.Void WeChatWASM.WXSDKManagerHandler::StartBeaconDiscovery(WeChatWASM.StartBeaconDiscoveryOption)
extern void WXSDKManagerHandler_StartBeaconDiscovery_m16A62D59899AED0C6235C72C3165B0A8DCE83741 (void);
// 0x00000509 System.Void WeChatWASM.WXSDKManagerHandler::StartBluetoothDevicesDiscoveryCallback(System.String)
extern void WXSDKManagerHandler_StartBluetoothDevicesDiscoveryCallback_mDC30C217249BE41B39E627C8504972E43FD64140 (void);
// 0x0000050A System.Void WeChatWASM.WXSDKManagerHandler::aZ(System.String,System.String)
extern void WXSDKManagerHandler_aZ_mF0B4C42918EBBE36A42361ABD61F670E7AB82DCB (void);
// 0x0000050B System.Void WeChatWASM.WXSDKManagerHandler::StartBluetoothDevicesDiscovery(WeChatWASM.StartBluetoothDevicesDiscoveryOption)
extern void WXSDKManagerHandler_StartBluetoothDevicesDiscovery_m21284111786329A15CC9C56BF3F69C5FE8374A0C (void);
// 0x0000050C System.Void WeChatWASM.WXSDKManagerHandler::StartCompassCallback(System.String)
extern void WXSDKManagerHandler_StartCompassCallback_m14917C9B89D8586BE76769710D5AD9E2DB20E268 (void);
// 0x0000050D System.Void WeChatWASM.WXSDKManagerHandler::az(System.String,System.String)
extern void WXSDKManagerHandler_az_m77F1651CF32B65805EEE787E3E388E95BA9FAEBE (void);
// 0x0000050E System.Void WeChatWASM.WXSDKManagerHandler::StartCompass(WeChatWASM.StartCompassOption)
extern void WXSDKManagerHandler_StartCompass_m7677D0D399081B1152BE8B013F1A82D10D94EBA8 (void);
// 0x0000050F System.Void WeChatWASM.WXSDKManagerHandler::StartDeviceMotionListeningCallback(System.String)
extern void WXSDKManagerHandler_StartDeviceMotionListeningCallback_m3B0505916C54C5C066652366C1B95737CE68057F (void);
// 0x00000510 System.Void WeChatWASM.WXSDKManagerHandler::BA(System.String,System.String)
extern void WXSDKManagerHandler_BA_m76B412ED1D4A1663C03392031C0053447298CA13 (void);
// 0x00000511 System.Void WeChatWASM.WXSDKManagerHandler::StartDeviceMotionListening(WeChatWASM.StartDeviceMotionListeningOption)
extern void WXSDKManagerHandler_StartDeviceMotionListening_m5C12590EFF805F4E6A630210A1A043B348EDCC4C (void);
// 0x00000512 System.Void WeChatWASM.WXSDKManagerHandler::StartGyroscopeCallback(System.String)
extern void WXSDKManagerHandler_StartGyroscopeCallback_m57AAE734C552E695329C2E90ADD03C0B8B48116C (void);
// 0x00000513 System.Void WeChatWASM.WXSDKManagerHandler::Ba(System.String,System.String)
extern void WXSDKManagerHandler_Ba_m2A29294E023EE3C1A6CB9E836B3B1AC53B57298B (void);
// 0x00000514 System.Void WeChatWASM.WXSDKManagerHandler::StartGyroscope(WeChatWASM.StartGyroscopeOption)
extern void WXSDKManagerHandler_StartGyroscope_mD09CDBB3C236AA9ECB6E1C58366B3998AD5D1B69 (void);
// 0x00000515 System.Void WeChatWASM.WXSDKManagerHandler::StopAccelerometerCallback(System.String)
extern void WXSDKManagerHandler_StopAccelerometerCallback_mBC58386FC9AEA306684FAB02B444B782F2488BEC (void);
// 0x00000516 System.Void WeChatWASM.WXSDKManagerHandler::BB(System.String,System.String)
extern void WXSDKManagerHandler_BB_m9B2707C1B9D1543E7AC1BA1C2581A39DFD8C245A (void);
// 0x00000517 System.Void WeChatWASM.WXSDKManagerHandler::StopAccelerometer(WeChatWASM.StopAccelerometerOption)
extern void WXSDKManagerHandler_StopAccelerometer_mC5CC6A584A1F9862989D4A083E7C2D0724271544 (void);
// 0x00000518 System.Void WeChatWASM.WXSDKManagerHandler::StopBeaconDiscoveryCallback(System.String)
extern void WXSDKManagerHandler_StopBeaconDiscoveryCallback_m1B41F591F788D724F799A8453F9AC92E17DD4A75 (void);
// 0x00000519 System.Void WeChatWASM.WXSDKManagerHandler::Bb(System.String,System.String)
extern void WXSDKManagerHandler_Bb_m3746D485ACC728E7AA701D1AC760B6AC3BF22281 (void);
// 0x0000051A System.Void WeChatWASM.WXSDKManagerHandler::StopBeaconDiscovery(WeChatWASM.StopBeaconDiscoveryOption)
extern void WXSDKManagerHandler_StopBeaconDiscovery_m694133C0CB9B8CD9ECD4E685ED293B095524ACE5 (void);
// 0x0000051B System.Void WeChatWASM.WXSDKManagerHandler::StopBluetoothDevicesDiscoveryCallback(System.String)
extern void WXSDKManagerHandler_StopBluetoothDevicesDiscoveryCallback_m10140A4AB1D670D2B370B43FBCFD14D6C7490B83 (void);
// 0x0000051C System.Void WeChatWASM.WXSDKManagerHandler::BC(System.String,System.String)
extern void WXSDKManagerHandler_BC_mC0DB880AD2EF45C2C1458211FE09BBF5E9FAE600 (void);
// 0x0000051D System.Void WeChatWASM.WXSDKManagerHandler::StopBluetoothDevicesDiscovery(WeChatWASM.StopBluetoothDevicesDiscoveryOption)
extern void WXSDKManagerHandler_StopBluetoothDevicesDiscovery_mDCCB1F188DCB8BD7C570EB8715DD87B9BF7D0049 (void);
// 0x0000051E System.Void WeChatWASM.WXSDKManagerHandler::StopCompassCallback(System.String)
extern void WXSDKManagerHandler_StopCompassCallback_m445C70808BBA42DF3E96E5A0D668742FD2870C0C (void);
// 0x0000051F System.Void WeChatWASM.WXSDKManagerHandler::Bc(System.String,System.String)
extern void WXSDKManagerHandler_Bc_m9B0742092E29B2CF27E5E0E985888A90F54CCD80 (void);
// 0x00000520 System.Void WeChatWASM.WXSDKManagerHandler::StopCompass(WeChatWASM.StopCompassOption)
extern void WXSDKManagerHandler_StopCompass_m4C3F27380D7366B3CD42788A90B4DC88296B6B02 (void);
// 0x00000521 System.Void WeChatWASM.WXSDKManagerHandler::StopDeviceMotionListeningCallback(System.String)
extern void WXSDKManagerHandler_StopDeviceMotionListeningCallback_m78C29A8000E50BF48EDE52E1AB180A2681785D92 (void);
// 0x00000522 System.Void WeChatWASM.WXSDKManagerHandler::BD(System.String,System.String)
extern void WXSDKManagerHandler_BD_m3E7683CEF520ABD10F801888309AE734F03FFFD7 (void);
// 0x00000523 System.Void WeChatWASM.WXSDKManagerHandler::StopDeviceMotionListening(WeChatWASM.StopDeviceMotionListeningOption)
extern void WXSDKManagerHandler_StopDeviceMotionListening_mE519D43776DED5D4433CA2A6640A73A9DDCC1047 (void);
// 0x00000524 System.Void WeChatWASM.WXSDKManagerHandler::StopFaceDetectCallback(System.String)
extern void WXSDKManagerHandler_StopFaceDetectCallback_m1ADDF54649022B596ECE01DA71CC940E2A4DC674 (void);
// 0x00000525 System.Void WeChatWASM.WXSDKManagerHandler::Bd(System.String,System.String)
extern void WXSDKManagerHandler_Bd_m0E9C3F36B6B92BE092BAF59C4B684DE40E7B03C6 (void);
// 0x00000526 System.Void WeChatWASM.WXSDKManagerHandler::StopFaceDetect(WeChatWASM.StopFaceDetectOption)
extern void WXSDKManagerHandler_StopFaceDetect_m1D3409578763AE733D6E7C4F43833E1D69896211 (void);
// 0x00000527 System.Void WeChatWASM.WXSDKManagerHandler::StopGyroscopeCallback(System.String)
extern void WXSDKManagerHandler_StopGyroscopeCallback_m5CFEF42F12A540A0306443DA86A7E5833A014995 (void);
// 0x00000528 System.Void WeChatWASM.WXSDKManagerHandler::BE(System.String,System.String)
extern void WXSDKManagerHandler_BE_mA0E1929034FFD0C8544C7379902BB787DFDEFD81 (void);
// 0x00000529 System.Void WeChatWASM.WXSDKManagerHandler::StopGyroscope(WeChatWASM.StopGyroscopeOption)
extern void WXSDKManagerHandler_StopGyroscope_mAB65414B4ED65F75D00AA3ABD79879765E024ED5 (void);
// 0x0000052A System.Void WeChatWASM.WXSDKManagerHandler::UpdateKeyboardCallback(System.String)
extern void WXSDKManagerHandler_UpdateKeyboardCallback_m99DA492898208A33C3901A3C56817EBB4891E383 (void);
// 0x0000052B System.Void WeChatWASM.WXSDKManagerHandler::Be(System.String,System.String)
extern void WXSDKManagerHandler_Be_m2B6F8920D94CF7C1D8107584EC123C8E6B4F43F6 (void);
// 0x0000052C System.Void WeChatWASM.WXSDKManagerHandler::UpdateKeyboard(WeChatWASM.UpdateKeyboardOption)
extern void WXSDKManagerHandler_UpdateKeyboard_m15E469A1C0C6EACF0434F726417563E888EE5887 (void);
// 0x0000052D System.Void WeChatWASM.WXSDKManagerHandler::UpdateShareMenuCallback(System.String)
extern void WXSDKManagerHandler_UpdateShareMenuCallback_m4105B59E38DB583FD6AA3642668DF7970CCE2BC2 (void);
// 0x0000052E System.Void WeChatWASM.WXSDKManagerHandler::BF(System.String,System.String)
extern void WXSDKManagerHandler_BF_m50BA44FC6B368927AB49C7C5FC5AF919BB9B7028 (void);
// 0x0000052F System.Void WeChatWASM.WXSDKManagerHandler::UpdateShareMenu(WeChatWASM.UpdateShareMenuOption)
extern void WXSDKManagerHandler_UpdateShareMenu_mCD4EE93D10D5D55CA450C6DDB382437F31080390 (void);
// 0x00000530 System.Void WeChatWASM.WXSDKManagerHandler::UpdateVoIPChatMuteConfigCallback(System.String)
extern void WXSDKManagerHandler_UpdateVoIPChatMuteConfigCallback_mFE045B790068E6AADCD889D33AF7633EDE3C68CC (void);
// 0x00000531 System.Void WeChatWASM.WXSDKManagerHandler::Bf(System.String,System.String)
extern void WXSDKManagerHandler_Bf_m27AEEFE5489277E609EC5BA9D7D9E19A3F17F610 (void);
// 0x00000532 System.Void WeChatWASM.WXSDKManagerHandler::UpdateVoIPChatMuteConfig(WeChatWASM.UpdateVoIPChatMuteConfigOption)
extern void WXSDKManagerHandler_UpdateVoIPChatMuteConfig_mF6542F4D31BF06743146FF16B9D7D76FAA92A851 (void);
// 0x00000533 System.Void WeChatWASM.WXSDKManagerHandler::UpdateWeChatAppCallback(System.String)
extern void WXSDKManagerHandler_UpdateWeChatAppCallback_mFE32E3472CDFE2FDBDF0F133D0858A0885B43EDF (void);
// 0x00000534 System.Void WeChatWASM.WXSDKManagerHandler::BG(System.String,System.String)
extern void WXSDKManagerHandler_BG_mAF09DFD2A0ABB50859A95F13727EA579FD3C0F39 (void);
// 0x00000535 System.Void WeChatWASM.WXSDKManagerHandler::UpdateWeChatApp(WeChatWASM.UpdateWeChatAppOption)
extern void WXSDKManagerHandler_UpdateWeChatApp_m8E913CF735CD9654C3250971E76117AE6C4805ED (void);
// 0x00000536 System.Void WeChatWASM.WXSDKManagerHandler::VibrateLongCallback(System.String)
extern void WXSDKManagerHandler_VibrateLongCallback_mE2AD5B2DD3DAEF8C5B6C7286469A366E762E09B0 (void);
// 0x00000537 System.Void WeChatWASM.WXSDKManagerHandler::Bg(System.String,System.String)
extern void WXSDKManagerHandler_Bg_m81CD9E7279C5C5F3AAE466F0BB16DAC1F78FB47F (void);
// 0x00000538 System.Void WeChatWASM.WXSDKManagerHandler::VibrateLong(WeChatWASM.VibrateLongOption)
extern void WXSDKManagerHandler_VibrateLong_mCED342D71124FECAF0DE2AE2C9F3F57A884D8E52 (void);
// 0x00000539 System.Void WeChatWASM.WXSDKManagerHandler::VibrateShortCallback(System.String)
extern void WXSDKManagerHandler_VibrateShortCallback_mEF257381799C161567EE72C89C4AF77FE7A48609 (void);
// 0x0000053A System.Void WeChatWASM.WXSDKManagerHandler::BH(System.String,System.String)
extern void WXSDKManagerHandler_BH_mD65736730604C709E7A89616510289FDE69A0530 (void);
// 0x0000053B System.Void WeChatWASM.WXSDKManagerHandler::VibrateShort(WeChatWASM.VibrateShortOption)
extern void WXSDKManagerHandler_VibrateShort_mDB55527776DAD703FD276E80BB5DF71CAB725C92 (void);
// 0x0000053C System.Void WeChatWASM.WXSDKManagerHandler::WriteBLECharacteristicValueCallback(System.String)
extern void WXSDKManagerHandler_WriteBLECharacteristicValueCallback_m77E68546E32C33FCF5DAFFF49AAD16EED234E7AA (void);
// 0x0000053D System.Void WeChatWASM.WXSDKManagerHandler::Bh(System.String,System.String)
extern void WXSDKManagerHandler_Bh_m6EC98BABDBDF64613E0E95E1DE52B306C584707F (void);
// 0x0000053E System.Void WeChatWASM.WXSDKManagerHandler::WriteBLECharacteristicValue(WeChatWASM.WriteBLECharacteristicValueOption)
extern void WXSDKManagerHandler_WriteBLECharacteristicValue_mEA39AA9BE7B9BE8A02B5BBEAC744EA535903055D (void);
// 0x0000053F System.Void WeChatWASM.WXSDKManagerHandler::StartGameLiveCallback(System.String)
extern void WXSDKManagerHandler_StartGameLiveCallback_m17E947B73FEA1EF8A0F0EC9DAFA7BB01A15EF0BA (void);
// 0x00000540 System.Void WeChatWASM.WXSDKManagerHandler::BI(System.String,System.String)
extern void WXSDKManagerHandler_BI_m0FF76155388A99A88CEA624AFED350E6699DFD8E (void);
// 0x00000541 System.Void WeChatWASM.WXSDKManagerHandler::StartGameLive(WeChatWASM.StartGameLiveOption)
extern void WXSDKManagerHandler_StartGameLive_mCE30913AE68EF7F6359C19AA7F6004E099822089 (void);
// 0x00000542 System.Void WeChatWASM.WXSDKManagerHandler::CheckGameLiveEnabledCallback(System.String)
extern void WXSDKManagerHandler_CheckGameLiveEnabledCallback_mDB575C1A87780ECF3A88C5F6C2BE19E3D10F5CAA (void);
// 0x00000543 System.Void WeChatWASM.WXSDKManagerHandler::Bi(System.String,System.String)
extern void WXSDKManagerHandler_Bi_mE95807ED56A4A1900EFD48A7667DE464818FF80F (void);
// 0x00000544 System.Void WeChatWASM.WXSDKManagerHandler::CheckGameLiveEnabled(WeChatWASM.CheckGameLiveEnabledOption)
extern void WXSDKManagerHandler_CheckGameLiveEnabled_m275E2D5E67BFDA9544EA21B3CDC043A57DB061FB (void);
// 0x00000545 System.Void WeChatWASM.WXSDKManagerHandler::GetUserCurrentGameliveInfoCallback(System.String)
extern void WXSDKManagerHandler_GetUserCurrentGameliveInfoCallback_m948932146F8C552DA0BE33A5A69D4ECA08C3AD47 (void);
// 0x00000546 System.Void WeChatWASM.WXSDKManagerHandler::BJ(System.String,System.String)
extern void WXSDKManagerHandler_BJ_mC1614130BF39BA7D2BEAEFB3F5283421924CD90E (void);
// 0x00000547 System.Void WeChatWASM.WXSDKManagerHandler::GetUserCurrentGameliveInfo(WeChatWASM.GetUserCurrentGameliveInfoOption)
extern void WXSDKManagerHandler_GetUserCurrentGameliveInfo_mFF63A375495FC59A0920E260C3D49319AB82888B (void);
// 0x00000548 System.Void WeChatWASM.WXSDKManagerHandler::GetUserRecentGameLiveInfoCallback(System.String)
extern void WXSDKManagerHandler_GetUserRecentGameLiveInfoCallback_m6D0082F6D4F60F3BD67BD06A4483D5C08154DFE9 (void);
// 0x00000549 System.Void WeChatWASM.WXSDKManagerHandler::Bj(System.String,System.String)
extern void WXSDKManagerHandler_Bj_mCF4EB815BCACD94CD79162ECA624FAA6346D6604 (void);
// 0x0000054A System.Void WeChatWASM.WXSDKManagerHandler::GetUserRecentGameLiveInfo(WeChatWASM.GetUserRecentGameLiveInfoOption)
extern void WXSDKManagerHandler_GetUserRecentGameLiveInfo_m87DF8F83E23A290CD673729FE89630F346B258DF (void);
// 0x0000054B System.Void WeChatWASM.WXSDKManagerHandler::GetUserGameLiveDetailsCallback(System.String)
extern void WXSDKManagerHandler_GetUserGameLiveDetailsCallback_m9D49EF496173BCFE51C292A539A366E325564786 (void);
// 0x0000054C System.Void WeChatWASM.WXSDKManagerHandler::BK(System.String,System.String)
extern void WXSDKManagerHandler_BK_m7D78C0AE281E917B5F293B1100CB93F4CAC8766B (void);
// 0x0000054D System.Void WeChatWASM.WXSDKManagerHandler::GetUserGameLiveDetails(WeChatWASM.GetUserGameLiveDetailsOption)
extern void WXSDKManagerHandler_GetUserGameLiveDetails_m560D1175D69FB71A3789D1A3100815B7F1200112 (void);
// 0x0000054E System.Void WeChatWASM.WXSDKManagerHandler::OpenChannelsLiveCollectionCallback(System.String)
extern void WXSDKManagerHandler_OpenChannelsLiveCollectionCallback_mBEA39F14A7E1B1716615D0E5CC0DE4900A1AC5E9 (void);
// 0x0000054F System.Void WeChatWASM.WXSDKManagerHandler::Bk(System.String,System.String)
extern void WXSDKManagerHandler_Bk_m9E12CB9F225D5DD1800D9A208FCDAF1BA3528FEA (void);
// 0x00000550 System.Void WeChatWASM.WXSDKManagerHandler::OpenChannelsLiveCollection(WeChatWASM.OpenChannelsLiveCollectionOption)
extern void WXSDKManagerHandler_OpenChannelsLiveCollection_m061534AE74AF0E9C18CD7F5EE6152FAA1A036807 (void);
// 0x00000551 System.Void WeChatWASM.WXSDKManagerHandler::OpenPageCallback(System.String)
extern void WXSDKManagerHandler_OpenPageCallback_m5D25AAA1884E0FB2D73197E2A7517C483A2842E1 (void);
// 0x00000552 System.Void WeChatWASM.WXSDKManagerHandler::BL(System.String,System.String)
extern void WXSDKManagerHandler_BL_mCA5A88D91E893EB601B1A0A2A6D58AD1AC18B053 (void);
// 0x00000553 System.Void WeChatWASM.WXSDKManagerHandler::OpenPage(WeChatWASM.OpenPageOption)
extern void WXSDKManagerHandler_OpenPage_m29F4F8326135449F71CDE94AA5AE1229DC4DEF60 (void);
// 0x00000554 System.Void WeChatWASM.WXSDKManagerHandler::RequestMidasPaymentGameItemCallback(System.String)
extern void WXSDKManagerHandler_RequestMidasPaymentGameItemCallback_m98EB1644874ED7160CCD4A286DBFF4E13BF8A2F7 (void);
// 0x00000555 System.Void WeChatWASM.WXSDKManagerHandler::Bl(System.String,System.String)
extern void WXSDKManagerHandler_Bl_m4962B6CA6892ABE00B2FA2686CC10E6E0ACC392E (void);
// 0x00000556 System.Void WeChatWASM.WXSDKManagerHandler::RequestMidasPaymentGameItem(WeChatWASM.RequestMidasPaymentGameItemOption)
extern void WXSDKManagerHandler_RequestMidasPaymentGameItem_mEFB8083DCD3AC7E758D27D1BA08B5907CDFE351F (void);
// 0x00000557 System.Void WeChatWASM.WXSDKManagerHandler::RequestSubscribeLiveActivityCallback(System.String)
extern void WXSDKManagerHandler_RequestSubscribeLiveActivityCallback_m4EBEB764BAEEE1CB2F6DFEA56F0BE8195DC69AF6 (void);
// 0x00000558 System.Void WeChatWASM.WXSDKManagerHandler::BM(System.String,System.String)
extern void WXSDKManagerHandler_BM_m14A596E409A016D2889A28EE38F2C5A6D3953F15 (void);
// 0x00000559 System.Void WeChatWASM.WXSDKManagerHandler::RequestSubscribeLiveActivity(WeChatWASM.RequestSubscribeLiveActivityOption)
extern void WXSDKManagerHandler_RequestSubscribeLiveActivity_m0EB504744FB6E815E82B48C85958BB4FCEFA7DD5 (void);
// 0x0000055A System.Void WeChatWASM.WXSDKManagerHandler::A(System.String)
extern void WXSDKManagerHandler_A_m8063A22CD5E13F49310BA73D9854C304ED30E1F4 (void);
// 0x0000055B System.Void WeChatWASM.WXSDKManagerHandler::OperateGameRecorderVideo(WeChatWASM.OperateGameRecorderVideoOption)
extern void WXSDKManagerHandler_OperateGameRecorderVideo_m0C3CCB8F336A97D6C0FD88F128B577FCA289DE31 (void);
// 0x0000055C System.Void WeChatWASM.WXSDKManagerHandler::a(System.String)
extern void WXSDKManagerHandler_a_mAD83337C55A5C9500E975129EC78E927F3E65B7B (void);
// 0x0000055D System.Void WeChatWASM.WXSDKManagerHandler::RemoveStorageSync(System.String)
extern void WXSDKManagerHandler_RemoveStorageSync_m9B3ADA16645D897FE0372542DB3098A42D2351C3 (void);
// 0x0000055E System.Void WeChatWASM.WXSDKManagerHandler::Bm(System.String,System.String)
extern void WXSDKManagerHandler_Bm_m2C17FC847F0CBD07A295FB502225A4D7CE2590DB (void);
// 0x0000055F System.Void WeChatWASM.WXSDKManagerHandler::ReportEvent(System.String,T)
// 0x00000560 System.Void WeChatWASM.WXSDKManagerHandler::A(System.String,System.Double)
extern void WXSDKManagerHandler_A_mB2E5D4740EEE6111B3FD29D289547E976CBB796E (void);
// 0x00000561 System.Void WeChatWASM.WXSDKManagerHandler::ReportMonitor(System.String,System.Double)
extern void WXSDKManagerHandler_ReportMonitor_mBEF810B899DBF204084489C9990F7A7960C1B150 (void);
// 0x00000562 System.Void WeChatWASM.WXSDKManagerHandler::A(System.Double,System.Double,System.String)
extern void WXSDKManagerHandler_A_m5D39423859794123075EBDFB4F8A25EC86A3CB17 (void);
// 0x00000563 System.Void WeChatWASM.WXSDKManagerHandler::ReportPerformance(System.Double,System.Double,System.String)
extern void WXSDKManagerHandler_ReportPerformance_m3240E0A04B4319EC08E29871380EED311511963B (void);
// 0x00000564 System.Void WeChatWASM.WXSDKManagerHandler::B(System.String)
extern void WXSDKManagerHandler_B_m9532AE805E11D2B0F6C1A6FB15350512254036EF (void);
// 0x00000565 System.Void WeChatWASM.WXSDKManagerHandler::ReportUserBehaviorBranchAnalytics(WeChatWASM.ReportUserBehaviorBranchAnalyticsOption)
extern void WXSDKManagerHandler_ReportUserBehaviorBranchAnalytics_m391111476972F67677086034D294BB1860B13B0A (void);
// 0x00000566 System.Void WeChatWASM.WXSDKManagerHandler::b(System.String)
extern void WXSDKManagerHandler_b_m6F2304C6EEA074E952440E49F6F808EDA60C8D46 (void);
// 0x00000567 System.Void WeChatWASM.WXSDKManagerHandler::ReserveChannelsLive(WeChatWASM.ReserveChannelsLiveOption)
extern void WXSDKManagerHandler_ReserveChannelsLive_mB38112E6EC48CABFCEE84C9C32D368263ADAC3F8 (void);
// 0x00000568 System.Void WeChatWASM.WXSDKManagerHandler::C(System.String)
extern void WXSDKManagerHandler_C_m55951192F0BE90A812239A3D4275D83609A2B90F (void);
// 0x00000569 System.Void WeChatWASM.WXSDKManagerHandler::RevokeBufferURL(System.String)
extern void WXSDKManagerHandler_RevokeBufferURL_mAEBB7502F31C52046BBAD2A0AD7C71B6717A8D9E (void);
// 0x0000056A System.Void WeChatWASM.WXSDKManagerHandler::BN(System.String,System.String)
extern void WXSDKManagerHandler_BN_mAD881D912C11D4EB5E8B5AED69C9C61C51A2E4F4 (void);
// 0x0000056B System.Void WeChatWASM.WXSDKManagerHandler::SetStorageSync(System.String,T)
// 0x0000056C System.Void WeChatWASM.WXSDKManagerHandler::c(System.String)
extern void WXSDKManagerHandler_c_mB3AADC68401F236A71D9C55E85A0AFCD2707184C (void);
// 0x0000056D System.Void WeChatWASM.WXSDKManagerHandler::ShareAppMessage(WeChatWASM.ShareAppMessageOption)
extern void WXSDKManagerHandler_ShareAppMessage_mF3DC99CDC5442D18A4AC37EDE142EFD6421F548A (void);
// 0x0000056E System.Void WeChatWASM.WXSDKManagerHandler::A()
extern void WXSDKManagerHandler_A_m9765583FE2B0FCEF4DACBC5E9A97E0627AE6BAF4 (void);
// 0x0000056F System.Void WeChatWASM.WXSDKManagerHandler::TriggerGC()
extern void WXSDKManagerHandler_TriggerGC_m4F074B94563066A91F971F5D6802115344DCD749 (void);
// 0x00000570 System.Void WeChatWASM.WXSDKManagerHandler::_OnAccelerometerChangeCallback(System.String)
extern void WXSDKManagerHandler__OnAccelerometerChangeCallback_m8D36E29D08CF3E746DA3B6C02D2C3F1EE71BBA1B (void);
// 0x00000571 System.Void WeChatWASM.WXSDKManagerHandler::a()
extern void WXSDKManagerHandler_a_m0393D8325BD308B5889593D72A73C3685F3AD13F (void);
// 0x00000572 System.Void WeChatWASM.WXSDKManagerHandler::OnAccelerometerChange(System.Action`1<WeChatWASM.OnAccelerometerChangeListenerResult>)
extern void WXSDKManagerHandler_OnAccelerometerChange_m95DB7599156B23BD81629ECCF92AC67935370DFF (void);
// 0x00000573 System.Void WeChatWASM.WXSDKManagerHandler::B()
extern void WXSDKManagerHandler_B_m2B572567FE156100B62F57E762A3C483A8CB8D16 (void);
// 0x00000574 System.Void WeChatWASM.WXSDKManagerHandler::OffAccelerometerChange(System.Action`1<WeChatWASM.OnAccelerometerChangeListenerResult>)
extern void WXSDKManagerHandler_OffAccelerometerChange_mB3F5AB389F3CF367B2356108E709ED8C92016639 (void);
// 0x00000575 System.Void WeChatWASM.WXSDKManagerHandler::_OnAudioInterruptionBeginCallback(System.String)
extern void WXSDKManagerHandler__OnAudioInterruptionBeginCallback_m7150D04F8A4325003F81897992B80E4E1F91FC35 (void);
// 0x00000576 System.Void WeChatWASM.WXSDKManagerHandler::b()
extern void WXSDKManagerHandler_b_m61CA1EDB92A05386AEF57DD0471FC5493B200ADB (void);
// 0x00000577 System.Void WeChatWASM.WXSDKManagerHandler::OnAudioInterruptionBegin(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WXSDKManagerHandler_OnAudioInterruptionBegin_m37B93870BD8520E3A09E416B41B85484AF3E10B7 (void);
// 0x00000578 System.Void WeChatWASM.WXSDKManagerHandler::C()
extern void WXSDKManagerHandler_C_m79F00F36D0DF4B0E1ACB1E9656BA13A654F4B627 (void);
// 0x00000579 System.Void WeChatWASM.WXSDKManagerHandler::OffAudioInterruptionBegin(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WXSDKManagerHandler_OffAudioInterruptionBegin_m679CE058630DE1DC887660202144A0F6756AE20B (void);
// 0x0000057A System.Void WeChatWASM.WXSDKManagerHandler::_OnAudioInterruptionEndCallback(System.String)
extern void WXSDKManagerHandler__OnAudioInterruptionEndCallback_mD814D3804AB5B484EA13AB5B69698BFB4D1B0653 (void);
// 0x0000057B System.Void WeChatWASM.WXSDKManagerHandler::c()
extern void WXSDKManagerHandler_c_m838A2D28EDE65F32E9ED34E7F500B1DD6C99765A (void);
// 0x0000057C System.Void WeChatWASM.WXSDKManagerHandler::OnAudioInterruptionEnd(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WXSDKManagerHandler_OnAudioInterruptionEnd_m34CDFFD52FCAD45F9FF120A67515DFCF5FA69B4E (void);
// 0x0000057D System.Void WeChatWASM.WXSDKManagerHandler::D()
extern void WXSDKManagerHandler_D_m360A3B74D0A88B86C466AAB239B06C7F426235EC (void);
// 0x0000057E System.Void WeChatWASM.WXSDKManagerHandler::OffAudioInterruptionEnd(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WXSDKManagerHandler_OffAudioInterruptionEnd_m92FAD2215EB9A749AD5946D1CB81248CFEBE74F7 (void);
// 0x0000057F System.Void WeChatWASM.WXSDKManagerHandler::_OnBLECharacteristicValueChangeCallback(System.String)
extern void WXSDKManagerHandler__OnBLECharacteristicValueChangeCallback_m80EA252DBADDCFC6A4622FB7B76A838BCD521F2F (void);
// 0x00000580 System.Void WeChatWASM.WXSDKManagerHandler::d()
extern void WXSDKManagerHandler_d_m6B4DA806C77BCCD63B2E4EE25CC47C82C964B378 (void);
// 0x00000581 System.Void WeChatWASM.WXSDKManagerHandler::OnBLECharacteristicValueChange(System.Action`1<WeChatWASM.OnBLECharacteristicValueChangeListenerResult>)
extern void WXSDKManagerHandler_OnBLECharacteristicValueChange_m14A93308941E99C6DBFD84876276BFEF97DFA714 (void);
// 0x00000582 System.Void WeChatWASM.WXSDKManagerHandler::E()
extern void WXSDKManagerHandler_E_mBBB2EFDE2A06BD89C070AC882ED56FCF796622F1 (void);
// 0x00000583 System.Void WeChatWASM.WXSDKManagerHandler::OffBLECharacteristicValueChange(System.Action`1<WeChatWASM.OnBLECharacteristicValueChangeListenerResult>)
extern void WXSDKManagerHandler_OffBLECharacteristicValueChange_mB3737B606FE28472D1FA644086643777E6867EB7 (void);
// 0x00000584 System.Void WeChatWASM.WXSDKManagerHandler::_OnBLEConnectionStateChangeCallback(System.String)
extern void WXSDKManagerHandler__OnBLEConnectionStateChangeCallback_m8BEA1F116C643151733483C8B512AFC6F93D65F4 (void);
// 0x00000585 System.Void WeChatWASM.WXSDKManagerHandler::e()
extern void WXSDKManagerHandler_e_mA5E3EFCAD88D8B90C0226E729CF1B09186645A48 (void);
// 0x00000586 System.Void WeChatWASM.WXSDKManagerHandler::OnBLEConnectionStateChange(System.Action`1<WeChatWASM.OnBLEConnectionStateChangeListenerResult>)
extern void WXSDKManagerHandler_OnBLEConnectionStateChange_m317B880360E55AAF450A1BDF3E5384D8B71FCCAF (void);
// 0x00000587 System.Void WeChatWASM.WXSDKManagerHandler::F()
extern void WXSDKManagerHandler_F_m055172513598F5D5EBA7DEAC7DC7CF2215ABA985 (void);
// 0x00000588 System.Void WeChatWASM.WXSDKManagerHandler::OffBLEConnectionStateChange(System.Action`1<WeChatWASM.OnBLEConnectionStateChangeListenerResult>)
extern void WXSDKManagerHandler_OffBLEConnectionStateChange_m978F4917E47AD8D8B8BEFEDB2AD1E1509FAD0F13 (void);
// 0x00000589 System.Void WeChatWASM.WXSDKManagerHandler::_OnBLEMTUChangeCallback(System.String)
extern void WXSDKManagerHandler__OnBLEMTUChangeCallback_mB8DE2D55F0C32C241D7A8B439567072D135C27D4 (void);
// 0x0000058A System.Void WeChatWASM.WXSDKManagerHandler::f()
extern void WXSDKManagerHandler_f_m2692E5228071ADD95E1828C796CF5B5F7880DA75 (void);
// 0x0000058B System.Void WeChatWASM.WXSDKManagerHandler::OnBLEMTUChange(System.Action`1<WeChatWASM.OnBLEMTUChangeListenerResult>)
extern void WXSDKManagerHandler_OnBLEMTUChange_m51FF262987CEB565F9E3088AEA796A2B7AF612F5 (void);
// 0x0000058C System.Void WeChatWASM.WXSDKManagerHandler::G()
extern void WXSDKManagerHandler_G_mEB8F8E80837CC1BB2BE89AB59110F3AED4863D18 (void);
// 0x0000058D System.Void WeChatWASM.WXSDKManagerHandler::OffBLEMTUChange(System.Action`1<WeChatWASM.OnBLEMTUChangeListenerResult>)
extern void WXSDKManagerHandler_OffBLEMTUChange_mB992D4711F0E4D55D60690387CF19318D2058F86 (void);
// 0x0000058E System.Void WeChatWASM.WXSDKManagerHandler::_OnBLEPeripheralConnectionStateChangedCallback(System.String)
extern void WXSDKManagerHandler__OnBLEPeripheralConnectionStateChangedCallback_m937EFE0AE503B7FEB93BDE8F178C4CA186D36E94 (void);
// 0x0000058F System.Void WeChatWASM.WXSDKManagerHandler::g()
extern void WXSDKManagerHandler_g_m940F8C4CC2B017EBEE0205586F34A8FCE31F851F (void);
// 0x00000590 System.Void WeChatWASM.WXSDKManagerHandler::OnBLEPeripheralConnectionStateChanged(System.Action`1<WeChatWASM.OnBLEPeripheralConnectionStateChangedListenerResult>)
extern void WXSDKManagerHandler_OnBLEPeripheralConnectionStateChanged_m99235F29BD6020602257EBA2ADEE632280ED3E6B (void);
// 0x00000591 System.Void WeChatWASM.WXSDKManagerHandler::H()
extern void WXSDKManagerHandler_H_m8993EA9488A73EC620BC317F39FFFD64946A2D49 (void);
// 0x00000592 System.Void WeChatWASM.WXSDKManagerHandler::OffBLEPeripheralConnectionStateChanged(System.Action`1<WeChatWASM.OnBLEPeripheralConnectionStateChangedListenerResult>)
extern void WXSDKManagerHandler_OffBLEPeripheralConnectionStateChanged_m57AD57ABF486FE9F2A61BFA36B9D650A3B39DD11 (void);
// 0x00000593 System.Void WeChatWASM.WXSDKManagerHandler::_OnBeaconServiceChangeCallback(System.String)
extern void WXSDKManagerHandler__OnBeaconServiceChangeCallback_mAB118E944AB10AF159654BBC935CDAC5DFE0AD6E (void);
// 0x00000594 System.Void WeChatWASM.WXSDKManagerHandler::h()
extern void WXSDKManagerHandler_h_mA22DC79F1228F6F6A25B83701DDBFF8DE5C307A0 (void);
// 0x00000595 System.Void WeChatWASM.WXSDKManagerHandler::OnBeaconServiceChange(System.Action`1<WeChatWASM.OnBeaconServiceChangeListenerResult>)
extern void WXSDKManagerHandler_OnBeaconServiceChange_m8622BE6B9862C5C7DD65B42A56AE8932C89A8132 (void);
// 0x00000596 System.Void WeChatWASM.WXSDKManagerHandler::I()
extern void WXSDKManagerHandler_I_mCD4EB4F48661CC0C30A9D0CD63E41E2080C5B9D1 (void);
// 0x00000597 System.Void WeChatWASM.WXSDKManagerHandler::OffBeaconServiceChange(System.Action`1<WeChatWASM.OnBeaconServiceChangeListenerResult>)
extern void WXSDKManagerHandler_OffBeaconServiceChange_mB4D9FFE589157541DAB9C2C21B39812B69F364AA (void);
// 0x00000598 System.Void WeChatWASM.WXSDKManagerHandler::_OnBeaconUpdateCallback(System.String)
extern void WXSDKManagerHandler__OnBeaconUpdateCallback_m78B6A27808D84F56B761CE0034AAE55032370E11 (void);
// 0x00000599 System.Void WeChatWASM.WXSDKManagerHandler::i()
extern void WXSDKManagerHandler_i_m98D8E2C1908EEDBA75A45F22430C786FC40181FF (void);
// 0x0000059A System.Void WeChatWASM.WXSDKManagerHandler::OnBeaconUpdate(System.Action`1<WeChatWASM.OnBeaconUpdateListenerResult>)
extern void WXSDKManagerHandler_OnBeaconUpdate_mA7BFAB100D5396D2FC426321A22369D1165508C6 (void);
// 0x0000059B System.Void WeChatWASM.WXSDKManagerHandler::J()
extern void WXSDKManagerHandler_J_m326D22FA4FBBCB5869A436EF0B89BE09CF19EE34 (void);
// 0x0000059C System.Void WeChatWASM.WXSDKManagerHandler::OffBeaconUpdate(System.Action`1<WeChatWASM.OnBeaconUpdateListenerResult>)
extern void WXSDKManagerHandler_OffBeaconUpdate_m49BFD222CCB627ED70D06FE5FBB26B738B5C6FB3 (void);
// 0x0000059D System.Void WeChatWASM.WXSDKManagerHandler::_OnBluetoothAdapterStateChangeCallback(System.String)
extern void WXSDKManagerHandler__OnBluetoothAdapterStateChangeCallback_m5722B41983AE7DB9A5742C27BDA18BC4E61A5148 (void);
// 0x0000059E System.Void WeChatWASM.WXSDKManagerHandler::j()
extern void WXSDKManagerHandler_j_m44CE5079887122A2FD8C852EEFF23FB8C6FC8C96 (void);
// 0x0000059F System.Void WeChatWASM.WXSDKManagerHandler::OnBluetoothAdapterStateChange(System.Action`1<WeChatWASM.OnBluetoothAdapterStateChangeListenerResult>)
extern void WXSDKManagerHandler_OnBluetoothAdapterStateChange_mC2567CF20EC0102780A577066DBA8150A4CC3406 (void);
// 0x000005A0 System.Void WeChatWASM.WXSDKManagerHandler::K()
extern void WXSDKManagerHandler_K_m3C2605B7D252EDB129D8DCAB6245CC97CB080766 (void);
// 0x000005A1 System.Void WeChatWASM.WXSDKManagerHandler::OffBluetoothAdapterStateChange(System.Action`1<WeChatWASM.OnBluetoothAdapterStateChangeListenerResult>)
extern void WXSDKManagerHandler_OffBluetoothAdapterStateChange_mA87A27013F7A2A1F9F1A67F64DFEC463EE393A4D (void);
// 0x000005A2 System.Void WeChatWASM.WXSDKManagerHandler::_OnBluetoothDeviceFoundCallback(System.String)
extern void WXSDKManagerHandler__OnBluetoothDeviceFoundCallback_mECD63FCA6A65443A86251F92ABDBDA3BE0A55296 (void);
// 0x000005A3 System.Void WeChatWASM.WXSDKManagerHandler::k()
extern void WXSDKManagerHandler_k_mA3BFD81BE7C24DB7843131C0B7BA5B93B3622B05 (void);
// 0x000005A4 System.Void WeChatWASM.WXSDKManagerHandler::OnBluetoothDeviceFound(System.Action`1<WeChatWASM.OnBluetoothDeviceFoundListenerResult>)
extern void WXSDKManagerHandler_OnBluetoothDeviceFound_m6E365D9A5CD8A9BD807390DC62B15780CA275986 (void);
// 0x000005A5 System.Void WeChatWASM.WXSDKManagerHandler::L()
extern void WXSDKManagerHandler_L_mCD5FB0B67A45CA685B08F50DE17D1B0D028E5812 (void);
// 0x000005A6 System.Void WeChatWASM.WXSDKManagerHandler::OffBluetoothDeviceFound(System.Action`1<WeChatWASM.OnBluetoothDeviceFoundListenerResult>)
extern void WXSDKManagerHandler_OffBluetoothDeviceFound_mB416E002061B53E9326BF62B60B9ADB9EED2B112 (void);
// 0x000005A7 System.Void WeChatWASM.WXSDKManagerHandler::_OnCompassChangeCallback(System.String)
extern void WXSDKManagerHandler__OnCompassChangeCallback_mE256130665A67D0A0A580F060F71CAAC5810A164 (void);
// 0x000005A8 System.Void WeChatWASM.WXSDKManagerHandler::l()
extern void WXSDKManagerHandler_l_m36C3AB84942D6B4D771527B393C80155680B852D (void);
// 0x000005A9 System.Void WeChatWASM.WXSDKManagerHandler::OnCompassChange(System.Action`1<WeChatWASM.OnCompassChangeListenerResult>)
extern void WXSDKManagerHandler_OnCompassChange_m7B486E0490AD931422D51CB0EF214C4CC72BF9C3 (void);
// 0x000005AA System.Void WeChatWASM.WXSDKManagerHandler::M()
extern void WXSDKManagerHandler_M_mA2A86AAC2C3A8663F7F7FB19BF016AAD4473454E (void);
// 0x000005AB System.Void WeChatWASM.WXSDKManagerHandler::OffCompassChange(System.Action`1<WeChatWASM.OnCompassChangeListenerResult>)
extern void WXSDKManagerHandler_OffCompassChange_mA71818AAF45D9977D26EED9B9274B440E94A1C0A (void);
// 0x000005AC System.Void WeChatWASM.WXSDKManagerHandler::_OnDeviceMotionChangeCallback(System.String)
extern void WXSDKManagerHandler__OnDeviceMotionChangeCallback_m88BFADF3DB6632BC20AE656BB82B52041CB6E011 (void);
// 0x000005AD System.Void WeChatWASM.WXSDKManagerHandler::m()
extern void WXSDKManagerHandler_m_m83F42FF857E123AE0B66971FF1CF28B10B1CFE66 (void);
// 0x000005AE System.Void WeChatWASM.WXSDKManagerHandler::OnDeviceMotionChange(System.Action`1<WeChatWASM.OnDeviceMotionChangeListenerResult>)
extern void WXSDKManagerHandler_OnDeviceMotionChange_m1681ACC9DD898C0A79230EF41618B4EA861FA02B (void);
// 0x000005AF System.Void WeChatWASM.WXSDKManagerHandler::N()
extern void WXSDKManagerHandler_N_m144F4215AE48D23E6D6E8B9A511A36408F355382 (void);
// 0x000005B0 System.Void WeChatWASM.WXSDKManagerHandler::OffDeviceMotionChange(System.Action`1<WeChatWASM.OnDeviceMotionChangeListenerResult>)
extern void WXSDKManagerHandler_OffDeviceMotionChange_m087564EFAB4063A618316CFAFB9ABFD923AC4244 (void);
// 0x000005B1 System.Void WeChatWASM.WXSDKManagerHandler::_OnDeviceOrientationChangeCallback(System.String)
extern void WXSDKManagerHandler__OnDeviceOrientationChangeCallback_m8806B0A205D0883BC93B859D99E431AD39EA71C2 (void);
// 0x000005B2 System.Void WeChatWASM.WXSDKManagerHandler::n()
extern void WXSDKManagerHandler_n_mD7A10C86B6BC1FBEC4A3B3FCF8108783281E6790 (void);
// 0x000005B3 System.Void WeChatWASM.WXSDKManagerHandler::OnDeviceOrientationChange(System.Action`1<WeChatWASM.OnDeviceOrientationChangeListenerResult>)
extern void WXSDKManagerHandler_OnDeviceOrientationChange_m4443B40E3010773178D373A8C9664B22D734CF79 (void);
// 0x000005B4 System.Void WeChatWASM.WXSDKManagerHandler::O()
extern void WXSDKManagerHandler_O_mC0A9D6D3517172F46D7897C49532CC8A304A1EE8 (void);
// 0x000005B5 System.Void WeChatWASM.WXSDKManagerHandler::OffDeviceOrientationChange(System.Action`1<WeChatWASM.OnDeviceOrientationChangeListenerResult>)
extern void WXSDKManagerHandler_OffDeviceOrientationChange_mFCA26CAEF946D6274605D12F88AEDA75961792F5 (void);
// 0x000005B6 System.Void WeChatWASM.WXSDKManagerHandler::_OnErrorCallback(System.String)
extern void WXSDKManagerHandler__OnErrorCallback_m6EE828BF1847F666C1F36B545DA59CDEFC6CE2E4 (void);
// 0x000005B7 System.Void WeChatWASM.WXSDKManagerHandler::o()
extern void WXSDKManagerHandler_o_mD7DDB19031981A0A164AC4EA08CA8B12E02FF280 (void);
// 0x000005B8 System.Void WeChatWASM.WXSDKManagerHandler::OnError(System.Action`1<WeChatWASM.WxOnErrorCallbackResult>)
extern void WXSDKManagerHandler_OnError_m4DCB20049CDBB1C47867A076AD2869F17A1D5F62 (void);
// 0x000005B9 System.Void WeChatWASM.WXSDKManagerHandler::P()
extern void WXSDKManagerHandler_P_m6C58E9E19522AA6CF2ADB558A10805F63F134D7A (void);
// 0x000005BA System.Void WeChatWASM.WXSDKManagerHandler::OffError(System.Action`1<WeChatWASM.WxOnErrorCallbackResult>)
extern void WXSDKManagerHandler_OffError_mA64741C89A9885A4C6F8067A5359543B2EFC3B80 (void);
// 0x000005BB System.Void WeChatWASM.WXSDKManagerHandler::_OnGyroscopeChangeCallback(System.String)
extern void WXSDKManagerHandler__OnGyroscopeChangeCallback_m9C4602D7D1DCA63E67A4413C45692E0AD52ECF42 (void);
// 0x000005BC System.Void WeChatWASM.WXSDKManagerHandler::p()
extern void WXSDKManagerHandler_p_mCF04FE4C687EB307CE090E89F59A025CAE5EE0EC (void);
// 0x000005BD System.Void WeChatWASM.WXSDKManagerHandler::OnGyroscopeChange(System.Action`1<WeChatWASM.OnGyroscopeChangeListenerResult>)
extern void WXSDKManagerHandler_OnGyroscopeChange_m92E80BF92E0EE85D5E8E3EFE1C9A2A926F410012 (void);
// 0x000005BE System.Void WeChatWASM.WXSDKManagerHandler::Q()
extern void WXSDKManagerHandler_Q_mE3CA348E0B9BE2C2DF7E7DCD418EA9C6AD73A4C2 (void);
// 0x000005BF System.Void WeChatWASM.WXSDKManagerHandler::OffGyroscopeChange(System.Action`1<WeChatWASM.OnGyroscopeChangeListenerResult>)
extern void WXSDKManagerHandler_OffGyroscopeChange_m983D798E9E61F484F01326B96ED411D85CAA6C26 (void);
// 0x000005C0 System.Void WeChatWASM.WXSDKManagerHandler::_OnHideCallback(System.String)
extern void WXSDKManagerHandler__OnHideCallback_mE46835F385B8C2AAF48C080429EF1EF664CFDF33 (void);
// 0x000005C1 System.Void WeChatWASM.WXSDKManagerHandler::q()
extern void WXSDKManagerHandler_q_m27A7B290404CE871F862F818B128EA4C22DE619D (void);
// 0x000005C2 System.Void WeChatWASM.WXSDKManagerHandler::OnHide(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WXSDKManagerHandler_OnHide_mE0295CD2EA6DA5879C628E5D27989EA5EC043F6F (void);
// 0x000005C3 System.Void WeChatWASM.WXSDKManagerHandler::R()
extern void WXSDKManagerHandler_R_m29DFF9C4EFC841DF6DF4F85450F002336EF86BFC (void);
// 0x000005C4 System.Void WeChatWASM.WXSDKManagerHandler::OffHide(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WXSDKManagerHandler_OffHide_mAC4467DE278EE929A8CDC12CBF8C6972D8408166 (void);
// 0x000005C5 System.Void WeChatWASM.WXSDKManagerHandler::_OnInteractiveStorageModifiedCallback(System.String)
extern void WXSDKManagerHandler__OnInteractiveStorageModifiedCallback_mD7787C14374B7A46B2B50E03665E3FAB2AF9097F (void);
// 0x000005C6 System.Void WeChatWASM.WXSDKManagerHandler::r()
extern void WXSDKManagerHandler_r_mFB2E8F675A3C088A23AB53E4AEDAB1FDB46ADB97 (void);
// 0x000005C7 System.Void WeChatWASM.WXSDKManagerHandler::OnInteractiveStorageModified(System.Action`1<System.String>)
extern void WXSDKManagerHandler_OnInteractiveStorageModified_mF69CF81C349AFF3D678EE295D065069504A37974 (void);
// 0x000005C8 System.Void WeChatWASM.WXSDKManagerHandler::S()
extern void WXSDKManagerHandler_S_mC2037D081B92308FC6B9968F8D485B56AF098C01 (void);
// 0x000005C9 System.Void WeChatWASM.WXSDKManagerHandler::OffInteractiveStorageModified(System.Action`1<System.String>)
extern void WXSDKManagerHandler_OffInteractiveStorageModified_mF7B7C717DC4F86363A5197177E620722A6FC8478 (void);
// 0x000005CA System.Void WeChatWASM.WXSDKManagerHandler::_OnKeyDownCallback(System.String)
extern void WXSDKManagerHandler__OnKeyDownCallback_mA02938C86252798B66B9A57A2433A6BE0F1A87FC (void);
// 0x000005CB System.Void WeChatWASM.WXSDKManagerHandler::s()
extern void WXSDKManagerHandler_s_m794DA6C96AF931FF63C074D67BC5A04376692444 (void);
// 0x000005CC System.Void WeChatWASM.WXSDKManagerHandler::OnKeyDown(System.Action`1<WeChatWASM.OnKeyDownListenerResult>)
extern void WXSDKManagerHandler_OnKeyDown_mACABE6824786A3B40EEEEABFD4A268DCDA092EF0 (void);
// 0x000005CD System.Void WeChatWASM.WXSDKManagerHandler::T()
extern void WXSDKManagerHandler_T_mC7551D4E7DB803F4470E7D9997F45EA720ADA24E (void);
// 0x000005CE System.Void WeChatWASM.WXSDKManagerHandler::OffKeyDown(System.Action`1<WeChatWASM.OnKeyDownListenerResult>)
extern void WXSDKManagerHandler_OffKeyDown_mA56F564EF9255EB159A83F46875FAA7C6EBEA884 (void);
// 0x000005CF System.Void WeChatWASM.WXSDKManagerHandler::_OnKeyUpCallback(System.String)
extern void WXSDKManagerHandler__OnKeyUpCallback_m3B1ADA5304FCC524537A2B0D9A6262539A64E104 (void);
// 0x000005D0 System.Void WeChatWASM.WXSDKManagerHandler::t()
extern void WXSDKManagerHandler_t_m21CA3D308A82BD68A3E371A0DFF7C83BD6A4B2C4 (void);
// 0x000005D1 System.Void WeChatWASM.WXSDKManagerHandler::OnKeyUp(System.Action`1<WeChatWASM.OnKeyDownListenerResult>)
extern void WXSDKManagerHandler_OnKeyUp_m0657008CEB0F105B67BED72BFA59CAFBBE7C70ED (void);
// 0x000005D2 System.Void WeChatWASM.WXSDKManagerHandler::U()
extern void WXSDKManagerHandler_U_m671E1BB0A594AE06F4A37E592CC674520E88BE6A (void);
// 0x000005D3 System.Void WeChatWASM.WXSDKManagerHandler::OffKeyUp(System.Action`1<WeChatWASM.OnKeyDownListenerResult>)
extern void WXSDKManagerHandler_OffKeyUp_m749301A0E38909F9358D42C819A4B61102D4998F (void);
// 0x000005D4 System.Void WeChatWASM.WXSDKManagerHandler::_OnKeyboardCompleteCallback(System.String)
extern void WXSDKManagerHandler__OnKeyboardCompleteCallback_mCCA19500F62D7E3981AFBB45BBC540A9D5292FFC (void);
// 0x000005D5 System.Void WeChatWASM.WXSDKManagerHandler::u()
extern void WXSDKManagerHandler_u_m0114F53AFFCA6F668CE874BA529B5BE0FEFE7A70 (void);
// 0x000005D6 System.Void WeChatWASM.WXSDKManagerHandler::OnKeyboardComplete(System.Action`1<WeChatWASM.OnKeyboardInputListenerResult>)
extern void WXSDKManagerHandler_OnKeyboardComplete_m66E8D797499A8A5B9EFF81516211CD96B82D2C2B (void);
// 0x000005D7 System.Void WeChatWASM.WXSDKManagerHandler::V()
extern void WXSDKManagerHandler_V_m80E1EBBA98C58A09ED12614ECFD95AF5C62D8F68 (void);
// 0x000005D8 System.Void WeChatWASM.WXSDKManagerHandler::OffKeyboardComplete(System.Action`1<WeChatWASM.OnKeyboardInputListenerResult>)
extern void WXSDKManagerHandler_OffKeyboardComplete_m8EF21F86FC0134727D95F835680C295B232BEFFF (void);
// 0x000005D9 System.Void WeChatWASM.WXSDKManagerHandler::_OnKeyboardConfirmCallback(System.String)
extern void WXSDKManagerHandler__OnKeyboardConfirmCallback_m1930F91E732AF0D4B417577511498864BBAB9715 (void);
// 0x000005DA System.Void WeChatWASM.WXSDKManagerHandler::v()
extern void WXSDKManagerHandler_v_m54046C5AE17653497A2FD579DEBC85A7CBB1077D (void);
// 0x000005DB System.Void WeChatWASM.WXSDKManagerHandler::OnKeyboardConfirm(System.Action`1<WeChatWASM.OnKeyboardInputListenerResult>)
extern void WXSDKManagerHandler_OnKeyboardConfirm_m53678BCEDBAF96EF125A25F078B673C1D72C596F (void);
// 0x000005DC System.Void WeChatWASM.WXSDKManagerHandler::W()
extern void WXSDKManagerHandler_W_m231559870983F6B0E2F14E6B913398566841418E (void);
// 0x000005DD System.Void WeChatWASM.WXSDKManagerHandler::OffKeyboardConfirm(System.Action`1<WeChatWASM.OnKeyboardInputListenerResult>)
extern void WXSDKManagerHandler_OffKeyboardConfirm_mB8DFE3AEA79B8DAEA275B76E0216A67D3B7493D0 (void);
// 0x000005DE System.Void WeChatWASM.WXSDKManagerHandler::_OnKeyboardHeightChangeCallback(System.String)
extern void WXSDKManagerHandler__OnKeyboardHeightChangeCallback_m148D4908D304C88EA0060E366D10B0122143346C (void);
// 0x000005DF System.Void WeChatWASM.WXSDKManagerHandler::w()
extern void WXSDKManagerHandler_w_mE82965035247638AEDDDE1C2E4F26437EFB476D9 (void);
// 0x000005E0 System.Void WeChatWASM.WXSDKManagerHandler::OnKeyboardHeightChange(System.Action`1<WeChatWASM.OnKeyboardHeightChangeListenerResult>)
extern void WXSDKManagerHandler_OnKeyboardHeightChange_m91A752B9FEB320FDE68814B0D7AF55A2365BF46C (void);
// 0x000005E1 System.Void WeChatWASM.WXSDKManagerHandler::X()
extern void WXSDKManagerHandler_X_mF59EA06F648838BDAF7C11D4F8E15418B2D2D053 (void);
// 0x000005E2 System.Void WeChatWASM.WXSDKManagerHandler::OffKeyboardHeightChange(System.Action`1<WeChatWASM.OnKeyboardHeightChangeListenerResult>)
extern void WXSDKManagerHandler_OffKeyboardHeightChange_mEF72404DF61DA563E72B8264242C387C2935A885 (void);
// 0x000005E3 System.Void WeChatWASM.WXSDKManagerHandler::_OnKeyboardInputCallback(System.String)
extern void WXSDKManagerHandler__OnKeyboardInputCallback_m2D3E42D1EC10EFB209B8A343B5C3BD6A27F289D9 (void);
// 0x000005E4 System.Void WeChatWASM.WXSDKManagerHandler::x()
extern void WXSDKManagerHandler_x_mCDE16AAD668994A8340B76591A29BE9BC1EADE16 (void);
// 0x000005E5 System.Void WeChatWASM.WXSDKManagerHandler::OnKeyboardInput(System.Action`1<WeChatWASM.OnKeyboardInputListenerResult>)
extern void WXSDKManagerHandler_OnKeyboardInput_m62A9277797AAE1804822A949D64A9A4693E61329 (void);
// 0x000005E6 System.Void WeChatWASM.WXSDKManagerHandler::Y()
extern void WXSDKManagerHandler_Y_mEE01E236014BCCB3D80E81D51E7DE26270847892 (void);
// 0x000005E7 System.Void WeChatWASM.WXSDKManagerHandler::OffKeyboardInput(System.Action`1<WeChatWASM.OnKeyboardInputListenerResult>)
extern void WXSDKManagerHandler_OffKeyboardInput_m8438E36876DA04E1F0279E1E92DF0896C1B115EB (void);
// 0x000005E8 System.Void WeChatWASM.WXSDKManagerHandler::_OnMemoryWarningCallback(System.String)
extern void WXSDKManagerHandler__OnMemoryWarningCallback_m06D1C80E97BE2B37A67994AB6F6BD8FDF6C8408A (void);
// 0x000005E9 System.Void WeChatWASM.WXSDKManagerHandler::y()
extern void WXSDKManagerHandler_y_mEACF022CFBBEA92FB3EE1BC6207763F426AD6DAE (void);
// 0x000005EA System.Void WeChatWASM.WXSDKManagerHandler::OnMemoryWarning(System.Action`1<WeChatWASM.OnMemoryWarningListenerResult>)
extern void WXSDKManagerHandler_OnMemoryWarning_m5435D411727CFE96F30DF65DABA788365924675F (void);
// 0x000005EB System.Void WeChatWASM.WXSDKManagerHandler::Z()
extern void WXSDKManagerHandler_Z_m8538EDB0DD72477145AD8A5BF22F8FB3645D84F1 (void);
// 0x000005EC System.Void WeChatWASM.WXSDKManagerHandler::OffMemoryWarning(System.Action`1<WeChatWASM.OnMemoryWarningListenerResult>)
extern void WXSDKManagerHandler_OffMemoryWarning_mA3C734B5E6FE22DA7D50B5A7EA3388D3A411BA0E (void);
// 0x000005ED System.Void WeChatWASM.WXSDKManagerHandler::_OnMessageCallback(System.String)
extern void WXSDKManagerHandler__OnMessageCallback_mB501DCA2C6A4087FF8E232DBA33CE6BD80A2709E (void);
// 0x000005EE System.Void WeChatWASM.WXSDKManagerHandler::z()
extern void WXSDKManagerHandler_z_mE6A28A440FC91134A9E7F9A3FF8A945BC9B304B0 (void);
// 0x000005EF System.Void WeChatWASM.WXSDKManagerHandler::OnMessage(System.Action`1<System.String>)
extern void WXSDKManagerHandler_OnMessage_m66553AA3D278D68AEBF68FC0E6A092C8AFF85C3D (void);
// 0x000005F0 System.Void WeChatWASM.WXSDKManagerHandler::_OnMouseDownCallback(System.String)
extern void WXSDKManagerHandler__OnMouseDownCallback_mD2AA5DC82737C650360B5B95A2607EEB22039455 (void);
// 0x000005F1 System.Void WeChatWASM.WXSDKManagerHandler::aA()
extern void WXSDKManagerHandler_aA_m8133CD74F7888EC8CC009AC2C741084881603CEC (void);
// 0x000005F2 System.Void WeChatWASM.WXSDKManagerHandler::OnMouseDown(System.Action`1<WeChatWASM.OnMouseDownListenerResult>)
extern void WXSDKManagerHandler_OnMouseDown_mA74E84744C7E19ACFE30752CCAD27C280FDFCD6C (void);
// 0x000005F3 System.Void WeChatWASM.WXSDKManagerHandler::aa()
extern void WXSDKManagerHandler_aa_m643C49D87A28A9687AB81D7592A4CD8385B97574 (void);
// 0x000005F4 System.Void WeChatWASM.WXSDKManagerHandler::OffMouseDown(System.Action`1<WeChatWASM.OnMouseDownListenerResult>)
extern void WXSDKManagerHandler_OffMouseDown_mD5B09949A20E497A779521571F49998A77FE7393 (void);
// 0x000005F5 System.Void WeChatWASM.WXSDKManagerHandler::_OnMouseMoveCallback(System.String)
extern void WXSDKManagerHandler__OnMouseMoveCallback_m1322B38E89690638E46F71EEF529B60FB1AE7E21 (void);
// 0x000005F6 System.Void WeChatWASM.WXSDKManagerHandler::aB()
extern void WXSDKManagerHandler_aB_mCE5EE5E29EA4FC042B3E6A3E436EB384E8620B4C (void);
// 0x000005F7 System.Void WeChatWASM.WXSDKManagerHandler::OnMouseMove(System.Action`1<WeChatWASM.OnMouseMoveListenerResult>)
extern void WXSDKManagerHandler_OnMouseMove_m9AC72FF5F1B0183AC4ECE6AF9DE644EE0742FD45 (void);
// 0x000005F8 System.Void WeChatWASM.WXSDKManagerHandler::ab()
extern void WXSDKManagerHandler_ab_m6F751F13D8353456E21767897C83C2ABDD22543D (void);
// 0x000005F9 System.Void WeChatWASM.WXSDKManagerHandler::OffMouseMove(System.Action`1<WeChatWASM.OnMouseMoveListenerResult>)
extern void WXSDKManagerHandler_OffMouseMove_m69B39205D7BDF931917DB5398D9E87C88F32EF1D (void);
// 0x000005FA System.Void WeChatWASM.WXSDKManagerHandler::_OnMouseUpCallback(System.String)
extern void WXSDKManagerHandler__OnMouseUpCallback_mA3B7EE65F6B2A8DB5D43A1EF88F3C9186C45326D (void);
// 0x000005FB System.Void WeChatWASM.WXSDKManagerHandler::aC()
extern void WXSDKManagerHandler_aC_m6938B24887B5042FCDD55D032F484A9C750D9CB0 (void);
// 0x000005FC System.Void WeChatWASM.WXSDKManagerHandler::OnMouseUp(System.Action`1<WeChatWASM.OnMouseDownListenerResult>)
extern void WXSDKManagerHandler_OnMouseUp_m561420B3C664A5823AF0C09D4205586B0C9045F3 (void);
// 0x000005FD System.Void WeChatWASM.WXSDKManagerHandler::ac()
extern void WXSDKManagerHandler_ac_mAA51313F1DC57F7914CF7253E81D7D9AED41E572 (void);
// 0x000005FE System.Void WeChatWASM.WXSDKManagerHandler::OffMouseUp(System.Action`1<WeChatWASM.OnMouseDownListenerResult>)
extern void WXSDKManagerHandler_OffMouseUp_m97C38EBAAB8DBD8FC16E41794DF528446CC13368 (void);
// 0x000005FF System.Void WeChatWASM.WXSDKManagerHandler::_OnNetworkStatusChangeCallback(System.String)
extern void WXSDKManagerHandler__OnNetworkStatusChangeCallback_mE0272AD6C05165BDD36ACBE3F9C0CDD89D282E0D (void);
// 0x00000600 System.Void WeChatWASM.WXSDKManagerHandler::aD()
extern void WXSDKManagerHandler_aD_m7E36EAAC510240636EEE4E8656A628A738969B76 (void);
// 0x00000601 System.Void WeChatWASM.WXSDKManagerHandler::OnNetworkStatusChange(System.Action`1<WeChatWASM.OnNetworkStatusChangeListenerResult>)
extern void WXSDKManagerHandler_OnNetworkStatusChange_m9F3B564BA8A51AD9BCF25D386C567E1D0649488B (void);
// 0x00000602 System.Void WeChatWASM.WXSDKManagerHandler::ad()
extern void WXSDKManagerHandler_ad_m85AA747D74D6C7D147109167FD0C58D8259F0FA7 (void);
// 0x00000603 System.Void WeChatWASM.WXSDKManagerHandler::OffNetworkStatusChange(System.Action`1<WeChatWASM.OnNetworkStatusChangeListenerResult>)
extern void WXSDKManagerHandler_OffNetworkStatusChange_m9D158BA4322DD5EF9C451FFF9BD7B302E0D19273 (void);
// 0x00000604 System.Void WeChatWASM.WXSDKManagerHandler::_OnNetworkWeakChangeCallback(System.String)
extern void WXSDKManagerHandler__OnNetworkWeakChangeCallback_m295999961C89CE29772CDBE3D906D835E34F363D (void);
// 0x00000605 System.Void WeChatWASM.WXSDKManagerHandler::aE()
extern void WXSDKManagerHandler_aE_m2FCC0675457269513E1AFDC2CEA25F0DDD5FCFFD (void);
// 0x00000606 System.Void WeChatWASM.WXSDKManagerHandler::OnNetworkWeakChange(System.Action`1<WeChatWASM.OnNetworkWeakChangeListenerResult>)
extern void WXSDKManagerHandler_OnNetworkWeakChange_mDEC850E3CC170D80CF1F4378A55A5196CE2BCCBF (void);
// 0x00000607 System.Void WeChatWASM.WXSDKManagerHandler::ae()
extern void WXSDKManagerHandler_ae_m9D3418D0BC0B483CA863B8AC99459FC25D4124A7 (void);
// 0x00000608 System.Void WeChatWASM.WXSDKManagerHandler::OffNetworkWeakChange(System.Action`1<WeChatWASM.OnNetworkWeakChangeListenerResult>)
extern void WXSDKManagerHandler_OffNetworkWeakChange_m7FBC836A2090B1A8756246238EF0CE2A672ABF66 (void);
// 0x00000609 System.Void WeChatWASM.WXSDKManagerHandler::_OnShareMessageToFriendCallback(System.String)
extern void WXSDKManagerHandler__OnShareMessageToFriendCallback_mC424F155EF5659868A0FDD6C7D492C502DB9210B (void);
// 0x0000060A System.Void WeChatWASM.WXSDKManagerHandler::aF()
extern void WXSDKManagerHandler_aF_mB23EB190411FB222E4047DAB2722BA57004FB1FF (void);
// 0x0000060B System.Void WeChatWASM.WXSDKManagerHandler::OnShareMessageToFriend(System.Action`1<WeChatWASM.OnShareMessageToFriendListenerResult>)
extern void WXSDKManagerHandler_OnShareMessageToFriend_m8D8C2C51256081BE68EC3262C9501F2014222E7A (void);
// 0x0000060C System.Void WeChatWASM.WXSDKManagerHandler::_OnShowCallback(System.String)
extern void WXSDKManagerHandler__OnShowCallback_m3175A889587D7F7FFEF8F2B88ECC138DFC0B88D8 (void);
// 0x0000060D System.Void WeChatWASM.WXSDKManagerHandler::af()
extern void WXSDKManagerHandler_af_m9E2A1BB9879033F71B8D554474B011ADA848BC34 (void);
// 0x0000060E System.Void WeChatWASM.WXSDKManagerHandler::OnShow(System.Action`1<WeChatWASM.OnShowListenerResult>)
extern void WXSDKManagerHandler_OnShow_m9DE2965371D03BDC11B95EFE19A20E09292F0D5D (void);
// 0x0000060F System.Void WeChatWASM.WXSDKManagerHandler::aG()
extern void WXSDKManagerHandler_aG_mD2868816EC0802C009D82416E4F66331B777E3F5 (void);
// 0x00000610 System.Void WeChatWASM.WXSDKManagerHandler::OffShow(System.Action`1<WeChatWASM.OnShowListenerResult>)
extern void WXSDKManagerHandler_OffShow_m5608BCB9EB4289F6B30A4805BFF8B4F953EA3201 (void);
// 0x00000611 System.Void WeChatWASM.WXSDKManagerHandler::_OnTouchCancelCallback(System.String)
extern void WXSDKManagerHandler__OnTouchCancelCallback_mD2BCB6A7CE87B5B0593E446718D69C9C2B935D45 (void);
// 0x00000612 System.Void WeChatWASM.WXSDKManagerHandler::ag()
extern void WXSDKManagerHandler_ag_mB884D480B7E781FEF8074F11BE99D9F86209A4B3 (void);
// 0x00000613 System.Void WeChatWASM.WXSDKManagerHandler::OnTouchCancel(System.Action`1<WeChatWASM.OnTouchStartListenerResult>)
extern void WXSDKManagerHandler_OnTouchCancel_m42ACACCEBE7FDEC18561631FE6515FA18B455CD8 (void);
// 0x00000614 System.Void WeChatWASM.WXSDKManagerHandler::aH()
extern void WXSDKManagerHandler_aH_m4A643B3FA64C2F7AA67D7B23F2513B9FAA05DF7D (void);
// 0x00000615 System.Void WeChatWASM.WXSDKManagerHandler::OffTouchCancel(System.Action`1<WeChatWASM.OnTouchStartListenerResult>)
extern void WXSDKManagerHandler_OffTouchCancel_mE9BB34BECD58A18BD3873E2B54E1938F7211450C (void);
// 0x00000616 System.Void WeChatWASM.WXSDKManagerHandler::_OnTouchEndCallback(System.String)
extern void WXSDKManagerHandler__OnTouchEndCallback_m6502E8F383A41F57B7F0A236599750DD57D91B9E (void);
// 0x00000617 System.Void WeChatWASM.WXSDKManagerHandler::ah()
extern void WXSDKManagerHandler_ah_m2EFC560C37B72BEA3D6593A71077365DD1D6EBB1 (void);
// 0x00000618 System.Void WeChatWASM.WXSDKManagerHandler::OnTouchEnd(System.Action`1<WeChatWASM.OnTouchStartListenerResult>)
extern void WXSDKManagerHandler_OnTouchEnd_m66D8F9DA36330956AB724F675AA90CF7F1C8389D (void);
// 0x00000619 System.Void WeChatWASM.WXSDKManagerHandler::aI()
extern void WXSDKManagerHandler_aI_mAD755466D841E547CFBCF81474D8D506EFE53B69 (void);
// 0x0000061A System.Void WeChatWASM.WXSDKManagerHandler::OffTouchEnd(System.Action`1<WeChatWASM.OnTouchStartListenerResult>)
extern void WXSDKManagerHandler_OffTouchEnd_m7A73754363DC4A24CAB3C029D287178524F43FBD (void);
// 0x0000061B System.Void WeChatWASM.WXSDKManagerHandler::_OnTouchMoveCallback(System.String)
extern void WXSDKManagerHandler__OnTouchMoveCallback_m9B8986C61D862C306BCABD7FEA468A652CAB9288 (void);
// 0x0000061C System.Void WeChatWASM.WXSDKManagerHandler::ai()
extern void WXSDKManagerHandler_ai_m34B97A9CD32D9988184F829D79F8F82AB7C7EEB7 (void);
// 0x0000061D System.Void WeChatWASM.WXSDKManagerHandler::OnTouchMove(System.Action`1<WeChatWASM.OnTouchStartListenerResult>)
extern void WXSDKManagerHandler_OnTouchMove_mA50021CA5E9E9313B851ACD5E9327F4207BD4A9F (void);
// 0x0000061E System.Void WeChatWASM.WXSDKManagerHandler::aJ()
extern void WXSDKManagerHandler_aJ_mD7B6BF703E78AC921C9A38816FFF33F95069E206 (void);
// 0x0000061F System.Void WeChatWASM.WXSDKManagerHandler::OffTouchMove(System.Action`1<WeChatWASM.OnTouchStartListenerResult>)
extern void WXSDKManagerHandler_OffTouchMove_m1B383FFA68D86BD384B36FE9FB6962ECF1A5A209 (void);
// 0x00000620 System.Void WeChatWASM.WXSDKManagerHandler::_OnTouchStartCallback(System.String)
extern void WXSDKManagerHandler__OnTouchStartCallback_mC7B8FA04783D908C3ABAF773664439218FEBC1BF (void);
// 0x00000621 System.Void WeChatWASM.WXSDKManagerHandler::aj()
extern void WXSDKManagerHandler_aj_m3EF64051F689A9F8553A2BB8774E3600ECA5285E (void);
// 0x00000622 System.Void WeChatWASM.WXSDKManagerHandler::OnTouchStart(System.Action`1<WeChatWASM.OnTouchStartListenerResult>)
extern void WXSDKManagerHandler_OnTouchStart_mD53382040A38D0E576BCD7E9A5F5600093251D1B (void);
// 0x00000623 System.Void WeChatWASM.WXSDKManagerHandler::aK()
extern void WXSDKManagerHandler_aK_m940161F17383C4934669BBEC98EECBA633D5729B (void);
// 0x00000624 System.Void WeChatWASM.WXSDKManagerHandler::OffTouchStart(System.Action`1<WeChatWASM.OnTouchStartListenerResult>)
extern void WXSDKManagerHandler_OffTouchStart_mA850745FBBFCB67D814200EE6834A5AF64FDE8E6 (void);
// 0x00000625 System.Void WeChatWASM.WXSDKManagerHandler::_OnUnhandledRejectionCallback(System.String)
extern void WXSDKManagerHandler__OnUnhandledRejectionCallback_m218EFE0F3F4FB600B9B213A110DA9E4805A7076E (void);
// 0x00000626 System.Void WeChatWASM.WXSDKManagerHandler::ak()
extern void WXSDKManagerHandler_ak_m48BF64C1A649DAB0179A3BAD54126AE75DB56FE4 (void);
// 0x00000627 System.Void WeChatWASM.WXSDKManagerHandler::OnUnhandledRejection(System.Action`1<WeChatWASM.OnUnhandledRejectionListenerResult>)
extern void WXSDKManagerHandler_OnUnhandledRejection_mBDE99E127FBAF9996CE4D61268D5C2CE56EE882A (void);
// 0x00000628 System.Void WeChatWASM.WXSDKManagerHandler::aL()
extern void WXSDKManagerHandler_aL_mE29798398E9FE74CE290AACEFBBEFA9C8282B20F (void);
// 0x00000629 System.Void WeChatWASM.WXSDKManagerHandler::OffUnhandledRejection(System.Action`1<WeChatWASM.OnUnhandledRejectionListenerResult>)
extern void WXSDKManagerHandler_OffUnhandledRejection_mC0A11ACED43E08D9710AD3BF07FF4D1A102B8EB0 (void);
// 0x0000062A System.Void WeChatWASM.WXSDKManagerHandler::_OnUserCaptureScreenCallback(System.String)
extern void WXSDKManagerHandler__OnUserCaptureScreenCallback_mC1D74C57301BC68FAC65439B624F26EAFD3F1247 (void);
// 0x0000062B System.Void WeChatWASM.WXSDKManagerHandler::al()
extern void WXSDKManagerHandler_al_m2C82078DD71DCC7D58C85D6DC9E28E2D38772445 (void);
// 0x0000062C System.Void WeChatWASM.WXSDKManagerHandler::OnUserCaptureScreen(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WXSDKManagerHandler_OnUserCaptureScreen_m0AB1B9A5436DF3F8F307FA6CE22F1E5BF919F0DE (void);
// 0x0000062D System.Void WeChatWASM.WXSDKManagerHandler::aM()
extern void WXSDKManagerHandler_aM_m01A532F5A8C328764E379174C9CCE7705CB825A7 (void);
// 0x0000062E System.Void WeChatWASM.WXSDKManagerHandler::OffUserCaptureScreen(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WXSDKManagerHandler_OffUserCaptureScreen_mB19A73BE4517CEC412236B313ABF7834E4EC4072 (void);
// 0x0000062F System.Void WeChatWASM.WXSDKManagerHandler::_OnVoIPChatInterruptedCallback(System.String)
extern void WXSDKManagerHandler__OnVoIPChatInterruptedCallback_m10A9B470C560F1DD4E9D43208C383713C06AE8CA (void);
// 0x00000630 System.Void WeChatWASM.WXSDKManagerHandler::am()
extern void WXSDKManagerHandler_am_m453D7C798FD6B85DC0DDCDB68C7316959D313457 (void);
// 0x00000631 System.Void WeChatWASM.WXSDKManagerHandler::OnVoIPChatInterrupted(System.Action`1<WeChatWASM.OnVoIPChatInterruptedListenerResult>)
extern void WXSDKManagerHandler_OnVoIPChatInterrupted_m7EA362B4EFE4B958446DCBC0B859C8540B43DEBC (void);
// 0x00000632 System.Void WeChatWASM.WXSDKManagerHandler::aN()
extern void WXSDKManagerHandler_aN_mAE87CB23E67934BE54DE86BC137DB6CAFC74B305 (void);
// 0x00000633 System.Void WeChatWASM.WXSDKManagerHandler::OffVoIPChatInterrupted(System.Action`1<WeChatWASM.OnVoIPChatInterruptedListenerResult>)
extern void WXSDKManagerHandler_OffVoIPChatInterrupted_m56301CF5CD64BC41CFEC53C93D3EECAFEFD812E5 (void);
// 0x00000634 System.Void WeChatWASM.WXSDKManagerHandler::_OnVoIPChatMembersChangedCallback(System.String)
extern void WXSDKManagerHandler__OnVoIPChatMembersChangedCallback_m0B501AFEF8529B559444309C2153AB043E96357F (void);
// 0x00000635 System.Void WeChatWASM.WXSDKManagerHandler::an()
extern void WXSDKManagerHandler_an_m13E2B525562A8694883994EAC5420E3D4D90BA63 (void);
// 0x00000636 System.Void WeChatWASM.WXSDKManagerHandler::OnVoIPChatMembersChanged(System.Action`1<WeChatWASM.OnVoIPChatMembersChangedListenerResult>)
extern void WXSDKManagerHandler_OnVoIPChatMembersChanged_mCDB00100D9009E7AB92C6090D83E5A8734C530B7 (void);
// 0x00000637 System.Void WeChatWASM.WXSDKManagerHandler::aO()
extern void WXSDKManagerHandler_aO_m387EB50209ECFDFDA55BBE64D9607895785D5FB6 (void);
// 0x00000638 System.Void WeChatWASM.WXSDKManagerHandler::OffVoIPChatMembersChanged(System.Action`1<WeChatWASM.OnVoIPChatMembersChangedListenerResult>)
extern void WXSDKManagerHandler_OffVoIPChatMembersChanged_m37F8D37370831D12A327A1302DC77F95903ED1ED (void);
// 0x00000639 System.Void WeChatWASM.WXSDKManagerHandler::_OnVoIPChatSpeakersChangedCallback(System.String)
extern void WXSDKManagerHandler__OnVoIPChatSpeakersChangedCallback_m8E66B07DEE04D10F1B6C854CF8012F80132486F5 (void);
// 0x0000063A System.Void WeChatWASM.WXSDKManagerHandler::ao()
extern void WXSDKManagerHandler_ao_mC9283AB0C18F01D6B2F2DC675E9D841F6DD38A2E (void);
// 0x0000063B System.Void WeChatWASM.WXSDKManagerHandler::OnVoIPChatSpeakersChanged(System.Action`1<WeChatWASM.OnVoIPChatSpeakersChangedListenerResult>)
extern void WXSDKManagerHandler_OnVoIPChatSpeakersChanged_mC93117666F01013AF53E0541C6B8307574777DE5 (void);
// 0x0000063C System.Void WeChatWASM.WXSDKManagerHandler::aP()
extern void WXSDKManagerHandler_aP_mDE46D2D1D3D93E4918D0A2861DC4266E5CBFD5E4 (void);
// 0x0000063D System.Void WeChatWASM.WXSDKManagerHandler::OffVoIPChatSpeakersChanged(System.Action`1<WeChatWASM.OnVoIPChatSpeakersChangedListenerResult>)
extern void WXSDKManagerHandler_OffVoIPChatSpeakersChanged_mBE47054258FFE22487BF9749DE26FCC537E424E9 (void);
// 0x0000063E System.Void WeChatWASM.WXSDKManagerHandler::_OnVoIPChatStateChangedCallback(System.String)
extern void WXSDKManagerHandler__OnVoIPChatStateChangedCallback_m04C0DCA4969EDA1F08DB832C0C124748CC0A0630 (void);
// 0x0000063F System.Void WeChatWASM.WXSDKManagerHandler::ap()
extern void WXSDKManagerHandler_ap_mBEF20844EF7B71BF11E9DE133B44E6E5BE98A8C2 (void);
// 0x00000640 System.Void WeChatWASM.WXSDKManagerHandler::OnVoIPChatStateChanged(System.Action`1<WeChatWASM.OnVoIPChatStateChangedListenerResult>)
extern void WXSDKManagerHandler_OnVoIPChatStateChanged_mE9893ABEC2B8F58397721472C40278C8D5028531 (void);
// 0x00000641 System.Void WeChatWASM.WXSDKManagerHandler::aQ()
extern void WXSDKManagerHandler_aQ_m3779B23550EE7A40CF1805403E5368954F0F8C70 (void);
// 0x00000642 System.Void WeChatWASM.WXSDKManagerHandler::OffVoIPChatStateChanged(System.Action`1<WeChatWASM.OnVoIPChatStateChangedListenerResult>)
extern void WXSDKManagerHandler_OffVoIPChatStateChanged_mFF30DADAEC4C2D359B97A80282186F207AA67F3C (void);
// 0x00000643 System.Void WeChatWASM.WXSDKManagerHandler::_OnWheelCallback(System.String)
extern void WXSDKManagerHandler__OnWheelCallback_mC7F6B1D2A6FAC7A6191040B400FBD5EDABC3AF42 (void);
// 0x00000644 System.Void WeChatWASM.WXSDKManagerHandler::aq()
extern void WXSDKManagerHandler_aq_m6B94FA406E387933B9F9E3668D60AA4380694B34 (void);
// 0x00000645 System.Void WeChatWASM.WXSDKManagerHandler::OnWheel(System.Action`1<WeChatWASM.OnWheelListenerResult>)
extern void WXSDKManagerHandler_OnWheel_m20030B80167760F6A082DA79C6EEA32CBD5231AE (void);
// 0x00000646 System.Void WeChatWASM.WXSDKManagerHandler::aR()
extern void WXSDKManagerHandler_aR_mE669CE38A57EC77BD8AB7A07694C190BC946FCDC (void);
// 0x00000647 System.Void WeChatWASM.WXSDKManagerHandler::OffWheel(System.Action`1<WeChatWASM.OnWheelListenerResult>)
extern void WXSDKManagerHandler_OffWheel_mD039BCE1155F9F50A90214FEC630E7B3CBFB77C1 (void);
// 0x00000648 System.Void WeChatWASM.WXSDKManagerHandler::_OnWindowResizeCallback(System.String)
extern void WXSDKManagerHandler__OnWindowResizeCallback_m8BB992E407F89A60301D6BAC39636DB44EFFB0BD (void);
// 0x00000649 System.Void WeChatWASM.WXSDKManagerHandler::ar()
extern void WXSDKManagerHandler_ar_m9BA5873785244D72F439DCFF88277F03417E98B0 (void);
// 0x0000064A System.Void WeChatWASM.WXSDKManagerHandler::OnWindowResize(System.Action`1<WeChatWASM.OnWindowResizeListenerResult>)
extern void WXSDKManagerHandler_OnWindowResize_m727AD53CD53EE46485AB4CF2E3B18D22C69C7E70 (void);
// 0x0000064B System.Void WeChatWASM.WXSDKManagerHandler::aS()
extern void WXSDKManagerHandler_aS_mFD5E61AD92C1A72BF5F7B6A3211713E48E41D590 (void);
// 0x0000064C System.Void WeChatWASM.WXSDKManagerHandler::OffWindowResize(System.Action`1<WeChatWASM.OnWindowResizeListenerResult>)
extern void WXSDKManagerHandler_OffWindowResize_mE2705B8986577AD6C8C5438C5B6318CBE7328467 (void);
// 0x0000064D System.Void WeChatWASM.WXSDKManagerHandler::_OnAddToFavoritesCallback(System.String)
extern void WXSDKManagerHandler__OnAddToFavoritesCallback_m2C1095C7DA27E6FA04AEDCF8FD08065831669480 (void);
// 0x0000064E System.Void WeChatWASM.WXSDKManagerHandler::D(System.String)
extern void WXSDKManagerHandler_D_m94A72FFFB712C05E51AA33064EBD630B68434DCD (void);
// 0x0000064F System.Void WeChatWASM.WXSDKManagerHandler::as()
extern void WXSDKManagerHandler_as_mA4421F9435177771E1D5740AD03044C681D9EB22 (void);
// 0x00000650 System.Void WeChatWASM.WXSDKManagerHandler::OnAddToFavorites(System.Action`1<System.Action`1<WeChatWASM.OnAddToFavoritesListenerResult>>)
extern void WXSDKManagerHandler_OnAddToFavorites_m75F170D82F754AD9D0A9FB6648678D6087420929 (void);
// 0x00000651 System.Void WeChatWASM.WXSDKManagerHandler::aT()
extern void WXSDKManagerHandler_aT_m125E851AA77A19A3D53045DF883F8040D0F414D7 (void);
// 0x00000652 System.Void WeChatWASM.WXSDKManagerHandler::OffAddToFavorites(System.Action`1<System.Action`1<WeChatWASM.OnAddToFavoritesListenerResult>>)
extern void WXSDKManagerHandler_OffAddToFavorites_m177A5345C7B4BBB0D805E199B52A64395C207007 (void);
// 0x00000653 System.Void WeChatWASM.WXSDKManagerHandler::_OnCopyUrlCallback(System.String)
extern void WXSDKManagerHandler__OnCopyUrlCallback_mCEA60FD7C8C4166B2544BA4898DFC8E1B856A677 (void);
// 0x00000654 System.Void WeChatWASM.WXSDKManagerHandler::d(System.String)
extern void WXSDKManagerHandler_d_mA11F3D969B8155C33256D93268ADF20DD8613F36 (void);
// 0x00000655 System.Void WeChatWASM.WXSDKManagerHandler::at()
extern void WXSDKManagerHandler_at_m6C027FAF694FAF703C74AB224D76287CA180B584 (void);
// 0x00000656 System.Void WeChatWASM.WXSDKManagerHandler::OnCopyUrl(System.Action`1<System.Action`1<WeChatWASM.OnCopyUrlListenerResult>>)
extern void WXSDKManagerHandler_OnCopyUrl_mF072168F2C7571EB3454E0461DDAAE4193920813 (void);
// 0x00000657 System.Void WeChatWASM.WXSDKManagerHandler::aU()
extern void WXSDKManagerHandler_aU_mE65DBF4313D2869D31851FE4DDE3E58F48AB25BF (void);
// 0x00000658 System.Void WeChatWASM.WXSDKManagerHandler::OffCopyUrl(System.Action`1<System.Action`1<WeChatWASM.OnCopyUrlListenerResult>>)
extern void WXSDKManagerHandler_OffCopyUrl_m1081939215289858CFDDCDC07787F713E75B3135 (void);
// 0x00000659 System.Void WeChatWASM.WXSDKManagerHandler::_OnHandoffCallback(System.String)
extern void WXSDKManagerHandler__OnHandoffCallback_mC8E849BFCBFF49135E2D13235C77A2565B17DED9 (void);
// 0x0000065A System.Void WeChatWASM.WXSDKManagerHandler::E(System.String)
extern void WXSDKManagerHandler_E_mFFC9EC75C6FAA47B3C3F9DC14D40780816DF0F87 (void);
// 0x0000065B System.Void WeChatWASM.WXSDKManagerHandler::au()
extern void WXSDKManagerHandler_au_m8C9F23184E26B2888E65814B17A4CF964234714E (void);
// 0x0000065C System.Void WeChatWASM.WXSDKManagerHandler::OnHandoff(System.Action`1<System.Action`1<WeChatWASM.OnHandoffListenerResult>>)
extern void WXSDKManagerHandler_OnHandoff_m9CEC8E2C9A011CA073C172D3CCA7A104A2ABCA36 (void);
// 0x0000065D System.Void WeChatWASM.WXSDKManagerHandler::aV()
extern void WXSDKManagerHandler_aV_mA3659C5249BAAEA007988EAC727E46BBA49B32E4 (void);
// 0x0000065E System.Void WeChatWASM.WXSDKManagerHandler::OffHandoff(System.Action`1<System.Action`1<WeChatWASM.OnHandoffListenerResult>>)
extern void WXSDKManagerHandler_OffHandoff_mC18A7AC7827E65A0EDF5A228740782D6D3514587 (void);
// 0x0000065F System.Void WeChatWASM.WXSDKManagerHandler::_OnShareTimelineCallback(System.String)
extern void WXSDKManagerHandler__OnShareTimelineCallback_m6B7265747820970A06F0B7CA21E58B6C1A6A0515 (void);
// 0x00000660 System.Void WeChatWASM.WXSDKManagerHandler::e(System.String)
extern void WXSDKManagerHandler_e_m6D1471597EEFD934FDC6034C0F020447217EA2B2 (void);
// 0x00000661 System.Void WeChatWASM.WXSDKManagerHandler::av()
extern void WXSDKManagerHandler_av_m9A381633354D9177CDC992DBFBE08AF06BDFAC2B (void);
// 0x00000662 System.Void WeChatWASM.WXSDKManagerHandler::OnShareTimeline(System.Action`1<System.Action`1<WeChatWASM.OnShareTimelineListenerResult>>)
extern void WXSDKManagerHandler_OnShareTimeline_m29277BD27BA1627ACCAD1B9EF82E9DBFAD9AA917 (void);
// 0x00000663 System.Void WeChatWASM.WXSDKManagerHandler::aW()
extern void WXSDKManagerHandler_aW_m4D2F3D2E5305E92AB7C4ABA3EF181307DA1BDF15 (void);
// 0x00000664 System.Void WeChatWASM.WXSDKManagerHandler::OffShareTimeline(System.Action`1<System.Action`1<WeChatWASM.OnShareTimelineListenerResult>>)
extern void WXSDKManagerHandler_OffShareTimeline_m039B0E12D6EF077566FD5A9E64B4FBFA8939C55B (void);
// 0x00000665 System.Void WeChatWASM.WXSDKManagerHandler::_OnGameLiveStateChangeCallback(System.String)
extern void WXSDKManagerHandler__OnGameLiveStateChangeCallback_mE294223F3B90395B69771795A299B67B35A34D41 (void);
// 0x00000666 System.Void WeChatWASM.WXSDKManagerHandler::F(System.String)
extern void WXSDKManagerHandler_F_m48933AC2CA60E93B671D9ADC45046B6780113637 (void);
// 0x00000667 System.Void WeChatWASM.WXSDKManagerHandler::aw()
extern void WXSDKManagerHandler_aw_m76DF8F7492D055D89176CA023215D0E4C02BAC19 (void);
// 0x00000668 System.Void WeChatWASM.WXSDKManagerHandler::OnGameLiveStateChange(System.Action`2<WeChatWASM.OnGameLiveStateChangeCallbackResult,System.Action`1<WeChatWASM.OnGameLiveStateChangeCallbackResponse>>)
extern void WXSDKManagerHandler_OnGameLiveStateChange_m17BED6C21A8DB97BB36E89E8B25E5888D6333733 (void);
// 0x00000669 System.Void WeChatWASM.WXSDKManagerHandler::aX()
extern void WXSDKManagerHandler_aX_mC07E6E71F2EC6AA643963AF987DB938E0A341E6C (void);
// 0x0000066A System.Void WeChatWASM.WXSDKManagerHandler::OffGameLiveStateChange(System.Action`2<WeChatWASM.OnGameLiveStateChangeCallbackResult,System.Action`1<WeChatWASM.OnGameLiveStateChangeCallbackResponse>>)
extern void WXSDKManagerHandler_OffGameLiveStateChange_mCDAB90677DD28759CBD6CB1F03EADCA902BEB6DA (void);
// 0x0000066B System.Boolean WeChatWASM.WXSDKManagerHandler::f(System.String)
extern void WXSDKManagerHandler_f_mAB0BB4D9B2B4F482DFECD83A38F31E593257D5F6 (void);
// 0x0000066C System.Boolean WeChatWASM.WXSDKManagerHandler::SetHandoffQuery(System.String)
extern void WXSDKManagerHandler_SetHandoffQuery_m60702737A5A59486AB4EB292519219015E25C7BD (void);
// 0x0000066D System.String WeChatWASM.WXSDKManagerHandler::ax()
extern void WXSDKManagerHandler_ax_m9370B4A92764B07923421DA401D2F5E42EAFE243 (void);
// 0x0000066E WeChatWASM.AccountInfo WeChatWASM.WXSDKManagerHandler::GetAccountInfoSync()
extern void WXSDKManagerHandler_GetAccountInfoSync_mFF08BB09D27DF37EB52EF135B2B244A7D1D28F3A (void);
// 0x0000066F System.String WeChatWASM.WXSDKManagerHandler::aY()
extern void WXSDKManagerHandler_aY_m71E2B992572B57B36C1907432485953B9B20F389 (void);
// 0x00000670 WeChatWASM.AppAuthorizeSetting WeChatWASM.WXSDKManagerHandler::GetAppAuthorizeSetting()
extern void WXSDKManagerHandler_GetAppAuthorizeSetting_mA6CA329EB46BF23B6D8BFF65FFF64A94EDFD49E6 (void);
// 0x00000671 System.String WeChatWASM.WXSDKManagerHandler::ay()
extern void WXSDKManagerHandler_ay_m9A51409DB9FE2DDC1751730F16D9FBE259FF4987 (void);
// 0x00000672 WeChatWASM.AppBaseInfo WeChatWASM.WXSDKManagerHandler::GetAppBaseInfo()
extern void WXSDKManagerHandler_GetAppBaseInfo_m2A313630B973FC497D2F3E404E86792ECA64E410 (void);
// 0x00000673 System.String WeChatWASM.WXSDKManagerHandler::aZ()
extern void WXSDKManagerHandler_aZ_mCB4644EC663E5643A176081E037D5E470D914F05 (void);
// 0x00000674 WeChatWASM.GetBatteryInfoSyncResult WeChatWASM.WXSDKManagerHandler::GetBatteryInfoSync()
extern void WXSDKManagerHandler_GetBatteryInfoSync_mC5A7ADB246B86F5A271C10CE475D2FEAD88EABFC (void);
// 0x00000675 System.String WeChatWASM.WXSDKManagerHandler::az()
extern void WXSDKManagerHandler_az_m2D19DC9A92E45532F22B5B10865CEC2E60E6EAD7 (void);
// 0x00000676 WeChatWASM.DeviceInfo WeChatWASM.WXSDKManagerHandler::GetDeviceInfo()
extern void WXSDKManagerHandler_GetDeviceInfo_m408C9EBF56C0C4A61CE919F63D0CE6128FECB4F3 (void);
// 0x00000677 System.String WeChatWASM.WXSDKManagerHandler::BA()
extern void WXSDKManagerHandler_BA_mFF0375C24C4A379A718ABD5246E55B45AC0652CD (void);
// 0x00000678 WeChatWASM.EnterOptionsGame WeChatWASM.WXSDKManagerHandler::GetEnterOptionsSync()
extern void WXSDKManagerHandler_GetEnterOptionsSync_m5587BAA90B74D49B7601C624E6B2594C2A6AE494 (void);
// 0x00000679 System.String WeChatWASM.WXSDKManagerHandler::G(System.String)
extern void WXSDKManagerHandler_G_m7C842238F813C7881ECEB0872B3267481E5070E1 (void);
// 0x0000067A T WeChatWASM.WXSDKManagerHandler::GetExptInfoSync(System.String[])
// 0x0000067B System.String WeChatWASM.WXSDKManagerHandler::Ba()
extern void WXSDKManagerHandler_Ba_mA42B08BC199BF1C8A3348BA50ADAD7667852271B (void);
// 0x0000067C T WeChatWASM.WXSDKManagerHandler::GetExtConfigSync()
// 0x0000067D System.String WeChatWASM.WXSDKManagerHandler::BB()
extern void WXSDKManagerHandler_BB_mA7D8AB58CC4CCF79B6BA00BDB63CF8011178F471 (void);
// 0x0000067E WeChatWASM.LaunchOptionsGame WeChatWASM.WXSDKManagerHandler::GetLaunchOptionsSync()
extern void WXSDKManagerHandler_GetLaunchOptionsSync_mAC55F3A9F8408ECD5392E8FB9B6AF8D80C79F8C2 (void);
// 0x0000067F System.String WeChatWASM.WXSDKManagerHandler::Bb()
extern void WXSDKManagerHandler_Bb_mD29A79A70D58C0CE167050D8DBC4EA9D6229514D (void);
// 0x00000680 WeChatWASM.ClientRect WeChatWASM.WXSDKManagerHandler::GetMenuButtonBoundingClientRect()
extern void WXSDKManagerHandler_GetMenuButtonBoundingClientRect_m4FE5CBC2348C85D0900FE9BCC7968C81B062729E (void);
// 0x00000681 System.String WeChatWASM.WXSDKManagerHandler::BC()
extern void WXSDKManagerHandler_BC_m45EF62780D4EE4AA61F408D401975B90D32867E1 (void);
// 0x00000682 WeChatWASM.GetStorageInfoSyncOption WeChatWASM.WXSDKManagerHandler::GetStorageInfoSync()
extern void WXSDKManagerHandler_GetStorageInfoSync_mD7CE2F4A6F82321EBCD30C392F275C8AB940A0FF (void);
// 0x00000683 System.String WeChatWASM.WXSDKManagerHandler::Bc()
extern void WXSDKManagerHandler_Bc_m2022D1DD116C4FAA1B411AD98E2B2D36E5885E13 (void);
// 0x00000684 WeChatWASM.SystemInfo WeChatWASM.WXSDKManagerHandler::GetSystemInfoSync()
extern void WXSDKManagerHandler_GetSystemInfoSync_mE5F735E2AA5777F1A17E8BA546FC1B3626B781D3 (void);
// 0x00000685 System.String WeChatWASM.WXSDKManagerHandler::BD()
extern void WXSDKManagerHandler_BD_m369B7159EEC313C5AC6BB96FDAC2F5B4BF59A4A5 (void);
// 0x00000686 WeChatWASM.SystemSetting WeChatWASM.WXSDKManagerHandler::GetSystemSetting()
extern void WXSDKManagerHandler_GetSystemSetting_m0449246A75894E54D169011F740C37BCD2DF5FD6 (void);
// 0x00000687 System.String WeChatWASM.WXSDKManagerHandler::Bd()
extern void WXSDKManagerHandler_Bd_m2F601250613DBDDF16001F64EFD3F9C6038FB4AC (void);
// 0x00000688 WeChatWASM.WindowInfo WeChatWASM.WXSDKManagerHandler::GetWindowInfo()
extern void WXSDKManagerHandler_GetWindowInfo_m65147FCE2550CE68675889625FBF0DA618B235F1 (void);
// 0x00000689 System.String WeChatWASM.WXSDKManagerHandler::BE()
extern void WXSDKManagerHandler_BE_mC40D21D5E56784AEB234C79EFD189767F881C079 (void);
// 0x0000068A WeChatWASM.ImageData WeChatWASM.WXSDKManagerHandler::CreateImageData()
extern void WXSDKManagerHandler_CreateImageData_m2FF44E296F64EE60C48C6E77D3668057B4B122AF (void);
// 0x0000068B System.String WeChatWASM.WXSDKManagerHandler::Be()
extern void WXSDKManagerHandler_Be_m3B8F2A948710F43F7DBC1F2D479AF5413D368B84 (void);
// 0x0000068C WeChatWASM.Path2D WeChatWASM.WXSDKManagerHandler::CreatePath2D()
extern void WXSDKManagerHandler_CreatePath2D_mE673C2E3CB90F4431D0997AFCD62A8CBA8A57DA0 (void);
// 0x0000068D System.Boolean WeChatWASM.WXSDKManagerHandler::A(System.String,System.Double,System.Double)
extern void WXSDKManagerHandler_A_m7FBCB19D38749C659D4F4C7A5D3B46F5C2DD5590 (void);
// 0x0000068E System.Boolean WeChatWASM.WXSDKManagerHandler::SetCursor(System.String,System.Double,System.Double)
extern void WXSDKManagerHandler_SetCursor_m6463B04A94A78E8D3D71053AE2008DC97E6063C9 (void);
// 0x0000068F System.Boolean WeChatWASM.WXSDKManagerHandler::g(System.String)
extern void WXSDKManagerHandler_g_m674F785AF3C448DB00CB3BE7B5F8DBF1389E4CDD (void);
// 0x00000690 System.Boolean WeChatWASM.WXSDKManagerHandler::SetMessageToFriendQuery(WeChatWASM.SetMessageToFriendQueryOption)
extern void WXSDKManagerHandler_SetMessageToFriendQuery_mB7C62BDF2EE47CB633D05125EFB9E882E1994CA3 (void);
// 0x00000691 System.Double WeChatWASM.WXSDKManagerHandler::H(System.String)
extern void WXSDKManagerHandler_H_mA272CA1A475F9C5CE40FDB0A0A24451323A59280 (void);
// 0x00000692 System.Double WeChatWASM.WXSDKManagerHandler::GetTextLineHeight(WeChatWASM.GetTextLineHeightOption)
extern void WXSDKManagerHandler_GetTextLineHeight_mB0AC80E753AA02970EE6910170E0F5EA5238333C (void);
// 0x00000693 System.String WeChatWASM.WXSDKManagerHandler::h(System.String)
extern void WXSDKManagerHandler_h_m59BC35C0D91E11FD0996542044DB844F575F2B3C (void);
// 0x00000694 System.String WeChatWASM.WXSDKManagerHandler::LoadFont(System.String)
extern void WXSDKManagerHandler_LoadFont_m79B315A6C735569E4F5583AAB4A06E47D9469365 (void);
// 0x00000695 System.String WeChatWASM.WXSDKManagerHandler::BF()
extern void WXSDKManagerHandler_BF_m2A4BACD5338FABA148858E5F01462E2857778317 (void);
// 0x00000696 WeChatWASM.GameLiveState WeChatWASM.WXSDKManagerHandler::GetGameLiveState()
extern void WXSDKManagerHandler_GetGameLiveState_mB4BDFC1495421051160503A099E64683DB888889 (void);
// 0x00000697 System.Void WeChatWASM.WXSDKManagerHandler::DownloadFileCallback(System.String)
extern void WXSDKManagerHandler_DownloadFileCallback_mF1300D06806FBE72CF81A3F60C8E73704ACBE36E (void);
// 0x00000698 System.String WeChatWASM.WXSDKManagerHandler::I(System.String)
extern void WXSDKManagerHandler_I_m296301E810DB23733AC5A6C45ACC8EE0949FD3D2 (void);
// 0x00000699 WeChatWASM.WXDownloadTask WeChatWASM.WXSDKManagerHandler::DownloadFile(WeChatWASM.DownloadFileOption)
extern void WXSDKManagerHandler_DownloadFile_mD14A209EF680229ACC649B3D8A143834A3C525B6 (void);
// 0x0000069A System.String WeChatWASM.WXSDKManagerHandler::i(System.String)
extern void WXSDKManagerHandler_i_mF9E7E59A01739567121FEC7397C1139E89B3CE37 (void);
// 0x0000069B WeChatWASM.WXFeedbackButton WeChatWASM.WXSDKManagerHandler::CreateFeedbackButton(WeChatWASM.CreateOpenSettingButtonOption)
extern void WXSDKManagerHandler_CreateFeedbackButton_m55B9F49DD32ECF32961B734CA33560C5AC648FC8 (void);
// 0x0000069C System.String WeChatWASM.WXSDKManagerHandler::J(System.String)
extern void WXSDKManagerHandler_J_m8BF865E591140F16D13804DCB33DE2E264F107F8 (void);
// 0x0000069D WeChatWASM.WXLogManager WeChatWASM.WXSDKManagerHandler::GetLogManager(WeChatWASM.GetLogManagerOption)
extern void WXSDKManagerHandler_GetLogManager_m2CD9433A2E564A78A2CA2AB30E028237E14CE586 (void);
// 0x0000069E System.String WeChatWASM.WXSDKManagerHandler::Bf()
extern void WXSDKManagerHandler_Bf_m802ABA4C73097896D37B31B5584596B17ECE1D89 (void);
// 0x0000069F WeChatWASM.WXRealtimeLogManager WeChatWASM.WXSDKManagerHandler::GetRealtimeLogManager()
extern void WXSDKManagerHandler_GetRealtimeLogManager_m96D409BAAFA1965FCE6FBEDBB55CFA0E337F93F4 (void);
// 0x000006A0 System.String WeChatWASM.WXSDKManagerHandler::BG()
extern void WXSDKManagerHandler_BG_m879E8DDD4C7DFEC9265D8D1DF2879D2E040DF688 (void);
// 0x000006A1 WeChatWASM.WXUpdateManager WeChatWASM.WXSDKManagerHandler::GetUpdateManager()
extern void WXSDKManagerHandler_GetUpdateManager_mA90AE929807C8DAE6CC676DB8A1F2413BF645270 (void);
// 0x000006A2 System.String WeChatWASM.WXSDKManagerHandler::Bg()
extern void WXSDKManagerHandler_Bg_m9DA1858E5BCC64A5C30864948A540FB8DCC21FD8 (void);
// 0x000006A3 WeChatWASM.WXVideoDecoder WeChatWASM.WXSDKManagerHandler::CreateVideoDecoder()
extern void WXSDKManagerHandler_CreateVideoDecoder_mD5BB268C66A5BDB39BB5537CD2F5D88402E1B5B6 (void);
// 0x000006A4 System.Void WeChatWASM.WXSDKManagerHandler::_DownloadTaskOnHeadersReceivedCallback(System.String)
extern void WXSDKManagerHandler__DownloadTaskOnHeadersReceivedCallback_m62FAC242EFCA9AD135081367C64AB03C28354894 (void);
// 0x000006A5 System.Void WeChatWASM.WXSDKManagerHandler::_DownloadTaskOnProgressUpdateCallback(System.String)
extern void WXSDKManagerHandler__DownloadTaskOnProgressUpdateCallback_m5DC4ADF8BAA07802882C28281DC9A2232F94FD71 (void);
// 0x000006A6 System.Void WeChatWASM.WXSDKManagerHandler::_FeedbackButtonOnTapCallback(System.String)
extern void WXSDKManagerHandler__FeedbackButtonOnTapCallback_m89D8E914CA3266A8842AE8D98F0E72409FD1B740 (void);
// 0x000006A7 System.Void WeChatWASM.WXSDKManagerHandler::_UpdateManagerOnCheckForUpdateCallback(System.String)
extern void WXSDKManagerHandler__UpdateManagerOnCheckForUpdateCallback_mE8854664E602D0401E8281C2099505D2E68701BD (void);
// 0x000006A8 System.Void WeChatWASM.WXSDKManagerHandler::_UpdateManagerOnUpdateFailedCallback(System.String)
extern void WXSDKManagerHandler__UpdateManagerOnUpdateFailedCallback_mE32DD60B80D194BB47EDE59FA4CA374AA34155DA (void);
// 0x000006A9 System.Void WeChatWASM.WXSDKManagerHandler::_UpdateManagerOnUpdateReadyCallback(System.String)
extern void WXSDKManagerHandler__UpdateManagerOnUpdateReadyCallback_mD26D2C1AEAE708311F44E26F7E32C33FDAEDC233 (void);
// 0x000006AA System.Void WeChatWASM.WXSDKManagerHandler::_VideoDecoderOnCallback(System.String)
extern void WXSDKManagerHandler__VideoDecoderOnCallback_mC93F8FF4BA4EECA438995020FF8C08947CCE697B (void);
// 0x000006AB WeChatWASM.WXSDKManagerHandler WeChatWASM.WXSDKManagerHandler::get_Instance()
extern void WXSDKManagerHandler_get_Instance_m9A42924F92A0528D0CD33E2850919A6FB487724B (void);
// 0x000006AC WeChatWASM.WXEnv WeChatWASM.WXSDKManagerHandler::get_Env()
extern void WXSDKManagerHandler_get_Env_mBE0E891F62EBAAC71A86AA09E5DB8791CD21DA8F (void);
// 0x000006AD WeChatWASM.Cloud WeChatWASM.WXSDKManagerHandler::get_cloud()
extern void WXSDKManagerHandler_get_cloud_mB357BF814941C3C5EA518B8FFDD1B8415269C861 (void);
// 0x000006AE System.Void WeChatWASM.WXSDKManagerHandler::OnDestroy()
extern void WXSDKManagerHandler_OnDestroy_mA57B04F1A52D5DD1ED2D3090D815019472666E9A (void);
// 0x000006AF System.Void WeChatWASM.WXSDKManagerHandler::j(System.String)
extern void WXSDKManagerHandler_j_mB8C3DA976838CA2B93F354538EAE8B781CBD5CC5 (void);
// 0x000006B0 System.Void WeChatWASM.WXSDKManagerHandler::A(System.String,System.Int32)
extern void WXSDKManagerHandler_A_m0D13EE3A820E18A7528391E3929B9711E7843CCA (void);
// 0x000006B1 System.Int32 WeChatWASM.WXSDKManagerHandler::a(System.String,System.Int32)
extern void WXSDKManagerHandler_a_m9E0940AF1AAF023535D884D5CB430A9D7B62474C (void);
// 0x000006B2 System.Void WeChatWASM.WXSDKManagerHandler::Bn(System.String,System.String)
extern void WXSDKManagerHandler_Bn_m3207B710E21DBABEA5FDE5D9F1E54EBBE46DE2F6 (void);
// 0x000006B3 System.String WeChatWASM.WXSDKManagerHandler::BO(System.String,System.String)
extern void WXSDKManagerHandler_BO_m2872864EB34198DBF833487296E3B42059C639C2 (void);
// 0x000006B4 System.Void WeChatWASM.WXSDKManagerHandler::A(System.String,System.Single)
extern void WXSDKManagerHandler_A_m21FDB376953E430ECF1F609CA2801FEB0ACDDF66 (void);
// 0x000006B5 System.Single WeChatWASM.WXSDKManagerHandler::a(System.String,System.Single)
extern void WXSDKManagerHandler_a_m8B078CBB9660F4916896A4C31DBE0D51702B9DE6 (void);
// 0x000006B6 System.Void WeChatWASM.WXSDKManagerHandler::BH()
extern void WXSDKManagerHandler_BH_m887BCA45C0F7E9A3AE573F040C1251A51EBBD41F (void);
// 0x000006B7 System.Void WeChatWASM.WXSDKManagerHandler::K(System.String)
extern void WXSDKManagerHandler_K_mEB2F70EFE723F76222786FE6D0DC1CF9B310FCEE (void);
// 0x000006B8 System.Boolean WeChatWASM.WXSDKManagerHandler::k(System.String)
extern void WXSDKManagerHandler_k_mC4B3C440CAFAA3A5D3BD138B2E777E1A2B58671B (void);
// 0x000006B9 System.String WeChatWASM.WXSDKManagerHandler::A(System.Int32,System.Int32,System.Int32,System.Int32,System.String,System.Boolean)
extern void WXSDKManagerHandler_A_m45840940368866E116227F96B9AD61D67C196980 (void);
// 0x000006BA System.Void WeChatWASM.WXSDKManagerHandler::A(System.String,System.Boolean)
extern void WXSDKManagerHandler_A_m7EAEF869C9347741B4389A6322FAC74CFB9F5F9B (void);
// 0x000006BB System.Void WeChatWASM.WXSDKManagerHandler::L(System.String)
extern void WXSDKManagerHandler_L_mD6A78C0D5E4935373CC7085297B7A93E7AD10F56 (void);
// 0x000006BC System.String WeChatWASM.WXSDKManagerHandler::l(System.String)
extern void WXSDKManagerHandler_l_mE4FABCCE36800D8B44BB2F087732BE04CA55111C (void);
// 0x000006BD System.String WeChatWASM.WXSDKManagerHandler::M(System.String)
extern void WXSDKManagerHandler_M_mB9EB6342DC765D1C6382528BADFA71DEE2F9665D (void);
// 0x000006BE System.String WeChatWASM.WXSDKManagerHandler::m(System.String)
extern void WXSDKManagerHandler_m_m035A5CA42CE1F0974D217E027BFF05CEEC69D42D (void);
// 0x000006BF System.String WeChatWASM.WXSDKManagerHandler::N(System.String)
extern void WXSDKManagerHandler_N_mDBA4E3278B754B033B901644F12B9CD9E5A40C2B (void);
// 0x000006C0 System.Void WeChatWASM.WXSDKManagerHandler::A(System.String,System.String,System.Int32)
extern void WXSDKManagerHandler_A_mB330FAE4DC1F34605317A53721F2AFEE509445D9 (void);
// 0x000006C1 System.String WeChatWASM.WXSDKManagerHandler::A(System.String,System.String,System.String)
extern void WXSDKManagerHandler_A_m0F85FEC3A96392F6F13D297FBF1684DB579F5FB0 (void);
// 0x000006C2 System.String WeChatWASM.WXSDKManagerHandler::A(System.String,System.String,System.String,System.String,System.String)
extern void WXSDKManagerHandler_A_m0404BE83541AB1B76E8D19D4CD80F1374647D17E (void);
// 0x000006C3 System.String WeChatWASM.WXSDKManagerHandler::a(System.String,System.String,System.String)
extern void WXSDKManagerHandler_a_mD3414510327ABA9478094F915F1B1449D40287A6 (void);
// 0x000006C4 System.Int32 WeChatWASM.WXSDKManagerHandler::Bo(System.String,System.String)
extern void WXSDKManagerHandler_Bo_m2F54340FCA384FBE2E36E583C8C68088FBD0AD53 (void);
// 0x000006C5 System.Int32 WeChatWASM.WXSDKManagerHandler::n(System.String)
extern void WXSDKManagerHandler_n_mED9D0B1266B0A3F19C6820ABD8600808B09ACF40 (void);
// 0x000006C6 System.Int32 WeChatWASM.WXSDKManagerHandler::B(System.String,System.String,System.String)
extern void WXSDKManagerHandler_B_m18FB8D45C5FF4E9138A54041A777C2BEC1C8EBD6 (void);
// 0x000006C7 System.String WeChatWASM.WXSDKManagerHandler::A(System.String,System.Int32,System.Int32)
extern void WXSDKManagerHandler_A_m5FF7AC24223B313FA196CC02942BCCF98B565203 (void);
// 0x000006C8 System.String WeChatWASM.WXSDKManagerHandler::O(System.String)
extern void WXSDKManagerHandler_O_mF1163C07BD54A24841F927BCD141E0758EA2B34E (void);
// 0x000006C9 System.Void WeChatWASM.WXSDKManagerHandler::A(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32)
extern void WXSDKManagerHandler_A_m7204732D37530F46A905C40DC716F459D26D60C8 (void);
// 0x000006CA System.Void WeChatWASM.WXSDKManagerHandler::Bh()
extern void WXSDKManagerHandler_Bh_m794F1A7D18AC189A06AC3FC88FA36A7F34048F19 (void);
// 0x000006CB System.Void WeChatWASM.WXSDKManagerHandler::BI()
extern void WXSDKManagerHandler_BI_m5CAF304723D73DDE36774C3DE53922F9B60CE713 (void);
// 0x000006CC System.Void WeChatWASM.WXSDKManagerHandler::A(System.Int32,System.Int32,System.String,System.String)
extern void WXSDKManagerHandler_A_mFB638B0F64EF825E5D6EED999243F67F7187A5C2 (void);
// 0x000006CD System.Void WeChatWASM.WXSDKManagerHandler::o(System.String)
extern void WXSDKManagerHandler_o_m348A4914F1B6A379E5E099F10D7D3D101C38FD7A (void);
// 0x000006CE System.Void WeChatWASM.WXSDKManagerHandler::P(System.String)
extern void WXSDKManagerHandler_P_m5DF802D7B732DA2D2204DC2CC2FEAC0B5FF64A23 (void);
// 0x000006CF System.Void WeChatWASM.WXSDKManagerHandler::Bi()
extern void WXSDKManagerHandler_Bi_mE26E9A78D311AE851A4EFF6578F05589673CC8CD (void);
// 0x000006D0 System.Void WeChatWASM.WXSDKManagerHandler::A(System.Int32)
extern void WXSDKManagerHandler_A_mC9AE2C681CE309039C5B3F5175AE7AD58CA369A7 (void);
// 0x000006D1 System.Void WeChatWASM.WXSDKManagerHandler::a(System.String,System.String,System.Int32)
extern void WXSDKManagerHandler_a_mEB98D9658B70E43968F38F1E9DDEF8650C5472C8 (void);
// 0x000006D2 System.String WeChatWASM.WXSDKManagerHandler::A(System.String,System.Boolean,System.Single,System.Boolean,System.Single,System.Single,System.Boolean)
extern void WXSDKManagerHandler_A_m9746ECEF1EAA83E399C1D79E9D0D288B44A88850 (void);
// 0x000006D3 System.Void WeChatWASM.WXSDKManagerHandler::B(System.String,System.Int32)
extern void WXSDKManagerHandler_B_m3393D29E2A0F35DA0D7C7C3F56829885D794B06A (void);
// 0x000006D4 System.Void WeChatWASM.WXSDKManagerHandler::p(System.String)
extern void WXSDKManagerHandler_p_m2F0F192336736E23573A16C88A97E2C606E036FB (void);
// 0x000006D5 System.Void WeChatWASM.WXSDKManagerHandler::Q(System.String)
extern void WXSDKManagerHandler_Q_m5120958F7CF0C06231F546A48CAEA92F153551CE (void);
// 0x000006D6 System.String WeChatWASM.WXSDKManagerHandler::q(System.String)
extern void WXSDKManagerHandler_q_mBF41D56A02D43E6507E8AB615746E3F8D03F1653 (void);
// 0x000006D7 System.String WeChatWASM.WXSDKManagerHandler::R(System.String)
extern void WXSDKManagerHandler_R_m41631A3101005587D92864E4882A58F5AC2DAE3E (void);
// 0x000006D8 System.Void WeChatWASM.WXSDKManagerHandler::B(System.String,System.String,System.Int32)
extern void WXSDKManagerHandler_B_m79A9060FD2042537B3A51015848F7B00439D6EE6 (void);
// 0x000006D9 System.Void WeChatWASM.WXSDKManagerHandler::b(System.String,System.String,System.String)
extern void WXSDKManagerHandler_b_mB53FBA69360B40277BF9EF2C0E87ED3B4549E296 (void);
// 0x000006DA System.Void WeChatWASM.WXSDKManagerHandler::r(System.String)
extern void WXSDKManagerHandler_r_mE080DCA626BDD5A279FE932AFF8F0F4C535CF6DE (void);
// 0x000006DB System.UInt32 WeChatWASM.WXSDKManagerHandler::BJ()
extern void WXSDKManagerHandler_BJ_m2D97417155A8D139C14D5C159A3E844B6FD6A2AF (void);
// 0x000006DC System.UInt32 WeChatWASM.WXSDKManagerHandler::Bj()
extern void WXSDKManagerHandler_Bj_m2601686AEBD1DF8E19DF52797BE1729DA181AA0E (void);
// 0x000006DD System.UInt32 WeChatWASM.WXSDKManagerHandler::BK()
extern void WXSDKManagerHandler_BK_m3580ABE717EBBD8DF8E311C045338EE40EE2A5D5 (void);
// 0x000006DE System.UInt32 WeChatWASM.WXSDKManagerHandler::Bk()
extern void WXSDKManagerHandler_Bk_m1A3F55041A9DC003A7AD55D95CC4BA9AEBCB5AB4 (void);
// 0x000006DF System.UInt32 WeChatWASM.WXSDKManagerHandler::BL()
extern void WXSDKManagerHandler_BL_m678114742DE76B820DF20B8A6CF993A7CB3AFD7F (void);
// 0x000006E0 System.UInt32 WeChatWASM.WXSDKManagerHandler::Bl()
extern void WXSDKManagerHandler_Bl_m9A14C5024B515636311195033DAD08533E6BCD53 (void);
// 0x000006E1 System.Single WeChatWASM.WXSDKManagerHandler::BM()
extern void WXSDKManagerHandler_BM_mF99EE092EE6143D6EF021C8C6427D113D201C392 (void);
// 0x000006E2 System.UInt32 WeChatWASM.WXSDKManagerHandler::Bm()
extern void WXSDKManagerHandler_Bm_m9C7AE350396B16166AB1C60E73A26D3B592A517D (void);
// 0x000006E3 System.UInt32 WeChatWASM.WXSDKManagerHandler::BN()
extern void WXSDKManagerHandler_BN_m408C1BB36E98133775CCA85855F3C4B81709EC35 (void);
// 0x000006E4 System.UInt32 WeChatWASM.WXSDKManagerHandler::Bn()
extern void WXSDKManagerHandler_Bn_mE7432C53AFAA2A0EF57F9EA335640042C3193BA0 (void);
// 0x000006E5 System.UInt32 WeChatWASM.WXSDKManagerHandler::BO()
extern void WXSDKManagerHandler_BO_mA521C89537E47D839B2E9FCCAB7365B08155AA4D (void);
// 0x000006E6 System.Void WeChatWASM.WXSDKManagerHandler::S(System.String)
extern void WXSDKManagerHandler_S_mC8D6500E35942475A0355A0B6B3B85D64FD1C983 (void);
// 0x000006E7 System.Void WeChatWASM.WXSDKManagerHandler::s(System.String)
extern void WXSDKManagerHandler_s_m5B1DC68ED740EFC5B027B0407259945BA8D49A3B (void);
// 0x000006E8 System.Void WeChatWASM.WXSDKManagerHandler::T(System.String)
extern void WXSDKManagerHandler_T_m5C097EC18B4470553D2E19775CDA3E0B8C77B2FF (void);
// 0x000006E9 System.Void WeChatWASM.WXSDKManagerHandler::t(System.String)
extern void WXSDKManagerHandler_t_mC6C79526238EBE33C42DD9FF437F48228433E268 (void);
// 0x000006EA System.Void WeChatWASM.WXSDKManagerHandler::Bo()
extern void WXSDKManagerHandler_Bo_mC01E277E725730802FC4EABA07F9D9EEFB75CB0A (void);
// 0x000006EB System.Void WeChatWASM.WXSDKManagerHandler::BP()
extern void WXSDKManagerHandler_BP_mDF6E2359AC5A5212C1ABC59A9C3F7C1826076C8C (void);
// 0x000006EC System.Boolean WeChatWASM.WXSDKManagerHandler::Bp()
extern void WXSDKManagerHandler_Bp_m38CA54CD912114D4B590B154D2E861EB6D149141 (void);
// 0x000006ED System.String WeChatWASM.WXSDKManagerHandler::U(System.String)
extern void WXSDKManagerHandler_U_m2519B28AC9E5F7E9294CB0EA8ED309299B5E73BD (void);
// 0x000006EE System.String WeChatWASM.WXSDKManagerHandler::BQ()
extern void WXSDKManagerHandler_BQ_mE6B081FF817B82C938AF43E8666835111A77C937 (void);
// 0x000006EF System.Void WeChatWASM.WXSDKManagerHandler::Bq()
extern void WXSDKManagerHandler_Bq_m2DCC4593A505CB383F3EBC6928BA2DF922752967 (void);
// 0x000006F0 System.Int32 WeChatWASM.WXSDKManagerHandler::a(System.String,System.Int32,System.Int32)
extern void WXSDKManagerHandler_a_m0D5E0E2B09109AA0B2C0AB68682B5501484AF7B8 (void);
// 0x000006F1 System.Void WeChatWASM.WXSDKManagerHandler::a(System.Int32)
extern void WXSDKManagerHandler_a_m1DA7D6C683C0E6331101BDABDDD5BCD64A2E26FF (void);
// 0x000006F2 System.Void WeChatWASM.WXSDKManagerHandler::A(System.Int32,System.Byte[],System.Int32,System.Int32)
extern void WXSDKManagerHandler_A_m95F18FF8B32AC04158C00EC2AE2B7CFE1EEB9024 (void);
// 0x000006F3 System.Void WeChatWASM.WXSDKManagerHandler::WXUDPSocketSetOnMessage(WeChatWASM.WXSDKManagerHandler/OnMessageCallback)
extern void WXSDKManagerHandler_WXUDPSocketSetOnMessage_m6D136005F99475D2F890286324E285747813708A (void);
// 0x000006F4 System.Void WeChatWASM.WXSDKManagerHandler::WXUDPSocketSetOnClose(WeChatWASM.WXSDKManagerHandler/OnCloseCallback)
extern void WXSDKManagerHandler_WXUDPSocketSetOnClose_m38DA6943AB1572C3FFEF5C4E690BB71BF5F065A0 (void);
// 0x000006F5 System.Void WeChatWASM.WXSDKManagerHandler::WXUDPSocketSetOnError(WeChatWASM.WXSDKManagerHandler/OnErrorCallback)
extern void WXSDKManagerHandler_WXUDPSocketSetOnError_mFF8B7AFF253D9015D391BA036022C28C67FCBBAA (void);
// 0x000006F6 System.Void WeChatWASM.WXSDKManagerHandler::DelegateOnMessageEvent(System.Int32,System.IntPtr,System.Int32)
extern void WXSDKManagerHandler_DelegateOnMessageEvent_mB4CC33FEFEA70BE79535B3D1CA87F4FEEEF1B418 (void);
// 0x000006F7 System.Void WeChatWASM.WXSDKManagerHandler::DelegateOnCloseEvent(System.Int32,System.IntPtr)
extern void WXSDKManagerHandler_DelegateOnCloseEvent_m51F987A089DD23E07359E77B998275CA8ADA64F1 (void);
// 0x000006F8 System.Void WeChatWASM.WXSDKManagerHandler::DelegateOnErrorEvent(System.Int32,System.IntPtr)
extern void WXSDKManagerHandler_DelegateOnErrorEvent_m84321402DEA4A472CEF6ECB9A0608A7A0ADB3A6A (void);
// 0x000006F9 System.Void WeChatWASM.WXSDKManagerHandler::Inited(System.Int32)
extern void WXSDKManagerHandler_Inited_m22C0ECFD3A6BFFF75D4B687C203C9FC073A0142A (void);
// 0x000006FA System.Void WeChatWASM.WXSDKManagerHandler::TextResponseCallback(System.String)
extern void WXSDKManagerHandler_TextResponseCallback_m4C8C6DDB0055B0AE5DEFF57BCA2D7EF1742104D2 (void);
// 0x000006FB System.Void WeChatWASM.WXSDKManagerHandler::TextResponseLongCallback(System.String)
extern void WXSDKManagerHandler_TextResponseLongCallback_m444CE1D6B88A53DF5B2DB4E9D1D78D98CA9A02A7 (void);
// 0x000006FC System.Void WeChatWASM.WXSDKManagerHandler::CloudCallFunctionResponseCallback(System.String)
extern void WXSDKManagerHandler_CloudCallFunctionResponseCallback_mC7D98CC0312D255F4D9CC7F40B48774DAD8A8434 (void);
// 0x000006FD System.Void WeChatWASM.WXSDKManagerHandler::UserInfoButtonOnTapCallback(System.String)
extern void WXSDKManagerHandler_UserInfoButtonOnTapCallback_m2611465DA3E3DB9DC345880947ED1894F212AAEC (void);
// 0x000006FE System.Void WeChatWASM.WXSDKManagerHandler::OnShareAppMessageCallback()
extern void WXSDKManagerHandler_OnShareAppMessageCallback_m283D03EFB657CCDEACA4C74EBC331F3769D8E78B (void);
// 0x000006FF System.Void WeChatWASM.WXSDKManagerHandler::ADOnErrorCallback(System.String)
extern void WXSDKManagerHandler_ADOnErrorCallback_m900E8E5DF22FAD943E9223BC0A99E7946CA2D12C (void);
// 0x00000700 System.Void WeChatWASM.WXSDKManagerHandler::ADOnLoadCallback(System.String)
extern void WXSDKManagerHandler_ADOnLoadCallback_mA8866CF3008C1C17805E0B4933E41BC90481B1F1 (void);
// 0x00000701 System.Void WeChatWASM.WXSDKManagerHandler::ADOnResizeCallback(System.String)
extern void WXSDKManagerHandler_ADOnResizeCallback_mD7D699D4D26ACC6CCEB3EC444EA79E79CB6A1C96 (void);
// 0x00000702 System.Void WeChatWASM.WXSDKManagerHandler::ADOnVideoCloseCallback(System.String)
extern void WXSDKManagerHandler_ADOnVideoCloseCallback_mF3F3B098B1534F09E5D10BD76A57CF84142189D5 (void);
// 0x00000703 System.Void WeChatWASM.WXSDKManagerHandler::ADOnHideCallback(System.String)
extern void WXSDKManagerHandler_ADOnHideCallback_mC186D6DA54FA0AF2042012DC1658548D8BDDF206 (void);
// 0x00000704 System.Void WeChatWASM.WXSDKManagerHandler::ADOnCloseCallback(System.String)
extern void WXSDKManagerHandler_ADOnCloseCallback_m7C088444A08477EC43A19AC0299CA7EC3A7E95B9 (void);
// 0x00000705 System.Void WeChatWASM.WXSDKManagerHandler::ADLoadErrorCallback(System.String)
extern void WXSDKManagerHandler_ADLoadErrorCallback_m632BFC6158D493AD3D9687CE3A2EA18A998B67DA (void);
// 0x00000706 System.Void WeChatWASM.WXSDKManagerHandler::OnGameClubButtonCallback(System.String)
extern void WXSDKManagerHandler_OnGameClubButtonCallback_m88C839177C8E3A6E7F2C8B14FA315BA234C9F5C2 (void);
// 0x00000707 System.Void WeChatWASM.WXSDKManagerHandler::OnAudioCallback(System.String)
extern void WXSDKManagerHandler_OnAudioCallback_m03FC815AAB43D54B6D975D8E43B08BB0BFCD6D5F (void);
// 0x00000708 System.Void WeChatWASM.WXSDKManagerHandler::WXPreDownloadAudiosCallback(System.String)
extern void WXSDKManagerHandler_WXPreDownloadAudiosCallback_m08CA17CC20A370FDF81E478653CBCDD9A30D4D38 (void);
// 0x00000709 System.Void WeChatWASM.WXSDKManagerHandler::OnVideoCallback(System.String)
extern void WXSDKManagerHandler_OnVideoCallback_m65401EFBC68C7EF992D080AD9F8002D899170E80 (void);
// 0x0000070A System.Void WeChatWASM.WXSDKManagerHandler::StatCallback(System.String)
extern void WXSDKManagerHandler_StatCallback_m5CB5D78001894D966743EC37E861671FAC68B1D6 (void);
// 0x0000070B System.Void WeChatWASM.WXSDKManagerHandler::FileSystemManagerCallback(System.String)
extern void WXSDKManagerHandler_FileSystemManagerCallback_m13E0DE1D54A33B91F220520D12348A5268044388 (void);
// 0x0000070C System.Void WeChatWASM.WXSDKManagerHandler::ToTempFilePathCallback(System.String)
extern void WXSDKManagerHandler_ToTempFilePathCallback_m84078A162F65E633656B605F6D0CE1D188E5DE92 (void);
// 0x0000070D System.Void WeChatWASM.WXSDKManagerHandler::GetFontRawDataCallback(System.String)
extern void WXSDKManagerHandler_GetFontRawDataCallback_m5EA2BCF9D36000759AB3054E3C26CA7FE3DF9084 (void);
// 0x0000070E System.Void WeChatWASM.WXSDKManagerHandler::InitSDK(System.Action`1<System.Int32>)
extern void WXSDKManagerHandler_InitSDK_mAC35C00E88DFF3C575ACDF8CE225B1CC88EFA4AB (void);
// 0x0000070F System.Void WeChatWASM.WXSDKManagerHandler::StorageSetIntSync(System.String,System.Int32)
extern void WXSDKManagerHandler_StorageSetIntSync_mFC81FBAF94CE4C8C564B4CE7AEF2590BE6FC2AE1 (void);
// 0x00000710 System.Int32 WeChatWASM.WXSDKManagerHandler::StorageGetIntSync(System.String,System.Int32)
extern void WXSDKManagerHandler_StorageGetIntSync_m03AD862EF1738CBABE0D740B223ACC2665B38DE9 (void);
// 0x00000711 System.Void WeChatWASM.WXSDKManagerHandler::StorageSetStringSync(System.String,System.String)
extern void WXSDKManagerHandler_StorageSetStringSync_mFE51CAF774745AADC518D710BC8D089A9C831C08 (void);
// 0x00000712 System.String WeChatWASM.WXSDKManagerHandler::StorageGetStringSync(System.String,System.String)
extern void WXSDKManagerHandler_StorageGetStringSync_mDF7F7B60E2F505B07F1B832014FA495730A04246 (void);
// 0x00000713 System.Void WeChatWASM.WXSDKManagerHandler::StorageSetFloatSync(System.String,System.Single)
extern void WXSDKManagerHandler_StorageSetFloatSync_m08BCC159B62735E209E057417F25B95C9BC07886 (void);
// 0x00000714 System.Single WeChatWASM.WXSDKManagerHandler::StorageGetFloatSync(System.String,System.Single)
extern void WXSDKManagerHandler_StorageGetFloatSync_m12C3E3A94C208DF8A7449BE784A117584F3166F4 (void);
// 0x00000715 System.Void WeChatWASM.WXSDKManagerHandler::StorageDeleteAllSync()
extern void WXSDKManagerHandler_StorageDeleteAllSync_m4839FFA49355D1845F07A82408A351DC602DADDF (void);
// 0x00000716 System.Void WeChatWASM.WXSDKManagerHandler::StorageDeleteKeySync(System.String)
extern void WXSDKManagerHandler_StorageDeleteKeySync_mF751DE56AFD2CFE759A067D3A6E0595194DA7779 (void);
// 0x00000717 System.Boolean WeChatWASM.WXSDKManagerHandler::StorageHasKeySync(System.String)
extern void WXSDKManagerHandler_StorageHasKeySync_m5F4C067B1E030377247C3BF03DB0E83E5BEDA623 (void);
// 0x00000718 WeChatWASM.WXUserInfoButton WeChatWASM.WXSDKManagerHandler::CreateUserInfoButton(System.Int32,System.Int32,System.Int32,System.Int32,System.String,System.Boolean)
extern void WXSDKManagerHandler_CreateUserInfoButton_mB8FC31345ACA3121077090207DFBED704006B14B (void);
// 0x00000719 System.Void WeChatWASM.WXSDKManagerHandler::OnShareAppMessage(WeChatWASM.WXShareAppMessageParam,System.Action`1<System.Action`1<WeChatWASM.WXShareAppMessageParam>>)
extern void WXSDKManagerHandler_OnShareAppMessage_m31DE5FDFB87BDDFC1A8DF25FA6D2526D9E2A20FD (void);
// 0x0000071A WeChatWASM.WXBannerAd WeChatWASM.WXSDKManagerHandler::CreateBannerAd(WeChatWASM.WXCreateBannerAdParam)
extern void WXSDKManagerHandler_CreateBannerAd_m62E6F16AD670C5580912BF64F6752118F4E40B86 (void);
// 0x0000071B WeChatWASM.WXBannerAd WeChatWASM.WXSDKManagerHandler::CreateFixedBottomMiddleBannerAd(System.String,System.Int32,System.Int32)
extern void WXSDKManagerHandler_CreateFixedBottomMiddleBannerAd_m624EBD61A5402B45C83D397A0B670586C5DCFE67 (void);
// 0x0000071C WeChatWASM.WXRewardedVideoAd WeChatWASM.WXSDKManagerHandler::CreateRewardedVideoAd(WeChatWASM.WXCreateRewardedVideoAdParam)
extern void WXSDKManagerHandler_CreateRewardedVideoAd_m5ABC1986E79317AA591F940F4C5257591998AB80 (void);
// 0x0000071D WeChatWASM.WXInterstitialAd WeChatWASM.WXSDKManagerHandler::CreateInterstitialAd(WeChatWASM.WXCreateInterstitialAdParam)
extern void WXSDKManagerHandler_CreateInterstitialAd_m6CFBC23B60595AB0AAE9709CB425296922C622F0 (void);
// 0x0000071E WeChatWASM.WXCustomAd WeChatWASM.WXSDKManagerHandler::CreateCustomAd(WeChatWASM.WXCreateCustomAdParam)
extern void WXSDKManagerHandler_CreateCustomAd_m4A842FBA62A24583C0DB066BE3A06793354C226A (void);
// 0x0000071F System.Void WeChatWASM.WXSDKManagerHandler::ADStyleChange(System.String,System.String,System.Int32)
extern void WXSDKManagerHandler_ADStyleChange_m44B4D652F79C45C8FF2102DE02029AE000540184 (void);
// 0x00000720 System.Void WeChatWASM.WXSDKManagerHandler::ShowAd(System.String,System.String,System.String)
extern void WXSDKManagerHandler_ShowAd_m08CA59FA78AA6E2D8A119DE5B8E501F22E8E1FEB (void);
// 0x00000721 System.Int32 WeChatWASM.WXSDKManagerHandler::CreateUDPSocket(System.String,System.Int32,System.Int32)
extern void WXSDKManagerHandler_CreateUDPSocket_mFE35832F3E44F173BBAD2B1E381208B1CA915C66 (void);
// 0x00000722 System.Void WeChatWASM.WXSDKManagerHandler::CloseUDPSocket(System.Int32)
extern void WXSDKManagerHandler_CloseUDPSocket_m7CEDB3D741CEF2F16C2881F17EB4CAC010282F8F (void);
// 0x00000723 System.Void WeChatWASM.WXSDKManagerHandler::SendUDPSocket(System.Int32,System.Byte[],System.Int32,System.Int32)
extern void WXSDKManagerHandler_SendUDPSocket_mF6D310EA6028D5B0C295228CEDB81A0F0D2F3833 (void);
// 0x00000724 System.Void WeChatWASM.WXSDKManagerHandler::ShowAd(System.String,System.String,System.String,System.String,System.String)
extern void WXSDKManagerHandler_ShowAd_mC9ACDDE6CEFBAB6211A18792211DEF8FEE8762CA (void);
// 0x00000725 System.Void WeChatWASM.WXSDKManagerHandler::HideAd(System.String,System.String,System.String)
extern void WXSDKManagerHandler_HideAd_mD29CD364179D84D97EF406BD68EAD87CDD0BB411 (void);
// 0x00000726 System.Int32 WeChatWASM.WXSDKManagerHandler::ADGetStyleValue(System.String,System.String)
extern void WXSDKManagerHandler_ADGetStyleValue_m85FD187B42DDD66979201D07B5922890644D6F8B (void);
// 0x00000727 System.Void WeChatWASM.WXSDKManagerHandler::ADDestroy(System.String)
extern void WXSDKManagerHandler_ADDestroy_m7EC3CFD188A9B283ACAB3C9B474CF409AF3288E0 (void);
// 0x00000728 System.Void WeChatWASM.WXSDKManagerHandler::ADLoad(System.String,System.String,System.String)
extern void WXSDKManagerHandler_ADLoad_m094D8D324D366D330DDEADB782E54FB7D4E8C9B3 (void);
// 0x00000729 System.Void WeChatWASM.WXSDKManagerHandler::OpenDataContextPostMessage(System.String)
extern void WXSDKManagerHandler_OpenDataContextPostMessage_m1CEC72B11F4FEC0D94F4C2E088EA04393ABA088E (void);
// 0x0000072A System.Void WeChatWASM.WXSDKManagerHandler::ShowOpenData(UnityEngine.Texture,System.Int32,System.Int32,System.Int32,System.Int32)
extern void WXSDKManagerHandler_ShowOpenData_mFBEAE663A4AFC5F38C778B6BD23E21C1FB38E0FB (void);
// 0x0000072B System.Void WeChatWASM.WXSDKManagerHandler::HideOpenData()
extern void WXSDKManagerHandler_HideOpenData_m9DE08E192F2AE27E92FE4DC6E0FF533CB011573F (void);
// 0x0000072C System.Void WeChatWASM.WXSDKManagerHandler::ReportGameStart()
extern void WXSDKManagerHandler_ReportGameStart_m5F7B3F4FB6F47F68A43A822961C528FB5FC82F68 (void);
// 0x0000072D System.Void WeChatWASM.WXSDKManagerHandler::ReportGameSceneError(System.Int32,System.Int32,System.String,System.String)
extern void WXSDKManagerHandler_ReportGameSceneError_m4C0F09DCF8DB6635705163A20726EDD95E5C3611 (void);
// 0x0000072E System.Void WeChatWASM.WXSDKManagerHandler::WriteLog(System.String)
extern void WXSDKManagerHandler_WriteLog_mB663FABFDE437831E87AD12E0EDD6C928D20F0F5 (void);
// 0x0000072F System.Void WeChatWASM.WXSDKManagerHandler::WriteWarn(System.String)
extern void WXSDKManagerHandler_WriteWarn_m38B4A78B41B67CD2492DE6AA41BC5A1E1F61A02F (void);
// 0x00000730 System.Void WeChatWASM.WXSDKManagerHandler::HideLoadingPage()
extern void WXSDKManagerHandler_HideLoadingPage_m273ACF0C1419D9E3CFF1A12B54991FA005D08D10 (void);
// 0x00000731 System.Void WeChatWASM.WXSDKManagerHandler::PreloadConcurrent(System.Int32)
extern void WXSDKManagerHandler_PreloadConcurrent_m0DDF75E2815459012791CAEF6793E99A1CEBF8D3 (void);
// 0x00000732 System.Void WeChatWASM.WXSDKManagerHandler::ReportUserBehaviorBranchAnalytics(System.String,System.String,System.Int32)
extern void WXSDKManagerHandler_ReportUserBehaviorBranchAnalytics_m8D44D146A3890826B8AEA35A374D6B73265A8857 (void);
// 0x00000733 WeChatWASM.WXInnerAudioContext WeChatWASM.WXSDKManagerHandler::CreateInnerAudioContext(WeChatWASM.InnerAudioContextParam)
extern void WXSDKManagerHandler_CreateInnerAudioContext_m556E4CFAB33265BF858341EDA665487ECA73E586 (void);
// 0x00000734 System.Void WeChatWASM.WXSDKManagerHandler::PreDownloadAudios(System.String[],System.Action`1<System.Int32>)
extern void WXSDKManagerHandler_PreDownloadAudios_m176A22B63E768F168F4777A5EED0A448539D1796 (void);
// 0x00000735 WeChatWASM.WXVideo WeChatWASM.WXSDKManagerHandler::CreateVideo(WeChatWASM.WXCreateVideoParam)
extern void WXSDKManagerHandler_CreateVideo_m2B1D7905278EAE58E6E8A6C3955391502A47D88A (void);
// 0x00000736 System.UInt32 WeChatWASM.WXSDKManagerHandler::GetTotalMemorySize()
extern void WXSDKManagerHandler_GetTotalMemorySize_m474305C81AEE9D7BCE6304F7BC60279F5E96B2FF (void);
// 0x00000737 System.UInt32 WeChatWASM.WXSDKManagerHandler::GetTotalStackSize()
extern void WXSDKManagerHandler_GetTotalStackSize_m0246A6BC731D99EBCEAF465BC215625786E9D9B2 (void);
// 0x00000738 System.UInt32 WeChatWASM.WXSDKManagerHandler::GetStaticMemorySize()
extern void WXSDKManagerHandler_GetStaticMemorySize_m20FB74F2D068E9ADAB2395C10C9CCCBC66EB0C66 (void);
// 0x00000739 System.UInt32 WeChatWASM.WXSDKManagerHandler::GetDynamicMemorySize()
extern void WXSDKManagerHandler_GetDynamicMemorySize_m0AC259A480F54FEA287347AF6295B01D0107E15B (void);
// 0x0000073A System.UInt32 WeChatWASM.WXSDKManagerHandler::GetUsedMemorySize()
extern void WXSDKManagerHandler_GetUsedMemorySize_m99E41333CA7595D4ECCF5F94E0C3D4F2FAAD77A5 (void);
// 0x0000073B System.UInt32 WeChatWASM.WXSDKManagerHandler::GetUnAllocatedMemorySize()
extern void WXSDKManagerHandler_GetUnAllocatedMemorySize_m1870BFA1D487D923F1F8F03123829D92715AC784 (void);
// 0x0000073C System.Single WeChatWASM.WXSDKManagerHandler::GetEXFrameTime()
extern void WXSDKManagerHandler_GetEXFrameTime_m0FD92DD59EE363F1C84FB7D53AC65B1792BD3DE7 (void);
// 0x0000073D System.UInt32 WeChatWASM.WXSDKManagerHandler::GetBundleNumberInMemory()
extern void WXSDKManagerHandler_GetBundleNumberInMemory_mEF1E849687A6168BD4D14CEFB4A84138069F5AA3 (void);
// 0x0000073E System.UInt32 WeChatWASM.WXSDKManagerHandler::GetBundleNumberOnDisk()
extern void WXSDKManagerHandler_GetBundleNumberOnDisk_m32E4DEE21B0E20F235CE301F8904CEA3418A9921 (void);
// 0x0000073F System.UInt32 WeChatWASM.WXSDKManagerHandler::GetBundleSizeInMemory()
extern void WXSDKManagerHandler_GetBundleSizeInMemory_m3BAADFD66DFCDB1DE920805E3D1D406176733446 (void);
// 0x00000740 System.UInt32 WeChatWASM.WXSDKManagerHandler::GetBundleSizeOnDisk()
extern void WXSDKManagerHandler_GetBundleSizeOnDisk_m3EF57CBC4ADBA50120AE70D9D711422F292860B5 (void);
// 0x00000741 System.Void WeChatWASM.WXSDKManagerHandler::LogUnityHeapMem()
extern void WXSDKManagerHandler_LogUnityHeapMem_m596EEF2A7D623E910191F1E12728DB1AA5C91A9B (void);
// 0x00000742 System.Void WeChatWASM.WXSDKManagerHandler::ProfilingMemoryDump()
extern void WXSDKManagerHandler_ProfilingMemoryDump_m0036A38C4356ACD47841035B799E9C09C1689A49 (void);
// 0x00000743 System.Void WeChatWASM.WXSDKManagerHandler::SetProfileStatsScript(System.Type)
extern void WXSDKManagerHandler_SetProfileStatsScript_m458015E3F922B0487CA556170FFEEEC47F485CB4 (void);
// 0x00000744 System.Void WeChatWASM.WXSDKManagerHandler::OpenProfileStats()
extern void WXSDKManagerHandler_OpenProfileStats_m83BC146CA7BF891314F76BEF0EA5CF203A6E5E1E (void);
// 0x00000745 System.Void WeChatWASM.WXSDKManagerHandler::LogManagerDebug(System.String)
extern void WXSDKManagerHandler_LogManagerDebug_m27C22B2C99AA25AC1F19276BF41D56CD2F1CE14E (void);
// 0x00000746 System.Void WeChatWASM.WXSDKManagerHandler::LogManagerInfo(System.String)
extern void WXSDKManagerHandler_LogManagerInfo_mC6F249214AB53B50C4B37581CBBAF84DE5E445AA (void);
// 0x00000747 System.Void WeChatWASM.WXSDKManagerHandler::LogManagerLog(System.String)
extern void WXSDKManagerHandler_LogManagerLog_m5CD2C016C93466A0363B349B80D5BB7F50EC5DDE (void);
// 0x00000748 System.Void WeChatWASM.WXSDKManagerHandler::LogManagerWarn(System.String)
extern void WXSDKManagerHandler_LogManagerWarn_mD8AFCFFB92678402BF1C23F0DAA199904538BDBD (void);
// 0x00000749 System.Boolean WeChatWASM.WXSDKManagerHandler::IsCloudTest()
extern void WXSDKManagerHandler_IsCloudTest_mF72CE7220855DEFE379E2EE845E9E92CFB24D30A (void);
// 0x0000074A System.Void WeChatWASM.WXSDKManagerHandler::UncaughtException()
extern void WXSDKManagerHandler_UncaughtException_m340C70224D09FACFBC0241F3E95E76DC43979DE7 (void);
// 0x0000074B WeChatWASM.WXGameClubButton WeChatWASM.WXSDKManagerHandler::CreateGameClubButton(WeChatWASM.WXCreateGameClubButtonParam)
extern void WXSDKManagerHandler_CreateGameClubButton_m09E7217DAA6712F9A58A133B6F20A6A5C3F52BE1 (void);
// 0x0000074C System.Void WeChatWASM.WXSDKManagerHandler::GameClubStyleChangeInt(System.String,System.String,System.Int32)
extern void WXSDKManagerHandler_GameClubStyleChangeInt_m1723A7F6C5F9C5A03BEEA95D4E43BDC9C08C7335 (void);
// 0x0000074D System.Void WeChatWASM.WXSDKManagerHandler::GameClubStyleChangeStr(System.String,System.String,System.String)
extern void WXSDKManagerHandler_GameClubStyleChangeStr_mBD730FD57B71441DD6DB3540BE74DA194C896BF4 (void);
// 0x0000074E System.Void WeChatWASM.WXSDKManagerHandler::CleanAllFileCache(System.Action`1<System.Boolean>)
extern void WXSDKManagerHandler_CleanAllFileCache_mA7EC03F6F186C0FD2FA0875884774D325ED71A76 (void);
// 0x0000074F System.Void WeChatWASM.WXSDKManagerHandler::CleanAllFileCacheCallback(System.String)
extern void WXSDKManagerHandler_CleanAllFileCacheCallback_m46F284537E40AE5D49C42F8979B321C8B7013DC1 (void);
// 0x00000750 System.Void WeChatWASM.WXSDKManagerHandler::CleanFileCache(System.Int32,System.Action`1<WeChatWASM.ReleaseResult>)
extern void WXSDKManagerHandler_CleanFileCache_m53E4A9F81CA0718A61E976527777873077F1A090 (void);
// 0x00000751 System.Void WeChatWASM.WXSDKManagerHandler::CleanFileCacheCallback(System.String)
extern void WXSDKManagerHandler_CleanFileCacheCallback_m247B7EACA668E2237FA8A6CAA1B4536936EBFE10 (void);
// 0x00000752 System.Void WeChatWASM.WXSDKManagerHandler::RemoveFile(System.String,System.Action`1<System.Boolean>)
extern void WXSDKManagerHandler_RemoveFile_m2571F73C3A662ABAF8B9A6C85786DE1E9B96E4BD (void);
// 0x00000753 System.Void WeChatWASM.WXSDKManagerHandler::RemoveFileCallback(System.String)
extern void WXSDKManagerHandler_RemoveFileCallback_mAF6844F0DBAFF25731F5E7F272EA8BD38A3DE2C3 (void);
// 0x00000754 System.String WeChatWASM.WXSDKManagerHandler::get_PluginCachePath()
extern void WXSDKManagerHandler_get_PluginCachePath_m0EC53B0A8D65202EAE3A82CACCC749FC5E58B255 (void);
// 0x00000755 System.String WeChatWASM.WXSDKManagerHandler::GetCachePath(System.String)
extern void WXSDKManagerHandler_GetCachePath_m5480634F31FC7E82C2E9DAE12E557E36AD8659A4 (void);
// 0x00000756 System.Void WeChatWASM.WXSDKManagerHandler::OnLaunchProgress(System.Action`1<WeChatWASM.LaunchEvent>)
extern void WXSDKManagerHandler_OnLaunchProgress_m4D406A2257DFDFDF0FB31D5BADECD19DDFA64382 (void);
// 0x00000757 System.Void WeChatWASM.WXSDKManagerHandler::OnLaunchProgressCallback(System.String)
extern void WXSDKManagerHandler_OnLaunchProgressCallback_mF608F109F2E682BD9E7F35FF24770CC6307C57BC (void);
// 0x00000758 System.Void WeChatWASM.WXSDKManagerHandler::RemoveLaunchProgressCallback(System.String)
extern void WXSDKManagerHandler_RemoveLaunchProgressCallback_mC2656189C2759565640B6B922DE99B5AE6359008 (void);
// 0x00000759 System.Void WeChatWASM.WXSDKManagerHandler::SetDataCDN(System.String)
extern void WXSDKManagerHandler_SetDataCDN_mFE18CE6881707D0EFD6094F9213571EDD4281B90 (void);
// 0x0000075A System.Void WeChatWASM.WXSDKManagerHandler::SetPreloadList(System.String[])
extern void WXSDKManagerHandler_SetPreloadList_m7813E6B509CF4401918E7D621F3E6BD739A4C184 (void);
// 0x0000075B System.Void WeChatWASM.WXSDKManagerHandler::A(System.Double)
extern void WXSDKManagerHandler_A_m4664C74D0D2721190C7914714D121AAFB01AEEAD (void);
// 0x0000075C System.Void WeChatWASM.WXSDKManagerHandler::SetPreferredFramesPerSecond(System.Double)
extern void WXSDKManagerHandler_SetPreferredFramesPerSecond_m9AA73D324748357DEC6ADE9DEE583BE3D4A6B96D (void);
// 0x0000075D System.Void WeChatWASM.WXSDKManagerHandler::BP(System.String,System.String)
extern void WXSDKManagerHandler_BP_mD14106932E5C7A4B567C39894C827C10432BEEC8 (void);
// 0x0000075E WeChatWASM.WXCamera WeChatWASM.WXSDKManagerHandler::CreateCamera(WeChatWASM.CreateCameraOption)
extern void WXSDKManagerHandler_CreateCamera_mBDA7EDD28DF537617D99DF1791FB3EE56A5B6F20 (void);
// 0x0000075F System.Void WeChatWASM.WXSDKManagerHandler::CameraCreateCallback(System.String)
extern void WXSDKManagerHandler_CameraCreateCallback_m1D39F71B077D035EDF08BA077BD45480B7A9097C (void);
// 0x00000760 System.Void WeChatWASM.WXSDKManagerHandler::CameraOnAuthCancelCallback(System.String)
extern void WXSDKManagerHandler_CameraOnAuthCancelCallback_m5C7F3C244591347F02939B7CC3E90B8D329CA35D (void);
// 0x00000761 System.String WeChatWASM.WXSDKManagerHandler::WXSetArrayBuffer(System.Byte[],System.String)
extern void WXSDKManagerHandler_WXSetArrayBuffer_mDAEA932726FFD397D3993F0F71E3CAC05F28B533 (void);
// 0x00000762 System.Void WeChatWASM.WXSDKManagerHandler::CameraOnCameraFrameCallback(System.String)
extern void WXSDKManagerHandler_CameraOnCameraFrameCallback_mF49346DBB7995911F0D33B1844962C400A4B5CE4 (void);
// 0x00000763 System.Void WeChatWASM.WXSDKManagerHandler::CameraOnStopCallback(System.String)
extern void WXSDKManagerHandler_CameraOnStopCallback_m1714F0DC0DB16B8BE92087C7D2F5BCE73BFDB377 (void);
// 0x00000764 System.String WeChatWASM.WXSDKManagerHandler::BR()
extern void WXSDKManagerHandler_BR_m8E49662A09309BEAEAA68526A12E5DB806C3271B (void);
// 0x00000765 WeChatWASM.WXGameRecorder WeChatWASM.WXSDKManagerHandler::GetGameRecorder()
extern void WXSDKManagerHandler_GetGameRecorder_mF6F704C5E1BBD77A377365398DE9465B2A3BD6F8 (void);
// 0x00000766 System.Void WeChatWASM.WXSDKManagerHandler::_OnGameRecorderCallback(System.String)
extern void WXSDKManagerHandler__OnGameRecorderCallback_mDBB303CCABB6DB39A41D3246302674DBFD721190 (void);
// 0x00000767 System.String WeChatWASM.WXSDKManagerHandler::Br()
extern void WXSDKManagerHandler_Br_m22BD724CB9C4E7132096FFB1695F4649CCD1706D (void);
// 0x00000768 WeChatWASM.WXRecorderManager WeChatWASM.WXSDKManagerHandler::GetRecorderManager()
extern void WXSDKManagerHandler_GetRecorderManager_m48035F84F9832C79857A57ABE6F3BE9CABC94805 (void);
// 0x00000769 System.Void WeChatWASM.WXSDKManagerHandler::_OnRecorderErrorCallback(System.String)
extern void WXSDKManagerHandler__OnRecorderErrorCallback_m5D045D0468B10539C2C807C0873BF9FAB61AC745 (void);
// 0x0000076A System.Void WeChatWASM.WXSDKManagerHandler::_OnRecorderFrameRecordedCallback(System.String)
extern void WXSDKManagerHandler__OnRecorderFrameRecordedCallback_m79BE33FBF0D794C2504C3CD10E95E7A98C2ADF1C (void);
// 0x0000076B System.Void WeChatWASM.WXSDKManagerHandler::_OnRecorderInterruptionBeginCallback(System.String)
extern void WXSDKManagerHandler__OnRecorderInterruptionBeginCallback_m6F6ED98CCEA86BC618AAB6D6CED3968363197856 (void);
// 0x0000076C System.Void WeChatWASM.WXSDKManagerHandler::_OnRecorderInterruptionEndCallback(System.String)
extern void WXSDKManagerHandler__OnRecorderInterruptionEndCallback_m9722A5D5F828D95DD1A9BFB44823AE04414DDFD9 (void);
// 0x0000076D System.Void WeChatWASM.WXSDKManagerHandler::_OnRecorderPauseCallback(System.String)
extern void WXSDKManagerHandler__OnRecorderPauseCallback_m082E5575F9DD810276441B98EE8F2EF74798E132 (void);
// 0x0000076E System.Void WeChatWASM.WXSDKManagerHandler::_OnRecorderStartCallback(System.String)
extern void WXSDKManagerHandler__OnRecorderStartCallback_m057BD0CCA8D87E95793A757C4772C20CB9EA5619 (void);
// 0x0000076F System.Void WeChatWASM.WXSDKManagerHandler::_OnRecorderStopCallback(System.String)
extern void WXSDKManagerHandler__OnRecorderStopCallback_mECA63AE4D6D7239F061A34B0925576F91E35E301 (void);
// 0x00000770 System.Void WeChatWASM.WXSDKManagerHandler::_OnRecorderResumeCallback(System.String)
extern void WXSDKManagerHandler__OnRecorderResumeCallback_m11FE7BD022D36EBE2AB4E5BC453E0F51A9B00461 (void);
// 0x00000771 System.String WeChatWASM.WXSDKManagerHandler::Bp(System.String,System.String)
extern void WXSDKManagerHandler_Bp_m0FF7AD727E913E69840482BEBF348344951FEC59 (void);
// 0x00000772 WeChatWASM.WXUploadTask WeChatWASM.WXSDKManagerHandler::UploadFile(WeChatWASM.UploadFileOption)
extern void WXSDKManagerHandler_UploadFile_m42895A82E5423BF33D06F727B21170283A713DA7 (void);
// 0x00000773 System.Void WeChatWASM.WXSDKManagerHandler::UploadFileCallback(System.String)
extern void WXSDKManagerHandler_UploadFileCallback_m71A50B1E7975CA0D95AA518E94735F33406246A9 (void);
// 0x00000774 System.Void WeChatWASM.WXSDKManagerHandler::_OnHeadersReceivedCallback(System.String)
extern void WXSDKManagerHandler__OnHeadersReceivedCallback_m3C2EB9451908572280B25516C5CDB4BBCBAB1263 (void);
// 0x00000775 System.Void WeChatWASM.WXSDKManagerHandler::_OnProgressUpdateCallback(System.String)
extern void WXSDKManagerHandler__OnProgressUpdateCallback_m0AFD6FD08C0B1A9F5671280C4943BF78DE320373 (void);
// 0x00000776 System.String WeChatWASM.WXSDKManagerHandler::u(System.String)
extern void WXSDKManagerHandler_u_m79211EF4B1E512F21937EDD26D5FCD7279CFB61D (void);
// 0x00000777 WeChatWASM.WXChat WeChatWASM.WXSDKManagerHandler::CreateMiniGameChat(WeChatWASM.WXChatOptions)
extern void WXSDKManagerHandler_CreateMiniGameChat_mB7959EDACFAD56326E6D7D8893723321995FFAC2 (void);
// 0x00000778 System.Void WeChatWASM.WXSDKManagerHandler::OnWXChatCallback(System.String)
extern void WXSDKManagerHandler_OnWXChatCallback_mD27C8B34CBD509D871D8B016D184EC3BD67014F6 (void);
// 0x00000779 System.Void WeChatWASM.WXSDKManagerHandler::_OnNeedPrivacyAuthorizationCallback(System.String)
extern void WXSDKManagerHandler__OnNeedPrivacyAuthorizationCallback_mC70D9BF5450542643D1C3B31CED758E8B3CD7626 (void);
// 0x0000077A System.Void WeChatWASM.WXSDKManagerHandler::BS()
extern void WXSDKManagerHandler_BS_mFD638E8AB7316E4C1FE9312ADDCD0E9AABC6A089 (void);
// 0x0000077B System.Void WeChatWASM.WXSDKManagerHandler::OnNeedPrivacyAuthorization(System.Action`1<System.String>)
extern void WXSDKManagerHandler_OnNeedPrivacyAuthorization_m9121F7F4ABF4EE2BF4CC56E0C368F27DAF1095E6 (void);
// 0x0000077C System.Void WeChatWASM.WXSDKManagerHandler::V(System.String)
extern void WXSDKManagerHandler_V_mA6AB2AD48E951E0127C9C62C3F54F55A38B5CD08 (void);
// 0x0000077D System.Void WeChatWASM.WXSDKManagerHandler::PrivacyAuthorizeResolve(WeChatWASM.PrivacyAuthorizeResolveOption)
extern void WXSDKManagerHandler_PrivacyAuthorizeResolve_mCD977032861782F05DA0CDF88113D83A69A4CD4D (void);
// 0x0000077E System.Void WeChatWASM.WXSDKManagerHandler::.ctor()
extern void WXSDKManagerHandler__ctor_m38D47BEC8FED1E450449746E33DF21045674FA4B (void);
// 0x0000077F System.Void WeChatWASM.WXSDKManagerHandler::.cctor()
extern void WXSDKManagerHandler__cctor_mEFFD0FDC42C15C3E9EDED0A31B7A3B2A23D8F78B (void);
// 0x00000780 System.Void WeChatWASM.WXSDKManagerHandler/OnMessageCallback::.ctor(System.Object,System.IntPtr)
extern void OnMessageCallback__ctor_mCAC48245FB9CF58F9035489D832308C93FF2CB0D (void);
// 0x00000781 System.Void WeChatWASM.WXSDKManagerHandler/OnMessageCallback::Invoke(System.Int32,System.IntPtr,System.Int32)
extern void OnMessageCallback_Invoke_mB7AB9D4761483D638895AAEA6BBB694F6F2D9DB9 (void);
// 0x00000782 System.IAsyncResult WeChatWASM.WXSDKManagerHandler/OnMessageCallback::BeginInvoke(System.Int32,System.IntPtr,System.Int32,System.AsyncCallback,System.Object)
extern void OnMessageCallback_BeginInvoke_mCDD6E64DA85E6CED0FE124657620AF53E3509F8B (void);
// 0x00000783 System.Void WeChatWASM.WXSDKManagerHandler/OnMessageCallback::EndInvoke(System.IAsyncResult)
extern void OnMessageCallback_EndInvoke_mE9705D406C9B2E306EA7A3D16D808E09C7E21E27 (void);
// 0x00000784 System.Void WeChatWASM.WXSDKManagerHandler/OnErrorCallback::.ctor(System.Object,System.IntPtr)
extern void OnErrorCallback__ctor_m5B1AC02DE84E0F472FE59B71F8874F57920631AC (void);
// 0x00000785 System.Void WeChatWASM.WXSDKManagerHandler/OnErrorCallback::Invoke(System.Int32,System.IntPtr)
extern void OnErrorCallback_Invoke_m0CEAAAD37ACCFDB4EDBDB10743647E0F0B3C595D (void);
// 0x00000786 System.IAsyncResult WeChatWASM.WXSDKManagerHandler/OnErrorCallback::BeginInvoke(System.Int32,System.IntPtr,System.AsyncCallback,System.Object)
extern void OnErrorCallback_BeginInvoke_mC22090E89B17D33714DA594B8CFB7BB38112823E (void);
// 0x00000787 System.Void WeChatWASM.WXSDKManagerHandler/OnErrorCallback::EndInvoke(System.IAsyncResult)
extern void OnErrorCallback_EndInvoke_m1D6B0F2E1E9CE5FBF56C9BF68D63543488194F88 (void);
// 0x00000788 System.Void WeChatWASM.WXSDKManagerHandler/OnCloseCallback::.ctor(System.Object,System.IntPtr)
extern void OnCloseCallback__ctor_mED9B269C6E307995A2964E1AD16F34543629BE32 (void);
// 0x00000789 System.Void WeChatWASM.WXSDKManagerHandler/OnCloseCallback::Invoke(System.Int32,System.IntPtr)
extern void OnCloseCallback_Invoke_mE7003EDD84DCEFE256291AE966C96447BC303C17 (void);
// 0x0000078A System.IAsyncResult WeChatWASM.WXSDKManagerHandler/OnCloseCallback::BeginInvoke(System.Int32,System.IntPtr,System.AsyncCallback,System.Object)
extern void OnCloseCallback_BeginInvoke_m228CDBE350784A62B12647399200FDCC06914F48 (void);
// 0x0000078B System.Void WeChatWASM.WXSDKManagerHandler/OnCloseCallback::EndInvoke(System.IAsyncResult)
extern void OnCloseCallback_EndInvoke_m75363E2E935180149F55E7FD353BF605EE3760A5 (void);
// 0x0000078C System.Void WeChatWASM.WXSDKManagerHandler/b::.cctor()
extern void b__cctor_m3D82D921A5493667F83063D772B72D074C3C8F24 (void);
// 0x0000078D System.Void WeChatWASM.WXSDKManagerHandler/b::.ctor()
extern void b__ctor_m1B7B124D1A11723FB9756344BEDE8F58AA826DCC (void);
// 0x0000078E System.Void WeChatWASM.WXSDKManagerHandler/b::A(WeChatWASM.OnAddToFavoritesListenerResult)
extern void b_A_m26C823AB15EEEE0324FDB9B3AB0FF49A409E71C6 (void);
// 0x0000078F System.Void WeChatWASM.WXSDKManagerHandler/b::A(WeChatWASM.OnCopyUrlListenerResult)
extern void b_A_m8CD159020CCC9640ED09FFAE2776756AF2519CA8 (void);
// 0x00000790 System.Void WeChatWASM.WXSDKManagerHandler/b::A(WeChatWASM.OnHandoffListenerResult)
extern void b_A_m7CAB41555C37A534C9EB999306FB79971E3F8982 (void);
// 0x00000791 System.Void WeChatWASM.WXSDKManagerHandler/b::A(WeChatWASM.OnShareTimelineListenerResult)
extern void b_A_mE59781BD2700C23B9BF21E4E1C7F2A61DB01AAC0 (void);
// 0x00000792 System.Void WeChatWASM.WXSDKManagerHandler/b::A(WeChatWASM.OnGameLiveStateChangeCallbackResponse)
extern void b_A_mAC787F340EFA9831CB01155630CCB81026E4E9CD (void);
// 0x00000793 System.Void WeChatWASM.WXSDKManagerHandler/b::A(WeChatWASM.WXShareAppMessageParam)
extern void b_A_mEF8A31E65455D3B33BB037850161CAFE86D397EE (void);
// 0x00000794 System.Void WeChatWASM.UDPClient::.ctor(System.String,System.Int32,System.Int32)
extern void UDPClient__ctor_m484B9BBB0948C03F2F6D5C1B925B7216452DE4A6 (void);
// 0x00000795 System.Void WeChatWASM.UDPClient::SetCallBack(System.Action`1<System.Byte[]>,System.Action`1<System.String>,System.Action`1<System.String>)
extern void UDPClient_SetCallBack_mA0CD3DCCCBDCF72D2C49C3D48E5867E8D3F99A01 (void);
// 0x00000796 System.Void WeChatWASM.UDPClient::Send(System.Byte[],System.Int32,System.Int32)
extern void UDPClient_Send_m4491E0E7572CBB8E6920C63ED535A170D2F8A0A6 (void);
// 0x00000797 System.Void WeChatWASM.UDPClient::OnMessage(System.Byte[])
extern void UDPClient_OnMessage_m11F747E473B777DE79DA50B0DD85B46350E8CA47 (void);
// 0x00000798 System.Void WeChatWASM.UDPClient::OnClose(System.String)
extern void UDPClient_OnClose_m994C38ECFA4D0C6DB0BD35F65C238BB23680C5C6 (void);
// 0x00000799 System.Void WeChatWASM.UDPClient::OnError(System.String)
extern void UDPClient_OnError_m8B9549EACF12A1EA0AB9CA1A0BC6C43150AC802F (void);
// 0x0000079A System.Void WeChatWASM.UDPClient::Destroy()
extern void UDPClient_Destroy_m64C5EDDF971996626F0C97F4452C27DDCBA6DB1C (void);
// 0x0000079B System.Void WeChatWASM.UDPSocketManager::.ctor()
extern void UDPSocketManager__ctor_m77491DD98A7638DCEFC9136BF248B1DA1F74688D (void);
// 0x0000079C System.Void WeChatWASM.UDPSocketManager::OnMessage(System.Int32,System.Byte[])
extern void UDPSocketManager_OnMessage_mDE8CC4EC59E3A4EE7EFEA8D5F09D67ABFD97ED19 (void);
// 0x0000079D System.Void WeChatWASM.UDPSocketManager::OnClose(System.Int32,System.String)
extern void UDPSocketManager_OnClose_m2338DB278802E39FBBB80A60C3F181B98F6B80CA (void);
// 0x0000079E System.Void WeChatWASM.UDPSocketManager::OnError(System.Int32,System.String)
extern void UDPSocketManager_OnError_mF74A45408C7AF04F0895B391BF8C42E60455A1D0 (void);
// 0x0000079F System.Void WeChatWASM.UDPSocketManager::AddClient(WeChatWASM.UDPClient)
extern void UDPSocketManager_AddClient_m9476091DB05E736B0C06038C076129B1736CCDDF (void);
// 0x000007A0 System.Void WeChatWASM.UDPSocketManager::DeleteClient(WeChatWASM.UDPClient)
extern void UDPSocketManager_DeleteClient_m2B9295F1C1C5768402D10868FF09430FFEEFF3D1 (void);
// 0x000007A1 System.Void WeChatWASM.UDPSocketManager::.cctor()
extern void UDPSocketManager__cctor_m9D1DF67E5025999FB52C767715E89A3911BCE979 (void);
// 0x000007A2 System.Void WeChatWASM.WXUpdateManager::.ctor(System.String)
extern void WXUpdateManager__ctor_mE4279282B4A07D5ADA2E5702E14A155D97D9A27B (void);
// 0x000007A3 System.Void WeChatWASM.WXUpdateManager::A(System.String)
extern void WXUpdateManager_A_m8F9F1CC44158D958B4193DC0B65959F62BD86148 (void);
// 0x000007A4 System.Void WeChatWASM.WXUpdateManager::ApplyUpdate()
extern void WXUpdateManager_ApplyUpdate_m1FE7AD2134B0A38B2ACE556D3862DCF6FD12A5CB (void);
// 0x000007A5 System.Void WeChatWASM.WXUpdateManager::a(System.String)
extern void WXUpdateManager_a_m9AED771B84B3C75957E96F91AECF67AE31B6F0B9 (void);
// 0x000007A6 System.Void WeChatWASM.WXUpdateManager::OnCheckForUpdate(System.Action`1<WeChatWASM.OnCheckForUpdateListenerResult>)
extern void WXUpdateManager_OnCheckForUpdate_m3980609043D82C4E40BCDC7FF05D9F2563BD400B (void);
// 0x000007A7 System.Void WeChatWASM.WXUpdateManager::B(System.String)
extern void WXUpdateManager_B_m2FADEC6DDCB3F47EC7BB146A7AC1CF0FB2920831 (void);
// 0x000007A8 System.Void WeChatWASM.WXUpdateManager::OnUpdateFailed(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WXUpdateManager_OnUpdateFailed_m622EF73958D0F130245FD32A856E33F298B5156D (void);
// 0x000007A9 System.Void WeChatWASM.WXUpdateManager::b(System.String)
extern void WXUpdateManager_b_mA8AB48A37C757E6AE0CA307D0E57D9E8F69532BE (void);
// 0x000007AA System.Void WeChatWASM.WXUpdateManager::OnUpdateReady(System.Action`1<WeChatWASM.GeneralCallbackResult>)
extern void WXUpdateManager_OnUpdateReady_m9BD7A5614BD4EC426817802DF06504EB3AC38DD0 (void);
// 0x000007AB System.Void WeChatWASM.WXUploadTask::.ctor(System.String)
extern void WXUploadTask__ctor_mD77B5197B5DF3570BA7799708799801D8CA06BBC (void);
// 0x000007AC System.Void WeChatWASM.WXUploadTask::A(System.String)
extern void WXUploadTask_A_m83AD339A6BCC2F90BA9639692050509E810A35F1 (void);
// 0x000007AD System.Void WeChatWASM.WXUploadTask::Abort(System.String)
extern void WXUploadTask_Abort_mF6DE262872E39DB49197F4C0B6216AC292E3388F (void);
// 0x000007AE System.Void WeChatWASM.WXUploadTask::a(System.String)
extern void WXUploadTask_a_m4455322F2533304EB2CEDF81DE1F98D4F956A42F (void);
// 0x000007AF System.Void WeChatWASM.WXUploadTask::OffHeadersReceived(System.Action`1<WeChatWASM.OnHeadersReceivedCallbackResult>)
extern void WXUploadTask_OffHeadersReceived_m57DA255F9D0246F84B8720B8C1D33E65976FC1E8 (void);
// 0x000007B0 System.Void WeChatWASM.WXUploadTask::B(System.String)
extern void WXUploadTask_B_m43490388BC195F908C384201B1EF1343BABDFF70 (void);
// 0x000007B1 System.Void WeChatWASM.WXUploadTask::OffProgressUpdate(System.Action`1<WeChatWASM.UploadTaskOnProgressUpdateCallbackResult>)
extern void WXUploadTask_OffProgressUpdate_m0CDD8103C4FF975F9C9BD49DD4AFC666878BDC40 (void);
// 0x000007B2 System.Void WeChatWASM.WXUploadTask::b(System.String)
extern void WXUploadTask_b_mD41BDCD652E8DD69C891B7B6B43B4B926330EAB0 (void);
// 0x000007B3 System.Void WeChatWASM.WXUploadTask::OnHeadersReceived(System.Action`1<WeChatWASM.OnHeadersReceivedCallbackResult>)
extern void WXUploadTask_OnHeadersReceived_mFAA7A965DBB1B65B623F7330E9B06AB540E89A9E (void);
// 0x000007B4 System.Void WeChatWASM.WXUploadTask::C(System.String)
extern void WXUploadTask_C_mDD73F701259E973CF4D16A9A9A743A067D4B1402 (void);
// 0x000007B5 System.Void WeChatWASM.WXUploadTask::OnProgressUpdate(System.Action`1<WeChatWASM.UploadTaskOnProgressUpdateCallbackResult>)
extern void WXUploadTask_OnProgressUpdate_m6C18B12F03668AE1AB1FDB2D4F9646E07F26C6A7 (void);
// 0x000007B6 System.Void WeChatWASM.UploadFileOption::.ctor()
extern void UploadFileOption__ctor_mB37E1C22289F1578EC908F929FECD05063EA7F69 (void);
// 0x000007B7 System.Void WeChatWASM.UploadFileSuccessCallbackResult::.ctor()
extern void UploadFileSuccessCallbackResult__ctor_m2E228B8F3CA6A54FB3D8FC55EDD4D9E7D0870428 (void);
// 0x000007B8 System.Void WeChatWASM.OnHeadersReceivedCallbackResult::.ctor()
extern void OnHeadersReceivedCallbackResult__ctor_m65D27B2CFE2F1BEE3AC0CB3C51AF1B9D57E5A782 (void);
// 0x000007B9 System.Void WeChatWASM.UploadTaskOnProgressUpdateCallbackResult::.ctor()
extern void UploadTaskOnProgressUpdateCallbackResult__ctor_mFAD1384075D15AEB879AEFA102B885BA5300E90A (void);
// 0x000007BA System.Void WeChatWASM.WXUserInfoButton::A(System.String)
extern void WXUserInfoButton_A_mF87F824642FE8DE346D36C37E7EED61955C3295F (void);
// 0x000007BB System.Void WeChatWASM.WXUserInfoButton::a(System.String)
extern void WXUserInfoButton_a_m00432EE2C76BF7CB780D47998149F5E9E3BDBC1E (void);
// 0x000007BC System.Void WeChatWASM.WXUserInfoButton::B(System.String)
extern void WXUserInfoButton_B_m04AAEFC96EC690AF9761EF8BFC9B7DB9476EB6FD (void);
// 0x000007BD System.Void WeChatWASM.WXUserInfoButton::b(System.String)
extern void WXUserInfoButton_b_m18200B0BC2CA6A78E49C3198D3D8DB74F78DB2A2 (void);
// 0x000007BE System.Void WeChatWASM.WXUserInfoButton::C(System.String)
extern void WXUserInfoButton_C_m09EEDBB2CD73AAC9431C9CC23B5F2806A55F5F6A (void);
// 0x000007BF System.Void WeChatWASM.WXUserInfoButton::.ctor(System.String)
extern void WXUserInfoButton__ctor_m014080AD98A37E26623D5F5ECA960DAAED61B277 (void);
// 0x000007C0 System.Void WeChatWASM.WXUserInfoButton::InvokeCallback(WeChatWASM.WXUserInfoResponse)
extern void WXUserInfoButton_InvokeCallback_mDCF9738081240CC2B3E7EA470C82C02FBA051014 (void);
// 0x000007C1 System.Void WeChatWASM.WXUserInfoButton::Destroy()
extern void WXUserInfoButton_Destroy_m580CBBE01F66772AF857D3897F492FCB84791877 (void);
// 0x000007C2 System.Void WeChatWASM.WXUserInfoButton::Hide()
extern void WXUserInfoButton_Hide_m3E7B0827CA298C17DE91106F131B1C2843DB0869 (void);
// 0x000007C3 System.Void WeChatWASM.WXUserInfoButton::OffTap()
extern void WXUserInfoButton_OffTap_m4A9C65F16CA3FA2310E99BDE2628BE6C90E8B281 (void);
// 0x000007C4 System.Void WeChatWASM.WXUserInfoButton::OnTap(System.Action`1<WeChatWASM.WXUserInfoResponse>)
extern void WXUserInfoButton_OnTap_mFE1DDE836D65F1DB187906360EE5618BA30BDC1A (void);
// 0x000007C5 System.Void WeChatWASM.WXUserInfoButton::Show()
extern void WXUserInfoButton_Show_m686FE89211F7ED31D1DC6C9E9FD8B4841DB595AA (void);
// 0x000007C6 System.Void WeChatWASM.WXUserInfoButton::.cctor()
extern void WXUserInfoButton__cctor_m199B57EA2CA6AA73CFE38A7033470492CF775F76 (void);
// 0x000007C7 System.Void WeChatWASM.WXVideo::A(System.String)
extern void WXVideo_A_mA954DD718B8E161694AF6F9DB1E08742EA7E2133 (void);
// 0x000007C8 System.Void WeChatWASM.WXVideo::A(System.String,System.String)
extern void WXVideo_A_m1F2B4D0610B4EC5A34C8309199033B0869B44565 (void);
// 0x000007C9 System.Void WeChatWASM.WXVideo::a(System.String)
extern void WXVideo_a_m0F806EE3268E15F98A45F252A5EB097E9D96BF42 (void);
// 0x000007CA System.Void WeChatWASM.WXVideo::B(System.String)
extern void WXVideo_B_m8C4A1D6D5DCC11A067725CC3AB6944FD9E018456 (void);
// 0x000007CB System.Void WeChatWASM.WXVideo::b(System.String)
extern void WXVideo_b_m389B900E05223B7316BFEC71740B98F63F0711C0 (void);
// 0x000007CC System.Void WeChatWASM.WXVideo::A(System.String,System.Int32)
extern void WXVideo_A_mF9C2646AA8F124E8318AF4541FDCF7DCF6383B08 (void);
// 0x000007CD System.Void WeChatWASM.WXVideo::a(System.String,System.Int32)
extern void WXVideo_a_m2B96236796C4AA903DA09E855EA8F4B6FAF3ACEF (void);
// 0x000007CE System.Void WeChatWASM.WXVideo::C(System.String)
extern void WXVideo_C_mF08D2E77543319EC13D77A6C8678FC779D00D214 (void);
// 0x000007CF System.Void WeChatWASM.WXVideo::a(System.String,System.String)
extern void WXVideo_a_m6C185DBE230427D9BE592576A6EEFCF89EC0D1A1 (void);
// 0x000007D0 System.Void WeChatWASM.WXVideo::A(System.String,System.String,System.String)
extern void WXVideo_A_m4168276950B112793EEECF498097D5DB3952BA0B (void);
// 0x000007D1 System.Void WeChatWASM.WXVideo::.ctor(System.String,WeChatWASM.WXCreateVideoParam)
extern void WXVideo__ctor_mEE4411332EA65C404A06649A57EECC7F1F0D2E98 (void);
// 0x000007D2 System.Void WeChatWASM.WXVideo::_HandleCallBack(WeChatWASM.WXVideoCallback)
extern void WXVideo__HandleCallBack_m708ECF4999EEC43E13505C184B38FF1CCE46D87B (void);
// 0x000007D3 System.String WeChatWASM.WXVideo::get_src()
extern void WXVideo_get_src_m6708B85CE0CDCF0E8613087377CFFF2479C97C08 (void);
// 0x000007D4 System.Void WeChatWASM.WXVideo::set_src(System.String)
extern void WXVideo_set_src_mBF7C29AAD0A1EDD9AF9617069E90D23797FD3D14 (void);
// 0x000007D5 System.String WeChatWASM.WXVideo::get_poster()
extern void WXVideo_get_poster_mAC1E2145C5F0B64946C202DE97B78ECF32F9E8E2 (void);
// 0x000007D6 System.Void WeChatWASM.WXVideo::set_poster(System.String)
extern void WXVideo_set_poster_mF408A5FA4E98C64EBE57DDD10B98D2DAFF8DF249 (void);
// 0x000007D7 System.Double WeChatWASM.WXVideo::get_x()
extern void WXVideo_get_x_m7E136C8DC99C5158AF5541C82D09A4ABC94CB134 (void);
// 0x000007D8 System.Void WeChatWASM.WXVideo::set_x(System.Double)
extern void WXVideo_set_x_m6976BB23ED8FDA99413735F64BDB9CA21D5976FC (void);
// 0x000007D9 System.Double WeChatWASM.WXVideo::get_y()
extern void WXVideo_get_y_m4AEEE6BF5B1D9645F9C94A5A9655CAD8AB85FC84 (void);
// 0x000007DA System.Void WeChatWASM.WXVideo::set_y(System.Double)
extern void WXVideo_set_y_mB17975C09B3515AC3ED57044073D4AB564E73F8B (void);
// 0x000007DB System.Double WeChatWASM.WXVideo::get_width()
extern void WXVideo_get_width_mA3636F15277249EDC237CDE4D5D351EB4CEBCFA9 (void);
// 0x000007DC System.Void WeChatWASM.WXVideo::set_width(System.Double)
extern void WXVideo_set_width_m1479C1DF020E8104D0094FB34EF2216B23DDB995 (void);
// 0x000007DD System.Double WeChatWASM.WXVideo::get_height()
extern void WXVideo_get_height_m5B56D1D51D7FBECA29B92AD3B5FFDE2E1AAD8A5A (void);
// 0x000007DE System.Void WeChatWASM.WXVideo::set_height(System.Double)
extern void WXVideo_set_height_m525C0940749FBEB14C48E78BC4E23D3911C5B4C8 (void);
// 0x000007DF System.Boolean WeChatWASM.WXVideo::get_isPlaying()
extern void WXVideo_get_isPlaying_m12A2BD6BF127399833D025A73B936402BF8DFC8D (void);
// 0x000007E0 System.Void WeChatWASM.WXVideo::Play()
extern void WXVideo_Play_mCD7C7FB8D9DD26AE5DD76361857F44E4F29C1311 (void);
// 0x000007E1 System.Void WeChatWASM.WXVideo::OnPlay(System.Action)
extern void WXVideo_OnPlay_m4B6D265CA005DD27476E9B91448E5845C16B237C (void);
// 0x000007E2 System.Void WeChatWASM.WXVideo::OffPlay(System.Action)
extern void WXVideo_OffPlay_m37E36A6EC024DE507BCFE72CC985BBEE670D5DDF (void);
// 0x000007E3 System.Void WeChatWASM.WXVideo::OnEnded(System.Action)
extern void WXVideo_OnEnded_m3D6327083E6A503B87A494971F1BD4983ED396D1 (void);
// 0x000007E4 System.Void WeChatWASM.WXVideo::OffEnded(System.Action)
extern void WXVideo_OffEnded_m19D53D340FED07AD74156186744B53C68DAD1AB0 (void);
// 0x000007E5 System.Void WeChatWASM.WXVideo::OnError(System.Action)
extern void WXVideo_OnError_m5A6BD8FEF518A8EB8031D21CF71BB98EEA618B0A (void);
// 0x000007E6 System.Void WeChatWASM.WXVideo::OffError(System.Action)
extern void WXVideo_OffError_mC34991C23942155D6DC00467C2BCC07C89279820 (void);
// 0x000007E7 System.Void WeChatWASM.WXVideo::OnPause(System.Action)
extern void WXVideo_OnPause_m0A7D67369472FEDFB31CDD1FE386535A7A6E9679 (void);
// 0x000007E8 System.Void WeChatWASM.WXVideo::OffPause(System.Action)
extern void WXVideo_OffPause_mA9C00F9B3C957AB0BB21A0988C89D2AAD55666BF (void);
// 0x000007E9 System.Void WeChatWASM.WXVideo::OnWaiting(System.Action)
extern void WXVideo_OnWaiting_m4CBD63CA147855D288FA09628F74243ADA1C400F (void);
// 0x000007EA System.Void WeChatWASM.WXVideo::OffWaiting(System.Action)
extern void WXVideo_OffWaiting_m415B34EFEFAB45F06952494D894F13A2168C5654 (void);
// 0x000007EB System.Void WeChatWASM.WXVideo::Destroy()
extern void WXVideo_Destroy_m2DE57580636DE80E3A81F49441D6C942816C8486 (void);
// 0x000007EC System.Void WeChatWASM.WXVideo::ExitFullScreen()
extern void WXVideo_ExitFullScreen_m26AE831DDF84468E3D4D057CEB7F8AF4B5DE1997 (void);
// 0x000007ED System.Void WeChatWASM.WXVideo::Pause()
extern void WXVideo_Pause_m0560DCAC8E6FF8A0515FEC78111F204A20A5A04A (void);
// 0x000007EE System.Void WeChatWASM.WXVideo::RequestFullScreen(System.Int32)
extern void WXVideo_RequestFullScreen_m47836A8932C848DB71BE15BB068019495E327A39 (void);
// 0x000007EF System.Void WeChatWASM.WXVideo::Seek(System.Int32)
extern void WXVideo_Seek_m8A18917362FD6EA5D5A5E9F698E7ABC039B79BA7 (void);
// 0x000007F0 System.Void WeChatWASM.WXVideo::Stop()
extern void WXVideo_Stop_m3C09D13BA13DE742B0B39F8AE2A39F21217D8C82 (void);
// 0x000007F1 System.Void WeChatWASM.WXVideo::OnTimeUpdate(System.Action`1<WeChatWASM.WXVideoTimeUpdate>)
extern void WXVideo_OnTimeUpdate_m47ADCB1A1CDB8E88B15347B1ABE6453B7B5A2FBB (void);
// 0x000007F2 System.Void WeChatWASM.WXVideo::OffTimeUpdate(System.Action`1<WeChatWASM.WXVideoTimeUpdate>)
extern void WXVideo_OffTimeUpdate_m0C362F4D87A0EC2A2DE49DE50B97DBB8001446CC (void);
// 0x000007F3 System.Void WeChatWASM.WXVideo::OnProgress(System.Action`1<WeChatWASM.WXVideoProgress>)
extern void WXVideo_OnProgress_mA442AAFCA40CD392B366B02496E89833F20E63D8 (void);
// 0x000007F4 System.Void WeChatWASM.WXVideo::OffProgress(System.Action`1<WeChatWASM.WXVideoProgress>)
extern void WXVideo_OffProgress_m0D96DE5CCB8F42FA6FD3C59574E7095D4EE72145 (void);
// 0x000007F5 System.Void WeChatWASM.WXVideo::.cctor()
extern void WXVideo__cctor_mC89C0359C234BEC20938850CC0C77C7F10E51914 (void);
// 0x000007F6 System.Void WeChatWASM.WXVideo::A()
extern void WXVideo_A_m31030BCD3571A728528E14EDB1B03564FEA6E95E (void);
// 0x000007F7 System.Void WeChatWASM.WXVideo::a()
extern void WXVideo_a_mC5F7B0C7111ADD5DA2AA57169EE0874FF7AA0CE0 (void);
// 0x000007F8 System.Void WeChatWASM.WXVideo::B()
extern void WXVideo_B_m09616AC965B265CB4D8E7354C50DB94CBCED7828 (void);
// 0x000007F9 System.Void WeChatWASM.WXVideoDecoder::.ctor(System.String)
extern void WXVideoDecoder__ctor_m9B3EA162CB13055C52CC62AD568416D9C05D756B (void);
// 0x000007FA System.String WeChatWASM.WXVideoDecoder::A(System.String)
extern void WXVideoDecoder_A_mA9CC35C18D80D968667C233937A041E3C13BBAF5 (void);
// 0x000007FB WeChatWASM.FrameDataOptions WeChatWASM.WXVideoDecoder::GetFrameData()
extern void WXVideoDecoder_GetFrameData_m4D31A329AF7156F772E41D0DBC91FE3C49DB5F66 (void);
// 0x000007FC System.Void WeChatWASM.WXVideoDecoder::a(System.String)
extern void WXVideoDecoder_a_m95AF245275836B82683E2C7D6D2CD97A71FD2B44 (void);
// 0x000007FD System.Void WeChatWASM.WXVideoDecoder::Remove()
extern void WXVideoDecoder_Remove_mE27FB0457AB06C1CD733E5E7A29232DE3ECE8EEB (void);
// 0x000007FE System.Void WeChatWASM.WXVideoDecoder::A(System.String,System.Double)
extern void WXVideoDecoder_A_m2DA4107B1994C26A198BD9B38951AC80AAA62808 (void);
// 0x000007FF System.Void WeChatWASM.WXVideoDecoder::Seek(System.Double)
extern void WXVideoDecoder_Seek_mEF6233784B3A783267630182D11591B645DCBE82 (void);
// 0x00000800 System.Void WeChatWASM.WXVideoDecoder::A(System.String,System.String)
extern void WXVideoDecoder_A_mC04899C7AC10F17A0CBEB5F2774A2FC61A66DAC0 (void);
// 0x00000801 System.Void WeChatWASM.WXVideoDecoder::Start(WeChatWASM.VideoDecoderStartOption)
extern void WXVideoDecoder_Start_m693C475622713400C4A39ADE19CF7B0CDED6BA43 (void);
// 0x00000802 System.Void WeChatWASM.WXVideoDecoder::B(System.String)
extern void WXVideoDecoder_B_mDAF92408E9FB19FDF04D6A6DDCC6BFC005FE2369 (void);
// 0x00000803 System.Void WeChatWASM.WXVideoDecoder::Stop()
extern void WXVideoDecoder_Stop_m941A5CFCD440F160C21750077CAE0E279F08F28B (void);
// 0x00000804 System.Void WeChatWASM.WXVideoDecoder::a(System.String,System.String)
extern void WXVideoDecoder_a_mE20C1E0C243196F9B19E333498D6F41DBB6D6C18 (void);
// 0x00000805 System.Void WeChatWASM.WXVideoDecoder::Off(System.String)
extern void WXVideoDecoder_Off_m18D01C3C71270757C56EFF8FC9F60F86F1C7FFC3 (void);
// 0x00000806 System.Void WeChatWASM.WXVideoDecoder::B(System.String,System.String)
extern void WXVideoDecoder_B_m4DA41D996448E6F70AE79329FBD4665E2DCA6CCE (void);
// 0x00000807 System.Void WeChatWASM.WXVideoDecoder::On(System.String,System.Action`1<System.String>)
extern void WXVideoDecoder_On_mF04B5B803CAAECE7E5F2DDF0E0E667A6D7678B77 (void);
// 0x00000808 System.Void WeChatWASM.IWXAdResizable::OnResizeCallback(WeChatWASM.WXADResizeResponse)
// 0x00000809 System.Void WeChatWASM.IWXAdVideoCloseable::OnCloseCallback(WeChatWASM.WXRewardedVideoAdOnCloseResponse)
// 0x0000080A System.Void WeChatWASM.IWXADCloseable::OnCloseCallback()
// 0x0000080B System.Void WeChatWASM.IWXADHideable::OnHideCallback()
// 0x0000080C System.UInt32 A.C::A(System.String)
extern void C_A_m7B3D910143258596FE37FF40986F10BAC7A56BA8 (void);
static Il2CppMethodPointer s_methodPointers[2060] = 
{
	A__ctor_mD2C8A206ADA7E9B311035F305B4765101C19AE5A,
	a__ctor_m9CBB7D8CC082AB77F1A7BACFADD193AD858C5E44,
	a__ctor_mD502D73051687A347BD946B842E5764424A9C7FA,
	PlayerPrefs_SetInt_m0B50B3EFF113A2E6A01E18CB839AC32D42CE6081,
	PlayerPrefs_GetInt_mFDDE681FB8681AF55487A406939A63879A1971BD,
	PlayerPrefs_SetString_m240AFEEBAB31ACA324135E1231FB2A60C8099AC7,
	PlayerPrefs_GetString_m9A8A6866EE5AD9B71A0070397F6031471AD0AAC2,
	PlayerPrefs_SetFloat_mAB94151B0C3595B0F5553E0725E61C722DDCEC52,
	PlayerPrefs_GetFloat_mF0318A2480AEDCFD4A390145FD140CC2CB96D859,
	PlayerPrefs_DeleteAll_m9C8A6B970FC696EFAFDEA765A5EE84D6C2F92E9A,
	PlayerPrefs_DeleteKey_mEA684FE4DADA7F05D3938512644842889EB69175,
	PlayerPrefs_HasKey_mDB54F9599829D4B307FEDC34275A4C01ACAE7B03,
	PlayerPrefs_Save_m5E9E78CA1C53E90DC6CA592F66DD043394CA6E65,
	Cloud_A_m15C3DC69C6BB05B0B6B4D6C94A5972D75457160D,
	Cloud_A_mE4C7ABF027A5F997756BA5CB4AD2A27364D4CC97,
	Cloud_a_mAD988736B81CE3DB98EAAF53AFE1D2BFF48D6707,
	Cloud_Init_mEA14E763F7BF8CE496A2CAA405A8873AF655B9E4,
	Cloud_CallFunction_mBAD01AD8443563D6D48F8B5B8A63B53E23C8B200,
	Cloud_CloudID_mDA96C9642E8D509C84F8A9CBD053838D625FEAEB,
	Cloud__ctor_mE7AEF87A4D4265D6879B364F02180F9AA1743DD5,
	WXAdBaseStyle__ctor_m2E315FD29901FB14612A426ED3161CA5B8BCC7D1,
	WXAdBaseStyle_get_left_m3A0415B9A751CDA668346C65786AE34FF83DB1A3,
	WXAdBaseStyle_set_left_m17E0D81D2AE0B3964A2719BC7B5B35220286A36B,
	WXAdBaseStyle_get_top_m46A709DC852BFAA08A930182AE375582979AD645,
	WXAdBaseStyle_set_top_m088BF2B9990B1DF22BCF82EF85D34708F6F6DC68,
	WXAdBaseStyle_get_width_m7763E75B0F6AEAA43CC5CA0F77F13AEA727640F9,
	WXAdBaseStyle_set_width_mB10CA97D4E5451D3DDE1FE70DF8658593D47DAA1,
	WXAdBaseStyle_get_height_mC533A17CE0262C1539F57C283B8699C2AF0CD43C,
	WXAdBaseStyle_set_height_m3C1FFBB10CDDE46B8F80A81E420896C188682310,
	WXAdBaseStyle_get_realHeight_mC6A4D7FA1058B6F5FAC9D3A994DBE319F8F20A33,
	WXAdBaseStyle_get_realWidth_m9C8FCBECDEFCF0D4199999AA8951AD9618355DA7,
	WXAdCustomStyle__ctor_m356128F9CF764392EC032C7637049E9363E2F2E2,
	WXAdCustomStyle_get_left_m5B478BE3F137A7172C2DBC78EAFDBF480F343EC1,
	WXAdCustomStyle_set_left_m120900208F4C25304170AFB2B3C15768CEB39A3E,
	WXAdCustomStyle_get_top_mB439372EA50882CDABD5230250F49C23FDF31724,
	WXAdCustomStyle_set_top_m77C4C1F0102BD83D1A8D9766154909A68816A9ED,
	WXAssetBundleRequest_get_url_m4E62A30BDFA2B3F4BAEE9BC349BA1837AAA306F9,
	WXAssetBundleRequest_Dispose_mB4B23267F91B9DAED647A0FAC9E177061C3EF4A6,
	WXAssetBundleRequest_get_assetBundle_mADF08C636E0460A882567267E8E42E940FC11069,
	WXAssetBundleRequest__ctor_m3B42CB55647F948A3BF8DA23690AD87CC104BB31,
	WXAssetBundleRequest_Callback_mBDBC695795945916CE988FA73D548FBFD70B9CCB,
	WXAssetBundleRequest_A_mA24EAF7B3D9C88B75FA829092E0D513223E8DF8C,
	WXAssetBundleRequest_get_Current_mD9E608C41CA90039DE2B97F90ECD14D1B21FE34A,
	WXAssetBundleRequest_MoveNext_m7DFC38CFFDECD8AD6D7F113918434637B1B0FD41,
	WXAssetBundleRequest_Reset_m49A3E7EE68558BEC80F12FC8469F7CFE84C567D3,
	WXAssetBundleRequest__cctor_mB5C3FB517A545F506CB1B86E14153BEA69B25160,
	WXAssetBundleCallBack__ctor_m27421744C2E47B438185B4C8EA73D554FA4692AF,
	WXAssetBundleCallBack_Invoke_mD5A374766A36A59D5AB27EE0185194E1211ECC88,
	WXAssetBundleCallBack_BeginInvoke_m2EB36FDACC854B2742272609225DDA9CAC2B6AA6,
	WXAssetBundleCallBack_EndInvoke_m7AE166BB184BDA3F87ECE3713E35B1228340382D,
	WXAssetBundle__ctor_m4C32474A1117F69306F9BA2C70F4C7372494908B,
	WXAssetBundle_get_name_mD43C1CD9CDA01C71B2B110C3250192C6989AF9F7,
	WXAssetBundle_GetInstanceID_mB02B0E502911E51451A4C9C4983AD38781C05ED5,
	WXAssetBundle_GetHashCode_mEA5A33D95A8BF7F821D87F8FB25EF5E3B90F823E,
	WXAssetBundle_ToString_mD438A2D06A9950C09D534516D6ACB9D7F5232DA2,
	WXAssetBundle_A_m4D3050923B6F9E263E3CFEE8C0075799D311F1CF,
	WXAssetBundle_A_mCB6917BB48B1ACCD583644B97CE5A4C036CA00B5,
	WXAssetBundle_UnloadbyPath_mA8BF25943B32E7E11CE57BFF13FFC5FACC607905,
	WXAssetBundle_UnCleanbyPath_mFEB78394B5A840A3275AE09266AFC76E1B06FC13,
	WXAssetBundle_CheckWXFSReady_m84B9C318CDF0C2C36E5196DD7CB6D26F4D28031D,
	WXAssetBundle_get_isStreamedSceneAssetBundle_mDAA16E8C842AEC7A858967EB3E924AA446D72217,
	WXAssetBundle_LoadFromFileAsync_mB95FB0D4248FC3E9099FB2555BDAC35601519590,
	WXAssetBundle_LoadFromFileAsync_m09DD7B4FD66E5B76BCFF59E4C51388F3D55318B1,
	WXAssetBundle_LoadFromFileAsync_mDEEBC0F87D86FD9E5B6FEB3ED820D2408038FF93,
	WXAssetBundle_LoadFromFile_m82E25FDE1ECFBB8E110307C474556406BFE1EC9B,
	WXAssetBundle_LoadFromFile_m3BB9915F9BCA23F55EED463F6D1C6C6EDFBC2E48,
	WXAssetBundle_LoadFromFile_mB7EB45F2F35A06DD46316A08DCC1DD61B87263AF,
	WXAssetBundle_LoadAssetAsync_m63819FE0F06BAA3F949BE93CB883BF6393FF7215,
	NULL,
	WXAssetBundle_LoadAssetAsync_m7EA02C544C6F69DC1FC77554B720EE26BE051E1A,
	WXAssetBundle_LoadAsset_mBF5CCED5B4E9CCABC3D0FD6A533FA95E7C6C69D7,
	NULL,
	WXAssetBundle_LoadAsset_mD816C47ABEE468D17BA15C39C9E5F91BCC48B746,
	NULL,
	WXAssetBundle_LoadAllAssets_mFAE50BA36101BC5F8227109753D92BC718B54064,
	NULL,
	WXAssetBundle_LoadAllAssets_mA82F86D685321E6DA9EBA80CC43F351C6471C7BA,
	WXAssetBundle_LoadAllAssetsAsync_m6B827DF80E4BAADEA2462D15081B9DECDC930546,
	NULL,
	WXAssetBundle_LoadAllAssetsAsync_m04A8819A9F19C3BFBD1BFC1EF7C7BF0E9E70C33D,
	WXAssetBundle_AllAssetNames_mE8E0834C89C63B6440F45949084D50FD4C9FC3A2,
	WXAssetBundle_GetAllAssetNames_mD846A328EC234B265AC14B67FB17CA1DD0565AB8,
	WXAssetBundle_UnloadAllAssetBundles_mE892F374E9F6AD063684B42233096DAD9AB57966,
	WXAssetBundle_Unload_m731E0E297A7EC36E554CB3761AB2BB725B3100EE,
	WXAssetBundle_op_Explicit_m01AFABD53FF72BA584146C1E521A60836D4E8A99,
	WXAssetBundle_GetAssetBundle_m1D5BEBBCC8FC2D0CAF9E1D006D10FF92A7B9935D,
	WXAssetBundle_GetAssetBundle_m5D4A6BF61B6CD7308AFEDE49CF2F8F9AC1F56E12,
	WXAssetBundle__cctor_m2DD6AD773A56BF1534985D2219FC2888DD8933BF,
	DownloadHandlerWXAssetBundle__ctor_m5E66ED397902696AE691E6461E80C7AFA4BA69F5,
	DownloadHandlerWXAssetBundle_get_assetBundle_mCCE7DB97A8B0446F1DA7A22A4A7E07AE3AFC02DE,
	DownloadHandlerWXAssetBundle_GetData_m7747202EB8E11E678AA4EC1EDBDF973FDC13FA0F,
	DownloadHandlerWXAssetBundle_ReceiveData_mA50ACAFF74F39600B0E721A41D0BD35A5EDDE89D,
	DownloadHandlerWXAssetBundle_CompleteContent_mC2F43B6054A619D5972DA550FA2E8BC89339B67F,
	AssetBundleExtensions_WXUnload_m9B92DA1C58848A9C896F1DC4640E96DFCA28AC80,
	WXBannerAd__ctor_m26192C8F799505C1FE297698A6C5D8A6F5095F2C,
	WXBannerAd_OnResizeCallback_m0282E67C9CE203C2B3E5B513F03157567B9BC425,
	WXBannerAd_OnResize_mC3E9C0926D723BD2A329E88CCC6B2D16CC1FF4AE,
	WXBannerAd_OffResize_mD7D8015CC17CE9D045C5DEB5BD21A745E973D5E2,
	WXBannerAd_Hide_m86DB266D122347EA56C55AB64E864F2D2302EC2F,
	WXBaseAd__ctor_mA7DA04EBEA505832C6B4B9014A81F3308B3F18DD,
	WXBaseAd_Show_m54A0DCD94CE9F5388D21DAA2E698F8BFD545AA10,
	WXBaseAd_Show_m5475FE3B14B8CFA5F3E6A84EB4E2F77BFAFFEDEE,
	WXBaseAd_OnError_m80C97C76469809C444DBCA3F0B5E85B8AC59AC0A,
	WXBaseAd_OnLoad_mDA34BEFB24F0277D90FB941DBCCAB3D9B839A992,
	WXBaseAd_OffError_mEF73FA4DC1D66D3B8663D6BEDE6829C3DF3CD18A,
	WXBaseAd_OffLoad_m005EB0306B4A5CC18D400EC3FBE57277CDD0843A,
	WXBaseAd_Destroy_mB760C762BA8047A0020031C7C1B7FADD2E8943D7,
	WXBaseAd__cctor_m5FDAD0BC043F67D607904A55B804E7C9EADB38B8,
	B_A_m4E48AC3983FA372CE7D69C3EA6159255CA7A84B7,
	B_a_mD8F715546AF0EDDCABD940D8D83D0612AE92C1B6,
	NULL,
	NULL,
	B_a_m78B1DF252D6681DB0C644F349D73AC2F8292AA79,
	B_B_mC080A903D8C65107D7F549C4BD44DB5AA4D81C9F,
	B__ctor_m90270E9130B5DE1DC0800DF973E914791E9A31F7,
	B__cctor_m7D14A65DC7DCFBC95987B568E48DF94DBD09C8D2,
	A__ctor_mEE08D8FA98577D81412E2B5C71A9634BC649B4B2,
	A_A_m1AF45509D6E215553BA3874E43C0E13FC51FEB9E,
	A_MoveNext_m11AEDB6E5E5E34D6973686CC6969F75CE4C46128,
	A_a_m2EC799866EFA7D3BF8BCC61ADB49017C2911A31B,
	A_B_m566C3B09D01C265876E46A3ED449EE65B6A979B7,
	A_b_m7FFBE02A63389544712CCAB91EA27F2E5C82CFB2,
	WXCamera__ctor_mDCCB7F5132E8CE92208AB4C319473070002177C4,
	WXCamera_A_m1EC08EA924045A3C28B08EC3D7D2A2C5D19897D4,
	WXCamera_CloseFrameChange_m50B0403D2679AEE500A0908665A38B1A619A8C4D,
	WXCamera_a_m60467455B02E94B55E26CCC50E414DDEA5EED941,
	WXCamera_Destroy_m927B8C44BE95F61BFE74654DD0F31B0EA682283E,
	WXCamera_B_m3447119896005D4492824EC7613FE556FA10E1D5,
	WXCamera_ListenFrameChange_m5557F7FD3EC66FC2FF38A6F77DF6AB14F3DC6113,
	WXCamera_b_m8B7EEB2A97D83DAFAA4F58F22D86A4C8756F9338,
	WXCamera_OnAuthCancel_m43D12CECE05BADB25FC11676B8B85B16CE036032,
	WXCamera_C_mAA02704063649E7F8F97530D23EB5E4C7CB05FCF,
	WXCamera_OnCameraFrame_m1DC6072627D3AADFEAA5121E9804FDF93BC5BE71,
	WXCamera_c_m8C280BFFC0E4BD2AB095A00257F575DC070AAECA,
	WXCamera_OnStop_m717B27357B02D6A12AD9B62E5EF785507780DDEC,
	WXCamera__cctor_m202CFEC690005B3D222A58EEEBCBDC088A2EF876,
	OnCameraFrameCallbackResult__ctor_m531F9A3635EB4F26B097B49F3A58B2FCE4DD1FD8,
	CreateCameraOption__ctor_mEA1A1C5053981B1025C0136381F8DDC36CE8F494,
	WXCanvas_A_mFB624D25BFDC45981BA7289C77C3EA42C1BCEF07,
	WXCanvas_A_m9A04F26A4FBE4955F7318B15A3C160EE1F21602B,
	WXCanvas_ToTempFilePathSync_mAD404D144E9CD5B4037FB5A3D8B3422A43C7A0E4,
	WXCanvas_ToTempFilePath_m1C4ADF2874CE867DE973C157F45709F1A14B6FE3,
	WXCanvas__ctor_mE4CA0446DE34F81E7053076107F1D80C2E4BF6A1,
	WXChat__ctor_mB593785D368580C90A767BFAF2FAD63D9C97458E,
	WXChat_A_m541D7A61A09CFDE50D7A4FCC4838B454570EA859,
	WXChat_On_mCDE8DA1B2A61E6A1EE32FD797462C5C8430E24D1,
	WXChat_a_mFB785E2B8BC643BC1FC037E27D19F503F66622D6,
	WXChat_Off_mBF3B58BB717E89BAC7B8FB3D0568805DACFCC305,
	WXChat_A_m21A96FCEBCE2531C18E93767F2445F1B5F5C4B6D,
	WXChat_Hide_mD8DE7632BB06796FBE5BC7C90B6E617344FFBFB8,
	WXChat_B_m9E4A8412A423EEE342D0502145CCE42D2B64B400,
	WXChat_Show_mE103FCC09D799C3D12E84805430FD56421B6FB20,
	WXChat_a_mF3F78B2089665FE9D725B1BA189F83145F13BA7E,
	WXChat_Close_m05C3B3FF04414FED3F076A13409D17ED56C07BF7,
	WXChat_b_m445ACF77948DF4E8589D6BB39F2226734F2A9FA8,
	WXChat_Open_mCB2FC0EBA00D29130F27C4681D8873883315D290,
	WXChat_C_m4708D44630DB29E7E22B95D887FF1A708439FD3A,
	WXChat_SetTabs_mA9F4E6F676352F2D44499DDDE4BE7D8225BF1B1D,
	WXChat_c_m67649AF18889B9651B1CE2C2B29862AC8779DE0F,
	WXChat_SetChatSignature_m9ED11FE1D262C607C895C2196E305D81DE47E5DF,
	WXChat__cctor_m68B8DF5DA672671950C5C66B5B104BA4E43AF1ED,
	WXChatOptions__ctor_mFD514551ADDCCFA66A20FB63D299E663440AEB19,
	WXChatOnCallback__ctor_m58E0233E08E9F11C6259044B8B578CC85A52CE56,
	WXChatOnCallbackRes__ctor_m2DA037F42ECB27DDF2D0C0757808DB4FB9771FAA,
	WXCustomAd__ctor_mEF651094144B2069883ABD834EB5081A5277641C,
	WXCustomAd_OnCloseCallback_mE08F86A9C80ECB053F110E16C4D155A73DD16A76,
	WXCustomAd_OnHideCallback_mD70D24D0FB1D359401DE19DD89C0A54F4AC24CB3,
	WXCustomAd_OnClose_mE1F055A8C3D066FBB81A1E9E07BBBFB9B7464B65,
	WXCustomAd_OnHide_mA54BF4C5AE29D84CC31BF25F0C3F96C7E7077685,
	WXCustomAd_OffClose_m80EBCCBA47614FBBFC9EA02C3EF0A341C37B7726,
	WXCustomAd_OffHide_m8DDC52C3EC926435B91567A7AB4499E5D841283E,
	WXCustomAd_Hide_m33FB4A77D72D5697651B58C399E57DB82AE72947,
	WXDownloadTask__ctor_mC9E93342B400BD6F243A44E32ECF3A9A9132A07D,
	WXDownloadTask_A_mC0240A67512BFE623B3DABA2B3D8FE72495DC92F,
	WXDownloadTask_Abort_mFCC67001644E11A792B698A9142CF00056DA6F7C,
	WXDownloadTask_a_m6395D092D973C731EAE7DF4C7BA22DD0A9F6AC9A,
	WXDownloadTask_OffHeadersReceived_mC99131BFD4D005AFB4247D40649A55A383845F0E,
	WXDownloadTask_B_m8777CF4698E44F919608E83BAD27FE2FA6FABD38,
	WXDownloadTask_OffProgressUpdate_mDE6A3C6F0BC73B96C50731DF7787CB42ECA3AA98,
	WXDownloadTask_b_m38F6B847779C75F383FE693E35BCE8AA00261AE0,
	WXDownloadTask_OnHeadersReceived_m21103E990EFD1D6ACA4A4449163BF1A142825640,
	WXDownloadTask_C_m61B6B8B972F73F24902D6AB1632D15266B9A4931,
	WXDownloadTask_OnProgressUpdate_mA73674328205CED0D3C0BF3800F3EC37E495EF93,
	WXEnv_A_m6C619AF6E1179D2C0870A0BBEE948CFE0B07CE98,
	WXEnv_get_USER_DATA_PATH_m18B5E1D35E0F96A2EC3556DAB5F522038FD08443,
	WXEnv__ctor_m3B0DF58986C02376ED27D7E0C49D649582EF0BB8,
	WXFeedbackButton_A_m470E791DEBD2EBFF89933D72B101661B8BFF270F,
	WXFeedbackButton__ctor_m6F76DDC96017B2F976FC99EEA1849B657D10E092,
	WXFeedbackButton_get_type_m2C8EE6E785C1D63687FE7E07AE37C58EFF25DB52,
	WXFeedbackButton_set_type_m688D37933F0FE53598A8343CC46FD2DF1F7CC9E4,
	WXFeedbackButton_get_image_mE8EC6D907A35221CAD032EC532BEE302A89BE083,
	WXFeedbackButton_set_image_m5D809B752618507195E2CB3A1CB48BB8198B8F96,
	WXFeedbackButton_get_text_m9B6726EFF17A0B98E3CC0F7F3BEB56C6ACB7D910,
	WXFeedbackButton_set_text_m2894CE5D5374140EF9F7F4146EAF0F125C206349,
	WXFeedbackButton_A_mD47A44EE8986DB7513B3D92ED88C2D89A560AF7C,
	WXFeedbackButton_Destroy_m85F9447436E5B7D3756DD8EDC1E2D6671755F344,
	WXFeedbackButton_a_mB997CE6759025C001CCFD0BC6BA6AAEE800E3791,
	WXFeedbackButton_Hide_m7B65A458F08962EBFF1AF4B64A0ECC02632BED4E,
	WXFeedbackButton_B_m7F1A5585FDA65975CADA990FCE65308790A302D8,
	WXFeedbackButton_OffTap_mED501D90E97F29756DEF1E91BC223F701E9E63E5,
	WXFeedbackButton_b_mA7C62EE79AB602762A759A52F7BC3A7D053B9AD4,
	WXFeedbackButton_OnTap_mCFA129959DAF12BC5FE311D605227808ED4B6ADA,
	WXFeedbackButton_C_mF39DB0D6E6A0A6586F8F7A32FB85DAEBC88F211F,
	WXFeedbackButton_Show_m07E29180A22A14F981F744B491F2E92F0D53E713,
	CleanFileCacheParams__ctor_m51F3466967FECDC0CFE4AC4DD93F37A6D29ECB40,
	FileCacheCommonParams__ctor_mC0D549BA966F1EFF3D1AB3DADD43CA25886AB866,
	WXFileCacheCleanTask_A_mCF8E704667CBEF2B4879E5B8E85E7B9901FD5D16,
	WXFileCacheCleanTask_A_mEB188F1CFEE3AA616277DACA1CBC842402FC7794,
	WXFileCacheCleanTask_A_m2B8BD0F84313DD1E26DB16360BDE9BED4A580C02,
	WXFileCacheCleanTask__ctor_mB5E5C26A45DE7879B61B0A87E7749B405EBFB8DF,
	WXFileCacheCleanTask__ctor_m095F207622C856CAFD5C220AFCB8604198A81EFB,
	WXFileCacheCleanTask__ctor_mE5EF300B9BAA6B90C26B70745BCE7348025D5417,
	WXFileCacheCleanTask__cctor_m291ED3D8F0FF3D2B2B32869DF6D4F159548AB866,
	WXFileSystemManager_A_m9591CF0640BB23FA5B7CD34A3909239884DFF38C,
	WXFileSystemManager_A_mD4C9355A46B27497E015C1F409DA8A10891031DB,
	WXFileSystemManager_A_m999770071533D1833D444FFD49DA141EACF82E04,
	WXFileSystemManager_A_m02F8E2FC9A1614283C8CD2ECB4F6525B9046BB6C,
	WXFileSystemManager_a_m4C6AFFB9E3820DF7517C3902662B8E786ADE29D6,
	WXFileSystemManager_a_m2492927C33C42D8CE4C239DC2CADCBD7ACDFD029,
	WXFileSystemManager_A_m669638DDB81E9216115E2B11B473A08D27218EB0,
	WXFileSystemManager_A_m238899490D3A4191351DA49AB88A48B15F51D6D1,
	WXFileSystemManager_a_mB1E5F7FEAA465CC34FF8A80F584AD06C6978E993,
	WXFileSystemManager_A_mF5AB7CABF9465110093D000295804D5EBA68A7B2,
	WXFileSystemManager_a_m24601D51E6D57106CA565DCB7F5009291DC11565,
	WXFileSystemManager_A_m52AF92D52BACBA92DA08005E3EEFE6E9F17AB620,
	WXFileSystemManager_B_m795D6B31E9447445F125751E17CD9F79964CBB1D,
	WXFileSystemManager_a_m46238F4249F24C604AAE04D01006EF269592661F,
	WXFileSystemManager_A_mD895BD5A0F8A233B04F64A24234E863D01551536,
	WXFileSystemManager_A_m576A5C367EC479E942748C0265F3688EEB8E3398,
	WXFileSystemManager_a_m91A176FC803EE96A090FFA50975D54609637EDA3,
	WXFileSystemManager_a_m5A44CD08AE030B9B7F2AB6C22B8CC1C5DC2F80E0,
	WXFileSystemManager_WriteFileSync_m1ED2CE83AA1A85249B185CD274A83D821A33A036,
	WXFileSystemManager_WriteFileSync_mB5C1BE9E45E9400BE306A2104A289DD6CEFA93EF,
	WXFileSystemManager_WriteFile_m0199A22E0EF1F48F95DC7921F9C24791A630CCE3,
	WXFileSystemManager_WriteFile_m6E42D71AB0FB03328B33E7F68380CB3FBFEF85B4,
	WXFileSystemManager_AppendFile_mE1E48F55CB23FA84F8EA61B222FF0750F518AF25,
	WXFileSystemManager_AppendFile_mF93528F4D4BE9AD4A8304B61B46ED1EE4C07023E,
	WXFileSystemManager_ReadFile_m23F1697D4060A8156D7150FC46FBE7BC832B9CFF,
	WXFileSystemManager_ReadFileSync_mB3DDEB867A560DFCE019564B38CA186AFB58F79A,
	WXFileSystemManager_ReadFileSync_mD4FCD980CF6893FF76882907FD9303376F33FBB8,
	WXFileSystemManager_AccessSync_mCB50A2CE49CADBF428A6202906B364B08A1AC5A3,
	WXFileSystemManager_Access_mB800F2F9AD2CEDAD4CD121649DBD4A8B18EB19BE,
	WXFileSystemManager_CopyFileSync_m160587D2FCDCDDE3B7AE3D65652EEDF61E3C578C,
	WXFileSystemManager_CopyFile_mED468262DE57DC709778958E733720290F899232,
	WXFileSystemManager_UnlinkSync_m59BAD70900F4DBFA96424E739C505478A58BB710,
	WXFileSystemManager_Unlink_m39F93FA799985CD09FF65860887ED95887AF0FDA,
	WXFileSystemManager_Mkdir_mE306EC05F3BFF3E3DC7784B20947505C525E7F8E,
	WXFileSystemManager_MkdirSync_mB96CE1EC8EC68AC3AE477AF6626E4CA97133D5FE,
	WXFileSystemManager_Rmdir_m8C4F013E104E681A9A19B6BFD84BAAF16C34B816,
	WXFileSystemManager_RmdirSync_m29FED2EF31B0C7DCF6507924D6D4D7FF75592DE4,
	WXFileSystemManager_B_m58C37951CCCD48EB4005CB8A1CD35833F52B553B,
	WXFileSystemManager_Stat_m356909F9D17DF6C5F41B2BF67C34BF74E40CDB9F,
	WXFileSystemManager_HandleStatCallback_m1A40863EF3B7E61480496868FC8771A7D10A92A7,
	WXFileSystemManager_HandleMethodCallback_m2D4A220BD67AA94EA647B3FF9779D447E0F71481,
	NULL,
	NULL,
	NULL,
	NULL,
	WXFileSystemManager_a_mE16A47B8A91018AD48798ADE1710B23D1BB2F70E,
	WXFileSystemManager_a_mE80347C5299EDD1B802079681909F91B1E6E57E3,
	WXFileSystemManager_AppendFileSync_mF9BE7B4C1F1670772D7F7B709FE4F440047CF3DA,
	WXFileSystemManager_AppendFileSync_m8C0AEABFB5487723081ED342662FB8766440CD1C,
	WXFileSystemManager_b_mA3D8A33F264808E719AFEE7B24779D12FB45435D,
	WXFileSystemManager_ReaddirSync_mDB746288392660B6556E95411965B124B65A4493,
	WXFileSystemManager_b_m407C8A7450A817C359B476852DA5BC9BA78324D2,
	WXFileSystemManager_ReadCompressedFileSync_m8E96317F6C923AA5F8208686DDBD1A27F66DEAFD,
	WXFileSystemManager_C_mAE790E31A6FDC2815EBCB307BD3AE9741DCF9DD0,
	WXFileSystemManager_Close_m4515A7DE1525A0B83B99BBE36844FC5072F3610B,
	WXFileSystemManager_c_mAABBB8B6B18085BCD7C6B126911400EAEBD4F2B2,
	WXFileSystemManager_Fstat_mB3823854328D85C55925DB4674430ECD93DC72E1,
	WXFileSystemManager_D_m3798A6F865295520A168299F1076903E1D1BC6A0,
	WXFileSystemManager_Ftruncate_m4742419E5902E2E198CF72E17365BCC491018583,
	WXFileSystemManager_d_mB3BB64747CDA2E7879C2048C20DD9DEB68F8D1DC,
	WXFileSystemManager_GetFileInfo_m24DDD955DD40999C58F2985F9A1A0C9C08B4F640,
	WXFileSystemManager_E_mD3D67F8236D060F8E839AF85E9AE699149C61837,
	WXFileSystemManager_GetSavedFileList_m3508124AC4BE31D205016F5FA37225A5129AD67F,
	WXFileSystemManager_e_mB9BCC070B5C3E4C48871E4750FA0EA6F839B6EAF,
	WXFileSystemManager_Open_m39341599AABCD154625D7D2C7EF706AA2487EFE3,
	WXFileSystemManager_B_m3C748E2196D4BC3B3BC1A8C2E0D9F92081527E73,
	WXFileSystemManager_Read_mA7AADAA6C97D86F712CA5F65E1E78CD9E1C57076,
	WXFileSystemManager_F_m122F335BC1BAD184F6006AFC668C85AB77D52668,
	WXFileSystemManager_f_m1D56E54B4E8B67B8824B8C62E6F71DA915BBB041,
	WXFileSystemManager_ReadZipEntry_m3547EEE019FA6576902E9A6B38EE1241B6DE54F1,
	WXFileSystemManager_ReadZipEntry_mE5CBBC94E5E830911C7F1594492065EA45B4B547,
	WXFileSystemManager_G_mE7FE0F9DF21D4F8EC3BC03C3C00E751B5C768BDF,
	WXFileSystemManager_Readdir_mA1DA3316E612C04B76CC5746C1C99FEE7EF394A6,
	WXFileSystemManager_g_mD92353E047EFDAD0EB4D7F4B56ECAB082AB49400,
	WXFileSystemManager_RemoveSavedFile_m59CA1073BA1E5E3A09027098F3823E294BFED4DA,
	WXFileSystemManager_H_mE3A526D46A7849D798F953D2309EFBBDA4746A6A,
	WXFileSystemManager_Rename_m6F6BF8E3A85C87D917C7C7F148936C49AF99A017,
	WXFileSystemManager_h_m0C7C93779C150F5BEC80F70CDA32A7F378B9B8AA,
	WXFileSystemManager_RenameSync_mF6915E3047B1E0FFE93793E0D559B998A2C23BFB,
	WXFileSystemManager_I_mD540CD62417582A572ACD9F81989C965C4C114E2,
	WXFileSystemManager_SaveFile_m1F65C1779F75684793D258EC2080534D923A2E03,
	WXFileSystemManager_i_m6B894F73354118C79E6BF77357353040F4EEE1C4,
	WXFileSystemManager_Truncate_m0094F4E62367C91F9375F901D8ABBE4C3CB45B49,
	WXFileSystemManager_J_m22BE7DFA63B1619CF2CF09C64C73F2F45A8904A2,
	WXFileSystemManager_Unzip_mD92281762629E86C75D8CC3515723FE5E4301B40,
	WXFileSystemManager_b_mD6F441A2C4FFE3A6F3402139F512EBF948410DE4,
	WXFileSystemManager_j_m43511A5EA6D2A95E7FED1A79D3A80702BCC0AC2B,
	WXFileSystemManager_Write_mEA4CC1C1FE1593571716D07F0C302DAAC7EBC67C,
	WXFileSystemManager_Write_m780D682F9AC588F9217D7298402E23B0BCAB4BE4,
	WXFileSystemManager_K_mB5688D3679B109B05E8C89DE1F17192E9DE2C0EF,
	WXFileSystemManager_ReadSync_mD0F927949871100CBDC6C43B53DCFA9E5D438CCE,
	WXFileSystemManager_C_mE3909D077B1FF3F31BC6B14B62F556DBB660BF48,
	WXFileSystemManager_FstatSync_m159078767B657D9677285A6D36AADDD808DC7B86,
	WXFileSystemManager_B_mEED45C2CDE33F11FF8D41B4D10B57B2FC166C6C5,
	WXFileSystemManager_StatSync_mEC9B6F13AF070FEE3EAEEB6E0A613E2ED06EC1B2,
	WXFileSystemManager_A_m54C632EA68E82BBA8165E595008A5FE3F16B5E08,
	WXFileSystemManager_c_mF9819E9B7EFDC37CCC0190E52A10DC70413E43D2,
	WXFileSystemManager_WriteSync_m0C4CB9A33EE790C46CBDFA29548CF179479FDCBE,
	WXFileSystemManager_WriteSync_m70710D645CF53DD161778544925119CB11DFB3D8,
	WXFileSystemManager_D_m4EF0BB8E5CFE37059E0FA41D21C70AC0FC0AD288,
	WXFileSystemManager_OpenSync_m62490F9238C6FCEE27FACE5D746FBC54F5B503B9,
	WXFileSystemManager_k_mD4795145F7058A5A9A5A125624619F0790E2B094,
	WXFileSystemManager_SaveFileSync_m3DDBA037D321CB6E5687EE117C60B491088BBC6D,
	WXFileSystemManager_d_mB7E15196A216EE5D5E9E41E8F1CB7A2C957B7AEC,
	WXFileSystemManager_CloseSync_m0EE64EE5C7A6DF234DE32DB48FDCBF9D2C7D1EFD,
	WXFileSystemManager_E_mEA94915A7EF1C3DA345081D9D08F1FCE4467C03A,
	WXFileSystemManager_FtruncateSync_m4768ECC26A015732F432A6F2BEDE345A77955EE4,
	WXFileSystemManager_e_m788381E49EF9C18FD0B416D2B3D26ADAD55A3FA8,
	WXFileSystemManager_TruncateSync_m1AAA461C6C3D6171BA8BC2A16D8EE8022EE0E789,
	WXFileSystemManager__ctor_m94A82C30476311581C5A4BA7F257797E00D632D6,
	WXFileSystemManager__cctor_m1DEB0EB91E402CE2B02048EBEB3E1018B72CA86D,
	WXFont_A_m1F5D52B763EF78F2C67ADF9CFDA60FDA186836BD,
	WXFont_A_mAF36F5DD5B8C1574F93CF6EA1CCD7C7C04291520,
	WXFont_GetFontData_m56CCBE553C89A5D733A6A3060B490A0D8E69C5B5,
	WXFont_HandleGetFontRawDataCallback_m4D9B3E45E91C79CBA97B0B17DA901E8C5B4D6FEC,
	WXFont__ctor_m82B8DBD396F0ABBC2372480E5F1C86E67EB778E8,
	WXFont__cctor_mA0D5069F657F02BD9B02CD6AC1246D2746909CF4,
	WXGameClubButton_A_m5A57A97D393C8ED275915FA99D85F73D74C7EB61,
	WXGameClubButton_a_mC1E810136460FF2B607158056EA3A6947A9EE7F1,
	WXGameClubButton_A_m48039D2AEBE467735E97B403585FC89B9EFB84AE,
	WXGameClubButton_B_mF24CBC7C524E674364CB327DDB960CB82EF106A3,
	WXGameClubButton_A_mFFC6CD9E346436132F7FA3E6FD098CD6158162C4,
	WXGameClubButton_a_m3FEE242352533A22D6984A2A614E25E762F08F3E,
	WXGameClubButton__ctor_mD659E9CA2652F112E1D36F148E315E9B2BA76BCC,
	WXGameClubButton_get_icon_mF6C3FBECD087174C58467D4C734B02D877910AFA,
	WXGameClubButton_set_icon_m729220A2C4D1F1C0A10595BD7EAEAC9EB962308F,
	WXGameClubButton_get_type_m566EC00E3DA1EF6C99D7C247EE1203F117458B3E,
	WXGameClubButton_set_type_m5870F9FF019DAC720B407C501D6C78382C12FFD2,
	WXGameClubButton_get_text_m746C64B9989FE55DF6FD51F0D29F90AB587E20DD,
	WXGameClubButton_set_text_m7CA027BC607F93C364FDD1E5FE8D22E09B198E0D,
	WXGameClubButton_get_image_m382C406AEFE343D788BD1FDE9EF33F406DAC81DE,
	WXGameClubButton_set_image_mD28FFDA8AF853D5A02A02A20C82A5360D7A0983D,
	WXGameClubButton_get_styleObj_m6AC492C9265C06A84AE5B1CD4CCAD3260B15ACA5,
	WXGameClubButton_set_styleObj_m917DA390978633A116AE39DDD874B2BD70ABBB9C,
	WXGameClubButton_Destroy_m8F0C1A8319082917A0D34B18FA633EEA936619FD,
	WXGameClubButton_Hide_m40BC14A215814F65460CB0F890D5449247EC49FA,
	WXGameClubButton_Show_mA7823782B538124FFD4FDB105FC2E50B64C50457,
	WXGameClubButton_OnTap_m2460D0FB36AEA25289D2E1CBE23FCA3015B497FB,
	WXGameClubButton_OffTap_mC6E5001DF80D1C8D9DA42372A9D50A662ADCDCB5,
	WXGameClubButton__HandleCallBack_mB3EFA6D1F86EFB44E90A291141655A7CA9133E00,
	WXGameClubButton__cctor_mB5D7431C1D63514AA35DA3E35544469DB090C1DE,
	WXGameClubButtonStyle__ctor_m4790FB4735C6CE2404B87B654CF8857F809FFC54,
	WXGameClubButtonStyle_get_styleObj_m704E0779BF681C476C6BB42C043290DE65533EEF,
	WXGameClubButtonStyle_get_left_m827925CF7788EC76D6FF9C39549DF6BCA55D9D69,
	WXGameClubButtonStyle_set_left_m69ED0DE3800078467FBD72884DC68B7827112677,
	WXGameClubButtonStyle_get_top_m368D52FCC4770BFC2C8B75AAC6F79EB7DA672559,
	WXGameClubButtonStyle_set_top_m236D035C2D906CA5E1285059A0EFEC1B09D8F126,
	WXGameClubButtonStyle_get_width_mCFCFBF1AD944B3F9C61B46B5B80394DAB3C96422,
	WXGameClubButtonStyle_set_width_m4FFEAA42E8070DA1B00E0869667B5255A08C36C9,
	WXGameClubButtonStyle_get_height_m35790BDFE1496718E0DAA809FE8A3072959818F1,
	WXGameClubButtonStyle_set_height_m259A4C2846383163F5BB7A6F57E682190EBAEEBF,
	WXGameClubButtonStyle_get_borderWidth_m39417608DCE1E49B11115ADDB05ED2E6DB03BF45,
	WXGameClubButtonStyle_set_borderWidth_m866CE0EF215EDAC5964B5B83138564A4E1A04C6C,
	WXGameClubButtonStyle_get_borderRadius_mF5628DFE626D2DEF6ADDD5BF58EC583AEC0CA23C,
	WXGameClubButtonStyle_set_borderRadius_m2FA5F3FAEE972404D5E8F207572D58A7A9FA7E83,
	WXGameClubButtonStyle_get_fontSize_mAC2E3E872CB0234EE2B8F939808929820090D6E5,
	WXGameClubButtonStyle_set_fontSize_m8DB9902778BDC30B9316B29B166EC3D49A04045E,
	WXGameClubButtonStyle_get_lineHeight_mD8A3A6E687F9ADDF9D3F4B457616A0C73E57D514,
	WXGameClubButtonStyle_set_lineHeight_mB3CB0DE75261FE8330F181AE7E22DCED58348C97,
	WXGameClubButtonStyle_get_backgroundColor_m1469F35AE99CB82879D375A8DCB11C6729B9FC42,
	WXGameClubButtonStyle_set_backgroundColor_mB74E48D6DEE050BD15068A8F67160F9567F6AB64,
	WXGameClubButtonStyle_get_borderColor_mE55C97A556A9324F7250DFE9D61898C0ACD26987,
	WXGameClubButtonStyle_set_borderColor_mF5484BEAE1170E57EF5E0B92E703DED116EB5965,
	WXGameClubButtonStyle_get_color_m0046F90546434A9AABF70E678D10213BB7C036CE,
	WXGameClubButtonStyle_set_color_mA58EA7718894A388FD76D61CD820E9DCA2EE01F8,
	WXGameClubButtonStyle_get_textAlign_mE04980A202CB88D4FDACC30CF0779070A7C6F035,
	WXGameClubButtonStyle_set_textAlign_mD23A829BBD7BD91CC793DCAAE2DB0A8E7270156A,
	WXGameRecorder__ctor_m90871441927A2E4F46A22E6613F7A65B7BAE4ADA,
	WXGameRecorder_A_mFC3D174D348FDDC4EEE0CEAAA0A8FC217721B62C,
	WXGameRecorder_Off_m6369B9CB87C96495730C58FEE1741ED415F73B60,
	WXGameRecorder_a_mCFCFDD39FEF0FDF936EEA8461CE153FED2947015,
	WXGameRecorder_On_m63B6C7D8EE9413CA7190A56F6F2C136C9F35CA7F,
	WXGameRecorder_B_m738D9CB3315042437BD822C1797C1BD824B9710F,
	WXGameRecorder_Start_mD02684EA6B127CC9842513C9F075BCC041FA1AE6,
	WXGameRecorder_A_mD496CA6F66B7984798DA9C8B08DB326A29C03A20,
	WXGameRecorder_Abort_m1AFD203D3A25DF068AFFFA614EC0AAA921D85873,
	WXGameRecorder_a_m100F6832D3651AD164E09310F432054C37DEDAEB,
	WXGameRecorder_Pause_mAA0E0E179CAFDAC80802B8129287E36364BAE444,
	WXGameRecorder_B_mFB2E046325D240E31F07531843D8395B183DF38D,
	WXGameRecorder_Resume_mD129EC3C8D428AA325EE73B02397CE5AE7B0EB6F,
	WXGameRecorder_b_m9B125031725E35EA860F2A8AE4D864A70B0CCCCD,
	WXGameRecorder_Stop_m6808D91390204103E8D75F3C6343E294D0F1F49E,
	WXGameRecorder__cctor_m948DBB1E62A125F3D0D98E62A7C587F70AF8B658,
	GameRecorderStartOption__ctor_mA8562A983B2ED945ABCEF6B3B65B6E558BD5FC63,
	GameRecorderCallbackRes__ctor_mC7815B29882B730FAAE71BDE60D66DE3FC797AD3,
	GameRecorderCallback__ctor_m439B9D9C5E973EB2DCE7986DE9CDB36835650551,
	WXInnerAudioContext_A_mCC5B431411E276B510837178C80F326D839984E9,
	WXInnerAudioContext_A_mEB069167335A738F73F2C4B467991D38D8A38478,
	WXInnerAudioContext_A_mCAD05B4619CE64EEFB8ACA428F60B1DA0B1003A8,
	WXInnerAudioContext_A_m938C42E78994F516264591AF523D6DF289CC5D15,
	WXInnerAudioContext_a_mDDAC7D845115FBC15215B4C6D6D1871CEBE89751,
	WXInnerAudioContext_A_m6BCB6588D8BC453E79738447FA62A0881CCE6369,
	WXInnerAudioContext_a_m6FACFE33007F5F5653602798CF0EA27AE804E3D6,
	WXInnerAudioContext_B_m18FFFBE837A5487AECAE60344CA71F9CDFF36BFD,
	WXInnerAudioContext_A_m5B30A5CEC4CABF77FC919FF661FAFAF76E1E4B24,
	WXInnerAudioContext_b_m2DC0CFBC4781D8E66EC8D3E327DD964D05BFB77D,
	WXInnerAudioContext_B_m1611EBCD43FAA2D7FE78AFCBB4EED6755FAC9BB5,
	WXInnerAudioContext_b_mAFFFE659D4180427794D4F9DF44BA13B0E0768B7,
	WXInnerAudioContext__ctor_m4A99E5C7EFF72080EF521E4E6F60C806D7C82F68,
	WXInnerAudioContext__HandleCallBack_m9EB8C55D4B05BA1A90A307B1D9460BB0AC523C6B,
	WXInnerAudioContext_get_autoplay_m8332FDA54B7BE6793591188CD3029728A3550913,
	WXInnerAudioContext_set_autoplay_mD4AEF513346974F6530102BEB472B10882822741,
	WXInnerAudioContext_get_src_m5B6E9F0D1673329441885F3D239805C19ACD572D,
	WXInnerAudioContext_set_src_m138A42B19CA284CE3C16719C47886891E2DE082A,
	WXInnerAudioContext_get_needDownload_mE19CF42194E74C89324D73505D3D080F6CC8014D,
	WXInnerAudioContext_set_needDownload_m0E945D03451F0CDEC85DF845C0C219BC9BF61455,
	WXInnerAudioContext_get_startTime_mD02A7AE8C38EDDBA3FA8CE2AF98D78D7F852D5F8,
	WXInnerAudioContext_set_startTime_m77AC475473D33E954C1ACF23737761FAEB1B1869,
	WXInnerAudioContext_get_loop_m9D8BFBCC1BD95ABA1104C6B5251CA7DC42EDFA95,
	WXInnerAudioContext_set_loop_mB027B701BE5456C906C8C5F2D46EC6C0E0D7FC6B,
	WXInnerAudioContext_get_volume_m2FEA9487EA81C98054EA759EADEDB5B6996A7A6B,
	WXInnerAudioContext_set_volume_mE5BFEF51CB435F315777D9CD8B60182A244850EE,
	WXInnerAudioContext_get_mute_mE5B30C2DF9B6D47BC8D430F5FE2BFCA6DFFC8B80,
	WXInnerAudioContext_set_mute_mF4B7D5DB8ADF06DEBE26325D2D3FDF944E1C92A9,
	WXInnerAudioContext_get_playbackRate_m631D4FF786155D9D72AB94472E635084C38BFB9D,
	WXInnerAudioContext_set_playbackRate_mA7ECAFB591918EC57894F2C23D98A77B77D87545,
	WXInnerAudioContext_get_duration_mA0B9D13FF35AFE1B34EE9C7DE9AF79AF3C0D8E77,
	WXInnerAudioContext_get_currentTime_m481335124A00A206A6CC88DE0481496BFA910275,
	WXInnerAudioContext_get_buffered_m9756B29D5D40282F5B50A4B982868B22E4A2D334,
	WXInnerAudioContext_get_paused_m9D5DC2C3D4251BD482DE60EE7406C9A4F0A80D26,
	WXInnerAudioContext_get_isPlaying_mCE239157D2C5AB5FB7F8692319815BCD9149DFCC,
	WXInnerAudioContext_Play_m3BCCF9354C6BF190B74362779A6971CE2AC4BA9D,
	WXInnerAudioContext_Pause_mB0AE7BF853F3BC1FC3FACC0822ABE440C10A49A2,
	WXInnerAudioContext_Stop_m71A150E8A7516986455B48A57B1504A50877B915,
	WXInnerAudioContext_Seek_m00CD843404ACEF377C309A4B73E4D89581ADF3A1,
	WXInnerAudioContext_Destroy_m466B97E027311156364EC197ECB11A496B517191,
	WXInnerAudioContext_OnCanplay_m85085F48B9239820B8F3000B71B3E53A6AE37F53,
	WXInnerAudioContext_OffCanplay_mBFC05E025E2CD32A4E9ED8420A81A262FF2F69BC,
	WXInnerAudioContext_OnPlay_m83AF0CEB631684568633A119E0631C0D7FC94921,
	WXInnerAudioContext_OffPlay_mD56E599FC3AA486C2AC8AAD45CB9A0F569326722,
	WXInnerAudioContext_OnPause_m7728168767B5D206BC9C28AB9131DA58EA018DEB,
	WXInnerAudioContext_OffPause_m120DC1F361C6453EA09DE6A9C84620B8709082ED,
	WXInnerAudioContext_OnStop_m0072ED8FE1F391B29BD442AD027E4983AD56F8BD,
	WXInnerAudioContext_OffStop_mD44706E698436D6A5231EF036FEA158F68BF5822,
	WXInnerAudioContext_OnEnded_m3BDB9FEAFD6F68D5B1D8D08D8E9336480C7816D0,
	WXInnerAudioContext_OffEnded_m705FBD4AFD1DD35140F4DD9FADB7FCD177910FE0,
	WXInnerAudioContext_OnTimeUpdate_m2B82FBA36D410405B78730F0CBA70593375AB865,
	WXInnerAudioContext_OffTimeUpdate_m102ED12B97B82CFE1F6818308245272BB8F0D43F,
	WXInnerAudioContext_OnError_m1F8DC3ED09CD83E593A73E44C5B1C1712C4E0146,
	WXInnerAudioContext_OffError_mE3F15607D35895E71E2D91BAD944DABB76D65AC4,
	WXInnerAudioContext_OnWaiting_m726AFC1D84E4E8C4FB20F9A70C39E989DED46835,
	WXInnerAudioContext_OffWaiting_m0DCCC0CA31EFDE6621F7B8901D07AD7E60D2E1BE,
	WXInnerAudioContext_OnSeeking_m6B8BBD160D9DA20B67DF5D1CF174C45741ECFD24,
	WXInnerAudioContext_OffSeeking_m2925CB97F293B46D300BB0743DBF44F06DEA4894,
	WXInnerAudioContext_OnSeeked_mACECD202640A041B47964F3C6AC4239D837F5AE3,
	WXInnerAudioContext_OffSeeked_m5C7C0C868D11E7454E329E360082F190C9DCB7F1,
	WXInnerAudioContext__cctor_m3CDDA8D30CC230D41273DCD3E86992ABF647AC88,
	WXInnerAudioContext_A_m39FCECA9FBCA63BC0F425E00B3E8459A7E4A0EDD,
	WXInnerAudioContext_a_m12EBA13620EB6541D25C395EA91A9ACD05FC584D,
	WXInnerAudioContext_B_m342407CF975D166E6759369BE76E4C65FAD7CE4F,
	WXInnerAudioContext_b_mE7250FEE3520CED4AF8FA227DEB22E7B06BAE346,
	WXInterstitialAd__ctor_m7A41CC58975CEB7AC4D472AED96A604D4EB5B6CC,
	WXInterstitialAd_Load_m2DDA990F4B4DB640A4D5FDB2AD6B7A2239DD6D13,
	WXInterstitialAd_OnClose_m54992787D926352D08026AFBFCB11161FFC32864,
	WXInterstitialAd_OffClose_m6D3EB797318C03EB4971478B7EBB1562D9189340,
	WXInterstitialAd_OnCloseCallback_m2CD8F1D69EED5516646EB54D7CF27E1896498ECE,
	LaunchProgressParams__ctor_mCE461380DA3ED0C85B91CC197F7D131783E95EE1,
	WXLaunchEventListener_A_m736C2D6AD9B7D9A779F6EA3C7BEA348BD633E12C,
	WXLaunchEventListener__ctor_m15C3F544AF0D29020BD7B6CD9137B631F55EEFD1,
	WXLaunchEventListener__cctor_m8B8610F8DA53E01E531AA3B9B29146731764E335,
	WXLogManager__ctor_mEE71805357F9EF8E1C8B7C6B5C50B30CFDC5D0A9,
	WXLogManager_A_mDC1023CDC59FDF6057CFCC58B79A72C4C6493ABD,
	WXLogManager_Debug_m822B9817D0CCBDD6B159C37995DDC5A49E2CE806,
	WXLogManager_a_m82FB7BA03E2B4649FE4863D64392E069256DD484,
	WXLogManager_Info_m14980D8500F07AC7B727EE6C0C5B4F0A43BF8720,
	WXLogManager_B_mD9EDFBB4864A56EE193802A0A9BC5143620F80BC,
	WXLogManager_Log_mB1E9C575650066A69231B55AE6E9E3ECDEE80D53,
	WXLogManager_b_m34C911A505B140F4B166EDD583A64B63598F235F,
	WXLogManager_Warn_mEA5643B7BCA7F05A59F2364CF11323D106DDC6DF,
	b_A_mE69E42D28E8AA3ED263522A1C8BA0FAAAE4FE6E5,
	NULL,
	NULL,
	b__ctor_mDF75E264434929DAC67D18172A353AEB1DC82748,
	b__cctor_mDC806E41285B3B67E9459B1E8C64B08D417F1BB6,
	WXJSTypeCallback__ctor_m4F9EBEDF17B404A1F222E47F99DBCD594913CB91,
	WXBaseResponse__ctor_mB8AB7AE058AF9F9260DE076BC2AD66598BD94238,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	WXInnerAudioResponse__ctor_mC916D9DA9B7A95E2C1E6CDD87A0041C118132021,
	WXTextResponse__ctor_m1036DFD7BF991500673381CD6D5F240BF631FC69,
	WXReadFileResponse__ctor_mEA60A03F21A4F9D338F0070178CF8DDE52B4EE65,
	WXUserInfoResponse__ctor_mA2D00CCF6D83CCA55CCAE778DB82655E2FB47E6E,
	WXADErrorResponse__ctor_m81FD136D05174A2F26DC22A5DF954EBD23D22E25,
	WXADLoadResponse__ctor_m3244D5C9CFF64177FDDC7FCC6055E60D295CFF3F,
	WXADResizeResponse__ctor_mC242C27353088D156D74E1E19B71944B7E5E2BC1,
	WXRewardedVideoAdOnCloseResponse__ctor_m907F3CA103E84A15FB797E389F4DB97C1CA08C27,
	RequestAdReportShareBehaviorParam__ctor_mB2DE81A093D95438FD320AFA634F710CF940E072,
	WXRewardedVideoAdReportShareBehaviorResponse__ctor_m285FEE50F3CBA28F91226411C6640FC9A4634F3F,
	WXCloudCallFunctionResponse__ctor_mE9261221AFF83E707977DDB8769D5D7A85BF6A46,
	WXAccountInfo__ctor_mE383D05E405D6DB46399FDB59A91F8060724C14D,
	TemplateInfo__ctor_m87807947743CC3E5F6775C54816CC82348AD731F,
	TemplateInfoItem__ctor_mBF3F5E00C67FEE76A478023AFDF26CF0EA3B0C15,
	WXShareAppMessageParam__ctor_mA6A26616D794C4D4F8D776249B8A3DB9C9785BDF,
	WXCreateBannerAdParam__ctor_m8ACC2D4318C26C5ADD1D083FFB33C43851C2F94F,
	WXCreateRewardedVideoAdParam__ctor_m884BBBB040C99488828641DBA5FCF032AE877928,
	WXCreateInterstitialAdParam__ctor_m7308687FC627D200589146ACD3DADE4A72C129EA,
	WXCreateCustomAdParam__ctor_m2D09A11DCEA6AD2DF74AF696134110299A04C083,
	WXToTempFilePathSyncParam__ctor_m59632348F73228E26FF8D357EC7E403B26F5F996,
	ToTempFilePathParamSuccessCallbackResult__ctor_m0FDEC6B39F433B50EDA34499675DF10BE1DCE532,
	WXToTempFilePathParam__ctor_m34A61FC0CD9EC7A93690EABD67159A5A4E6AA893,
	AccessParam__ctor_m76242295CD52A695032894268C9F8EA913FCD608,
	UnlinkParam__ctor_m769BDEC2A023FBD57D4A56087FE16C611C8B7EC9,
	MkdirParam__ctor_m33D200AEA7AC3200C14101F034B5F32DB58F0DE7,
	RmdirParam__ctor_mB0D606863EC541CCFDAA98416111AF8A8D600D7A,
	CopyFileParam__ctor_m08836402B6640631BCC57CAC38C072670B62C925,
	TouchEvent__ctor_m395714798AA26AAF8CA3DFDA95E3E72A2B129443,
	CallFunctionParam__ctor_m843E986A04A12A3E2D72F063FB8902628F131DB9,
	CallFunctionConf__ctor_mB976A9A9255791C91C9CEE41F0D74C12E4828E4B,
	CallFunctionInitParam__ctor_m0E4A9BC5414C2E82B48B67C7CA1F42DE4FE87D6C,
	InnerAudioContextParam__ctor_mBEFACA7301B347C0CD2C9AFE5E9DC8F355D562A9,
	NetworkStatus__ctor_m8FC9607BBC798FECE41A588E10D790621AAA6F5F,
	WriteFileParam__ctor_mFED492280DC4CBAB23CCBFDFE1AC58D470359C78,
	WriteFileStringParam__ctor_mEA15252785A805170C25D0CED61F7A11DF6F6BA8,
	ReadFileParam__ctor_m28A41E35544BF59771E87D323788E4245D2EB9F5,
	GetFontResponse__ctor_mACCCFDF56450517C02102D21FA218FADCFFBBA85,
	GetFontParam__ctor_m9FCABA84E92B85B94F09DEFCB0363BD0C315B96E,
	WXGetFontCallback__ctor_mB6962D1EBF7ABFB63DF4231331D67B909BA6A690,
	WXStatInfo__ctor_mF837F1784107E70E544820175693436F3CED9736,
	WXStat__ctor_mD916D8C443796EA7D4EBF672C62BA78CE3AEF050,
	WXStatResponse__ctor_mBEC924BA3944D44945E457277A6B40DEDBBB2168,
	WXStatOption__ctor_m6D3F95DA7DE60B81D0893C349729D050E5EFCAE0,
	WXVideoCallback__ctor_mDE62A33227486135212BC97B774EE48C89641F5B,
	WXVideoProgress__ctor_m51EE87D28A9F16D66FCE39D6FA3EAAB1161B0BA5,
	WXVideoTimeUpdate__ctor_m3A45CA6A70CDD82D4F3FD643E957CAF8A6343B27,
	WXCreateVideoParam__ctor_m07BF803234A336999037AD5DB0509C0EA2D36925,
	WXCreateGameClubButtonParam__ctor_mDEEEE12CC97D7E64F65A6098D6BDF896C6B8CBAB,
	LaunchEvent__ctor_m342E0BE8718F55B9D22FF5640BF9156F676A7D0A,
	InnerAudioContextOnErrorListenerResult__ctor_m102EBFD7EAD89A9D099CEF0D80FFD4A0C38734FB,
	ReadCompressedFileSyncOption__ctor_m4C814765E4262F0A57ED4A2397F9016DD153EE3C,
	FileError__ctor_mAB78B16BF6B5622D5C31A0DCF3677970DA5B24C7,
	FileSystemManagerCloseOption__ctor_m64581DCABB2DFAC584639A8F4DF9A968F3CA74CF,
	CopyFileOption__ctor_m3493C633D87E73BF713103F60051C578131B49C3,
	FstatOption__ctor_m42CD789FE4602432E0407CDDB7AF0333AA29FAD2,
	FstatSuccessCallbackResult__ctor_m530BE8D11F21FF61573CE8648DD7E91F1F57C32D,
	Stats__ctor_mF89A8A8C72FAC2DB3113AB857C023FA509B3734A,
	FtruncateOption__ctor_mFD5752391476101618E6A8A7C5A774E54B09A81D,
	GetFileInfoOption__ctor_mBA2F2BA0FEC60944B1C5FE8D2DA5CFBE45979B7A,
	GetFileInfoSuccessCallbackResult__ctor_m81BDD977813E61EF3A188BED03535B9453860531,
	GetSavedFileListOption__ctor_m23ECDEA91B85ECEC5E14EEF507194E742933AC0F,
	GetSavedFileListSuccessCallbackResult__ctor_m60117C55524F11C25935FA81336E80344C7A25D5,
	FileItem__ctor_m96843FF2512E65C01F3448D27AC0639CABA52BC7,
	OpenOption__ctor_m48D92EB39F92FDB95B62B79CF94E2C9284A4027A,
	OpenSuccessCallbackResult__ctor_m5E3576E24303653786CDAD8ECBA923A95CBAEE2B,
	ReadOption__ctor_mE24E917999B075AF3D273F08C63E6CB4B37E4BB4,
	ReadSuccessCallbackResult__ctor_mF89E454E4EC8A1A78DBF8385EB679CA78A153330,
	ReadCompressedFileOption__ctor_m389C3D65C22BF2706E3B42D8951EC65C1E7649BF,
	ReadCompressedFileSuccessCallbackResult__ctor_m3992577DC53B6268E772A6891005B06C7A9BD671,
	ReadZipEntryOptionString__ctor_m18041060A7831ADDC45E3489CF97A1AC55FDC518,
	ReadZipEntryOption__ctor_m210C8E345BDE3650C95C5092C5339C3CE6CA9240,
	EntryItem__ctor_mC113894AA53D57A4447D3E1CAAC522D33929E5F9,
	ReadZipEntrySuccessCallbackResult__ctor_m5389BAF6B7BBADE026D75C850941645723EB7ED8,
	EntriesResult__ctor_m594A56BAEAFE4352BE27AC076146575738E11EAB,
	ZipFileItem__ctor_m8B83F0526C80D585C3D5DD482716D3D0BE704E1F,
	ReaddirOption__ctor_mF3CA3E163C255943597C9D7EE93E79811F80FD07,
	ReaddirSuccessCallbackResult__ctor_mEA44FB742CC98D2A4EEFBF3839CE6F1B00C253BB,
	RemoveSavedFileOption__ctor_m35C94DA5D215BBA47EEF5511811F03D8976F8B0E,
	RenameOption__ctor_m5BF7071C139BF2E22C8924FB12F0558A22574834,
	SaveFileOption__ctor_mC9F2352ACB09AA7B4CE23C8A35544B038457FC52,
	SaveFileSuccessCallbackResult__ctor_m040A2B5300A0FA30D88B0B92E8A4ED86D83F6E7A,
	StatOption__ctor_m5198A889570995D82A1844C2C04049ED32B6B3A6,
	StatSuccessCallbackResult__ctor_m7436DC1A4734B337857C14B56BD8DA939006BC07,
	TruncateOption__ctor_mEA917A3FC261F4694F7450A77E94976B6EBB7756,
	UnlinkOption__ctor_m3AAAB604348B7AD769CCB0C2696FFA78966584EF,
	UnzipOption__ctor_m6331A9D28C6F690C14D5B342ED861FC67BDDE529,
	WriteOption__ctor_mC5DE86791C6985673740AFB9B2721A2A79105FD0,
	WriteStringOption__ctor_mB808F8807ADE481C6C83623A22B3E13E80F6FA30,
	WriteSuccessCallbackResult__ctor_mEDC2840A4326B8B9688E1FBB70FB13184A1FA6BF,
	ReadSyncOption__ctor_m1EFC2B2E7BCC7996D90DFB987FC562DB971E7BF0,
	FstatSyncOption__ctor_m83E92CD6E10C6287A0BFEF29E678E44007E33FC6,
	WriteSyncOption__ctor_mF576F1ED456FC948C771BCB64AFB0454528754CB,
	WriteSyncStringOption__ctor_m32691671BE73A7CA2F0D3FEACFA303ADFFFAA7FC,
	OpenSyncOption__ctor_m5454D3C46BB5D2BAA3F99E9A38EEBA6EA4908BDB,
	CloseSyncOption__ctor_m9E150A33950491A337065DA9D86C66A77604AEF0,
	FtruncateSyncOption__ctor_m5BCEFD3C48461FCE819AB129EF5B8E6B805C6CFA,
	TruncateSyncOption__ctor_m92265FF4D487E27D91164F53D8AD9C74A9F010BE,
	ReadResult__ctor_mAA18D82D81E728DF424054EC47280817A7517CC1,
	WriteResult__ctor_m9113694DA2F74B5A42CE250AF5C757141B2CC3A0,
	PrivacyAuthorizeResolveOption__ctor_m710F625E439179DE443DFC56C8E1F675B8E815C1,
	WXJSCallback__ctor_m20F3C51480B3DDFAD412C347981811F0FA6F5CF9,
	AccountInfo__ctor_m34DC405CA3DBE3C17337079746366DD5E1B56E93,
	MiniProgram__ctor_mF540A45D3EFAEBA6040780900238AEEF7480AE07,
	Plugin__ctor_mDF8FCE52CFF719259C5C465F3CCA104EA738A637,
	AppAuthorizeSetting__ctor_m061052E980553D8A7934D79FB3F9942894CD9889,
	AppBaseInfo__ctor_mCBA3702C49C9898DF404F3F499A780AF67B7F8CD,
	AppBaseInfoHost__ctor_m28EB3EF07E24B780A15654F1C6A442825CC0E338,
	GetBatteryInfoSyncResult__ctor_mA14E20B0F0A504E543D2EEF62D6D4B4342EFC68B,
	DeviceInfo__ctor_m06686DEEAFE1FCE8A36730885B1ABAFCA02F0999,
	EnterOptionsGame__ctor_m8C57F0369A56AD99301341E231D01E0CB2812993,
	EnterOptionsGameReferrerInfo__ctor_mFE9DA137D44FB1D58FFA1703B013AA1B77A908DC,
	GameLiveInfo__ctor_m1210B79D56B8D441761B7DC910A2ED7AC4384B15,
	LaunchOptionsGame__ctor_mFE38E642E97D234E6107F29424FAEAB4842A935F,
	ClientRect__ctor_m1B321C0B17498C9C70AC85D8F6E9F813C01C5C49,
	GetStorageInfoSyncOption__ctor_mC3C84543E1D56FA57666EF2212F940E6CFCD8159,
	SystemInfo__ctor_mF7B157B9EFC8D4333D2C4A100BB6C6FFE6348175,
	SystemInfoHost__ctor_m4CD610F0013663642B3C6A4E55E3D5180372E4C2,
	SafeArea__ctor_m7951E986A04D426DAF0F9EF066AC78D6F3630019,
	SystemSetting__ctor_mAC7B93F6473B16D15CAD6D0A48CB8949E1F54D84,
	WindowInfo__ctor_mE411EFE07F120F517BFFD24D2101AA06E39B9070,
	DownloadFileOption__ctor_m41A17E066255DC5362F9B37D1953E59DEC4ED2D7,
	GeneralCallbackResult__ctor_mEA2FBB48B4DFBDF2E9402C3F06350193C5E37BAC,
	DownloadFileSuccessCallbackResult__ctor_m953F1CA5B30E3CBA7C35634B55968DE7B2ACA064,
	RequestProfile__ctor_m394F0D98DF740C130A4C9B840EDC37C3A246AA73,
	OnHeadersReceivedListenerResult__ctor_m047868F7FD44B05D9D12BF34440A35EB5C97D94C,
	DownloadTaskOnProgressUpdateListenerResult__ctor_m66EFD5FC4AFC2608D96BF44DC89BBCB10F4B406B,
	CreateOpenSettingButtonOption__ctor_m1822443315D3BB224494180FB50DC1120CB06571,
	OptionStyle__ctor_mD31F31A5E3AD002A5E4D4DFDE71DE2125D968291,
	ImageData__ctor_m9F1807750EA61FD07C7A9D9AC87B94ED4C693F7C,
	GetLogManagerOption__ctor_mBFF6AC05216511379C3635CA1BB3B25B7FF318DB,
	Path2D__ctor_m9F98E00E8B2914CDC70F51629F336FC58E8B5507,
	OnCheckForUpdateListenerResult__ctor_mE7482638765AB2FE99874043C5AEFF1C7308A0D6,
	VideoDecoderStartOption__ctor_m19DCE8D573379B9EC83EDF857E694CAEF9CF882A,
	SetMessageToFriendQueryOption__ctor_mD9AB373E3C8FD30CAB9C4FA912346A67ADDF7322,
	GetTextLineHeightOption__ctor_mECFDB7E89E6485F65AD3BD8BF04AFA78D63EE621,
	AddCardOption__ctor_m5FBD70B6533E9EDF088A5C90EBCF430CD31401A0,
	AddCardRequestInfo__ctor_m046C6C6CD682D8683022A2203E85F18974981802,
	AddCardSuccessCallbackResult__ctor_mAE2CEB7F47ABCBF902F5D41929F95BB8DEFCA6ED,
	AddCardResponseInfo__ctor_m7E39B466491B1FD92B82D698E7EEE2030EE22A92,
	AuthPrivateMessageOption__ctor_mFA75497BD43231F1ADE8485EF92ABCFAAFAD18F7,
	AuthPrivateMessageSuccessCallbackResult__ctor_m7D4EBA922E619ED3037F1A04DEB7B4E66514875D,
	AuthorizeOption__ctor_m28713D260682257CB9F08640473A7F581569D133,
	CheckIsAddedToMyMiniProgramOption__ctor_m3AE2D03349191AF2CD01D2A4BD2B009F88BEA8CB,
	CheckIsAddedToMyMiniProgramSuccessCallbackResult__ctor_m90C219205C39ECC464820A3B79E1E3990681A075,
	CheckSessionOption__ctor_m3C6D1232694258F5BF820DC46255F194A99ADB2A,
	ChooseImageOption__ctor_m812502E3B8C834EFD2F601344D193E83962B5DCD,
	ChooseImageSuccessCallbackResult__ctor_m45BD438079712DBC3FB9077F79018913B7A67DF0,
	ImageFile__ctor_m77BA7A7B96044E2BCD08F9BED3CC2A7F48E5F933,
	ChooseMediaOption__ctor_m8003265B7C5059AC194968C38CC2A8F6FE7A4565,
	ChooseMediaSuccessCallbackResult__ctor_m9ACED599852BDC3D18DB07143ADB91A12BF959C5,
	MediaFile__ctor_mA3CED9F7A1AC3FC07720AE56E93A09A3D39F5E1E,
	ChooseMessageFileOption__ctor_m3CCD8033D8727292B744DA6F885F32CBA09780F0,
	ChooseMessageFileSuccessCallbackResult__ctor_mAC8830CAB6B4A63D18E77A2BA6BDE46D09FE84DC,
	ChooseFile__ctor_m8A77A06F8743A4748720C3E02EBA1658476A34B4,
	CloseBLEConnectionOption__ctor_mEAC589A317414C8BF32B213D8A3F7EEFE704BAB1,
	BluetoothError__ctor_m04663751C344CA799993986FEF6DEEBDF47DE339,
	CloseBluetoothAdapterOption__ctor_m8EB536D414E3B423B93FE5D096262296FA41BDE7,
	CreateBLEConnectionOption__ctor_m08A468FF8EC395EC48A53EBA172ED620C3145713,
	CreateBLEPeripheralServerOption__ctor_m155C39E4CA0718B09A587848E10C4B5F6B4B425B,
	CreateBLEPeripheralServerSuccessCallbackResult__ctor_m9B55D1083F65B19A218E72F010AE4F808CFD6A41,
	BLEPeripheralServer_addService_m241FE3B03985BBE7ED0A45C40E2865FB138C1D77,
	BLEPeripheralServer_offCharacteristicReadRequest_m3FED090DC8BE5557367ABDDAC5AB85531E2FF759,
	BLEPeripheralServer_offCharacteristicSubscribed_mD9425C4D8BD8C9C3EB8F0EF79052F3CEECFA841C,
	BLEPeripheralServer_offCharacteristicUnsubscribed_m56BA5DE7B287EC3BF13DD917B12BB46E5A773910,
	BLEPeripheralServer_offCharacteristicWriteRequest_m72DFD242D1A5774AFD1E954657402D7AA73AFF06,
	BLEPeripheralServer_onCharacteristicReadRequest_m37B1EAB26B22DCE1A2D0390DECA3AECC8F8BA6F2,
	BLEPeripheralServer_onCharacteristicSubscribed_mE9B9D91B8163A29CFBCCA9FCA07907943D6F03BD,
	BLEPeripheralServer_onCharacteristicUnsubscribed_m717045A97D03EB093791E8103DD551119AF7A62B,
	BLEPeripheralServer_onCharacteristicWriteRequest_mE320C7EB915650BC0D66094F5477AA622F3FAD29,
	BLEPeripheralServer_removeService_m9029DFDC9E18F8CC9EA19080983BC59D3A038DDC,
	BLEPeripheralServer_startAdvertising_m2A2D9251537B41F919F25763441E4C6F9BA80FF8,
	BLEPeripheralServer_stopAdvertising_m2C5A79E7EBF1B20041AD0DE407FF0E737A6F115A,
	BLEPeripheralServer_writeCharacteristicValue_m23F5E4BECC82777B1808F739948C6F88B0F9A2FB,
	BLEPeripheralServer__ctor_mCC0A528D7EE26BC418960F824B8104E65EB14E9D,
	AddServiceOption__ctor_mF2330940768FD6A5E32B4DE5A8F5C966436AC110,
	BLEPeripheralService__ctor_mCDE78F1C99F8387430D2DB46CFB8F6DAF6E9BAD2,
	Characteristic__ctor_m0912A51487D22278DD5409F68311AB8C1EB4A3FD,
	Descriptor__ctor_m457A324908735E037CB7CBBF443B30BB3725CAD2,
	DescriptorPermission__ctor_m7F4C0A1B23C0C6EF222647137CCB49627FC134FB,
	CharacteristicPermission__ctor_m8AE4ECA84A75CBBAEF09116B84EA14F69AFE9D46,
	CharacteristicProperties__ctor_m30A5D3771B649487D2F818528128F4FF73175AF2,
	OnCharacteristicReadRequestListenerResult__ctor_m512C9BC5A3A0F9D28F93D1E4977079D2685D4130,
	OnCharacteristicSubscribedListenerResult__ctor_mAB9CAF3171E5EA169DBB67C5E6BFAB317F8F5611,
	OnCharacteristicWriteRequestListenerResult__ctor_mB91B3C089743BC6FD14A14C0DC862AB7CC141860,
	RemoveServiceOption__ctor_m58A01C682015C93B340BC78BFEB4156CB92314BF,
	StartAdvertisingObject__ctor_m7C3C638296D56C063E894577AC839222F3B99B50,
	AdvertiseReqObj__ctor_m962352AE39FA314C4E63F5919C02F2698A3785E0,
	BeaconInfoObj__ctor_m5F84583189BD303073832055C1261824B3D962E8,
	ManufacturerData__ctor_m120DF42E5BBB8F81D730A5E492E996573CBE8697,
	StopAdvertisingOption__ctor_mAA3D98E6482FB3F85174E8CFB3836957003B0A27,
	WriteCharacteristicValueObject__ctor_mB1A60FF37FD9F73FFC0276A281242F2305EE13B1,
	ExitMiniProgramOption__ctor_m8416B198E0006CF255DA5785331A375BECF9A855,
	ExitVoIPChatOption__ctor_m9849EED70A9A7B92D7C8E7F2580F4BEB302BA62E,
	FaceDetectOption__ctor_m9DCB470937A411AE29B1B6DD65C93746CDC8750E,
	FaceDetectSuccessCallbackResult__ctor_mC8147FDD4FF4BED1F3AADB592E9A179B736DD56E,
	FaceAngel__ctor_m2D72AE7773D31B5900BAAAE19D37E44BCE0557B6,
	FaceConf__ctor_m91C6720DF2C3914CE20BD8744D6DBE604A2F577E,
	GetAvailableAudioSourcesOption__ctor_m2BF98C56312BAD1BF2C66412C59D67017BE9B0DD,
	GetAvailableAudioSourcesSuccessCallbackResult__ctor_m8F3D79140CA1842964F848DAA05FAD043639840E,
	GetBLEDeviceCharacteristicsOption__ctor_m4DADAF79B4EA68AEB4BFC20D54DDF986BED5AEDC,
	GetBLEDeviceCharacteristicsSuccessCallbackResult__ctor_m4BD2279518CE6EBC411688139C5F45A01F6F11DF,
	BLECharacteristic__ctor_m5E118089BC649E10E8298B519DAB00328B201ACC,
	BLECharacteristicProperties__ctor_m6444D972D8D9B738E92B9A8B3EEE433A802F8113,
	GetBLEDeviceRSSIOption__ctor_mA0A2FF67922EBF577483B869E21B3134C039476B,
	GetBLEDeviceRSSISuccessCallbackResult__ctor_m22248FA095865F6C8B758B546F2BADE5F67EB597,
	GetBLEDeviceServicesOption__ctor_mD060DA095FD48320742FE37F1E03599B5C4BEC3E,
	GetBLEDeviceServicesSuccessCallbackResult__ctor_m7FBECB5DAA23C6779352AAC41DDA546D64E0E13D,
	BLEService__ctor_mD2266968BAD422E31DE0AA8FDED96C1E3E23B020,
	GetBLEMTUOption__ctor_m0DF6D5E5DD70B8B0BAC00C523F06D00372B23685,
	GetBLEMTUSuccessCallbackResult__ctor_mA56BB447E34DDF74DB3F34F37CCEBDEB63D6F3E9,
	GetBatteryInfoOption__ctor_m15ABDE488B7DDE225B85BB3AB428B56A9082A2A0,
	GetBatteryInfoSuccessCallbackResult__ctor_mAA0B262311BCF3E6CC57058162DB78D606460E7D,
	GetBeaconsOption__ctor_mEF283F6E2A6D379348200D7D8237C812F5415D6D,
	BeaconError__ctor_mE7ABF680B37AD6BDEC59303092A8AA60F2BDE350,
	GetBeaconsSuccessCallbackResult__ctor_mA1FC7E34F3BA206D5962C68375F6A9F33CDEB34D,
	BeaconInfo__ctor_m8425413CE17B50A27DA8DC65B8F8171230107B9C,
	GetBluetoothAdapterStateOption__ctor_m32ADA1C463F210332A42DFFBEEEA23E8A3633AF5,
	GetBluetoothAdapterStateSuccessCallbackResult__ctor_m8CDF4DFBA7EC73C3F718FC8346491BCA55E2F36E,
	GetBluetoothDevicesOption__ctor_m559E2AA8AD6F5B430402B77241E0BE1034B0920B,
	GetBluetoothDevicesSuccessCallbackResult__ctor_m3A35FF42553C742204B8DBFDEAED88D0CAB72888,
	BlueToothDevice__ctor_m1BEF3347022B8302B6B67296EA065010E156112A,
	GetChannelsLiveInfoOption__ctor_m46F5B4A6A725A0B1C5ABDF4B7283CB3CA0572813,
	GetChannelsLiveInfoSuccessCallbackResult__ctor_m369F128F3094EB63D36B382FE4CAEB37731AFDF0,
	GetChannelsLiveNoticeInfoOption__ctor_m7212B338980BC8D1011C3E84E6BEDAA0C97A188E,
	GetChannelsLiveNoticeInfoSuccessCallbackResult__ctor_m85D1CAF30719E5EED13E28DB2F661C3E19D719C6,
	GetClipboardDataOption__ctor_m81847558B433460628FC7242DD3CBFED729E9DC0,
	GetClipboardDataSuccessCallbackOption__ctor_m8286E4B09D2BDFEDFD149CA98399E916336F72C9,
	GetConnectedBluetoothDevicesOption__ctor_m83F7F03CDCC437590EFF8AD8640796555A8D6E59,
	GetConnectedBluetoothDevicesSuccessCallbackResult__ctor_m8B6D41587C535C8596ECD09EC2DA28FEFE687DBB,
	BluetoothDeviceInfo__ctor_m6D36E27246FE12E49F7043E5ECCCC207B97C1176,
	GetExtConfigOption__ctor_m8D4B75921AAC27D5AB5C93EFFFD0E9D49104F8DB,
	GetExtConfigSuccessCallbackResult__ctor_mF0CCCB65416EBE55EBC55CF89A5419FDF8877683,
	GetGameClubDataOption__ctor_m42373D4CC59E6023804FE0A2BB98DD6DAC93B6D1,
	DataType__ctor_m18AE959465EAFA6BA6DBC6EF7BBAA1E59F36DCF6,
	GetGameClubDataSuccessCallbackResult__ctor_mAABFD436C9AC801EAD68AF4610500C0216CF12AE,
	GetGroupEnterInfoOption__ctor_mBC2C990963C5E30EBAB773D87CC5F9A3B9BB4B6E,
	GetGroupEnterInfoSuccessCallbackResult__ctor_mBE6C8D556916661C7703FF6C116B271EAADF43B0,
	GetInferenceEnvInfoOption__ctor_m07D74505BEEBDCE34F1B069E51B23A77F45D0F64,
	GetInferenceEnvInfoSuccessCallbackResult__ctor_m025D18CF255D7A00B4CD094B6A97CAA86228C3DE,
	GetLocalIPAddressOption__ctor_m8D4D7468684F8D9F8396EF80C009CF021A8CCCFA,
	GetLocalIPAddressSuccessCallbackResult__ctor_m38A451C761B61E931C549D5A4689013EDBDF26BD,
	GetLocationOption__ctor_mBBDD92ADF5A080941E15F6B7D38B5656DEA6C260,
	GetLocationSuccessCallbackResult__ctor_m45DF4117CB5B3F0332874E37AB9DA39C497C53E5,
	GetNetworkTypeOption__ctor_mC8BDC4E6707EECB95744DC555A64BD0C0F2352BB,
	GetNetworkTypeSuccessCallbackResult__ctor_m6041480CE5A15EC7CEBC009C8154F111388D8412,
	GetPrivacySettingOption__ctor_m62BA9DCD2A4662ED7B806791CE3C7D7573CC37E3,
	GetPrivacySettingSuccessCallbackResult__ctor_m51D468D27E2009AEB06088C8CEE605EB78D0F772,
	GetScreenBrightnessOption__ctor_m3A96300ECF78BA24787BC6F670B7754827BA660D,
	GetScreenBrightnessSuccessCallbackOption__ctor_m16A0F29281CE34988DBCF872FE54F297239F860C,
	GetSettingOption__ctor_mC37D2E28ADECAAE90F1F841EB678D2650FCB414F,
	GetSettingSuccessCallbackResult__ctor_mEB31987CAF73BC1E61AC05E1342DC3704F1785C6,
	AuthSetting__ctor_m78098C3670AC6F2BA30023A54C99C2F2974FC753,
	SubscriptionsSetting__ctor_m8A473707EA25EE2358E38F3A79EA460A26365F62,
	GetShareInfoOption__ctor_m4F1648804AEB3C79D2A4978DC06F09C3DE6DD2A1,
	GetStorageInfoOption__ctor_mCF136D54733F9D93532D01E18D5A411427D29F7E,
	GetStorageInfoSuccessCallbackOption__ctor_m70A64CB40FAA88E5216CB1592ED2D26D7857CE2F,
	GetSystemInfoOption__ctor_mBE651FE489E9CDF55099CB583FFA843D18E3459E,
	GetSystemInfoAsyncOption__ctor_mC9DA6B5728B3AD5E09F9E633A4F4F15DF37CBBC3,
	GetUserInfoOption__ctor_m6609286616997FEC18A26F4DC1B03ABEBB12C1E5,
	GetUserInfoSuccessCallbackResult__ctor_mE2D15A0959801EDE74D0806AC714A03B52B68A76,
	UserInfo__ctor_m7DE1BEF0EAA4491CCCD869FC71284ABB13E1A5A0,
	GetUserInteractiveStorageOption__ctor_mD9C0DF59280D39B5D1B5407BFD1675011C69C6EF,
	GetUserInteractiveStorageFailCallbackResult__ctor_mC961AB0174533EEC368E7889D7843BD96CBF5C5A,
	GetUserInteractiveStorageSuccessCallbackResult__ctor_mDBB5E26418C7FC461AF674006C283D4922C4EF4F,
	GetWeRunDataOption__ctor_m6F80FC3795B741804E509E47DB20032ECAB5EBB3,
	GetWeRunDataSuccessCallbackResult__ctor_mFBB8ECA6FAADC239400913F0621491B11D5B17A4,
	HideKeyboardOption__ctor_m0EFBABA954A073F6E6922CAE41B962CE25BF89E2,
	HideLoadingOption__ctor_mE75B52342E332DFBF35FCB691769ACADE5DD83ED,
	HideShareMenuOption__ctor_m16B9DFB5ACE06AE5B8EBE96346B94E1218624705,
	HideToastOption__ctor_mB0BEDAE224B2658A52656F62E33BBA0D6705C29F,
	InitFaceDetectOption__ctor_m56A21FBEFA6803D318899DCA49A6A4967F557401,
	IsBluetoothDevicePairedOption__ctor_m05A065D4A05BD3091D099A8A6D96F2D20679126C,
	JoinVoIPChatOption__ctor_m3351CBFC23D636DC1872CF6FDAB9947DD4E908D3,
	JoinVoIPChatError__ctor_m57F20CCE55BFABAEDC75020A6FA1BE8E3947031E,
	MuteConfig__ctor_mC867E103033E007616F8774FBA8CF820D28C7ABF,
	JoinVoIPChatSuccessCallbackResult__ctor_mEB95604BFF9E183C35099CAE776C9961EA528728,
	LoginOption__ctor_m93BDACFA012FBE764615C6CDA1878970834E250B,
	RequestFailCallbackErr__ctor_mCE29E0282B933F1CCBCC60F8563EA5C8E80DD26C,
	LoginSuccessCallbackResult__ctor_m4FD99CC272E1182F9292AB46E95A08F29318C664,
	MakeBluetoothPairOption__ctor_m27525F85F7047ED168115EB056DC6E49B4E4DABA,
	NavigateToMiniProgramOption__ctor_mFF6E5EC639E1CC28623351A9FF3F3ED8BD9C483B,
	NotifyBLECharacteristicValueChangeOption__ctor_mAF1C3B7D36FEB0266BA74A9698E6E45E4B71746E,
	OnAccelerometerChangeListenerResult__ctor_m1EC942DC32B2D529713A6A8049BB1A387F349854,
	OnAddToFavoritesListenerResult__ctor_m36CAE86E7ACE0C1FAA99BE65D594347769074549,
	OnBLECharacteristicValueChangeListenerResult__ctor_mD816C40BE69327EA08198C6B9609525039D6509D,
	OnBLEConnectionStateChangeListenerResult__ctor_m7B1B6EB98B5098602617C71F7DF82A88FC757A04,
	OnBLEMTUChangeListenerResult__ctor_mD5A71F4C73EF09FC5F86554790967252FE3A0B10,
	OnBLEPeripheralConnectionStateChangedListenerResult__ctor_mF97D0DD5B0ADF6F943F6C72AED8066546F54F7B2,
	OnBeaconServiceChangeListenerResult__ctor_m9BCDD0A2321C20130B7974C7ED8345AEB4A3EDF9,
	OnBeaconUpdateListenerResult__ctor_m05ED8CA8B07E1DE46CB515D5344CF1267823AADA,
	OnBluetoothAdapterStateChangeListenerResult__ctor_mDB4BDE68CF8254B8CDB2FCBF53678B32B12603C0,
	OnBluetoothDeviceFoundListenerResult__ctor_mD06CF807A539AE4C7015040F41D159086E10B689,
	OnCompassChangeListenerResult__ctor_m96DF04E8B5140D96063348B44309DD923BA1C870,
	OnCopyUrlListenerResult__ctor_m2C76C0D6B2548CEDCDF3A70318282E6DCDC04B33,
	OnDeviceMotionChangeListenerResult__ctor_m23B6C7BD79E58593D8629D6971D77604F20A6082,
	OnDeviceOrientationChangeListenerResult__ctor_m17BC3F01E9CB80F673AEEEE5BCACBDD0361CB04B,
	WxOnErrorCallbackResult__ctor_m80A1177FEDF716E2E8039A4139288219BF433095,
	OnGyroscopeChangeListenerResult__ctor_m4F1BE6EF71091F1DFD3F57AE62BADE392791BBAC,
	OnHandoffListenerResult__ctor_mC541EEFBBA49AA9E01573E501983BF0F0E8E5CF5,
	OnKeyDownListenerResult__ctor_mD45F0A390FF3DB2861064522EB7A8ED45D4C8D3D,
	OnKeyboardInputListenerResult__ctor_m197464C7BA8B8C9824F5BBE7A29ABCE88AE6AD55,
	OnKeyboardHeightChangeListenerResult__ctor_m9E805308686DBC1CF5610C84080E33A8805DDFD4,
	OnMemoryWarningListenerResult__ctor_m8169100BD871B3C23F16A2E98A7AAEEB802DAEFE,
	OnMouseDownListenerResult__ctor_m171D12C1D92B160618CE214650A4426748880AD7,
	OnMouseMoveListenerResult__ctor_m3ED63B6D76FBDF9A16473F391275FB47F26F8BD6,
	OnNetworkStatusChangeListenerResult__ctor_mC68441949E13C661D6B5711F5CD8D5C2D484F163,
	OnNetworkWeakChangeListenerResult__ctor_m9120AD1F69EBFEA77BEF2B162F9DEBB3C0F350DA,
	OnShareMessageToFriendListenerResult__ctor_m1EFD1C67168A5B2C3A6E44CD5E577E408EA7EBD5,
	OnShareTimelineListenerResult__ctor_m46A20AFAE151261879B58BCCC0FF92638A35D820,
	OnShowListenerResult__ctor_m0E84FEA9AD1B58545E37E214A669DA70FE63BC8E,
	ResultReferrerInfo__ctor_m1AAB5D628A029F8E6EB71B9444DA88F54ED7431F,
	OnTouchStartListenerResult__ctor_m8101E5F2EC2E8620266F564EB873BBA1A67C00FA,
	Touch__ctor_m04431B32D0209BB4210477F1B3ECF59B1582FFE2,
	OnUnhandledRejectionListenerResult__ctor_m7B2D6181B16CDAA64723F20589EBDD2C3D1B9A2F,
	OnVoIPChatInterruptedListenerResult__ctor_mDF8B833C082038557003CE3A6E466F82C9453E35,
	OnVoIPChatMembersChangedListenerResult__ctor_mB8079D880B85A38081145FE502291627C8E39DCF,
	OnVoIPChatSpeakersChangedListenerResult__ctor_m4CE382563073A87B45667B3D358CCECEE71643DB,
	OnVoIPChatStateChangedListenerResult__ctor_m29904DE02343A0C125B231A0962ECABDEA197DAE,
	OnWheelListenerResult__ctor_mBE43CC1B60A2C20F26439863B9F57CAAE7340908,
	OnWindowResizeListenerResult__ctor_mD5D2E058D16875C7E51EAE8EEFF618151D928BF9,
	OpenAppAuthorizeSettingOption__ctor_mE2F9BDF9A69E1CD75135AD79EA27FA477F24BC3E,
	OpenBluetoothAdapterOption__ctor_m47E8949C94AC49FCB91743E8A126FD64670F2F59,
	OpenCardOption__ctor_mC212F2189CC22E4CA53BDFB8827F536F9C928E32,
	OpenCardRequestInfo__ctor_mFF9AB8B60ED86F8CD86C7895ADB6964EA2070580,
	OpenChannelsActivityOption__ctor_m8D0A52ED850B966532A43D562C80435C85D328B6,
	OpenChannelsEventOption__ctor_mEE35B5B6CF4A98152870C911A54568F29B29C759,
	OpenChannelsLiveOption__ctor_m9C6B1A43885F38DFFDFD69AF10BD79D85E506DBD,
	OpenChannelsUserProfileOption__ctor_m6AB50402B10E06F38C25F3AB73181586D5390569,
	OpenCustomerServiceChatOption__ctor_m81408C93D8AF4AF4183152B7DE92FE05BE024974,
	ExtInfoOption__ctor_mE4A9A404A3D092FE4489234DD15762825A006596,
	OpenCustomerServiceConversationOption__ctor_m4B7E65F4D832EC4D796E8CD9088F0D7BCE3FB58A,
	OpenCustomerServiceConversationSuccessCallbackResult__ctor_mB72212CF4775782A83969E8B4CD29DD37C8B89CA,
	OpenPrivacyContractOption__ctor_m217DB5AB0AEFBDD625CFEFE0979C20CE59A6453A,
	OpenSettingOption__ctor_mA56FD734D8E199086E7E0E410FE4FD42231F95C2,
	OpenSettingSuccessCallbackResult__ctor_m86B6BB1BF27D215372560C58C144F29D457BC1F9,
	OpenSystemBluetoothSettingOption__ctor_m377E1659653C59094F1EC97BF349659F0D6DF717,
	OperateGameRecorderVideoOption__ctor_m368827A798152668F873371C078E5FDCB0C49E07,
	PreviewImageOption__ctor_m332324B488FD023C1CE820BFFFE6908642BC41B9,
	PreviewMediaOption__ctor_m9DE60A1CDE2BAE446C065BA9CB106A111467C8DA,
	MediaSource__ctor_mDE56F23EDA5D484B00AD7E322FE96729D3294580,
	ReadBLECharacteristicValueOption__ctor_m509EFBF44AC14EF7F7DF3223F03C4A9B8B8E9156,
	RemoveStorageOption__ctor_mA8F58D1A913C8DD7E3026AF17C6115F00189EE4E,
	RemoveUserCloudStorageOption__ctor_m4AEC9D566801C54A973FF326DF0DA1E110BDFD88,
	ReportSceneOption__ctor_m8E5ADE63AFB27BA189AD9AE787D91403B355EDFA,
	ReportSceneError__ctor_m92CA15D7CCA9A6D059C347FA6AA44717607BA802,
	ReportSceneFailCallbackErr__ctor_mF9896D6E78BF7E5ADD278D1E1F66486AF33A3ECD,
	ReportSceneSuccessCallbackResult__ctor_mA6C9EE4C58CB96A8403A2BBC15A4326788690549,
	ReportUserBehaviorBranchAnalyticsOption__ctor_mB633CE445561F87224E174542BC6309B67B60CCE,
	RequestMidasFriendPaymentOption__ctor_m3CF0B117B388286D5A8C2B54A4350D6617FA504E,
	MidasFriendPaymentError__ctor_m8EFE83862EA6566C4866FF8944D561EF9BB8333B,
	RequestMidasFriendPaymentSuccessCallbackResult__ctor_m0128D58EDF03A3D86AF8BFECF89C6ECD6C79127D,
	RequestMidasPaymentOption__ctor_mE14E7CD5D96772C97C3AC6FAF318B950FC98DB75,
	MidasPaymentError__ctor_mB1B7C6B27D9C151C11A621524C61E6902C403349,
	RequestMidasPaymentFailCallbackErr__ctor_m262A82B62F9D4D2A04C647F9A3A22E5D1B8BD38D,
	RequestMidasPaymentSuccessCallbackResult__ctor_m968B3E39A7B2FD2455B0C565C48E7C699E37968E,
	RequestSubscribeMessageOption__ctor_m90AE2820A24A5AEAEF614C46B732255999B522AE,
	RequestSubscribeMessageFailCallbackResult__ctor_m6B12B94335CDCEC9EEDF56632D3F88EF970F6F8F,
	RequestSubscribeMessageSuccessCallbackResult__ctor_mAC61C9C693EEF202EC4E16DE2DBD4E75DD7E9B6B,
	RequestSubscribeSystemMessageOption__ctor_mF4F88D20F5CCEFF380AEDFC3421460214F393E16,
	RequestSubscribeSystemMessageSuccessCallbackResult__ctor_mDA6EE15972207E0068E1C066BD4C92AB7426BD62,
	RequirePrivacyAuthorizeOption__ctor_m1CB5BB103872A5B6F024F793B71D5879CE00B2DF,
	ReserveChannelsLiveOption__ctor_m6F30CD8F921CDD475E952F2C6AA2BF5471F6DD89,
	RestartMiniProgramOption__ctor_m5B62CC59FCFDAAD0D6C7E4F1927E46ABF94530AA,
	SaveFileToDiskOption__ctor_m32380FC6255F05C6D69EE0E6968DEFBC6C2AC637,
	SaveImageToPhotosAlbumOption__ctor_m692B8D525AE2F2CAA4D4DB89319F61C0A8A8ED23,
	ScanCodeOption__ctor_m50F159CDABE935B95CFF562A86F891757CC8B7A2,
	ScanCodeSuccessCallbackResult__ctor_m22D1C13FB7A508157E99D2AE73B45D0A434B4237,
	SetBLEMTUOption__ctor_mEB8A220D6099F4EF0EA2628B8A09FD20C48A6E66,
	SetBLEMTUFailCallbackResult__ctor_m74D99C20515353BA1365B067DAE24E298C2A7161,
	SetBLEMTUSuccessCallbackResult__ctor_m36606C47A0E9B2A0A20CC333A28092B235FA14F4,
	SetClipboardDataOption__ctor_m94216C21A1A6DB1E4C67D95881CA56FA92C5C875,
	SetDeviceOrientationOption__ctor_mF935408329233B077E1E28E99180EA9AC1D55EB8,
	SetEnableDebugOption__ctor_mBB143C9A78FA1BB325E681BB99ACCE76CEE122B2,
	SetInnerAudioOption__ctor_m1091CEE9D92C6F0601758E66AE8AC49742D3F287,
	SetKeepScreenOnOption__ctor_mBC05EC1CEFCFAC959972EB903F5755AF075E6258,
	SetMenuStyleOption__ctor_m0EFC093E8A86A3B72B670C8585A5D6629228ECD2,
	SetScreenBrightnessOption__ctor_m385340455DEEFBAD80E9DC82B6ED451DE02152DA,
	SetStatusBarStyleOption__ctor_m2A7531F5516FD1BD0155EA223036C678325B5C14,
	SetUserCloudStorageOption__ctor_m3E9B633FB9C691CFD9A72309DBE387337570A299,
	KVData__ctor_m4BB18319F41812D14FF808CB833EEE596D0C7616,
	ShareAppMessageOption__ctor_m9D2D4E0270AA34A0EE5BFDEA3667D1FC8F2067B8,
	ShowActionSheetOption__ctor_m417EDAF0551FD1E9AC9812C181D93AB26748CC41,
	ShowActionSheetSuccessCallbackResult__ctor_mC14EE54342212B3C069500091FF304371B7B02DD,
	ShowKeyboardOption__ctor_m205E38B38E3C7D7DD5A6DE9BEFC05241F2F80EB1,
	ShowLoadingOption__ctor_mC5599336A5B8AB58BD293818CC8E009B5DC25B3B,
	ShowModalOption__ctor_m085DDB72C61278E2C91F1ACB8A70D4F3190B001F,
	ShowModalSuccessCallbackResult__ctor_mFBB3727BD3617016BC8B33D63D37C343FFFC27E8,
	ShowShareImageMenuOption__ctor_m675F2AE841562F51BD64BE4540B44FBD99BEAD1C,
	ShowShareMenuOption__ctor_m5255C765E1814A055529B01815F7213B24BF32A2,
	ShowToastOption__ctor_mCABB59D742D2891ACC6E25A101606FB84F2E8474,
	StartAccelerometerOption__ctor_m041071735D49E9D9BA203C3F95D9F07C915364DD,
	StartBeaconDiscoveryOption__ctor_m08FBEB25648F3F6DA4AD0253A363F927E6DE96CB,
	StartBluetoothDevicesDiscoveryOption__ctor_mD759B1497F133BA779BB3D6E528D6E790D0BE16C,
	StartCompassOption__ctor_mEBFB1C53CC35899EA2499330725F175E1A46D9BE,
	StartDeviceMotionListeningOption__ctor_m7619445DC5A6587B67240D3E80BB20FFF12A14E0,
	StartGyroscopeOption__ctor_m6554029ADB18ECA1F7D8F348104866B72C272C1D,
	StopAccelerometerOption__ctor_m67D127F65B0015DEF248735F2EDBB39919AF3B4B,
	StopBeaconDiscoveryOption__ctor_m2A988BDD6D68A032518F118D58A984E81DEF7CCB,
	StopBluetoothDevicesDiscoveryOption__ctor_m238A2F8ED5EA8A06B8232970D56FE67E7C281C71,
	StopCompassOption__ctor_mF1D345FAEF8C8EC7CEC872AA12908DABA33CF83B,
	StopDeviceMotionListeningOption__ctor_mCE233581F3A2F5284AE265678DB6B4E7DAF03DA9,
	StopFaceDetectOption__ctor_mA554A6F9D9AAB5E5EECA58E0E52E01AB7B4ED3A3,
	StopGyroscopeOption__ctor_mA0655EDA37118FC487BA1E54F182544746548F05,
	UpdateKeyboardOption__ctor_m4A6A076E1138A931C6D230D6FA9499B7CED54358,
	UpdateShareMenuOption__ctor_m482E41EE8FD17D0980C2B745E675F860CB2FE270,
	UpdatableMessageFrontEndTemplateInfo__ctor_m3F4C9AB56D504ACEFE7BF4A1A7D0D0F5F1BE63A6,
	UpdatableMessageFrontEndParameter__ctor_mB7818C309BFD7768D7420422FC0F5B547FB5A1AF,
	UpdateVoIPChatMuteConfigOption__ctor_m95E7E27B635144023EF289E87791C5869ED074F7,
	UpdateWeChatAppOption__ctor_m6608DB8E04275CF71D28C2D229E101D2F09C5FCB,
	VibrateLongOption__ctor_m74D40F6E969C3F59E4DB36CFE5FA552B28CFC7D3,
	VibrateShortOption__ctor_m19AF74068ACD0917D52AEED7FEA443B07D81AB68,
	VibrateShortFailCallbackResult__ctor_m7A4AF09BED206290BACCD7CA7B6052CFDEE70C73,
	WriteBLECharacteristicValueOption__ctor_m4C0E11AA0B0EA7F20CC7C584B65BEBDF3A5D4E2C,
	StartGameLiveOption__ctor_m67062514F3F656F7F98413E578AE6FFD96D7B060,
	CheckGameLiveEnabledOption__ctor_m7B63ED1C3C1241AF77BF16FA448D055CFC7FE237,
	CheckGameLiveEnabledSuccessCallbackOption__ctor_mE15EC51137A0F7D58394A743EDD3A4BEC2EC66C9,
	OnGameLiveStateChangeCallbackResult__ctor_m6C7AFBF09893774C4541CBBC91EBDEDB9BE4E084,
	OnGameLiveStateChangeCallbackResponse__ctor_m6566D436C34309D523610AA125CD79EAD2A212DB,
	GameLiveState__ctor_m463EEB622EDD0A42D6A2FCD66399977C296FE6D6,
	GetUserCurrentGameliveInfoOption__ctor_m891185F9A4792C74564A81DFAA37A155E99096C9,
	GetUserCurrentGameliveInfoSuccessCallbackOption__ctor_m1CDC665EFAACBC4F8BAA8FFC148F1F304D47B41D,
	GetUserRecentGameLiveInfoOption__ctor_m7D8E3324209097DAE283FB196B7730189ACD8C7C,
	GetUserGameLiveDetailsSuccessCallbackOption__ctor_mDE05BE5CEB5172979A7317706750F49E041D4AA5,
	GetUserGameLiveDetailsOption__ctor_mCA43F9E45384BA58885F19E924DD5AD4257C245E,
	OpenChannelsLiveCollectionOption__ctor_m416AB518AFE5177242E171E7F2FEE76AA4ADAE23,
	OpenPageOption__ctor_mC449748161D14DA9F3CD537AB990B2181F48035B,
	RequestMidasPaymentGameItemOption__ctor_mE860E58202D5AB273D85E24A939252E2DA72954C,
	MidasPaymentGameItemError__ctor_mBBEF23D64DDF03E34FC485CC0E6899DA30AB6433,
	RequestSubscribeLiveActivityOption__ctor_m43A22B90F9C712F68533410555EA50574918FD28,
	RequestSubscribeLiveActivitySuccessCallbackResult__ctor_m8B9F0F1693B717094477841DDF54CB9869E08A23,
	FrameDataOptions__ctor_mE4FDF4B2ED374D0DC3056D1C1DFE3B3E360B7EDB,
	WXOpenDataContext_PostMessage_m85C3FDCA0C6C814C5416650D59C94325EA2C9EE4,
	WXOpenDataContext__ctor_mA53F9F53F8C03548101FEC3A6AED060695476BD2,
	WXRealtimeLogManager__ctor_mD0568EFD441FEA72D47AA6AEBCD34C211B628F44,
	WXRealtimeLogManager_A_mCEFDF920469F9C7465ECEE0C259CA107DC11A0A0,
	WXRealtimeLogManager_AddFilterMsg_m5A92D9791357E586D740FC737E4B1F6CE3EDDDB8,
	WXRealtimeLogManager_a_mCCFFF5E47D99374C3B84F7912B573F025D7CE6EC,
	WXRealtimeLogManager_Error_mA94A1468388EE347C59E4A7C622894BBE13E2691,
	WXRealtimeLogManager_B_mBADE1B3EF4CE01ECAACB3A69AEA15DC86F0133F5,
	WXRealtimeLogManager_Info_m86DC3D51ABEAC094ECAE6AA40849E38BBB86B57D,
	WXRealtimeLogManager_b_m8B4D60AB29A53E8AE9CCCDC7E37CE8AB08BF13BE,
	WXRealtimeLogManager_SetFilterMsg_m12966EBBF5F46BEC374FFED36567699267CF32F1,
	WXRealtimeLogManager_C_mE73EDD2CF0B6020C8FB6D3C5F41C25E0A61D2384,
	WXRealtimeLogManager_Warn_m2306D988C1D3F95564B663D9125CE7154535C09D,
	WXRecorderManager__ctor_m6C63BB5EF3B079CBB60C407DC56E74181BAA67BF,
	WXRecorderManager_A_m508936A50D9C8CA42DB8D1906ED3C0DEF0A71A9F,
	WXRecorderManager_OnError_mC75A9487C26A2588B454A0B1D91D323520CB67FF,
	WXRecorderManager_a_m9A29AB914B7E46EB8856957711BD31BE9236DFA8,
	WXRecorderManager_OnFrameRecorded_m8F58CC34FF5994A60EBEE9E311B352D5ABC66FFB,
	WXRecorderManager_B_m56A17D9BD1800F9ACADB1D36D9C748AE5C7DEF39,
	WXRecorderManager_OnInterruptionBegin_m3358E28D71344A3085C11C4B7C79AABDEC9774CE,
	WXRecorderManager_b_m9728E23BBB102F6F7010C5B77DDC9E1BAB6438BC,
	WXRecorderManager_OnInterruptionEnd_m8EF77072F7D6E406A5B5A3049E9827D7C5AC4096,
	WXRecorderManager_C_m06A08E6AA1D40E21F928FF35E4FCF91980BE9708,
	WXRecorderManager_OnPause_m253B24D2479D768CBB82887863F38589F4F3DA2A,
	WXRecorderManager_c_mDEFEE50E47FDC9F47F161801486E5EC6B76CCE78,
	WXRecorderManager_OnResume_m199F7B6849C988C854DC23E9EBAE4C280F6C5960,
	WXRecorderManager_D_m1408F4715458D6E30596FB4CC512E4175ED7417E,
	WXRecorderManager_OnStart_m5DBBEB6D734CD1922E5A34F60C4340203631529B,
	WXRecorderManager_d_m2D58D99F5E2E505049A567139A2AED85548EC651,
	WXRecorderManager_OnStop_m07FBD1D69CD901D7323D21327B0D190D748018EE,
	WXRecorderManager_E_m104395455A4DC4EF808A0A2B429A2E73B60CA882,
	WXRecorderManager_Pause_mF7CB0B3CE8678872584008D668818FE6FDCEF79F,
	WXRecorderManager_e_m312A077955504F16996991B5C8B0A91834867191,
	WXRecorderManager_Resume_m7C1B863E1D73404F6FF9F5CF4989DCACDC59B9E9,
	WXRecorderManager_A_mC7EBF70FF01BDCB70F9AF6FB73731FDB03C0397E,
	WXRecorderManager_Start_m460C9009B65330AE45DAC58F9EE71F51128F82AC,
	WXRecorderManager_F_m8EFB62D690DA48100F247ECD17CAC9D90D95F305,
	WXRecorderManager_Stop_m0F70E95BD08A28764D54E8BF1FE25A3F04B00B55,
	OnFrameRecordedCallbackResult__ctor_m9FAFAC7DDF34C5688DFC8E717FB22BE9918C75FA,
	OnFrameRecordedBufferCallbackResult__ctor_m29B24E42A2178866DFBCC3F8669C34EC2640ABEE,
	OnStopCallbackResult__ctor_m54AE90E0A07A6B182DAAFEABF47D3601B0BCFE33,
	RecorderManagerStartOption__ctor_m40B474DEA2F37054EB1D2D423C1CED8AAFD5445E,
	WXRewardedVideoAd_A_m5ACA2EB7B87E244380E11FFB089D8B27E66F2F78,
	WXRewardedVideoAd__ctor_mBA3881062D98F861C0AA8572913091F5E8AC0E33,
	WXRewardedVideoAd_OnCloseCallback_m1AF901145BDB7B36719303CCA18CF37494AC3D32,
	WXRewardedVideoAd_Load_m44346E7FEB1EDF8D06846F102C5A8D50D570A294,
	WXRewardedVideoAd_OnClose_m36E643BE8F5D9AB65E8E1500E4243A943C061CA0,
	WXRewardedVideoAd_OffClose_m81118DC866C735F81D70A26A8E5D77FF7AD22AB3,
	WXRewardedVideoAd_ReportShareBehavior_mF6BFB3562343F85EC41B23ABF968E67B0F0F7842,
	NULL,
	WXSDKManagerHandler_AddCardCallback_mF81C26ED35A5A9ABEE7F99E14BA4401E0CED4AF7,
	WXSDKManagerHandler_A_mF78D18580496D91EECA5A81F4E8C7F625A6C5BA2,
	WXSDKManagerHandler_AddCard_m36AE74DD5E00C43AEB1B7430FD9FAEE139E86B32,
	WXSDKManagerHandler_AuthPrivateMessageCallback_m0F24164768DD7C8D3E47423697FA095C4587ADEB,
	WXSDKManagerHandler_a_mB2CD877A88109489BBEC92B3C828988810A78A42,
	WXSDKManagerHandler_AuthPrivateMessage_m9CCC429D98017D5DD1834461C0E3154EC5940AE1,
	WXSDKManagerHandler_AuthorizeCallback_m8D9A412DD52B0266DACF5947C12D5EDD608ED60F,
	WXSDKManagerHandler_B_mBD775616DD641BB88E4399A2C1DA84D67ED55E1D,
	WXSDKManagerHandler_Authorize_m4412649F0D9A306A8B8DDDD2EC9961415ECFA408,
	WXSDKManagerHandler_CheckIsAddedToMyMiniProgramCallback_m6FE88C5FF2769023A61DDE8589239757F5F1A56F,
	WXSDKManagerHandler_b_mEE617029548D202A73BFEA208ACFD3A2B8ED25A0,
	WXSDKManagerHandler_CheckIsAddedToMyMiniProgram_mB2852C4469B80C00C2DF306ED88C9A54DD2C579C,
	WXSDKManagerHandler_CheckSessionCallback_m1BFDF3CE1D0B029027ADF6EBFDF13609F097A76F,
	WXSDKManagerHandler_C_mA49DFF4E5255356C40CE7EE2284AFEFD8FEDEEAE,
	WXSDKManagerHandler_CheckSession_m9BE75BA73BD576C9C37481EB5D58A2B773CB6763,
	WXSDKManagerHandler_ChooseImageCallback_m3A9093329E7286E91E094EE0F6670CBE2076CFF7,
	WXSDKManagerHandler_c_m22A33D6C009F7034D19A0E1C3BC60E50E18AE0EF,
	WXSDKManagerHandler_ChooseImage_m9A1976749AE6D06EB0E05D1DC877F9AE64771CBC,
	WXSDKManagerHandler_ChooseMediaCallback_mDE8F36B7A59A7F2ED1BEED10BFE4230BA4EB9170,
	WXSDKManagerHandler_D_mDF26CC2E9D07B155709DF464669701EAF4F9D2A7,
	WXSDKManagerHandler_ChooseMedia_m7C8E0D8B637975325992160AAE738429872639C8,
	WXSDKManagerHandler_ChooseMessageFileCallback_m590DF04D7A3276CEEB9F5F1102847A376AC51015,
	WXSDKManagerHandler_d_m3F3043B925F3A333386124806B577623B64B5980,
	WXSDKManagerHandler_ChooseMessageFile_mB7E7F98C95B8DDF43CD03B2AAA13B9428201C36F,
	WXSDKManagerHandler_CloseBLEConnectionCallback_mE9981D10549D04C7AEF342FB14EE54C23DEA3191,
	WXSDKManagerHandler_E_mE4D151CF693252D176C9A7051DD618D202A120BF,
	WXSDKManagerHandler_CloseBLEConnection_m027893595FB94024CA8230DC76A8F5D9EB9D5677,
	WXSDKManagerHandler_CloseBluetoothAdapterCallback_m7C885B1CAAA63329FAD4C255A19981DF6BC005DD,
	WXSDKManagerHandler_e_m4840004541A104FC3B8FF06E6533DCCFEC6AA490,
	WXSDKManagerHandler_CloseBluetoothAdapter_mDE24D06948F1CA476CEE89A7D61153DD25B3ECDB,
	WXSDKManagerHandler_CreateBLEConnectionCallback_m5D33DA3DF10FFC17C7EA0E0383A23FF8C0D7D595,
	WXSDKManagerHandler_F_m32C126B54D2609E20B1160F54170D9CB0CD35C97,
	WXSDKManagerHandler_CreateBLEConnection_m5BB6D877BE8C77CC9FAD17FF9B84A2A2D5F341FA,
	WXSDKManagerHandler_CreateBLEPeripheralServerCallback_m61995D2318A8F504A61F7DB5F3BBE8AD2530F21C,
	WXSDKManagerHandler_f_m41EF543EBA205484EC03A5C45F38B5DC21A9709A,
	WXSDKManagerHandler_CreateBLEPeripheralServer_m1AE25612CBAA74473E9A4AA55D5855DD04DCA677,
	WXSDKManagerHandler_ExitMiniProgramCallback_m32EF41E6FEE8D7F384BE0590E89864F3165CFB9E,
	WXSDKManagerHandler_G_mA93C6C55252C72363B16A5ABA5F9E706BF8A4720,
	WXSDKManagerHandler_ExitMiniProgram_m0AD9C912FFB39E9326309300C640ED888B9E0615,
	WXSDKManagerHandler_ExitVoIPChatCallback_m36CE2131E677F2E3C439E295617BF53A20937C0E,
	WXSDKManagerHandler_g_m1930C23894082B41941AF3557437CBD083179079,
	WXSDKManagerHandler_ExitVoIPChat_m2DE1A56CC8C27545DB0AD8F0832D1FA78E838270,
	WXSDKManagerHandler_FaceDetectCallback_m15D6F80FA5C719E53020AC4F32929D61DE774C4E,
	WXSDKManagerHandler_H_m6C50414A2465CFC5A2EFC0CE61C60D5549EC0A00,
	WXSDKManagerHandler_FaceDetect_mA8B1F45A2E0FF82EEE0ADC4A3F402A02DDA51955,
	WXSDKManagerHandler_GetAvailableAudioSourcesCallback_mBE586BD77B1054401B28D25D36C16610C62E1D2F,
	WXSDKManagerHandler_h_m6CC463398DAFD841F4262B76458EFCE0A48D60F2,
	WXSDKManagerHandler_GetAvailableAudioSources_m083E786C4427C33C122845AD79CB1621D398C069,
	WXSDKManagerHandler_GetBLEDeviceCharacteristicsCallback_m90F977DF087811BF85CD97054D51BF8126522AFF,
	WXSDKManagerHandler_I_mC99FED494527D6D079DE3A0E425CAC9CAF88C5A6,
	WXSDKManagerHandler_GetBLEDeviceCharacteristics_m340DC9700BB9DB064966533A0FB8E350DE5458F9,
	WXSDKManagerHandler_GetBLEDeviceRSSICallback_m8C69C4BD89C2D4483F16B2CFFC10B636F2D4927F,
	WXSDKManagerHandler_i_m781A1DAA4474EA284F07C8352E0B7C03CE1CE3DF,
	WXSDKManagerHandler_GetBLEDeviceRSSI_m75C2161BDF843C56FE46822EAAE92C116AB6FBAC,
	WXSDKManagerHandler_GetBLEDeviceServicesCallback_mBB991E7925ED181233C2F10DE8E1BB3C8B062388,
	WXSDKManagerHandler_J_m9F9D214021C356B5CC0914894868305A184A4A02,
	WXSDKManagerHandler_GetBLEDeviceServices_m34B68DC2E47A061A68D4F8E3869DDC8A08882EA7,
	WXSDKManagerHandler_GetBLEMTUCallback_m9FA0AB1460F042CFFB40C820FCC754E9154FDD7D,
	WXSDKManagerHandler_j_m62CF5357D9E235EFFE8408E5EB94910A7FC27F35,
	WXSDKManagerHandler_GetBLEMTU_mA29C9B06479DC7A110BE077FB9DDCB8A05470B62,
	WXSDKManagerHandler_GetBatteryInfoCallback_m70FB9A8B05F6DAD5205274A6FD991B7B1A080887,
	WXSDKManagerHandler_K_m306046EA4FD11161EA266D423EF1F0A2DFF73C31,
	WXSDKManagerHandler_GetBatteryInfo_m2E842FB0D7F4F9D5B041FED0EE9D661C1EC48957,
	WXSDKManagerHandler_GetBeaconsCallback_m147B06F603CB741E303D09A9FB5C8C291E807AC2,
	WXSDKManagerHandler_k_mAD2D117B222C0F9EFDD778FFDE0CF217CDAD5726,
	WXSDKManagerHandler_GetBeacons_mD98329375BD8A8709BC92036086D3ED4EEF632CE,
	WXSDKManagerHandler_GetBluetoothAdapterStateCallback_m3F37A4D57F5E42B537731D70478113085E5D423A,
	WXSDKManagerHandler_L_mCDD9738B1EB21E2E1B1AE9C44BAE029141D7C537,
	WXSDKManagerHandler_GetBluetoothAdapterState_m9B4683CDDB69F2B078BC2839030AA4C16AFAE0C3,
	WXSDKManagerHandler_GetBluetoothDevicesCallback_m5E57B2367BA75F43A5E0DC3FB6B783306416BD4C,
	WXSDKManagerHandler_l_mFD43BFA587FCB2F174502155D968FE59BAAB98FF,
	WXSDKManagerHandler_GetBluetoothDevices_mE21CF08C3F24C718662A16770B8278F550919802,
	WXSDKManagerHandler_GetChannelsLiveInfoCallback_m8E6BE4127E7ED953E36EE875DB3AD55B39D2D894,
	WXSDKManagerHandler_M_m73BF600C7B7FD1C8928C76B446EC70CF068F5B93,
	WXSDKManagerHandler_GetChannelsLiveInfo_mFEC764C69BFD437C868271C298164C6F12A45652,
	WXSDKManagerHandler_GetChannelsLiveNoticeInfoCallback_m5306288E30351C7BCF1E81547FC1576E64FC02F8,
	WXSDKManagerHandler_m_mDBB3E5701656230BB01D4748AA08695CF11F7ADF,
	WXSDKManagerHandler_GetChannelsLiveNoticeInfo_m9BF668AD530C732A3E1EBEE0830C52D7DB2A7EA1,
	WXSDKManagerHandler_GetClipboardDataCallback_mBC05116D756A31E9AEF115EECBD09BC4A0C2A1B4,
	WXSDKManagerHandler_N_mEC253FB6A4FCE362FA77ED8FAE133058A73B39ED,
	WXSDKManagerHandler_GetClipboardData_m5472556E307D7B073BEA936CA187D2D5607118F2,
	WXSDKManagerHandler_GetConnectedBluetoothDevicesCallback_m77812E2D48D25F03545ACB1A5E3D14C9E71DDA74,
	WXSDKManagerHandler_n_m26A6A0D5D99DA2B54CCED7D56625F5DFB4EE24F7,
	WXSDKManagerHandler_GetConnectedBluetoothDevices_m2E5624B35160A28D840BF1FD1ACAF3ADFB9C28CB,
	WXSDKManagerHandler_GetExtConfigCallback_m3DEDA5B64738176F92261E63FC3B80D6DFB155BA,
	WXSDKManagerHandler_O_m0684677CC35C324BC23E063E9272651ED5ED23B3,
	WXSDKManagerHandler_GetExtConfig_m894BFD20E89ED4BFC625748BC3FAF4C70B804BF5,
	WXSDKManagerHandler_GetGameClubDataCallback_m67B9370BA8D0CE0C0464B795535B8F44152C2CAD,
	WXSDKManagerHandler_o_m924E71EB830887E70534AB61C90C96D8A9D57185,
	WXSDKManagerHandler_GetGameClubData_m89839BFA486D4C3CB403D2365B0C90133BFD838F,
	WXSDKManagerHandler_GetGroupEnterInfoCallback_m8BF3F284CC0C2EDDEB73F775E5288935A8232CF3,
	WXSDKManagerHandler_P_m2B1408E5E71FF2FE1FA08D37E0C29B61A77ECEE2,
	WXSDKManagerHandler_GetGroupEnterInfo_m113BC3AE3E054ADA900B4E069887F003F233678A,
	WXSDKManagerHandler_GetInferenceEnvInfoCallback_m43DE363C84E18C004A3F3B46F2875F5D2EA4E010,
	WXSDKManagerHandler_p_mBFC819AC230F9B7836FD87FE33BB60D6A5E825D7,
	WXSDKManagerHandler_GetInferenceEnvInfo_m90F3C805FD6CFCB171C1D69F0D2C2F3F422F4D49,
	WXSDKManagerHandler_GetLocalIPAddressCallback_mD54687848AAD0B49B7B0EFB28F2539AAA8A5A46B,
	WXSDKManagerHandler_Q_m0AA1BBBDEAE654B05C43532441E57D2D2959B541,
	WXSDKManagerHandler_GetLocalIPAddress_mC7CA3C25A2447F9D66FBC9984D5047D1050DC9C7,
	WXSDKManagerHandler_GetLocationCallback_m77761B1CB80F1C154A4A2666297EE68A58402C37,
	WXSDKManagerHandler_q_m2ED81D2D35AE686951841316AC566944C8801063,
	WXSDKManagerHandler_GetLocation_m979940B142AB5C389B375A91D7DA520CFD529125,
	WXSDKManagerHandler_GetNetworkTypeCallback_mA3E939A65901A25C308CEE63E79DF481969C7F5D,
	WXSDKManagerHandler_R_mAFD5C7A9C937CBB6FDD85C8C9DF7D6BB6422E08A,
	WXSDKManagerHandler_GetNetworkType_mED4BB255951D163132499F726828F3BB4B893D63,
	WXSDKManagerHandler_GetPrivacySettingCallback_m41C9CB32696AF2A9C445692711A4F9D64E4AE52C,
	WXSDKManagerHandler_r_mD7973A1B23E30898CFC76209EE12DF5C9EAA6E3C,
	WXSDKManagerHandler_GetPrivacySetting_m3C46E2ADA953BEFABF40D067EFBA9D19839F4235,
	WXSDKManagerHandler_GetScreenBrightnessCallback_m8987F1F6C36B4E7FC2CBA95483394EF11385EA07,
	WXSDKManagerHandler_S_m29AF791F83E1625662C0FB2A8B6C3CF2E7F807AF,
	WXSDKManagerHandler_GetScreenBrightness_mC95E4F5D70A6CA77F1404366C77E8E221ACAE5DA,
	WXSDKManagerHandler_GetSettingCallback_m989F3B898C2948931C332CE7537F41D1B49B26D1,
	WXSDKManagerHandler_s_m6828271ED68351B34003A618AC047F5C58D9423E,
	WXSDKManagerHandler_GetSetting_m7B02E3B76717911CBB487F5976C0678C242385DA,
	WXSDKManagerHandler_GetShareInfoCallback_mC635911F387CA72D397F009A5C6A097AD3377493,
	WXSDKManagerHandler_T_mD8B8F13A2DD5F4EB21D0D4F39AB4BFACF8137E13,
	WXSDKManagerHandler_GetShareInfo_m33CF859B78EA955F5778D20858BC3F0391CD9A56,
	WXSDKManagerHandler_GetStorageInfoCallback_mB9EEED332AD2AF346361D9E41FC8262C9F6CF540,
	WXSDKManagerHandler_t_mA986484771914B285BB61593326943EBFEF4A76A,
	WXSDKManagerHandler_GetStorageInfo_mD7DD5F0522893C29CC35ECB67305B4C8F24E86C5,
	WXSDKManagerHandler_GetSystemInfoCallback_m5AD9B149BDBB1298B9708DF44AB9323849F90011,
	WXSDKManagerHandler_U_m7592DF329C8CCDE134BAAA09EF054C3BAB07DBFD,
	WXSDKManagerHandler_GetSystemInfo_m0E9225EEF0D53184FEB52EFFC41F84565725FB31,
	WXSDKManagerHandler_GetSystemInfoAsyncCallback_m0CA117D3B9B8CFA1D55272A8CEAFA83E722FF486,
	WXSDKManagerHandler_u_mC586A5FF4609FCF490C66BDD6C5467663004DB98,
	WXSDKManagerHandler_GetSystemInfoAsync_m42341E72B567844235AF0A0C175F6CD9095B1143,
	WXSDKManagerHandler_GetUserInfoCallback_m3B67B2E07A09A61F61F1563C37FE255F171BA9CE,
	WXSDKManagerHandler_V_m33890476C03B5220212584F65464DF29535FE399,
	WXSDKManagerHandler_GetUserInfo_m4F937F33D57A112AF3D88686329A216D0187EDD1,
	WXSDKManagerHandler_GetUserInteractiveStorageCallback_m2F4C47686B11365A618D5DCF6962211AF014CF28,
	WXSDKManagerHandler_v_m2DE00E0A91F2016CF040246AB038AB1319B970F2,
	WXSDKManagerHandler_GetUserInteractiveStorage_mED49E8A76A2D58ED3DF76B64AF2B92012EE231E1,
	WXSDKManagerHandler_GetWeRunDataCallback_m1F04715220BBECE408430084B8AEF5A32E9890FA,
	WXSDKManagerHandler_W_mC37346BB2E84FC2045A2A20CF92CAE76CAC9658E,
	WXSDKManagerHandler_GetWeRunData_m021C41FA1A4F7FDF45E0F6DE82EFE530E494813A,
	WXSDKManagerHandler_HideKeyboardCallback_mD978903D7BC5B2392EDFB71D51571757DA58AA68,
	WXSDKManagerHandler_w_m2009B33323A434E8275A4E746EC8D8A374FD39F9,
	WXSDKManagerHandler_HideKeyboard_mB753D4B94A0AD39403703D65D2A0BCA5AEA5CF93,
	WXSDKManagerHandler_HideLoadingCallback_m1240E594B171F5284E93311A0401FB7C767B8192,
	WXSDKManagerHandler_X_mF64D343AB1F98A3F564523B5E4059C1B30498120,
	WXSDKManagerHandler_HideLoading_m0443D04528C7E20E0A27E6C2E32EAD53A391DB7A,
	WXSDKManagerHandler_HideShareMenuCallback_mD419B2A84542C299AD8E97573F8927158D867D7E,
	WXSDKManagerHandler_x_mC3500B136222DDF2F24121C3DD6B3E087B09E064,
	WXSDKManagerHandler_HideShareMenu_m3088A1881330A64E7DCC7CE3B35E5EDEFDD1E631,
	WXSDKManagerHandler_HideToastCallback_m7DFEEC9A3786D2E10FB87F73022EBDC8B8B338C6,
	WXSDKManagerHandler_Y_mF4FB5957FF3B03DE7E205A291C5A56A56600A257,
	WXSDKManagerHandler_HideToast_m0E015FAE5DDFABE390DFC87023A66B3B6B1010E2,
	WXSDKManagerHandler_InitFaceDetectCallback_m42ABBD03790F8B90EB4EA688A597FC8657B5E417,
	WXSDKManagerHandler_y_m196F68DF6BDD2764F32A9779B499FCA1466F5CDE,
	WXSDKManagerHandler_InitFaceDetect_mB242C5581EDEFBFFE66A30739451F3B2A02D2E16,
	WXSDKManagerHandler_IsBluetoothDevicePairedCallback_m698F406604F33255E111AF6A8769E6ABA8BE5E54,
	WXSDKManagerHandler_Z_m046742AF0B48E476BB9A141EA03978BAC8FD6AC6,
	WXSDKManagerHandler_IsBluetoothDevicePaired_m74D6D108BB6849A4147C8D25677B697F970EDA7E,
	WXSDKManagerHandler_JoinVoIPChatCallback_m4EF8E8F33ABA130AE7869478578764446F4E838A,
	WXSDKManagerHandler_z_mF414F43E3923642FCE179E839C05A906AEAC88BA,
	WXSDKManagerHandler_JoinVoIPChat_m4FB725CB3D5EFAFF91009ACA9A8BAD7BE9066CC2,
	WXSDKManagerHandler_LoginCallback_mD36D507015DF95F83A866CB317183AFFF07DA07C,
	WXSDKManagerHandler_aA_mE0EDBF877556F9D84BBA8FB3FA6EF5977402B3B2,
	WXSDKManagerHandler_Login_m7C7FAB035B0B17C892EC8D1D6F6086AD78B3B996,
	WXSDKManagerHandler_MakeBluetoothPairCallback_m523D0EF99BCD76671A8116453EF57D33B1740D67,
	WXSDKManagerHandler_aa_mD15A12EECE3757C191D5CFBF5CFF9617D3A423D6,
	WXSDKManagerHandler_MakeBluetoothPair_mBAF7180401248CD0F463219571854E61E8B617B8,
	WXSDKManagerHandler_NavigateToMiniProgramCallback_m622145989C8838ED3FCC7692EC78CFDD80CCA813,
	WXSDKManagerHandler_aB_mE8A45E26485788E3822A8AC828B54209E853D82D,
	WXSDKManagerHandler_NavigateToMiniProgram_m13C999C5F1A732436025EBDC75129597E7837252,
	WXSDKManagerHandler_NotifyBLECharacteristicValueChangeCallback_mE1AC58B4724801D0324EBCD6766A0D97C80DC440,
	WXSDKManagerHandler_ab_m74BD3D38A41A1DD35ED6913D3562BDCB0BE6AF5B,
	WXSDKManagerHandler_NotifyBLECharacteristicValueChange_m03EC6F44E3D791CE3B09060B8C16C9644F65A112,
	WXSDKManagerHandler_OpenAppAuthorizeSettingCallback_m57E9E7A0673E72331B4DEE39FC34BFC201258052,
	WXSDKManagerHandler_aC_m55CD47754DFA9AE3E3422B82CC7CC00585C45E1E,
	WXSDKManagerHandler_OpenAppAuthorizeSetting_mCA5C24FE86AEC1522AE905A574D915C586FB1A2C,
	WXSDKManagerHandler_OpenBluetoothAdapterCallback_m8CDD98BD1B70BD4A184556F31A6409F5E8ED7863,
	WXSDKManagerHandler_ac_mE54DD3458BDA3DDD9A9BEEF7A154C706DB689FFA,
	WXSDKManagerHandler_OpenBluetoothAdapter_mCE7AE6F2FED2B94594EC88587FBA6333E7E6E389,
	WXSDKManagerHandler_OpenCardCallback_m724A55CD53AC4DA66B2642EC5176E7AD76AF4E89,
	WXSDKManagerHandler_aD_m17E7DE497B0E5B16CAD5839E17002CAD80E6BABF,
	WXSDKManagerHandler_OpenCard_m879E7A3050D3A272E9C733DCB0B112F416C8DCB3,
	WXSDKManagerHandler_OpenChannelsActivityCallback_m25F40676CB710F32EB08795FDC014BD98DB22DBB,
	WXSDKManagerHandler_ad_m2E8B91DC0F5453979F42A8FA979BD7D8F64889EF,
	WXSDKManagerHandler_OpenChannelsActivity_m1F94C917A94F75D5AAA20411770C11AFD0CF2CFC,
	WXSDKManagerHandler_OpenChannelsEventCallback_m0E0FDF81127777780B7DD065B4A6B4F2C660FD49,
	WXSDKManagerHandler_aE_mB07B9389D1E36D473A9F8FF0056F2B6819CF0135,
	WXSDKManagerHandler_OpenChannelsEvent_m65D68A273540E7DCB760A504B941C56B976E31CD,
	WXSDKManagerHandler_OpenChannelsLiveCallback_mA69D1C9CB69C0FD4CCCD5D1F3D2F5847944044A5,
	WXSDKManagerHandler_ae_mB0898F959107D1F49277796467E308622D9E9BCA,
	WXSDKManagerHandler_OpenChannelsLive_mC750A4D4FE4D78DEF6395E03131BC4AB5B38AAEF,
	WXSDKManagerHandler_OpenChannelsUserProfileCallback_m1035C8B4BBF4D785FA16F550FA4AFFDD9C039D28,
	WXSDKManagerHandler_aF_m768DC7A39A2E130351AD39448BDD6ABB29C2EF92,
	WXSDKManagerHandler_OpenChannelsUserProfile_m01DE2A9258CC08B83A9B2BAFCA76A56B2A571D81,
	WXSDKManagerHandler_OpenCustomerServiceChatCallback_m5611CAFF0D03269BBB784ADD5AB4A3D07C0B5D86,
	WXSDKManagerHandler_af_m32B3DDEDB3100A3F527CF7EAB93C3539EAD1EE3D,
	WXSDKManagerHandler_OpenCustomerServiceChat_m3F563644F36227DA258C50ADA80FD1E491389D3D,
	WXSDKManagerHandler_OpenCustomerServiceConversationCallback_mA1AD8D758D5C61BAED04F34181D95522533518AC,
	WXSDKManagerHandler_aG_mDA55B5FE41E86E0C5126E489D2B157CE7AD4BA6D,
	WXSDKManagerHandler_OpenCustomerServiceConversation_m46049B1AB5BFAA9F127C2D835CDDA6D4052FE1E9,
	WXSDKManagerHandler_OpenPrivacyContractCallback_m2A11D62D2F3E7C4CEF325871122060F37B85EF40,
	WXSDKManagerHandler_ag_mAF62D8B3DBA738C49D7DC5D7E0E898872CD9692F,
	WXSDKManagerHandler_OpenPrivacyContract_m01C142E6E0544EEEBC3AAEEE3DEC81CBC1D61E78,
	WXSDKManagerHandler_OpenSettingCallback_mE54A1CD613F0C1252CBB8CEEC7486028F0E4D61A,
	WXSDKManagerHandler_aH_m598E6B661527C370F627E25501E5FFFAA0B3FF20,
	WXSDKManagerHandler_OpenSetting_m9AEC5E0A46C02EB99D90FCDF274203473B40A001,
	WXSDKManagerHandler_OpenSystemBluetoothSettingCallback_m0F9E0B340B947B14FA1FBFF6761C12C06B7460C2,
	WXSDKManagerHandler_ah_m830BA29B87F8CDCBF1831E1E8E51D70CBD16BE65,
	WXSDKManagerHandler_OpenSystemBluetoothSetting_m25775D3702FF2C5991D04CF4693BF57ADE606828,
	WXSDKManagerHandler_PreviewImageCallback_m4F31AD81A378601766B808B43723BCE60500B6A1,
	WXSDKManagerHandler_aI_m23D54F838523AC77170B4DE64CF49126535E18C1,
	WXSDKManagerHandler_PreviewImage_mAD6331B440447C8E3AE6240457116DBE0E29DF04,
	WXSDKManagerHandler_PreviewMediaCallback_mC9A2BE1FAE50F04F4DA5734893C74D36B6B211D4,
	WXSDKManagerHandler_ai_m4A623E10B9C18EB08AC721BFF9735A3D6076391F,
	WXSDKManagerHandler_PreviewMedia_m41EAB743A1FBA5177ADC5883F3CCA50A98E8AEA1,
	WXSDKManagerHandler_ReadBLECharacteristicValueCallback_mC491B3EF0C7CB91AA52820C4BC6D4EFA9E8DBB16,
	WXSDKManagerHandler_aJ_m429364A8D28D50DC6F46CE099E5DEE50E23AE655,
	WXSDKManagerHandler_ReadBLECharacteristicValue_mF53D36B71F88B956DA8C8E59D722DCB00D38BE97,
	WXSDKManagerHandler_RemoveStorageCallback_m8DB4376399791AE842734E0316DAAB159913AA75,
	WXSDKManagerHandler_aj_m71B6136D8820AEE04421F1A9ABEBB8702B880F51,
	WXSDKManagerHandler_RemoveStorage_mCF939B7365C1A11C628CDD8A1BB1077A2A23A8D5,
	WXSDKManagerHandler_RemoveUserCloudStorageCallback_m22BC0AB635F08C25E0E905CB29030739FEBF9269,
	WXSDKManagerHandler_aK_mC2F0B76B236793DB0787316E66B055665C134B4C,
	WXSDKManagerHandler_RemoveUserCloudStorage_mFA4B1131A13BE834A8C4E000576A78EB709F9D2B,
	WXSDKManagerHandler_ReportSceneCallback_mA1C3FA5B9582A84FCCDF0D03FD6B784C91033E22,
	WXSDKManagerHandler_ak_mAC36648F4DEECEA65D762CACE127B261FB51FB2D,
	WXSDKManagerHandler_ReportScene_m6B5F743A26CE7CE787169E7E2D883F30C58A1EE3,
	WXSDKManagerHandler_RequestMidasFriendPaymentCallback_m1B9A8A442CE5C86081ADA2358BC36DF3E7B71F6E,
	WXSDKManagerHandler_aL_m0AE256681ABD8059113DC7976F83399C2C129077,
	WXSDKManagerHandler_RequestMidasFriendPayment_m548211F94007F7A8A83ECF1C42B3A1EAE66AA8DC,
	WXSDKManagerHandler_RequestMidasPaymentCallback_mF1CA3E30E8E5FB9B2806D01F819BB712EEB387DC,
	WXSDKManagerHandler_al_mDCC0C275CF3FB3F917EC7298C8B56EA871655958,
	WXSDKManagerHandler_RequestMidasPayment_m43AED6CC79D8378AF03381D63D4B563C96A0DA89,
	WXSDKManagerHandler_RequestSubscribeMessageCallback_mEADB5B8C1889C3E02B72E4F64052BF3B5FC45FF1,
	WXSDKManagerHandler_aM_mCC21BCCF7AC7C022B29662C4A6F1F9062EDBEDFD,
	WXSDKManagerHandler_RequestSubscribeMessage_m881E791B57DB18DA8A330AFD24482CE5EFBA07B5,
	WXSDKManagerHandler_RequestSubscribeSystemMessageCallback_m16D4CA6E279FC416F183081B934158B93D710B74,
	WXSDKManagerHandler_am_m812664FC348C1867DEC894A0E7559ED31952964D,
	WXSDKManagerHandler_RequestSubscribeSystemMessage_m24A9738F380306EC3C6BACAFC4F3F5591721056E,
	WXSDKManagerHandler_RequirePrivacyAuthorizeCallback_m558DF92ED713BFA2DADF3A9DF5FF1463ADA7E068,
	WXSDKManagerHandler_aN_mC6506C54BE34110E429CF06B96666ED0C46BEACC,
	WXSDKManagerHandler_RequirePrivacyAuthorize_m4418D8959395CB75ED5901255C4AC5DA0A16C4CE,
	WXSDKManagerHandler_RestartMiniProgramCallback_mB5A1D925BFE96D960B7DDB7220C38A46436B2D8C,
	WXSDKManagerHandler_an_mB73BC5FD5C5EF057679D6AA82D125CF20D53E034,
	WXSDKManagerHandler_RestartMiniProgram_mD69C210A3AA74A3C6DE5C51DE5D2D2530F8A548D,
	WXSDKManagerHandler_SaveFileToDiskCallback_mB6415BAEE01704AAC120FA55D96052E4D42685FF,
	WXSDKManagerHandler_aO_m334D8FA00310F476514F6FFD2E4C0BC9601FA970,
	WXSDKManagerHandler_SaveFileToDisk_mC837E5854D4124B82724D30AFCBE48939CC9A320,
	WXSDKManagerHandler_SaveImageToPhotosAlbumCallback_mA66869782E89E66649E83FB21F2CCD51C501AC2B,
	WXSDKManagerHandler_ao_mBD17D0F8BAA772CC40F179925BC5EEE5C43B9A62,
	WXSDKManagerHandler_SaveImageToPhotosAlbum_m84834C7F52857CFF032AC17A7C29D336B917E64D,
	WXSDKManagerHandler_ScanCodeCallback_m7D0672D687D025E8F9B7830D662BB43EE8D05409,
	WXSDKManagerHandler_aP_m99B0907EED3A887E71B7BB373589ABD17A60D230,
	WXSDKManagerHandler_ScanCode_m1D271DFEC44EB0F85FF187F25B936224270DA9BA,
	WXSDKManagerHandler_SetBLEMTUCallback_mD24E3EBC8DDDC485FB571B3278D52B8EF159D582,
	WXSDKManagerHandler_ap_mFA750E2372549E8FC9D920857678FCF6DD05528F,
	WXSDKManagerHandler_SetBLEMTU_m4B28677A6607A80566B197080A4A52DAC7458C3C,
	WXSDKManagerHandler_SetClipboardDataCallback_m7A02783DDA08FFB6ED4F49ABD317FD00F6768739,
	WXSDKManagerHandler_aQ_mD958296311DDF89A0D180F577EA2BF40EF0BC7D6,
	WXSDKManagerHandler_SetClipboardData_m0EBA78E48C4C4DEB162799C78E26E5D591B2E58D,
	WXSDKManagerHandler_SetDeviceOrientationCallback_m44225ABE9A96A03155F82D034649BD74189653F4,
	WXSDKManagerHandler_aq_m1040AF398D2FA3700FB43DC8DE1E6150EAC8A64B,
	WXSDKManagerHandler_SetDeviceOrientation_mD9047CE0C2C7289F9DAF3BD352F7378E59DBD989,
	WXSDKManagerHandler_SetEnableDebugCallback_m8A3AF344579609F4A858E937849169E5304730AA,
	WXSDKManagerHandler_aR_m454AE1DE47021AA9BFC524F7D1AA922C7FBF0659,
	WXSDKManagerHandler_SetEnableDebug_m57134913B18BB65387D8770098C84A4541AA2CFF,
	WXSDKManagerHandler_SetInnerAudioOptionCallback_mB072F0A5B25C3F2D5C2E954E9BC4638D6FA925CC,
	WXSDKManagerHandler_ar_m830874528A28426E9DDAF04E67F51C2AE0F9F65E,
	WXSDKManagerHandler_SetInnerAudioOption_m61F05ED6B8053FB98B8D791B86CD2221D8AC8B8B,
	WXSDKManagerHandler_SetKeepScreenOnCallback_mB85CA0864430FE4933568C062B28ED1B7E19E976,
	WXSDKManagerHandler_aS_m044D5999C8FB3990817E39B77735296028114A11,
	WXSDKManagerHandler_SetKeepScreenOn_m0EF7CB786DFE67BCCD08889872992E702E1BC564,
	WXSDKManagerHandler_SetMenuStyleCallback_mE7FBDF1C8098F1ECDCC3C7D3E6D139C728F8219D,
	WXSDKManagerHandler_as_m01B6D46C51318DB4F0863D5CD0DF808DBDDFE727,
	WXSDKManagerHandler_SetMenuStyle_m23D1761E926A9A31C4A684CB86B7BC030D906BFC,
	WXSDKManagerHandler_SetScreenBrightnessCallback_m429829C64D4A30E10A2FA66D888B6E49F02C1EAF,
	WXSDKManagerHandler_aT_m890F7E9AE6D80B09396C312F2A540C4E2E1300C6,
	WXSDKManagerHandler_SetScreenBrightness_mA3F70E1E6F2B7F29B91E6C42A658F74415AC26CF,
	WXSDKManagerHandler_SetStatusBarStyleCallback_m2882AD70ADC5EC09A37DDCD9DBDDC864C857339E,
	WXSDKManagerHandler_at_m7CECEC27CB76778517AA7F2B3EA79891A0F8F0C2,
	WXSDKManagerHandler_SetStatusBarStyle_m85AEB483A0B9AA9070A39B7CE987D105FED419A1,
	WXSDKManagerHandler_SetUserCloudStorageCallback_mAF70370DB301902F9C6F7562517796FB72045C3B,
	WXSDKManagerHandler_aU_mDFA254A1B2EE639DDEF0A284CA3BED122D1DBCCF,
	WXSDKManagerHandler_SetUserCloudStorage_m551FD6981EE6A203C59EF65CA85C28E6941F0026,
	WXSDKManagerHandler_ShowActionSheetCallback_m41E54935C5D9D8E52781ADA90571A150591D0706,
	WXSDKManagerHandler_au_mE2B972604605771EF3CC324A3BBC736D806EE36D,
	WXSDKManagerHandler_ShowActionSheet_m9CA806108DAA857E2EFCFA165233DFF98FBDC0FC,
	WXSDKManagerHandler_ShowKeyboardCallback_m82B8B6BB776E74B0ACE5EBDCE3CD4C6348194C12,
	WXSDKManagerHandler_aV_mEF02629243EB3CDA47B50D30F6E2DD9B98772387,
	WXSDKManagerHandler_ShowKeyboard_mCA61419A23654C95852AF1171EDDEB375AB0FFA4,
	WXSDKManagerHandler_ShowLoadingCallback_mE0CB3EBE1DF5E89102FF4304E95565D3FB298978,
	WXSDKManagerHandler_av_m2B33F7E25DE05024D1CF47024AC9ABE2901A7733,
	WXSDKManagerHandler_ShowLoading_m7D4341653254E2921698A876C8B30AFB065B7DD5,
	WXSDKManagerHandler_ShowModalCallback_m9805D3009CE775ED8434E49C2F6039E9E6F4C9CC,
	WXSDKManagerHandler_aW_m939BF4E7867EF04A64428C6F587BBFDE62DF988D,
	WXSDKManagerHandler_ShowModal_m9C8A86296D2A2C4C84B3CB5FDD5E925173EE967D,
	WXSDKManagerHandler_ShowShareImageMenuCallback_mBE62DFA5605A03228D1D9C0C97EF6F1F54D93F66,
	WXSDKManagerHandler_aw_mED6C7169B343012E9A7A6AF6F9DFF8CA4D7F5974,
	WXSDKManagerHandler_ShowShareImageMenu_m3281A663A04DE3943690691F0A05985994555BF1,
	WXSDKManagerHandler_ShowShareMenuCallback_mEBD75CE8D92B6B365B7DFE0CE5C3663324E51A4C,
	WXSDKManagerHandler_aX_mAD2BC71FAAA14AD53511CB49F9E53014FD5F4C84,
	WXSDKManagerHandler_ShowShareMenu_m6B0EF3E27690E87CB12BC19ADF07DB2C6A8BB39B,
	WXSDKManagerHandler_ShowToastCallback_mCA1745EC33408A6867AB6C35363FC658C13F4830,
	WXSDKManagerHandler_ax_mBE540F30025A91A8DE593A792DDBCC87FDB51E99,
	WXSDKManagerHandler_ShowToast_m5D91FE337092A6B80F60DDF4EA091EAD21E42BB6,
	WXSDKManagerHandler_StartAccelerometerCallback_mFA1E10663F9500DECBCB122EC8E4B0F510178C50,
	WXSDKManagerHandler_aY_m51B1B02DA707678858FF121607DE4E16C8E525C6,
	WXSDKManagerHandler_StartAccelerometer_m6029E87CC481AD76704D4858856C660F6467668F,
	WXSDKManagerHandler_StartBeaconDiscoveryCallback_m2B26461413E656142AE4D43E24E73C6B3308A937,
	WXSDKManagerHandler_ay_mCEEF66D3D68DE00E93F6408F5688E6BF2732BA3E,
	WXSDKManagerHandler_StartBeaconDiscovery_m16A62D59899AED0C6235C72C3165B0A8DCE83741,
	WXSDKManagerHandler_StartBluetoothDevicesDiscoveryCallback_mDC30C217249BE41B39E627C8504972E43FD64140,
	WXSDKManagerHandler_aZ_mF0B4C42918EBBE36A42361ABD61F670E7AB82DCB,
	WXSDKManagerHandler_StartBluetoothDevicesDiscovery_m21284111786329A15CC9C56BF3F69C5FE8374A0C,
	WXSDKManagerHandler_StartCompassCallback_m14917C9B89D8586BE76769710D5AD9E2DB20E268,
	WXSDKManagerHandler_az_m77F1651CF32B65805EEE787E3E388E95BA9FAEBE,
	WXSDKManagerHandler_StartCompass_m7677D0D399081B1152BE8B013F1A82D10D94EBA8,
	WXSDKManagerHandler_StartDeviceMotionListeningCallback_m3B0505916C54C5C066652366C1B95737CE68057F,
	WXSDKManagerHandler_BA_m76B412ED1D4A1663C03392031C0053447298CA13,
	WXSDKManagerHandler_StartDeviceMotionListening_m5C12590EFF805F4E6A630210A1A043B348EDCC4C,
	WXSDKManagerHandler_StartGyroscopeCallback_m57AAE734C552E695329C2E90ADD03C0B8B48116C,
	WXSDKManagerHandler_Ba_m2A29294E023EE3C1A6CB9E836B3B1AC53B57298B,
	WXSDKManagerHandler_StartGyroscope_mD09CDBB3C236AA9ECB6E1C58366B3998AD5D1B69,
	WXSDKManagerHandler_StopAccelerometerCallback_mBC58386FC9AEA306684FAB02B444B782F2488BEC,
	WXSDKManagerHandler_BB_m9B2707C1B9D1543E7AC1BA1C2581A39DFD8C245A,
	WXSDKManagerHandler_StopAccelerometer_mC5CC6A584A1F9862989D4A083E7C2D0724271544,
	WXSDKManagerHandler_StopBeaconDiscoveryCallback_m1B41F591F788D724F799A8453F9AC92E17DD4A75,
	WXSDKManagerHandler_Bb_m3746D485ACC728E7AA701D1AC760B6AC3BF22281,
	WXSDKManagerHandler_StopBeaconDiscovery_m694133C0CB9B8CD9ECD4E685ED293B095524ACE5,
	WXSDKManagerHandler_StopBluetoothDevicesDiscoveryCallback_m10140A4AB1D670D2B370B43FBCFD14D6C7490B83,
	WXSDKManagerHandler_BC_mC0DB880AD2EF45C2C1458211FE09BBF5E9FAE600,
	WXSDKManagerHandler_StopBluetoothDevicesDiscovery_mDCCB1F188DCB8BD7C570EB8715DD87B9BF7D0049,
	WXSDKManagerHandler_StopCompassCallback_m445C70808BBA42DF3E96E5A0D668742FD2870C0C,
	WXSDKManagerHandler_Bc_m9B0742092E29B2CF27E5E0E985888A90F54CCD80,
	WXSDKManagerHandler_StopCompass_m4C3F27380D7366B3CD42788A90B4DC88296B6B02,
	WXSDKManagerHandler_StopDeviceMotionListeningCallback_m78C29A8000E50BF48EDE52E1AB180A2681785D92,
	WXSDKManagerHandler_BD_m3E7683CEF520ABD10F801888309AE734F03FFFD7,
	WXSDKManagerHandler_StopDeviceMotionListening_mE519D43776DED5D4433CA2A6640A73A9DDCC1047,
	WXSDKManagerHandler_StopFaceDetectCallback_m1ADDF54649022B596ECE01DA71CC940E2A4DC674,
	WXSDKManagerHandler_Bd_m0E9C3F36B6B92BE092BAF59C4B684DE40E7B03C6,
	WXSDKManagerHandler_StopFaceDetect_m1D3409578763AE733D6E7C4F43833E1D69896211,
	WXSDKManagerHandler_StopGyroscopeCallback_m5CFEF42F12A540A0306443DA86A7E5833A014995,
	WXSDKManagerHandler_BE_mA0E1929034FFD0C8544C7379902BB787DFDEFD81,
	WXSDKManagerHandler_StopGyroscope_mAB65414B4ED65F75D00AA3ABD79879765E024ED5,
	WXSDKManagerHandler_UpdateKeyboardCallback_m99DA492898208A33C3901A3C56817EBB4891E383,
	WXSDKManagerHandler_Be_m2B6F8920D94CF7C1D8107584EC123C8E6B4F43F6,
	WXSDKManagerHandler_UpdateKeyboard_m15E469A1C0C6EACF0434F726417563E888EE5887,
	WXSDKManagerHandler_UpdateShareMenuCallback_m4105B59E38DB583FD6AA3642668DF7970CCE2BC2,
	WXSDKManagerHandler_BF_m50BA44FC6B368927AB49C7C5FC5AF919BB9B7028,
	WXSDKManagerHandler_UpdateShareMenu_mCD4EE93D10D5D55CA450C6DDB382437F31080390,
	WXSDKManagerHandler_UpdateVoIPChatMuteConfigCallback_mFE045B790068E6AADCD889D33AF7633EDE3C68CC,
	WXSDKManagerHandler_Bf_m27AEEFE5489277E609EC5BA9D7D9E19A3F17F610,
	WXSDKManagerHandler_UpdateVoIPChatMuteConfig_mF6542F4D31BF06743146FF16B9D7D76FAA92A851,
	WXSDKManagerHandler_UpdateWeChatAppCallback_mFE32E3472CDFE2FDBDF0F133D0858A0885B43EDF,
	WXSDKManagerHandler_BG_mAF09DFD2A0ABB50859A95F13727EA579FD3C0F39,
	WXSDKManagerHandler_UpdateWeChatApp_m8E913CF735CD9654C3250971E76117AE6C4805ED,
	WXSDKManagerHandler_VibrateLongCallback_mE2AD5B2DD3DAEF8C5B6C7286469A366E762E09B0,
	WXSDKManagerHandler_Bg_m81CD9E7279C5C5F3AAE466F0BB16DAC1F78FB47F,
	WXSDKManagerHandler_VibrateLong_mCED342D71124FECAF0DE2AE2C9F3F57A884D8E52,
	WXSDKManagerHandler_VibrateShortCallback_mEF257381799C161567EE72C89C4AF77FE7A48609,
	WXSDKManagerHandler_BH_mD65736730604C709E7A89616510289FDE69A0530,
	WXSDKManagerHandler_VibrateShort_mDB55527776DAD703FD276E80BB5DF71CAB725C92,
	WXSDKManagerHandler_WriteBLECharacteristicValueCallback_m77E68546E32C33FCF5DAFFF49AAD16EED234E7AA,
	WXSDKManagerHandler_Bh_m6EC98BABDBDF64613E0E95E1DE52B306C584707F,
	WXSDKManagerHandler_WriteBLECharacteristicValue_mEA39AA9BE7B9BE8A02B5BBEAC744EA535903055D,
	WXSDKManagerHandler_StartGameLiveCallback_m17E947B73FEA1EF8A0F0EC9DAFA7BB01A15EF0BA,
	WXSDKManagerHandler_BI_m0FF76155388A99A88CEA624AFED350E6699DFD8E,
	WXSDKManagerHandler_StartGameLive_mCE30913AE68EF7F6359C19AA7F6004E099822089,
	WXSDKManagerHandler_CheckGameLiveEnabledCallback_mDB575C1A87780ECF3A88C5F6C2BE19E3D10F5CAA,
	WXSDKManagerHandler_Bi_mE95807ED56A4A1900EFD48A7667DE464818FF80F,
	WXSDKManagerHandler_CheckGameLiveEnabled_m275E2D5E67BFDA9544EA21B3CDC043A57DB061FB,
	WXSDKManagerHandler_GetUserCurrentGameliveInfoCallback_m948932146F8C552DA0BE33A5A69D4ECA08C3AD47,
	WXSDKManagerHandler_BJ_mC1614130BF39BA7D2BEAEFB3F5283421924CD90E,
	WXSDKManagerHandler_GetUserCurrentGameliveInfo_mFF63A375495FC59A0920E260C3D49319AB82888B,
	WXSDKManagerHandler_GetUserRecentGameLiveInfoCallback_m6D0082F6D4F60F3BD67BD06A4483D5C08154DFE9,
	WXSDKManagerHandler_Bj_mCF4EB815BCACD94CD79162ECA624FAA6346D6604,
	WXSDKManagerHandler_GetUserRecentGameLiveInfo_m87DF8F83E23A290CD673729FE89630F346B258DF,
	WXSDKManagerHandler_GetUserGameLiveDetailsCallback_m9D49EF496173BCFE51C292A539A366E325564786,
	WXSDKManagerHandler_BK_m7D78C0AE281E917B5F293B1100CB93F4CAC8766B,
	WXSDKManagerHandler_GetUserGameLiveDetails_m560D1175D69FB71A3789D1A3100815B7F1200112,
	WXSDKManagerHandler_OpenChannelsLiveCollectionCallback_mBEA39F14A7E1B1716615D0E5CC0DE4900A1AC5E9,
	WXSDKManagerHandler_Bk_m9E12CB9F225D5DD1800D9A208FCDAF1BA3528FEA,
	WXSDKManagerHandler_OpenChannelsLiveCollection_m061534AE74AF0E9C18CD7F5EE6152FAA1A036807,
	WXSDKManagerHandler_OpenPageCallback_m5D25AAA1884E0FB2D73197E2A7517C483A2842E1,
	WXSDKManagerHandler_BL_mCA5A88D91E893EB601B1A0A2A6D58AD1AC18B053,
	WXSDKManagerHandler_OpenPage_m29F4F8326135449F71CDE94AA5AE1229DC4DEF60,
	WXSDKManagerHandler_RequestMidasPaymentGameItemCallback_m98EB1644874ED7160CCD4A286DBFF4E13BF8A2F7,
	WXSDKManagerHandler_Bl_m4962B6CA6892ABE00B2FA2686CC10E6E0ACC392E,
	WXSDKManagerHandler_RequestMidasPaymentGameItem_mEFB8083DCD3AC7E758D27D1BA08B5907CDFE351F,
	WXSDKManagerHandler_RequestSubscribeLiveActivityCallback_m4EBEB764BAEEE1CB2F6DFEA56F0BE8195DC69AF6,
	WXSDKManagerHandler_BM_m14A596E409A016D2889A28EE38F2C5A6D3953F15,
	WXSDKManagerHandler_RequestSubscribeLiveActivity_m0EB504744FB6E815E82B48C85958BB4FCEFA7DD5,
	WXSDKManagerHandler_A_m8063A22CD5E13F49310BA73D9854C304ED30E1F4,
	WXSDKManagerHandler_OperateGameRecorderVideo_m0C3CCB8F336A97D6C0FD88F128B577FCA289DE31,
	WXSDKManagerHandler_a_mAD83337C55A5C9500E975129EC78E927F3E65B7B,
	WXSDKManagerHandler_RemoveStorageSync_m9B3ADA16645D897FE0372542DB3098A42D2351C3,
	WXSDKManagerHandler_Bm_m2C17FC847F0CBD07A295FB502225A4D7CE2590DB,
	NULL,
	WXSDKManagerHandler_A_mB2E5D4740EEE6111B3FD29D289547E976CBB796E,
	WXSDKManagerHandler_ReportMonitor_mBEF810B899DBF204084489C9990F7A7960C1B150,
	WXSDKManagerHandler_A_m5D39423859794123075EBDFB4F8A25EC86A3CB17,
	WXSDKManagerHandler_ReportPerformance_m3240E0A04B4319EC08E29871380EED311511963B,
	WXSDKManagerHandler_B_m9532AE805E11D2B0F6C1A6FB15350512254036EF,
	WXSDKManagerHandler_ReportUserBehaviorBranchAnalytics_m391111476972F67677086034D294BB1860B13B0A,
	WXSDKManagerHandler_b_m6F2304C6EEA074E952440E49F6F808EDA60C8D46,
	WXSDKManagerHandler_ReserveChannelsLive_mB38112E6EC48CABFCEE84C9C32D368263ADAC3F8,
	WXSDKManagerHandler_C_m55951192F0BE90A812239A3D4275D83609A2B90F,
	WXSDKManagerHandler_RevokeBufferURL_mAEBB7502F31C52046BBAD2A0AD7C71B6717A8D9E,
	WXSDKManagerHandler_BN_mAD881D912C11D4EB5E8B5AED69C9C61C51A2E4F4,
	NULL,
	WXSDKManagerHandler_c_mB3AADC68401F236A71D9C55E85A0AFCD2707184C,
	WXSDKManagerHandler_ShareAppMessage_mF3DC99CDC5442D18A4AC37EDE142EFD6421F548A,
	WXSDKManagerHandler_A_m9765583FE2B0FCEF4DACBC5E9A97E0627AE6BAF4,
	WXSDKManagerHandler_TriggerGC_m4F074B94563066A91F971F5D6802115344DCD749,
	WXSDKManagerHandler__OnAccelerometerChangeCallback_m8D36E29D08CF3E746DA3B6C02D2C3F1EE71BBA1B,
	WXSDKManagerHandler_a_m0393D8325BD308B5889593D72A73C3685F3AD13F,
	WXSDKManagerHandler_OnAccelerometerChange_m95DB7599156B23BD81629ECCF92AC67935370DFF,
	WXSDKManagerHandler_B_m2B572567FE156100B62F57E762A3C483A8CB8D16,
	WXSDKManagerHandler_OffAccelerometerChange_mB3F5AB389F3CF367B2356108E709ED8C92016639,
	WXSDKManagerHandler__OnAudioInterruptionBeginCallback_m7150D04F8A4325003F81897992B80E4E1F91FC35,
	WXSDKManagerHandler_b_m61CA1EDB92A05386AEF57DD0471FC5493B200ADB,
	WXSDKManagerHandler_OnAudioInterruptionBegin_m37B93870BD8520E3A09E416B41B85484AF3E10B7,
	WXSDKManagerHandler_C_m79F00F36D0DF4B0E1ACB1E9656BA13A654F4B627,
	WXSDKManagerHandler_OffAudioInterruptionBegin_m679CE058630DE1DC887660202144A0F6756AE20B,
	WXSDKManagerHandler__OnAudioInterruptionEndCallback_mD814D3804AB5B484EA13AB5B69698BFB4D1B0653,
	WXSDKManagerHandler_c_m838A2D28EDE65F32E9ED34E7F500B1DD6C99765A,
	WXSDKManagerHandler_OnAudioInterruptionEnd_m34CDFFD52FCAD45F9FF120A67515DFCF5FA69B4E,
	WXSDKManagerHandler_D_m360A3B74D0A88B86C466AAB239B06C7F426235EC,
	WXSDKManagerHandler_OffAudioInterruptionEnd_m92FAD2215EB9A749AD5946D1CB81248CFEBE74F7,
	WXSDKManagerHandler__OnBLECharacteristicValueChangeCallback_m80EA252DBADDCFC6A4622FB7B76A838BCD521F2F,
	WXSDKManagerHandler_d_m6B4DA806C77BCCD63B2E4EE25CC47C82C964B378,
	WXSDKManagerHandler_OnBLECharacteristicValueChange_m14A93308941E99C6DBFD84876276BFEF97DFA714,
	WXSDKManagerHandler_E_mBBB2EFDE2A06BD89C070AC882ED56FCF796622F1,
	WXSDKManagerHandler_OffBLECharacteristicValueChange_mB3737B606FE28472D1FA644086643777E6867EB7,
	WXSDKManagerHandler__OnBLEConnectionStateChangeCallback_m8BEA1F116C643151733483C8B512AFC6F93D65F4,
	WXSDKManagerHandler_e_mA5E3EFCAD88D8B90C0226E729CF1B09186645A48,
	WXSDKManagerHandler_OnBLEConnectionStateChange_m317B880360E55AAF450A1BDF3E5384D8B71FCCAF,
	WXSDKManagerHandler_F_m055172513598F5D5EBA7DEAC7DC7CF2215ABA985,
	WXSDKManagerHandler_OffBLEConnectionStateChange_m978F4917E47AD8D8B8BEFEDB2AD1E1509FAD0F13,
	WXSDKManagerHandler__OnBLEMTUChangeCallback_mB8DE2D55F0C32C241D7A8B439567072D135C27D4,
	WXSDKManagerHandler_f_m2692E5228071ADD95E1828C796CF5B5F7880DA75,
	WXSDKManagerHandler_OnBLEMTUChange_m51FF262987CEB565F9E3088AEA796A2B7AF612F5,
	WXSDKManagerHandler_G_mEB8F8E80837CC1BB2BE89AB59110F3AED4863D18,
	WXSDKManagerHandler_OffBLEMTUChange_mB992D4711F0E4D55D60690387CF19318D2058F86,
	WXSDKManagerHandler__OnBLEPeripheralConnectionStateChangedCallback_m937EFE0AE503B7FEB93BDE8F178C4CA186D36E94,
	WXSDKManagerHandler_g_m940F8C4CC2B017EBEE0205586F34A8FCE31F851F,
	WXSDKManagerHandler_OnBLEPeripheralConnectionStateChanged_m99235F29BD6020602257EBA2ADEE632280ED3E6B,
	WXSDKManagerHandler_H_m8993EA9488A73EC620BC317F39FFFD64946A2D49,
	WXSDKManagerHandler_OffBLEPeripheralConnectionStateChanged_m57AD57ABF486FE9F2A61BFA36B9D650A3B39DD11,
	WXSDKManagerHandler__OnBeaconServiceChangeCallback_mAB118E944AB10AF159654BBC935CDAC5DFE0AD6E,
	WXSDKManagerHandler_h_mA22DC79F1228F6F6A25B83701DDBFF8DE5C307A0,
	WXSDKManagerHandler_OnBeaconServiceChange_m8622BE6B9862C5C7DD65B42A56AE8932C89A8132,
	WXSDKManagerHandler_I_mCD4EB4F48661CC0C30A9D0CD63E41E2080C5B9D1,
	WXSDKManagerHandler_OffBeaconServiceChange_mB4D9FFE589157541DAB9C2C21B39812B69F364AA,
	WXSDKManagerHandler__OnBeaconUpdateCallback_m78B6A27808D84F56B761CE0034AAE55032370E11,
	WXSDKManagerHandler_i_m98D8E2C1908EEDBA75A45F22430C786FC40181FF,
	WXSDKManagerHandler_OnBeaconUpdate_mA7BFAB100D5396D2FC426321A22369D1165508C6,
	WXSDKManagerHandler_J_m326D22FA4FBBCB5869A436EF0B89BE09CF19EE34,
	WXSDKManagerHandler_OffBeaconUpdate_m49BFD222CCB627ED70D06FE5FBB26B738B5C6FB3,
	WXSDKManagerHandler__OnBluetoothAdapterStateChangeCallback_m5722B41983AE7DB9A5742C27BDA18BC4E61A5148,
	WXSDKManagerHandler_j_m44CE5079887122A2FD8C852EEFF23FB8C6FC8C96,
	WXSDKManagerHandler_OnBluetoothAdapterStateChange_mC2567CF20EC0102780A577066DBA8150A4CC3406,
	WXSDKManagerHandler_K_m3C2605B7D252EDB129D8DCAB6245CC97CB080766,
	WXSDKManagerHandler_OffBluetoothAdapterStateChange_mA87A27013F7A2A1F9F1A67F64DFEC463EE393A4D,
	WXSDKManagerHandler__OnBluetoothDeviceFoundCallback_mECD63FCA6A65443A86251F92ABDBDA3BE0A55296,
	WXSDKManagerHandler_k_mA3BFD81BE7C24DB7843131C0B7BA5B93B3622B05,
	WXSDKManagerHandler_OnBluetoothDeviceFound_m6E365D9A5CD8A9BD807390DC62B15780CA275986,
	WXSDKManagerHandler_L_mCD5FB0B67A45CA685B08F50DE17D1B0D028E5812,
	WXSDKManagerHandler_OffBluetoothDeviceFound_mB416E002061B53E9326BF62B60B9ADB9EED2B112,
	WXSDKManagerHandler__OnCompassChangeCallback_mE256130665A67D0A0A580F060F71CAAC5810A164,
	WXSDKManagerHandler_l_m36C3AB84942D6B4D771527B393C80155680B852D,
	WXSDKManagerHandler_OnCompassChange_m7B486E0490AD931422D51CB0EF214C4CC72BF9C3,
	WXSDKManagerHandler_M_mA2A86AAC2C3A8663F7F7FB19BF016AAD4473454E,
	WXSDKManagerHandler_OffCompassChange_mA71818AAF45D9977D26EED9B9274B440E94A1C0A,
	WXSDKManagerHandler__OnDeviceMotionChangeCallback_m88BFADF3DB6632BC20AE656BB82B52041CB6E011,
	WXSDKManagerHandler_m_m83F42FF857E123AE0B66971FF1CF28B10B1CFE66,
	WXSDKManagerHandler_OnDeviceMotionChange_m1681ACC9DD898C0A79230EF41618B4EA861FA02B,
	WXSDKManagerHandler_N_m144F4215AE48D23E6D6E8B9A511A36408F355382,
	WXSDKManagerHandler_OffDeviceMotionChange_m087564EFAB4063A618316CFAFB9ABFD923AC4244,
	WXSDKManagerHandler__OnDeviceOrientationChangeCallback_m8806B0A205D0883BC93B859D99E431AD39EA71C2,
	WXSDKManagerHandler_n_mD7A10C86B6BC1FBEC4A3B3FCF8108783281E6790,
	WXSDKManagerHandler_OnDeviceOrientationChange_m4443B40E3010773178D373A8C9664B22D734CF79,
	WXSDKManagerHandler_O_mC0A9D6D3517172F46D7897C49532CC8A304A1EE8,
	WXSDKManagerHandler_OffDeviceOrientationChange_mFCA26CAEF946D6274605D12F88AEDA75961792F5,
	WXSDKManagerHandler__OnErrorCallback_m6EE828BF1847F666C1F36B545DA59CDEFC6CE2E4,
	WXSDKManagerHandler_o_mD7DDB19031981A0A164AC4EA08CA8B12E02FF280,
	WXSDKManagerHandler_OnError_m4DCB20049CDBB1C47867A076AD2869F17A1D5F62,
	WXSDKManagerHandler_P_m6C58E9E19522AA6CF2ADB558A10805F63F134D7A,
	WXSDKManagerHandler_OffError_mA64741C89A9885A4C6F8067A5359543B2EFC3B80,
	WXSDKManagerHandler__OnGyroscopeChangeCallback_m9C4602D7D1DCA63E67A4413C45692E0AD52ECF42,
	WXSDKManagerHandler_p_mCF04FE4C687EB307CE090E89F59A025CAE5EE0EC,
	WXSDKManagerHandler_OnGyroscopeChange_m92E80BF92E0EE85D5E8E3EFE1C9A2A926F410012,
	WXSDKManagerHandler_Q_mE3CA348E0B9BE2C2DF7E7DCD418EA9C6AD73A4C2,
	WXSDKManagerHandler_OffGyroscopeChange_m983D798E9E61F484F01326B96ED411D85CAA6C26,
	WXSDKManagerHandler__OnHideCallback_mE46835F385B8C2AAF48C080429EF1EF664CFDF33,
	WXSDKManagerHandler_q_m27A7B290404CE871F862F818B128EA4C22DE619D,
	WXSDKManagerHandler_OnHide_mE0295CD2EA6DA5879C628E5D27989EA5EC043F6F,
	WXSDKManagerHandler_R_m29DFF9C4EFC841DF6DF4F85450F002336EF86BFC,
	WXSDKManagerHandler_OffHide_mAC4467DE278EE929A8CDC12CBF8C6972D8408166,
	WXSDKManagerHandler__OnInteractiveStorageModifiedCallback_mD7787C14374B7A46B2B50E03665E3FAB2AF9097F,
	WXSDKManagerHandler_r_mFB2E8F675A3C088A23AB53E4AEDAB1FDB46ADB97,
	WXSDKManagerHandler_OnInteractiveStorageModified_mF69CF81C349AFF3D678EE295D065069504A37974,
	WXSDKManagerHandler_S_mC2037D081B92308FC6B9968F8D485B56AF098C01,
	WXSDKManagerHandler_OffInteractiveStorageModified_mF7B7C717DC4F86363A5197177E620722A6FC8478,
	WXSDKManagerHandler__OnKeyDownCallback_mA02938C86252798B66B9A57A2433A6BE0F1A87FC,
	WXSDKManagerHandler_s_m794DA6C96AF931FF63C074D67BC5A04376692444,
	WXSDKManagerHandler_OnKeyDown_mACABE6824786A3B40EEEEABFD4A268DCDA092EF0,
	WXSDKManagerHandler_T_mC7551D4E7DB803F4470E7D9997F45EA720ADA24E,
	WXSDKManagerHandler_OffKeyDown_mA56F564EF9255EB159A83F46875FAA7C6EBEA884,
	WXSDKManagerHandler__OnKeyUpCallback_m3B1ADA5304FCC524537A2B0D9A6262539A64E104,
	WXSDKManagerHandler_t_m21CA3D308A82BD68A3E371A0DFF7C83BD6A4B2C4,
	WXSDKManagerHandler_OnKeyUp_m0657008CEB0F105B67BED72BFA59CAFBBE7C70ED,
	WXSDKManagerHandler_U_m671E1BB0A594AE06F4A37E592CC674520E88BE6A,
	WXSDKManagerHandler_OffKeyUp_m749301A0E38909F9358D42C819A4B61102D4998F,
	WXSDKManagerHandler__OnKeyboardCompleteCallback_mCCA19500F62D7E3981AFBB45BBC540A9D5292FFC,
	WXSDKManagerHandler_u_m0114F53AFFCA6F668CE874BA529B5BE0FEFE7A70,
	WXSDKManagerHandler_OnKeyboardComplete_m66E8D797499A8A5B9EFF81516211CD96B82D2C2B,
	WXSDKManagerHandler_V_m80E1EBBA98C58A09ED12614ECFD95AF5C62D8F68,
	WXSDKManagerHandler_OffKeyboardComplete_m8EF21F86FC0134727D95F835680C295B232BEFFF,
	WXSDKManagerHandler__OnKeyboardConfirmCallback_m1930F91E732AF0D4B417577511498864BBAB9715,
	WXSDKManagerHandler_v_m54046C5AE17653497A2FD579DEBC85A7CBB1077D,
	WXSDKManagerHandler_OnKeyboardConfirm_m53678BCEDBAF96EF125A25F078B673C1D72C596F,
	WXSDKManagerHandler_W_m231559870983F6B0E2F14E6B913398566841418E,
	WXSDKManagerHandler_OffKeyboardConfirm_mB8DFE3AEA79B8DAEA275B76E0216A67D3B7493D0,
	WXSDKManagerHandler__OnKeyboardHeightChangeCallback_m148D4908D304C88EA0060E366D10B0122143346C,
	WXSDKManagerHandler_w_mE82965035247638AEDDDE1C2E4F26437EFB476D9,
	WXSDKManagerHandler_OnKeyboardHeightChange_m91A752B9FEB320FDE68814B0D7AF55A2365BF46C,
	WXSDKManagerHandler_X_mF59EA06F648838BDAF7C11D4F8E15418B2D2D053,
	WXSDKManagerHandler_OffKeyboardHeightChange_mEF72404DF61DA563E72B8264242C387C2935A885,
	WXSDKManagerHandler__OnKeyboardInputCallback_m2D3E42D1EC10EFB209B8A343B5C3BD6A27F289D9,
	WXSDKManagerHandler_x_mCDE16AAD668994A8340B76591A29BE9BC1EADE16,
	WXSDKManagerHandler_OnKeyboardInput_m62A9277797AAE1804822A949D64A9A4693E61329,
	WXSDKManagerHandler_Y_mEE01E236014BCCB3D80E81D51E7DE26270847892,
	WXSDKManagerHandler_OffKeyboardInput_m8438E36876DA04E1F0279E1E92DF0896C1B115EB,
	WXSDKManagerHandler__OnMemoryWarningCallback_m06D1C80E97BE2B37A67994AB6F6BD8FDF6C8408A,
	WXSDKManagerHandler_y_mEACF022CFBBEA92FB3EE1BC6207763F426AD6DAE,
	WXSDKManagerHandler_OnMemoryWarning_m5435D411727CFE96F30DF65DABA788365924675F,
	WXSDKManagerHandler_Z_m8538EDB0DD72477145AD8A5BF22F8FB3645D84F1,
	WXSDKManagerHandler_OffMemoryWarning_mA3C734B5E6FE22DA7D50B5A7EA3388D3A411BA0E,
	WXSDKManagerHandler__OnMessageCallback_mB501DCA2C6A4087FF8E232DBA33CE6BD80A2709E,
	WXSDKManagerHandler_z_mE6A28A440FC91134A9E7F9A3FF8A945BC9B304B0,
	WXSDKManagerHandler_OnMessage_m66553AA3D278D68AEBF68FC0E6A092C8AFF85C3D,
	WXSDKManagerHandler__OnMouseDownCallback_mD2AA5DC82737C650360B5B95A2607EEB22039455,
	WXSDKManagerHandler_aA_m8133CD74F7888EC8CC009AC2C741084881603CEC,
	WXSDKManagerHandler_OnMouseDown_mA74E84744C7E19ACFE30752CCAD27C280FDFCD6C,
	WXSDKManagerHandler_aa_m643C49D87A28A9687AB81D7592A4CD8385B97574,
	WXSDKManagerHandler_OffMouseDown_mD5B09949A20E497A779521571F49998A77FE7393,
	WXSDKManagerHandler__OnMouseMoveCallback_m1322B38E89690638E46F71EEF529B60FB1AE7E21,
	WXSDKManagerHandler_aB_mCE5EE5E29EA4FC042B3E6A3E436EB384E8620B4C,
	WXSDKManagerHandler_OnMouseMove_m9AC72FF5F1B0183AC4ECE6AF9DE644EE0742FD45,
	WXSDKManagerHandler_ab_m6F751F13D8353456E21767897C83C2ABDD22543D,
	WXSDKManagerHandler_OffMouseMove_m69B39205D7BDF931917DB5398D9E87C88F32EF1D,
	WXSDKManagerHandler__OnMouseUpCallback_mA3B7EE65F6B2A8DB5D43A1EF88F3C9186C45326D,
	WXSDKManagerHandler_aC_m6938B24887B5042FCDD55D032F484A9C750D9CB0,
	WXSDKManagerHandler_OnMouseUp_m561420B3C664A5823AF0C09D4205586B0C9045F3,
	WXSDKManagerHandler_ac_mAA51313F1DC57F7914CF7253E81D7D9AED41E572,
	WXSDKManagerHandler_OffMouseUp_m97C38EBAAB8DBD8FC16E41794DF528446CC13368,
	WXSDKManagerHandler__OnNetworkStatusChangeCallback_mE0272AD6C05165BDD36ACBE3F9C0CDD89D282E0D,
	WXSDKManagerHandler_aD_m7E36EAAC510240636EEE4E8656A628A738969B76,
	WXSDKManagerHandler_OnNetworkStatusChange_m9F3B564BA8A51AD9BCF25D386C567E1D0649488B,
	WXSDKManagerHandler_ad_m85AA747D74D6C7D147109167FD0C58D8259F0FA7,
	WXSDKManagerHandler_OffNetworkStatusChange_m9D158BA4322DD5EF9C451FFF9BD7B302E0D19273,
	WXSDKManagerHandler__OnNetworkWeakChangeCallback_m295999961C89CE29772CDBE3D906D835E34F363D,
	WXSDKManagerHandler_aE_m2FCC0675457269513E1AFDC2CEA25F0DDD5FCFFD,
	WXSDKManagerHandler_OnNetworkWeakChange_mDEC850E3CC170D80CF1F4378A55A5196CE2BCCBF,
	WXSDKManagerHandler_ae_m9D3418D0BC0B483CA863B8AC99459FC25D4124A7,
	WXSDKManagerHandler_OffNetworkWeakChange_m7FBC836A2090B1A8756246238EF0CE2A672ABF66,
	WXSDKManagerHandler__OnShareMessageToFriendCallback_mC424F155EF5659868A0FDD6C7D492C502DB9210B,
	WXSDKManagerHandler_aF_mB23EB190411FB222E4047DAB2722BA57004FB1FF,
	WXSDKManagerHandler_OnShareMessageToFriend_m8D8C2C51256081BE68EC3262C9501F2014222E7A,
	WXSDKManagerHandler__OnShowCallback_m3175A889587D7F7FFEF8F2B88ECC138DFC0B88D8,
	WXSDKManagerHandler_af_m9E2A1BB9879033F71B8D554474B011ADA848BC34,
	WXSDKManagerHandler_OnShow_m9DE2965371D03BDC11B95EFE19A20E09292F0D5D,
	WXSDKManagerHandler_aG_mD2868816EC0802C009D82416E4F66331B777E3F5,
	WXSDKManagerHandler_OffShow_m5608BCB9EB4289F6B30A4805BFF8B4F953EA3201,
	WXSDKManagerHandler__OnTouchCancelCallback_mD2BCB6A7CE87B5B0593E446718D69C9C2B935D45,
	WXSDKManagerHandler_ag_mB884D480B7E781FEF8074F11BE99D9F86209A4B3,
	WXSDKManagerHandler_OnTouchCancel_m42ACACCEBE7FDEC18561631FE6515FA18B455CD8,
	WXSDKManagerHandler_aH_m4A643B3FA64C2F7AA67D7B23F2513B9FAA05DF7D,
	WXSDKManagerHandler_OffTouchCancel_mE9BB34BECD58A18BD3873E2B54E1938F7211450C,
	WXSDKManagerHandler__OnTouchEndCallback_m6502E8F383A41F57B7F0A236599750DD57D91B9E,
	WXSDKManagerHandler_ah_m2EFC560C37B72BEA3D6593A71077365DD1D6EBB1,
	WXSDKManagerHandler_OnTouchEnd_m66D8F9DA36330956AB724F675AA90CF7F1C8389D,
	WXSDKManagerHandler_aI_mAD755466D841E547CFBCF81474D8D506EFE53B69,
	WXSDKManagerHandler_OffTouchEnd_m7A73754363DC4A24CAB3C029D287178524F43FBD,
	WXSDKManagerHandler__OnTouchMoveCallback_m9B8986C61D862C306BCABD7FEA468A652CAB9288,
	WXSDKManagerHandler_ai_m34B97A9CD32D9988184F829D79F8F82AB7C7EEB7,
	WXSDKManagerHandler_OnTouchMove_mA50021CA5E9E9313B851ACD5E9327F4207BD4A9F,
	WXSDKManagerHandler_aJ_mD7B6BF703E78AC921C9A38816FFF33F95069E206,
	WXSDKManagerHandler_OffTouchMove_m1B383FFA68D86BD384B36FE9FB6962ECF1A5A209,
	WXSDKManagerHandler__OnTouchStartCallback_mC7B8FA04783D908C3ABAF773664439218FEBC1BF,
	WXSDKManagerHandler_aj_m3EF64051F689A9F8553A2BB8774E3600ECA5285E,
	WXSDKManagerHandler_OnTouchStart_mD53382040A38D0E576BCD7E9A5F5600093251D1B,
	WXSDKManagerHandler_aK_m940161F17383C4934669BBEC98EECBA633D5729B,
	WXSDKManagerHandler_OffTouchStart_mA850745FBBFCB67D814200EE6834A5AF64FDE8E6,
	WXSDKManagerHandler__OnUnhandledRejectionCallback_m218EFE0F3F4FB600B9B213A110DA9E4805A7076E,
	WXSDKManagerHandler_ak_m48BF64C1A649DAB0179A3BAD54126AE75DB56FE4,
	WXSDKManagerHandler_OnUnhandledRejection_mBDE99E127FBAF9996CE4D61268D5C2CE56EE882A,
	WXSDKManagerHandler_aL_mE29798398E9FE74CE290AACEFBBEFA9C8282B20F,
	WXSDKManagerHandler_OffUnhandledRejection_mC0A11ACED43E08D9710AD3BF07FF4D1A102B8EB0,
	WXSDKManagerHandler__OnUserCaptureScreenCallback_mC1D74C57301BC68FAC65439B624F26EAFD3F1247,
	WXSDKManagerHandler_al_m2C82078DD71DCC7D58C85D6DC9E28E2D38772445,
	WXSDKManagerHandler_OnUserCaptureScreen_m0AB1B9A5436DF3F8F307FA6CE22F1E5BF919F0DE,
	WXSDKManagerHandler_aM_m01A532F5A8C328764E379174C9CCE7705CB825A7,
	WXSDKManagerHandler_OffUserCaptureScreen_mB19A73BE4517CEC412236B313ABF7834E4EC4072,
	WXSDKManagerHandler__OnVoIPChatInterruptedCallback_m10A9B470C560F1DD4E9D43208C383713C06AE8CA,
	WXSDKManagerHandler_am_m453D7C798FD6B85DC0DDCDB68C7316959D313457,
	WXSDKManagerHandler_OnVoIPChatInterrupted_m7EA362B4EFE4B958446DCBC0B859C8540B43DEBC,
	WXSDKManagerHandler_aN_mAE87CB23E67934BE54DE86BC137DB6CAFC74B305,
	WXSDKManagerHandler_OffVoIPChatInterrupted_m56301CF5CD64BC41CFEC53C93D3EECAFEFD812E5,
	WXSDKManagerHandler__OnVoIPChatMembersChangedCallback_m0B501AFEF8529B559444309C2153AB043E96357F,
	WXSDKManagerHandler_an_m13E2B525562A8694883994EAC5420E3D4D90BA63,
	WXSDKManagerHandler_OnVoIPChatMembersChanged_mCDB00100D9009E7AB92C6090D83E5A8734C530B7,
	WXSDKManagerHandler_aO_m387EB50209ECFDFDA55BBE64D9607895785D5FB6,
	WXSDKManagerHandler_OffVoIPChatMembersChanged_m37F8D37370831D12A327A1302DC77F95903ED1ED,
	WXSDKManagerHandler__OnVoIPChatSpeakersChangedCallback_m8E66B07DEE04D10F1B6C854CF8012F80132486F5,
	WXSDKManagerHandler_ao_mC9283AB0C18F01D6B2F2DC675E9D841F6DD38A2E,
	WXSDKManagerHandler_OnVoIPChatSpeakersChanged_mC93117666F01013AF53E0541C6B8307574777DE5,
	WXSDKManagerHandler_aP_mDE46D2D1D3D93E4918D0A2861DC4266E5CBFD5E4,
	WXSDKManagerHandler_OffVoIPChatSpeakersChanged_mBE47054258FFE22487BF9749DE26FCC537E424E9,
	WXSDKManagerHandler__OnVoIPChatStateChangedCallback_m04C0DCA4969EDA1F08DB832C0C124748CC0A0630,
	WXSDKManagerHandler_ap_mBEF20844EF7B71BF11E9DE133B44E6E5BE98A8C2,
	WXSDKManagerHandler_OnVoIPChatStateChanged_mE9893ABEC2B8F58397721472C40278C8D5028531,
	WXSDKManagerHandler_aQ_m3779B23550EE7A40CF1805403E5368954F0F8C70,
	WXSDKManagerHandler_OffVoIPChatStateChanged_mFF30DADAEC4C2D359B97A80282186F207AA67F3C,
	WXSDKManagerHandler__OnWheelCallback_mC7F6B1D2A6FAC7A6191040B400FBD5EDABC3AF42,
	WXSDKManagerHandler_aq_m6B94FA406E387933B9F9E3668D60AA4380694B34,
	WXSDKManagerHandler_OnWheel_m20030B80167760F6A082DA79C6EEA32CBD5231AE,
	WXSDKManagerHandler_aR_mE669CE38A57EC77BD8AB7A07694C190BC946FCDC,
	WXSDKManagerHandler_OffWheel_mD039BCE1155F9F50A90214FEC630E7B3CBFB77C1,
	WXSDKManagerHandler__OnWindowResizeCallback_m8BB992E407F89A60301D6BAC39636DB44EFFB0BD,
	WXSDKManagerHandler_ar_m9BA5873785244D72F439DCFF88277F03417E98B0,
	WXSDKManagerHandler_OnWindowResize_m727AD53CD53EE46485AB4CF2E3B18D22C69C7E70,
	WXSDKManagerHandler_aS_mFD5E61AD92C1A72BF5F7B6A3211713E48E41D590,
	WXSDKManagerHandler_OffWindowResize_mE2705B8986577AD6C8C5438C5B6318CBE7328467,
	WXSDKManagerHandler__OnAddToFavoritesCallback_m2C1095C7DA27E6FA04AEDCF8FD08065831669480,
	WXSDKManagerHandler_D_m94A72FFFB712C05E51AA33064EBD630B68434DCD,
	WXSDKManagerHandler_as_mA4421F9435177771E1D5740AD03044C681D9EB22,
	WXSDKManagerHandler_OnAddToFavorites_m75F170D82F754AD9D0A9FB6648678D6087420929,
	WXSDKManagerHandler_aT_m125E851AA77A19A3D53045DF883F8040D0F414D7,
	WXSDKManagerHandler_OffAddToFavorites_m177A5345C7B4BBB0D805E199B52A64395C207007,
	WXSDKManagerHandler__OnCopyUrlCallback_mCEA60FD7C8C4166B2544BA4898DFC8E1B856A677,
	WXSDKManagerHandler_d_mA11F3D969B8155C33256D93268ADF20DD8613F36,
	WXSDKManagerHandler_at_m6C027FAF694FAF703C74AB224D76287CA180B584,
	WXSDKManagerHandler_OnCopyUrl_mF072168F2C7571EB3454E0461DDAAE4193920813,
	WXSDKManagerHandler_aU_mE65DBF4313D2869D31851FE4DDE3E58F48AB25BF,
	WXSDKManagerHandler_OffCopyUrl_m1081939215289858CFDDCDC07787F713E75B3135,
	WXSDKManagerHandler__OnHandoffCallback_mC8E849BFCBFF49135E2D13235C77A2565B17DED9,
	WXSDKManagerHandler_E_mFFC9EC75C6FAA47B3C3F9DC14D40780816DF0F87,
	WXSDKManagerHandler_au_m8C9F23184E26B2888E65814B17A4CF964234714E,
	WXSDKManagerHandler_OnHandoff_m9CEC8E2C9A011CA073C172D3CCA7A104A2ABCA36,
	WXSDKManagerHandler_aV_mA3659C5249BAAEA007988EAC727E46BBA49B32E4,
	WXSDKManagerHandler_OffHandoff_mC18A7AC7827E65A0EDF5A228740782D6D3514587,
	WXSDKManagerHandler__OnShareTimelineCallback_m6B7265747820970A06F0B7CA21E58B6C1A6A0515,
	WXSDKManagerHandler_e_m6D1471597EEFD934FDC6034C0F020447217EA2B2,
	WXSDKManagerHandler_av_m9A381633354D9177CDC992DBFBE08AF06BDFAC2B,
	WXSDKManagerHandler_OnShareTimeline_m29277BD27BA1627ACCAD1B9EF82E9DBFAD9AA917,
	WXSDKManagerHandler_aW_m4D2F3D2E5305E92AB7C4ABA3EF181307DA1BDF15,
	WXSDKManagerHandler_OffShareTimeline_m039B0E12D6EF077566FD5A9E64B4FBFA8939C55B,
	WXSDKManagerHandler__OnGameLiveStateChangeCallback_mE294223F3B90395B69771795A299B67B35A34D41,
	WXSDKManagerHandler_F_m48933AC2CA60E93B671D9ADC45046B6780113637,
	WXSDKManagerHandler_aw_m76DF8F7492D055D89176CA023215D0E4C02BAC19,
	WXSDKManagerHandler_OnGameLiveStateChange_m17BED6C21A8DB97BB36E89E8B25E5888D6333733,
	WXSDKManagerHandler_aX_mC07E6E71F2EC6AA643963AF987DB938E0A341E6C,
	WXSDKManagerHandler_OffGameLiveStateChange_mCDAB90677DD28759CBD6CB1F03EADCA902BEB6DA,
	WXSDKManagerHandler_f_mAB0BB4D9B2B4F482DFECD83A38F31E593257D5F6,
	WXSDKManagerHandler_SetHandoffQuery_m60702737A5A59486AB4EB292519219015E25C7BD,
	WXSDKManagerHandler_ax_m9370B4A92764B07923421DA401D2F5E42EAFE243,
	WXSDKManagerHandler_GetAccountInfoSync_mFF08BB09D27DF37EB52EF135B2B244A7D1D28F3A,
	WXSDKManagerHandler_aY_m71E2B992572B57B36C1907432485953B9B20F389,
	WXSDKManagerHandler_GetAppAuthorizeSetting_mA6CA329EB46BF23B6D8BFF65FFF64A94EDFD49E6,
	WXSDKManagerHandler_ay_m9A51409DB9FE2DDC1751730F16D9FBE259FF4987,
	WXSDKManagerHandler_GetAppBaseInfo_m2A313630B973FC497D2F3E404E86792ECA64E410,
	WXSDKManagerHandler_aZ_mCB4644EC663E5643A176081E037D5E470D914F05,
	WXSDKManagerHandler_GetBatteryInfoSync_mC5A7ADB246B86F5A271C10CE475D2FEAD88EABFC,
	WXSDKManagerHandler_az_m2D19DC9A92E45532F22B5B10865CEC2E60E6EAD7,
	WXSDKManagerHandler_GetDeviceInfo_m408C9EBF56C0C4A61CE919F63D0CE6128FECB4F3,
	WXSDKManagerHandler_BA_mFF0375C24C4A379A718ABD5246E55B45AC0652CD,
	WXSDKManagerHandler_GetEnterOptionsSync_m5587BAA90B74D49B7601C624E6B2594C2A6AE494,
	WXSDKManagerHandler_G_m7C842238F813C7881ECEB0872B3267481E5070E1,
	NULL,
	WXSDKManagerHandler_Ba_mA42B08BC199BF1C8A3348BA50ADAD7667852271B,
	NULL,
	WXSDKManagerHandler_BB_mA7D8AB58CC4CCF79B6BA00BDB63CF8011178F471,
	WXSDKManagerHandler_GetLaunchOptionsSync_mAC55F3A9F8408ECD5392E8FB9B6AF8D80C79F8C2,
	WXSDKManagerHandler_Bb_mD29A79A70D58C0CE167050D8DBC4EA9D6229514D,
	WXSDKManagerHandler_GetMenuButtonBoundingClientRect_m4FE5CBC2348C85D0900FE9BCC7968C81B062729E,
	WXSDKManagerHandler_BC_m45EF62780D4EE4AA61F408D401975B90D32867E1,
	WXSDKManagerHandler_GetStorageInfoSync_mD7CE2F4A6F82321EBCD30C392F275C8AB940A0FF,
	WXSDKManagerHandler_Bc_m2022D1DD116C4FAA1B411AD98E2B2D36E5885E13,
	WXSDKManagerHandler_GetSystemInfoSync_mE5F735E2AA5777F1A17E8BA546FC1B3626B781D3,
	WXSDKManagerHandler_BD_m369B7159EEC313C5AC6BB96FDAC2F5B4BF59A4A5,
	WXSDKManagerHandler_GetSystemSetting_m0449246A75894E54D169011F740C37BCD2DF5FD6,
	WXSDKManagerHandler_Bd_m2F601250613DBDDF16001F64EFD3F9C6038FB4AC,
	WXSDKManagerHandler_GetWindowInfo_m65147FCE2550CE68675889625FBF0DA618B235F1,
	WXSDKManagerHandler_BE_mC40D21D5E56784AEB234C79EFD189767F881C079,
	WXSDKManagerHandler_CreateImageData_m2FF44E296F64EE60C48C6E77D3668057B4B122AF,
	WXSDKManagerHandler_Be_m3B8F2A948710F43F7DBC1F2D479AF5413D368B84,
	WXSDKManagerHandler_CreatePath2D_mE673C2E3CB90F4431D0997AFCD62A8CBA8A57DA0,
	WXSDKManagerHandler_A_m7FBCB19D38749C659D4F4C7A5D3B46F5C2DD5590,
	WXSDKManagerHandler_SetCursor_m6463B04A94A78E8D3D71053AE2008DC97E6063C9,
	WXSDKManagerHandler_g_m674F785AF3C448DB00CB3BE7B5F8DBF1389E4CDD,
	WXSDKManagerHandler_SetMessageToFriendQuery_mB7C62BDF2EE47CB633D05125EFB9E882E1994CA3,
	WXSDKManagerHandler_H_mA272CA1A475F9C5CE40FDB0A0A24451323A59280,
	WXSDKManagerHandler_GetTextLineHeight_mB0AC80E753AA02970EE6910170E0F5EA5238333C,
	WXSDKManagerHandler_h_m59BC35C0D91E11FD0996542044DB844F575F2B3C,
	WXSDKManagerHandler_LoadFont_m79B315A6C735569E4F5583AAB4A06E47D9469365,
	WXSDKManagerHandler_BF_m2A4BACD5338FABA148858E5F01462E2857778317,
	WXSDKManagerHandler_GetGameLiveState_mB4BDFC1495421051160503A099E64683DB888889,
	WXSDKManagerHandler_DownloadFileCallback_mF1300D06806FBE72CF81A3F60C8E73704ACBE36E,
	WXSDKManagerHandler_I_m296301E810DB23733AC5A6C45ACC8EE0949FD3D2,
	WXSDKManagerHandler_DownloadFile_mD14A209EF680229ACC649B3D8A143834A3C525B6,
	WXSDKManagerHandler_i_mF9E7E59A01739567121FEC7397C1139E89B3CE37,
	WXSDKManagerHandler_CreateFeedbackButton_m55B9F49DD32ECF32961B734CA33560C5AC648FC8,
	WXSDKManagerHandler_J_m8BF865E591140F16D13804DCB33DE2E264F107F8,
	WXSDKManagerHandler_GetLogManager_m2CD9433A2E564A78A2CA2AB30E028237E14CE586,
	WXSDKManagerHandler_Bf_m802ABA4C73097896D37B31B5584596B17ECE1D89,
	WXSDKManagerHandler_GetRealtimeLogManager_m96D409BAAFA1965FCE6FBEDBB55CFA0E337F93F4,
	WXSDKManagerHandler_BG_m879E8DDD4C7DFEC9265D8D1DF2879D2E040DF688,
	WXSDKManagerHandler_GetUpdateManager_mA90AE929807C8DAE6CC676DB8A1F2413BF645270,
	WXSDKManagerHandler_Bg_m9DA1858E5BCC64A5C30864948A540FB8DCC21FD8,
	WXSDKManagerHandler_CreateVideoDecoder_mD5BB268C66A5BDB39BB5537CD2F5D88402E1B5B6,
	WXSDKManagerHandler__DownloadTaskOnHeadersReceivedCallback_m62FAC242EFCA9AD135081367C64AB03C28354894,
	WXSDKManagerHandler__DownloadTaskOnProgressUpdateCallback_m5DC4ADF8BAA07802882C28281DC9A2232F94FD71,
	WXSDKManagerHandler__FeedbackButtonOnTapCallback_m89D8E914CA3266A8842AE8D98F0E72409FD1B740,
	WXSDKManagerHandler__UpdateManagerOnCheckForUpdateCallback_mE8854664E602D0401E8281C2099505D2E68701BD,
	WXSDKManagerHandler__UpdateManagerOnUpdateFailedCallback_mE32DD60B80D194BB47EDE59FA4CA374AA34155DA,
	WXSDKManagerHandler__UpdateManagerOnUpdateReadyCallback_mD26D2C1AEAE708311F44E26F7E32C33FDAEDC233,
	WXSDKManagerHandler__VideoDecoderOnCallback_mC93F8FF4BA4EECA438995020FF8C08947CCE697B,
	WXSDKManagerHandler_get_Instance_m9A42924F92A0528D0CD33E2850919A6FB487724B,
	WXSDKManagerHandler_get_Env_mBE0E891F62EBAAC71A86AA09E5DB8791CD21DA8F,
	WXSDKManagerHandler_get_cloud_mB357BF814941C3C5EA518B8FFDD1B8415269C861,
	WXSDKManagerHandler_OnDestroy_mA57B04F1A52D5DD1ED2D3090D815019472666E9A,
	WXSDKManagerHandler_j_mB8C3DA976838CA2B93F354538EAE8B781CBD5CC5,
	WXSDKManagerHandler_A_m0D13EE3A820E18A7528391E3929B9711E7843CCA,
	WXSDKManagerHandler_a_m9E0940AF1AAF023535D884D5CB430A9D7B62474C,
	WXSDKManagerHandler_Bn_m3207B710E21DBABEA5FDE5D9F1E54EBBE46DE2F6,
	WXSDKManagerHandler_BO_m2872864EB34198DBF833487296E3B42059C639C2,
	WXSDKManagerHandler_A_m21FDB376953E430ECF1F609CA2801FEB0ACDDF66,
	WXSDKManagerHandler_a_m8B078CBB9660F4916896A4C31DBE0D51702B9DE6,
	WXSDKManagerHandler_BH_m887BCA45C0F7E9A3AE573F040C1251A51EBBD41F,
	WXSDKManagerHandler_K_mEB2F70EFE723F76222786FE6D0DC1CF9B310FCEE,
	WXSDKManagerHandler_k_mC4B3C440CAFAA3A5D3BD138B2E777E1A2B58671B,
	WXSDKManagerHandler_A_m45840940368866E116227F96B9AD61D67C196980,
	WXSDKManagerHandler_A_m7EAEF869C9347741B4389A6322FAC74CFB9F5F9B,
	WXSDKManagerHandler_L_mD6A78C0D5E4935373CC7085297B7A93E7AD10F56,
	WXSDKManagerHandler_l_mE4FABCCE36800D8B44BB2F087732BE04CA55111C,
	WXSDKManagerHandler_M_mB9EB6342DC765D1C6382528BADFA71DEE2F9665D,
	WXSDKManagerHandler_m_m035A5CA42CE1F0974D217E027BFF05CEEC69D42D,
	WXSDKManagerHandler_N_mDBA4E3278B754B033B901644F12B9CD9E5A40C2B,
	WXSDKManagerHandler_A_mB330FAE4DC1F34605317A53721F2AFEE509445D9,
	WXSDKManagerHandler_A_m0F85FEC3A96392F6F13D297FBF1684DB579F5FB0,
	WXSDKManagerHandler_A_m0404BE83541AB1B76E8D19D4CD80F1374647D17E,
	WXSDKManagerHandler_a_mD3414510327ABA9478094F915F1B1449D40287A6,
	WXSDKManagerHandler_Bo_m2F54340FCA384FBE2E36E583C8C68088FBD0AD53,
	WXSDKManagerHandler_n_mED9D0B1266B0A3F19C6820ABD8600808B09ACF40,
	WXSDKManagerHandler_B_m18FB8D45C5FF4E9138A54041A777C2BEC1C8EBD6,
	WXSDKManagerHandler_A_m5FF7AC24223B313FA196CC02942BCCF98B565203,
	WXSDKManagerHandler_O_mF1163C07BD54A24841F927BCD141E0758EA2B34E,
	WXSDKManagerHandler_A_m7204732D37530F46A905C40DC716F459D26D60C8,
	WXSDKManagerHandler_Bh_m794F1A7D18AC189A06AC3FC88FA36A7F34048F19,
	WXSDKManagerHandler_BI_m5CAF304723D73DDE36774C3DE53922F9B60CE713,
	WXSDKManagerHandler_A_mFB638B0F64EF825E5D6EED999243F67F7187A5C2,
	WXSDKManagerHandler_o_m348A4914F1B6A379E5E099F10D7D3D101C38FD7A,
	WXSDKManagerHandler_P_m5DF802D7B732DA2D2204DC2CC2FEAC0B5FF64A23,
	WXSDKManagerHandler_Bi_mE26E9A78D311AE851A4EFF6578F05589673CC8CD,
	WXSDKManagerHandler_A_mC9AE2C681CE309039C5B3F5175AE7AD58CA369A7,
	WXSDKManagerHandler_a_mEB98D9658B70E43968F38F1E9DDEF8650C5472C8,
	WXSDKManagerHandler_A_m9746ECEF1EAA83E399C1D79E9D0D288B44A88850,
	WXSDKManagerHandler_B_m3393D29E2A0F35DA0D7C7C3F56829885D794B06A,
	WXSDKManagerHandler_p_m2F0F192336736E23573A16C88A97E2C606E036FB,
	WXSDKManagerHandler_Q_m5120958F7CF0C06231F546A48CAEA92F153551CE,
	WXSDKManagerHandler_q_mBF41D56A02D43E6507E8AB615746E3F8D03F1653,
	WXSDKManagerHandler_R_m41631A3101005587D92864E4882A58F5AC2DAE3E,
	WXSDKManagerHandler_B_m79A9060FD2042537B3A51015848F7B00439D6EE6,
	WXSDKManagerHandler_b_mB53FBA69360B40277BF9EF2C0E87ED3B4549E296,
	WXSDKManagerHandler_r_mE080DCA626BDD5A279FE932AFF8F0F4C535CF6DE,
	WXSDKManagerHandler_BJ_m2D97417155A8D139C14D5C159A3E844B6FD6A2AF,
	WXSDKManagerHandler_Bj_m2601686AEBD1DF8E19DF52797BE1729DA181AA0E,
	WXSDKManagerHandler_BK_m3580ABE717EBBD8DF8E311C045338EE40EE2A5D5,
	WXSDKManagerHandler_Bk_m1A3F55041A9DC003A7AD55D95CC4BA9AEBCB5AB4,
	WXSDKManagerHandler_BL_m678114742DE76B820DF20B8A6CF993A7CB3AFD7F,
	WXSDKManagerHandler_Bl_m9A14C5024B515636311195033DAD08533E6BCD53,
	WXSDKManagerHandler_BM_mF99EE092EE6143D6EF021C8C6427D113D201C392,
	WXSDKManagerHandler_Bm_m9C7AE350396B16166AB1C60E73A26D3B592A517D,
	WXSDKManagerHandler_BN_m408C1BB36E98133775CCA85855F3C4B81709EC35,
	WXSDKManagerHandler_Bn_mE7432C53AFAA2A0EF57F9EA335640042C3193BA0,
	WXSDKManagerHandler_BO_mA521C89537E47D839B2E9FCCAB7365B08155AA4D,
	WXSDKManagerHandler_S_mC8D6500E35942475A0355A0B6B3B85D64FD1C983,
	WXSDKManagerHandler_s_m5B1DC68ED740EFC5B027B0407259945BA8D49A3B,
	WXSDKManagerHandler_T_m5C097EC18B4470553D2E19775CDA3E0B8C77B2FF,
	WXSDKManagerHandler_t_mC6C79526238EBE33C42DD9FF437F48228433E268,
	WXSDKManagerHandler_Bo_mC01E277E725730802FC4EABA07F9D9EEFB75CB0A,
	WXSDKManagerHandler_BP_mDF6E2359AC5A5212C1ABC59A9C3F7C1826076C8C,
	WXSDKManagerHandler_Bp_m38CA54CD912114D4B590B154D2E861EB6D149141,
	WXSDKManagerHandler_U_m2519B28AC9E5F7E9294CB0EA8ED309299B5E73BD,
	WXSDKManagerHandler_BQ_mE6B081FF817B82C938AF43E8666835111A77C937,
	WXSDKManagerHandler_Bq_m2DCC4593A505CB383F3EBC6928BA2DF922752967,
	WXSDKManagerHandler_a_m0D5E0E2B09109AA0B2C0AB68682B5501484AF7B8,
	WXSDKManagerHandler_a_m1DA7D6C683C0E6331101BDABDDD5BCD64A2E26FF,
	WXSDKManagerHandler_A_m95F18FF8B32AC04158C00EC2AE2B7CFE1EEB9024,
	WXSDKManagerHandler_WXUDPSocketSetOnMessage_m6D136005F99475D2F890286324E285747813708A,
	WXSDKManagerHandler_WXUDPSocketSetOnClose_m38DA6943AB1572C3FFEF5C4E690BB71BF5F065A0,
	WXSDKManagerHandler_WXUDPSocketSetOnError_mFF8B7AFF253D9015D391BA036022C28C67FCBBAA,
	WXSDKManagerHandler_DelegateOnMessageEvent_mB4CC33FEFEA70BE79535B3D1CA87F4FEEEF1B418,
	WXSDKManagerHandler_DelegateOnCloseEvent_m51F987A089DD23E07359E77B998275CA8ADA64F1,
	WXSDKManagerHandler_DelegateOnErrorEvent_m84321402DEA4A472CEF6ECB9A0608A7A0ADB3A6A,
	WXSDKManagerHandler_Inited_m22C0ECFD3A6BFFF75D4B687C203C9FC073A0142A,
	WXSDKManagerHandler_TextResponseCallback_m4C8C6DDB0055B0AE5DEFF57BCA2D7EF1742104D2,
	WXSDKManagerHandler_TextResponseLongCallback_m444CE1D6B88A53DF5B2DB4E9D1D78D98CA9A02A7,
	WXSDKManagerHandler_CloudCallFunctionResponseCallback_mC7D98CC0312D255F4D9CC7F40B48774DAD8A8434,
	WXSDKManagerHandler_UserInfoButtonOnTapCallback_m2611465DA3E3DB9DC345880947ED1894F212AAEC,
	WXSDKManagerHandler_OnShareAppMessageCallback_m283D03EFB657CCDEACA4C74EBC331F3769D8E78B,
	WXSDKManagerHandler_ADOnErrorCallback_m900E8E5DF22FAD943E9223BC0A99E7946CA2D12C,
	WXSDKManagerHandler_ADOnLoadCallback_mA8866CF3008C1C17805E0B4933E41BC90481B1F1,
	WXSDKManagerHandler_ADOnResizeCallback_mD7D699D4D26ACC6CCEB3EC444EA79E79CB6A1C96,
	WXSDKManagerHandler_ADOnVideoCloseCallback_mF3F3B098B1534F09E5D10BD76A57CF84142189D5,
	WXSDKManagerHandler_ADOnHideCallback_mC186D6DA54FA0AF2042012DC1658548D8BDDF206,
	WXSDKManagerHandler_ADOnCloseCallback_m7C088444A08477EC43A19AC0299CA7EC3A7E95B9,
	WXSDKManagerHandler_ADLoadErrorCallback_m632BFC6158D493AD3D9687CE3A2EA18A998B67DA,
	WXSDKManagerHandler_OnGameClubButtonCallback_m88C839177C8E3A6E7F2C8B14FA315BA234C9F5C2,
	WXSDKManagerHandler_OnAudioCallback_m03FC815AAB43D54B6D975D8E43B08BB0BFCD6D5F,
	WXSDKManagerHandler_WXPreDownloadAudiosCallback_m08CA17CC20A370FDF81E478653CBCDD9A30D4D38,
	WXSDKManagerHandler_OnVideoCallback_m65401EFBC68C7EF992D080AD9F8002D899170E80,
	WXSDKManagerHandler_StatCallback_m5CB5D78001894D966743EC37E861671FAC68B1D6,
	WXSDKManagerHandler_FileSystemManagerCallback_m13E0DE1D54A33B91F220520D12348A5268044388,
	WXSDKManagerHandler_ToTempFilePathCallback_m84078A162F65E633656B605F6D0CE1D188E5DE92,
	WXSDKManagerHandler_GetFontRawDataCallback_m5EA2BCF9D36000759AB3054E3C26CA7FE3DF9084,
	WXSDKManagerHandler_InitSDK_mAC35C00E88DFF3C575ACDF8CE225B1CC88EFA4AB,
	WXSDKManagerHandler_StorageSetIntSync_mFC81FBAF94CE4C8C564B4CE7AEF2590BE6FC2AE1,
	WXSDKManagerHandler_StorageGetIntSync_m03AD862EF1738CBABE0D740B223ACC2665B38DE9,
	WXSDKManagerHandler_StorageSetStringSync_mFE51CAF774745AADC518D710BC8D089A9C831C08,
	WXSDKManagerHandler_StorageGetStringSync_mDF7F7B60E2F505B07F1B832014FA495730A04246,
	WXSDKManagerHandler_StorageSetFloatSync_m08BCC159B62735E209E057417F25B95C9BC07886,
	WXSDKManagerHandler_StorageGetFloatSync_m12C3E3A94C208DF8A7449BE784A117584F3166F4,
	WXSDKManagerHandler_StorageDeleteAllSync_m4839FFA49355D1845F07A82408A351DC602DADDF,
	WXSDKManagerHandler_StorageDeleteKeySync_mF751DE56AFD2CFE759A067D3A6E0595194DA7779,
	WXSDKManagerHandler_StorageHasKeySync_m5F4C067B1E030377247C3BF03DB0E83E5BEDA623,
	WXSDKManagerHandler_CreateUserInfoButton_mB8FC31345ACA3121077090207DFBED704006B14B,
	WXSDKManagerHandler_OnShareAppMessage_m31DE5FDFB87BDDFC1A8DF25FA6D2526D9E2A20FD,
	WXSDKManagerHandler_CreateBannerAd_m62E6F16AD670C5580912BF64F6752118F4E40B86,
	WXSDKManagerHandler_CreateFixedBottomMiddleBannerAd_m624EBD61A5402B45C83D397A0B670586C5DCFE67,
	WXSDKManagerHandler_CreateRewardedVideoAd_m5ABC1986E79317AA591F940F4C5257591998AB80,
	WXSDKManagerHandler_CreateInterstitialAd_m6CFBC23B60595AB0AAE9709CB425296922C622F0,
	WXSDKManagerHandler_CreateCustomAd_m4A842FBA62A24583C0DB066BE3A06793354C226A,
	WXSDKManagerHandler_ADStyleChange_m44B4D652F79C45C8FF2102DE02029AE000540184,
	WXSDKManagerHandler_ShowAd_m08CA59FA78AA6E2D8A119DE5B8E501F22E8E1FEB,
	WXSDKManagerHandler_CreateUDPSocket_mFE35832F3E44F173BBAD2B1E381208B1CA915C66,
	WXSDKManagerHandler_CloseUDPSocket_m7CEDB3D741CEF2F16C2881F17EB4CAC010282F8F,
	WXSDKManagerHandler_SendUDPSocket_mF6D310EA6028D5B0C295228CEDB81A0F0D2F3833,
	WXSDKManagerHandler_ShowAd_mC9ACDDE6CEFBAB6211A18792211DEF8FEE8762CA,
	WXSDKManagerHandler_HideAd_mD29CD364179D84D97EF406BD68EAD87CDD0BB411,
	WXSDKManagerHandler_ADGetStyleValue_m85FD187B42DDD66979201D07B5922890644D6F8B,
	WXSDKManagerHandler_ADDestroy_m7EC3CFD188A9B283ACAB3C9B474CF409AF3288E0,
	WXSDKManagerHandler_ADLoad_m094D8D324D366D330DDEADB782E54FB7D4E8C9B3,
	WXSDKManagerHandler_OpenDataContextPostMessage_m1CEC72B11F4FEC0D94F4C2E088EA04393ABA088E,
	WXSDKManagerHandler_ShowOpenData_mFBEAE663A4AFC5F38C778B6BD23E21C1FB38E0FB,
	WXSDKManagerHandler_HideOpenData_m9DE08E192F2AE27E92FE4DC6E0FF533CB011573F,
	WXSDKManagerHandler_ReportGameStart_m5F7B3F4FB6F47F68A43A822961C528FB5FC82F68,
	WXSDKManagerHandler_ReportGameSceneError_m4C0F09DCF8DB6635705163A20726EDD95E5C3611,
	WXSDKManagerHandler_WriteLog_mB663FABFDE437831E87AD12E0EDD6C928D20F0F5,
	WXSDKManagerHandler_WriteWarn_m38B4A78B41B67CD2492DE6AA41BC5A1E1F61A02F,
	WXSDKManagerHandler_HideLoadingPage_m273ACF0C1419D9E3CFF1A12B54991FA005D08D10,
	WXSDKManagerHandler_PreloadConcurrent_m0DDF75E2815459012791CAEF6793E99A1CEBF8D3,
	WXSDKManagerHandler_ReportUserBehaviorBranchAnalytics_m8D44D146A3890826B8AEA35A374D6B73265A8857,
	WXSDKManagerHandler_CreateInnerAudioContext_m556E4CFAB33265BF858341EDA665487ECA73E586,
	WXSDKManagerHandler_PreDownloadAudios_m176A22B63E768F168F4777A5EED0A448539D1796,
	WXSDKManagerHandler_CreateVideo_m2B1D7905278EAE58E6E8A6C3955391502A47D88A,
	WXSDKManagerHandler_GetTotalMemorySize_m474305C81AEE9D7BCE6304F7BC60279F5E96B2FF,
	WXSDKManagerHandler_GetTotalStackSize_m0246A6BC731D99EBCEAF465BC215625786E9D9B2,
	WXSDKManagerHandler_GetStaticMemorySize_m20FB74F2D068E9ADAB2395C10C9CCCBC66EB0C66,
	WXSDKManagerHandler_GetDynamicMemorySize_m0AC259A480F54FEA287347AF6295B01D0107E15B,
	WXSDKManagerHandler_GetUsedMemorySize_m99E41333CA7595D4ECCF5F94E0C3D4F2FAAD77A5,
	WXSDKManagerHandler_GetUnAllocatedMemorySize_m1870BFA1D487D923F1F8F03123829D92715AC784,
	WXSDKManagerHandler_GetEXFrameTime_m0FD92DD59EE363F1C84FB7D53AC65B1792BD3DE7,
	WXSDKManagerHandler_GetBundleNumberInMemory_mEF1E849687A6168BD4D14CEFB4A84138069F5AA3,
	WXSDKManagerHandler_GetBundleNumberOnDisk_m32E4DEE21B0E20F235CE301F8904CEA3418A9921,
	WXSDKManagerHandler_GetBundleSizeInMemory_m3BAADFD66DFCDB1DE920805E3D1D406176733446,
	WXSDKManagerHandler_GetBundleSizeOnDisk_m3EF57CBC4ADBA50120AE70D9D711422F292860B5,
	WXSDKManagerHandler_LogUnityHeapMem_m596EEF2A7D623E910191F1E12728DB1AA5C91A9B,
	WXSDKManagerHandler_ProfilingMemoryDump_m0036A38C4356ACD47841035B799E9C09C1689A49,
	WXSDKManagerHandler_SetProfileStatsScript_m458015E3F922B0487CA556170FFEEEC47F485CB4,
	WXSDKManagerHandler_OpenProfileStats_m83BC146CA7BF891314F76BEF0EA5CF203A6E5E1E,
	WXSDKManagerHandler_LogManagerDebug_m27C22B2C99AA25AC1F19276BF41D56CD2F1CE14E,
	WXSDKManagerHandler_LogManagerInfo_mC6F249214AB53B50C4B37581CBBAF84DE5E445AA,
	WXSDKManagerHandler_LogManagerLog_m5CD2C016C93466A0363B349B80D5BB7F50EC5DDE,
	WXSDKManagerHandler_LogManagerWarn_mD8AFCFFB92678402BF1C23F0DAA199904538BDBD,
	WXSDKManagerHandler_IsCloudTest_mF72CE7220855DEFE379E2EE845E9E92CFB24D30A,
	WXSDKManagerHandler_UncaughtException_m340C70224D09FACFBC0241F3E95E76DC43979DE7,
	WXSDKManagerHandler_CreateGameClubButton_m09E7217DAA6712F9A58A133B6F20A6A5C3F52BE1,
	WXSDKManagerHandler_GameClubStyleChangeInt_m1723A7F6C5F9C5A03BEEA95D4E43BDC9C08C7335,
	WXSDKManagerHandler_GameClubStyleChangeStr_mBD730FD57B71441DD6DB3540BE74DA194C896BF4,
	WXSDKManagerHandler_CleanAllFileCache_mA7EC03F6F186C0FD2FA0875884774D325ED71A76,
	WXSDKManagerHandler_CleanAllFileCacheCallback_m46F284537E40AE5D49C42F8979B321C8B7013DC1,
	WXSDKManagerHandler_CleanFileCache_m53E4A9F81CA0718A61E976527777873077F1A090,
	WXSDKManagerHandler_CleanFileCacheCallback_m247B7EACA668E2237FA8A6CAA1B4536936EBFE10,
	WXSDKManagerHandler_RemoveFile_m2571F73C3A662ABAF8B9A6C85786DE1E9B96E4BD,
	WXSDKManagerHandler_RemoveFileCallback_mAF6844F0DBAFF25731F5E7F272EA8BD38A3DE2C3,
	WXSDKManagerHandler_get_PluginCachePath_m0EC53B0A8D65202EAE3A82CACCC749FC5E58B255,
	WXSDKManagerHandler_GetCachePath_m5480634F31FC7E82C2E9DAE12E557E36AD8659A4,
	WXSDKManagerHandler_OnLaunchProgress_m4D406A2257DFDFDF0FB31D5BADECD19DDFA64382,
	WXSDKManagerHandler_OnLaunchProgressCallback_mF608F109F2E682BD9E7F35FF24770CC6307C57BC,
	WXSDKManagerHandler_RemoveLaunchProgressCallback_mC2656189C2759565640B6B922DE99B5AE6359008,
	WXSDKManagerHandler_SetDataCDN_mFE18CE6881707D0EFD6094F9213571EDD4281B90,
	WXSDKManagerHandler_SetPreloadList_m7813E6B509CF4401918E7D621F3E6BD739A4C184,
	WXSDKManagerHandler_A_m4664C74D0D2721190C7914714D121AAFB01AEEAD,
	WXSDKManagerHandler_SetPreferredFramesPerSecond_m9AA73D324748357DEC6ADE9DEE583BE3D4A6B96D,
	WXSDKManagerHandler_BP_mD14106932E5C7A4B567C39894C827C10432BEEC8,
	WXSDKManagerHandler_CreateCamera_mBDA7EDD28DF537617D99DF1791FB3EE56A5B6F20,
	WXSDKManagerHandler_CameraCreateCallback_m1D39F71B077D035EDF08BA077BD45480B7A9097C,
	WXSDKManagerHandler_CameraOnAuthCancelCallback_m5C7F3C244591347F02939B7CC3E90B8D329CA35D,
	WXSDKManagerHandler_WXSetArrayBuffer_mDAEA932726FFD397D3993F0F71E3CAC05F28B533,
	WXSDKManagerHandler_CameraOnCameraFrameCallback_mF49346DBB7995911F0D33B1844962C400A4B5CE4,
	WXSDKManagerHandler_CameraOnStopCallback_m1714F0DC0DB16B8BE92087C7D2F5BCE73BFDB377,
	WXSDKManagerHandler_BR_m8E49662A09309BEAEAA68526A12E5DB806C3271B,
	WXSDKManagerHandler_GetGameRecorder_mF6F704C5E1BBD77A377365398DE9465B2A3BD6F8,
	WXSDKManagerHandler__OnGameRecorderCallback_mDBB303CCABB6DB39A41D3246302674DBFD721190,
	WXSDKManagerHandler_Br_m22BD724CB9C4E7132096FFB1695F4649CCD1706D,
	WXSDKManagerHandler_GetRecorderManager_m48035F84F9832C79857A57ABE6F3BE9CABC94805,
	WXSDKManagerHandler__OnRecorderErrorCallback_m5D045D0468B10539C2C807C0873BF9FAB61AC745,
	WXSDKManagerHandler__OnRecorderFrameRecordedCallback_m79BE33FBF0D794C2504C3CD10E95E7A98C2ADF1C,
	WXSDKManagerHandler__OnRecorderInterruptionBeginCallback_m6F6ED98CCEA86BC618AAB6D6CED3968363197856,
	WXSDKManagerHandler__OnRecorderInterruptionEndCallback_m9722A5D5F828D95DD1A9BFB44823AE04414DDFD9,
	WXSDKManagerHandler__OnRecorderPauseCallback_m082E5575F9DD810276441B98EE8F2EF74798E132,
	WXSDKManagerHandler__OnRecorderStartCallback_m057BD0CCA8D87E95793A757C4772C20CB9EA5619,
	WXSDKManagerHandler__OnRecorderStopCallback_mECA63AE4D6D7239F061A34B0925576F91E35E301,
	WXSDKManagerHandler__OnRecorderResumeCallback_m11FE7BD022D36EBE2AB4E5BC453E0F51A9B00461,
	WXSDKManagerHandler_Bp_m0FF7AD727E913E69840482BEBF348344951FEC59,
	WXSDKManagerHandler_UploadFile_m42895A82E5423BF33D06F727B21170283A713DA7,
	WXSDKManagerHandler_UploadFileCallback_m71A50B1E7975CA0D95AA518E94735F33406246A9,
	WXSDKManagerHandler__OnHeadersReceivedCallback_m3C2EB9451908572280B25516C5CDB4BBCBAB1263,
	WXSDKManagerHandler__OnProgressUpdateCallback_m0AFD6FD08C0B1A9F5671280C4943BF78DE320373,
	WXSDKManagerHandler_u_m79211EF4B1E512F21937EDD26D5FCD7279CFB61D,
	WXSDKManagerHandler_CreateMiniGameChat_mB7959EDACFAD56326E6D7D8893723321995FFAC2,
	WXSDKManagerHandler_OnWXChatCallback_mD27C8B34CBD509D871D8B016D184EC3BD67014F6,
	WXSDKManagerHandler__OnNeedPrivacyAuthorizationCallback_mC70D9BF5450542643D1C3B31CED758E8B3CD7626,
	WXSDKManagerHandler_BS_mFD638E8AB7316E4C1FE9312ADDCD0E9AABC6A089,
	WXSDKManagerHandler_OnNeedPrivacyAuthorization_m9121F7F4ABF4EE2BF4CC56E0C368F27DAF1095E6,
	WXSDKManagerHandler_V_mA6AB2AD48E951E0127C9C62C3F54F55A38B5CD08,
	WXSDKManagerHandler_PrivacyAuthorizeResolve_mCD977032861782F05DA0CDF88113D83A69A4CD4D,
	WXSDKManagerHandler__ctor_m38D47BEC8FED1E450449746E33DF21045674FA4B,
	WXSDKManagerHandler__cctor_mEFFD0FDC42C15C3E9EDED0A31B7A3B2A23D8F78B,
	OnMessageCallback__ctor_mCAC48245FB9CF58F9035489D832308C93FF2CB0D,
	OnMessageCallback_Invoke_mB7AB9D4761483D638895AAEA6BBB694F6F2D9DB9,
	OnMessageCallback_BeginInvoke_mCDD6E64DA85E6CED0FE124657620AF53E3509F8B,
	OnMessageCallback_EndInvoke_mE9705D406C9B2E306EA7A3D16D808E09C7E21E27,
	OnErrorCallback__ctor_m5B1AC02DE84E0F472FE59B71F8874F57920631AC,
	OnErrorCallback_Invoke_m0CEAAAD37ACCFDB4EDBDB10743647E0F0B3C595D,
	OnErrorCallback_BeginInvoke_mC22090E89B17D33714DA594B8CFB7BB38112823E,
	OnErrorCallback_EndInvoke_m1D6B0F2E1E9CE5FBF56C9BF68D63543488194F88,
	OnCloseCallback__ctor_mED9B269C6E307995A2964E1AD16F34543629BE32,
	OnCloseCallback_Invoke_mE7003EDD84DCEFE256291AE966C96447BC303C17,
	OnCloseCallback_BeginInvoke_m228CDBE350784A62B12647399200FDCC06914F48,
	OnCloseCallback_EndInvoke_m75363E2E935180149F55E7FD353BF605EE3760A5,
	b__cctor_m3D82D921A5493667F83063D772B72D074C3C8F24,
	b__ctor_m1B7B124D1A11723FB9756344BEDE8F58AA826DCC,
	b_A_m26C823AB15EEEE0324FDB9B3AB0FF49A409E71C6,
	b_A_m8CD159020CCC9640ED09FFAE2776756AF2519CA8,
	b_A_m7CAB41555C37A534C9EB999306FB79971E3F8982,
	b_A_mE59781BD2700C23B9BF21E4E1C7F2A61DB01AAC0,
	b_A_mAC787F340EFA9831CB01155630CCB81026E4E9CD,
	b_A_mEF8A31E65455D3B33BB037850161CAFE86D397EE,
	UDPClient__ctor_m484B9BBB0948C03F2F6D5C1B925B7216452DE4A6,
	UDPClient_SetCallBack_mA0CD3DCCCBDCF72D2C49C3D48E5867E8D3F99A01,
	UDPClient_Send_m4491E0E7572CBB8E6920C63ED535A170D2F8A0A6,
	UDPClient_OnMessage_m11F747E473B777DE79DA50B0DD85B46350E8CA47,
	UDPClient_OnClose_m994C38ECFA4D0C6DB0BD35F65C238BB23680C5C6,
	UDPClient_OnError_m8B9549EACF12A1EA0AB9CA1A0BC6C43150AC802F,
	UDPClient_Destroy_m64C5EDDF971996626F0C97F4452C27DDCBA6DB1C,
	UDPSocketManager__ctor_m77491DD98A7638DCEFC9136BF248B1DA1F74688D,
	UDPSocketManager_OnMessage_mDE8CC4EC59E3A4EE7EFEA8D5F09D67ABFD97ED19,
	UDPSocketManager_OnClose_m2338DB278802E39FBBB80A60C3F181B98F6B80CA,
	UDPSocketManager_OnError_mF74A45408C7AF04F0895B391BF8C42E60455A1D0,
	UDPSocketManager_AddClient_m9476091DB05E736B0C06038C076129B1736CCDDF,
	UDPSocketManager_DeleteClient_m2B9295F1C1C5768402D10868FF09430FFEEFF3D1,
	UDPSocketManager__cctor_m9D1DF67E5025999FB52C767715E89A3911BCE979,
	WXUpdateManager__ctor_mE4279282B4A07D5ADA2E5702E14A155D97D9A27B,
	WXUpdateManager_A_m8F9F1CC44158D958B4193DC0B65959F62BD86148,
	WXUpdateManager_ApplyUpdate_m1FE7AD2134B0A38B2ACE556D3862DCF6FD12A5CB,
	WXUpdateManager_a_m9AED771B84B3C75957E96F91AECF67AE31B6F0B9,
	WXUpdateManager_OnCheckForUpdate_m3980609043D82C4E40BCDC7FF05D9F2563BD400B,
	WXUpdateManager_B_m2FADEC6DDCB3F47EC7BB146A7AC1CF0FB2920831,
	WXUpdateManager_OnUpdateFailed_m622EF73958D0F130245FD32A856E33F298B5156D,
	WXUpdateManager_b_mA8AB48A37C757E6AE0CA307D0E57D9E8F69532BE,
	WXUpdateManager_OnUpdateReady_m9BD7A5614BD4EC426817802DF06504EB3AC38DD0,
	WXUploadTask__ctor_mD77B5197B5DF3570BA7799708799801D8CA06BBC,
	WXUploadTask_A_m83AD339A6BCC2F90BA9639692050509E810A35F1,
	WXUploadTask_Abort_mF6DE262872E39DB49197F4C0B6216AC292E3388F,
	WXUploadTask_a_m4455322F2533304EB2CEDF81DE1F98D4F956A42F,
	WXUploadTask_OffHeadersReceived_m57DA255F9D0246F84B8720B8C1D33E65976FC1E8,
	WXUploadTask_B_m43490388BC195F908C384201B1EF1343BABDFF70,
	WXUploadTask_OffProgressUpdate_m0CDD8103C4FF975F9C9BD49DD4AFC666878BDC40,
	WXUploadTask_b_mD41BDCD652E8DD69C891B7B6B43B4B926330EAB0,
	WXUploadTask_OnHeadersReceived_mFAA7A965DBB1B65B623F7330E9B06AB540E89A9E,
	WXUploadTask_C_mDD73F701259E973CF4D16A9A9A743A067D4B1402,
	WXUploadTask_OnProgressUpdate_m6C18B12F03668AE1AB1FDB2D4F9646E07F26C6A7,
	UploadFileOption__ctor_mB37E1C22289F1578EC908F929FECD05063EA7F69,
	UploadFileSuccessCallbackResult__ctor_m2E228B8F3CA6A54FB3D8FC55EDD4D9E7D0870428,
	OnHeadersReceivedCallbackResult__ctor_m65D27B2CFE2F1BEE3AC0CB3C51AF1B9D57E5A782,
	UploadTaskOnProgressUpdateCallbackResult__ctor_mFAD1384075D15AEB879AEFA102B885BA5300E90A,
	WXUserInfoButton_A_mF87F824642FE8DE346D36C37E7EED61955C3295F,
	WXUserInfoButton_a_m00432EE2C76BF7CB780D47998149F5E9E3BDBC1E,
	WXUserInfoButton_B_m04AAEFC96EC690AF9761EF8BFC9B7DB9476EB6FD,
	WXUserInfoButton_b_m18200B0BC2CA6A78E49C3198D3D8DB74F78DB2A2,
	WXUserInfoButton_C_m09EEDBB2CD73AAC9431C9CC23B5F2806A55F5F6A,
	WXUserInfoButton__ctor_m014080AD98A37E26623D5F5ECA960DAAED61B277,
	WXUserInfoButton_InvokeCallback_mDCF9738081240CC2B3E7EA470C82C02FBA051014,
	WXUserInfoButton_Destroy_m580CBBE01F66772AF857D3897F492FCB84791877,
	WXUserInfoButton_Hide_m3E7B0827CA298C17DE91106F131B1C2843DB0869,
	WXUserInfoButton_OffTap_m4A9C65F16CA3FA2310E99BDE2628BE6C90E8B281,
	WXUserInfoButton_OnTap_mFE1DDE836D65F1DB187906360EE5618BA30BDC1A,
	WXUserInfoButton_Show_m686FE89211F7ED31D1DC6C9E9FD8B4841DB595AA,
	WXUserInfoButton__cctor_m199B57EA2CA6AA73CFE38A7033470492CF775F76,
	WXVideo_A_mA954DD718B8E161694AF6F9DB1E08742EA7E2133,
	WXVideo_A_m1F2B4D0610B4EC5A34C8309199033B0869B44565,
	WXVideo_a_m0F806EE3268E15F98A45F252A5EB097E9D96BF42,
	WXVideo_B_m8C4A1D6D5DCC11A067725CC3AB6944FD9E018456,
	WXVideo_b_m389B900E05223B7316BFEC71740B98F63F0711C0,
	WXVideo_A_mF9C2646AA8F124E8318AF4541FDCF7DCF6383B08,
	WXVideo_a_m2B96236796C4AA903DA09E855EA8F4B6FAF3ACEF,
	WXVideo_C_mF08D2E77543319EC13D77A6C8678FC779D00D214,
	WXVideo_a_m6C185DBE230427D9BE592576A6EEFCF89EC0D1A1,
	WXVideo_A_m4168276950B112793EEECF498097D5DB3952BA0B,
	WXVideo__ctor_mEE4411332EA65C404A06649A57EECC7F1F0D2E98,
	WXVideo__HandleCallBack_m708ECF4999EEC43E13505C184B38FF1CCE46D87B,
	WXVideo_get_src_m6708B85CE0CDCF0E8613087377CFFF2479C97C08,
	WXVideo_set_src_mBF7C29AAD0A1EDD9AF9617069E90D23797FD3D14,
	WXVideo_get_poster_mAC1E2145C5F0B64946C202DE97B78ECF32F9E8E2,
	WXVideo_set_poster_mF408A5FA4E98C64EBE57DDD10B98D2DAFF8DF249,
	WXVideo_get_x_m7E136C8DC99C5158AF5541C82D09A4ABC94CB134,
	WXVideo_set_x_m6976BB23ED8FDA99413735F64BDB9CA21D5976FC,
	WXVideo_get_y_m4AEEE6BF5B1D9645F9C94A5A9655CAD8AB85FC84,
	WXVideo_set_y_mB17975C09B3515AC3ED57044073D4AB564E73F8B,
	WXVideo_get_width_mA3636F15277249EDC237CDE4D5D351EB4CEBCFA9,
	WXVideo_set_width_m1479C1DF020E8104D0094FB34EF2216B23DDB995,
	WXVideo_get_height_m5B56D1D51D7FBECA29B92AD3B5FFDE2E1AAD8A5A,
	WXVideo_set_height_m525C0940749FBEB14C48E78BC4E23D3911C5B4C8,
	WXVideo_get_isPlaying_m12A2BD6BF127399833D025A73B936402BF8DFC8D,
	WXVideo_Play_mCD7C7FB8D9DD26AE5DD76361857F44E4F29C1311,
	WXVideo_OnPlay_m4B6D265CA005DD27476E9B91448E5845C16B237C,
	WXVideo_OffPlay_m37E36A6EC024DE507BCFE72CC985BBEE670D5DDF,
	WXVideo_OnEnded_m3D6327083E6A503B87A494971F1BD4983ED396D1,
	WXVideo_OffEnded_m19D53D340FED07AD74156186744B53C68DAD1AB0,
	WXVideo_OnError_m5A6BD8FEF518A8EB8031D21CF71BB98EEA618B0A,
	WXVideo_OffError_mC34991C23942155D6DC00467C2BCC07C89279820,
	WXVideo_OnPause_m0A7D67369472FEDFB31CDD1FE386535A7A6E9679,
	WXVideo_OffPause_mA9C00F9B3C957AB0BB21A0988C89D2AAD55666BF,
	WXVideo_OnWaiting_m4CBD63CA147855D288FA09628F74243ADA1C400F,
	WXVideo_OffWaiting_m415B34EFEFAB45F06952494D894F13A2168C5654,
	WXVideo_Destroy_m2DE57580636DE80E3A81F49441D6C942816C8486,
	WXVideo_ExitFullScreen_m26AE831DDF84468E3D4D057CEB7F8AF4B5DE1997,
	WXVideo_Pause_m0560DCAC8E6FF8A0515FEC78111F204A20A5A04A,
	WXVideo_RequestFullScreen_m47836A8932C848DB71BE15BB068019495E327A39,
	WXVideo_Seek_m8A18917362FD6EA5D5A5E9F698E7ABC039B79BA7,
	WXVideo_Stop_m3C09D13BA13DE742B0B39F8AE2A39F21217D8C82,
	WXVideo_OnTimeUpdate_m47ADCB1A1CDB8E88B15347B1ABE6453B7B5A2FBB,
	WXVideo_OffTimeUpdate_m0C362F4D87A0EC2A2DE49DE50B97DBB8001446CC,
	WXVideo_OnProgress_mA442AAFCA40CD392B366B02496E89833F20E63D8,
	WXVideo_OffProgress_m0D96DE5CCB8F42FA6FD3C59574E7095D4EE72145,
	WXVideo__cctor_mC89C0359C234BEC20938850CC0C77C7F10E51914,
	WXVideo_A_m31030BCD3571A728528E14EDB1B03564FEA6E95E,
	WXVideo_a_mC5F7B0C7111ADD5DA2AA57169EE0874FF7AA0CE0,
	WXVideo_B_m09616AC965B265CB4D8E7354C50DB94CBCED7828,
	WXVideoDecoder__ctor_m9B3EA162CB13055C52CC62AD568416D9C05D756B,
	WXVideoDecoder_A_mA9CC35C18D80D968667C233937A041E3C13BBAF5,
	WXVideoDecoder_GetFrameData_m4D31A329AF7156F772E41D0DBC91FE3C49DB5F66,
	WXVideoDecoder_a_m95AF245275836B82683E2C7D6D2CD97A71FD2B44,
	WXVideoDecoder_Remove_mE27FB0457AB06C1CD733E5E7A29232DE3ECE8EEB,
	WXVideoDecoder_A_m2DA4107B1994C26A198BD9B38951AC80AAA62808,
	WXVideoDecoder_Seek_mEF6233784B3A783267630182D11591B645DCBE82,
	WXVideoDecoder_A_mC04899C7AC10F17A0CBEB5F2774A2FC61A66DAC0,
	WXVideoDecoder_Start_m693C475622713400C4A39ADE19CF7B0CDED6BA43,
	WXVideoDecoder_B_mDAF92408E9FB19FDF04D6A6DDCC6BFC005FE2369,
	WXVideoDecoder_Stop_m941A5CFCD440F160C21750077CAE0E279F08F28B,
	WXVideoDecoder_a_mE20C1E0C243196F9B19E333498D6F41DBB6D6C18,
	WXVideoDecoder_Off_m18D01C3C71270757C56EFF8FC9F60F86F1C7FFC3,
	WXVideoDecoder_B_m4DA41D996448E6F70AE79329FBD4665E2DCA6CCE,
	WXVideoDecoder_On_mF04B5B803CAAECE7E5F2DDF0E0E667A6D7678B77,
	NULL,
	NULL,
	NULL,
	NULL,
	C_A_m7B3D910143258596FE37FF40986F10BAC7A56BA8,
};
static const int32_t s_InvokerIndices[2060] = 
{
	1766,
	1543,
	1576,
	3128,
	2923,
	3131,
	2981,
	3135,
	3032,
	3572,
	3487,
	3203,
	3572,
	1973,
	3487,
	3341,
	1576,
	1576,
	1457,
	1766,
	1162,
	1698,
	1571,
	1698,
	1571,
	1698,
	1571,
	1698,
	1571,
	1698,
	1698,
	1149,
	1698,
	1571,
	1698,
	1571,
	1708,
	1766,
	1708,
	748,
	2697,
	2722,
	1708,
	1668,
	1766,
	3572,
	1156,
	701,
	241,
	1576,
	1158,
	1708,
	1698,
	1698,
	1708,
	3111,
	3543,
	3487,
	3487,
	3522,
	1668,
	3341,
	2987,
	2599,
	3341,
	2987,
	2599,
	1457,
	0,
	979,
	1457,
	0,
	979,
	0,
	1708,
	0,
	1457,
	1708,
	0,
	1457,
	1708,
	1708,
	3478,
	1543,
	3341,
	3341,
	2987,
	3572,
	1164,
	1708,
	1708,
	823,
	1766,
	3126,
	1162,
	1576,
	1576,
	1576,
	1766,
	1576,
	1158,
	471,
	1576,
	1576,
	1576,
	1576,
	1766,
	3572,
	3536,
	3522,
	0,
	0,
	3487,
	3543,
	1766,
	3572,
	1571,
	1766,
	1668,
	1708,
	1766,
	1708,
	1576,
	3487,
	1766,
	3487,
	1766,
	3487,
	1766,
	3487,
	1576,
	3487,
	1576,
	3487,
	1576,
	3572,
	1766,
	1766,
	3341,
	2392,
	3341,
	3487,
	1766,
	1576,
	3487,
	1158,
	3487,
	1576,
	3572,
	1766,
	3487,
	1576,
	3572,
	1766,
	3487,
	1576,
	3487,
	1576,
	3487,
	1576,
	3572,
	1766,
	1766,
	1766,
	1149,
	1766,
	1766,
	1576,
	1576,
	1576,
	1576,
	1158,
	1158,
	3487,
	1766,
	3487,
	1766,
	3487,
	1766,
	3487,
	1576,
	3487,
	1576,
	3543,
	1708,
	1766,
	2722,
	1158,
	1708,
	1576,
	1708,
	1576,
	1708,
	1576,
	3487,
	1766,
	3487,
	1766,
	3487,
	1766,
	3487,
	1576,
	3487,
	1766,
	1766,
	1766,
	3337,
	3543,
	3341,
	1127,
	1107,
	1158,
	3572,
	2588,
	2266,
	1863,
	1934,
	1863,
	1934,
	2981,
	3341,
	3341,
	2392,
	2981,
	2117,
	3341,
	2392,
	2102,
	2973,
	2102,
	2973,
	603,
	603,
	1576,
	1576,
	1576,
	1576,
	1576,
	591,
	362,
	1457,
	1576,
	979,
	1576,
	1457,
	1576,
	1576,
	976,
	1576,
	976,
	3131,
	1576,
	3487,
	3487,
	0,
	0,
	0,
	0,
	2722,
	2388,
	738,
	738,
	3341,
	1457,
	2924,
	1457,
	3131,
	1576,
	3131,
	1576,
	3131,
	1576,
	3131,
	1576,
	3131,
	1576,
	3131,
	1576,
	2266,
	1576,
	3131,
	3131,
	1576,
	1576,
	3131,
	1576,
	3131,
	1576,
	3131,
	1576,
	3131,
	1158,
	3131,
	1576,
	3131,
	1576,
	3131,
	1576,
	2266,
	2981,
	1576,
	1576,
	2981,
	1457,
	3341,
	1457,
	2973,
	1457,
	2587,
	3341,
	1457,
	1457,
	3341,
	1457,
	2981,
	979,
	3487,
	1576,
	3487,
	1576,
	3487,
	1576,
	1766,
	3572,
	3131,
	2981,
	3487,
	3487,
	1766,
	3572,
	3487,
	3487,
	2722,
	3487,
	3131,
	3131,
	1158,
	1698,
	1571,
	1698,
	1571,
	1708,
	1576,
	1708,
	1576,
	1708,
	1576,
	1766,
	1766,
	1766,
	1576,
	1576,
	1576,
	3572,
	1152,
	1688,
	1698,
	1571,
	1698,
	1571,
	1698,
	1571,
	1698,
	1571,
	1698,
	1571,
	1698,
	1571,
	1698,
	1571,
	1698,
	1571,
	1708,
	1576,
	1708,
	1576,
	1708,
	1576,
	1698,
	1571,
	1576,
	3131,
	1576,
	3131,
	1158,
	3131,
	1576,
	3487,
	1766,
	3487,
	1766,
	3487,
	1766,
	3487,
	1766,
	3572,
	1766,
	1766,
	1766,
	2718,
	2722,
	2723,
	3031,
	2817,
	3487,
	3487,
	3487,
	3135,
	3487,
	3131,
	3131,
	1158,
	1158,
	1668,
	1543,
	1708,
	1576,
	1668,
	1543,
	1731,
	1593,
	1668,
	1543,
	1731,
	1593,
	1668,
	1543,
	1731,
	1593,
	1731,
	1731,
	1731,
	1668,
	1668,
	1766,
	1766,
	1766,
	1593,
	1766,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	3572,
	1766,
	1766,
	1766,
	1766,
	1576,
	1158,
	1576,
	1576,
	1766,
	1766,
	3543,
	1576,
	3572,
	1158,
	3131,
	1576,
	3131,
	1576,
	3131,
	1576,
	3131,
	1576,
	3536,
	0,
	0,
	1766,
	3572,
	1766,
	1766,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1766,
	1576,
	1766,
	1576,
	3131,
	1576,
	3131,
	1576,
	3131,
	1576,
	3131,
	1576,
	3131,
	1576,
	1576,
	3487,
	1576,
	3487,
	1576,
	3487,
	1576,
	3487,
	1576,
	3487,
	1576,
	3487,
	1576,
	3487,
	1576,
	3487,
	1576,
	3487,
	1766,
	3487,
	1766,
	3131,
	1576,
	3487,
	1766,
	1766,
	1766,
	1766,
	1766,
	2981,
	1576,
	1576,
	1158,
	1576,
	1576,
	1457,
	0,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	1576,
	3131,
	1576,
	3487,
	1576,
	3487,
	1576,
	3131,
	0,
	3127,
	1151,
	2683,
	672,
	3487,
	1576,
	3487,
	1576,
	3487,
	1576,
	3131,
	0,
	3487,
	1576,
	3572,
	1766,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3572,
	1576,
	3572,
	1576,
	1576,
	3487,
	3572,
	1576,
	3572,
	1576,
	1576,
	3487,
	3572,
	1576,
	3572,
	1576,
	1576,
	3487,
	3572,
	1576,
	3572,
	1576,
	1576,
	3487,
	3572,
	1576,
	3572,
	1576,
	1576,
	3487,
	3572,
	1576,
	3572,
	1576,
	3203,
	1314,
	3543,
	1708,
	3543,
	1708,
	3543,
	1708,
	3543,
	1708,
	3543,
	1708,
	3543,
	1708,
	3341,
	0,
	3543,
	0,
	3543,
	1708,
	3543,
	1708,
	3543,
	1708,
	3543,
	1708,
	3543,
	1708,
	3543,
	1708,
	3543,
	1708,
	3543,
	1708,
	2456,
	534,
	3203,
	1314,
	3248,
	1399,
	3341,
	1457,
	3543,
	1708,
	1576,
	3341,
	1457,
	3341,
	1457,
	3341,
	1457,
	3543,
	1708,
	3543,
	1708,
	3543,
	1708,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	3543,
	3543,
	3543,
	1766,
	3487,
	3128,
	2923,
	3131,
	2981,
	3135,
	3032,
	3572,
	3487,
	3203,
	1919,
	3126,
	3487,
	3341,
	3341,
	3341,
	3341,
	2720,
	2588,
	2062,
	2588,
	2924,
	3285,
	2528,
	2582,
	3341,
	2096,
	3572,
	3572,
	2356,
	3487,
	3487,
	3572,
	3483,
	2720,
	1859,
	3128,
	3487,
	3487,
	3341,
	3341,
	2720,
	2722,
	3487,
	3567,
	3567,
	3567,
	3567,
	3567,
	3567,
	3559,
	3567,
	3567,
	3567,
	3567,
	3487,
	3487,
	3487,
	3487,
	3572,
	3572,
	3522,
	3341,
	3543,
	3572,
	2523,
	3483,
	2360,
	3487,
	3487,
	3487,
	2689,
	3112,
	3112,
	1571,
	1576,
	1576,
	1576,
	1576,
	1766,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1154,
	876,
	1158,
	979,
	1160,
	1053,
	1766,
	1576,
	1314,
	156,
	1158,
	1457,
	598,
	1457,
	1457,
	1457,
	736,
	738,
	566,
	1571,
	422,
	286,
	738,
	877,
	1576,
	738,
	1576,
	274,
	1766,
	1766,
	418,
	1576,
	1576,
	1766,
	1571,
	736,
	1457,
	1158,
	1457,
	1757,
	1757,
	1757,
	1757,
	1757,
	1757,
	1731,
	1757,
	1757,
	1757,
	1757,
	1766,
	1766,
	3487,
	1766,
	1576,
	1576,
	1576,
	1576,
	1668,
	1766,
	1457,
	736,
	738,
	1576,
	1576,
	1127,
	1576,
	1158,
	1576,
	3543,
	1457,
	1576,
	1576,
	1576,
	1576,
	1576,
	3481,
	1556,
	3131,
	1457,
	1576,
	1576,
	2981,
	1576,
	1576,
	3543,
	1708,
	1576,
	3543,
	1708,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	2981,
	1457,
	1576,
	1576,
	1576,
	3341,
	1457,
	1576,
	1576,
	3572,
	1576,
	3487,
	1576,
	1766,
	3572,
	1156,
	687,
	237,
	1576,
	1156,
	1125,
	350,
	1576,
	1156,
	1125,
	350,
	1576,
	3572,
	1766,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	718,
	738,
	718,
	1576,
	1576,
	1576,
	1766,
	1766,
	1127,
	1127,
	1127,
	1576,
	1576,
	3572,
	1576,
	3487,
	1766,
	3487,
	1576,
	3487,
	1576,
	3487,
	1576,
	1576,
	3487,
	1576,
	3487,
	1576,
	3487,
	1576,
	3487,
	1576,
	3487,
	1576,
	1766,
	1766,
	1766,
	1766,
	3487,
	3487,
	3487,
	3487,
	3487,
	1576,
	1576,
	1766,
	1766,
	1766,
	1576,
	1766,
	3572,
	3487,
	3131,
	3487,
	3487,
	3487,
	3128,
	3128,
	3487,
	3131,
	2722,
	1158,
	1576,
	1708,
	1576,
	1708,
	1576,
	1681,
	1556,
	1681,
	1556,
	1681,
	1556,
	1681,
	1556,
	1668,
	1766,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1576,
	1766,
	1766,
	1766,
	1571,
	1571,
	1766,
	1576,
	1576,
	1576,
	1576,
	3572,
	1766,
	1766,
	1766,
	1576,
	3341,
	1708,
	3487,
	1766,
	3127,
	1556,
	3131,
	1576,
	3487,
	1766,
	3131,
	1576,
	3131,
	1158,
	0,
	0,
	0,
	0,
	3443,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[4] = 
{
	{ 0x06000029, 2,  (void**)&WXAssetBundleRequest_Callback_mBDBC695795945916CE988FA73D548FBFD70B9CCB_RuntimeMethod_var, 0 },
	{ 0x060006F6, 5,  (void**)&WXSDKManagerHandler_DelegateOnMessageEvent_mB4CC33FEFEA70BE79535B3D1CA87F4FEEEF1B418_RuntimeMethod_var, 0 },
	{ 0x060006F7, 3,  (void**)&WXSDKManagerHandler_DelegateOnCloseEvent_m51F987A089DD23E07359E77B998275CA8ADA64F1_RuntimeMethod_var, 0 },
	{ 0x060006F8, 4,  (void**)&WXSDKManagerHandler_DelegateOnErrorEvent_m84321402DEA4A472CEF6ECB9A0608A7A0ADB3A6A_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[21] = 
{
	{ 0x02000030, { 37, 4 } },
	{ 0x02000031, { 41, 7 } },
	{ 0x02000032, { 48, 10 } },
	{ 0x06000045, { 0, 1 } },
	{ 0x06000048, { 1, 2 } },
	{ 0x0600004A, { 3, 3 } },
	{ 0x0600004C, { 6, 3 } },
	{ 0x0600004F, { 9, 1 } },
	{ 0x0600006F, { 10, 1 } },
	{ 0x06000070, { 11, 4 } },
	{ 0x060000FF, { 15, 2 } },
	{ 0x06000100, { 17, 4 } },
	{ 0x06000101, { 21, 5 } },
	{ 0x06000102, { 26, 6 } },
	{ 0x060001E4, { 32, 1 } },
	{ 0x060001E5, { 33, 4 } },
	{ 0x060003D6, { 58, 3 } },
	{ 0x0600055F, { 61, 1 } },
	{ 0x0600056B, { 62, 1 } },
	{ 0x0600067A, { 63, 2 } },
	{ 0x0600067C, { 65, 2 } },
};
extern const uint32_t g_rgctx_T_t05B87E281BD442A7E51E7311F7CC498F2DE2AFD4;
extern const uint32_t g_rgctx_T_tF4A444D6963737D42FA1425638A99318B5C612A8;
extern const uint32_t g_rgctx_T_tF4A444D6963737D42FA1425638A99318B5C612A8;
extern const uint32_t g_rgctx_AU5BU5D_t5126347664B2F059DE9CF63976124F4A044A5ECA;
extern const uint32_t g_rgctx_A_t419BE7BD256C16664B4136CEC63935F89CDBB2CE;
extern const uint32_t g_rgctx_AU5BU5D_t5126347664B2F059DE9CF63976124F4A044A5ECA;
extern const uint32_t g_rgctx_T_tEAEBCADC36FDE9945CB4A7C7A62C6B2FBCFB4ED4;
extern const uint32_t g_rgctx_WXAssetBundle_A_TisT_tEAEBCADC36FDE9945CB4A7C7A62C6B2FBCFB4ED4_m33D03978840B307D7F3375378591F5B7B815F8DC;
extern const uint32_t g_rgctx_TU5BU5D_tA1ABF53AE51C00F3EF94577A90937B70BCC12AB1;
extern const uint32_t g_rgctx_T_tA5FF1B4404CE99EF0FFB87CFA21C3C9F3B21B7CD;
extern const uint32_t g_rgctx_Action_1_t5F2C022B66489CA8AF33760972405CF206F3AADE;
extern const uint32_t g_rgctx_JsonUtility_FromJson_TisA_t99F6E99DEA52A46B3E87A812B65F883D7CAC52B7_m9DF537346473C3B327B1E4BB19DBED887DADA597;
extern const uint32_t g_rgctx_A_t99F6E99DEA52A46B3E87A812B65F883D7CAC52B7;
extern const uint32_t g_rgctx_Action_1_t801B8A9D58AE1D225C261AD4C059A83AFCE7D4A3;
extern const uint32_t g_rgctx_Action_1_Invoke_m90A5188E41D4908F100C03EEDB251B8186D02879;
extern const uint32_t g_rgctx_T_t22031FF3C98F160EFF44B810A8F8B276696DB469;
extern const uint32_t g_rgctx_T_t22031FF3C98F160EFF44B810A8F8B276696DB469;
extern const uint32_t g_rgctx_A_tD91E49759634FF7137EA3B8CD6FB454C9CAA8534;
extern const uint32_t g_rgctx_A_tD91E49759634FF7137EA3B8CD6FB454C9CAA8534;
extern const uint32_t g_rgctx_WXBaseActionOption_1_t92A6B04C0944F8778520E2F9D44C1B2D687B52BB;
extern const uint32_t g_rgctx_Action_1_tC7CE0701981A6C5548E2B7A78013E092F8E006C6;
extern const uint32_t g_rgctx_A_t3C9C2A5B56813922EAB8557B3E81F24FAB04BF40;
extern const uint32_t g_rgctx_A_t3C9C2A5B56813922EAB8557B3E81F24FAB04BF40;
extern const uint32_t g_rgctx_WXBaseActionOption_2_t93C8D02E6CEB9F04076C2E22A3EF4F393C84C06F;
extern const uint32_t g_rgctx_Action_1_t63DD569B1853DE5F4817EF3B24FA2B7174A820E7;
extern const uint32_t g_rgctx_Action_1_tA6F1B20172FD3EC2025B7BC2A95A98EF4C25E491;
extern const uint32_t g_rgctx_A_tE8CCB1CC3A4A1D05F7D3632ED7621054C4F441C6;
extern const uint32_t g_rgctx_A_tE8CCB1CC3A4A1D05F7D3632ED7621054C4F441C6;
extern const uint32_t g_rgctx_WXBaseActionOption_3_t45B4D7C08A289EEB4CEB5E2917765627355A7DBC;
extern const uint32_t g_rgctx_Action_1_tD5652752192F75EE12CFEAD967D10329F64BAA0F;
extern const uint32_t g_rgctx_Action_1_tBD9EA1AE73BACFCA63512975453EF0F56336903D;
extern const uint32_t g_rgctx_Action_1_tDBF138232B0EEEDD9D01D9C2B0560DEC016C1882;
extern const uint32_t g_rgctx_Action_1_tD2B749817E93CC3FCBCF316D9C9B1A3BADED451E;
extern const uint32_t g_rgctx_JsonUtility_FromJson_TisA_t612F28F9085193FA593E2E8784066164A93402DE_m31E0D7110FF3126BCCDCF5355CDFAF5D2AC128D8;
extern const uint32_t g_rgctx_A_t612F28F9085193FA593E2E8784066164A93402DE;
extern const uint32_t g_rgctx_Action_1_tB1A39B1E82C55E0507E491E6A2BADCBC0F688129;
extern const uint32_t g_rgctx_Action_1_Invoke_m91B03EC56246230E433ACE38870639E92C78D3D0;
extern const uint32_t g_rgctx_WXBaseActionOption_1_t96BD42EA47C03858FAD039648C20A0618AA5A2AC;
extern const uint32_t g_rgctx_Action_1_tFF536E1FA6491C36BA2919B75DFA02AA55649805;
extern const uint32_t g_rgctx_T_t8BFFC9CB35DE6C432ED1127ED8B043426550DD47;
extern const uint32_t g_rgctx_Action_1_Invoke_m2B06CF65FB3D4832A1FF72AE0048B571077A86F2;
extern const uint32_t g_rgctx_WXBaseActionOption_2_t0AE2E5B893981619B08A7AEC71BC2A414707466C;
extern const uint32_t g_rgctx_Action_1_t102D02807E7B032959E8D8ECF16760E40D3D05EE;
extern const uint32_t g_rgctx_S_tA2BF266AC081F9C9FE4D6EDDFC4A00383F0704A6;
extern const uint32_t g_rgctx_Action_1_Invoke_m7A47D9A422B206C25325CA01FE84C99EFB9F79C1;
extern const uint32_t g_rgctx_Action_1_t2ED311C9C383536B3ADDC84B2F60222DEB866EAF;
extern const uint32_t g_rgctx_C_tD7E5C5A6713F0E77410D545B2F786EF22502B664;
extern const uint32_t g_rgctx_Action_1_Invoke_m6A294ED2B6A28FEBEEC3D3F0C7A7A5AA82FCE87C;
extern const uint32_t g_rgctx_WXBaseActionOption_3_tF06A2B5E7B56D95616C5CC932504B289CFAFB0C2;
extern const uint32_t g_rgctx_Action_1_t3EC99F0D8ED8FBF019F94A65557254835DDFDCDD;
extern const uint32_t g_rgctx_S_t42199238E17FB4873B9020AE48BD176487A45548;
extern const uint32_t g_rgctx_Action_1_Invoke_m1CE868095647FB05507D2774EF4F22368AC30955;
extern const uint32_t g_rgctx_Action_1_tCD5C8410BD5F175226B5A89648CDB6D47B8D183D;
extern const uint32_t g_rgctx_F_t66A662207ADC22F356F2EBD81B98F52C49473230;
extern const uint32_t g_rgctx_Action_1_Invoke_m817B2D634C77B447E55098FB0C4958C58B4D3F85;
extern const uint32_t g_rgctx_Action_1_t6F042549CE7D3D23AB5FB69AA4EB46D5550E2F8D;
extern const uint32_t g_rgctx_C_t0E847D1E27F6253831CAB5D3D2EF3AE57ED09503;
extern const uint32_t g_rgctx_Action_1_Invoke_m658F90766C2EF6A29C104909FC3F5C462B2EAA61;
extern const uint32_t g_rgctx_Dictionary_2_t914B7E3011B62DB1360CD09228BF0F0E3B9302F7;
extern const uint32_t g_rgctx_Dictionary_2_get_Count_m65DCE4B38635137AF01BE213DCD932A8AF2360D2;
extern const uint32_t g_rgctx_Dictionary_2_ContainsKey_m3889CC0B328C81E112132A3E70D9E89B764ECA39;
extern const uint32_t g_rgctx_T_t57131DC66D2EF27431FFDB6E8D865FCA9D051F3A;
extern const uint32_t g_rgctx_T_t7B1853538C0F534DF50673A9E492294AD67C984B;
extern const uint32_t g_rgctx_JsonMapper_ToObject_TisT_tDCDDFEC383DCED1921514B37C81A0B4E02449707_mD581A3965281A91D94F4F73F3D3E52429D9D685F;
extern const uint32_t g_rgctx_T_tDCDDFEC383DCED1921514B37C81A0B4E02449707;
extern const uint32_t g_rgctx_JsonMapper_ToObject_TisT_tF45FE73A93080E188DC8B503A03E1D187B2818BA_m58DEE2F8927D1E58EA973092C1A01C9E8106644F;
extern const uint32_t g_rgctx_T_tF45FE73A93080E188DC8B503A03E1D187B2818BA;
static const Il2CppRGCTXDefinition s_rgctxValues[67] = 
{
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t05B87E281BD442A7E51E7311F7CC498F2DE2AFD4 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tF4A444D6963737D42FA1425638A99318B5C612A8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tF4A444D6963737D42FA1425638A99318B5C612A8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_AU5BU5D_t5126347664B2F059DE9CF63976124F4A044A5ECA },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_A_t419BE7BD256C16664B4136CEC63935F89CDBB2CE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_AU5BU5D_t5126347664B2F059DE9CF63976124F4A044A5ECA },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tEAEBCADC36FDE9945CB4A7C7A62C6B2FBCFB4ED4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_WXAssetBundle_A_TisT_tEAEBCADC36FDE9945CB4A7C7A62C6B2FBCFB4ED4_m33D03978840B307D7F3375378591F5B7B815F8DC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TU5BU5D_tA1ABF53AE51C00F3EF94577A90937B70BCC12AB1 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tA5FF1B4404CE99EF0FFB87CFA21C3C9F3B21B7CD },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t5F2C022B66489CA8AF33760972405CF206F3AADE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonUtility_FromJson_TisA_t99F6E99DEA52A46B3E87A812B65F883D7CAC52B7_m9DF537346473C3B327B1E4BB19DBED887DADA597 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_A_t99F6E99DEA52A46B3E87A812B65F883D7CAC52B7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t801B8A9D58AE1D225C261AD4C059A83AFCE7D4A3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_m90A5188E41D4908F100C03EEDB251B8186D02879 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t22031FF3C98F160EFF44B810A8F8B276696DB469 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t22031FF3C98F160EFF44B810A8F8B276696DB469 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_A_tD91E49759634FF7137EA3B8CD6FB454C9CAA8534 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_A_tD91E49759634FF7137EA3B8CD6FB454C9CAA8534 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_WXBaseActionOption_1_t92A6B04C0944F8778520E2F9D44C1B2D687B52BB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_tC7CE0701981A6C5548E2B7A78013E092F8E006C6 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_A_t3C9C2A5B56813922EAB8557B3E81F24FAB04BF40 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_A_t3C9C2A5B56813922EAB8557B3E81F24FAB04BF40 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_WXBaseActionOption_2_t93C8D02E6CEB9F04076C2E22A3EF4F393C84C06F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t63DD569B1853DE5F4817EF3B24FA2B7174A820E7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_tA6F1B20172FD3EC2025B7BC2A95A98EF4C25E491 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_A_tE8CCB1CC3A4A1D05F7D3632ED7621054C4F441C6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_A_tE8CCB1CC3A4A1D05F7D3632ED7621054C4F441C6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_WXBaseActionOption_3_t45B4D7C08A289EEB4CEB5E2917765627355A7DBC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_tD5652752192F75EE12CFEAD967D10329F64BAA0F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_tBD9EA1AE73BACFCA63512975453EF0F56336903D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_tDBF138232B0EEEDD9D01D9C2B0560DEC016C1882 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_tD2B749817E93CC3FCBCF316D9C9B1A3BADED451E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonUtility_FromJson_TisA_t612F28F9085193FA593E2E8784066164A93402DE_m31E0D7110FF3126BCCDCF5355CDFAF5D2AC128D8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_A_t612F28F9085193FA593E2E8784066164A93402DE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_tB1A39B1E82C55E0507E491E6A2BADCBC0F688129 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_m91B03EC56246230E433ACE38870639E92C78D3D0 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_WXBaseActionOption_1_t96BD42EA47C03858FAD039648C20A0618AA5A2AC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_tFF536E1FA6491C36BA2919B75DFA02AA55649805 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t8BFFC9CB35DE6C432ED1127ED8B043426550DD47 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_m2B06CF65FB3D4832A1FF72AE0048B571077A86F2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_WXBaseActionOption_2_t0AE2E5B893981619B08A7AEC71BC2A414707466C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t102D02807E7B032959E8D8ECF16760E40D3D05EE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_S_tA2BF266AC081F9C9FE4D6EDDFC4A00383F0704A6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_m7A47D9A422B206C25325CA01FE84C99EFB9F79C1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t2ED311C9C383536B3ADDC84B2F60222DEB866EAF },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_C_tD7E5C5A6713F0E77410D545B2F786EF22502B664 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_m6A294ED2B6A28FEBEEC3D3F0C7A7A5AA82FCE87C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_WXBaseActionOption_3_tF06A2B5E7B56D95616C5CC932504B289CFAFB0C2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t3EC99F0D8ED8FBF019F94A65557254835DDFDCDD },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_S_t42199238E17FB4873B9020AE48BD176487A45548 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_m1CE868095647FB05507D2774EF4F22368AC30955 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_tCD5C8410BD5F175226B5A89648CDB6D47B8D183D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_F_t66A662207ADC22F356F2EBD81B98F52C49473230 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_m817B2D634C77B447E55098FB0C4958C58B4D3F85 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t6F042549CE7D3D23AB5FB69AA4EB46D5550E2F8D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_C_t0E847D1E27F6253831CAB5D3D2EF3AE57ED09503 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_m658F90766C2EF6A29C104909FC3F5C462B2EAA61 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Dictionary_2_t914B7E3011B62DB1360CD09228BF0F0E3B9302F7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_get_Count_m65DCE4B38635137AF01BE213DCD932A8AF2360D2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_ContainsKey_m3889CC0B328C81E112132A3E70D9E89B764ECA39 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t57131DC66D2EF27431FFDB6E8D865FCA9D051F3A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t7B1853538C0F534DF50673A9E492294AD67C984B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonMapper_ToObject_TisT_tDCDDFEC383DCED1921514B37C81A0B4E02449707_mD581A3965281A91D94F4F73F3D3E52429D9D685F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tDCDDFEC383DCED1921514B37C81A0B4E02449707 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonMapper_ToObject_TisT_tF45FE73A93080E188DC8B503A03E1D187B2818BA_m58DEE2F8927D1E58EA973092C1A01C9E8106644F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tF45FE73A93080E188DC8B503A03E1D187B2818BA },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_wxU2Druntime_CodeGenModule;
const Il2CppCodeGenModule g_wxU2Druntime_CodeGenModule = 
{
	"wx-runtime.dll",
	2060,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	4,
	s_reversePInvokeIndices,
	21,
	s_rgctxIndices,
	67,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
